/*! \page from_prev070 New in SDK Version 0.7B

\subsection prev070_overview Overview

This section explain what changes that are needed for plugins written with version 0.6B
of the SDK to change them to use the version 0.7B SDK. If you are converting plugins from
version prior to 0.6B, make the changes needed for version 0.6B.


\subsection prev070_param Parameters

The file parameter has been changed to allow users to animate the file parameter. The most
noticable change is the methods \b get_file() and \b set_file(), which now requires more arguments.

The user can also change the file time offset and time scale (playback speed). This change affects also
to the \b get_file() method call.

The new procedure to use a file parameter is:
\code
	...
	FileHandleC*	pHandle = 0;
	int32			i32FileTime = 0;

	// Get file handle, and file evaluation time.
	// The evaluation time is calculated from the file time offset and time scale.
	m_pParamFile->get_file( i32Time, pHandle, i32FileTime );

	if( pHandle ) {
		// Get importable.
		ImportableImageI*	pImp = (ImportableImageI*)pHandle->get_importable();

		// Evaluate the importable, NOTE: use the file evaluation time here,
		// instead of the time passed to the Composition::EffectI::eval_state().
		pImp->eval_state( i32FileTime, pContext, pTimeContext );
		...
	}
	...
\endcode

The evaluation is important, since all importables are assumed to be animated.

In previous SDK there was an example which used the \b set_time_scale() to change the
length of a file in the Timeline. That is no longer necessary. The time scale value
was saved with the parameter, and the system automatically reads it and uses as time scale,
to be used as in the example above.


\subsection prev070_param Gizmos

AutoGizmoC has new method \b get_parameter_by_id(). It returns a parameter based on the parameter ID
which is given to a parameter when it's created.


\subsection prev070_effect Effects

The file parameter change requires several changesto the effect classes.

First, the \b set_default_file() has new argument, time. The new definition is:
\code
	virtual void				set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
\endcode

The method should look like this:
\code
	void
	TestEffectC::set_default_file( int32 i32Time, FileHandleC* pHandle )
	{
		// Get the file parameter
		ParamFileC*	pParam = (ParamFileC*)m_pAttribGizmo->get_parameter( ATTRIBUTE_PARAM_FILL_FILE );

		// Start editing.
		UndoC*	pOldUndo;
		if( get_undo() ) {
			pOldUndo = pParam->begin_editing( get_undo() );
		}

		// Set the file.
		pParam->set_file( i32Time, pHandle );

		// End editing.
		if( get_undo() ) {
			pParam->end_editing( pOldUndo );
		}
	}
\endcode


The \b eval_state() and \b do_frame() methods are combined into simple \b eval_state(). The reason there were
tho methods was that it might had been possible to optimize the drawing in some situations. It turned out that
the effects ended up caching more and more data, and advantage that the separate methods should have given
actually turned the system slower.

The new method definition is:
\code
	virtual void	eval_state( PajaTypes::int32 i32Time, PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext ) = 0;
\endcode

The purpose of the eval_state() method is separated into two parts. The first part should calculate the
effect transformation, and bounding box. The device checking (and possibly aborting of the eval_state()
should be done after the transformation and bounding box is calculated.

Most of the effects can be converted to this new layout simply by pasting the old do_frame() method after
the old eval_state() method. Some tweaking has to be done, but the transition should go without too huge effort.

\b Important: Read the end of the next section about image flipping!


\subsection prev070_importers Importables

The way importables are handled has been changed. Now the importables has to be evaluated before they are
used. When the evaluation method is called, the importable prepares it's data to a given time. For example
an animation plugin would uncompress the current frame. After the evaluation is done, the other interface methods
can be called and are assumed to return valid data.

Example:
\code
void
AnimImportC::eval_state( int32 i32Time, DeviceContextC* pContext, TimeContextC* pTimeContext )
{
	// convert Demopaja time to FPS timing.
	int32 i32CurrentFrame = pTimeContext->convert_time_to_fps( i32Time, get_fps() );

	// Check if the frame is same as the last decompressed.
	if( m_i32LastFrame == i32CurrentFrame )
		return;

	m_i32LastFrame = i32CurrentFrame;

	// Decompress this frame
	decompress_frame( i32CurrentFrame );

	// Turn on flag indicating that next bind_texture() needs to upload the new frame.
	m_bNeedUpdate = true;
}
\endcode

The changes needed to use the new \b eval_state() method is discussed earlier in this section.


\subsection prev070_image ImportableImageI and ImportableVideoI

Imaportable image has been changed to different pixel aspect rations. This means, that now it's possible
to have texture which size in memory is different that it's physical size. This change was made to allow
any sized images on any hardware platform. For example OpenGL has limitation that the texture size must be
power of two. Now the plugin can resize the image to fit the OpenGL rules, but the image will be shown in
the original size.

The methods:

\code
	virtual PajaTypes::int32		get_width();
	virtual PajaTypes::int32		get_height();
	virtual PajaTypes::int32		get_pitch();
	virtual PajaTypes::int32		get_bpp();
\endcode

are changed to:

\code
	// Returns width of the image data in pixels.
	virtual PajaTypes::int32	get_data_width();
	// Returns height of the image data in pixels.
	virtual PajaTypes::int32	get_data_height();
	// Returns pitch of the image, pitch is the actual length on mempry of the scanline in pixels.
	virtual PajaTypes::int32	get_data_pitch();
	// Return bits per pixel.
	virtual PajaTypes::int32	get_data_bpp();
\endcode

Methods get_width() and get_height() still exist in the new API, but their definitions are different.

\code
	// Returns width of the image in pixels.
	virtual PajaTypes::float32	get_width();
	// Returns height of the image in pixels.
	virtual PajaTypes::float32	get_height();
\endcode


The way the data is stored into the memory has also been changed. OpenGL assumes that the data is stored so that
the first byte of the image is at bottom-right corner. The ImportableImageI interface has now been changed to
assume that storing order.

\b NOTE: This will need the ecisting images to be flipped vertically (upside down), and to change the texture
coordinates used to render the textures.

Importable video interface has been removed from the SDK. It's replaced by the ImportableImageI interface.

New procedural importable type has been created. See the examples.

*/