/*! \page work_parameters Working with parameters

\subsection param_overview Overview

Parameters in a Demopaja plugin effect are the interface between the effect and the user. The parameters are edited either using a parameter type-in dialogs or manipulating the keyframes in the Valuegraph. Most of the parameters can be animated using keyframes. To the developer the animation of a parameter is transparent and the same interface is always used to get the parameter value.

The time values used in a parameter are relative to the time origo of the effect. The system takes care of adjusting the time passed to the effect rendering and state evaluating methods so that the effect class does not have to change the time.

There are seven different kinds of parameters, which all are derived from the same base class ParamI. The base class controls most of the basic functionality of the paramter such as setting parameter flags, name or controller.

When creating a parameter pointer to the parent Gizmo is provided to enable the parameter change notifications to be releyed to the gizmo and further on to the effect. Each parameter also has an ID. This ID is used to identify the parameter notifcations.

\subsection param_int ParamIntC

The integer parameter (ParamIntC) can be used to control integer parameters such as selection lists, or number-of parameters. It is possible to attach labels to anyvalue of the parameter (this is special feature of integer parameter). These labels can be selected using a dropdown list. The type-in styles for integer parameter are dropdown list (\b PARAM_STYLE_COMBOBOX) and simple edit box with spinner button (\b PARAM_STYLE_EDITBOX).

To get the current value of the pamaeter use the get_value() method. The arguments fo the get_value() are time and reference to the variale where the value is stored.

New integer parameter is created using the create_new() static method of the ParamIntC class.

\see Composition::ParamIntC


\subsection param_float ParamFloatC

The float parameter (ParamFloatC) can be used to implement any parameter which can be controlled using one value, such as an angle, or a percentage parameter. The type-in style of float parameter is always edit box (\b PARAM_STYLE_EDITBOX). Additionally two flags can be set: \b PARAM_STYLE_PERCENT or \b PARAM_STYLE_ANGLE. The first makes the parameter to act as a percent in the user interface, percent symbols is shown at the end of the value and the parameter value is scale by 100 before shown in the user interface. The latter makes the parameter to act as a angle in the user interface, the angle symbols is added at the end of the parameter value.

New float parameter is created using the create_new() static method of the ParamIntC class.


\see Composition::ParamFloatC

\subsection param_vector2 ParamVector2C

The vector 2D can be used to control any 2D parameter such as position or scale. The type-in style and percent and angle styles are the same as in the float parameter (see above). If the parameter is used as positional parameter there are additional flags which enables the parameter to be edited in the Demopaja Layout View. These flags are: \b PARAM_STYLE_ABS_POSITION style indicates that the parameter is a in the same coordinate system as the layout. The position parameter of each effect should have this flag. \b PARAM_STYLE_REL_POSITION indicates that the parameter is realtive to the effect position. This style cannot be used alone. One of the follwing two flags have to be used. \b PARAM_STYLE_WORLD_SPACE indicates that a relative positional parameter should be edited in the layout coordinate system. \b PARAM_STYLE_OBJECT_SPACE style indicates a relative parameter should be edited in effect's coordinate system.

If effect has a pivot parameter it should have \b PARAM_STYLE_REL_POSITION and \b PARAM_STYLE_WORLD_SPACE styles. The position parameter should have \b PARAM_STYLE_ABS_POSITION style.

New vector 2D parameter is created using the create_new() static method of the ParamVector2C class.

\see \ref transformations
\see Composition::ParamVector2C


\subsection param_vector3 ParamVector3C

The vector 3D can be used to control any 3D parameter such as position or scale. The type-in style and percent and angle styles are the same as in the float parameter (see above).

New vector 3D parameter is created using the create_new() static method of the ParamVector3C class.

\see Composition::ParamVector3C


\subsection param_color ParamColorC

The color parameter can be sued to control four component (RGBA) colors. The alpha component may be ignored if not used. The type-in style of color parameter is always color picker(\b PARAM_STYLE_COLORPICKER).

New color parameter is created using the create_new() static method of the ParamColorC class.

\see Composition::ParamColorC


\subsection param_text ParamTextC

The text parameter can be used for example to implement text input for text effects. The text parameter cannot be animated. The type-in style of text parameter is always edit box (\b PARAM_STYLE_EDITBOX).

New text parameter is created using the create_new() static method of the ParamTextC class.

\see Composition::ParamTextC


\subsection param_file ParamFileC

The file parameter is the door to the import files for the effect. There is a file handle attached to each file parameter. This handle can be used to get a pointer to the actual importer class. The type-in style of file parameter is always file selector (\b PARAM_STYLE_FILE).

New file parameter is created using the create_new() static method of the ParamTextC class.

\see Composition::ParamFileC

\subsection param_change Changing the Value of a Parameter

If you wish to change the value of a parameter you must store the changes to a undo object. There is no way to create new undo objects and store them into the undo stack. The change is usually needed to do when another parameter is changed. The parameter change can be catched either in the Composition::EffectI::update_notify() or Composition::GizmoI::update_notify() methods. The parameter which is changed has a valid undo object attached to it. The undo object can be get via the get_undo() method.

Here is an example which changes the content of a camera dropdown list when a file parameter is changed. The parameter change notify is catched in the gizmo.


\code

	void
	TestGizmoC::init()
	{
		set_name( "Attributes" );

		m_pParamCamera = ParamIntC::create_new( this, "Camera", 0, ID_TEST_PARAMCAMERA,
			PARAM_STYLE_COMBOBOX, PARAM_ANIMATABLE, 0, 1 );
		m_pParamFile = ParamFileC::create_new( this, "Scene", SUPERCLASS_MESH3D, NULL_CLASSID, ID_TEST_PARAMFILE );
	}

	...

	void
	TestGizmoC::update_notify( uint32 ui32Id, int32 i32Time )
	{
		if( ui32Id == ID_TEST_PARAMFILE ) {
			// The file has changed, update camera list.

			// Get undo object from the changed parameter.
			UndoC*			pUndo = m_pParamFile->get_undo();

			FileHandleC*	pHandle = 0;
			MASImportC*		pImp = 0;

			// Get importable.
			pHandle = m_pParamFile->get_file();
			if( pHandle )
				pImp = (MASImportC*)pHandle->get_importable();
			if( pImp ) {
				// Update labels.

				// Begin undo block.
				UndoC*	pOldUndo = m_pParamCamera->begin_editing( pUndo );

				// Set labels in camera parameter to the names of the cameras in the file.
				m_pParamCamera->clear_labels();
				for( uint32 i = 0; i < pImp->get_camera_count(); i++ ) {
					CameraC*	pCam = pImp->get_camera( i );
					if( !pCam ) continue;
						m_pParamCamera->add_label( i, pCam->get_name() );
				}
				// Limit the parameter range to number of cameras.
				m_pParamCamera->set_min_max( 0, pImp->get_camera_count() );

				// Close undo block.
				m_pParamCamera->end_editing( pOldUndo );
			}
		}
		// Relay the message to the effect
		GizmoI::update_notify( ui32Id, i32Time );
	}

	...

\endcode

\see \ref undo_system

*/