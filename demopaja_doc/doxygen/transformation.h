/*! \page transformations Effect Transformation

\subsection trans_overview Overview

Contructing the effect transformation matrix

The Demopaja system supports plugin effects to be transformed using translation rotation and scale. Additionally rotation and scale can have a pivot point. The plugin effect does not have to support none of these transformations. It is though adviced to support at least position and scale transformation.

The rotation matrix is constructed in following order using the Matrix2C class:

<UL>
\e M = \e P * \e R * \e S * \e T
</UL>

Where \e P is pivot patrix, \e R is rotation matrix, \e S is scale matrix, and \e T is translation matrix.

First the effect is offset to the pivot position then it is rotated, then scaled and finally translated to it's final position. The rotation and scale are in different order than usually expected. This avoids extra calculation which should be handled if the effect were scaled after rotation. It will also ease the editing process, since effects can be easier transformed in groups.

\subsection default_param Default Parameters

The Demopaja system gets the parameters which affects the transformation via the get_default_param() method in the EffectI interface. It is important to implement the method correctly. The parameters can be inside any gizmo, the get_default_param() method just returns the requested parameter.

\see Composition::EffectI::get_default_param()

\subsection pos_param Position
\b DEFAULT_PARAM_POSITION
Position parameter has to be type ParamVector2C and have style: \b PARAM_STYLE_ABS_POSITION.

\subsection pivot_param Pivot
\b DEFAULT_PARAM_PIVOT
Pivot parameter has to be type ParamVector2C and have styles: \b PARAM_STYLE_REL_POSITION and \b PARAM_STYLE_WORLD_SPACE. The system handles pivot parameter a bit different than other parameters which affect the transformation. When a pivot is moved, also the position of the effect changes. If the position parameter is animated the change will also affect the animation. This is done to compensate the pivot change.

\subsection rot_param Rotation
\b DEFAULT_PARAM_ROTATION
Rotation parameter has to be type ParamFloatC. It is adviced to also set the \b PARAM_STYLE_ANGLE style to indicate that the parameter is angle in the user interface.

\subsection scale_param Scale
\b DEFAULT_PARAM_SCALE
Scale parameter has to be type ParamVector2C. It is adviced to also set the \b PARAM_STYLE_PERCENT style to indicate that the parameter is show as percent in the user interface.

\see Composition::EffectI
\see PajaTypes::Matrix2C

*/