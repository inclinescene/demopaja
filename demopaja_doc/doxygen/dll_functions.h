/*! \page dll_functions Creating the DLL functions

\subsection overview Overview

The plugins are interfaced to the Demopaja system via four functions which are implemented inside the DLL. In addition to those four functions also one function is needed for Windows to initialize the DLL. The Demopaja or Windows calls these functions when the plugin is loaded.

The functions are:

<UL>
\li \c DllMain( HANDLE hModule, DWORD ulReasonForCall, LPVOID lpReserved )
\li \c get_classdesc_count()
\li \c get_classdesc( int32 i )
\li \c get_api_version()
\li \c get_dll_name()
</UL>

The \c __declspec( dllexport ) before each function declaration (see code snippet below) makes sure the functions can be loaded from the plugin. Only the four last functions has to have this declaration.

The first funtion \c DllMain() is a Windows specific funtions which is called when the plugin is loaded. Since there is nothing to do in it, it may simply return TRUE to indicate that everything in initialisation went ok. See Windows API documentation for more information about DllMain.

The second function \c get_classdesc_count() returns number of plugin class descripors in the DLL. One DLL may have several class descriptors, one for each plugin class. The classdescriptor is used to provide more information about the plugin class, and also to create new instances of the plugin class.

The third function \c get_classdesc() returns the class descriptor at specified index. Since a pointer is returned the class descriptor has to be statically declared (such as a global variable) inside the DLL.

The fourth function \c get_api_version() returns the version number of the Demopaja API which was used to compile the plugin. The version information is used at load time to check if the plugin can be used with the current program version.

Finally, the fifth function \c get_dll_name() returns a short description about the DLL as a null terminated string.

The basic skeleton of a plugin DLL should look like this:

\code
	#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from
					// Windows headers
	#include <windows.h>

	// Demopaja headers
	#include "DemopajaVersion.h"
	#include "PajaTypes.h"

	using namespace PajaTypes;

	// The DLL
	BOOL APIENTRY
	DllMain( HANDLE hModule, DWORD ulReasonForCall, LPVOID lpReserved )
	{
	    return TRUE;
	}

	// Returns number of classes inside this plugin DLL.
	__declspec( dllexport )
	int32
	get_classdesc_count()
	{
		return 1;
	}

	// Returns class descriptors of the plugin classes.
	__declspec( dllexport )
	ClassDescC*
	get_classdesc( int32 i )
	{
		if( i == 0 )
			return g_rImageDesc;
		return 0;
	}

	// Returns the API version this DLL was made with.
	__declspec( dllexport )
	int32
	get_api_version()
	{
		return DEMOPAJA_VERSION;
	}

	// Returns the DLL name.
	__declspec( dllexport )
	char*
	get_dll_name()
	{
		return "PCXimport.dll - PCX importer (c) 2000 memon/moppi productions";
	}
\endcode

*/