/*! \mainpage SDK Documentation

<CENTER>
\image html pajalogo_small.gif

</CENTER>

\section gen General

\li \ref work_parameters
\li \ref undo_system
\li \ref transformations
\li \ref initialisation

\section proj Plugin Projects

\li \ref new_project
\li \ref dll_functions
\li \ref basic_import
\li \ref basic_effect
\li \ref html_help
\li \ref from_prev060
\li \ref from_prev070



*/