/*! \page html_help Demopaja Help System

\subsection help_overview Overview

The help files in Demopaja system are in HTML format. The application has internal help viewer which will get the file name of the help file via a method in a plugin class descriptor. The file path is relative to the plugin directory. The plugin directory is paste at the beginning of the help file name before it is loaded in to the help viewer. There are no restrictions in the help file.

\subsection help_resource Help Files in Resources

It is possible to store the help files as resource files. It is also possible to store images to the resource which is shown in the help file. See the example plugins how to do it.

*/