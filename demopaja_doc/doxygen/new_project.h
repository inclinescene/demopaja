/*! \page new_project Creating a New Plugin Project

\subsection new_overview Overview

This section explains the procedures which are common to all new plugin projects. It is assumed that the developer uses Microsoft Visual C++.


\subsection new_file New Project File

To create a new Demopaja plugin project file follow the steps below:

-# Choose \b File > \b New... then make sure the \b Projects tab is active.
-# Select \b Win32 \b Dynamic \b Link \b Library from the list.
-# Select the directory where the plugin project is created and fill in the project name.
-# Press \b OK button to exit the dialog.
-# At the prompt \b What \b kind \b of \b DLL \b would \b you \b like \b to \b create? choose \b An \b Empty \b DLL \b project.

\subsection proj_settings Setting the Project Settings

First you will need to specify the output file name for the plugin. It is convenient to set the output directory to the directory where the other Demopaja plugins are. This way you don't have to copy the plugin file all the time. It's assumed that the Demopaja application is installed in directory \b C:\demopaja. You specify the output file name as follows:

-# Choose \b Project > \b Settings... (ALT + F7). Make sure all the settings arebeing changed by selecting \b All \b Configurations from the \b Settings \b For: dropdown list. The next procedures assumes that all configurations are select until otherwise told.
-# Select the \b Link tab.
-# On the \b Output \b file \b name: editbox enter the path for the Demopaja plugin directory plus the name of you plugin. For example: \b C:\demopaja\plugins\myplugin.dll.

Since DLL cannot be run themselfs we need to specify an executable which will be run. Of course we will use the Demopaja application. To select the application:

-# Select the \b Debug tab from the \b Project \b Settings dialog.
-# Press the arrow next to the \b Executable \b for \b the \b debug \b session editbox and Choose \b Browse....
-# Browse to the Demopaja directory and select the Demopaja executable. Press \b OK. Now the editbox should read for example: \b C:\demopaja\demopaja.exe.
-# In the working directory write the Demopaja path. For example: \b C:\demopaja.

The third change to the project settings is to specify the path where the Demopaja SDK header files can be found. If you have the Demopaja SDK installed on \b C:\demopajasdk use the following:

-# Select the \b C/C++ tab from the \b Project \b Settings dialog.
-# Select \b Preprocessor rom the \b Category: dropdown list.
-# On the \b Additional \b include \b directories: editbox enter the path \b C:\demopajasdk\include.

The next step is to specify the libraries you need to use. You should still keep the \b All \b Configurations selected. Pay attention to the library names. The debug library ends to \c D character and release to \c R.

-# Select the \b Link tab from the \b Project \b Settings dialog.
-# Choose the \b Input from the \b Category: dropdown list.
-# On the \b Additional \b library \b path: editbox write the path \b C:\demopajasdk\lib.

Now update the \b Win32 \b Debug settings:

-# From the \b Settings \b For: dropdown list choose \b Win32 \b Debug.
-# From the \b Link tab, under the \b Category: dropdown list select \b General.
-# Add \b corelibd.lib to the \b Object/library modules: edit box.
-# From the \b C/C++ tab, under the \b Category: dropdown list select \b Code generation.
-# Make sure \b Debug \b Multithreaded is selected from the \b Use \b run-time \b library: dropdown list.

Now update the \b Win32 \b Release settings:

-# From the \b Settings \b For: dropdown list choose \b Win32 \b Release.
-# From the \b Link tab, under the \b Category: dropdown list select \b General.
-# Add \b corelibr.lib to the \b Object/library \b modules: edit box.
-# From the \b C/C++ tab, under the \b Category: dropdown list select \b Code \b generation.
-# Make sure \b Multithreaded is selected from the \b Use \b run-time \b library: dropdown list.

Now all the settings for the project are set. It is good idea to save the workspace now.

\subsection add_cpp Adding CPP file to the project

To create a new plugin class you will need a C++ source file where the code for the plugin is written. To create a new C++ source file follow the steps below:

-# Choose \b File > \b New... then make sure the \b Files tab is activate.
-# Select \b C++ \b Source File from the list.
-# Fill in the name for the file and press \b OK to exit the dialog.

In addition to the CPP source file you propably will need a C++ header file too. The steps to create a new header file is similar, just choose \b C/C++ \b Header \b File from the \b Files list.

\see \ref dll_functions

*/