/*! \page initialisation Understanding the Intialisation procedure

\subsection init_overview Overview

The calling of the \b initialize() method for EffectI and ImportableI depends on which
device drivers is used. For example OpenGL device dirver has far less calls to that methods compared
to DirectX 8 driver since, the DX8 driver handles the device lost status via intialize() method.

But there are some ruler which applies to all devices.

\subsection init_new_effect New Effect

Right after a new effect is created, the \b initialize() method is called with reason \b INIT_INTIAL_UPDATE.

\subsection init_new_importable New Importable

Right after importable is created and the \b load_file() is called, the \b initialize() method is called with reason \b INIT_INTIAL_UPDATE.

\subsection init_load Load

When all the files and effects are loaded and all device drivers are created the \b initialize() method of all importables are called. The initialise method is the first called method where the devices are quaranteed to be valid. So you should not use any device specifid code in \b load() and \b load_file() methods!

\subsection init_device_change Device Changed

When a device is changed (for example Graphics Device) the initialise method is called with reason \b INIT_DEVICE_CHANGED. The device which is about to be shut down, has set state \b DEVICE_STATE_SHUTTINGDOWN.
After the device has chaged, a new call to the \b intialize() method is made with reason \b INIT_DEVICE_VALIDATE. Then the effect or importable should check the device context for available devices reinitialise device specific data (such as textures).

\subsection init_graph_device DX8 Graphics device specific

The Direct X driver has a state called lost device. This is D3D specific and it happens everytime the main surface changes (full screen change, window size change, etc.). This state is handled via the \b initialize() method. When ever a device is about to be lost, the \b initialize() method is called with reason \b INIT_DEVICE_INVALIDATE. If then the graphics device driver has \b DEVICE_STATE_LOST state set, the effect or importer should handle the device lost state. When the device is restored again, the \b intialize() method is called with reason \b INIT_DEVICE_VALIDATE.

Example:
\code
void
TestEffectC::initialize( uint32 ui32Reason, DeviceContextC* pContext, TimeContextC* pTimeContext )
{
	// Get the device and make sure it exists
	DX8DeviceC*	pDevice = (DX8DeviceC*)pContext->query_interface( CLASS_DX8_DEVICEDRIVER );
	if( !pDevice )
		return;

	if( ui32Reason == INIT_INITIAL_UPDATE ) {
		// Do the initial update
	}
	else if( ui32Reason == INIT_DEVICE_CHANGED ) {
		if( pDevice->get_state() == DEVICE_STATE_SHUTTINGDOWN ) {
			// The device is being shut down...
		}
	}
	else if( ui32Reason == INIT_DEVICE_INVALIDATE ) {
		if( pDevice->get_state() == DEVICE_STATE_LOST ) {
			// The device is lost
		}
	}
	else if( ui32Reason == INIT_DEVICE_VALIDATE ) {
		if( pDevice->get_state() == DEVICE_STATE_OK ) {
			// The device is got back
		}
	}

}
\endcode

*/