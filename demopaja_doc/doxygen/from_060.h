/*! \page from_prev060 New in SDK Version 0.6B

\subsection prev060_overview Overview

This section explain what changes that are needed for plugins written with version 0.5B
of the SDK to change them to use the version 0.6B SDK.

\subsection prev060_classdesc Class Descriptor

The class descriptor to changes: first, \b ClassDescC::get_classid(), and \b PluginClass::ClassDescC::get_super_classid() are changed to PluginClass::ClassDescC::get_class_id(), and PluginClass::ClassDescC::get_class_id() respectively (notice the underscore!).

The device support detection is change too. The former method \b check_device_support() no longer exists, use the new methods instead.

Example:
\code
uint32
TestImportDescC::get_required_device_driver_count() const
{
	return 1;
}

const ClassIdC&
TestImportDescC::get_required_device_driver( uint32 ui32Idx )
{
	if( ui32Idx == 0 )
		return PajaSystem::CLASS_OPENGL_DEVICEDRIVER;
	
	return PluginClass::NULL_CLASSID;
}
\endcode

See also:
\see PluginClass::ClassDescC::get_required_device_driver_count()
\see PluginClass::ClassDescC::get_required_device_driver()


\subsection prev060_driver OpenGL Device Driver

Previously the OpenGL rendering was done thru OpenGLInterfaceC class. This has change so that there is OpenGL device (PajaSystem::OpenGLDeviceC) and you have to query the viewport interface (PajaSystem::OpenGLViewportC) from the device to use it (more on this later). The viewport class has all the same functionality as the former \b OpenGLInterfaceC had. You have to change the include files to match the new setup:

\code
#include "opengldriver\OpenGLDeviceC.h"
#include "opengldriver\OpenGLViewportC.h"
\endcode


\subsection prev060_editable Editable

First the definition copy() method of all classes derived from EditableI has changed.
The new definition is:

\code

virtual void	copy( EditableI* pEditable ) = 0;

\endcode

(The parameter was of type DataBlockI)


\subsection prev060_gizmo Gizmo

The return type of Composition::GizmoI::update_notify() is changed to PajaTypes::uint32. This makes it possible to
change the number of parameters in the \b update_notify() method and the UI updates correctly
after the change.


\subsection prev060_effect Effects

The return type of Composition::EffectI::update_notify() is changed to PajaTypes::uint32. This makes it possible to change the number of parameters and gizmos in the \b update_notify() method and the UI updates correctly
after the change.

The method Composition::EffectI::initialise() takes a new first parameter the \b initialize() method should be modified to:

\code
const ClassIdC&
void TestImportC::initialize( PajaTypes::uint32 ui32Reason, DeviceContextC* pContext, TimeContextC* pTimeContext )
\endcode

The reason parameter tells the state the effect or importer has to be initialised.
For example the DirectX has a state caleld "lost device" this state change is handle
using the \b initialise() method.

The old initialise equals to \b initialise() called with parameter \b INIT_INITIAL_UPDATE.

Since the device system has changed quite much, there's new way to query the device from the device context.

Example:
\code
void
TestPlayerEffectC::do_frame( DeviceContextC* pContext )
{
	// Get the OpenGL device.
	OpenGLDeviceC*	pDevice = (OpenGLDeviceC*)pContext->query_interface( CLASS_OPENGL_DEVICEDRIVER );
	if( !pDevice )
		return;

	// Get the OpenGL viewport.
	OpenGLViewportC*	pViewport = (OpenGLViewportC*)pDevice->query_interface( GRAPHICSDEVICE_VIEWPORT_INTERFACE );
	if( !pViewport )
		return;

	// Set orthographic projection.
	pViewport->set_ortho( m_rBBox, m_rBBox[0][0], m_rBBox[1][0], m_rBBox[1][1], m_rBBox[0][1] );

	...
}
\endcode


Also note that Import::ImportableImageI::bind_texture() requires the device interface, not the viewport interface.
Example
\code
	...
	pImp->bind_texture( m_i32Frame, pDevice, i32Flags );
	...
\endcode


\subsection prev060_importers Importables

For image/video importers, the testing of the correct device has changed.

Example:
\code
void
TestImportC::bind_texture( DeviceInterfaceI* pInterface, uint32 ui32Properties )
{
	if( !pInterface || pInterface->get_class_id() != CLASS_OPENGL_DEVICEDRIVER )
		return;
	...
}
\endcode

The importables have the Import::ImportableI::initialize() method too. \see initialisation for more details.

*/