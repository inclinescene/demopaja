// FileDialogEx.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "FileDialogEx.h"
#include "Dlgs.h"
#include <string.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileDialogEx

IMPLEMENT_DYNAMIC(CFileDialogEx, CFileDialog)

CFileDialogEx::CFileDialogEx(BOOL bOpenFileDialog, BOOL bAddButton, LPCTSTR lpszDefExt, LPCTSTR lpszFileName,
		DWORD dwFlags, LPCTSTR lpszFilter, CWnd* pParentWnd) :
		CFileDialog(bOpenFileDialog, lpszDefExt, lpszFileName, dwFlags, lpszFilter, pParentWnd),
		m_SubWnd( 0 ),
		m_bAddButton( bAddButton )
{
	m_SubWnd.m_pFileDlg = this;
}


BEGIN_MESSAGE_MAP(CFileDialogEx, CFileDialog)
	//{{AFX_MSG_MAP(CFileDialogEx)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


BOOL CFileDialogEx::OnInitDialog() 
{
	CFileDialog::OnInitDialog();

	if( m_bAddButton ) {

		CWnd*	pW = GetParent(); // Get a pointer to the parent window.
		CWnd*	pCtl;
		CRect	rWinRect, rRect;

		pW->GetWindowRect( &rWinRect ); //Get rect of parent window

		// Get the height of the OK button.
		int	iBtnHeight;
		pCtl = pW->GetDlgItem( IDOK ); //Fetch control
		pCtl->GetWindowRect( &rRect );   //Get control's rect
		iBtnHeight = rRect.Height();

		// Create a new CMySubClassWnd so we can catch dialog control notifications
		VERIFY( m_SubWnd.SubclassWindow( pW->m_hWnd ) );
		
		// Resize the parent window
		pW->SetWindowPos( NULL, 0, 0, 
			rWinRect.right - rWinRect.left + ((3 * iBtnHeight) / 2),
			rWinRect.bottom - rWinRect.top,
			SWP_NOMOVE );

		// Move IDOK and IDCANCEL buttons
		pCtl = pW->GetDlgItem( IDOK );
		pCtl->GetWindowRect( &rRect );
		pW->ScreenToClient( &rRect );
		pCtl->SetWindowPos( NULL, rRect.left + ((3 * iBtnHeight) / 2), rRect.top, 0, 0, SWP_NOSIZE );
			
		pCtl = pW->GetDlgItem( IDCANCEL );
		pCtl->GetWindowRect( &rRect );
		pW->ScreenToClient( &rRect );
		pCtl->SetWindowPos( NULL, rRect.left + ((3 * iBtnHeight) / 2), rRect.top, 0, 0, SWP_NOSIZE );

		// Resize the folder view
		pCtl = pW->GetDlgItem( lst1 );
		pCtl->GetWindowRect( &rRect );
		pW->ScreenToClient( &rRect );
		pCtl->SetWindowPos( NULL, 0, 0, rRect.right - rRect.left + ((3 * iBtnHeight) / 2), rRect.bottom - rRect.top, SWP_NOMOVE );

		// Create the increment button
		pCtl = pW->GetDlgItem( IDOK );
		pCtl->GetWindowRect( &rRect );
		pW->ScreenToClient( &rRect );

		rRect.left -= ((3 * iBtnHeight) / 2);
		rRect.right = rRect.left + iBtnHeight;

		m_rIncButton.Create( "+", BS_PUSHBUTTON, rRect, pW, IDC_FILEDLG_INCBUTTON );
		m_rIncButton.ShowWindow( SW_SHOW );
		m_rIncButton.SetFont( pCtl->GetFont(), TRUE );
	}	

	return TRUE;
}

void CFileDialogEx::OnAddButton()
{
	CWnd*	pW = GetParent(); // Get a pointer to the parent window.
	CWnd*	pCtlOK = pW->GetDlgItem( IDOK );
	CWnd*	pCtlEdit = pW->GetDlgItem( edt1 );
	
	// Get the file name
	CString	sName;
	pCtlEdit->GetWindowText( sName );

	// If the Increment
	char szPathBuffer[_MAX_PATH];
	char szDrive[_MAX_DRIVE];
	char szDir[_MAX_DIR];
	char szFName[_MAX_FNAME];
	char szExt[_MAX_EXT];
	_splitpath( sName, szDrive, szDir, szFName, szExt );

	int	iLen = strlen( szFName );
	if( iLen > 0 ) {
		for( int i = iLen - 1; i >= 0; i-- ) {
			// Find first non-number character in the filename body.
			if( (szFName[i] < '0' || szFName[i] > '9') ) {
				if( i == (iLen - 1) ) {
					// There were no numbers at the end of the file name, add default number.
					strcat( szFName, "_000" );
				}
				else {
					// Count number of number digits in the file name.
					int	iNumDigits = (iLen - 1) - i;

					// Make copy of the filename body (exclude the number part).
					char	szTemp[_MAX_FNAME];
					strcpy( szTemp, szFName );
					szTemp[i + 1] = '\0';

					// Create format string for printf
					char	szFormat[16] = "%s%02d";
					sprintf( szFormat, "%%s%%0%dd\0", iNumDigits );

					// Read the current number from the file name.
					int	iNum;
					sscanf( szFName + (i + 1), "%d", &iNum );

					// Create new, incremented file name.
					sprintf( szFName, szFormat, szTemp, iNum + 1 );
				}
				break;
			}
		}
	}

	// Complete the path.
	_makepath( szPathBuffer, szDrive, szDir, szFName, szExt );

	sName = szPathBuffer;

	// Set the file name
	pCtlEdit->SetWindowText( sName );

	// emulate the OK button
	pW->PostMessage( WM_COMMAND, MAKEWPARAM( IDOK, BN_PUSHED ), (LPARAM)pCtlOK->m_hWnd );
}
