// FloatTypeInDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "demopajadoc.h"
#include "FloatTypeInDlg.h"
#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "ParamI.h"

using namespace PajaTypes;
using namespace Composition;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFloatTypeInDlg dialog


void
AFXAPI
CFloatTypeInDlg::DDX_MyText( CDataExchange* pDX, int nIDC, float& value )
{
	TCHAR	szBuffer[32];
	TCHAR	szFormat[32];
	HWND	hWndCtrl = pDX->PrepareEditCtrl( nIDC );

	if( pDX->m_bSaveAndValidate ) {
		// to value
		szBuffer[0] = 0;
		::GetWindowText( hWndCtrl, szBuffer, 30 );
		value = _tcstod( szBuffer, NULL );
		value /= m_f32Scale;
	}
	else {
		// from value
		_sntprintf( szFormat, 31, "%%.%df", (int32)__max( -floor( log10( m_f32Inc ) ), 0 ) );
		_sntprintf( szBuffer, 31, szFormat, value * m_f32Scale );
		::SetWindowText( hWndCtrl, szBuffer );
	}
}


CFloatTypeInDlg::CFloatTypeInDlg(CWnd* pParent /*=NULL*/) :
	CDialog(CFloatTypeInDlg::IDD, pParent),
	m_f32Min( 0 ),
	m_f32Max( 0 ),
	m_f32Inc( 0.1f ),
	m_bClampValues( false ),
	m_f32Scale( 1 )
{
	//{{AFX_DATA_INIT(CFloatTypeInDlg)
	m_rValue = 0.0f;
	m_sInfoText = _T("");
	//}}AFX_DATA_INIT
}


void CFloatTypeInDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFloatTypeInDlg)
	DDX_Control(pDX, IDOK, m_rOk);
	DDX_Control(pDX, IDCANCEL, m_rCancel);
	DDX_Control(pDX, IDC_LABEL1, m_rStaticLabel1);
	DDX_Control(pDX, IDC_EDIT, m_rEdit);
	DDX_MyText(pDX, IDC_EDIT, m_rValue);
	DDX_Text(pDX, IDC_INFO, m_sInfoText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFloatTypeInDlg, CDialog)
	//{{AFX_MSG_MAP(CFloatTypeInDlg)
	ON_EN_CHANGE(IDC_EDIT, OnChangeEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFloatTypeInDlg message handlers

BOOL CFloatTypeInDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if( m_pParam->get_style() & PARAM_STYLE_PERCENT ) {
		m_f32Inc *= 100.0f;
		m_f32Scale = 100.0f;
		m_f32Min *= 100.0f;
		m_f32Max *= 100.0f;

		m_rStaticLabel1.SetWindowText( "%" );
	}
	else if( m_pParam->get_style() & PARAM_STYLE_ANGLE ) {
		m_rStaticLabel1.SetWindowText( "�" );
	}
	
	if( m_bClampValues ) {
		if( !m_rSpinner.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT | SPNB_USELIMITS, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
			return FALSE;
		m_rSpinner.SetMinMax( m_f32Min, m_f32Max );
	}
	else
		if( !m_rSpinner.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
			return FALSE;

	m_rSpinner.SetScale( m_f32Inc );
	m_rSpinner.SetBuddy( &m_rEdit, SPNB_ATTACH_RIGHT );

	// update dialog data
	m_pParam->get_val( m_i32Time, m_rValue );
	UpdateData( FALSE );

	m_rOk.SetFlat( FALSE );
	m_rCancel.SetFlat( FALSE );

	return TRUE;
}

void CFloatTypeInDlg::OnChangeEdit() 
{
	UpdateData( TRUE );

	if( m_bClampValues ) {
		// clamp value
		if( m_rValue < m_f32Min )
			m_rValue = m_f32Min;
		if( m_rValue > m_f32Max )
			m_rValue = m_f32Max;
	}

	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );
	pDoc->HandleParamNotify( m_pParam->set_val( m_i32Time, m_rValue ) );
	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}

