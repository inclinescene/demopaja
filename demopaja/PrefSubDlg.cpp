// PrefSubDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "PrefSubDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CPrefSubDlg dialog

IMPLEMENT_DYNCREATE( CPrefSubDlg, CDialog )

CPrefSubDlg::CPrefSubDlg()
{
	// dont use this constructor
	ASSERT( 0 );
}

CPrefSubDlg::CPrefSubDlg( UINT iID, CWnd* pParent )
	: CDialog( iID )
{
	m_iID = iID;
}

CPrefSubDlg::~CPrefSubDlg()
{
	// empty
}

BEGIN_MESSAGE_MAP(CPrefSubDlg, CDialog)
	//{{AFX_MSG_MAP(CPrefSubDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


UINT CPrefSubDlg::GetID()
{
	return m_iID;
}

void CPrefSubDlg::OnOK()
{
   EndDialog( IDOK );
}

void CPrefSubDlg::OnCancel()
{
   EndDialog( IDCANCEL );
}

BOOL CPrefSubDlg::PreTranslateMessage(MSG* pMsg) 
{
	// Don't let CDialog process the Escape key.
	if( (pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_ESCAPE) ) {
		return TRUE;
	}
	
	// Don't let CDialog process the Return key, if a multi-line edit has focus
	if( (pMsg->message == WM_KEYDOWN) && (pMsg->wParam == VK_RETURN) ) {
		// Special case: if control with focus is an edit control with
		// ES_WANTRETURN style, let it handle the Return key.

		TCHAR szClass[10];
		CWnd* pWndFocus = GetFocus();
		if( ((pWndFocus = GetFocus()) != NULL) &&
			IsChild( pWndFocus ) &&
			(pWndFocus->GetStyle() & ES_WANTRETURN) &&
			GetClassName( pWndFocus->m_hWnd, szClass, 10 ) &&
			(lstrcmpi( szClass, _T( "EDIT" ) ) == 0) )
		{
			pWndFocus->SendMessage( WM_CHAR, pMsg->wParam, pMsg->lParam );
			return TRUE;
		}

		return FALSE;
	}

	return CDialog::PreTranslateMessage(pMsg);
}
