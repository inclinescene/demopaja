// DropTarget.h: interface for the CDropTarget class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DROPTARGET_H__9E1DC18C_0113_4877_9E36_79297C59A9AF__INCLUDED_)
#define AFX_DROPTARGET_H__9E1DC18C_0113_4877_9E36_79297C59A9AF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CDropTarget  
{
public:
	CDropTarget();
	virtual ~CDropTarget();

	virtual DROPEFFECT OnDragOver( COleDataObject* pDataObject, DWORD dwKeyState, CPoint point ) = 0;
	virtual BOOL OnDrop( COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point ) = 0;
};

#endif // !defined(AFX_DROPTARGET_H__9E1DC18C_0113_4877_9E36_79297C59A9AF__INCLUDED_)
