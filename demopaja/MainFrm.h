// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__B4C5B4EC_2A90_11D4_A80C_0000E8D926FD__INCLUDED_)
#define AFX_MAINFRM_H__B4C5B4EC_2A90_11D4_A80C_0000E8D926FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// Controlbars
#include "InspectorBar.h"
#include "TimelineBar.h"
#include "TransformDlg.h"
#include "AlignDlg.h"
#include "HelpDlg.h"
//#include "ParameterBar.h"

enum BarVisE {
	STATUSBAR_VIS = 0x01,
	TOOLBAR_VIS = 0x02,
	TIMELINE_VIS = 0x04,
	INSPECTOR_VIS = 0x08,
	PARAMETER_VIS = 0x10
};

class CMainFrame : public CFrameWnd
{
	
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:

// Operations
public:

	void			DockControlBarLeftOf( CControlBar* pBar, CControlBar* pPrev );
	void			DockControlBarTopOf( CControlBar* pBar, CControlBar* pPrev );

//	void			UpdateAllWindows( bool bRecalc = false );
//	void			Redraw( PajaTypes::uint32 ui32Flags, PajaTypes::uint32 ui32Ignore = 0 );
	void			NotifyViews( PajaTypes::uint32 ui32Notify );

	CTimeLineBar*	GetTimelineBar();


	void			BrowseHelp( const PluginClass::ClassIdC& rClassId );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL DestroyWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

	BOOL	VerifyBarState( LPCTSTR lpszProfileName );


protected:  // control bar embedded members
	CStatusBar		m_wndStatusBar;
	CToolBar		m_wndStandardBar;
	CToolBar		m_wndToolsBar;
	CToolBar		m_wndPlayBar;
	CTimeLineBar	m_wndTimeLine;
	CInspectorBar	m_wndInspector;
	CTransformDlg*	m_pTransDlg;
	CAlignDlg*		m_pAlignDlg;
	CHelpDlg*		m_pHelpDlg;
//	CParameterBar	m_wndParameter;

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnEditUndo();
	afx_msg void OnUpdateEditUndo(CCmdUI* pCmdUI);
	afx_msg void OnUpdateHelpPlugins(CCmdUI* pCmdUI);
	afx_msg void OnHelpPlugins();
	afx_msg void OnEditPurgeUndostack();
	afx_msg void OnUpdateEditPurgeUndostack(CCmdUI* pCmdUI);
	afx_msg void OnEditRedo();
	afx_msg void OnUpdateEditRedo(CCmdUI* pCmdUI);
	afx_msg void OnViewFileinspector();
	afx_msg void OnUpdateViewFileinspector(CCmdUI* pCmdUI);
	afx_msg void OnViewTimeline();
	afx_msg void OnUpdateViewTimeline(CCmdUI* pCmdUI);
	afx_msg void OnFileImport();
	afx_msg void OnUpdateFileImport(CCmdUI* pCmdUI);
	afx_msg void OnEditDemoproperties();
	afx_msg void OnUpdateEditDemoproperties(CCmdUI* pCmdUI);
	afx_msg void OnViewGrid();
	afx_msg void OnUpdateViewGrid(CCmdUI* pCmdUI);
	afx_msg void OnViewSnaptogrid();
	afx_msg void OnUpdateViewSnaptogrid(CCmdUI* pCmdUI);
	afx_msg void OnViewGridsettings();
	afx_msg void OnUpdateViewGridsettings(CCmdUI* pCmdUI);
	afx_msg void OnPlayPlay();
	afx_msg void OnUpdatePlayPlay(CCmdUI* pCmdUI);
	afx_msg void OnPlayPlaypreview();
	afx_msg void OnUpdatePlayPlaypreview(CCmdUI* pCmdUI);
	afx_msg void OnFileMerge();
	afx_msg void OnUpdateFileMerge(CCmdUI* pCmdUI);
	afx_msg void OnViewToolstoolbar();
	afx_msg void OnUpdateViewToolstoolbar(CCmdUI* pCmdUI);
	afx_msg void OnToolArrow();
	afx_msg void OnUpdateToolArrow(CCmdUI* pCmdUI);
	afx_msg void OnToolKeyarrow();
	afx_msg void OnUpdateToolKeyarrow(CCmdUI* pCmdUI);
	afx_msg void OnToolPan();
	afx_msg void OnUpdateToolPan(CCmdUI* pCmdUI);
	afx_msg void OnToolRotate();
	afx_msg void OnUpdateToolRotate(CCmdUI* pCmdUI);
	afx_msg void OnUpdateToolScale(CCmdUI* pCmdUI);
	afx_msg void OnToolScale();
	afx_msg void OnToolZoom();
	afx_msg void OnUpdateToolZoom(CCmdUI* pCmdUI);
	afx_msg void OnViewTransformdialog();
	afx_msg void OnUpdateViewTransformdialog(CCmdUI* pCmdUI);
	afx_msg void OnViewAligndialog();
	afx_msg void OnUpdateViewAligndialog(CCmdUI* pCmdUI);
	afx_msg void OnFileExport();
	afx_msg void OnUpdateFileExport(CCmdUI* pCmdUI);
	afx_msg void OnViewHelpdialog();
	afx_msg void OnUpdateViewHelpdialog(CCmdUI* pCmdUI);
	afx_msg void OnViewLayoutrulers();
	afx_msg void OnUpdateViewLayoutrulers(CCmdUI* pCmdUI);
	afx_msg void OnViewToolbar();
	afx_msg void OnUpdateViewToolbar(CCmdUI* pCmdUI);
	afx_msg void OnViewPlaytoolbar();
	afx_msg void OnUpdateViewPlaytoolbar(CCmdUI* pCmdUI);
	afx_msg void OnFileExportavi();
	afx_msg void OnUpdateFileExportavi(CCmdUI* pCmdUI);
	afx_msg void OnViewParameters();
	afx_msg void OnUpdateViewParameters(CCmdUI* pCmdUI);
	//}}AFX_MSG
	afx_msg void OnUpdateIndicatorGraphDevice(CCmdUI* pCmdUI);
	afx_msg void OnUpdateIndicatorProjectPath(CCmdUI* pCmdUI);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEditProjectpath();
	afx_msg void OnUpdateEditProjectpath(CCmdUI *pCmdUI);
	afx_msg void OnHelpHelpindex();
	afx_msg void OnUpdateHelpHelpindex(CCmdUI *pCmdUI);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__B4C5B4EC_2A90_11D4_A80C_0000E8D926FD__INCLUDED_)
