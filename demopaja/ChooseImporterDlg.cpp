// ChooseImporterDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "ChooseImporterDlg.h"
#include "PajaTypes.h"
#include <vector>

using namespace PajaTypes;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CChooseImporterDlg dialog


CChooseImporterDlg::CChooseImporterDlg(CWnd* pParent /*=NULL*/) :
	CDialog(CChooseImporterDlg::IDD, pParent),
	m_i32SelectedImporter( -1 )
	, m_bUseToAll(FALSE)
	{
	//{{AFX_DATA_INIT(CChooseImporterDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CChooseImporterDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CChooseImporterDlg)
	DDX_Control(pDX, IDC_STATICFILE, m_rStaticFile);
	DDX_Control(pDX, IDC_IMPORTERLIST, m_rImporterList);
	//}}AFX_DATA_MAP
	DDX_Check(pDX, IDC_CHECKUSEALL, m_bUseToAll);
}


BEGIN_MESSAGE_MAP(CChooseImporterDlg, CDialog)
	//{{AFX_MSG_MAP(CChooseImporterDlg)
	//}}AFX_MSG_MAP
	ON_NOTIFY(NM_DBLCLK, IDC_IMPORTERLIST, OnNMDblclkImporterlist)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CChooseImporterDlg message handlers

BOOL CChooseImporterDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	CRect	rRect;
	m_rImporterList.GetClientRect( rRect );

	m_rImporterList.InsertColumn( 0, "", LVCFMT_LEFT, rRect.Width() - 5 );

	for( uint32 i = 0; i < m_rImporters.size(); i++ ) {
		int32	i32Idx = m_rImporterList.InsertItem( i, m_rImporters[i].m_sName );
		m_rImporterList.SetItemData( i32Idx, m_rImporters[i].m_i32Id );
	}

	char	szPathBuffer[_MAX_PATH];
	char	szDrive[_MAX_DRIVE];
	char	szDir[_MAX_DIR];
	char	szFName[_MAX_FNAME];
	char	szExt[_MAX_EXT];
	_splitpath( m_sFileName, szDrive, szDir, szFName, szExt );

	CString	sText;

	sText = szFName;
	sText += szExt;
	sText += "\n";
	sText += szDrive;
	sText += szDir;

	m_rStaticFile.SetWindowText( sText );
	
	return TRUE;
}

void CChooseImporterDlg::OnOK() 
{
	// find the selected item
	for( int32 i = 0; i < m_rImporterList.GetItemCount(); i++ ) {
		if( m_rImporterList.GetItemState( i, LVIS_SELECTED  ) ) {
			m_i32SelectedImporter = m_rImporterList.GetItemData( i );
			break;
		}
	}

	CDialog::OnOK();
}

void
CChooseImporterDlg::AddImporter( const char* szName, int32 i32Id )
{
	ImporterS	rImp;
	rImp.m_sName = szName;
	rImp.m_i32Id = i32Id;
	m_rImporters.push_back( rImp );
}

uint32
CChooseImporterDlg::GetImporterCount()
{
	return m_rImporters.size();
}

int32
CChooseImporterDlg::GetSelectedImporter()
{
	return m_i32SelectedImporter;
}

int32
CChooseImporterDlg::GetImporter( PajaTypes::int32 i32Idx )
{
	if( i32Idx >= 0 && i32Idx < m_rImporters.size() )
		return m_rImporters[i32Idx].m_i32Id;
	return -1;
}

void CChooseImporterDlg::OnNMDblclkImporterlist(NMHDR *pNMHDR, LRESULT *pResult)
{
	OnOK();
	*pResult = 0;
}
