#if !defined(AFX_FILETYPEINDLG_H__5E608BA6_1C5E_478C_8B79_BE900E1A990A__INCLUDED_)
#define AFX_FILETYPEINDLG_H__5E608BA6_1C5E_478C_8B79_BE900E1A990A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FileTypeInDlg.h : header file
//


#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "ParamI.h"
#include "FileListC.h"
#include "ImportableI.h"
#include "BtnST.h"


/////////////////////////////////////////////////////////////////////////////
// CFileTypeInDlg dialog

class CFileTypeInDlg : public CDialog
{
// Construction
public:
	CFileTypeInDlg(CWnd* pParent = NULL);   // standard constructor

	Composition::ParamFileC*	m_pParam;
	PajaTypes::int32			m_i32Time;
	Import::FileListC*			m_pFileList;


	// Dialog Data
	//{{AFX_DATA(CFileTypeInDlg)
	enum { IDD = IDD_FILE_TYPEIN };
	CEdit	m_rEditOffset;
	CEdit	m_rEditSpeed;
	CButtonST	m_rOk;
	CButtonST	m_rCancel;
	CComboBox	m_rCombo;
	int		m_i32FileNum;
	CString	m_sInfoText;
	float	m_f32Speed;
	int		m_i32TimeOffset;
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFileTypeInDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:

	CSpinnerButton		m_rSpinnerOffset;
	CSpinnerButton		m_rSpinnerSpeed;

	PajaTypes::float32	m_f32Inc;
	PajaTypes::float32	m_f32Scale;

	PajaTypes::float32	m_f32StartLabel;
	PajaTypes::float32	m_f32EndLabel;
	PajaTypes::float32	m_f32RangeLabel;
	PajaTypes::float32	m_f32Duration;

	void AFXAPI		DDX_MyText( CDataExchange* pDX, int nIDC, float& value );

	// Generated message map functions
	//{{AFX_MSG(CFileTypeInDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeCombo();
	afx_msg void OnChangeEditspeed();
	afx_msg void OnChangeEdittimeoffset();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILETYPEINDLG_H__5E608BA6_1C5E_478C_8B79_BE900E1A990A__INCLUDED_)
