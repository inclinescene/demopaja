#if !defined(AFX_VECTOR3TYPEINDLG_H__BB20071A_D724_49B3_A027_DBE85B34C90C__INCLUDED_)
#define AFX_VECTOR3TYPEINDLG_H__BB20071A_D724_49B3_A027_DBE85B34C90C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Vector3TypeInDlg.h : header file
//

#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "Vector3C.h"
#include "ParamI.h"
#include "BtnST.h"

/////////////////////////////////////////////////////////////////////////////
// CVector3TypeInDlg dialog

class CVector3TypeInDlg : public CDialog
{
// Construction
public:
	CVector3TypeInDlg(CWnd* pParent = NULL);   // standard constructor

	PajaTypes::Vector3C			m_rMin, m_rMax;
	bool						m_bClampValues;
	PajaTypes::float32			m_f32Inc;
	Composition::ParamVector3C*	m_pParam;
	PajaTypes::int32			m_i32Time;

	// Dialog Data
	//{{AFX_DATA(CVector3TypeInDlg)
	enum { IDD = IDD_VECTOR3_TYPEIN };
	CButtonST	m_rOk;
	CButtonST	m_rCancel;
	CStatic	m_rStaticLabel2;
	CStatic	m_rStaticLabel3;
	CStatic	m_rStaticLabel1;
	CEdit	m_rEditZ;
	CEdit	m_rEditY;
	CEdit	m_rEditX;
	float	m_f32ValueX;
	float	m_f32ValueY;
	float	m_f32ValueZ;
	CString	m_sInfoText;
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVector3TypeInDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSpinnerButton		m_rSpinnerX;
	CSpinnerButton		m_rSpinnerY;
	CSpinnerButton		m_rSpinnerZ;
	PajaTypes::float32	m_f32Scale;

	void AFXAPI	DDX_MyText( CDataExchange* pDX, int nIDC, float& value );

	// Generated message map functions
	//{{AFX_MSG(CVector3TypeInDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEditx();
	afx_msg void OnChangeEdity();
	afx_msg void OnChangeEditz();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VECTOR3TYPEINDLG_H__BB20071A_D724_49B3_A027_DBE85B34C90C__INCLUDED_)
