#if !defined(AFX_MOVEDLG_H__6360B988_1693_44DB_B2C0_061FD2C6E432__INCLUDED_)
#define AFX_MOVEDLG_H__6360B988_1693_44DB_B2C0_061FD2C6E432__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MoveDlg.h : header file
//

#include "SpinnerButton.h"


/////////////////////////////////////////////////////////////////////////////
// CMoveDlg dialog

class CMoveDlg : public CDialog
{
// Construction
public:
	CMoveDlg( CWnd* pParent = NULL );   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMoveDlg)
	enum { IDD = IDD_TRANS_MOVE };
	CEdit	m_rEditY;
	CEdit	m_rEditX;
	float	m_f32X;
	float	m_f32Y;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMoveDlg)
	public:
	virtual BOOL Create( CWnd* pParentWnd );
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSpinnerButton	m_rSpinnerX;
	CSpinnerButton	m_rSpinnerY;

	// Generated message map functions
	//{{AFX_MSG(CMoveDlg)
	afx_msg void OnApply();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MOVEDLG_H__6360B988_1693_44DB_B2C0_061FD2C6E432__INCLUDED_)
