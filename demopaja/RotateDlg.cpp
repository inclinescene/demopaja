// RotateDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "RotateDlg.h"
#include "demopajadoc.h"
#include "PajaTypes.h"
#include "Vector2C.h"
#include "BBox2C.h"
#include <math.h>
#include "SceneC.h"
#include "LayerC.h"
#include "EffectI.h"
#include "ParamI.h"
#include "UndoC.h"


using namespace Composition;
using namespace PajaTypes;
using namespace Edit;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRotateDlg dialog


CRotateDlg::CRotateDlg( CWnd* pParent /*=NULL*/ ) :
	CDialog(CRotateDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRotateDlg)
	m_f32Angle = 0.0f;
	//}}AFX_DATA_INIT
}


void CRotateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRotateDlg)
	DDX_Control(pDX, IDC_EDITANGLE, m_rEditAngle);
	DDX_Text(pDX, IDC_EDITANGLE, m_f32Angle);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRotateDlg, CDialog)
	//{{AFX_MSG_MAP(CRotateDlg)
	ON_BN_CLICKED(IDC_APPLY, OnApply)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRotateDlg message handlers

void CRotateDlg::OnApply() 
{
	// get latest data
	UpdateData( TRUE );

	CDemopajaDoc*			pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	SceneC*		pScene = pDoc->GetCurrentScene();
	int32		i32Time = pDoc->GetTimecursor() * pDoc->GetFrameSizeInTicks();

	UndoC*	pUndo = new UndoC( "Transform Rotate" );
	UndoC*	pOldUndo;

	int32	i32SelCount = 0;

	for( uint32 i = 0; i < pScene->get_layer_count(); i++ ) {

		LayerC*	pLayer = pScene->get_layer( i );

		if( !pLayer || !(pLayer->get_flags() & ITEM_VISIBLE) || (pLayer->get_flags() & ITEM_LOCKED) ||
			!pLayer->get_timesegment()->is_visible( i32Time ) )
			continue;

		int32	i32TimeOffset = pLayer->get_timesegment()->get_segment_start();

		for( uint32 j = 0; j < pLayer->get_effect_count(); j++ ) {
			EffectI*	pEffect = pLayer->get_effect( j );

			if( !pEffect || !(pEffect->get_flags() & ITEM_VISIBLE) || (pEffect->get_flags() & ITEM_LOCKED) ||
				!pEffect->get_timesegment()->is_visible( i32Time - i32TimeOffset ) )
				continue;

			if( pEffect->get_flags() & ITEM_SELECTED ) {
				int32	i32ParamTime = i32Time - (i32TimeOffset + pEffect->get_timesegment()->get_segment_start());

				ParamFloatC*	pParamRot = (ParamFloatC*)pEffect->get_default_param( DEFAULT_PARAM_ROTATION );

				if( pParamRot ) {
					float32	f32Angle;
					pOldUndo = pParamRot->begin_editing( pUndo );
					pParamRot->get_val( i32ParamTime, f32Angle );
					f32Angle += m_f32Angle;
					pDoc->HandleParamNotify( pParamRot->set_val( i32ParamTime, f32Angle ) );	// create key at the timecursor time.
					pParamRot->end_editing( pOldUndo );
				}
				i32SelCount++;
			}
		}
	}
	
	if( i32SelCount ) {
		pDoc->GetUndoManager()->push( pUndo );
		pDoc->SetModifiedFlag();
	}
	else
		delete pUndo;

	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}

BOOL CRotateDlg::Create( CWnd* pParentWnd ) 
{
	return CDialog::Create(IDD, pParentWnd);
}

BOOL CRotateDlg::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN ) {
		if( (int)pMsg->wParam == VK_RETURN ) {
			OnApply();
			return TRUE; 
		}
		else if( (int)pMsg->wParam == VK_ESCAPE )
			return TRUE;
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CRotateDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if( !m_rSpinner.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
		return FALSE;
	m_rSpinner.SetScale( 1 );
	m_rSpinner.SetBuddy( &m_rEditAngle, SPNB_ATTACH_RIGHT );
	
	return TRUE;
}
