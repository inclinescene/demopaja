#if !defined(AFX_FILEDIALOGEX_H__110BD351_3FFE_4791_B825_15E37B88994D__INCLUDED_)
#define AFX_FILEDIALOGEX_H__110BD351_3FFE_4791_B825_15E37B88994D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FileDialogEx.h : header file
//

// forw decl
class CFileDialogEx;

#include "FileSubClassWnd.h"

/////////////////////////////////////////////////////////////////////////////
// CFileDialogEx dialog

class CFileDialogEx : public CFileDialog
{
	DECLARE_DYNAMIC(CFileDialogEx)

public:
	CFileDialogEx(BOOL bOpenFileDialog, // TRUE for FileOpen, FALSE for FileSaveAs
		BOOL bAddButton = FALSE,
		LPCTSTR lpszDefExt = NULL,
		LPCTSTR lpszFileName = NULL,
		DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		LPCTSTR lpszFilter = NULL,
		CWnd* pParentWnd = NULL);


	void	OnAddButton();

protected:

	CButton				m_rIncButton;
	CFileSubClassWnd	m_SubWnd;
	BOOL				m_bAddButton;

	//{{AFX_MSG(CFileDialogEx)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILEDIALOGEX_H__110BD351_3FFE_4791_B825_15E37B88994D__INCLUDED_)
