#if !defined(AFX_PREFSUBDLG_H__65C27F5A_4D53_46C2_AC6E_A60883454F26__INCLUDED_)
#define AFX_PREFSUBDLG_H__65C27F5A_4D53_46C2_AC6E_A60883454F26__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PrefSubDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CPrefSubDlg dialog

class CPrefSubDlg : public CDialog
{
// Construction
public:

	DECLARE_DYNCREATE( CPrefSubDlg )

	CPrefSubDlg();
	CPrefSubDlg( UINT iID, CWnd* pParent = NULL );
	~CPrefSubDlg();

	UINT			GetID();
	virtual BOOL	PreTranslateMessage( MSG* pMsg );
	virtual void	OnOK();
	virtual void	OnCancel();


	DECLARE_MESSAGE_MAP()

protected:
	UINT	m_iID;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PREFSUBDLG_H__65C27F5A_4D53_46C2_AC6E_A60883454F26__INCLUDED_)
