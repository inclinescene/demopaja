#if !defined(AFX_MUSICSTARTTIMEDLG_H__F87EBB2D_B93E_404C_B9D2_D928C80E80C9__INCLUDED_)
#define AFX_MUSICSTARTTIMEDLG_H__F87EBB2D_B93E_404C_B9D2_D928C80E80C9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MusicStartTimeDlg.h : header file
//

#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "BtnST.h"


/////////////////////////////////////////////////////////////////////////////
// CMusicStartTimeDlg dialog

class CMusicStartTimeDlg : public CDialog
{
// Construction
public:
	CMusicStartTimeDlg(CWnd* pParent = NULL);   // standard constructor

	PajaTypes::int32			m_i32BeatsPerMin;
	PajaTypes::int32			m_i32QNotesPerBeat;
	PajaTypes::int32			m_i32EditAccuracy;

	void		AddMarker( const char* szName, PajaTypes::int32 i32Time );

// Dialog Data
	//{{AFX_DATA(CMusicStartTimeDlg)
	enum { IDD = IDD_MUSICSTARTTIMEDLG };
	CListCtrl	m_rListMarkers;
		CButtonST	m_rOk;
		CButtonST	m_rCancel;
		CStatic		m_rStaticStartTime;
		CEdit		m_rEditStartTime;
		int			m_i32StartTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMusicStartTimeDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	struct MarkerS {
		CString				sName;
		PajaTypes::int32	i32Time;
	};
	std::vector<MarkerS>	m_rMarkers;

	CSpinnerButton		m_rSpinnerStart;

	CString			FormatTime( PajaTypes::int32 i32Time );
	void AFXAPI		DDX_MyText( CDataExchange* pDX, int nIDC, int& value );

	// Generated message map functions
	//{{AFX_MSG(CMusicStartTimeDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEditstarttime();
	afx_msg void OnClickListmarkers(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MUSICSTARTTIMEDLG_H__F87EBB2D_B93E_404C_B9D2_D928C80E80C9__INCLUDED_)
