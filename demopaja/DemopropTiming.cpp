// DemopropTiming.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "DemopropTiming.h"
#include "PajaTypes.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


using namespace PajaTypes;


/////////////////////////////////////////////////////////////////////////////
// CDemopropTiming dialog


CDemopropTiming::CDemopropTiming(CWnd* pParent /*=NULL*/)
	: CPrefSubDlg(CDemopropTiming::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDemopropTiming)
	m_i32BeatSize = -1;
	m_i32MeasureSize = -1;
	m_sDuration = _T("");
	m_i32BPM = 0;
	m_i32Accuracy = -1;
	//}}AFX_DATA_INIT
}


void CDemopropTiming::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDemopropTiming)
	DDX_Control(pDX, IDC_FPS, m_rStaticFPS);
	DDX_CBIndex(pDX, IDC_BEAT_SIZE, m_i32BeatSize);
	DDX_CBIndex(pDX, IDC_MEASURE_SIZE, m_i32MeasureSize);
	DDX_Text(pDX, IDC_EDIT_DURATION, m_sDuration);
	DDX_Text(pDX, IDC_EDIT_BPM, m_i32BPM);
	DDX_CBIndex(pDX, IDC_COMBO_ACCURACY, m_i32Accuracy);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDemopropTiming, CPrefSubDlg)
	//{{AFX_MSG_MAP(CDemopropTiming)
	ON_EN_CHANGE(IDC_EDIT_BPM, OnChangeEditBpm)
	ON_CBN_SELCHANGE(IDC_BEAT_SIZE, OnSelchangeBeatSize)
	ON_CBN_SELCHANGE(IDC_COMBO_ACCURACY, OnSelchangeComboAccuracy)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDemopropTiming message handlers

void CDemopropTiming::UpdateFPS()
{
	UpdateData( TRUE );


	int32	i32BPM = m_i32BPM;
	int32	i32QNotesPerBeat = m_i32BeatSize + 1;
	int32	i32EditAcc = 0;

	switch( m_i32Accuracy ) {
	case 0: i32EditAcc = 1; break;
	case 1: i32EditAcc = 2; break;
	case 2: i32EditAcc = 4; break;
	case 3: i32EditAcc = 8; break;
	case 4: i32EditAcc = 16; break;
	case 5: i32EditAcc = 32; break;
	case 6: i32EditAcc = 64; break;
	}


	float64	f64FPS = (float64)(i32BPM * i32QNotesPerBeat * i32EditAcc) / 60.0;

	CString	sVal;

	sVal.Format( "%.2f FPS", f64FPS );

	m_rStaticFPS.SetWindowText( sVal );
}

void CDemopropTiming::OnChangeEditBpm() 
{
	UpdateFPS();
}

void CDemopropTiming::OnSelchangeBeatSize() 
{
	UpdateFPS();
}

void CDemopropTiming::OnSelchangeComboAccuracy() 
{
	UpdateFPS();
}

BOOL CDemopropTiming::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	UpdateFPS();
		
	return TRUE;
}
