// MarkerTypeInDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "MarkerTypeInDlg.h"
#include "PajaTypes.h"
#include "SpinnerButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


using namespace PajaTypes;


static
COLORREF
Mix( COLORREF rCol1, COLORREF rCol2, int iAlpha )
{
	int	iR, iG, iB;

	iR = ((GetRValue( rCol1 ) * iAlpha) + (GetRValue( rCol2) * (255 - iAlpha))) / 255;
	iG = ((GetGValue( rCol1 ) * iAlpha) + (GetGValue( rCol2) * (255 - iAlpha))) / 255;
	iB = ((GetBValue( rCol1 ) * iAlpha) + (GetBValue( rCol2) * (255 - iAlpha))) / 255;

	return	RGB( iR, iG, iB );
}



/////////////////////////////////////////////////////////////////////////////
// CMarkerTypeInDlg dialog


CMarkerTypeInDlg::CMarkerTypeInDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMarkerTypeInDlg::IDD, pParent)
	, m_i32Color(0)
{
	//{{AFX_DATA_INIT(CMarkerTypeInDlg)
	m_sName = _T("");
	m_i32Time = 0;
	//}}AFX_DATA_INIT
}


void CMarkerTypeInDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMarkerTypeInDlg)
	DDX_Control(pDX, IDOK, m_rOk);
	DDX_Control(pDX, IDCANCEL, m_rCancel);
	DDX_Control(pDX, IDC_TIME, m_rEditTime);
	DDX_Control(pDX, IDC_TIME_INFO, m_rStaticTimeInfo);
	DDX_Text(pDX, IDC_NAME, m_sName);
	DDX_Text(pDX, IDC_TIME, m_i32Time);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_COMBOCOLOR, m_rComboColor);
	DDX_CBIndex(pDX, IDC_COMBOCOLOR, m_i32Color);
}


BEGIN_MESSAGE_MAP(CMarkerTypeInDlg, CDialog)
	//{{AFX_MSG_MAP(CMarkerTypeInDlg)
	ON_EN_CHANGE(IDC_TIME, OnChangeTime)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMarkerTypeInDlg message handlers

void CMarkerTypeInDlg::OnChangeTime() 
{
	UpdateData( TRUE );
	m_rStaticTimeInfo.SetWindowText( FormatTime( m_i32Time * (256 / m_i32EditAccuracy) ) );
}

CString CMarkerTypeInDlg::FormatTime( int32 i32Time )
{
	CString	sDuration;
	int32	i32Hours, i32Mins, i32Secs, i32Frames;
	int32	i32TimeScale = m_i32BeatsPerMin * m_i32QNotesPerBeat * 256;

	i32Frames = (i32Time * 60 * 60 / i32TimeScale); // / (256 / m_i32EditAccuracy);
	i32Secs = (i32Time * 60 / i32TimeScale) % 60;
	i32Mins = (i32Time / i32TimeScale) % 60;
	i32Hours = i32Time / 60 / i32TimeScale;
	i32Frames = (i32Time - ((i32Hours * 3600 + i32Mins * 60 + i32Secs) * i32TimeScale) / 60) / (256 / m_i32EditAccuracy);

	sDuration.Format( "%d:%02d:%02d:%02d", i32Hours, i32Mins, i32Secs, i32Frames );

	return sDuration;
}

BOOL CMarkerTypeInDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	COLORREF	rMarkerColors[9] = {
		0,
		RGB( 255, 255, 255 ),
		RGB( 255, 0,   0 ),
		RGB( 255, 255, 0 ),
		RGB( 0,   255, 0 ),
		RGB( 0,   255, 255 ),
		RGB( 0,   0,   255 ),
		RGB( 255, 0,   255 ),
		RGB( 0,   0,   0 ),
	};
	COLORREF	rBaseGrey = ::GetSysColor( COLOR_BTNFACE );
	rMarkerColors[0] = rBaseGrey;

	char	szNames[9][32] = {
		"<None>",
		"White",
		"Red",
		"Yellow",
		"Green",
		"Cyan",
		"Blue",
		"Magenta",
		"Black",
	};
	
	for( int i = 0; i < 9; i++ )
		m_rComboColor.AddColor( szNames[i], Mix( rMarkerColors[i], rBaseGrey, 64 ) );

	m_rComboColor.SetCurSel( m_i32Color );

	// buttons
	if( !m_rSpinnerTime.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYINT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
		return FALSE;
	
	m_rSpinnerTime.SetScale( 1 );
	m_rSpinnerTime.SetBuddy( &m_rEditTime, SPNB_ATTACH_RIGHT );

	m_rStaticTimeInfo.SetWindowText( FormatTime( m_i32Time * (256 / m_i32EditAccuracy) ) );
	
	m_rOk.SetFlat( FALSE );
	m_rCancel.SetFlat( FALSE );

	return TRUE;
}
