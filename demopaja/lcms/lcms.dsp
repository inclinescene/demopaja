# Microsoft Developer Studio Project File - Name="lcms" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=lcms - Win32 MFC Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "lcms.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "lcms.mak" CFG="lcms - Win32 MFC Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "lcms - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "lcms - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "lcms - Win32 MFC Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "lcms - Win32 MFC Release" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "lcms - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /I "F:\Code and Docs\Libraries\lcms108a\include" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x40b /d "NDEBUG"
# ADD RSC /l 0x40b /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "lcms - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ  /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I "F:\Code and Docs\Libraries\lcms108a\include" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ  /c
# ADD BASE RSC /l 0x40b /d "_DEBUG"
# ADD RSC /l 0x40b /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "lcms - Win32 MFC Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "MFC Debug"
# PROP BASE Intermediate_Dir "MFC Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "MFC_Debug"
# PROP Intermediate_Dir "MFC_Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /I "F:\Code and Docs\Libraries\lcms108a\include" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ  /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "F:\Code and Docs\Libraries\lcms108a\include" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ  /c
# ADD BASE RSC /l 0x40b /d "_DEBUG"
# ADD RSC /l 0x40b /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "lcms - Win32 MFC Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "MFC Release"
# PROP BASE Intermediate_Dir "MFC Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "MFC_Release"
# PROP Intermediate_Dir "MFC_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /I "F:\Code and Docs\Libraries\lcms108a\include" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "F:\Code and Docs\Libraries\lcms108a\include" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD BASE RSC /l 0x40b /d "NDEBUG"
# ADD RSC /l 0x40b /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "lcms - Win32 Release"
# Name "lcms - Win32 Debug"
# Name "lcms - Win32 MFC Debug"
# Name "lcms - Win32 MFC Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmscam97.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmscnvrt.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmserr.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmsgamma.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmsgmt.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmsintrp.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmsio1.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmslut.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmsmatsh.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmsmtrx.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmspack.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmspcs.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmssamp.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmswtpnt.c"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\src\cmsxform.c"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\include\icc34.h"
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\lcms108a\include\lcms.h"
# End Source File
# End Group
# End Target
# End Project
