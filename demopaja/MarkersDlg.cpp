// MarkersDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "MarkersDlg.h"
#include "flatpopupmenu.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMarkersDlg dialog


CMarkersDlg::CMarkersDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMarkersDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMarkersDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CMarkersDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMarkersDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMarkersDlg, CDialog)
	//{{AFX_MSG_MAP(CMarkersDlg)
	ON_NOTIFY(NM_RCLICK, IDC_MARKERLIST, OnRclickMarkerlist)
	ON_NOTIFY(NM_CLICK, IDC_MARKERLIST, OnClickMarkerlist)
	ON_NOTIFY(NM_DBLCLK, IDC_MARKERLIST, OnDblclkMarkerlist)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMarkersDlg message handlers

void CMarkersDlg::OnRclickMarkerlist( NMHDR* pNMHDR, LRESULT* pResult ) 
{


	*pResult = 0;
}

void CMarkersDlg::OnClickMarkerlist(NMHDR* pNMHDR, LRESULT* pResult) 
{
	
	*pResult = 0;
}

void CMarkersDlg::OnDblclkMarkerlist(NMHDR* pNMHDR, LRESULT* pResult) 
{
	
	*pResult = 0;
}
