#ifndef __DX9GUIDRAWINTERFACEC_H__
#define __DX9GUIDRAWINTERFACEC_H__

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

// forw declaration
namespace PajaSystem {
	class DX9GUIDrawInterfaceC;
};

#include "PajaTypes.h"
#include "BBox2C.h"
#include "ColorC.h"
#include "Vector2C.h"
#include "DeviceInterfaceI.h"
#include "GUIDrawInterfaceI.h"
#include "DX9ViewportC.h"
#include "DX9DeviceC.h"

#include "d3dfont.h"

namespace PajaSystem {

	//! The ID of the OpenGL interface, use this ID to request the interface.
	const PluginClass::ClassIdC	CLASS_DX9_GUIINTERFACE = PluginClass::ClassIdC( 0, 30002 );


	class DX9GUIDrawInterfaceC : public GUIDrawInterfaceI
	{
	public:

		virtual Edit::DataBlockI*				create();
		static DX9GUIDrawInterfaceC*			create_new( DX9DeviceC* pDevice );
		virtual PluginClass::ClassIdC			get_class_id() const;
		virtual const char*						get_class_name();

		// draws the layout
		virtual void	draw_layout( const PajaTypes::ColorC& rLayout );

		virtual void	begin_layout();
		virtual void	end_layout();

		// Uses specified font.
		virtual void	use_font( HFONT hFont );
		// Draws text using current font.
		virtual void	draw_text( const PajaTypes::Vector2C& rPos, const char* szText );

		// Sets current draw color.
		virtual void	set_color( const PajaTypes::ColorC& rColor );
		// Sets current point size.
		virtual void	set_point_size( PajaTypes::float32 f32Size );
		// Draws a line.
		virtual void	draw_line( const PajaTypes::Vector2C& rFrom, const PajaTypes::Vector2C& rTo );
		// Draws a line.
		virtual void	draw_point( const PajaTypes::Vector2C& rPos );
		// Draws grid to based on latest draw layout call and grid size.
		virtual void	draw_grid( PajaTypes::float32 f32Width, PajaTypes::float32 f32Height, PajaTypes::float32 f32GridSize );
		// Draws selection box.
		virtual void	draw_selection_box( const PajaTypes::Vector2C& rMin, const PajaTypes::Vector2C& rMax );
		// Draw a box.
		virtual void	draw_box( const PajaTypes::Vector2C& rMin, const PajaTypes::Vector2C& rMax );
		// Draws a marker.
		virtual void	draw_marker( const PajaTypes::Vector2C& rPos, PajaTypes::float32 f32Size );

		void			init_invalidate();
		void			init_validate();

	protected:
		DX9GUIDrawInterfaceC();
		DX9GUIDrawInterfaceC( DX9DeviceC* pDevice );
		virtual ~DX9GUIDrawInterfaceC();

	private:

		struct XYZCOLOR_VERTEX
		{
			float	x, y, z;
			DWORD	color;
		};

		struct XYZWCOLOR_VERTEX
		{
			float	x, y, z, w;
			DWORD	color;
		};

		XYZCOLOR_VERTEX*	m_pVertBuffer;
		DWORD							m_nVerts;

		DX9DeviceC*		m_pDevice;
		CD3DFont*		m_pFont;
		DWORD			m_dwCurColor;
		PajaTypes::float32	m_f32PointSize;
	};

};	// namespace

#endif