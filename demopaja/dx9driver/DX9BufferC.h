//-------------------------------------------------------------------------
//
// File:		DX9BufferC.h
// Desc:		DX9 off-screen graphics rendering interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://moppi.inside.org/demopaja/
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_DX9BUFFERC_H__
#define __DEMOPAJA_DX9BUFFERC_H__

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <d3d9.h>

// Forward declaration
namespace PajaSystem {
	class DX9BufferC;
};

#include "PajaTypes.h"
#include "DeviceInterfaceI.h"
#include "DataBlockI.h"
#include "ClassIdC.h"
#include "GraphicsDeviceI.h"
#include "DX9ViewportC.h"
#include "DX9DeviceC.h"

namespace PajaSystem {


	//! The ID of the OpenGL buffer interface.
	const PluginClass::ClassIdC	CLASS_DX9_BUFFER = PluginClass::ClassIdC( 0, 30003 );


	//! OpenGL graphics buffer class.
	/*!	To get the dimensions of the graphics buffer, query the GraphicsViewport class from it.
	*/
	class DX9BufferC : public GraphicsBufferI
	{
	public:
		//! Create new graphics device.
		virtual Edit::DataBlockI*				create();
		static DX9BufferC*					create_new( GraphicsDeviceI* pDevice );
		virtual PluginClass::ClassIdC			get_class_id() const;
		virtual const char*						get_class_name();
		virtual DeviceInterfaceI*				query_interface( const PluginClass::SuperClassIdC& rSuperClassId );

		//! Sets the owner of the buffer.
		virtual void							set_graphicsdevice( GraphicsDeviceI* pDevice );

		//! Intialises a created graphics buffer.
		/*!	If init() is called for already created buffer, the old contents is
			deleted and new buffer is initialised.
		*/
		virtual bool							init( PajaTypes::uint32 ui32Flags,
													  PajaTypes::uint32 ui32Width = 0,
													  PajaTypes::uint32 ui32Height = 0 );
		//! Returns initialisation flags.
		virtual PajaTypes::uint32		get_flags();
		//! Starts drawing block.
		virtual void							begin_draw();
		//! Ends drawing block.
		virtual void							end_draw();
		//! Flushes the rendering buffer.
		virtual void							flush();

		//! Returns the texture coordinates of the buffer.
		virtual PajaTypes::BBox2C&	get_tex_coord_bounds();

		//! Uses the graphics buffer as a texture in specified device.
		virtual void							bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Stage, PajaTypes::uint32 ui32Properties );

		//! Get contents of the graphics buffer.
		virtual void							read_pixels( PajaTypes::uint32 ui32Flags, void* pData );

		virtual void							activate();

		virtual void							init_invalidate();
		virtual void							init_validate();

		virtual LPDIRECT3DTEXTURE9	get_texture();
		virtual LPDIRECT3DTEXTURE9	get_extra_texture();
		virtual DWORD							get_clear_flag_mask();

	protected:
		DX9BufferC();
		DX9BufferC( GraphicsDeviceI* pDevice );
		virtual ~DX9BufferC();

		DX9DeviceC*					m_pParentDev;
		DX9ViewportC*				m_pViewport;
		PajaTypes::uint32		m_ui32InitFlags;
		PajaTypes::BBox2C		m_rTexBounds;

		LPDIRECT3DDEVICE9			m_pD3DDevice;
		LPDIRECT3DTEXTURE9		m_pBufferTex;
		LPDIRECT3DSURFACE9		m_pBufferSurf;
		LPDIRECT3DSURFACE9		m_pBufferZ;
		LPDIRECT3DTEXTURE9		m_pBufferExtraTex;
		LPDIRECT3DSURFACE9		m_pBufferExtraSurf;

		DWORD									m_dwClearFlagMask;
		DWORD									m_dwLastBindStage;

    LPDIRECT3DSTATEBLOCK9 m_pStateBlockSaved;
	};

};	// namespace

#endif // __DEMOPAJA_DX9BUFFERC_H__
