#ifndef __DX9DRIVER_H__
#define __DX9DRIVER_H__

#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "ClassDescC.h"
#include "EditableI.h"
#include "GraphicsDeviceI.h"
#include "GraphicsViewportI.h"
#include "TimeContextC.h"
#include "DX9DeviceC.h"


//////////////////////////////////////////////////////////////////////////
//
//  DirectX 9 Device Driver class descriptor.
//

class DX9DeviceDescC : public PluginClass::ClassDescC
{
public:
	DX9DeviceDescC();
	virtual ~DX9DeviceDescC();
	virtual void*							create();
	virtual PajaTypes::int32				get_classtype() const;
	virtual PluginClass::SuperClassIdC		get_super_class_id() const;
	virtual PluginClass::ClassIdC			get_class_id() const;
	virtual const char*						get_name() const;
	virtual const char*						get_desc() const;
	virtual const char*						get_author_name() const;
	virtual const char*						get_copyright_message() const;
	virtual const char*						get_url() const;
	virtual const char*						get_help_filename() const;
	virtual PajaTypes::uint32				get_ext_count() const;
	virtual const char*						get_ext( PajaTypes::uint32 ui32Index ) const;
	virtual PajaTypes::uint32				get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
};


namespace DX9DeviceDriver {
};	// namespace


// The global descriptors.
extern DX9DeviceDescC		g_rDX9DeviceDesc;

#endif // __OPENGLDRIVER_H__