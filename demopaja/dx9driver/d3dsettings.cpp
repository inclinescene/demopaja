//-----------------------------------------------------------------------------
// File: D3DSettings.cpp
//
// Desc: Settings class and change-settings dialog class for the Direct3D 
//       samples framework library.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
//#include "dxstdafx.h"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <D3DX9.h>

#include "D3DEnumeration.h"
#include "D3DSettings.h"

#include "arraylist.h"


