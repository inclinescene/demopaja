#pragma once
#include "afxwin.h"
#include "BtnST.h"
#include "PajaTypes.h"
#include "LayerC.h"
#include "EffectI.h"
#include "ParamI.h"

// CSingleLinkTypeIn dialog

class CSingleLinkTypeIn : public CDialog
{
	DECLARE_DYNAMIC(CSingleLinkTypeIn)

public:
	CSingleLinkTypeIn(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSingleLinkTypeIn();

	Composition::LayerC*		m_pLayer;
	Composition::EffectI*	m_pEffect;
	Composition::ParamLinkC*	m_pParam;
	

// Dialog Data
	enum { IDD = IDD_SINGLE_LINK_TYPEIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	CString m_sInfoText;
	CComboBox m_rCombo;
	CButtonST m_rOk;
	CButtonST m_rCancel;
	virtual BOOL OnInitDialog();
	int m_i32EffectNum;
	afx_msg void OnCbnSelchangeCombo();
};
