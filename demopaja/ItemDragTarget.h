#if !defined(AFX_ITEMDRAGTARGET_H__D46453EF_BECB_4949_8E7F_1A122E9183EC__INCLUDED_)
#define AFX_ITEMDRAGTARGET_H__D46453EF_BECB_4949_8E7F_1A122E9183EC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ItemDragTarget.h : header file
//

#include "afxole.h"
#include <vector>

/////////////////////////////////////////////////////////////////////////////
// CItemDragTarget command target

class CItemDragTarget : public COleDropTarget
{
//	DECLARE_DYNCREATE(CItemDragTarget)

public:
	CItemDragTarget();           // protected constructor used by dynamic creation
	virtual ~CItemDragTarget();

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CItemDragTarget)
	public:
	virtual DROPEFFECT OnDragOver(CWnd* pWnd, COleDataObject* pDataObject, DWORD dwKeyState, CPoint point);
	virtual BOOL OnDrop(CWnd* pWnd, COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point);
	//}}AFX_VIRTUAL

// Implementation
protected:

//	std::vector<HWND>	m_rActiveDockBars;
//	HWND				m_hWndParent;

	// Generated message map functions
	//{{AFX_MSG(CItemDragTarget)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ITEMDRAGTARGET_H__D46453EF_BECB_4949_8E7F_1A122E9183EC__INCLUDED_)
