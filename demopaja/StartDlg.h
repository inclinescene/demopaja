#if !defined(AFX_STARTDLG_H__4BA7F5C8_4BC5_4D74_BD04_D67CF7863AC8__INCLUDED_)
#define AFX_STARTDLG_H__4BA7F5C8_4BC5_4D74_BD04_D67CF7863AC8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// StartDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CStartDlg dialog

class CStartDlg : public CDialog
{
// Construction
public:
	CStartDlg(CWnd* pParent = NULL);   // standard constructor

	void	SetText( const char* szStr );

// Dialog Data
	//{{AFX_DATA(CStartDlg)
	enum { IDD = IDD_START_DIALOG };
	CStatic	m_rStaticText;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CStartDlg)
	public:
	virtual BOOL Create( CWnd* pParentWnd = NULL );
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CStartDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STARTDLG_H__4BA7F5C8_4BC5_4D74_BD04_D67CF7863AC8__INCLUDED_)
