//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Demopaja Plugin Wizard.rc
//
#define IDI_DEMOPAJAPLUGINWIZARD        1
#define IDD_CUSTOM1                     129
#define IDD_CUSTOM2                     130
#define IDD_CUSTOM3                     131
#define IDC_AUTHORNAME                  1000
#define IDC_COPYRIGHT                   1001
#define IDC_URL                         1002
#define IDC_NAME                        1003
#define IDC_DESC                        1004
#define IDC_TYPE                        1008

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        126
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
