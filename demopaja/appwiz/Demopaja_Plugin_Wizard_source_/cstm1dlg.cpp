// cstm1dlg.cpp : implementation file
//

#include "stdafx.h"
#include "Demopaja Plugin Wizard.h"
#include "cstm1dlg.h"
#include "Demopaja Plugin Wizardaw.h"

#ifdef _PSEUDO_DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCustom1Dlg dialog


CCustom1Dlg::CCustom1Dlg()
	: CAppWizStepDlg(CCustom1Dlg::IDD)
{
	//{{AFX_DATA_INIT(CCustom1Dlg)
    m_sAuthor = _T("");
	m_sCopyright = _T("");
	m_sURL = _T("");
    m_sName = _T("");
	m_sDesc = _T("");
    //}}AFX_DATA_INIT

    // Try restoring the user info from the registry.
    LoadSettings();
}


void CCustom1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CAppWizStepDlg::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCustom1Dlg)
	DDX_Text(pDX, IDC_AUTHORNAME, m_sAuthor);
	DDX_Text(pDX, IDC_COPYRIGHT, m_sCopyright);
	DDX_Text(pDX, IDC_URL, m_sURL);

	DDX_Control(pDX, IDC_TYPE, m_cbType);
	DDX_Text(pDX, IDC_NAME, m_sName);
	DDX_Text(pDX, IDC_DESC, m_sDesc);
    //}}AFX_DATA_MAP
}

// This is called whenever the user presses Next, Back, or Finish with this step
//  present.  Do all validation & data exchange from the dialog in this function.
BOOL CCustom1Dlg::OnDismiss()
{
	if (!UpdateData(TRUE))
		return FALSE;

    // Gotta have a valid information.
    if( m_sName.GetLength() == 0 )
    {
        MessageBox("Please enter a plugin name.", "Error");
        return FALSE;
    }

    if( m_sDesc.GetLength() == 0 )
    {
        MessageBox("Please enter a plugin description.", "Error");
        return FALSE;
    }

    DemopajaPluginWizardaw.m_Dictionary["AUTHOR_NAME"] = m_sAuthor;
    DemopajaPluginWizardaw.m_Dictionary["AUTHOR_COPYRIGHT"] = m_sCopyright;
    DemopajaPluginWizardaw.m_Dictionary["AUTHOR_URL"] = m_sURL;

    DemopajaPluginWizardaw.m_Dictionary["NAME"] = m_sName;
    DemopajaPluginWizardaw.m_Dictionary["DESC"] = m_sDesc;

    {
        GUID rGuid;
        CString sClassID;
        CoCreateGuid( &rGuid );
        sClassID.Format( "0x%X, 0x%X", rGuid.Data1, (rGuid.Data2 << 16) | rGuid.Data3 );
        DemopajaPluginWizardaw.m_Dictionary["CLASS_IDCODE"] = sClassID;
    }

    // Persist the current settings entered by the user.
    SaveSettings();

	return TRUE;	// return FALSE if the dialog shouldn't be dismissed
}


BEGIN_MESSAGE_MAP(CCustom1Dlg, CAppWizStepDlg)
	//{{AFX_MSG_MAP(CCustom1Dlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
#define APPWIZ_REGISTRY_SUBKEY "Software\\OTri.NET\\demopajaAppWiz"

void CCustom1Dlg::LoadSettings()
{
    HKEY key;
    if( RegOpenKeyEx( HKEY_CURRENT_USER, APPWIZ_REGISTRY_SUBKEY, 0, KEY_READ, &key) != ERROR_SUCCESS )
    { // Couldn't open the registry key, so it probably doesn't exist.
        return;
    }

    // Use the horrible MS interface to retrieve data out of the registry.
    DWORD nData;
    if( RegQueryValueEx( key, "Author", 0, NULL, 0, &nData ) == ERROR_SUCCESS )
    {
        RegQueryValueEx( key, "Author", 0, NULL, (BYTE*)m_sAuthor.GetBufferSetLength( nData ), &nData );
        m_sAuthor.ReleaseBuffer();
    }

    if( RegQueryValueEx( key, "Copyright", 0, NULL, 0, &nData ) == ERROR_SUCCESS )
    {
        RegQueryValueEx( key, "Copyright", 0, NULL, (BYTE*)m_sCopyright.GetBufferSetLength( nData ), &nData );
        m_sCopyright.ReleaseBuffer();
    }

    if( RegQueryValueEx( key, "URL", 0, NULL, 0, &nData ) == ERROR_SUCCESS )
    {
        RegQueryValueEx( key, "URL", 0, NULL, (BYTE*)m_sURL.GetBufferSetLength( nData ), &nData );
        m_sURL.ReleaseBuffer();
    }


    RegCloseKey( key );
}


void CCustom1Dlg::SaveSettings()
{
    HKEY key;
    if( RegCreateKeyEx( HKEY_CURRENT_USER, APPWIZ_REGISTRY_SUBKEY, 0,
                        "", REG_OPTION_NON_VOLATILE, KEY_WRITE,
                        NULL, &key, NULL) != ERROR_SUCCESS )
    { // Couldn't open the registry key, bummer. This means we can't save our info.
        return;
    }

    // Save our required info.
    RegSetValueEx( key, "Author", 0, REG_SZ, (const BYTE*)m_sAuthor.operator LPCTSTR() , m_sAuthor.GetLength()+1 );
    RegSetValueEx( key, "Copyright", 0, REG_SZ, (const BYTE*)m_sCopyright.operator LPCTSTR(), m_sCopyright.GetLength()+1 );
    RegSetValueEx( key, "URL", 0, REG_SZ, (const BYTE*)m_sURL.operator LPCTSTR(), m_sURL.GetLength()+1 );

    RegCloseKey( key );
}

