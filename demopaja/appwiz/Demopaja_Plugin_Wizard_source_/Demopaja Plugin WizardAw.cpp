// Demopaja Plugin Wizardaw.cpp : implementation file
//

#include "stdafx.h"
#include "Demopaja Plugin Wizard.h"
#include "Demopaja Plugin Wizardaw.h"
#include "chooser.h"

#ifdef _PSEUDO_DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

// This is called immediately after the custom AppWizard is loaded.  Initialize
//  the state of the custom AppWizard here.
void CDemopajaPluginWizardAppWiz::InitCustomAppWiz()
{
	// Create a new dialog chooser; CDialogChooser's constructor initializes
	//  its internal array with pointers to the steps.
	m_pChooser = new CDialogChooser;

	// Set the maximum number of steps.
	SetNumberOfSteps(LAST_DLG);
}

// This is called just before the custom AppWizard is unloaded.
void CDemopajaPluginWizardAppWiz::ExitCustomAppWiz()
{
	// Deallocate memory used for the dialog chooser
	ASSERT(m_pChooser != NULL);
	delete m_pChooser;
	m_pChooser = NULL;
}

// This is called when the user clicks "Create..." on the New Project dialog
//  or "Next" on one of the custom AppWizard's steps.
CAppWizStepDlg* CDemopajaPluginWizardAppWiz::Next(CAppWizStepDlg* pDlg)
{
	// Delegate to the dialog chooser
	return m_pChooser->Next(pDlg);
}

// This is called when the user clicks "Back" on one of the custom
//  AppWizard's steps.
CAppWizStepDlg* CDemopajaPluginWizardAppWiz::Back(CAppWizStepDlg* pDlg)
{
	// Delegate to the dialog chooser
	return m_pChooser->Back(pDlg);
}

void CDemopajaPluginWizardAppWiz::CustomizeProject(IBuildProject* pProject)
{
	// TODO: Add code here to customize the project.  If you don't wish
	//  to customize project, you may remove this virtual override.
	
	// This is called immediately after the default Debug and Release
	//  configurations have been created for each platform.  You may customize
	//  existing configurations on this project by using the methods
	//  of IBuildProject and IConfiguration such as AddToolSettings,
	//  RemoveToolSettings, and AddCustomBuildStep. These are documented in
	//  the Developer Studio object model documentation.

	// WARNING!!  IBuildProject and all interfaces you can get from it are OLE
	//  COM interfaces.  You must be careful to release all new interfaces
	//  you acquire.  In accordance with the standard rules of COM, you must
	//  NOT release pProject, unless you explicitly AddRef it, since pProject
	//  is passed as an "in" parameter to this function.  See the documentation
	//  on CCustomAppWiz::CustomizeProject for more information.

    IConfigurations *pConfigs = NULL;
    pProject->get_Configurations( &pConfigs );
    ASSERT( pConfigs );

    long nConfigs;
    pConfigs->get_Count( &nConfigs );
    ASSERT( nConfigs == 2 );

    // Grab the release and debug configs, for setting up dev studio.
    HRESULT hr;
    IConfiguration *pTmp;

    IConfiguration *pDebug;
    COleVariant varDebug((long)1);
    hr = pConfigs->Item(varDebug, &pDebug);
    ASSERT( hr == ERROR_SUCCESS );
    ASSERT( pDebug );
    BSTR bstNameDebug;
    pDebug->get_Name( &bstNameDebug );
    CString sNameDebug(bstNameDebug);

    IConfiguration *pRelease;
    COleVariant varRelease((long)2);
    hr = pConfigs->Item(varRelease, &pRelease);
    ASSERT( hr == ERROR_SUCCESS );
    ASSERT( pRelease );
    BSTR bstNameRelease;
    pRelease->get_Name( &bstNameRelease );
    CString sNameRelease(bstNameRelease);

    // Check and swap configs if they are backwards.
    // MS builds them in random order.
    CString sTail = sNameDebug.Right( 5 );
    if( sTail != "Debug" )
    {
        pTmp = pRelease;
        pRelease = pDebug;
        pDebug = pTmp;
    }

    //---  Setup the builds ---
    // Turn off MFC build.
    for( int i=0; i<2; i++ )
    {
        if( i == 0 )
            pTmp = pDebug;
        else
            pTmp = pRelease;

        // We DO NOT use mfc, so turn it off.
        AddToolSetting( pTmp, "mfc", "0" );

        // We use multithreaded libraries.
        if( pTmp == pDebug )
            AddToolSetting( pTmp, "cl.exe", "/MTd" );
        else
            AddToolSetting( pTmp, "cl.exe", "/MT" );

        // This project should default to DLL build mode.
        AddToolSetting( pTmp, "link.exe", "/dll" );

        // Nuke a bunch of default libs.
        RemoveToolSetting( pTmp, "link.exe", "kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib" );

        // Fill in all the libs we use.
        AddToolSetting( pTmp, "link.exe", "OpenGL32.lib Glu32.lib" );

        if( pTmp == pDebug )
            AddToolSetting( pTmp, "link.exe", "corelibd.lib" );
        else
            AddToolSetting( pTmp, "link.exe", "corelibr.lib" );

        // Including the evil ones at the end.
        AddToolSetting( pTmp, "link.exe", "kernel32.lib user32.lib gdi32.lib" );
    }

    pRelease->Release();
    pDebug->Release();
    pConfigs->Release();
}

void CDemopajaPluginWizardAppWiz::AddToolSetting( IConfiguration *pConfig, const char *szTool, const char *szData )
{
    CString sTool( szTool ), sData( szData );
    BSTR bszTool = sTool.AllocSysString();
    BSTR bszData = sData.AllocSysString();
    COleVariant v((long)0);
    pConfig->AddToolSettings( bszTool, bszData, v );
    SysFreeString( bszTool );
    SysFreeString( bszData );
}

void CDemopajaPluginWizardAppWiz::RemoveToolSetting( IConfiguration *pConfig, const char *szTool, const char *szData )
{
    CString sTool( szTool ), sData( szData );
    BSTR bszTool = sTool.AllocSysString();
    BSTR bszData = sData.AllocSysString();
    COleVariant v((long)0);
    pConfig->RemoveToolSettings( bszTool, bszData, v );
    SysFreeString( bszTool );
    SysFreeString( bszData );
}

// Here we define one instance of the CDemopajaPluginWizardAppWiz class.  You can access
//  m_Dictionary and any other public members of this class through the
//  global DemopajaPluginWizardaw.
CDemopajaPluginWizardAppWiz DemopajaPluginWizardaw;

