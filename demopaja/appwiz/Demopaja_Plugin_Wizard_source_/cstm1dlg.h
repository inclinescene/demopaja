#if !defined(AFX_CSTM1DLG_H__59B29313_2941_45F7_980E_FF2FABFFFA77__INCLUDED_)
#define AFX_CSTM1DLG_H__59B29313_2941_45F7_980E_FF2FABFFFA77__INCLUDED_

// cstm1dlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCustom1Dlg dialog

class CCustom1Dlg : public CAppWizStepDlg
{
// Construction
public:
	CCustom1Dlg();
	virtual BOOL OnDismiss();

// Dialog Data
	//{{AFX_DATA(CCustom1Dlg)
	enum { IDD = IDD_CUSTOM1 };
	CString	m_sAuthor;
	CString	m_sCopyright;
	CString	m_sURL;

    CComboBox	m_cbType;
	CString	m_sName;
	CString	m_sDesc;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCustom1Dlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
    void LoadSettings();
    void SaveSettings();

	// Generated message map functions
	//{{AFX_MSG(CCustom1Dlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CSTM1DLG_H__59B29313_2941_45F7_980E_FF2FABFFFA77__INCLUDED_)
