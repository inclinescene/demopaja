#if !defined(AFX_DEMOPAJAPLUGINWIZARDAW_H__27DB0F0E_B833_4BA2_AE4C_BE52380181E3__INCLUDED_)
#define AFX_DEMOPAJAPLUGINWIZARDAW_H__27DB0F0E_B833_4BA2_AE4C_BE52380181E3__INCLUDED_

// Demopaja Plugin Wizardaw.h : header file
//

class CDialogChooser;

// All function calls made by mfcapwz.dll to this custom AppWizard (except for
//  GetCustomAppWizClass-- see Demopaja Plugin Wizard.cpp) are through this class.  You may
//  choose to override more of the CCustomAppWiz virtual functions here to
//  further specialize the behavior of this custom AppWizard.
class CDemopajaPluginWizardAppWiz : public CCustomAppWiz
{
public:
	virtual CAppWizStepDlg* Next(CAppWizStepDlg* pDlg);
	virtual CAppWizStepDlg* Back(CAppWizStepDlg* pDlg);
		
	virtual void InitCustomAppWiz();
	virtual void ExitCustomAppWiz();
	virtual void CustomizeProject(IBuildProject* pProject);

protected:
    void AddToolSetting( IConfiguration *pConfig, const char *szTool, const char *szData );
    void RemoveToolSetting( IConfiguration *pConfig, const char *szTool, const char *szData );

	CDialogChooser* m_pChooser;
};

// This declares the one instance of the CDemopajaPluginWizardAppWiz class.  You can access
//  m_Dictionary and any other public members of this class through the
//  global DemopajaPluginWizardaw.  (Its definition is in Demopaja Plugin Wizardaw.cpp.)
extern CDemopajaPluginWizardAppWiz DemopajaPluginWizardaw;

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMOPAJAPLUGINWIZARDAW_H__27DB0F0E_B833_4BA2_AE4C_BE52380181E3__INCLUDED_)
