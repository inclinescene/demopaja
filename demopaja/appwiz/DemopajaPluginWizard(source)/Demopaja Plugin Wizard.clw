; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CCustom1Dlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Demopaja Plugin Wizard.h"

ClassCount=3
Class1=CCustom1Dlg
Class2=CCustom2Dlg
Class3=CCustom3Dlg

ResourceCount=2
Resource1=IDD_CUSTOM1
Resource2=IDD_CUSTOM1 (English (Canada))


[CLS:CCustom1Dlg]
Type=0
HeaderFile=cstm1dlg.h
ImplementationFile=cstm1dlg.cpp
Filter=D
LastObject=CCustom1Dlg
BaseClass=CAppWizStepDlg
VirtualFilter=dWC

[DLG:IDD_CUSTOM1]
Type=1
Class=CCustom1Dlg
ControlCount=22
Control1=IDC_STATIC,button,1342177287
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_AUTHORNAME,edit,1350631552
Control6=IDC_COPYRIGHT,edit,1350631552
Control7=IDC_URL,edit,1350631552
Control8=IDC_STATIC,button,1342177287
Control9=IDC_STATIC,static,1476526080
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_TYPE,combobox,1478557699
Control13=IDC_NAME,edit,1350631552
Control14=IDC_DESC,edit,1352728644
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_WWW_MOPPI,button,1342242816
Control18=IDC_STATIC,static,1342308352
Control19=IDC_STATIC,static,1342308352
Control20=IDC_STATIC,static,1342308352
Control21=IDC_STATIC,static,1342308352
Control22=IDC_WWW_OTRI,button,1342242816

[CLS:CCustom2Dlg]
Type=0
HeaderFile=cstm2dlg.h
ImplementationFile=cstm2dlg.cpp
Filter=D
BaseClass=CAppWizStepDlg
VirtualFilter=dWC
LastObject=CCustom2Dlg

[CLS:CCustom3Dlg]
Type=0
HeaderFile=cstm3dlg.h
ImplementationFile=cstm3dlg.cpp
Filter=D
LastObject=IDC_HEADER
BaseClass=CAppWizStepDlg
VirtualFilter=dWC

[DLG:IDD_CUSTOM1 (English (Canada))]
Type=1
ControlCount=22
Control1=IDC_STATIC,button,1342177287
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_AUTHORNAME,edit,1350631552
Control6=IDC_COPYRIGHT,edit,1350631552
Control7=IDC_URL,edit,1350631552
Control8=IDC_STATIC,button,1342177287
Control9=IDC_STATIC,static,1476526080
Control10=IDC_STATIC,static,1342308352
Control11=IDC_STATIC,static,1342308352
Control12=IDC_TYPE,combobox,1478557699
Control13=IDC_NAME,edit,1350631552
Control14=IDC_DESC,edit,1352728644
Control15=IDC_STATIC,static,1342308352
Control16=IDC_STATIC,static,1342308352
Control17=IDC_WWW_MOPPI,button,1342242816
Control18=IDC_STATIC,static,1342308352
Control19=IDC_STATIC,static,1342308352
Control20=IDC_STATIC,static,1342308352
Control21=IDC_STATIC,static,1342308352
Control22=IDC_WWW_OTRI,button,1342242816

