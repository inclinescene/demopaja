// $$root$$
//
//        By: $$AUTHOR_NAME$$
// Copyright: $$AUTHOR_COPYRIGHT$$
//       URL: $$AUTHOR_URL$$
/*-----------------------------------------------------------------*\
$$DESC$$
\*-----------------------------------------------------------------*/

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>

// Demopaja headers
#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "ClassDescC.h"
#include "ImportableImageI.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "FileIO.h"
#include "AutoGizmoC.h"

using namespace PajaTypes;
using namespace PluginClass;
using namespace PajaSystem;
using namespace Edit;
using namespace Composition;
using namespace Import;
using namespace FileIO;

#include "$$root$$.h"

//////////////////////////////////////////////////////////////////////////
//
//  $$root$$ class descriptor.
//

$$safe_root$$DescC::$$safe_root$$DescC()
{
	// empty
}

$$safe_root$$DescC::~$$safe_root$$DescC()
{
	// empty
}

void*
$$safe_root$$DescC::create()
{
	return $$safe_root$$C::create_new();
}

int32
$$safe_root$$DescC::get_classtype() const
{
	return CLASS_TYPE_EFFECT;
}

SuperClassIdC
$$safe_root$$DescC::get_super_class_id() const
{
	return SUPERCLASS_EFFECT;
}


// Returns the class ID of the image effect class.
//  This class ID has to be unique for every
//  plugin class that is implemented. 

ClassIdC
$$safe_root$$DescC::get_class_id() const
{
	return CLASSID_$$SAFE_ROOT$$;
}


const char*
$$safe_root$$DescC::get_name() const
{
	return "$$NAME$$";
}

const char*
$$safe_root$$DescC::get_desc() const
{
	return "$$DESC$$";
}

const char*
$$safe_root$$DescC::get_author_name() const
{
	return "$$AUTHOR_NAME$$";
}

const char*
$$safe_root$$DescC::get_copyright_message() const
{
	return "$$AUTHOR_COPYRIGHT$$";
}

const char*
$$safe_root$$DescC::get_url() const
{
	return "$$AUTHOR_URL$$";
}

const char*
$$safe_root$$DescC::get_help_filename() const
{
	return "res://help.html";
}

// Returns the number of device driver this effect requires. 
PajaTypes::uint32
$$safe_root$$DescC::get_required_device_driver_count() const
{
	return 1;
}

// Return the Class Id of a required device.
const ClassIdC&
$$safe_root$$DescC::get_required_device_driver( uint32 ui32Index )
{
	return PajaSystem::CLASS_OPENGL_DEVICEDRIVER;
}

// These two methods are not used for effect plugins.
// These are used with importer plugin classes and they
// return the file extensions which the plugin could load. 
uint32
$$safe_root$$DescC::get_ext_count() const
{
	return 0;
}

const char*
$$safe_root$$DescC::get_ext( uint32 ui32Index ) const
{
	return 0;
}


//////////////////////////////////////////////////////////////////////////
//
// Global descriptors, which will be returned to the Demopaja.
//

$$safe_root$$DescC	g_r$$safe_root$$Desc;


#ifndef DEMOPAJAPLAYER

//////////////////////////////////////////////////////////////////////////
//
// The DLL
//

BOOL APIENTRY
DllMain( HANDLE hModule, DWORD ulReasonForCall, LPVOID lpReserved )
{
    switch( ulReasonForCall )
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}


//
// Returns number of classes inside this plugin DLL.
//

__declspec( dllexport )
int32
get_classdesc_count()
{
	return 1;
}


//
// Returns class descriptors of the plugin classes.
//

__declspec( dllexport )
ClassDescC*
get_classdesc( int32 i )
{
	if( i == 0 )
		return &g_r$$safe_root$$Desc;
	return 0;
}


//
// Returns the API version this DLL was made with.
//

__declspec( dllexport )
int32
get_api_version()
{
	return DEMOPAJA_VERSION;
}

//
// Returns the DLL name.
//

__declspec( dllexport )
char*
get_dll_name()
{
	return "$$root$$.dll - $$NAME$$ $$AUTHOR_COPYRIGHT$$";
}

#endif	// DEMOPAJAPLAYER

//////////////////////////////////////////////////////////////////////////
//
// The effect
//

$$safe_root$$C::$$safe_root$$C()
{
	//
	// Create Transform gizmo.
	//
	m_pTraGizmo = AutoGizmoC::create_new( this, "Transform", ID_GIZMO_TRANS );

	m_pTraGizmo->add_parameter(	ParamVector2C::create_new( m_pTraGizmo, "Position", Vector2C(), ID_TRANSFORM_POS,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_ABS_POSITION, PARAM_ANIMATABLE ) );
	
	m_pTraGizmo->add_parameter(	ParamVector2C::create_new( m_pTraGizmo, "Pivot", Vector2C(), ID_TRANSFORM_PIVOT,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_REL_POSITION | PARAM_STYLE_WORLD_SPACE, PARAM_ANIMATABLE ) );
	
	m_pTraGizmo->add_parameter(	ParamFloatC::create_new( m_pTraGizmo, "Rotation", 0, ID_TRANSFORM_ROT,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_ANGLE, PARAM_ANIMATABLE, 0, 0, 1.0f ) );

	m_pTraGizmo->add_parameter(	ParamVector2C::create_new( m_pTraGizmo, "Scale", Vector2C( 1, 1 ), ID_TRANSFORM_SCALE,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_PERCENT, PARAM_ANIMATABLE, Vector2C(), Vector2C(), 0.01f ) );


	//
	// Create Attributes gizmo.
	//
	m_pAttGizmo = AutoGizmoC::create_new( this, "Attributes", ID_GIZMO_ATTRIB );

/*--- Example ------
	m_pAttGizmo->add_parameter(	ParamColorC::create_new( m_pAttGizmo, "Fill Color", ColorC( 1, 1, 1, 1 ), ID_ATTRIBUTE_COLOR,
							PARAM_STYLE_COLORPICKER, PARAM_ANIMATABLE ) );

	m_pAttGizmo->add_parameter(	ParamFileC::create_new( m_pAttGizmo, "Fill Image", SUPERCLASS_IMAGE, NULL_CLASSID, ID_ATTRIBUTE_FILE ) );
  --- Example ------*/
}

$$safe_root$$C::$$safe_root$$C( EditableI* pOriginal ) :
	EffectI( pOriginal ),
	m_pTraGizmo( 0 ),
	m_pAttGizmo( 0 )
{
	// Empty. The parameters are not created in the clone constructor.
}

$$safe_root$$C::~$$safe_root$$C()
{
	// Return if this is a clone.
	if( get_original() )
		return;

	// Release gizmos.
	m_pTraGizmo->release();
	m_pAttGizmo->release();
}

$$safe_root$$C*
$$safe_root$$C::create_new()
{
	return new $$safe_root$$C;
}

DataBlockI*
$$safe_root$$C::create()
{
	return new $$safe_root$$C;
}

DataBlockI*
$$safe_root$$C::create( EditableI* pOriginal )
{
	return new $$safe_root$$C( pOriginal );
}

void
$$safe_root$$C::copy( EditableI* pEditable )
{
	EffectI::copy( pEditable );

	// Make deep copy.
	$$safe_root$$C*	pEffect = ($$safe_root$$C*)pEditable;
	m_pTraGizmo->copy( pEffect->m_pTraGizmo );
	m_pAttGizmo->copy( pEffect->m_pAttGizmo );
}

void
$$safe_root$$C::restore( EditableI* pEditable )
{
	EffectI::restore( pEditable );

	// Make shallow copy.
	$$safe_root$$C*	pEffect = ($$safe_root$$C*)pEditable;
	m_pTraGizmo = pEffect->m_pTraGizmo;
	m_pAttGizmo = pEffect->m_pAttGizmo;
}

int32
$$safe_root$$C::get_gizmo_count()
{
	// Return number of gizmos inside this effect.
	return GIZMO_COUNT;
}

GizmoI*
$$safe_root$$C::get_gizmo( PajaTypes::int32 i32Index )
{
	// Returns specified gizmo.
	// Since the ID's are zero based, we can use them as indices.
	switch( i32Index ) {
	case ID_GIZMO_TRANS:
		return m_pTraGizmo;
	case ID_GIZMO_ATTRIB:
		return m_pAttGizmo;
	}

	return 0;
}

ClassIdC
$$safe_root$$C::get_class_id()
{
	// Return the class ID. Should be same as in the class descriptor.
	return CLASSID_$$SAFE_ROOT$$;
}

const char*
$$safe_root$$C::get_class_name()
{
	// Return the class name. Should be same as in the class descriptor.
	return "$$NAME$$";
}

void
$$safe_root$$C::set_default_file( FileHandleC* pHandle )
{
	// Sets the default file.

/*--- Example ------
	// Get the file parameter.
	ParamFileC*	pParam = (ParamFileC*)m_pAttGizmo->get_parameter( ID_ATTRIBUTE_FILE );

	// Begin Undo block.
	UndoC*	pOldUndo = pParam->begin_editing( get_undo() );
		// Set the file.
		pParam->set_file( pHandle );
	// End undo block.
	pParam->end_editing( pOldUndo );
  --- Example ------*/
}

ParamI*
$$safe_root$$C::get_default_param( int32 i32Param )
{
	// Return specified default parameter.
	if( i32Param == DEFAULT_PARAM_POSITION )
		return m_pTraGizmo->get_parameter( ID_TRANSFORM_POS );
	else if( i32Param == DEFAULT_PARAM_ROTATION )
		return m_pTraGizmo->get_parameter( ID_TRANSFORM_ROT );
	else if( i32Param == DEFAULT_PARAM_SCALE )
		return m_pTraGizmo->get_parameter( ID_TRANSFORM_SCALE );
	else if( i32Param == DEFAULT_PARAM_PIVOT )
		return m_pTraGizmo->get_parameter( ID_TRANSFORM_PIVOT );

	return 0;
}

void
$$safe_root$$C::initialize( PajaTypes::uint32 ui32Reason, DeviceContextC* pContext, TimeContextC* pTimeContext )
{
	// Empty, nothing to initialize.
}

void
$$safe_root$$C::do_frame( DeviceContextC* pContext )
{
	// Get the OpenGL device.
	OpenGLDeviceC*	pDevice = (OpenGLDeviceC*)pContext->query_interface( CLASS_OPENGL_DEVICEDRIVER );
	if( !pDevice )
		return;

	// Get the OpenGL viewport.
	OpenGLViewportC*	pViewport = (OpenGLViewportC*)pDevice->query_interface( GRAPHICSDEVICE_VIEWPORT_INTERFACE );
	if( !pViewport )
		return;

	// Set orthographic projection.
	pViewport->set_ortho( m_rBBox, m_rBBox[0][0], m_rBBox[1][0], m_rBBox[1][1], m_rBBox[0][1] );

/*--- Example ------
	// Get file handle and image.
	FileHandleC*		pHandle = 0;
	ImportableImageI*	pImp = 0;

	pHandle = ((ParamFileC*)m_pAttGizmo->get_parameter( ID_ATTRIBUTE_FILE ))->get_file();
	if( pHandle )
		pImp = (ImportableImageI*)pHandle->get_importable();

	if( pImp ) {
		// If there is image set it as current texture.
		pImp->bind_texture( pInterface, IMAGE_CLAMP | IMAGE_LINEAR );
		glEnable( GL_TEXTURE_2D );
	}
	else
		glDisable( GL_TEXTURE_2D );
  --- Example ------*/

	glDisable( GL_DEPTH_TEST );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glDepthMask( GL_FALSE );

	// Set color
	glColor4f( 1, 1, 1, 0.5f );


	// Draw rectangle.
	glBegin( GL_QUADS );

	glTexCoord2f( 0, 1 );
	glVertex2f( m_rVertices[0][0], m_rVertices[0][1] );
	
	glTexCoord2f( 1, 1 );
	glVertex2f( m_rVertices[1][0], m_rVertices[1][1] );
	
	glTexCoord2f( 1, 0 );
	glVertex2f( m_rVertices[2][0], m_rVertices[2][1] );

	glTexCoord2f( 0, 0 );
	glVertex2f( m_rVertices[3][0], m_rVertices[3][1] );

	glEnd();

	glDepthMask( GL_TRUE );
	glDisable( GL_BLEND );
}

void
$$safe_root$$C::eval_state( int32 i32Time, TimeContextC* pTimeContext )
{
	Matrix2C	rPosMat, rRotMat, rScaleMat, rPivotMat;

	Vector2C	rScale;
	Vector2C	rPos;
	Vector2C	rPivot;
	float32		f32Rot;

	// Get parameters which affect the transformation.
	((ParamVector2C*)m_pTraGizmo->get_parameter( ID_TRANSFORM_POS ))->get_val( i32Time, rPos );
	((ParamVector2C*)m_pTraGizmo->get_parameter( ID_TRANSFORM_PIVOT ))->get_val( i32Time, rPivot );
	((ParamFloatC*)m_pTraGizmo->get_parameter( ID_TRANSFORM_ROT ))->get_val( i32Time, f32Rot );
	((ParamVector2C*)m_pTraGizmo->get_parameter( ID_TRANSFORM_SCALE ))->get_val( i32Time, rScale );

	// Calculate transformation matrix.
	rPivotMat.set_trans( rPivot );
	rPosMat.set_trans( rPos );
	rRotMat.set_rot( f32Rot / 180.0f * M_PI );
	rScaleMat.set_scale( rScale ) ;
	m_rTM = rPivotMat * rRotMat * rScaleMat * rPosMat;

	float32		f32Width = 25;
	float32		f32Height = 25;
	Vector2C	rMin, rMax;
	Vector2C	rVec;

/*--- Example effect ------
	// Get the size from the fiel or use the defautls if no file.
	ImportableImageI*	pImp = 0;
	FileHandleC*		pHandle = ((ParamFileC*)m_pAttGizmo->get_parameter( ID_ATTRIBUTE_FILE ))->get_file();

	if( pHandle )
		pImp = (ImportableImageI*)pHandle->get_importable();

	if( pImp ) {
		f32Width = (float32)pImp->get_width() * 0.5f;
		f32Height = (float32)pImp->get_height() * 0.5f;
	}
  --- Example effect ------*/

	// Calcualte vertices of the rectangle.
	m_rVertices[0][0] = -f32Width;		// top-left
	m_rVertices[0][1] = -f32Height;

	m_rVertices[1][0] =  f32Width;		// top-right
	m_rVertices[1][1] = -f32Height;

	m_rVertices[2][0] =  f32Width;		// bottom-right
	m_rVertices[2][1] =  f32Height;

	m_rVertices[3][0] = -f32Width;		// bottom-left
	m_rVertices[3][1] =  f32Height;

	// Calculate bounding box
	for( uint32 i = 0; i < 4; i++ ) {
		rVec = m_rTM * m_rVertices[i];
		m_rVertices[i] = rVec;

		if( !i )
			rMin = rMax = rVec;
		else {
			if( rVec[0] < rMin[0] ) rMin[0] = rVec[0];
			if( rVec[1] < rMin[1] ) rMin[1] = rVec[1];
			if( rVec[0] > rMax[0] ) rMax[0] = rVec[0];
			if( rVec[1] > rMax[1] ) rMax[1] = rVec[1];
		}
	}

	// Store bounding box.
	m_rBBox[0] = rMin;
	m_rBBox[1] = rMax;
}

BBox2C
$$safe_root$$C::get_bbox()
{
	// Return the bounding box.
	return m_rBBox;
}

const Matrix2C&
$$safe_root$$C::get_transform_matrix() const
{
	// Return the trnasformation matrix.
	return m_rTM;
}

bool
$$safe_root$$C::hit_test( const Vector2C& rPoint )
{
	// Point in polygon test.
	// from c.g.a FAQ
	int		i, j;
	bool	bInside = false;

	for( i = 0, j = 4 - 1; i < 4; j = i++ ) {
		if( ( ((m_rVertices[i][1] <= rPoint[1]) && (rPoint[1] < m_rVertices[j][1])) ||
			((m_rVertices[j][1] <= rPoint[1]) && (rPoint[1] < m_rVertices[i][1])) ) &&
			(rPoint[0] < (m_rVertices[j][0] - m_rVertices[i][0]) * (rPoint[1] - m_rVertices[i][1]) / (m_rVertices[j][1] - m_rVertices[i][1]) + m_rVertices[i][0]) )

			bInside = !bInside;
	}

	return bInside;
}


enum $$safe_root$$ChunksE {
	CHUNK_EFFECT_BASE =		0x1000,
	CHUNK_EFFECT_TRANSGIZMO =	0x2000,
	CHUNK_EFFECT_ATTRIBGIZMO =	0x3000,
};

const uint32	EFFECT_VERSION = 1;

uint32
$$safe_root$$C::save( SaveC* pSave )
{
	uint32	ui32Error = IO_OK;

	// EffectI base class
	pSave->begin_chunk( CHUNK_EFFECT_BASE, EFFECT_VERSION );
		ui32Error = EffectI::save( pSave );
	pSave->end_chunk();

	// Transform
	pSave->begin_chunk( CHUNK_EFFECT_TRANSGIZMO, EFFECT_VERSION );
		ui32Error = m_pTraGizmo->save( pSave );
	pSave->end_chunk();

	// Attribute
	pSave->begin_chunk( CHUNK_EFFECT_ATTRIBGIZMO, EFFECT_VERSION );
		ui32Error = m_pAttGizmo->save( pSave );
	pSave->end_chunk();

	return ui32Error;
}

uint32
$$safe_root$$C::load( LoadC* pLoad )
{
	uint32	ui32Error = IO_OK;

	while( (ui32Error = pLoad->open_chunk()) == IO_OK ) {

		switch( pLoad->get_chunk_id() ) {
		case CHUNK_EFFECT_BASE:
			// EffectI base class
			if( pLoad->get_chunk_version() == EFFECT_VERSION )
				ui32Error = EffectI::load( pLoad );
			break;

		case CHUNK_EFFECT_TRANSGIZMO:
			// Transform
			if( pLoad->get_chunk_version() == EFFECT_VERSION )
				ui32Error = m_pTraGizmo->load( pLoad );
			break;

		case CHUNK_EFFECT_ATTRIBGIZMO:
			// Attribute
			if( pLoad->get_chunk_version() == EFFECT_VERSION )
				ui32Error = m_pAttGizmo->load( pLoad );
			break;

		default:
			assert( 0 );
		}

		pLoad->close_chunk();

		if( ui32Error != IO_OK && ui32Error != IO_END )
			return ui32Error;
	}

	return ui32Error;
}
