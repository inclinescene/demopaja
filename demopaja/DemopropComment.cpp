// DemopropComment.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "DemopropComment.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDemopropComment dialog


CDemopropComment::CDemopropComment(CWnd* pParent /*=NULL*/)
	: CPrefSubDlg(CDemopropComment::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDemopropComment)
	m_sComment = _T("");
	//}}AFX_DATA_INIT
}


void CDemopropComment::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDemopropComment)
	DDX_Control(pDX, IDC_EDIT_COMMENT, m_rEditComment);
	DDX_Text(pDX, IDC_EDIT_COMMENT, m_sComment);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDemopropComment, CPrefSubDlg)
	//{{AFX_MSG_MAP(CDemopropComment)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDemopropComment message handlers

BOOL CDemopropComment::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if( !m_rFont.CreateFont( 14, 0, 0, 0, FW_NORMAL, 0, 0, 0,
		ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"courier new" ) ) {
		return FALSE;
	}

	m_rEditComment.SetFont( &m_rFont );
	
	return TRUE;
}
