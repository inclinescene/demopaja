// Vector2TypeInDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "demopajadoc.h"
#include "Vector2TypeInDlg.h"
#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "ControllerC.h"
#include "KeyC.h"
#include "Vector2C.h"

using namespace PajaTypes;
using namespace Composition;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CVector2TypeInDlg dialog

void
AFXAPI
CVector2TypeInDlg::DDX_MyText( CDataExchange* pDX, int nIDC, float& value )
{
	TCHAR	szBuffer[32];
	TCHAR	szFormat[32];
	HWND	hWndCtrl = pDX->PrepareEditCtrl( nIDC );

	if( pDX->m_bSaveAndValidate ) {
		// to value
		szBuffer[0] = 0;
		::GetWindowText( hWndCtrl, szBuffer, 30 );
		value = _tcstod( szBuffer, NULL );
		value /= m_f32Scale;
	}
	else {
		// from value
		_sntprintf( szFormat, 31, "%%.%df", (int32)__max( -floor( log10( m_f32Inc ) ), 0 ) );
		_sntprintf( szBuffer, 31, szFormat, value * m_f32Scale );
		::SetWindowText( hWndCtrl, szBuffer );
	}
}



CVector2TypeInDlg::CVector2TypeInDlg(CWnd* pParent /*=NULL*/) :
	CDialog(CVector2TypeInDlg::IDD, pParent),
	m_f32Inc( 0.1f ),
	m_bClampValues( false ),
	m_f32Scale( 1.0f )
{
	//{{AFX_DATA_INIT(CVector2TypeInDlg)
	m_f32ValueX = 0.0f;
	m_f32ValueY = 0.0f;
	m_sInfoText = _T("");
	//}}AFX_DATA_INIT
}


void CVector2TypeInDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVector2TypeInDlg)
	DDX_Control(pDX, IDOK, m_rOk);
	DDX_Control(pDX, IDCANCEL, m_rCancel);
	DDX_Control(pDX, IDC_LABEL2, m_rStaticLabel2);
	DDX_Control(pDX, IDC_LABEL1, m_rStaticLabel1);
	DDX_Control(pDX, IDC_EDITY, m_rEditY);
	DDX_Control(pDX, IDC_EDITX, m_rEditX);
	DDX_MyText(pDX, IDC_EDITX, m_f32ValueX);
	DDX_MyText(pDX, IDC_EDITY, m_f32ValueY);
	DDX_Text(pDX, IDC_INFO, m_sInfoText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CVector2TypeInDlg, CDialog)
	//{{AFX_MSG_MAP(CVector2TypeInDlg)
	ON_EN_CHANGE(IDC_EDITX, OnChangeEditx)
	ON_EN_CHANGE(IDC_EDITY, OnChangeEdity)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVector2TypeInDlg message handlers

BOOL CVector2TypeInDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	TRACE( "flags: 0x%x\n", m_pParam->get_style() );

	if( m_pParam->get_style() & PARAM_STYLE_PERCENT ) {
		m_f32Inc *= 100.0f;
		m_f32Scale = 100.0f;

		m_rMin *= 100.0f;
		m_rMax *= 100.0f;

		m_rStaticLabel1.SetWindowText( "%" );
		m_rStaticLabel2.SetWindowText( "%" );
	}
	else if( m_pParam->get_style() & PARAM_STYLE_ANGLE ) {
		m_rStaticLabel1.SetWindowText( "�" );
		m_rStaticLabel2.SetWindowText( "�" );
	}
	
	if( m_bClampValues ) {
		if( !m_rSpinnerX.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT | SPNB_USELIMITS, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
			return FALSE;
		m_rSpinnerX.SetMinMax( m_rMin[0], m_rMax[0] );

		if( !m_rSpinnerY.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT | SPNB_USELIMITS, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER2 ) )
			return FALSE;
		m_rSpinnerY.SetMinMax( m_rMin[1], m_rMax[1] );
	}
	else {
		if( !m_rSpinnerX.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
			return FALSE;

		if( !m_rSpinnerY.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER2 ) )
			return FALSE;
	}

	m_rSpinnerX.SetScale( m_f32Inc );
	m_rSpinnerX.SetBuddy( &m_rEditX, SPNB_ATTACH_RIGHT );
	m_rSpinnerY.SetScale( m_f32Inc );
	m_rSpinnerY.SetBuddy( &m_rEditY, SPNB_ATTACH_RIGHT );

	// update dialog data
	Vector2C	rVec;
	m_pParam->get_val( m_i32Time, rVec );
	m_f32ValueX = rVec[0];
	m_f32ValueY = rVec[1];
	UpdateData( FALSE );

	m_rOk.SetFlat( FALSE );
	m_rCancel.SetFlat( FALSE );

	return TRUE;
}

void CVector2TypeInDlg::OnChangeEditx() 
{
	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	UpdateData( TRUE );

	if( m_bClampValues ) {
		// clamp value
		if( m_f32ValueX < m_rMin[0] )
			m_f32ValueX = m_rMin[0];
		if( m_f32ValueX > m_rMax[0] )
			m_f32ValueX = m_rMax[0];
	}

	Vector2C	rVec;
	rVec[0] = m_f32ValueX;
	rVec[1] = m_f32ValueY;
	pDoc->HandleParamNotify( m_pParam->set_val( m_i32Time, rVec ) );
	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}

void CVector2TypeInDlg::OnChangeEdity() 
{
	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	UpdateData( TRUE );

	if( m_bClampValues ) {
		// clamp value
		if( m_f32ValueY < m_rMin[1] )
			m_f32ValueY = m_rMin[1];
		if( m_f32ValueY > m_rMax[1] )
			m_f32ValueY = m_rMax[1];
	}

	Vector2C	rVec;
	rVec[0] = m_f32ValueX;
	rVec[1] = m_f32ValueY;
	pDoc->HandleParamNotify( m_pParam->set_val( m_i32Time, rVec ) );
	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}
