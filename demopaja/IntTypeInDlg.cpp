// IntTypeInDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "demopajadoc.h"
#include "IntTypeInDlg.h"
#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "ParamI.h"

using namespace PajaTypes;
using namespace Composition;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIntTypeInDlg dialog


void
AFXAPI
CIntTypeInDlg::DDX_MyText( CDataExchange* pDX, int nIDC, int& value )
{
	TCHAR	szBuffer[32];
	HWND	hWndCtrl = pDX->PrepareEditCtrl( nIDC );

	if( pDX->m_bSaveAndValidate ) {
		// to value
		szBuffer[0] = 0;
		::GetWindowText( hWndCtrl, szBuffer, 30 );
		value = _ttoi( szBuffer );
	}
	else {
		// from value
		_tcscpy( szBuffer, _itot( value, szBuffer, 10 ) );
		::SetWindowText( hWndCtrl, szBuffer );
	}
}

CIntTypeInDlg::CIntTypeInDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIntTypeInDlg::IDD, pParent),
	m_i32Min( 0 ),
	m_i32Max( 0 ),
	m_i32Inc( 1 ),
	m_bClampValues( false )
{
	//{{AFX_DATA_INIT(CIntTypeInDlg)
	m_i32Value = 0;
	m_sInfoText = _T("");
	//}}AFX_DATA_INIT
}


void CIntTypeInDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIntTypeInDlg)
	DDX_Control(pDX, IDOK, m_rOk);
	DDX_Control(pDX, IDCANCEL, m_rCancel);
	DDX_Control(pDX, IDC_EDIT, m_rEdit);
	DDX_Text(pDX, IDC_EDIT, m_i32Value);
	DDX_Text(pDX, IDC_INFO, m_sInfoText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CIntTypeInDlg, CDialog)
	//{{AFX_MSG_MAP(CIntTypeInDlg)
	ON_EN_CHANGE(IDC_EDIT, OnChangeEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIntTypeInDlg message handlers

BOOL CIntTypeInDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if( m_bClampValues ) {
		if( !m_rSpinner.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYINT | SPNB_USELIMITS, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
			return FALSE;
		m_rSpinner.SetMinMax( m_i32Min, m_i32Max );
	}
	else
		if( !m_rSpinner.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYINT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
			return FALSE;

	m_rSpinner.SetScale( m_i32Inc );
	m_rSpinner.SetBuddy( &m_rEdit, SPNB_ATTACH_RIGHT );

	// update dialog data
	m_pParam->get_val( m_i32Time, m_i32Value );
	UpdateData( FALSE );

	m_rOk.SetFlat( FALSE );
	m_rCancel.SetFlat( FALSE );

	return TRUE;
}

void CIntTypeInDlg::OnChangeEdit() 
{
	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	UpdateData( TRUE );

	if( m_bClampValues ) {
		// clamp value
		if( m_i32Value < m_i32Min )
			m_i32Value = m_i32Min;
		if( m_i32Value > m_i32Max )
			m_i32Value = m_i32Max;
	}

	pDoc->HandleParamNotify( m_pParam->set_val( m_i32Time, m_i32Value ) );
	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}

