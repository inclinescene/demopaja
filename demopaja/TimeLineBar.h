#if !defined(AFX_TIMELINEBAR_H__B4C5B4F9_2A90_11D4_A80C_0000E8D926FD__INCLUDED_)
#define AFX_TIMELINEBAR_H__B4C5B4F9_2A90_11D4_A80C_0000E8D926FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TimeLineBar.h : header file
//

// forward declaration
class CTimeLineBar;

#include "afxole.h"
#include "DemopajaDoc.h"
#include "SizeCBar.h"
#include "ItemDragTarget.h"
#include "LayerC.h"
#include "EffectI.h"
#include "GizmoI.h"
#include "SceneItemC.h"
#include "TimeSegmentC.h"
#include "KeyC.h"
#include "KeySelectorC.h"
#include "FlatPopUpMenu.h"
#include "UndoC.h"
#include "BtnST.h"
#include "DropTarget.h"
#include "EnTabCtrl.h"
#include "SceneItemListC.h"

/////////////////////////////////////////////////////////////////////////////
// CTimeLineBar window


#define UPDATE_TIME		(WM_APP + 100)


#ifndef baseCMyBar
#define baseCMyBar CSizingControlBarG
#endif

class CTimeLineBar : public baseCMyBar //, CDropTarget
{
public:
	// Construction
	CTimeLineBar();
	virtual ~CTimeLineBar();


	virtual DROPEFFECT	OnDragOver( COleDataObject* pDataObject, DWORD dwKeyState, CPoint point );
	virtual BOOL		OnDrop( COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point );

//	void				RedrawTimecursor();
	void				SetTimecursor( PajaTypes::int32 i32Time );
	PajaTypes::int32	GetTimecursor();
	afx_msg void		OnPlay();

	void				OnEditCopy();
	void				OnEditCut();
	void				OnEditPaste();

	void				UpdateTimeCursor( PajaTypes::int32 i32Time );

	void				SetScrollRanges();
	void				UpdateSceneTab();

	void				UpdateNotify( PajaTypes::uint32 ui32Notify );

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimeLineBar)
	//}}AFX_VIRTUAL

	// Generated message map functions
protected:

	struct EffectNameS {
		PajaTypes::uint32	m_ui32Idx;
		std::string			m_sName;

		bool	operator<( const EffectNameS& rOther ) const
		{
			if( m_sName.compare( rOther.m_sName ) < 0 )
				return true;
			return false;
		}
	};

	enum LayerHitE {
		HIT_LIST_NONE			= 0x0000,
		HIT_LIST_EYE			= 0x0001,
		HIT_LIST_LOCK			= 0x0002,
		HIT_LIST_ARROW			= 0x0004,
		HIT_LIST_ITEM			= 0x0008,
		HIT_LIST_SIZEPARAM		= 0x0010,
		HIT_LIST_VALUESEPARATOR	= 0x0020,
		HIT_LIST_SEPARATOR		= 0x0040,
		HIT_LIST_ANIM			= 0x0080,
		HIT_LIST_VALUE			= 0x0100,
		HIT_LIST_NEXT_KEY		= 0x0200,
		HIT_LIST_SPINNERX		= 0x0400,
		HIT_LIST_SPINNERY		= 0x0800,
		HIT_LIST_SPINNERZ		= 0x1000,
		HIT_LIST_FILECOMBO		= 0x2000,
		HIT_LIST_INTCOMBO		= 0x4000,
	};

	enum TimeSegmentHitE {
		HIT_TSEG_NONE = 0,
		HIT_TSEG_KEY = 1,
		HIT_TSEG_BAR = 2,
		HIT_TSEG_SOMEWHERE = 4,
	};

	enum MarkerHitE {
		HIT_MARKER_NONE = 0,
		HIT_MARKER_LABEL = 1,
	};

	enum ControllerHitE {
		HIT_CONT_NONE = 0,
		HIT_CONT_KEY_TIME = 1,
		HIT_CONT_KEY_VALUE = 2,
		HIT_CONT_SOMEWHERE = 4,
	};

	enum TrackingActionE {
		TRACKING_NONE = 0,
		TRACKING_VERTRULER ,
		TRACKING_SIZEPARAM,
		TRACKING_TIMECURSOR,
		TRACKING_VALUESEPARATOR,
		TRACKING_SEPARATOR,
		TRACKING_KEYS,
		TRACKING_TIMESEGMENT,
		TRACKING_KEYVALUES,
		TRACKING_BOXSELECT,
		TRACKING_PAN,
		TRACKING_LAYERDRAG,
		TRACKING_EFFECTDRAG,
		TRACKING_MARKER,
		TRACKING_EYE,
		TRACKING_LOCK,
		TRACKING_SPINNERX,
		TRACKING_SPINNERY,
		TRACKING_SPINNERZ,
	};

	enum TimerEventE {
		TIMER_TIMERCURSOR_LEFT = 1,
		TIMER_TIMERCURSOR_RIGHT,
		TIMER_BOXSELECT_LEFT,
		TIMER_BOXSELECT_RIGHT,
		TIMER_BOXSELECT_TOP,
		TIMER_BOXSELECT_BOTTOM,
		TIMER_MOVEKEYS_LEFT,
		TIMER_MOVEKEYS_RIGHT,
		TIMER_MOVETIMESEGMENT_LEFT,
		TIMER_MOVETIMESEGMENT_RIGHT,
	};

	enum TimelineColorsE {
		TIMELINE_TEXT,
		TIMELINE_BLACK,
		TIMELINE_BACK,
		TIMELINE_BACK_SEL,
		TIMELINE_DARK_TICK,
		TIMELINE_LIGHT_TICK,
		TIMELINE_GRID,
		TIMELINE_VALUE_GRID,
		TIMELINE_VALUE_TEXT,
		TIMELINE_PARAM_FACE,
		TIMELINE_PARAM_LIGHT,
		TIMELINE_PARAM_SHADOW,
	};

	CItemDragTarget		m_rDropTarget;
	CScrollBar			m_rListScroll;
	CScrollBar			m_rTimelineScroll;
	CImageList			m_rImageList;
	CImageList			m_rImageListKeys;
	CImageList			m_rImageListTab;
	CDemopajaDoc*		m_pDoc;
	CFont				m_rFont;
	CFont				m_rBoldFont;
	CFont				m_rItalicFont;
	CFont				m_rSmallFont;
	CEdit*				m_pEditCtrl;
	bool				m_bInitialised;
	HCURSOR				m_hCursor;
	CBitmap				m_rStopBitmap;
	CBitmap				m_rPlayBitmap;
	CBitmap				m_rForwBitmap;
	CBitmap				m_rBackBitmap;
	CBitmap				m_rNextBitmap;
	CBitmap				m_rPrevBitmap;
	CButtonST			m_rPlayButton;
	CButtonST			m_rForwButton;
	CButtonST			m_rNextButton;
	CButtonST			m_rBackButton;
	CButtonST			m_rPrevButton;
	CEnTabCtrl			m_rSceneTab;

	CBitmap				m_rLayerBitmap;
	CBitmap				m_rEffectBitmap;
	CBitmap				m_rDelBitmap;
	CButtonST			m_rLayerButton;
	CButtonST			m_rEffectButton;
	CButtonST			m_rDelButton;

	CBitmap				m_rZoomInBitmap;
	CBitmap				m_rZoomOutBitmap;
	CButtonST			m_rZoomInButton;
	CButtonST			m_rZoomOutButton;
	CStatic				m_rZoomStatic;

	CEdit				m_rFrameEdit;

	COLORREF			GetColor( PajaTypes::uint32 ui32Col );
	CSize				GetTextExtent( const char* szText, CFont* pFont );

	PajaTypes::int32	GetListPos();
	void				SetListPos( PajaTypes::int32 i32Pos );
	void				SetTimeStart( PajaTypes::int32 i32Start );

	const char*			GetItemName( SceneItemC* pItem );
	void				RenameItem( SceneItemC* pItem );
	void				CreateEffect( SceneItemC* pItem, PajaTypes::uint32 ui32Num );
	void				SampleController( SceneItemC* pItem );

	void				CutTrailingZeros( CString& str );
	void				AdjustControls();
	void				FindVisibleRange();

	void				LayerMenu( CPoint rPoint, SceneItemC* pItem );
	void				EffectMenu( CPoint rPoint, SceneItemC* pItem );
	void				GizmoMenu( CPoint rPoint, SceneItemC* pItem );
	void				FillEffectMenu( CFlatPopupMenu& rMenu );

	PajaTypes::int32	HitTestSceneList( CPoint pt, SceneItemC** pHitItem = 0 );
	PajaTypes::int32	HitTestListHeader( CPoint pt );
	PajaTypes::int32	HitTestTimeSegment( CPoint rPt, SceneItemC* pItem, Composition::KeyC** pKey = 0 );
	PajaTypes::int32	HitTestController( CPoint rPt, SceneItemC* pItem, Composition::KeyC** pOutKey = 0, PajaTypes::uint32* ui32HitChannel = 0, bool bForceSample = false );
	PajaTypes::int32	HitTestMarker( CPoint rPt, PajaTypes::int32* pIndexOut, PajaTypes::int32* pTimeOut );

	// Timeline/Curves
	PajaTypes::float32	NiceNum( PajaTypes::float32 f32X, bool bRound );
	void				DrawExpandedContGrid( CDC* pDC, PajaTypes::int32 i32Y, PajaTypes::int32 i32Height,
												PajaTypes::int32 i32MinTime, PajaTypes::int32 i32MaxTime,
												PajaTypes::float32 f32MinVal, PajaTypes::float32 f32MaxVal,
												PajaTypes::float32 f32ScaleY, bool bUseInt = false );
	void				DrawSceneList( CDC* pDC );
	void				DrawSceneTimeline( CDC* pDC );
	void				DrawTimeRuler( CDC* pDC );
	void				DrawWaveform( CDC* pDC );
	void				DrawTimeCursor( CDC* pDC, bool bClearOld = false );
	void				DrawTimeSegment( CDC* pDC, SceneItemC* pItem );
	void				DrawTimeSegmentRange( CDC* pDC, SceneItemC* pItem );
	void				DrawGizmo( CDC* pDC, SceneItemC* pItem );
	void				DrawParameterCurves( CDC* pDC, SceneItemC* pItem, Composition::ParamI* pParam, PajaTypes::int32 i32TimeOffset, PajaTypes::int32 i32MinTime, PajaTypes::int32 i32MaxTime );
	void				DrawParameterFile( CDC* pDC, SceneItemC* pItem, Composition::ParamFileC* pParam, PajaTypes::int32 i32TimeOffset, PajaTypes::int32 i32MinTime, PajaTypes::int32 i32MaxTime );
	void				DrawParameterFileTimeruler( CDC* pDC, SceneItemC* pItem, Composition::ParamFileC* pParam, PajaTypes::int32 i32FrameTime, PajaTypes::int32 i32TimeOffset, const CRect& rBoundsRect );
	void				DrawParameter( CDC* pDC, SceneItemC* pItem );
	void				DrawControllerBar( CDC* pDC, SceneItemC* pItem, Composition::ControllerC* pCont, PajaTypes::int32 i32TimeOffset, PajaTypes::int32 i32MaxTime );
	void				DrawControllerKeys( CDC* pDC, SceneItemC* pItem, Composition::ControllerC* pCont, PajaTypes::int32 i32TimeOffset, PajaTypes::int32 i32MaxTime );
	void				DrawBoxSelect();
	void				ParamIntCombo( const CPoint& rPt, Composition::ParamIntC* pParam, PajaTypes::int32 i32Time );
	void				ParamFileCombo( const CPoint& rPt, Composition::ParamFileC* pParam, PajaTypes::int32 i32Time );


	// Scene list
	SceneItemListC				m_rSceneItemList;

	// Size of different parts of the UI.
	CRect						m_rListRect;
	CRect						m_rOrigListRect;
	CRect						m_rTimelineRect;
	CRect						m_rRulerRect;
	CRect						m_rWaveformRect;
	PajaTypes::int32			m_i32WaveformHeight;
	PajaTypes::int32			m_i32LayerValueSize;

	PajaTypes::int32			m_i32ViewTimeStart;			// time start in frames
	PajaTypes::int32			m_i32FrameWidthInPixels;	// width of frame in pixels

	// Visible item range
	PajaTypes::int32			m_i32FirstVisible;
	PajaTypes::int32			m_i32LastVisible;

	// Tracking variables
	TrackingActionE				m_eTrackAction;
	CPoint						m_rTrackOrigPt;
	PajaTypes::int32			m_i32TrackOrigTime;
	PajaTypes::int32			m_i32TrackOrigPos;
	PajaTypes::float32			m_f32TrackOrigVal;
	SceneItemC*					m_pTrackItem;
	PajaTypes::uint32			m_ui32TrackEditingChannel;
	SceneItemC					m_rTrackTargetItem;
	bool						m_bTrackInsertAfter;
	PajaTypes::int32			m_i32TrackValue;

	SceneItemC*					m_pEditNameItem;

	PajaTypes::uint32			m_ui32ScrollTimeID;

	PajaTypes::int32			m_i32BoxSelectStartTime;
	PajaTypes::int32			m_i32BoxSelectStartY;
	PajaTypes::int32			m_i32BoxSelectLastTime;
	PajaTypes::int32			m_i32BoxSelectLastY;
	PajaTypes::int32			m_i32BoxSelectCurTime;
	PajaTypes::int32			m_i32BoxSelectCurY;

	bool						m_bFirstTimeBoxSelect;
	bool						m_bScrollingBoxSelect;
	bool						m_bEditingFrame;
	bool						m_bMouseMoved;

	PajaTypes::int32			m_i32OldTimeCursor;

	//{{AFX_MSG(CTimeLineBar)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnKeyUp(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnDestroy();
	afx_msg UINT OnGetDlgCode();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	afx_msg void OnKillfocusEditLabel();
	afx_msg void OnChangeEditLabel();
	afx_msg void OnChangeFrameEdit();
	afx_msg void OnBack();
	afx_msg void OnPrev();
	afx_msg void OnNext();
	afx_msg void OnForw();
	afx_msg void OnPrevBeat();
	afx_msg void OnNextBeat();
	afx_msg void OnCreateLayer();
	afx_msg void OnCreateEffect();
	afx_msg void OnDeleteEffect();
	afx_msg void OnZoomIn();
	afx_msg void OnZoomOut();
	afx_msg void OnSelchangeSceneSelect( NMHDR* pNMHDR, LRESULT* pResult );
	afx_msg void OnRClickSceneSelect( NMHDR* pNMHDR, LRESULT* pResult );
	virtual LRESULT WindowProc( UINT message, WPARAM wParam, LPARAM lParam );

	bool OnSceneLButtonDown( UINT nFlags, CPoint point );
	bool OnSceneLButtonDblClk( UINT nFlags, CPoint point );
	bool OnSceneLButtonUp( UINT nFlags, CPoint point );
	bool OnSceneRButtonDown( UINT nFlags, CPoint point );
	bool OnSceneMouseMove( UINT nFlags, CPoint point );
	void OnSceneTimer( UINT nIDEvent );

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TIMELINEBAR_H__B4C5B4F9_2A90_11D4_A80C_0000E8D926FD__INCLUDED_)
