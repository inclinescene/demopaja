// RecursionErrorDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "RecursionErrorDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CRecursionErrorDlg dialog


CRecursionErrorDlg::CRecursionErrorDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CRecursionErrorDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CRecursionErrorDlg)
	m_sWarningText = _T("");
	//}}AFX_DATA_INIT
}


void CRecursionErrorDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CRecursionErrorDlg)
	DDX_Control(pDX, IDC_STACKTREE, m_rStackTree);
	DDX_Text(pDX, IDC_STATICWARNING, m_sWarningText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CRecursionErrorDlg, CDialog)
	//{{AFX_MSG_MAP(CRecursionErrorDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CRecursionErrorDlg message handlers


void CRecursionErrorDlg::AddErrorItem( const char* szName, int iFlag )
{
	RecErrorItemS	rItem;
	rItem.sName = szName;
	rItem.iFlags = iFlag;
	m_rItems.push_back( rItem );
}


BOOL CRecursionErrorDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Create imagelist for images used in timeline
	if( !m_rImageList.Create( IDB_RECURSIONERROR, 16, 5, RGB( 255, 0, 255 ) ) )
		return -1;

	m_rStackTree.SetImageList( &m_rImageList, TVSIL_NORMAL );

	m_rStackTree.SetItemHeight( 16 );

	HTREEITEM	hParent = TVI_ROOT;

	for( int i = 0; i < m_rItems.size(); i++ ) {
		int	iImage = 0;
		switch( m_rItems[i].iFlags ) {
		case RECERR_SCENE:	iImage = 1; break;
		case RECERR_EFFECT:	iImage = 2; break;
		case RECERR_PARAM:	iImage = 3; break;
		case RECERR_FILEHANDLE:	iImage = 4; break;
		case RECERR_RENDER_TO_TEXTURE:	iImage = 5; break;
		case RECERR_ERROR:	iImage = 0; break;
		};
		hParent = m_rStackTree.InsertItem( m_rItems[i].sName, iImage, iImage, hParent );
		m_rStackTree.EnsureVisible( hParent );
	}
	
	return TRUE;
}
