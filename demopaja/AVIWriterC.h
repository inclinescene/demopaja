#ifndef __DEMOPAJA_AVIWRITERC_H__
#define __DEMOPAJA_AVIWRITERC_H__

#include <windows.h>
#include <vfw.h>
#include "PajaTypes.h"



class AVIWriterC
{
public:
	AVIWriterC();
	virtual ~AVIWriterC();

	bool	open( const char* szName, PajaTypes::int32 i32Width, PajaTypes::int32 i32Height, PajaTypes::int32 i32FPS, COMPVARS* pCompVars, PajaTypes::int32 i32AudioChannels, PajaTypes::int32 i32AudioFreq, HACMDRIVERID hAudioDriverId, WAVEFORMATEX* pCompressedFormat );

	bool	write_frame( PajaTypes::uint8* pImageData, PajaTypes::uint8* pAudioData, PajaTypes::int32 i32AudioDataSize );

	PajaTypes::int32	get_audio_bytes_per_frame() const;

	void	close();

protected:

	PajaTypes::int32	m_i32Rate;				// Frame rate 
	PajaTypes::int32	m_i32SampleCountPerFrame;				// Frame rate 
	PajaTypes::int32	m_i32Frame;				// Frame
	PajaTypes::int32	m_i32AudioFrame;				// Frame
	PajaTypes::int32	m_i32SampleSize;				// Frame
	HACMDRIVER				m_had;             // ACM driver handle
	HACMSTREAM				m_hstr;            // ACM conversion stream handle
	PajaTypes::uint8*	m_pAudioCompressionBuffer;
	WAVEFORMATEX			m_rWaveFormat;

	BITMAPINFOHEADER	m_rBIH;					// structure contains information for a single stream

	PAVIFILE				m_pAVIFile;				// file interface pointer
	PAVISTREAM			m_pStream;				// Address of the stream interface
	PAVISTREAM			m_pStreamCompressed;	// Address of the compressed video stream
	PAVISTREAM			m_pStreamAudio;				// Address of the stream interface
};



#endif //__DEMOPAJA_AVIWRITERC_H__