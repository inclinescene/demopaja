#if !defined(AFX_DEMOPROPERTIES_H__0E3487E6_BABF_4C69_97DD_E04625F5DFF9__INCLUDED_)
#define AFX_DEMOPROPERTIES_H__0E3487E6_BABF_4C69_97DD_E04625F5DFF9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DemoProperties.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDemoPropertiesDlg dialog

class CDemoPropertiesDlg : public CDialog
{
// Construction
public:
	CDemoPropertiesDlg(CWnd* pParent = NULL);   // standard constructor

	// Dialog Data
	//{{AFX_DATA(CDemoPropertiesDlg)
	enum { IDD = IDD_DEMOPROPERTIES };
	CEdit	m_rEditComment;
	CString	m_sComment;
	CString	m_sDuration;
	int		m_i32FPS;
	int		m_i32Height;
	CString	m_rMusicFile;
	int		m_i32Width;
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDemoPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CFont	m_rFont;

	// Generated message map functions
	//{{AFX_MSG(CDemoPropertiesDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPickMusic();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMOPROPERTIES_H__0E3487E6_BABF_4C69_97DD_E04625F5DFF9__INCLUDED_)
