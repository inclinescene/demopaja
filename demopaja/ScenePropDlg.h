#if !defined(AFX_SCENEPROPDLG_H__591AD036_5AE7_4ED9_88DE_839B6BD4E427__INCLUDED_)
#define AFX_SCENEPROPDLG_H__591AD036_5AE7_4ED9_88DE_839B6BD4E427__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScenePropDlg.h : header file
//

#include "SpinnerButton.h"
#include "ColorC.h"

/////////////////////////////////////////////////////////////////////////////
// CScenePropDlg dialog

class CScenePropDlg : public CDialog
{
// Construction
public:
	CScenePropDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CScenePropDlg)
	enum { IDD = IDD_SUBSCENEPROP };
	CEdit	m_rEditWidth;
	CEdit	m_rEditHeight;
	CString	m_sName;
	int		m_iHeight;
	int		m_iWidth;
	CString	m_sDuration;
	//}}AFX_DATA

	PajaTypes::ColorC	m_rBGColor;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScenePropDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSpinnerButton		m_rSpinnerWidth;
	CSpinnerButton		m_rSpinnerHeight;

	// Generated message map functions
	//{{AFX_MSG(CScenePropDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonLayout();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCENEPROPDLG_H__591AD036_5AE7_4ED9_88DE_839B6BD4E427__INCLUDED_)
