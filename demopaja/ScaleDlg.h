#if !defined(AFX_SCALEDLG_H__C4D9524B_48DF_4A1C_AAAA_983B1F37A2D3__INCLUDED_)
#define AFX_SCALEDLG_H__C4D9524B_48DF_4A1C_AAAA_983B1F37A2D3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScaleDlg.h : header file
//

#include "SpinnerButton.h"

/////////////////////////////////////////////////////////////////////////////
// CScaleDlg dialog

class CScaleDlg : public CDialog
{
// Construction
public:
	CScaleDlg( CWnd* pParent = NULL );   // standard constructor

// Dialog Data
	//{{AFX_DATA(CScaleDlg)
	enum { IDD = IDD_TRANS_SCALE };
	CEdit	m_rEditX;
	CEdit	m_rEditY;
	CButton	m_rUniform;
	float	m_f32X;
	float	m_f32Y;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CScaleDlg)
	public:
	virtual BOOL Create( CWnd* pParentWnd );
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSpinnerButton	m_rSpinnerX;
	CSpinnerButton	m_rSpinnerY;

	// Generated message map functions
	//{{AFX_MSG(CScaleDlg)
	afx_msg void OnApply();
	afx_msg void OnUniform();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCALEDLG_H__C4D9524B_48DF_4A1C_AAAA_983B1F37A2D3__INCLUDED_)
