// CommonDirsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "CommonDirsDlg.h"
#include "FolderDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


using namespace PajaTypes;

/////////////////////////////////////////////////////////////////////////////
// CCommonDirsDlg dialog


CCommonDirsDlg::CCommonDirsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCommonDirsDlg::IDD, pParent)
	, m_sProjectPath(_T(""))
	, m_bUpdateImportables(FALSE)
{
	//{{AFX_DATA_INIT(CCommonDirsDlg)
	//}}AFX_DATA_INIT
}


void CCommonDirsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCommonDirsDlg)
	DDX_Control(pDX, IDC_BUTTONMOD, m_rButtonMod);
	DDX_Control(pDX, IDC_BUTTONDEL, m_rButtonDel);
	DDX_Control(pDX, IDC_BUTTONADD, m_rButtonAdd);
	DDX_Control(pDX, IDC_DIRLIST, m_rDirList);
	//}}AFX_DATA_MAP
	DDX_Control(pDX, IDC_BUTTONCHOOSE, m_rButtonChoose);
	DDX_Text(pDX, IDC_EDIT1, m_sProjectPath);
	DDX_Control(pDX, IDC_BUTTONSELECT, m_rButtonSelect);
	DDX_Control(pDX, IDC_BUTTONADDPATH, m_rButtonAddPath);
	DDX_Check(pDX, IDC_CHECK_UPDATEIMPORTABLES, m_bUpdateImportables);
}


BEGIN_MESSAGE_MAP(CCommonDirsDlg, CDialog)
	//{{AFX_MSG_MAP(CCommonDirsDlg)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_DIRLIST, OnEndlabeleditDirlist)
	ON_NOTIFY(NM_DBLCLK, IDC_DIRLIST, OnDblclkDirlist)
	ON_BN_CLICKED(IDC_BUTTONADD, OnButtonadd)
	ON_BN_CLICKED(IDC_BUTTONDEL, OnButtondel)
	ON_BN_CLICKED(IDC_BUTTONMOD, OnButtonmod)
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTONCHOOSE, OnBnClickedButtonchoose)
	ON_BN_CLICKED(IDC_BUTTONSELECT, OnBnClickedButtonselect)
	ON_BN_CLICKED(IDC_BUTTONADDPATH, OnBnClickedButtonaddpath)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCommonDirsDlg message handlers

BOOL CCommonDirsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();


	// map colors
	COLORMAP	rColors[3];
	// magenta BG
	rColors[0].from = RGB( 255, 0, 255 );
	rColors[0].to = ::GetSysColor( COLOR_BTNFACE );
	// light grey
	rColors[1].from = RGB( 192, 192, 192 );
	rColors[1].to = ::GetSysColor( COLOR_BTNFACE );
	// dark grey
	rColors[2].from = RGB( 128, 128, 128 );
	rColors[2].to = ::GetSysColor( COLOR_BTNSHADOW );

	// create bitmaps for buttons
	if( !m_rBitmapAdd.LoadMappedBitmap( IDB_COMDIR_ADD, 0, rColors, 3 ) )
		return -1;
	if( !m_rBitmapAddPath.LoadMappedBitmap( IDB_COMDIR_ADDPATH, 0, rColors, 3 ) )
		return -1;
	if( !m_rBitmapDel.LoadMappedBitmap( IDB_COMDIR_DEL, 0, rColors, 3 ) )
		return -1;
	if( !m_rBitmapMod.LoadMappedBitmap( IDB_COMDIR_MODIFY, 0, rColors, 3 ) )
		return -1;
	if( !m_rBitmapUp.LoadMappedBitmap( IDB_COMDIR_UP, 0, rColors, 3 ) )
		return -1;

	m_rButtonAdd.SetBitmaps( m_rBitmapAdd );
	m_rButtonAddPath.SetBitmaps( m_rBitmapAddPath );
	m_rButtonDel.SetBitmaps( m_rBitmapDel );
	m_rButtonMod.SetBitmaps( m_rBitmapMod );
	m_rButtonChoose.SetBitmaps( m_rBitmapMod );
	m_rButtonSelect.SetBitmaps( m_rBitmapUp );
	
	m_rButtonAdd.SetTooltipText( "Add New Path" );
	m_rButtonAddPath.SetTooltipText( "Add Current Project Path" );
	m_rButtonDel.SetTooltipText( "Delete Path" );
	m_rButtonMod.SetTooltipText( "Edit Path" );
	m_rButtonChoose.SetTooltipText( "Choose Path" );
	m_rButtonSelect.SetTooltipText( "Select as New Project Path" );
	
	m_rButtonSelect.SetFlat( FALSE );
	m_rButtonChoose.SetFlat( FALSE );

	m_rDirList.InsertColumn( 0, "Directory", LVCFMT_LEFT, 380 );

	// get folders from registry
	CDemopajaApp*	pApp = (CDemopajaApp*)AfxGetApp();
	uint32	ui32DirCount = pApp->GetProfileInt( "CommonDirs", "DirCount", 0 );
	CString	sDirEntry, sDir;
	for( uint32 i = 0; i < ui32DirCount; i++ ) {
		sDirEntry.Format( "Dir%03d", i );
		sDir = pApp->GetProfileString( "CommonDirs", sDirEntry );
		m_rDirList.InsertItem( 0, sDir );
 	}

	return TRUE;
}

void CCommonDirsDlg::OnEndlabeleditDirlist(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;

	if( pDispInfo->item.pszText != NULL ) {
		TRACE( "new item(%d) name: %s\n", pDispInfo->item.iItem, pDispInfo->item.pszText );
		m_rDirList.SetItemText( pDispInfo->item.iItem, 0, pDispInfo->item.pszText );
	}
	
	*pResult = 0;
}

void CCommonDirsDlg::OnDblclkDirlist(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int	iSel = m_rDirList.GetSelectionMark();
	if( iSel != -1 )
		m_rDirList.EditLabel( iSel );

	*pResult = 0;
}

void CCommonDirsDlg::OnButtonadd() 
{
	CFolderDialog	rDlg;
	if( rDlg.DoModal() == IDOK ) {
		m_rDirList.InsertItem( 0, rDlg.GetPathName() );
	}
}

void CCommonDirsDlg::OnButtondel() 
{
	int	iSel = m_rDirList.GetSelectionMark();
	if( iSel != -1 ) {
		m_rDirList.DeleteItem( iSel );
	}
}

void CCommonDirsDlg::OnButtonmod() 
{
	int	iSel = m_rDirList.GetSelectionMark();
	if( iSel != -1 ) {
		CFolderDialog	rDlg( m_rDirList.GetItemText( iSel, 0 ) );
		if( rDlg.DoModal() == IDOK ) {
			m_rDirList.SetItemText( iSel, 0, rDlg.GetPathName() );
		}
	}
}

void CCommonDirsDlg::OnBnClickedButtonchoose()
{
	CFolderDialog	rDlg( m_sProjectPath );
	if( rDlg.DoModal() == IDOK ) {
		m_sProjectPath = rDlg.GetPathName();
		UpdateData( FALSE );	// modify text
	}
}

void CCommonDirsDlg::OnOK() 
{
	int	i;
	CDemopajaApp*	pApp = (CDemopajaApp*)AfxGetApp();

	pApp->WriteProfileInt( "CommonDirs", "DirCount", m_rDirList.GetItemCount() );

	CString	sDirEntry;
	for( i = 0; i < m_rDirList.GetItemCount(); i++ ) {
		sDirEntry.Format( "Dir%03d", i );
		pApp->WriteProfileString( "CommonDirs", sDirEntry, m_rDirList.GetItemText( i, 0 ) );
   	}
	
	CDialog::OnOK();
}

void CCommonDirsDlg::OnBnClickedButtonselect()
{
	int	iSel = m_rDirList.GetSelectionMark();
	if( iSel != -1 ) {
		m_sProjectPath = m_rDirList.GetItemText( iSel, 0 );
		UpdateData( FALSE );	// modify text
	}
}

void CCommonDirsDlg::OnBnClickedButtonaddpath()
{
}
