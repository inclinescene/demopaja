// MoveDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "MoveDlg.h"
#include "demopajadoc.h"
#include "PajaTypes.h"
#include "Vector2C.h"
#include "BBox2C.h"
#include <math.h>
#include "SceneC.h"
#include "LayerC.h"
#include "EffectI.h"
#include "ParamI.h"
#include "UndoC.h"


using namespace Composition;
using namespace PajaTypes;
using namespace Edit;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMoveDlg dialog


CMoveDlg::CMoveDlg( CWnd* pParent /*=NULL*/ ) :
	CDialog(CMoveDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMoveDlg)
	m_f32X = 0.0f;
	m_f32Y = 0.0f;
	//}}AFX_DATA_INIT
}


void CMoveDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMoveDlg)
	DDX_Control(pDX, IDC_EDITY, m_rEditY);
	DDX_Control(pDX, IDC_EDITX, m_rEditX);
	DDX_Text(pDX, IDC_EDITX, m_f32X);
	DDX_Text(pDX, IDC_EDITY, m_f32Y);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMoveDlg, CDialog)
	//{{AFX_MSG_MAP(CMoveDlg)
	ON_BN_CLICKED(IDC_APPLY, OnApply)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMoveDlg message handlers

void CMoveDlg::OnApply() 
{
	// get latest data
	UpdateData( TRUE );

	CDemopajaDoc*			pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	SceneC*		pScene = pDoc->GetCurrentScene();
	int32		i32Time = pDoc->GetTimecursor() * pDoc->GetFrameSizeInTicks();

	UndoC*	pUndo = new UndoC( "Transform Move" );
	UndoC*	pOldUndo;

	int32	i32SelCount = 0;

	for( uint32 i = 0; i < pScene->get_layer_count(); i++ ) {

		LayerC*	pLayer = pScene->get_layer( i );

		if( !pLayer || !(pLayer->get_flags() & ITEM_VISIBLE) || (pLayer->get_flags() & ITEM_LOCKED) ||
			!pLayer->get_timesegment()->is_visible( i32Time ) )
			continue;

		int32	i32TimeOffset = pLayer->get_timesegment()->get_segment_start();

		for( uint32 j = 0; j < pLayer->get_effect_count(); j++ ) {
			EffectI*	pEffect = pLayer->get_effect( j );

			if( !pEffect || !(pEffect->get_flags() & ITEM_VISIBLE) || (pEffect->get_flags() & ITEM_LOCKED) ||
				!pEffect->get_timesegment()->is_visible( i32Time - i32TimeOffset ) )
				continue;

			if( pEffect->get_flags() & ITEM_SELECTED ) {
				int32	i32ParamTime = i32Time - (i32TimeOffset + pEffect->get_timesegment()->get_segment_start());

				ParamVector2C*	pParamMove = (ParamVector2C*)pEffect->get_default_param( DEFAULT_PARAM_POSITION );

				if( pParamMove ) {
					Vector2C	rPos;
					pOldUndo = pParamMove->begin_editing( pUndo );
					pParamMove->get_val( i32ParamTime, rPos );
					rPos[0] += m_f32X;
					rPos[1] += m_f32Y;
					pDoc->HandleParamNotify( pParamMove->set_val( i32ParamTime, rPos ) );	// create key at the timecursor time.
					pParamMove->end_editing( pOldUndo );
				}
				i32SelCount++;
			}
		}
	}
	
	if( i32SelCount ) {
		pDoc->GetUndoManager()->push( pUndo );
		pDoc->SetModifiedFlag();
	}
	else
		delete pUndo;

	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}

BOOL CMoveDlg::Create( CWnd* pParentWnd ) 
{
	return CDialog::Create(IDD, pParentWnd);
}

BOOL CMoveDlg::PreTranslateMessage(MSG* pMsg) 
{
	if( pMsg->message == WM_KEYDOWN ) {
		if( (int)pMsg->wParam == VK_RETURN ) {
			OnApply();
			return TRUE; 
		}
		else if( (int)pMsg->wParam == VK_ESCAPE )
			return TRUE;
	}

	return CDialog::PreTranslateMessage(pMsg);
}

BOOL CMoveDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if( !m_rSpinnerX.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
		return FALSE;
	m_rSpinnerX.SetScale( 1 );
	m_rSpinnerX.SetBuddy( &m_rEditX, SPNB_ATTACH_RIGHT );

	if( !m_rSpinnerY.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER2 ) )
		return FALSE;
	m_rSpinnerY.SetScale( 1 );
	m_rSpinnerY.SetBuddy( &m_rEditY, SPNB_ATTACH_RIGHT );
	
	return TRUE;
}
