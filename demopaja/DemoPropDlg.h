#if !defined(AFX_DEMOPROPDLG_H__BA435EA4_0077_4375_8B59_0DCB4C9D89A9__INCLUDED_)
#define AFX_DEMOPROPDLG_H__BA435EA4_0077_4375_8B59_0DCB4C9D89A9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DemoPropDlg.h : header file
//

#include "PrefSubDlg.h"
#include <afxtempl.h>


/////////////////////////////////////////////////////////////////////////////
// CDemoPropDlg dialog


class PageStructC
{
public:
   CPrefSubDlg*	pDlg;
   UINT			id;
   CPrefSubDlg*	pDlgParent;
   CString		csCaption;
};

#define WM_CHANGE_PAGE (WM_APP + 100)

class CDemoPropDlg : public CDialog
{
// Construction
public:
	CDemoPropDlg(CWnd* pParent = NULL);   // standard constructor
	~CDemoPropDlg();


	// add a page (page, page title, optional parent)
	bool	AddPage( CPrefSubDlg& page, const char *pCaption, CPrefSubDlg* pDlgParent = NULL );
	bool	ShowPage( int iPage );
	bool	ShowPage( CPrefSubDlg* pPage );



// Dialog Data
	//{{AFX_DATA(CDemoPropDlg)
	enum { IDD = IDD_DEMOPROPERTIES };
	CTreeCtrl	m_rPageTree;
	CStatic	m_rStaticBoundingFrame;
	CStatic	m_rStaticTitle;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDemoPropDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	//}}AFX_VIRTUAL

// Implementation
protected:

	CFont	m_rFont;

	// check to see if this dlg has already been added to the tree
	HTREEITEM	FindHTREEItemForDlg( CPrefSubDlg *pParent );
	
	bool EndOK();

	// Generated message map functions
	//{{AFX_MSG(CDemoPropDlg)
	virtual BOOL OnInitDialog();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnSelchangedPagetree(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnGetdispinfoPagetree(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	afx_msg long OnChangePage(UINT, LONG);
	DECLARE_MESSAGE_MAP()

	CPtrArray		m_pages;
	int				m_iCurPage;
	CRect			m_frameRect;

	CPrefSubDlg*	m_pStartPage;

	// store info about *pDlgs that have been added to 
	// the tree - used for quick lookup of parent nodes
	// DWORDs are used because HTREEITEMs can't be... blame Microsoft
	CMap< CPrefSubDlg *, CPrefSubDlg *, DWORD, DWORD&  > m_dlgMap;

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMOPROPDLG_H__BA435EA4_0077_4375_8B59_0DCB4C9D89A9__INCLUDED_)
