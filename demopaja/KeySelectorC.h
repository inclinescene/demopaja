// KeySelectorC.h: interface for the KeySelectorC class.
//
//////////////////////////////////////////////////////////////////////

#ifndef __KEYSELECTORC_H__
#define __KEYSELECTORC_H__

#include "PajaTypes.h"
#include "DataBlockI.h"
#include "EditableI.h"
#include "LayerC.h"
#include "EffectI.h"
#include "GizmoI.h"
#include "ParamI.h"
#include "UndoC.h"
#include <vector>
#include "FileIO.h"
#include "SceneItemC.h"


struct CommonKeyValuesS {
	PajaTypes::int32	m_i32SelCount;
	PajaTypes::int32	m_i32SmoothCount ;
	PajaTypes::int32	m_i32LinCount;
	PajaTypes::int32	m_i32HoldCount;
	PajaTypes::int32	m_i32Interpolation;
	PajaTypes::int32	m_i32Tens;
	PajaTypes::int32	m_i32Cont;
	PajaTypes::int32	m_i32Bias;
	PajaTypes::int32	m_i32EaseIn;
	PajaTypes::int32	m_i32EaseOut;
};


enum InterpolationPropertyE {
	PROPERTY_TENS = 1,
	PROPERTY_CONT,
	PROPERTY_BIAS,
	PROPERTY_EASE_IN,
	PROPERTY_EASE_OUT,
};


class KeySelectorC : public Edit::EditableI
{
public:
	static KeySelectorC*		create_new();
	virtual Edit::DataBlockI*	create();
	virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
	virtual void				copy( Edit::EditableI* pEditable );
	virtual void				restore( Edit::EditableI* pEditable );

	virtual void				select_key( SceneItemC* pItem, PajaTypes::int32 i32Time );
	virtual void				deselect_key( SceneItemC* pItem, PajaTypes::int32 i32Time );

	virtual void				update_selected_keys( SceneItemC* pItem );
	virtual void				select_all_keys( SceneItemC* pItem );
	virtual void				deselect_all();

	virtual void				save_state_selected();
	virtual void				move_keys( PajaTypes::int32 i32DeltaTime );
	virtual void				delete_selected();

	virtual void				gather_common_values( CommonKeyValuesS* pCommon );

	virtual void				set_interpolation( PajaTypes::uint32 ui32Type );
	virtual void				set_interpolation_property( PajaTypes::uint32 ui32Type, PajaTypes::float32 f32Val );

	virtual PajaTypes::uint32	selected_items_count();
	virtual SceneItemC*			get_selected_item( PajaTypes::uint32 ui32Index );

	virtual bool				select_keys_in_range( SceneItemC* pItem, PajaTypes::int32 i32StartTime, PajaTypes::int32 i32EndTime );

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

protected:
	KeySelectorC();
	KeySelectorC( Edit::EditableI* pOriginal );
	virtual ~KeySelectorC();

private:

	std::vector<SceneItemC>		m_rSelItems;

};

#endif // __KEYSELECTORC_H__
