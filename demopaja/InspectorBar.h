#if !defined(AFX_INSPECTORBAR_H__B4C5B4FA_2A90_11D4_A80C_0000E8D926FD__INCLUDED_)
#define AFX_INSPECTORBAR_H__B4C5B4FA_2A90_11D4_A80C_0000E8D926FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// InspectorBar.h : header file
//

#include "afxole.h"
#include "SizeCBar.h"
#include <vector>
#include "FlatPopupMenu.h"
#include "BtnST.h"
#include "ItemDragTarget.h"

/////////////////////////////////////////////////////////////////////////////
// CInspectorBar window

#ifndef baseCMyBar
#define baseCMyBar CSizingControlBarG
#endif

class CInspectorBar : public baseCMyBar
{
// Construction
public:
	CInspectorBar();
	virtual ~CInspectorBar();

	virtual DROPEFFECT	OnDragOver( COleDataObject* pDataObject, DWORD dwKeyState, CPoint point );
	virtual BOOL		OnDrop( COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point );

	void				UpdateNotify( PajaTypes::uint32 ui32Notify );

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CInspectorBar)
	//}}AFX_VIRTUAL

	Import::FileHandleC*	GetCurrentFolderHandle() const;

	struct CacheItemS {
		PajaTypes::uint32		m_ui32FileIdx;
		PajaTypes::int32		m_i32Y;
		std::string				m_sName;
		std::string				m_sType;
		std::string				m_sRef;
		std::string				m_sInfo;
		std::string				m_sPath;
		PajaTypes::int32		m_i32Pic;
		PajaTypes::int32		m_i32Indent;
		bool					m_bFolder;
		bool					m_bExpanded;
		Import::FileHandleC*	m_pHandle;

		bool	operator<( const CacheItemS& rOther ) const;
	};

	// Generated message map functions
protected:

	void				UpdateList();

	void				OnItemDrag();
	void				DrawHeader( CDC* pDC );

	void				UpdateFileCache();
	void				UpdateFolder( Import::FileHandleC* pRoot, PajaTypes::int32 i32Indent );
	
	void				UpdateScrollRange( bool bUpdatePos = true );
	bool				HandleRButtonDown( CPoint point );
	bool				HandleLButtonDown( CPoint point, bool bDblClick = false );
	PajaTypes::int32	HitTestItem( CPoint rPt, PajaTypes::int32& i32HitItem );
	PajaTypes::int32	HitTestHeader( CPoint rPt );
	bool				FillProceduralMenu( CFlatPopupMenu& rMenu );
	void				DeleteSelected();

	enum HitTestItemFlagsE {
		HITTEST_NONE = 0,
		HITTEST_ITEM,
		HITTEST_ARROW,
	};

	std::vector<CacheItemS>	m_rFileCache;
	bool					m_bFileCacheValid;
	PajaTypes::int32		m_i32FileCacheMaxHeight;
	PajaTypes::int32		m_i32FileCacheMaxIndent;
	PajaTypes::int32		m_i32SelItem;
	CItemDragTarget			m_rDropTarget;

	PajaTypes::int32		m_i32ListOffsetX;
	PajaTypes::int32		m_i32ListOffsetY;

	enum HeaderTrackE {
		HEADER_TRACK_NONE = 0,
		HEADER_TRACK_NAME,
		HEADER_TRACK_TYPE,
		HEADER_TRACK_REF,
		HEADER_TRACK_INFO,
		HEADER_TRACK_PATH,
	};

	PajaTypes::int32		m_i32NameSize;
	PajaTypes::int32		m_i32TypeSize;
	PajaTypes::int32		m_i32RefSize;
	PajaTypes::int32		m_i32InfoSize;
	PajaTypes::int32		m_i32PathSize;
	PajaTypes::int32		m_i32HeaderTrack;
	PajaTypes::int32		m_i32HeaderOrigSize;
	CRect					m_rHeaderRect;
	
	enum ListTrackE {
		LIST_TRACK_NONE = 0,
		LIST_TRACK_ITEM,
	};

	PajaTypes::int32		m_i32ListTrack;
	CRect					m_rListRect;

	CPoint					m_rOrigPt;
	bool					m_bInitialised;


	enum TimerEventE {
		TIMER_SCROLL_UP_INIT = 1,
		TIMER_SCROLL_UP,
		TIMER_SCROLL_DOWN_INIT,
		TIMER_SCROLL_DOWN,
	};

	PajaTypes::uint32		m_ui32TimerID;

	// drag & drop support

	CImageList	m_rFileTypeImages;
	CImageList	m_rFileListImages;
	CImageList	m_rFolderImages;
	CFont		m_rFont;
	CFont		m_rSmallFont;
	CScrollBar	m_rListScrollVert;
	CScrollBar	m_rListScrollHoriz;

	CBitmap				m_rImportBitmap;
	CBitmap				m_rFolderBitmap;
	CBitmap				m_rCreateBitmap;
	CBitmap				m_rSceneBitmap;
	CBitmap				m_rDelBitmap;
	CButtonST			m_rImportButton;
	CButtonST			m_rFolderButton;
	CButtonST			m_rCreateButton;
	CButtonST			m_rSceneButton;
	CButtonST			m_rDelButton;

	HCURSOR		m_hCursor;

	//{{AFX_MSG(CInspectorBar)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnDestroy();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	afx_msg void OnImportFile();
	afx_msg void OnCreateFolder();
	afx_msg void OnCreateFile();
	afx_msg void OnCreateScene();
	afx_msg void OnDeleteFile();
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INSPECTORBAR_H__B4C5B4FA_2A90_11D4_A80C_0000E8D926FD__INCLUDED_)
