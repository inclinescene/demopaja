// TextTypeInDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "demopajadoc.h"
#include "TextTypeInDlg.h"
#include "PajaTypes.h"
#include "ParamI.h"
#include <string>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace PajaTypes;
using namespace Composition;

/////////////////////////////////////////////////////////////////////////////
// CTextTypeInDlg dialog


CTextTypeInDlg::CTextTypeInDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CTextTypeInDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTextTypeInDlg)
	m_sText = _T("");
	m_sInfoText = _T("");
	//}}AFX_DATA_INIT
}


void CTextTypeInDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTextTypeInDlg)
	DDX_Control(pDX, IDOK, m_rOk);
	DDX_Control(pDX, IDCANCEL, m_rCancel);
	DDX_Text(pDX, IDC_EDIT, m_sText);
	DDX_Text(pDX, IDC_INFO, m_sInfoText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTextTypeInDlg, CDialog)
	//{{AFX_MSG_MAP(CTextTypeInDlg)
	ON_EN_CHANGE(IDC_EDIT, OnChangeEdit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTextTypeInDlg message handlers

void CTextTypeInDlg::OnChangeEdit() 
{
	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );
	UpdateData( TRUE );
	pDoc->HandleParamNotify( m_pParam->set_val( m_i32Time, (const char*)m_sText ) );
	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}

BOOL CTextTypeInDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// update dialog data

	std::string	sVal;

	sVal = m_pParam->get_val( m_i32Time );
	m_sText = sVal.c_str();
	UpdateData( FALSE );

	m_rOk.SetFlat( FALSE );
	m_rCancel.SetFlat( FALSE );

	return TRUE;
}
