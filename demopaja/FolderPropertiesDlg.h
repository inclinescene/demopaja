#if !defined(AFX_FOLDERPROPERTIESDLG_H__3B4BF4FE_2100_4558_931C_EB7D6DA05659__INCLUDED_)
#define AFX_FOLDERPROPERTIESDLG_H__3B4BF4FE_2100_4558_931C_EB7D6DA05659__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FolderPropertiesDlg.h : header file
//


#include "ColorPickerCB.h"


/////////////////////////////////////////////////////////////////////////////
// CFolderPropertiesDlg dialog

class CFolderPropertiesDlg : public CDialog
{
// Construction
public:
	CFolderPropertiesDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CFolderPropertiesDlg)
	enum { IDD = IDD_FOLDERPROP };
	CString	m_sName;
	int		m_iColor;
	//}}AFX_DATA

	CColorPickerCB	m_rComboColor;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFolderPropertiesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CFolderPropertiesDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FOLDERPROPERTIESDLG_H__3B4BF4FE_2100_4558_931C_EB7D6DA05659__INCLUDED_)
