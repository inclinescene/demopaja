// demopaja.h : main header file for the DEMOPAJA application
//

#if !defined(AFX_DEMOPAJA_H__B4C5B4E8_2A90_11D4_A80C_0000E8D926FD__INCLUDED_)
#define AFX_DEMOPAJA_H__B4C5B4E8_2A90_11D4_A80C_0000E8D926FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "FactoryC.h"
#include "ClassIdC.h"
#include "resource.h"       // main symbols
#include <string>
#include "GraphicsDeviceI.h"
#include "DeviceContextC.h"

/////////////////////////////////////////////////////////////////////////////
// CDemopajaApp:
// See demopaja.cpp for the implementation of this class
//

enum CurentDirE {
	CURDIR_OPEN = 0,
	CURDIR_SAVE,
	CURDIR_IMPORT,
	CURDIR_EXPORT,
	CURDIR_COLORSWATCH,
	COUNT_CURDIR,
};

class CDemopajaApp : public CWinApp
{
public:
	CDemopajaApp();


	PluginClass::FactoryC*			GetFactory();
	void*							FactoryCreate( const PluginClass::ClassIdC& rClassId );
	const char*						GetPluginDir();
	const char*						GetAppDir();

	PajaSystem::GraphicsDeviceI*	CreateGraphicsDevice();

	const PluginClass::ClassIdC&	GetDefaultGraphicsDeviceId() const;
	void							SetDefaultGraphicsDeviceId( const PluginClass::ClassIdC& rClassId );
	const char*						GetDefaultGraphicsDeviceName() const;
	void							SetDefaultGraphicsDeviceName( const char* szName );

	bool							MusicSystemPresent();

	void							GetMusicSystemConfig( PajaTypes::int32& i32Driver, PajaTypes::int32& i32Device, PajaTypes::int32& i32Mixer, PajaTypes::int32& i32Rate );
	void							SetMusicSystemConfig( PajaTypes::int32 i32Driver, PajaTypes::int32 i32Device, PajaTypes::int32 i32Mixer, PajaTypes::int32 i32Rate );
	void							SetMusicDriver( PajaTypes::int32 i32Driver );
	PajaTypes::int32				GetMusicDeviceCount();
	const char*						GetMusicDeviceName( PajaTypes::int32 i32Index );


	const char*						GetCurrentDir( PajaTypes::int32 i32CurDir );
	void							SetCurrentDir( PajaTypes::int32 i32CurDir, const char* szDir );

	void				PlayPreview( PajaTypes::int32 i32Time, PajaTypes::int32 i32MaxTime, bool bPlayOnce = false );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDemopajaApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
	//{{AFX_MSG(CDemopajaApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

private:
	PluginClass::FactoryC		m_rFactory;
	bool						m_bFMODPresent;
	std::string					m_sPluginPath;
	std::string					m_sAppPath;
	PluginClass::ClassIdC		m_rDefaultGraphicsDeviceId;
	std::string					m_sDefaultGraphicsDeviceName;

	std::string					m_sCurrentDir[COUNT_CURDIR];

	// FMOD settings
	PajaTypes::int32			m_i32FMODDriver;
	PajaTypes::int32			m_i32FMODDevice;
	PajaTypes::int32			m_i32FMODMixer;
	PajaTypes::int32			m_i32FMODRate;
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMOPAJA_H__B4C5B4E8_2A90_11D4_A80C_0000E8D926FD__INCLUDED_)
