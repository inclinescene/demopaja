#if !defined(AFX_DEMOPROPLAYOUT_H__8A99E7BF_A6CC_4DE2_AB3D_022F1852CE93__INCLUDED_)
#define AFX_DEMOPROPLAYOUT_H__8A99E7BF_A6CC_4DE2_AB3D_022F1852CE93__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DemopropLayout.h : header file
//


#include "PrefSubDlg.h"
#include "ColorC.h"

/////////////////////////////////////////////////////////////////////////////
// CDemopropLayout dialog

class CDemopropLayout : public CPrefSubDlg
{
// Construction
public:
	CDemopropLayout(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDemopropLayout)
	enum { IDD = IDD_DEMOPROP_LAYOUT };
	int		m_i32Height;
	int		m_i32Width;
	//}}AFX_DATA

	PajaTypes::ColorC	m_rBGColor;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDemopropLayout)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDemopropLayout)
	afx_msg void OnButtonColor();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMOPROPLAYOUT_H__8A99E7BF_A6CC_4DE2_AB3D_022F1852CE93__INCLUDED_)
