#if !defined(AFX_TRANSFORMDLG_H__A8A32CD9_456A_4872_BB30_78D455EDE19D__INCLUDED_)
#define AFX_TRANSFORMDLG_H__A8A32CD9_456A_4872_BB30_78D455EDE19D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TransformDlg.h : header file
//

#include "MoveDlg.h"
#include "RotateDlg.h"
#include "ScaleDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CTransformDlg dialog

class CTransformDlg : public CDialog
{
// Construction
public:
	CTransformDlg( CWnd* pParent = NULL );   // standard constructor

// Dialog Data
	//{{AFX_DATA(CTransformDlg)
	enum { IDD = IDD_TRANSFORM };
	CTabCtrl	m_rTab;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTransformDlg)
	public:
	virtual BOOL Create( CWnd* pParentWnd = 0 );
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CRect			m_rOldPos;
	CMoveDlg*		m_pMoveDlg;
	CRotateDlg*		m_pRotateDlg;
	CScaleDlg*		m_pScaleDlg;
	CImageList		m_rImageList;

	// Generated message map functions
	//{{AFX_MSG(CTransformDlg)
	afx_msg void OnSelchangeTab(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TRANSFORMDLG_H__A8A32CD9_456A_4872_BB30_78D455EDE19D__INCLUDED_)
