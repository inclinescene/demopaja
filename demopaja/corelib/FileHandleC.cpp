#include "DataBlockI.h"
#include "EditableI.h"
#include "FileHandleC.h"
#include "ImportableI.h"

using namespace Edit;
using namespace PajaTypes;
using namespace Import;
using namespace FileIO;


static
void
TRACE( const char* szFormat, ...  )
{
	char	szMsg[256];
	va_list	rList;
	va_start( rList, szFormat );
	_vsnprintf( szMsg, 255, szFormat, rList );
	va_end( rList );
	OutputDebugString( szMsg );
}

//
// File handle
//

FileHandleC::FileHandleC() :
	m_pImportable( 0 ),
	m_i32RefCount( 0 ),
	m_pParent( 0 ),
	m_i32ParentID( FILEHANDLE_INVALID_ID ),
	m_i32Flags( 0 )
{
	// empty
}

FileHandleC::FileHandleC( EditableI* pOriginal ) :
	EditableI( pOriginal ),
	m_pImportable( 0 ),
	m_i32RefCount( 0 ),
	m_pParent( 0 ),
	m_i32ID( FILEHANDLE_INVALID_ID ),
	m_i32ParentID( FILEHANDLE_INVALID_ID ),
	m_i32Flags( 0 )
{
	// empty
}

FileHandleC::~FileHandleC()
{
	// empty
}


FileHandleC*
FileHandleC::create_new()
{
	return new FileHandleC;
}

DataBlockI*
FileHandleC::create()
{
	return new FileHandleC;
}

DataBlockI*
FileHandleC::create( EditableI* pOriginal )
{
	return new FileHandleC( pOriginal );
}

void
FileHandleC::copy( EditableI* pEditable )
{
	FileHandleC*	pHandle = (FileHandleC*)pEditable;
	m_i32RefCount = pHandle->m_i32RefCount;
	m_pImportable = pHandle->m_pImportable;
	m_pParent = pHandle->m_pParent;
	m_i32Flags = pHandle->m_i32Flags;
	m_sFolderName = pHandle->m_sFolderName;
//	m_i32ID = pHandle->m_i32ID;
//	m_i32ParentID = pHandle->m_i32ParentID;
}

void
FileHandleC::restore( EditableI* pEditable )
{
	FileHandleC*	pHandle = (FileHandleC*)pEditable;
	m_i32RefCount = pHandle->m_i32RefCount;
	m_pImportable = pHandle->m_pImportable;
	m_pParent = pHandle->m_pParent;
	m_i32Flags = pHandle->m_i32Flags;
	m_sFolderName = pHandle->m_sFolderName;
//	m_i32ID = pHandle->m_i32ID;
//	m_i32ParentID = pHandle->m_i32ParentID;
}

PluginClass::SuperClassIdC
FileHandleC::get_base_class_id() const
{
	return BASECLASS_FILEHANDLE;
}

uint32
FileHandleC::update_notify( EditableI* pCaller )
{
	for( std::list<EditableI*>::iterator It = m_lstReferences.begin(); It != m_lstReferences.end(); ++It )
	{
		EditableI*	pEdit = (*It);
		if( pEdit )
			pEdit->update_notify( this );
	}
	return 0;
}

void
FileHandleC::add_reference( EditableI* pEditable )
{
	m_lstReferences.push_back( pEditable );
	m_i32RefCount++;
}

void
FileHandleC::reset_references()
{
	m_lstReferences.clear();
	m_i32RefCount = 0;
}

int32
FileHandleC::get_reference_count()
{
	return m_i32RefCount;
}

FileHandleC*
FileHandleC::get_parent_handle()
{
	return m_pParent;
}

void
FileHandleC::set_parent_handle( FileHandleC* pHandle )
{
	m_pParent = pHandle;
}

int32
FileHandleC::get_parent_id()
{
	return m_i32ParentID;
}

void
FileHandleC::set_parent_id( int32 i32ID )
{
	m_i32ParentID = i32ID;
}

const char*
FileHandleC::get_folder_name() const
{
	return m_sFolderName.c_str();
}

void
FileHandleC::set_folder_name( const char* szName )
{
	m_sFolderName = szName;
}

void
FileHandleC::set_flags( int32 i32Flags )
{
	m_i32Flags = i32Flags;
}

void
FileHandleC::add_flags( int32 i32Flags )
{
	m_i32Flags |= i32Flags;
}

void
FileHandleC::del_flags( int32 i32Flags )
{
	m_i32Flags &= ~i32Flags;
}

void
FileHandleC::toggle_flags( int32 i32Flags )
{
	m_i32Flags ^= i32Flags;
}

int32
FileHandleC::get_flags()
{
	return m_i32Flags;
}

ImportableI*
FileHandleC::get_importable()
{
	return m_pImportable;
}

void
FileHandleC::set_importable( ImportableI* pImportable )
{
	m_pImportable = pImportable;
}

uint32
FileHandleC::get_id()
{
	return m_i32ID;
}

void
FileHandleC::set_id( PajaTypes::uint32 i32ID )
{
	m_i32ID = i32ID;
}

uint32
FileHandleC::save( SaveC* pSave )
{
	return IO_OK;	// we dont save file handles
}

uint32
FileHandleC::load( LoadC* pLoad )
{
	return IO_OK;	// we dont save file handles
}
