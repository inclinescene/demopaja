

#include "DeviceInterfaceI.h"

using namespace PajaTypes;
using namespace PajaSystem;
using namespace PluginClass;

DeviceInterfaceI::DeviceInterfaceI()
{
	// empty
}

DeviceInterfaceI::~DeviceInterfaceI()
{
	// empty
}

DeviceInterfaceI*
DeviceInterfaceI::query_interface( const SuperClassIdC& rSuperClassId )
{
	return 0;
}

bool
DeviceInterfaceI::get_exclusive() const
{
	return false;
}

uint32
DeviceInterfaceI::get_state() const
{
	return m_ui32State;
}

void
DeviceInterfaceI::set_state( uint32 ui32State )
{
	m_ui32State = ui32State;
}

