//-------------------------------------------------------------------------
//
// File:		Import.h
// Desc:		Import namespace header.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_IMPORT_H__
#define __DEMOPAJA_IMPORT_H__

//! The Import namespace
/*!	The classes collected to the Import namespace are used in
	import related tasks.
*/
namespace Import {
	// empty
};

#endif