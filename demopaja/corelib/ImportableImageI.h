//-------------------------------------------------------------------------
//
// File:		ImportableImageI.h
// Desc:		Importable image interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_IMPORTABLEIMAGEI_H__
#define __DEMOPAJA_IMPORTABLEIMAGEI_H__


#include "PajaTypes.h"
#include "EditableI.h"
#include "UndoC.h"
#include "LayerC.h"
#include "ImportableI.h"
#include "DeviceInterfaceI.h"


namespace PluginClass {
	//! The importable super class ID.
	/*! Every importer should return SUPERCLASS_IMAGE as super class if it
		implements the ImportableImageI interface. */
	const PluginClass::SuperClassIdC		SUPERCLASS_IMAGE = PluginClass::SuperClassIdC( 0, 0x2000001 );
};

namespace Import {

	//! Properties for ImportableImageI::bind_texture().
	enum ImagePropertiesE {
		IMAGE_RGB = 1,			//!< Bind as RGB image (if the data is gray scale the bind may use grayscale).
		IMAGE_ALPHA = 2,		//!< Use alpha channel (only applies to one channel images).
		IMAGE_GREY = 4,			//!< Force to grey (only applies to one channel images, may not work).
		IMAGE_WRAP = 8,			//!< Wrap texture texture coordinates.
		IMAGE_CLAMP = 16,		//!< Clamp texture coordinates.
		IMAGE_LINEAR = 32,		//!< Bilinear inperpolation.
		IMAGE_NEAREST = 64,		//!< Point sampling interpolation.
	};

	//! Importable image interface.
	/*!	This interface, derived from ImportableI, implements super class for image
		importers. There are set of methods which enables to get the information of
		the image and to bind it (set as active image) on specified device.

		The dimension of a single pixel in the image is determined from the physical size
		of the image (get_width(), get_heigt()) and the data size of the image
		(get_data_width(), get_data_height()). This enables the image to be
		resized to fit the underlying rendering API an still has correct size
		compared to the original image.
	*/
	class ImportableImageI : public ImportableI
	{
	public:
		//! Returns the super class ID.
		virtual PluginClass::SuperClassIdC		get_super_class_id();

		//! Returns width of the image in pixels.
		virtual PajaTypes::float32	get_width() = 0;
		//! Returns height of the image in pixels.
		virtual PajaTypes::float32	get_height() = 0;
		//! Returns width of the image data in pixels.
		virtual PajaTypes::int32		get_data_width() = 0;
		//! Returns height of the image data in pixels.
		virtual PajaTypes::int32		get_data_height() = 0;
		//! Returns pitch of the image, pitch is the actual length of scanline in pixels.
		virtual PajaTypes::int32		get_data_pitch() = 0;
		//! Returns the testure coordinates of the image.
		virtual PajaTypes::BBox2C&	get_tex_coord_bounds() = 0;
		//! Return bits per pixel.
		virtual PajaTypes::int32		get_data_bpp() = 0;
		//! Returns pointer to the imagedata.
		virtual PajaTypes::uint8*		get_data() = 0;
		//! Binds texture to the given device.
		virtual void								bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Stage, PajaTypes::uint32 ui32Properties ) = 0;

	protected:
		//! Default constructor.
		ImportableImageI();
		//! Constructor with reference to the original.
		ImportableImageI( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~ImportableImageI();

	};

}; // namespace

#endif