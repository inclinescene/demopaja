
#include "ClassIdC.h"
#include "PajaTypes.h"

using namespace PajaTypes;
using namespace PluginClass;


//
// Class
//

ClassIdC::ClassIdC() :
	m_ui32ClassA( 0 ),
	m_ui32ClassB( 0 )
{
	// empty
}

ClassIdC::ClassIdC( uint32 ui32ClassA, uint32 ui32ClassB ) :
	m_ui32ClassA( ui32ClassA ),
	m_ui32ClassB( ui32ClassB )
{
	// empty
}


ClassIdC::ClassIdC( const ClassIdC& rClassId ) :
	m_ui32ClassA( rClassId.m_ui32ClassA ),
	m_ui32ClassB( rClassId.m_ui32ClassB )
{
	// empty
}

ClassIdC::~ClassIdC()
{
	// empty
}


PajaTypes::uint32
ClassIdC::get_class_a() const
{
	return m_ui32ClassA;
}

PajaTypes::uint32
ClassIdC::get_class_b() const
{
	return m_ui32ClassB;
}

bool
ClassIdC::operator==( const ClassIdC& rClassId ) const
{
	return	(m_ui32ClassA == rClassId.m_ui32ClassA &&
			m_ui32ClassB == rClassId.m_ui32ClassB);
}

bool
ClassIdC::operator!=( const ClassIdC& rClassId ) const
{
	return	(m_ui32ClassA != rClassId.m_ui32ClassA ||
			m_ui32ClassB != rClassId.m_ui32ClassB);
}

ClassIdC&
ClassIdC::operator=( const ClassIdC& rClassId )
{
	m_ui32ClassA = rClassId.m_ui32ClassA;
	m_ui32ClassB = rClassId.m_ui32ClassB;
	return *this;
}


//
// Super class
//

SuperClassIdC::SuperClassIdC() :
	m_ui32ClassA( 0 ),
	m_ui32ClassB( 0 )
{
	// empty
}

SuperClassIdC::SuperClassIdC( uint32 ui32ClassA, uint32 ui32ClassB ) :
	m_ui32ClassA( ui32ClassA ),
	m_ui32ClassB( ui32ClassB )
{
	// empty
}


SuperClassIdC::SuperClassIdC( const SuperClassIdC& rClassId ) :
	m_ui32ClassA( rClassId.m_ui32ClassA ),
	m_ui32ClassB( rClassId.m_ui32ClassB )
{
	// empty
}

SuperClassIdC::~SuperClassIdC()
{
	// empty
}


PajaTypes::uint32
SuperClassIdC::get_class_a() const
{
	return m_ui32ClassA;
}

PajaTypes::uint32
SuperClassIdC::get_class_b() const
{
	return m_ui32ClassB;
}

bool
SuperClassIdC::operator==( const SuperClassIdC& rClassId ) const
{
	return (m_ui32ClassA == rClassId.m_ui32ClassA) && (m_ui32ClassB == rClassId.m_ui32ClassB);
}

bool
SuperClassIdC::operator!=( const SuperClassIdC& rClassId ) const
{
	return (m_ui32ClassA != rClassId.m_ui32ClassA) || (m_ui32ClassB != rClassId.m_ui32ClassB);
}

SuperClassIdC&
SuperClassIdC::operator=( const SuperClassIdC& rClassId )
{
	m_ui32ClassA = rClassId.m_ui32ClassA;
	m_ui32ClassB = rClassId.m_ui32ClassB;
	return *this;
}
