
#include "GizmoI.h"
#include "PajaTypes.h"
#include "DataBlockI.h"
#include "ParamI.h"
#include "UndoC.h"
#include "FileIO.h"
#include "Composition.h"
#include <string>
#include <vector>


using namespace PajaTypes;
using namespace Edit;
using namespace Composition;
using namespace FileIO;


GizmoI::GizmoI() :
	m_i32Flags( ITEM_GIZMO ),
	m_pParent( 0 ),
	m_ui32Id( 0 )
{
	// empty
}

GizmoI::GizmoI( EffectI* pParent, uint32 ui32Id ) :
	m_i32Flags( ITEM_GIZMO ),
	m_pParent( pParent ),
	m_ui32Id( ui32Id )
{
	// empty
}

GizmoI::GizmoI( EditableI* pOriginal ) :
	EditableI( pOriginal ),
	m_i32Flags( ITEM_GIZMO )
{
	// empty
}

GizmoI::~GizmoI()
{
	// empty
}

void
GizmoI::copy( EditableI* pEditable )
{
	GizmoI*	pGizmo = (GizmoI*)pEditable;
	m_sName = pGizmo->m_sName;
	m_i32Flags = pGizmo->m_i32Flags;
	m_ui32Id = pGizmo->m_ui32Id;
}

void
GizmoI::restore( EditableI* pEditable )
{
	GizmoI*	pGizmo = (GizmoI*)pEditable;
	m_sName = pGizmo->m_sName;
	m_i32Flags = pGizmo->m_i32Flags;
	m_pParent = pGizmo->m_pParent;
	m_ui32Id = pGizmo->m_ui32Id;
}

PluginClass::SuperClassIdC
GizmoI::get_base_class_id() const
{
	return BASECLASS_GIZMO;
}

uint32
GizmoI::update_notify( EditableI* pCaller )
{
	if( m_pParent )
	{
		return m_pParent->update_notify( pCaller );
	}
	return PARAM_NOTIFY_NONE;
}

uint32
GizmoI::get_id()
{
	return m_ui32Id;
}

void
GizmoI::set_id( uint32 ui32Id )
{
	m_ui32Id = ui32Id;
}

EffectI*
GizmoI::get_parent() const
{
	return m_pParent;
}

void
GizmoI::set_name( const char* szName )
{
	m_sName = szName;
}

const char*
GizmoI::get_name() const
{
	return m_sName.c_str();
}

void
GizmoI::set_flags( int32 i32Flags )
{
	m_i32Flags = i32Flags;
}

void
GizmoI::add_flags( int32 i32Flags )
{
	m_i32Flags |= i32Flags;
}

void
GizmoI::del_flags( int32 i32Flags )
{
	m_i32Flags &= ~i32Flags;
}

void
GizmoI::toggle_flags( int32 i32Flags )
{
	m_i32Flags ^= i32Flags;
}

int32
GizmoI::get_flags()
{
	return m_i32Flags;
}

uint32
GizmoI::save( SaveC* pSave )
{
	uint32	ui32Error = IO_OK;

	// make sure this string fits into the buffer when it's loaded back
	std::string	sStr = m_sName;
	if( sStr.size() > 255 )
		sStr.resize( 255 );

	// name
	ui32Error = pSave->write_str( sStr.c_str() );
	// flags
	ui32Error = pSave->write( &m_i32Flags, sizeof( m_i32Flags ) );

	return ui32Error;
}

uint32
GizmoI::load( LoadC* pLoad )
{
	uint32	ui32Error = IO_OK;
	char	szStr[256];

	// name
	ui32Error = pLoad->read_str( szStr );
	m_sName = szStr;
	// flags
	ui32Error = pLoad->read( &m_i32Flags, sizeof( m_i32Flags ) );

	return IO_OK;
}

