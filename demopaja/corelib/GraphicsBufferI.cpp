//-------------------------------------------------------------------------
//
// File:		GraphicsBufferC.h
// Desc:		Off-screen graphics rendering implementation.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#include "PajaTypes.h"
#include "DeviceInterfaceI.h"
#include "GraphicsBufferI.h"


using namespace PajaTypes;
using namespace PajaSystem;
using namespace PluginClass;
using namespace FileIO;

GraphicsBufferI::GraphicsBufferI()
{
}

GraphicsBufferI::~GraphicsBufferI()
{
}

SuperClassIdC
GraphicsBufferI::get_super_class_id() const
{
	return SUPERCLASS_GRAPHICSBUFFER;
}

uint32
GraphicsBufferI::save( SaveC* pSave )
{
	return IO_OK;
}

uint32
GraphicsBufferI::load( LoadC* pLoad )
{
	return IO_OK;
}
