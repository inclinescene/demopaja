//-------------------------------------------------------------------------
//
// File:		EffectMaskI.h
// Desc:		Mask Effect interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#include "EffectMaskI.h"

using namespace Composition;
using namespace PluginClass;


EffectMaskI::EffectMaskI()
{
	// empty
}

EffectMaskI::EffectMaskI( EditableI* pOriginal ) :
	EffectI( pOriginal )
{
	// empty
}

EffectMaskI::~EffectMaskI()
{
	// empty
}

PluginClass::SuperClassIdC
EffectMaskI::get_super_class_id()
{
	return SUPERCLASS_EFFECT_MASK;
}
