
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include "PajaTypes.h"
#include "BBox2C.h"
#include "ColorC.h"
#include "Vector2C.h"
#include "DeviceInterfaceI.h"
#include "GraphicsDeviceI.h"


using namespace PajaTypes;
using namespace PajaSystem;
using namespace PluginClass;

GraphicsDeviceI::GraphicsDeviceI()
{
}

GraphicsDeviceI::~GraphicsDeviceI()
{
}

SuperClassIdC
GraphicsDeviceI::get_super_class_id() const
{
	return SUPERCLASS_GRAPHICSDEVICE;
}

bool
GraphicsDeviceI::get_exclusive() const
{
	return true;
}
