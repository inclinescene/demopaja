
#include "PajaTypes.h"
#include "EditableI.h"
#include "UndoC.h"
#include "LayerC.h"
#include "ImportableI.h"
#include "ImportableFontI.h"


using namespace Edit;
using namespace PajaTypes;
using namespace Import;
using namespace PluginClass;


SuperClassIdC
ImportableFontI::get_super_class_id()
{
	return SUPERCLASS_FONT;
}

ImportableFontI::ImportableFontI()
{
	// empty
}

ImportableFontI::ImportableFontI( EditableI* pOriginal ) :
	ImportableI( pOriginal )
{
	// empty
}

ImportableFontI::~ImportableFontI()
{
	// empty
}
