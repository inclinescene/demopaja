//-------------------------------------------------------------------------
//
// File:		OutputTrackSegmentC.h
// Desc:		Output track segment class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://moppi.inside.org/demopaja/
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_OUTPUTTRACKSEGMENTC_H__
#define __DEMOPAJA_OUTPUTTRACKSEGMENTC_H__

namespace Composition {
	class OutputTrackSegmentC;
};

#include "DataBlockI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "PajaTypes.h"
#include "FileIO.h"
#include "FileHandleC.h"
#include "OutputTrackC.h"

namespace Composition {

	//! Fflags.
	enum OutputTrackSegmentFlagsE {
		SEGMENT_NOFLAGS =		0,
		SEGMENT_SELECTED =		0x0001,		//!< Key is selected (used only in editor).
		SEGMENT_COLOR_WHITE =	0x010,
		SEGMENT_COLOR_RED =		0x020,
		SEGMENT_COLOR_YELLOW =	0x040,
		SEGMENT_COLOR_GREEN =	0x080,
		SEGMENT_COLOR_CYAN =	0x100,
		SEGMENT_COLOR_BLUE =	0x200,
		SEGMENT_COLOR_MAGENTA =	0x400,
		SEGMENT_COLOR_BLACK =	0x800,
		SEGMENT_COLOR_MASK =	0xff0,
	};


	class OutputTrackSegmentC : public Edit::EditableI
	{
	public:
		//! Create new key with zero no channels.
		static OutputTrackSegmentC*	create_new();
		//! Create new key with zero no channels.
		virtual Edit::DataBlockI*	create();
		//! Creates new file handle (used internally), see Edit::EditableI::create().
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		//! Deep copy from a data block, see Edit::DataBlockI::copy().
		virtual void				copy( Edit::EditableI* pEditable );
		//! Shallow copy from a editable, see Edit::EditableI::restore().
		virtual void				restore( Edit::EditableI* pEditable );

		virtual void					set_filehandle( Import::FileHandleC* pHandle );
		virtual Import::FileHandleC*	get_filehandle();

		//! Returns the start time of the segment.
		virtual PajaTypes::int32	get_start_time();
		//! Sets the start time of the segment.
		virtual void				set_start_time( PajaTypes::int32 i32Time );

		//! Returns the end time of the segment.
		virtual PajaTypes::int32	get_end_time();
		//! Sets the end time of the segment.
		virtual void				set_end_time( PajaTypes::int32 i32Time );

		//! Returns the origo time of the segment.
		virtual PajaTypes::int32	get_origo_time();
		//! Sets the end origo of the segment.
		virtual void				set_origo_time( PajaTypes::int32 i32Time );

		virtual void				set_parent( OutputTrackC* pParent );
		virtual OutputTrackC*		get_parent() const;

		//! Sets the effect flags.
		/*! Be careful to use this method. There are some flags, which
			have to be in place to make the key work correctly.
			Use add, del or toggle flags methods instead.
		*/
		virtual void				set_flags( PajaTypes::int32 i32Flags );
		//! Sets only specified flags.
		virtual void				add_flags( PajaTypes::int32 i32Flags );
		//! Removes only specified flags.
		virtual void				del_flags( PajaTypes::int32 i32Flags );
		//! Toggles only specified flags.
		virtual void				toggle_flags( PajaTypes::int32 i32Flags );
		//! Returns key flags.
		virtual PajaTypes::int32	get_flags();

		//! Serialize the key to a Demopaja output stream.
		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		//! Serialize the key from a Demopaja input stream.
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		OutputTrackSegmentC();
		//! Constructor with reference to the original.
		OutputTrackSegmentC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~OutputTrackSegmentC();

	private:
		PajaTypes::int32		m_i32Flags;
		PajaTypes::int32		m_i32StartTime;
		PajaTypes::int32		m_i32EndTime;
		PajaTypes::int32		m_i32OrigoTime;
		OutputTrackC*			m_pParent;
		Import::FileHandleC*	m_pFile;
	};

};	// namespace Parameter

#endif // __DEMOPAJA_OUTPUTTRACKSEGMENTC_H__
