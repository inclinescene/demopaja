//-------------------------------------------------------------------------
//
// File:		OutputTrackC.h
// Desc:		Output track class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://moppi.inside.org/demopaja/
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_OUTPUTTRACKC_H__
#define __DEMOPAJA_OUTPUTTRACKC_H__

namespace Composition {
	class OutputTrackC;
};

#include "PajaTypes.h"
#include "DataBlockI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "FileIO.h"
#include "SceneC.h"
#include "OutputChannelC.h"
#include "OutputTrackSegmentC.h"
#include <string>
#include <vector>


namespace Composition {

	class OutputTrackC : public Edit::EditableI
	{
	public:
		//! Creates new layer.
		static OutputTrackC*		create_new( OutputChannelC* pParent );
		//! Creates new layer (used internally), see Edit::EditableI::create().
		virtual Edit::DataBlockI*	create();
		//! Creates new layer (used internally), see Edit::EditableI::create().
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		//! Deep copy from a data block, see Edit::DataBlockI::copy().
		virtual void				copy( Edit::EditableI* pEditable );
		//! Shallow copy from a editable, see Edit::EditableI::restore().
		virtual void				restore( Edit::EditableI* pEditable );

		//! Sets the name of the layer\. The name is NULL terminated string.
		void						set_name( const char* szName );
		//! Return the name of the layer.
		const char*					get_name() const;

		//! Return number of effects inside the layer.
		PajaTypes::int32			get_segment_count();
		//! Returns effect at specified index.
		OutputTrackSegmentC*		get_segment( PajaTypes::int32 i32Index );

		//! Adds new effect to the layer.
		PajaTypes::int32			add_segment( OutputTrackSegmentC* pSeg );
		//! Removes and deletes a effect at specified index from the layer.
		void						del_segment( PajaTypes::int32 i32Index );

		//! Removes a effect at specified index from list.
		/*!	This method removes the effect from the list inside the layer, and
			retuns the effect. The effect is not deleted.
			\see del_scene
		*/
		OutputTrackSegmentC*		remove_segment( PajaTypes::int32 i32Index );
		OutputTrackSegmentC*		remove_segment( OutputTrackSegmentC* pSeg );
		//! Adds a effect to list, the change is not stored to the present undo object.
		void						insert_segment( PajaTypes::int32 i32IndexBefore, OutputTrackSegmentC* pSeg );

		void						sort_segments();

		//! Sets the parent Scene.
		void						set_parent( OutputChannelC* pOutCh );
		//! Gets the parent Scene.
		OutputChannelC*				get_parent() const;

		//! Sets the effect flags.
		/*! Be careful to use this method. There are some flags, which
			have to be in place to make the key work correctly.
			Use add, del or toggle flags methods instead.
		*/
		virtual void				set_flags( PajaTypes::int32 i32Flags );
		//! Sets only specified flags.
		virtual void				add_flags( PajaTypes::int32 i32Flags );
		//! Removes only specified flags.
		virtual void				del_flags( PajaTypes::int32 i32Flags );
		//! Toggles only specified flags.
		virtual void				toggle_flags( PajaTypes::int32 i32Flags );
		//! Returns key flags.
		virtual PajaTypes::int32	get_flags();

		//! Serialize the layer to a Demopaja output stream.
		PajaTypes::uint32			save( FileIO::SaveC* pSave );
		//! Serialize the layer from a Demopaja input stream.
		PajaTypes::uint32			load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		OutputTrackC( OutputChannelC* pParent );
		//! Constructor with reference to the original.
		OutputTrackC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~OutputTrackC();

	private:
		std::string				m_sName;
		OutputChannelC*			m_pParent;
		PajaTypes::int32		m_i32Flags;
		std::vector<OutputTrackSegmentC*>	m_rSegments;
	};

};

#endif // __DEMOPAJA_OUTPUTTRACKC_H__
