
#include "PajaTypes.h"
#include "EditableI.h"
#include "UndoC.h"
#include "LayerC.h"
#include "ImportableI.h"
#include "ImportableVideoI.h"


using namespace Edit;
using namespace PajaTypes;
using namespace Import;
using namespace PluginClass;


SuperClassIdC
ImportableVideoI::get_super_class_id()
{
	return SUPERCLASS_VIDEO;
}

ImportableVideoI::ImportableVideoI()
{
	// empty
}

ImportableVideoI::ImportableVideoI( EditableI* pOriginal ) :
	ImportableI( pOriginal )
{
	// empty
}

ImportableVideoI::~ImportableVideoI()
{
	// empty
}
