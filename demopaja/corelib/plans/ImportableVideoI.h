//-------------------------------------------------------------------------
//
// File:		ImportableVideoI.h
// Desc:		Importable video interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://moppi.inside.org/demopaja/
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_IMPORTABLEVIDEOI_H__
#define __DEMOPAJA_IMPORTABLEVIDEOI_H__


#include "PajaTypes.h"
#include "EditableI.h"
#include "UndoC.h"
#include "LayerC.h"
#include "ImportableI.h"
#include "DeviceInterfaceI.h"


namespace PluginClass {
	//! The importable super class ID.
	/*! Every importer should return SUPERCLASS_VIDEO as super class if it
		implements the ImportableVideoI interface. */
	const PluginClass::SuperClassIdC		SUPERCLASS_VIDEO = PluginClass::SuperClassIdC( 0x2000002 );
};

namespace Import {

	//! Importable video interface.
	/*!	This interface, derived from ImportableI, implements super class for video
		importers. There are set of methods which enables to get the information of
		the video and to bind it (set as active image) on specified device.

		The dimension of a single pixel in the image is determined from the physical size
		of the image (get_width(), get_heigt()) and the data size of the image
		(get_data_width(), get_data_height()). This enables the image to be
		resized to fit the underlying rendering API an still has correct size
		compared to the original image.
	*/
	class ImportableVideoI : public ImportableI
	{
	public:
		//! Returns the super class ID.
		virtual PluginClass::SuperClassIdC		get_super_class_id();

		//! Returns width of the image in pixels.
		virtual PajaTypes::float32	get_width() = 0;
		//! Returns height of the image in pixels.
		virtual PajaTypes::float32	get_height() = 0;
		//! Returns width of the image in pixels.
		virtual PajaTypes::int32	get_data_width() = 0;
		//! Returns height of the image in pixels.
		virtual PajaTypes::int32	get_data_height() = 0;
		//! Returns pitch of the image, pitch is the actual length of scanline in pixels.
		virtual PajaTypes::int32	get_data_pitch() = 0;
		//! Return bits per pixel.
		virtual PajaTypes::int32	get_data_bpp() = 0;
		//! Returns pointer to the imagedata.
		virtual PajaTypes::uint8*	get_data() = 0;
		//! Binds texture to the given device.
		/*!	\param i32Frame		The frame of video to use.
			\param pInterface	The device interface to use.
			\param ui32Properties	The properties for bind texture, \see ImagePropertiesE.
		*/
		virtual void				bind_texture( PajaTypes::int32 i32Frame, PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Properties ) = 0;

		//! Returns the first frame number of the video stream.
		virtual PajaTypes::int32	get_first_frame() = 0;
		//! Returns the last frame number of the video stream.
		virtual PajaTypes::int32	get_last_frame() = 0;
		//! Returns number of frames per second of the video stream.
		virtual PajaTypes::float32	get_fps() = 0;

	protected:
		//! Default constructor.
		ImportableVideoI();
		//! Constructor with reference to the original.
		ImportableVideoI( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~ImportableVideoI();

	};

}; // namespace

#endif