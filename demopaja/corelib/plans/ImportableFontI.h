//-------------------------------------------------------------------------
//
// File:		ImportableFontI.h
// Desc:		Importable font interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://moppi.inside.org/demopaja/
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_IMPORTABLEIMAGEI_H__
#define __DEMOPAJA_IMPORTABLEIMAGEI_H__


#include "PajaTypes.h"
#include "EditableI.h"
#include "UndoC.h"
#include "LayerC.h"
#include "ImportableI.h"
#include "DeviceInterfaceI.h"


namespace PluginClass {
	//! The importable super class ID.
	/*! Every importer should return SUPERCLASS_FONT as super class if it
		implements the ImportableFontI interface. */
	const PluginClass::SuperClassIdC		SUPERCLASS_FONT = PluginClass::SuperClassIdC( 0x2000003 );
};

namespace Import {

	enum ImportableFontTypeE {
		FONT_TYPE_BITMAP = 0,	//!< The font is bitmap font.
		FONT_TYPE_VECTOR,		//!< The font is vector font.
	};

	//! Importable font interface.
	/*!	This interface, derived from ImportableI, implements super class for font
		importers. The interface supports both vector and bitmap fonts. Bitmap
	*/
	class ImportableFontI : public ImportableI
	{
	public:
		//! Returns the super class ID.
		virtual PluginClass::SuperClassIdC		get_super_class_id();

		//! Returns width of the image in pixels.
		virtual PajaTypes::int32	get_width() = 0;
		//! Returns height of the image in pixels.
		virtual PajaTypes::int32	get_height() = 0;
		//! Returns pitch of the image, pitch is the actual length of scanline in pixels.
		virtual PajaTypes::int32	get_pitch() = 0;
		//! Return bits per pixel.
		virtual PajaTypes::int32	get_bpp() = 0;
		//! Returns pointer to the imagedata.
		virtual PajaTypes::uint8*	get_data() = 0;
		//! Binds texture to the given device.
		/*! Only IMAGE_LINEAR and IMAGE_NEAREST properties are supported, others are ignored. The image is bind as alpha channel image. */
		virtual void				bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Properties ) = 0;

		virtual PajaTypes::int32		get_glyph_height() const;
		virtual PajaTypes::int32		get_glyph_width( PajaTypes::uint32 ui32Index ) const;
		virtual PajaTypes::int32		get_glyph_advance( PajaTypes::uint32 ui32Index ) const;
		virtual PajaTypes::uint32		get_glyph_vertex_count() const;
		virtual PajaTypes::Vector2C*	get_glyph_vertices( PajaTypes::uint32 ui32Index ) const;
		virtual PajaTypes::uint32		get_glyph_texcoord_count() const;
		virtual PajaTypes::Vector2C*	get_glyph_texcoords( PajaTypes::uint32 ui32Index ) const;

	protected:
		//! Default constructor.
		ImportableFontI();
		//! Constructor with reference to the original.
		ImportableFontI( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~ImportableFontI();

	};

}; // namespace

#endif