//-------------------------------------------------------------------------
//
// File:		OutputChannelC.h
// Desc:		Output channel class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://moppi.inside.org/demopaja/
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_OUTPUTCHANNELC_H__
#define __DEMOPAJA_OUTPUTCHANNELC_H__


namespace Composition {
	class OutputChannelC;
};

#include "DataBlockI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "LayerC.h"
#include "PajaTypes.h"
#include "DeviceContextC.h"
#include "TimeContextC.h"
#include "OutputTrackC.h"
#include "GraphicsBufferI.h"
#include <vector>
#include <string>


namespace Composition {


	enum OutputChannelE {
		OUTPUTCHANNEL_SCREEN	= 0x00010000,
		OUTPUTCHANNEL_GBUFFER	= 0x00020000,
	};


	class OutputChannelC : public Edit::EditableI
	{
	public:
		//! Creates new, empty output channel.
		static OutputChannelC*		create_new( PajaTypes::int32 i32Flags );
		//! Creates new, empty scene.
		virtual Edit::DataBlockI*	create();
		//! Creates new scene (used internally), see Edit::EditableI::create().
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		//! Deep copy from a data block, see Edit::DataBlockI::copy().
		virtual void				copy( Edit::EditableI* pEditable );
		//! Shallow copy from a editable, see Edit::EditableI::restore().
		virtual void				restore( Edit::EditableI* pEditable );


		//! Sets the name of the scene.
		virtual void						set_name( const char* szName );
		//! Returns the name of the scene.
		virtual const char*					get_name() const;

		virtual void							set_graphicsbuffer( PajaSystem::GraphicsBufferI* pBuffer );
		virtual PajaSystem::GraphicsBufferI*	get_graphicsbuffer();

		//! Returns number of layers in the scene.
		virtual PajaTypes::uint32			get_track_count();
		//! Returns a layer at specified index.
		virtual OutputTrackC*				get_track( PajaTypes::uint32 ui32Index );
		//! Adds new layer to the scene.
		virtual OutputTrackC*				add_track();
		//! Deletes a layer at specified index.
		virtual void						del_track( PajaTypes::uint32 ui32Index );

		//! Returns number of beats per minute (BPM) of the demo.
		virtual PajaTypes::int32			get_beats_per_min();
		//! Sets number of beats per minute (BPM) of the demo.
		virtual void						set_beats_per_min( PajaTypes::int32 i32BeatsPerMin );

		//! Returns the edit accuracy of the demo.
		virtual PajaTypes::int32			get_edit_accuracy();
		//! Sets the edit accuracy of the demo.
		virtual void						set_edit_accuracy( PajaTypes::int32 i32Accuracy );

		//! Returns number of beats per measure of the demo.
		virtual PajaTypes::int32			get_beats_per_measure();
		//! Sets number of beats per measure of the demo.
		virtual void						set_beats_per_measure( PajaTypes::int32 i32Beats );

		//! Returns number of quater-notes per beat.
		virtual PajaTypes::int32			get_qnotes_per_beat();
		//! Sets number of quater-notes per beat.
		virtual void						set_qnotes_per_beat( PajaTypes::int32 i32QNotes );


		//! Returns the current time.
		virtual PajaTypes::int32			get_time() const;
		//! Sets the current time.
		virtual void						set_time( PajaTypes::int32 i32Time );
		//! Returns the duration of the demo.
		/*!	The return value is number of quater-notes multiplied by 256.
		*/
		virtual PajaTypes::int32			get_duration();
		//! Sets the duration of the demo.
		virtual void						set_duration( PajaTypes::int32 i32Duration );

		//! Returns the width of the layout in pixels.
		virtual PajaTypes::int32			get_width();
		//! Sets the width of the layout in pixels.
		virtual void						set_width( PajaTypes::int32 i32Width );

		//! Returns the height of the layout in pixels.
		virtual PajaTypes::int32			get_height();
		//! Sets the height of the layout in pixels.
		virtual void						set_height( PajaTypes::int32 i32Height );

		//! Returns the number of markers.
		virtual PajaTypes::uint32			get_marker_count() const;
		//! Returns the time of the specified marker.
		virtual PajaTypes::int32			get_marker_time( PajaTypes::uint32 ui32Index ) const;
		//! Returns the name of the specified marker.
		virtual const char*					get_marker_name( PajaTypes::uint32 ui32Index ) const;

		//! Sets the time of the specified marker
		virtual void						set_marker_time( PajaTypes::uint32 ui32Index, PajaTypes::int32 i32Time );
		//! Sets the name of the specified marker.
		virtual void						set_marker_name( PajaTypes::uint32 ui32Index, const char* szName );

		//! Adds new marker at specified time.
		virtual void						add_marker( PajaTypes::int32 i32Time, const char* szName );
		//! Removes the specified marker.
		virtual void						del_marker( PajaTypes::uint32 ui32Index );
		//! Sorts markers (time).
		virtual void						sort_markers();


		//! Sets the effect flags.
		/*! Be careful to use this method. There are some flags, which
			have to be in place to make the key work correctly.
			Use add, del or toggle flags methods instead.
		*/
		virtual void				set_flags( PajaTypes::int32 i32Flags );
		//! Sets only specified flags.
		virtual void				add_flags( PajaTypes::int32 i32Flags );
		//! Removes only specified flags.
		virtual void				del_flags( PajaTypes::int32 i32Flags );
		//! Toggles only specified flags.
		virtual void				toggle_flags( PajaTypes::int32 i32Flags );
		//! Returns key flags.
		virtual PajaTypes::int32	get_flags();

		//! Serialize the key to a Demopaja output stream.
		virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
		//! Serialize the key from a Demopaja input stream.
		virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		OutputChannelC();
		//! Default constructor.
		OutputChannelC( PajaTypes::int32 i32Flags );
		//! Constructor with reference to the original.
		OutputChannelC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~OutputChannelC();

	private:
		static int	compare_func( const void* vpParam1, const void* vpParam2 );

		struct MarkerS {
			PajaTypes::int32	m_i32Time;
			std::string			m_sName;
		};

		std::vector<MarkerS>		m_rMarkers;
		std::string					m_sName;
		std::vector<OutputTrackC*>	m_rTracks;
		PajaTypes::int32			m_i32BeatsPerMin;
		PajaTypes::int32			m_i32EditAccuracy;
		PajaTypes::int32			m_i32BeatsPerMeasure;
		PajaTypes::int32			m_i32QNotesPerBeat;
		PajaTypes::int32			m_i32Duration;
		PajaTypes::int32			m_i32Width;
		PajaTypes::int32			m_i32Height;
		PajaTypes::int32			m_i32Time;
		PajaTypes::uint32			m_i32Flags;
		PajaSystem::GraphicsBufferI*	m_pGBuffer;
	};

};

#endif	// __DEMOPAJA_OUTPUTCHANNELC_H__