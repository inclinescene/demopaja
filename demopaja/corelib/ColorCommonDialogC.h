//-------------------------------------------------------------------------
//
// File:		ColorCommonDialogC.h
// Desc:		Color common dialog.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_COLORCOMMONDIALOGC_H__
#define __DEMOPAJA_COLORCOMMONDIALOGC_H__

#include <windows.h>
#include "PajaTypes.h"
#include "ColorC.h"
#include "ClassIdC.h"
#include "DataBlockI.h"
#include "CommonDialogI.h"
#include <string>

namespace PajaSystem {

	//! Color common dialog class ID.	
	const PluginClass::ClassIdC		CLASS_COLORCOMMONDIALOG( 0, 1000 );

	//! Color Dialog Class
	/*! Color dialog implements the same color choose dialog
		which is used all over Demopaja.

		Example:
		\code
		...
		ColorC				rColor;
		ColorCommonDialogC*	pDlg = (ColorCommonDialogC*)pImpInterface->get_common_dialog( CLASS_COLORCOMMONDIALOG );
		if( pDlg ) {
			// set the initial color
			pDlg->set_color( rColor );
			// set parent windows handle, important!
			pDlg->set_parent_wnd( hDlg );
			if( pDlg->do_modal() ) {
				// get color
				rColor = pDlg->get_color();
			}
		}
		...
		\endcode
	*/

	class ColorCommonDialogC : public CommonDialogI
	{
	public:

		//! Creates new dialog.
		static ColorCommonDialogC*			create_new();

		//! Creates new dialog.
		virtual Edit::DataBlockI*			create();

		//! Returns the class ID if the dialog.
		virtual PluginClass::ClassIdC		get_class_id();
		//! Prompts the dialog.
		virtual bool						do_modal();

		//! Sets the parent window handle, use before calling do_modal().
		virtual void						set_parent_wnd( HWND hParent );
		//! Sets the caption of the dialog, use before calling do_modal().
		virtual void						set_caption( const char* szName );
		//! Sets the initial color fo the dialog, use before calling do_modal().
		virtual void						set_color( const PajaTypes::ColorC& rCol );
		//! Returns the selected color (use after calling do_modal()).
		virtual const PajaTypes::ColorC&	get_color() const;

	protected:
		ColorCommonDialogC();
		virtual ~ColorCommonDialogC();

		PajaTypes::ColorC	m_rColor;
		std::string			m_sCaption;
		HWND				m_hWndParent;
	};

};

#endif // __DEMOPAJA_COLORCOMMONDIALOGC_H__