
#pragma warning( disable : 4786 )		// long names generated by STL

#include "PajaTypes.h"
#include "DeviceInterfaceI.h"
#include "DeviceContextC.h"
//#include <map>
#include <vector>

using namespace PajaTypes;
using namespace std;
using namespace PajaSystem;
using namespace PluginClass;


DeviceContextC::DeviceContextC()
{
	// empty
}

DeviceContextC::~DeviceContextC()
{
	for( uint32 i = 0; i < m_rInterfaces.size(); i++ )
		m_rInterfaces[i]->release();
	m_rInterfaces.clear();
}

DeviceInterfaceI*
DeviceContextC::query_interface( const ClassIdC& rClassId )
{
	for( uint32 i = 0; i < m_rInterfaces.size(); i++ )
		if( m_rInterfaces[i]->get_class_id() == rClassId )
			return m_rInterfaces[i];

	return 0;
}

DeviceInterfaceI*
DeviceContextC::query_interface( const SuperClassIdC& rSClassId )
{
	for( uint32 i = 0; i < m_rInterfaces.size(); i++ )
		if( m_rInterfaces[i]->get_super_class_id() == rSClassId )
			return m_rInterfaces[i];

	return 0;
}

void
DeviceContextC::register_interface( DeviceInterfaceI* pInterface )
{
	m_rInterfaces.push_back( pInterface );
}

void
DeviceContextC::unregister_interface( const ClassIdC& rClassId )
{
	for( uint32 i = 0; i < m_rInterfaces.size(); i++ ) {
		if( m_rInterfaces[i]->get_class_id() == rClassId ) {
			m_rInterfaces[i]->release();
			m_rInterfaces.erase( m_rInterfaces.begin() + i );
			break;
		}
	}
}

uint32
DeviceContextC::get_interface_count() const
{
	return m_rInterfaces.size();
}

DeviceInterfaceI*
DeviceContextC::get_interface( uint32 ui32Index )
{
	if( ui32Index < m_rInterfaces.size() )
		return m_rInterfaces[ui32Index];
	return 0;
}

