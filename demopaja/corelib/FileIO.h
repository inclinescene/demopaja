//-------------------------------------------------------------------------
//
// File:		FileIO.h
// Desc:		FileIO namespace header.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------


#ifndef __DEMOPAJA_FILEIO_H__
#define __DEMOPAJA_FILEIO_H__

//! Namespace for file IO related classes.
namespace FileIO {

	//! File IO errors.
	/*!	Set of error codes returned from the calls to file IO classes.
	*/
	enum FileIOErrorsE {
		IO_OK = 0,			//!< OK, no error.
		IO_END,				//!< Preliminary end of file.
		IO_EOF,				//!< End of file, no error.
		IO_ERROR_OPEN,		//!< Error opening file.
		IO_ERROR_WRITE,		//!< Error writing to a file.
		IO_ERROR_READ,		//!< Error reading from a file.
		IO_ERROR_FORMAT,	//!< Bad file format.
	};

};

#ifndef FILEIO_DONT_INCLUDE_STREAMS
	#include "LoadC.h"
	#include "SaveC.h"
#endif

#endif