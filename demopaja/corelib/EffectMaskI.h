//-------------------------------------------------------------------------
//
// File:		EffectMaskI.h
// Desc:		Mask Effect interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------


#ifndef __DEMOPAJA_EFFECTMASKI_H__
#define __DEMOPAJA_EFFECTMASKI_H__

// forward declaration
namespace Composition {
	class EffectMaskI;
};

#include "PajaTypes.h"
#include "DataBlockI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "GizmoI.h"
#include "TimeSegmentC.h"
#include "ClassIdC.h"
#include "FileIO.h"
#include "ImportableI.h"
#include "DeviceContextC.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "TimeContextC.h"
#include "FileHandleC.h"
#include "LayerC.h"
#include "EffectI.h"
#include "ImportableImageI.h"
#include <string>
#include <vector>


namespace PluginClass {
	//! The Mask Effect super class ID.
	const PluginClass::SuperClassIdC		SUPERCLASS_EFFECT_MASK = PluginClass::SuperClassIdC( 0x3B1A58FD, 0x21334E1B );
};

namespace Composition {

	//! The Mask effect interface.
	class EffectMaskI : public EffectI
	{
	public:
		//! Returns super class ID of the effect.
		virtual PluginClass::SuperClassIdC	get_super_class_id();

		//! Returns importable image to be used as mask.
		/*!	The texture returned by the method mayalso contain
				color channels.
		*/
		virtual Import::ImportableImageI*		get_mask_image() = 0;

		//! Returns texture flags to be used binding the image.
		virtual PajaTypes::int32						get_mask_image_flags() = 0;

		//! Returns matrix to convertex vertices to texturecoordinates.
		/*! The matrix returned by this method converts the layout coordinates
				to texture coordinates to be used with the mask image returned by the
				get_mask_image().
		*/
		virtual const PajaTypes::Matrix2C&	get_mask_matrix() const = 0;

	protected:
		//! Default constructor.
		EffectMaskI();
		//! Default constructor with reference to the original.
		EffectMaskI( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~EffectMaskI();
	};
};

#endif // __DEMOPAJA_EFFECTMASKI_H__
