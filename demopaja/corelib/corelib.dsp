# Microsoft Developer Studio Project File - Name="corelib" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=corelib - Win32 MFC Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "corelib.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "corelib.mak" CFG="corelib - Win32 MFC Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "corelib - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "corelib - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "corelib - Win32 MFC Debug" (based on "Win32 (x86) Static Library")
!MESSAGE "corelib - Win32 MFC Release" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "corelib - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "F:\Code and Docs\javascript\js-1.5-rc3a\js\src" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /FD /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x40b /d "NDEBUG"
# ADD RSC /l 0x40b /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "corelib - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "F:\Code and Docs\javascript\js-1.5-rc3a\js\src" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x40b /d "_DEBUG"
# ADD RSC /l 0x40b /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "corelib - Win32 MFC Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "MFC Debug"
# PROP BASE Intermediate_Dir "MFC Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "MFC_Debug"
# PROP Intermediate_Dir "MFC_Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "F:\Code and Docs\javascript\js-1.5-rc3a\js\src" /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /FR /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x40b /d "_DEBUG"
# ADD RSC /l 0x40b /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "corelib - Win32 MFC Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "MFC Release"
# PROP BASE Intermediate_Dir "MFC Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 1
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "MFC_Release"
# PROP Intermediate_Dir "MFC_Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "F:\Code and Docs\javascript\js-1.5-rc3a\js\src" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /FD /c
# SUBTRACT CPP /YX
# ADD BASE RSC /l 0x40b /d "NDEBUG"
# ADD RSC /l 0x40b /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "corelib - Win32 Release"
# Name "corelib - Win32 Debug"
# Name "corelib - Win32 MFC Debug"
# Name "corelib - Win32 MFC Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AutoGizmoC.cpp
# End Source File
# Begin Source File

SOURCE=.\BBox2C.cpp
# End Source File
# Begin Source File

SOURCE=.\ClassDescC.cpp
# End Source File
# Begin Source File

SOURCE=.\ClassIdC.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorC.cpp
# End Source File
# Begin Source File

SOURCE=.\CommonDialogI.cpp
# End Source File
# Begin Source File

SOURCE=.\ControllerC.cpp
# End Source File
# Begin Source File

SOURCE=.\DataBlockI.cpp
# End Source File
# Begin Source File

SOURCE=.\DecomposeAffineC.cpp
# End Source File
# Begin Source File

SOURCE=.\DeviceContextC.cpp
# End Source File
# Begin Source File

SOURCE=.\DeviceFeedbackC.cpp
# End Source File
# Begin Source File

SOURCE=.\DeviceInterfaceI.cpp
# End Source File
# Begin Source File

SOURCE=.\DllInterfaceC.cpp
# End Source File
# Begin Source File

SOURCE=.\EditableI.cpp
# End Source File
# Begin Source File

SOURCE=.\EffectI.cpp
# End Source File
# Begin Source File

SOURCE=.\FactoryC.cpp
# End Source File
# Begin Source File

SOURCE=.\FileHandleC.cpp
# End Source File
# Begin Source File

SOURCE=.\FileListC.cpp
# End Source File
# Begin Source File

SOURCE=.\GizmoI.cpp
# End Source File
# Begin Source File

SOURCE=.\GraphicsBufferI.cpp
# End Source File
# Begin Source File

SOURCE=.\GraphicsDeviceI.cpp
# End Source File
# Begin Source File

SOURCE=.\GraphicsViewportI.cpp
# End Source File
# Begin Source File

SOURCE=.\GUIDrawInterfaceI.cpp
# End Source File
# Begin Source File

SOURCE=.\ImageResampleC.cpp
# End Source File
# Begin Source File

SOURCE=.\ImportableI.cpp
# End Source File
# Begin Source File

SOURCE=.\ImportableImageI.cpp
# End Source File
# Begin Source File

SOURCE=.\ImportInterfaceC.cpp
# End Source File
# Begin Source File

SOURCE=.\KeyC.cpp
# End Source File
# Begin Source File

SOURCE=.\LayerC.cpp
# End Source File
# Begin Source File

SOURCE=.\LoadC.cpp
# End Source File
# Begin Source File

SOURCE=.\Matrix2C.cpp
# End Source File
# Begin Source File

SOURCE=.\Matrix3C.cpp
# End Source File
# Begin Source File

SOURCE=.\ParamI.cpp
# End Source File
# Begin Source File

SOURCE=.\QuatC.cpp
# End Source File
# Begin Source File

SOURCE=.\SaveC.cpp
# End Source File
# Begin Source File

SOURCE=.\SceneC.cpp
# End Source File
# Begin Source File

SOURCE=.\TimeContextC.cpp
# End Source File
# Begin Source File

SOURCE=.\TimeSegmentC.cpp
# End Source File
# Begin Source File

SOURCE=.\UndoC.cpp
# End Source File
# Begin Source File

SOURCE=.\Vector2C.cpp
# End Source File
# Begin Source File

SOURCE=.\Vector3C.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AutoGizmoC.h
# End Source File
# Begin Source File

SOURCE=.\BBox2C.h
# End Source File
# Begin Source File

SOURCE=.\ClassDescC.h
# End Source File
# Begin Source File

SOURCE=.\ClassIdC.h
# End Source File
# Begin Source File

SOURCE=.\ColorC.h
# End Source File
# Begin Source File

SOURCE=.\ColorCommonDialogC.h
# End Source File
# Begin Source File

SOURCE=.\CommonDialogI.h
# End Source File
# Begin Source File

SOURCE=.\Composition.h
# End Source File
# Begin Source File

SOURCE=.\ControllerC.h
# End Source File
# Begin Source File

SOURCE=.\DataBlockI.h
# End Source File
# Begin Source File

SOURCE=.\DecomposeAffineC.h
# End Source File
# Begin Source File

SOURCE=.\DemopajaVersion.h
# End Source File
# Begin Source File

SOURCE=.\DeviceContextC.h
# End Source File
# Begin Source File

SOURCE=.\DeviceFeedbackC.h
# End Source File
# Begin Source File

SOURCE=.\DeviceInterfaceI.h
# End Source File
# Begin Source File

SOURCE=.\DllInterfaceC.h
# End Source File
# Begin Source File

SOURCE=.\Edit.h
# End Source File
# Begin Source File

SOURCE=.\EditableI.h
# End Source File
# Begin Source File

SOURCE=.\EffectI.h
# End Source File
# Begin Source File

SOURCE=.\FactoryC.h
# End Source File
# Begin Source File

SOURCE=.\FileHandleC.h
# End Source File
# Begin Source File

SOURCE=.\FileIO.h
# End Source File
# Begin Source File

SOURCE=.\FileListC.h
# End Source File
# Begin Source File

SOURCE=.\GizmoI.h
# End Source File
# Begin Source File

SOURCE=.\GraphicsBufferI.h
# End Source File
# Begin Source File

SOURCE=.\GraphicsDeviceI.h
# End Source File
# Begin Source File

SOURCE=.\GraphicsViewportI.h
# End Source File
# Begin Source File

SOURCE=.\GUIDrawInterfaceI.h
# End Source File
# Begin Source File

SOURCE=.\ImageResampleC.h
# End Source File
# Begin Source File

SOURCE=.\Import.h
# End Source File
# Begin Source File

SOURCE=.\ImportableI.h
# End Source File
# Begin Source File

SOURCE=.\ImportableImageI.h
# End Source File
# Begin Source File

SOURCE=.\ImportInterfaceC.h
# End Source File
# Begin Source File

SOURCE=.\KeyC.h
# End Source File
# Begin Source File

SOURCE=.\LayerC.h
# End Source File
# Begin Source File

SOURCE=.\LoadC.h
# End Source File
# Begin Source File

SOURCE=.\Matrix2C.h
# End Source File
# Begin Source File

SOURCE=.\Matrix3C.h
# End Source File
# Begin Source File

SOURCE=.\PajaTypes.h
# End Source File
# Begin Source File

SOURCE=.\ParamI.h
# End Source File
# Begin Source File

SOURCE=.\PluginClass.h
# End Source File
# Begin Source File

SOURCE=.\QuatC.h
# End Source File
# Begin Source File

SOURCE=.\SaveC.h
# End Source File
# Begin Source File

SOURCE=.\SceneC.h
# End Source File
# Begin Source File

SOURCE=.\TimeContextC.h
# End Source File
# Begin Source File

SOURCE=.\TimeSegmentC.h
# End Source File
# Begin Source File

SOURCE=.\UndoC.h
# End Source File
# Begin Source File

SOURCE=.\Vector2C.h
# End Source File
# Begin Source File

SOURCE=.\Vector3C.h
# End Source File
# End Group
# End Target
# End Project
