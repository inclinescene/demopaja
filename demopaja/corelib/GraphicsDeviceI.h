//-------------------------------------------------------------------------
//
// File:		GraphicsDeviceI.h
// Desc:		Graphics device interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_GRAPHICSDEVICEI_H__
#define __DEMOPAJA_GRAPHICSDEVICEI_H__

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

// Forward declaration
namespace PajaSystem {
	class GraphicsDeviceI;
};

#include "PajaTypes.h"
#include "ColorC.h"
#include "DeviceInterfaceI.h"
#include "DataBlockI.h"
#include "ClassIdC.h"
#include "DeviceFeedbackC.h"
#include "GraphicsBufferI.h"

namespace PajaSystem {

	#define DP_END_PREVIEW		(WM_APP + 110)

	//! Graphics device super class ID.
	const PluginClass::SuperClassIdC		SUPERCLASS_GRAPHICSDEVICE = PluginClass::SuperClassIdC( 0, 0x3000001 );


	//! Graphics device creation flags.
	enum GDeviceCreateFlagsE {
		GRAPHICSDEVICE_CREATE_EDITOR_CHILD = 1,	//!< Create a child window, to be used inside the editor.
		GRAPHICSDEVICE_CREATE_WINDOWED,			//!< Create a window, to be used in windowed preview.
		GRAPHICSDEVICE_CREATE_FULLSCREEN,		//!< Create a full screen window for preview.
		GRAPHICSDEVICE_CREATE_CHILD,			//!< Creates a child window which behaves like windowed mode.
	};

	//! Graphics device clear flags.
	enum GDeviceClearFlagsE {
		GRAPHICSDEVICE_COLORBUFFER =	0x01,	//!< Clear color buffer.
		GRAPHICSDEVICE_DEPTHBUFFER =	0x02,	//!< Clear depth buffer.
		GRAPHICSDEVICE_STENCILBUFFER =	0x04,	//!< Clear stencil buffer.
		GRAPHICSDEVICE_ACCUMBUFFER =	0x08,	//!< Clear accum buffer.
		GRAPHICSDEVICE_ALLBUFFERS =		0x0f,	//!< Clear all buffers.
	};

	//! Graphics device clear flags.
	enum GDeviceTempBufFlagsE {
		GRAPHICSDEVICE_TEMPBUF_HALF_SIZE =	0x01,	//!< Get half-size buffer.
		GRAPHICSDEVICE_TEMPBUF_QUARTER_SIZE =	0x02,	//!< Get quarter-size buffer.
		GRAPHICSDEVICE_TEMPBUF_COLOR =	0x04,	//!< Get quarter-size buffer.
		GRAPHICSDEVICE_TEMPBUF_DEPTH =	0x08,	//!< Get quarter-size buffer.
		GRAPHICSDEVICE_TEMPBUF_EXTRA =	0x10,	//!< Get quarter-size buffer.
	};

	//! Graphics device class.
	/*!
	*/
	class GraphicsDeviceI : public DeviceInterfaceI
	{
	public:
		//! Create new graphics device.
		virtual Edit::DataBlockI*				create() = 0;

		//! Returns super class ID.
		virtual PluginClass::SuperClassIdC		get_super_class_id() const;
		//! Returns true, if the device is exclusive.
		/*! If this method returns true, there can be only one instance of this device
			in the device context.
		*/
		virtual bool							get_exclusive() const;

		//! Intialises a created graphics device.
		virtual bool							init( HINSTANCE hInstance, HWND hParent,
													PajaTypes::int32 i32ID, PajaTypes::uint32 ui32Flags,
													PajaTypes::uint32 ui32Width = 0, PajaTypes::uint32 ui32Height = 0,
													PajaTypes::uint32 ui32BPP = 0,
													PajaSystem::DeviceFeedbackC* pFeedback = 0 ) = 0;
		//! Destroys a graphics device.
		virtual void							destroy() = 0;
		//! Flushes the current rendering buffer to screen.
		virtual void							flush() = 0;
		//! Activates this device.
		virtual void							activate() = 0;
		//! Returns the wondow handle of this device.
		virtual HWND							get_hwnd() = 0;
		//! Launches the configuration dialog.
		virtual bool							configure() = 0;

		//! Sets windowed or child device to full screen.
		virtual bool							set_fullscreen( PajaTypes::uint32 ui32Width, PajaTypes::uint32 ui32Height,
													PajaTypes::uint32 ui32BPP = 0 ) = 0;
		//! Sets windowed or child device to back to normal state.
		virtual bool							set_windowed() = 0;

		//! Clears the device.
		virtual void							clear_device( PajaTypes::uint32 ui32Flags,
													const PajaTypes::ColorC& rColor = PajaTypes::ColorC(),
													PajaTypes::float32 f32Depth = 1.0f, PajaTypes::int32 i32Stencil = 0 ) = 0;

		//! Starts drawing block.
		virtual bool							begin_draw() = 0;
		//! Ends drawing block.
		virtual void							end_draw() = 0;

		//! Pushes current state and prepares effect drawing block.
		virtual void							begin_effects() = 0;
		//! Ends effect drawing block.
		virtual void							end_effects() = 0;

		//! Resizes the device viewport.
		virtual void							set_size( PajaTypes::int32 int32X, PajaTypes::int32 int32Y,
													PajaTypes::int32 i32Width, PajaTypes::int32 i32Height ) = 0;

		//! Returns new graphics buffer.
		/*!	Use GraphicsBufferI::init() to initialize the graphics buffer before
			using it.
		*/
		virtual GraphicsBufferI*				create_graphicsbuffer() = 0;

		//! Sets the recommended size of the temp buffers.
		virtual void										set_temp_graphicsbuffer_size( PajaTypes::uint32 ui32Width, PajaTypes::uint32 ui32Height ) = 0;

		//! Returns temporary graphics buffer.
		virtual GraphicsBufferI*				get_temp_graphicsbuffer( PajaTypes::uint32 ui32Flags ) = 0;
		//! Frees temporary graphics buffer.
		virtual void										free_temp_graphicsbuffer( GraphicsBufferI* pGBuf ) = 0;

		//! Sets rendering target.
		/*!	\param pBuffer new rendering target.
			If the pBuffer is NULL, default rendring target is restored.
			The return value may also be NULL. In that case the previous rendering
			target was the main device.
			\returns previous rendering target.
		*/
		virtual GraphicsBufferI*				set_render_target( GraphicsBufferI* pBuffer ) = 0;

		//! Gets current rendering target.
		/*! If the return value is NULL, the rendering is performed to the frame buffer. */
		virtual GraphicsBufferI*				get_render_target() = 0;

	protected:
		GraphicsDeviceI();
		virtual ~GraphicsDeviceI();

	};

};

#endif // __DEMOPAJA_GRAPHICSDEVICEI_H__
