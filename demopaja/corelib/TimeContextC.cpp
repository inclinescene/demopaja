#include "PajaTypes.h"
#include "TimeContextC.h"

using namespace PajaTypes;
using namespace PajaSystem;


TimeContextC::TimeContextC( int32 i32BeatsPerMin, int32 i32QNotesPerBeat, int32 i32EditAccuracy ) :
	m_ui32FrameID( 0 )
{
	m_i32BeatsPerMin = i32BeatsPerMin;
	m_i32QNotesPerBeat = i32QNotesPerBeat;
	m_i32EditAccuracy = i32EditAccuracy;

	m_f64TimeScale = (float64)m_i32BeatsPerMin * (float64)m_i32QNotesPerBeat * (float64)m_i32EditAccuracy / 60.0 * (256.0 / (float64)m_i32EditAccuracy);
	m_f64InvTimeScale = 1.0 / m_f64TimeScale;
}

TimeContextC::~TimeContextC()
{
	// empty
}

float64
TimeContextC::convert_fps_to_time( float64 f64Time, float64 f64FPS )
{
	return (f64Time / f64FPS) * m_f64TimeScale;
}

float64
TimeContextC::convert_time_to_fps( float64 f64FpsTime, float64 f64FPS )
{
	return f64FpsTime / m_f64TimeScale * f64FPS;
}

int32
TimeContextC::get_beats_per_min() const
{
	return m_i32BeatsPerMin;
}

int32
TimeContextC::get_qnotes_per_beat() const
{
	return m_i32QNotesPerBeat;
}

int32
TimeContextC::get_edit_accuracy() const
{
	return m_i32EditAccuracy;
}

uint32
TimeContextC::get_frame_id() const
{
	return m_ui32FrameID;
}

void
TimeContextC::set_frame_id( uint32 ui32ID )
{
	m_ui32FrameID = ui32ID;
}

int32
TimeContextC::get_frame_size_in_ticks() const
{
	return 256 / m_i32EditAccuracy;
}

void
TimeContextC::set_time_settings( int32 i32BeatsPerMin, int32 i32QNotesPerBeat, int32 i32EditAccuracy )
{
	m_i32BeatsPerMin = i32BeatsPerMin;
	m_i32QNotesPerBeat = i32QNotesPerBeat;
	m_i32EditAccuracy = i32EditAccuracy;

	m_f64TimeScale = (float64)m_i32BeatsPerMin * (float64)m_i32QNotesPerBeat * (float64)m_i32EditAccuracy / 60.0 * (256.0 / (float64)m_i32EditAccuracy);
	m_f64InvTimeScale = 1.0 / m_f64TimeScale;
}
