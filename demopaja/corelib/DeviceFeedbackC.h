//-------------------------------------------------------------------------
//
// File:		DeviceFeedbackC.h
// Desc:		Device feedback class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_DEVICEFEEDBACKC_H__
#define __DEMOPAJA_DEVICEFEEDBACKC_H__

#include "PajaTypes.h"
#include "FileListC.h"
#include "SceneC.h"
#include "DeviceContextC.h"

namespace PajaSystem {

	//! Reasons for EffectI::initialize() and ImportableI::initialize().
	enum InitReasonE {
		INIT_INITIAL_UPDATE = 1,		//!< Initial update of the effect, called after effect is created.
		INIT_DEVICE_CHANGED = 2,		//!< The settings of a device has changed (i.e. graphics device).
		INIT_DEVICE_INVALIDATE = 3,		//!< One of the devices has became invalid.
		INIT_DEVICE_VALIDATE = 4,		//!< One of the devices has became valid.
	};


	//! Device Feedback class.
	/*! Device feedback class is used by the device drivers to send messages to the effects
		and importers.
	*/
	class DeviceFeedbackC {
	public:
		DeviceFeedbackC();
		virtual ~DeviceFeedbackC();

		//! Sets the scene to use.
		virtual void	set_scene( Composition::SceneC* pScene );
		//! Sets the file list to use.
		virtual void	set_demo_interface( PajaSystem::DemoInterfaceC* pInterface );

		//! Sends initalize notify to effects and importers.
		virtual void	send_init( PajaTypes::uint32 ui32Reason );

	private:
		Composition::SceneC*		m_pScene;
		PajaSystem::DemoInterfaceC*	m_pDemoInterface;
	};

};

#endif