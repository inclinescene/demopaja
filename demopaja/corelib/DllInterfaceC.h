//-------------------------------------------------------------------------
//
// File:		DllInterfaceC.h
// Desc:		DLL interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------


#ifndef __DEMOPAJA_DLLINTERFACEC_H__
#define __DEMOPAJA_DLLINTERFACEC_H__

#include <windows.h>
#include "PajaTypes.h"
#include "ClassDescC.h"
#include "DemopajaVersion.h"

namespace PluginClass {

	// imported functions
	typedef PajaTypes::int32	(*GetClassdescCountF)();
	typedef ClassDescC*			(*GetClassdescF)( PajaTypes::int32 );
	typedef PajaTypes::int32	(*GetApiVersionF)();
	typedef char*				(*GetDllNameF)();


	//! DLLInterface class
	/*!	This class is used by the system (Factory) to hold all the plugin entries.
	*/
	class DllInterfaceC
	{
	public:
		DllInterfaceC();
		virtual ~DllInterfaceC();

		bool				init( const char* szDllname );
		PajaTypes::int32	get_classdesc_count() const;
		ClassDescC*			get_classdesc( PajaTypes::int32 );
		PajaTypes::int32	get_api_version() const;
		const char*			get_dll_name() const;

	private:
		HINSTANCE			m_hDLL;
		GetClassdescCountF	m_pGetClassdescCount;
		GetClassdescF		m_pGetClassdesc;
		GetApiVersionF		m_pGetApiVersion;
		GetDllNameF			m_pGetDllName;
	};

}; // namespace

#endif // __DEMOPAJA_DLLINTERFACEC_H__