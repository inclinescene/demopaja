
#include "PajaTypes.h"
#include "Vector2C.h"
#include "BBox2C.h"

using namespace PajaTypes;

BBox2C::BBox2C()
{
	// empty
}

BBox2C::BBox2C( const Vector2C& rMin, const Vector2C& rMax ) :
	m_rMin( rMin ), m_rMax( rMax )
{
	// empty
}

BBox2C::BBox2C( const BBox2C& rBBox ) :
	m_rMin( rBBox.m_rMin ), m_rMax( rBBox.m_rMax )
{
	// empty
}

// destructor
BBox2C::~BBox2C()
{
	// empty
}

BBox2C
BBox2C::combine( const BBox2C& rBBox ) const
{
	BBox2C	rRes = normalize();

	if( rBBox[0][0] < rRes.m_rMin[0] ) rRes.m_rMin[0] = rBBox[0][0];
	if( rBBox[0][1] < rRes.m_rMin[1] ) rRes.m_rMin[1] = rBBox[0][1];

	if( rBBox[1][0] > rRes.m_rMax[0] ) rRes.m_rMax[0] = rBBox[1][0];
	if( rBBox[1][1] > rRes.m_rMax[1] ) rRes.m_rMax[1] = rBBox[1][1];

	return rRes;
}

BBox2C
BBox2C::normalize() const
{
	float32	f32Tmp;

	BBox2C	rRes( *this );

	if( rRes.m_rMin[0] > rRes.m_rMax[0] ) {
		f32Tmp = rRes.m_rMin[0];
		rRes.m_rMin[0] = rRes.m_rMax[0];
		rRes.m_rMax[0] = f32Tmp;
	}
	if( rRes.m_rMin[1] > rRes.m_rMax[1] ) {
		f32Tmp = rRes.m_rMin[1];
		rRes.m_rMin[1] = rRes.m_rMax[1];
		rRes.m_rMax[1] = f32Tmp;
	}

	return rRes;
}

BBox2C
BBox2C::trim( const BBox2C& rBBox ) const
{
	// we want to be really sure this bbox is normalised
	BBox2C	rRes = normalize();

	// clamp this bbox to the bbox we use to trim
	if( rRes.m_rMax[0] < rBBox.m_rMin[0] )
		rRes.m_rMax[0] = rRes.m_rMin[0] = rBBox.m_rMin[0];
	if( rRes.m_rMax[1] < rBBox.m_rMin[1] )
		rRes.m_rMax[1] = rRes.m_rMin[1] = rBBox.m_rMin[1];

	if( rRes.m_rMin[0] > rBBox.m_rMax[0] )
		rRes.m_rMax[0] = rRes.m_rMin[0] = rBBox.m_rMax[0];
	if( rRes.m_rMin[1] > rBBox.m_rMax[1] )
		rRes.m_rMax[1] = rRes.m_rMin[1] = rBBox.m_rMax[1];

	if( rRes.m_rMin[0] < rBBox.m_rMin[0] )
		rRes.m_rMin[0] = rBBox.m_rMin[0];
	if( rRes.m_rMin[1] < rBBox.m_rMin[1] )
		rRes.m_rMin[1] = rBBox.m_rMin[1];

	if( rRes.m_rMax[0] > rBBox.m_rMax[0] )
		rRes.m_rMax[0] = rBBox.m_rMax[0];
	if( rRes.m_rMax[1] > rBBox.m_rMax[1] )
		rRes.m_rMax[1] = rBBox.m_rMax[1];

	return rRes;
}
