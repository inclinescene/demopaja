#include "PajaTypes.h"
#include "FileListC.h"
#include "SceneC.h"
#include "DeviceContextC.h"
#include "DeviceFeedbackC.h"

#include "LayerC.h"
#include "EffectI.h"
#include "ImportableI.h"

using namespace Composition;
using namespace Import;
using namespace PajaSystem;
using namespace PajaTypes;


DeviceFeedbackC::DeviceFeedbackC() :
	m_pScene( 0 ),
	m_pDemoInterface( 0 )
{
	// empty
}

DeviceFeedbackC::~DeviceFeedbackC()
{
	// empty
}

void
DeviceFeedbackC::set_scene( SceneC* pScene )
{
	m_pScene = pScene;
}

void
DeviceFeedbackC::set_demo_interface( PajaSystem::DemoInterfaceC* pInterface )
{
	m_pDemoInterface = pInterface;
}

void
DeviceFeedbackC::send_init( uint32 ui32Reason )
{
	if( !m_pScene || !m_pDemoInterface )
		return;

	uint32				i;

	// init effects
	m_pScene->initialize( ui32Reason, m_pDemoInterface );

	FileListC*	pFileList = m_pDemoInterface->get_filelist();

	// init importables
	for( i = 0; i < pFileList->get_file_count(); i++ ) {
		ImportableI*	pImp = pFileList->get_file( i )->get_importable();
		if( !pImp )
			continue;
		pImp->initialize( ui32Reason, m_pDemoInterface );
	}
}
