//-------------------------------------------------------------------------
//
// File:		ImageResampleC.h
// Desc:		Image resample class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------


#ifndef __DEMOPAJA_IMAGERESAMPLEC_H__
#define __DEMOPAJA_IMAGERESAMPLEC_H__

#include "PajaTypes.h"

namespace PajaSystem {

	//! Image Resampler class
	/*!	Image resampler class provides static methods to reample images. There are two
		methods, the first uses nearest resampled pixels, and the second uses bi-linear
		interpolation to decide smooth pixel values.

		Both filters expect that the source and destination data buffers are allocated
		and valid. The Bits per pixel can be 8, 24, or 32.
	*/
	class ImageResampleC
	{
	public:
		ImageResampleC();
		~ImageResampleC();

		//! Nearest interpolation.
		static	bool	resample_nearest( PajaTypes::uint32 ui32Bpp,
								PajaTypes::uint8* pSrcData, PajaTypes::uint32 ui32SrcWidth, PajaTypes::uint32 ui32SrcHeight,
								PajaTypes::uint8* pDstData, PajaTypes::uint32 ui32DstWidth, PajaTypes::uint32 ui32DstHeight );

		//! Bilinear interpolation.
		static	bool	resample_bilinear( PajaTypes::uint32 ui32Bpp,
								PajaTypes::uint8* pSrcData, PajaTypes::uint32 ui32SrcWidth, PajaTypes::uint32 ui32SrcHeight,
								PajaTypes::uint8* pDstData, PajaTypes::uint32 ui32DstWidth, PajaTypes::uint32 ui32DstHeight );

	};

};	// namespace

#endif