//-------------------------------------------------------------------------
//
// File:		AudioSpectrumC.h
// Desc:		Audio spectrum device.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_AUDIOSPECTRUMC_H__
#define __DEMOPAJA_AUDIOSPECTRUMC_H__

#include "PajaTypes.h"
#include "ClassIdC.h"
#include "DataBlockI.h"
#include "FileIO.h"
#include "TimeContextC.h"
#include "DeviceInterfaceI.h"
#include "ClassDescC.h"
#include <vector>

//////////////////////////////////////////////////////////////////////////
//
//  Audio Spectrum Class ID
//

	const PluginClass::ClassIdC	CLASS_AUDIO_SPECTRUM = PluginClass::ClassIdC( 0xE1470378, 0xD51344FF );

//////////////////////////////////////////////////////////////////////////
//
//  Audio Spectrum class descriptor.
//

class AudioSpectrumDescC : public PluginClass::ClassDescC
{
public:
	AudioSpectrumDescC();
	virtual ~AudioSpectrumDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  Audio Spectrum class.
//

class AudioSpectrumC : public PajaSystem::DeviceInterfaceI
{
public:

	static AudioSpectrumC*			create_new();
	//! Create new graphics device.
	virtual Edit::DataBlockI*				create();
	//! Returns super class ID.
	virtual PluginClass::SuperClassIdC		get_super_class_id() const;
	//! Returns class ID.
	virtual PluginClass::ClassIdC			get_class_id() const;
	//! Returns device's class name as NULL terminated string.
	virtual const char*						get_class_name();

	//! Extends device functionality, default implementation returns NULL.
	virtual PajaSystem::DeviceInterfaceI*		query_interface( const PluginClass::SuperClassIdC& rSuperClassId );

	//! Analyzes audio data, and builds the filtered bands.
	virtual bool								analyze( PajaTypes::uint32 ui32NumBands, PajaTypes::float32* pData, PajaTypes::uint32 ui32DataSize, PajaTypes::uint32 ui32DataFreq, PajaSystem::TimeContextC* pTimeContext );
	virtual PajaTypes::uint32		get_band_count() const;
	virtual PajaTypes::uint8*		get_band_data_ptr( PajaTypes::uint32 ui32Band );
	virtual PajaTypes::uint32		get_band_data_size() const;
	virtual PajaTypes::uint32		get_band_data_freq() const;
	virtual void								eval_state( PajaTypes::int32 i32Time, PajaSystem::TimeContextC* pTimeContext );
	virtual PajaTypes::float32	get_band_value( PajaTypes::uint32 ui32Band ) const;

	//! Serialize device settings to a Demopaja output stream.
	virtual PajaTypes::uint32				save( FileIO::SaveC* pSave );
	//! Serialize device settings from a Demopaja input stream.
	virtual PajaTypes::uint32				load( FileIO::LoadC* pLoad );

protected:
	AudioSpectrumC();
	virtual ~AudioSpectrumC();

	void		purge();

	struct BandS
	{
		PajaTypes::uint8*		m_pData;
		PajaTypes::float32	m_f32Freq;
		PajaTypes::float32	m_f32Value;
	};
	std::vector<BandS>		m_vecBands;
	PajaTypes::uint32			m_ui32BandDataSize;

	PajaSystem::TimeContextC*					m_pTimeContext;
};

extern AudioSpectrumDescC			g_rAudioSpectrumDesc;

#endif