//-------------------------------------------------------------------------
//
// File:		EffectValueFieldI.h
// Desc:		Value Field Effect interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#include "EffectValueFieldI.h"

using namespace Composition;
using namespace PluginClass;


EffectValueFieldI::EffectValueFieldI()
{
	// empty
}

EffectValueFieldI::EffectValueFieldI( EditableI* pOriginal ) :
	EffectI( pOriginal )
{
	// empty
}

EffectValueFieldI::~EffectValueFieldI()
{
	// Empty
}

PluginClass::SuperClassIdC
EffectValueFieldI::get_super_class_id()
{
	return SUPERCLASS_EFFECT_VALUEFIELD;
}
