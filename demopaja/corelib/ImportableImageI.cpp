
#include "PajaTypes.h"
#include "EditableI.h"
#include "UndoC.h"
#include "LayerC.h"
#include "ImportableI.h"
#include "ImportableImageI.h"


using namespace Edit;
using namespace PajaTypes;
using namespace Import;
using namespace PluginClass;


SuperClassIdC
ImportableImageI::get_super_class_id()
{
	return SUPERCLASS_IMAGE;
}

ImportableImageI::ImportableImageI()
{
	// empty
}

ImportableImageI::ImportableImageI( EditableI* pOriginal ) :
	ImportableI( pOriginal )
{
	// empty
}

ImportableImageI::~ImportableImageI()
{
	// empty
}
