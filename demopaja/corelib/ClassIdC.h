//-------------------------------------------------------------------------
//
// File:		ClassIdC.h
// Desc:		Plugin class ID and super class ID classes.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_CLASSIDC_H__
#define __DEMOPAJA_CLASSIDC_H__

#include "PajaTypes.h"

namespace PluginClass {

	//!	Plugin Class ID
	/*!	This class represents the unique class ID for a Demopaja plugin class.
		A Class ID consists of two 32-bit numbers. The class ID must be unique,
		since plugin classes are identified by it. To avoid any conflicts always
		generate random class ID for each plugin class you use. You should use
		the Class ID generator (classidgen.exe) provided with this documentation.
		A class ID with two zero numbers is a NULL class ID, and is not a valid ID. 

		Class IDs which has zero first (A) part are reserved to be used only by
		the Demopaja development team. If you use one of the sample plugins as
		basis of your own plugin remember to change the class IDs! 

		This class is implemented by the system.
	*/
	class ClassIdC  
	{
	public:
		//! Default constructor, assigns each number to zero (NULL class ID). 
		ClassIdC();

		//! Creates a new class ID with the values assigned from the arguments. 
		ClassIdC( PajaTypes::uint32 ui32ClassA, PajaTypes::uint32 ui32ClassB );

		//! Copy constructor, creates a new class ID with the same values as the argument. 
		ClassIdC( const ClassIdC& rClassId );

		//! Default Destructor
		virtual ~ClassIdC();

		//! Returns the first part of the class ID. 
		PajaTypes::uint32	get_class_a() const;

		//! Returns the second part of the class ID. 
		PajaTypes::uint32	get_class_b() const;

		//! Returns true if the class IDs are equal, else false. 
		bool				operator==( const ClassIdC& rClassId ) const;

		//! Returns true if the class IDs are non-equal, else false. 
		bool				operator!=( const ClassIdC& rClassId ) const;

		//! Copy operator.
		ClassIdC&			operator=( const ClassIdC& rClassId );

	private:
		PajaTypes::uint32	m_ui32ClassA, m_ui32ClassB;
	};


	//! Plugin super class ID
	/*!	Super class IDs are system defined constants which describes the class
		the plugin class is derived from. For example all effects are derived
		from EffectI and the plugin class recriptor should return SUPERCLASS_EFFECT
		(defined in EffectI.h). The super class ID is more important in imported
		files where, for example, all image loaders have same interface and the
		effect plugin class can use any type of image to work with. Super class ID
		consists of one 32-bit unsigned integer number. 

		This class is implemented by the system. 
	*/
	class SuperClassIdC
	{
	public:
		//! Default constructor, assigns the number to zero (NULL super class ID).
		SuperClassIdC();

		//! Creates a new super class ID with the same values as the argument. 
		SuperClassIdC( PajaTypes::uint32 ui32ClassA, PajaTypes::uint32 ui32ClassB );

		//! Copy constructor, creates a new super class ID with the same values as the argument. 
		SuperClassIdC( const SuperClassIdC& rClassId );

		//! Default Destructor
		virtual ~SuperClassIdC();

		//! Returns the first part of the class ID. 
		PajaTypes::uint32	get_class_a() const;

		//! Returns the second part of the class ID. 
		PajaTypes::uint32	get_class_b() const;

		//! Returns true if the super class IDs are equal, else false. 
		bool				operator==( const SuperClassIdC& rClassId ) const;

		//! Returns true if the super class IDs are non-equal, else false. 
		bool				operator!=( const SuperClassIdC& rClassId ) const;
		
		//! Copy operator.
		SuperClassIdC&		operator=( const SuperClassIdC& rClassId );

	private:
		PajaTypes::uint32	m_ui32ClassA, m_ui32ClassB;
	};


	//! Super class ID for internal use.
	const SuperClassIdC		SUPERCLASS_INTERNAL = SuperClassIdC( 0, 0x0000001 );
	//! NULL super class ID. (not implemented)
	const SuperClassIdC		NULL_SUPERCLASS = SuperClassIdC( 0, 0x0000000 );
	//! NULL class ID.
	const ClassIdC			NULL_CLASSID = ClassIdC( 0, 0 );

}; // namespace

#endif // __DEMOPAJA_CLASSIDC_H__
