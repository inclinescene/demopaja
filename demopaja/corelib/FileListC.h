//-------------------------------------------------------------------------
//
// File:		FileListC.h
// Desc:		File list class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_FILELISTC_H__
#define __DEMOPAJA_FILELISTC_H__

namespace Import {
	class FileListC;
};

#include "PajaTypes.h"
#include "DataBlockI.h"
#include "EditableI.h"
#include "UndoC.h"
#include <vector>
#include "ImportableI.h"
#include "FileHandleC.h"
#include "FileIO.h"
#include "DemoInterfaceC.h"
#include "DeviceContextC.h"
#include "TimeContextC.h"
#include "ClassIdC.h"

namespace Import {

	//! Base classID.
	const PluginClass::SuperClassIdC		BASECLASS_FILELIST = PluginClass::SuperClassIdC( 1, 4 );

	//! File list class.
	/*! File list class manages the imported files. It checks for duplicates
		enables to load, reload, replace, and delete importable files.
		Changes made to file list are also undoable.
		This class is used internally.

		This class is implemented by the system.
	*/
	class FileListC : public Edit::EditableI
	{
	public:
		//! Create new file list.
		static FileListC*			create_new();
		//! Create new file list.
		virtual Edit::DataBlockI*	create();
		//! Creates new file list(used internally), see Edit::EditableI::create().
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		//! Deep copy from a data block, see Edit::DataBlockI::copy().
		virtual void				copy( Edit::EditableI* pEditable );
		//! Shallow copy from a editable, see Edit::EditableI::restore().
		virtual void				restore( Edit::EditableI* pEditable );
		//! Returns the base class ID.
		virtual PluginClass::SuperClassIdC		get_base_class_id() const;

		//! Loads a file and adds it to the list.
		virtual FileHandleC*		add_file( ImportableI* pImportable, FileHandleC* pFolder );
		virtual FileHandleC*		add_folder( const char* szName, FileHandleC* pFolder );
		//! Returns number of files in the list.
		virtual PajaTypes::uint32	get_file_count();

		//! Reloads a file already in the list.
		/*! This method will create a new instace of the same plugin class
			and attach it to the file handle. The old instance is not used.
			The caller of this method is responsible to update every parameter
			which has the filehandle of this importable points. The Demopaja
			GUI does that every time the reload is requested.
		*/
		virtual void				reload_file( PajaTypes::uint32 ui32Index, PajaSystem::DemoInterfaceC* pIface );
		//! Replaces a file inthe list with another file .
		/*! This method will create a new instace of the same plugin class
			and attach it to the file handle. The old instance is not used.
			The caller of this method is responsible to update every parameter
			which has the filehandle of this importable points. The Demopaja
			GUI does that every time the replace is requested.
		*/
		virtual void				replace_file( PajaTypes::uint32 ui32Index, const char* szName, PajaSystem::DemoInterfaceC* pIface );
		//! Returns the file at given index.
		virtual FileHandleC*		get_file( PajaTypes::uint32 ui32Index );
		//! Returns the file with given name.
		virtual FileHandleC*		get_file( const char* szName, const PluginClass::SuperClassIdC& rSuperFilter, const PluginClass::ClassIdC& rClassFilter );
		//! Returns equal file if exists.
		virtual FileHandleC*		get_equal_file( ImportableI* pImportable );
		//! Deletes the file at given index.
		virtual void				del_file( PajaTypes::uint32 ui32Index );
		//! Deletes the file at given handle.
		virtual void				del_file( FileHandleC* pHandle );
		//! Prompts file properties.
		virtual bool				prompt_properties( PajaTypes::uint32 ui32Index );

		// Serialize editable to a Demopaja output stream.
		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		// Serialize editable from a Demopaja input stream.
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		FileListC();
		//! Default constructor with reference to the original.
		FileListC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~FileListC();

		//! Clears the internal file list, but doesn't delete the content.
		void	clear_list();

	private:
		std::vector<FileHandleC*>	m_rFiles;

	};

}; // namespace

#endif // __DEMOPAJA_FILELISTC_H__