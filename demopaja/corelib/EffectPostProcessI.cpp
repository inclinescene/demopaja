//-------------------------------------------------------------------------
//
// File:		EffectPostProcessI.h
// Desc:		Mask Effect interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#include "EffectPostProcessI.h"

using namespace Composition;
using namespace PluginClass;


EffectPostProcessI::EffectPostProcessI()
{
	// empty
}

EffectPostProcessI::EffectPostProcessI( EditableI* pOriginal ) :
	EffectI( pOriginal )
{
	// empty
}

EffectPostProcessI::~EffectPostProcessI()
{
	// empty
}

PluginClass::SuperClassIdC
EffectPostProcessI::get_super_class_id()
{
	return SUPERCLASS_EFFECT_POST_PROCESS;
}
