#include "PajaTypes.h"
#include "DeviceInterfaceI.h"
#include "Vector2C.h"
#include "BBox2C.h"
#include "GraphicsViewportI.h"


using namespace PajaTypes;
using namespace PajaSystem;
using namespace PluginClass;
using namespace FileIO;


GraphicsViewportI::GraphicsViewportI()
{
	// empty
}

GraphicsViewportI::~GraphicsViewportI()
{
	// empty
}

SuperClassIdC
GraphicsViewportI::get_super_class_id() const
{
	return GRAPHICSDEVICE_VIEWPORT_INTERFACE;
}

uint32
GraphicsViewportI::save( SaveC* pSave )
{
	return IO_OK;
}

uint32
GraphicsViewportI::load( LoadC* pLoad )
{
	return IO_OK;
}
