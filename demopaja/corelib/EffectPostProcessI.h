//-------------------------------------------------------------------------
//
// File:		EffectPostProcessI.h
// Desc:		Post Process Effect interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------


#ifndef __DEMOPAJA_EFFECTPOSTPROCESSI_H__
#define __DEMOPAJA_EFFECTPOSTPROCESSI_H__

// forward declaration
namespace Composition {
	class EffectPostProcessI;
};

#include "PajaTypes.h"
#include "DataBlockI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "GizmoI.h"
#include "TimeSegmentC.h"
#include "ClassIdC.h"
#include "FileIO.h"
#include "ImportableI.h"
#include "DeviceContextC.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "TimeContextC.h"
#include "FileHandleC.h"
#include "LayerC.h"
#include "EffectI.h"
#include "ImportableImageI.h"
#include "GraphicsBufferI.h"
#include <string>
#include <vector>


namespace PluginClass {
	//! The Post Process Effect super class ID.
	const PluginClass::SuperClassIdC		SUPERCLASS_EFFECT_POST_PROCESS = PluginClass::SuperClassIdC( 0x096DADDC, 0x8DB348D9 );
};

namespace Composition {

	//! The Mask effect interface.
	class EffectPostProcessI : public EffectI
	{
	public:
		//! Returns super class ID of the effect.
		virtual PluginClass::SuperClassIdC		get_super_class_id();

		//! Returns render target buffer where the previous effects are rendered.
		virtual PajaSystem::GraphicsBufferI*	get_render_target() = 0;

	protected:
		//! Default constructor.
		EffectPostProcessI();
		//! Default constructor with reference to the original.
		EffectPostProcessI( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~EffectPostProcessI();
	};
};

#endif // __DEMOPAJA_EFFECTPOSTPROCESSI_H__
