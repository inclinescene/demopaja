#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include "PajaTypes.h"
#include "BBox2C.h"
#include "ColorC.h"
#include "Vector2C.h"
#include "GUIDrawInterfaceI.h"

using namespace PajaTypes;
using namespace PajaSystem;
using namespace PluginClass;
using namespace FileIO;


GUIDrawInterfaceI::GUIDrawInterfaceI()
{
}

GUIDrawInterfaceI::~GUIDrawInterfaceI()
{
}

SuperClassIdC
GUIDrawInterfaceI::get_super_class_id() const
{
	return GRAPHICSDEVICE_GUIDRAW_INTERFACE;
}

uint32
GUIDrawInterfaceI::save( SaveC* pSave )
{
	return IO_OK;
}

uint32
GUIDrawInterfaceI::load( LoadC* pLoad )
{
	return IO_OK;
}
