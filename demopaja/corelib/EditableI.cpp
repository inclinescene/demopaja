
#include "DataBlockI.h"
#include "EditableI.h"

using namespace Edit;


EditableI::EditableI() :
	m_pOriginal( 0 ),
	m_pUndo( 0 ),
	m_bAlive( true )
{
	// empty
}

EditableI::EditableI( EditableI* pOriginal ) :
	m_pOriginal( pOriginal ),
	m_pUndo( 0 ),
	m_bAlive( true )
{
	// empty
}

EditableI::~EditableI()
{
	// empty
}

EditableI*
EditableI::get_original() const
{
	return m_pOriginal;
}

EditableI*
EditableI::duplicate()
{
	EditableI*	pEditable = (EditableI*)create();
	pEditable->copy( this );
	return pEditable;
}

EditableI*
EditableI::clone()
{
	EditableI*	pEditable = (EditableI*)create( this );
	pEditable->restore( this );
	return pEditable;
}


// Starts new editing scope. Returns current undo.
UndoC*
EditableI::begin_editing( UndoC* pUndo )
{
	UndoC*	pOldUndo = m_pUndo;
	m_pUndo = pUndo;

	if( pUndo )
		pUndo->add_restore_data( clone() );

	return pOldUndo;
}

// Ends current editing scope. Set old current undo.
void
EditableI::end_editing( UndoC* pUndo )
{
	m_pUndo = pUndo;
}

UndoC*
EditableI::get_undo()
{
	return m_pUndo;
}

PluginClass::SuperClassIdC
EditableI::get_base_class_id() const
{
	return BASECLASS_EDITABLE;
}

PajaTypes::uint32
EditableI::update_notify( EditableI* pCaller )
{
	return 0;
}
