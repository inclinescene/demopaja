//-------------------------------------------------------------------------
//
// File:		ClassDescC.h
// Desc:		Plugin Class Descriptor class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_CLASSDESCC_H__
#define __DEMOPAJA_CLASSDESCC_H__

#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "ClassIdC.h"


namespace PluginClass {


	//! Plugin types.
	enum ClassTypesE {
		CLASS_TYPE_NONE = 0,
		CLASS_TYPE_EFFECT,			//!< Effect plugin.
		CLASS_TYPE_FILEIMPORT,		//!< File import plugin.
		CLASS_TYPE_DEVICEDRIVER,	//!< Device driver plugin.
		CLASS_TYPE_FILEPROCEDURAL,	//!< Procedural file plugin.
		CLASS_TYPE_INTERNAL,		//!< Internal plugin (won't show in any lists).
	};

	//! Plugin Class Descriptor class.
	/*! Class descriptors provide the system information about the
		plugin classes in the DLL. Developer creates a class descriptor
		by deriving a class from ClassDescC and implementing the methods

		This class is implemented by the plugin.
	*/
	class ClassDescC  
	{
	public:

		//! Default constructor.
		ClassDescC();

		//! Default destructor.
		virtual ~ClassDescC();

		//! Demopaja calls this method when it needs new instance of the plugin class.
		/*! The plugin is responds by allocating a new insance of its plugin class.
			The instace should be derived from DataBlockI interface, which has the
			release method for deleting the instance.

			Example Implementation:

			\code
			void*
			TGAImportDescC::create()
			{
				return (void*)TGAImportC::create_new();
			}
			\endcode
		*/
		virtual void*				create() = 0;

		//! Returns the type of the plugin
		/*! The return value can be either CLASS_TYPE_EFFECT or CLASS_TYPE_FILEIMPORT
			depending on the plugin class' type.
			Both values are defined in the ClassDescC.h. 
		*/
		virtual PajaTypes::int32	get_classtype() const = 0;

		//! Returns unique class ID of the plugin class.
		/*! Each plugin class must have unique class ID. The Demopaja system
			will identify the plugin classes (Importers and Effects) by its class ID.
			If two classes conflict the system will use the first class it finds.
			Use the random class ID generator provided with the SDK to generate
			new class IDs to avoid conflicts. This ID is for per class, you don't
			have to create new ID for every instance. 
		*/
		virtual ClassIdC			get_class_id() const = 0;

		//! Returns super class ID of the plugin class.
		/*! This method returns system defined constant which describes
			the class this plugin class is derived from. For example all
			effects returns SUPERCLASS_EFFECT (defined in EffectI.h). 
		*/
		virtual SuperClassIdC		get_super_class_id() const = 0;

		//! Returns the name of the plugin class as NULL terminated string.
		/*! The name of the plugin class should be as descriptive as possible.
			This name is used all over the user interface so make it short.
			Name of below 15 characters is good.

			Example Implementation:

			\code
			const char*
			TGAImportDescC::get_name() const
			{
				return "Targa Image";
			}
			\endcode
		*/
		virtual const char*			get_name() const = 0;

		//! Returns description of the plugin class as NULL terminated string.
		/*!	Example Implementation:

			\code
			const char*
			TGAImportDescC::get_desc() const
			{
				return "Importer for Targa (.TGA) images";
			}
			\endcode
		*/
		virtual const char*			get_desc() const = 0;

		//! Returns the short description of the developer of the plugin class as NULL terminated string.
		/*! Example Implementation:

			\code
			const char*
			TGAImportDescC::get_author_name() const
			{
				return "Mikko \"memon\" Mononen";
			}
			\endcode
		*/
		virtual const char*			get_author_name() const = 0;

		//! Returns copyright message of the plugin class as NULL terminated string.
		/*!	Example Implementation

			\code
			const char*
			TGAImportDescC::get_copyright_message() const
			{
				return "Copyright (c) 2000 Moppi Productions";
			}
			\endcode
		*/
		virtual const char*			get_copyright_message() const = 0;

		//! Returns the URL of the developers homepage (or e-mail address) as NULL terminated string.
		/*! This information will be used in the About Plugins dialog to forward user to the homepage of the plugin. 

			Example Implementation:

			\code
			const char*
			TGAImportDescC::get_url() const
			{
				return "http://www.demopaja.org";
			}
			\endcode
		*/
		virtual const char*			get_url() const = 0;

		//! Returns the path name to the help file of the plugin class.
		/*! This path name will be used to load the help file of a plugin class to
			be shown in the Help Viewer. The help files has to be in HTML format.

			The path is relative to the Demopaja plugin directory.

			Example Implementation:

			\code
			const char*
			TGAImportDescC::get_url() const
			{
				return "TGAImport.html";
			}
			\endcode

		*/
		virtual const char*			get_help_filename() const = 0;

		//! Returns number of file extensions used by the plugin class.
		/*! This information is only used with the importer and exporter plugins.
			Effect plugin class descriptors should return zero. 

			Example Implementation:

			\code
			uint32
			TGAImportDescC::get_ext_count() const
			{
				return 4;
			}
			\endcode
		*/
		virtual PajaTypes::uint32	get_ext_count() const = 0;

		//! Returns the extension of specified index as a NULL terminated string. 
		/*!	\param ui32Index A zero based index of the extension string get get.

			The extension can be any length, but it should not include the leading ".". 

			Example Implementation:

			\code
			const char*
			TGAImportDescC::get_ext( uint32 ui32Index ) const
			{
				if( ui32Index == 0 )
					return "tga";
				else if( ui32Index == 1 )
					return "vda";
				else if( ui32Index == 2 )
					return "icb";
				else if( ui32Index == 3 )
					return "vst";
				return 0;
			}
			\endcode
		*/
		virtual const char*			get_ext( PajaTypes::uint32 ui32Index ) const = 0;

		//! Returns the number of device driver this effect requires.
		/*!
			Example implmentation:

			\code
			uint32
			AVIImportDescC::get_required_device_driver_count() const
			{
				return 1;
			}
			\endcode
		*/
		virtual PajaTypes::uint32				get_required_device_driver_count() const = 0;

		//! Return the Class Id of a required device.
		/*!
			Example implmentation:

			\code
			const ClassIdC&
			AVIImportDescC::get_required_device_driver( uint32 ui32Idx )
			{
				if( u3i2Idx == 0 )
					return PajaSystem::CLASS_OPENGL_DEVICEDRIVER;
				return PajaSystem::NULL_CLASSID;
			}
			\endcode
		*/
		virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx ) = 0;
	};


};	// namespace

#endif // __DEMOPAJA_CLASSDESCC_H__
