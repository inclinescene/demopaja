//-------------------------------------------------------------------------
//
// File:		AutoGizmoC.h
// Desc:		Auto Gizmo class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_AUTOGIZMOC_H__
#define __DEMOPAJA_AUTOGIZMOC_H__

namespace Composition {
	class AutoGizmoC;
};

#include "PajaTypes.h"
#include "DataBlockI.h"
#include "EditableI.h"
#include "ParamI.h"
#include "UndoC.h"
#include "FileIO.h"
#include "EffectI.h"
#include "GizmoI.h"
#include <string>
#include <vector>

namespace Composition {

	//! Auto gizmo class
	/*! Auto gizmo is a class derived from GizmoI interface. Auto gizmo class
		provides an easy way to implement a basic gizmo. This class has methods
		to create a gizmo without writing a new class. New parameters can be added
		and existing deleted. The changes made with add_parameter() and del_parameter()
		methods are not stored into the undo object.
		
		The auto gizmo has to be created in the constructor of the plugin effect
		class. The auto gizmo has to be completely created at the time the auto
		gizmo is serialized.

		The parameters are saved and loaded based on their IDs, so make them unique.

		Example use:
		\code
		PlaaEffect::PlaaEffect()
		{
			// Create Transform gizmo.
			m_pTransGizmo = AutoGizmoC::create_new( this, "Transform", ID_TRANSFORM );
			m_pTransGizmo->add_parameter( ParamVector2C::create_new( this, "Position", Vector2C(), ID_TRANS_POS,
											PARAM_STYLE_EDITBOX | PARAM_STYLE_ABS_POSITION, PARAM_ANIMATABLE ) );
			m_pTransGizmo->add_parameter( ParamVector2C::create_new( this, "Pivot", Vector2C(), ID_TRANS_PIVOT,
											PARAM_STYLE_EDITBOX | PARAM_STYLE_REL_POSITION | PARAM_STYLE_WORLD_SPACE, PARAM_ANIMATABLE );
			m_pTransGizmo->add_parameter( ParamFloatC::create_new( this, "Rotation", 0, ID_TRANS_ROT,
											PARAM_STYLE_EDITBOX | PARAM_STYLE_ANGLE, PARAM_ANIMATABLE, 0, 0, 1.0f );
			m_pTransGizmo->add_parameter( ParamVector2C::create_new( this, "Scale", Vector2C( 1, 1 ), ID_TRANS_SCALE,
											PARAM_STYLE_EDITBOX | PARAM_STYLE_PERCENT, PARAM_ANIMATABLE, Vector2C(), Vector2C(), 0.01f );
		}
		\endcode

		\see Composition::GizmoI
	*/
	class AutoGizmoC : public GizmoI
	{
	public:

		//! Creates new datablock.
		/*!	\param pParent Pointer to the parent effect.
			\param ui32Id ID of the gizmo.
		*/
		static AutoGizmoC*			create_new( EffectI* pParent, const char* szName, PajaTypes::uint32 ui32Id );

		//! Creates new datablock. See DataBlockI.
		virtual Edit::DataBlockI*	create();

		//! Creates new layer (used internally), see Edit::EditableI::create().
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );

		//! Deep copy from a data block, see Edit::DataBlockI::copy().
		virtual void				copy( Edit::EditableI* pEditable );
		//! Shallow copy from a editable, see Edit::EditableI::restore().
		virtual void				restore( Edit::EditableI* pEditable );

		//! Returns number of parameters in the gizmo.
		virtual PajaTypes::int32	get_parameter_count();

		//! Returns parameter at specified index.
		virtual void				add_parameter( ParamI* pParam );

		//! Returns parameter at specified index.
		virtual void				del_parameter( PajaTypes::int32 i32Index );

		//! Returns parameter at specified index.
		virtual ParamI*				get_parameter( PajaTypes::int32 i32Index );

		//! Returns parameter with spcified id.
		/*!	The param ID is the ID set when the parameter is created. */
		virtual ParamI*				get_parameter_by_id( PajaTypes::int32 i32Id );

		//! Serialize the auto gizmo to a Demopaja output stream.
		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );

		//! Serialize the auto gizmo from a Demopaja input stream.
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		AutoGizmoC();
		//! Constructor.
		/*!	\param pParent pointer to the effect which is holds this gizmo.
			\param szName The name of the gizmo.
			\param ui32Id ID of the gizmo, this ID is passed to the update_notify() method.
		*/
		AutoGizmoC( EffectI* pParent, const char* szName, PajaTypes::uint32 ui32Id );
		//! Constructor with reference to the original.
		AutoGizmoC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~AutoGizmoC();

	private:
		std::vector<ParamI*>	m_rParams;
	};

}; // namespace

#endif // __DEMOPAJA_AUTOGIZMOC_H__