//-------------------------------------------------------------------------
//
// File:		PluginClass.h
// Desc:		Plugin Class namespace header.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_PLUGINCLASS_H__
#define __DEMOPAJA_PLUGINCLASS_H__

//! Namespace for plugin class related classes.
namespace PluginClass {
};

#include "ClassDescC.h"
#include "ClassIdC.h"
#include "FactoryC.h"
#include "DllInterfaceI.h"

#endif