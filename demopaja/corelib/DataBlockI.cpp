
#include "DataBlockI.h"

using namespace Edit;

DataBlockI::DataBlockI() :
	m_bAlive( true )
{
	// empty
}

DataBlockI::~DataBlockI()
{
	// empty
}

void
DataBlockI::release()
{
	delete this;
}

void
DataBlockI::set_alive( bool bState )
{
	m_bAlive = bState;
}

bool
DataBlockI::get_alive() const
{
	return m_bAlive;
}
