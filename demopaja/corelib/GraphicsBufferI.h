//-------------------------------------------------------------------------
//
// File:		GraphicsBufferI.h
// Desc:		Off-screen graphics rendering interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_GRAPHICSBUFFERI_H__
#define __DEMOPAJA_GRAPHICSBUFFERI_H__

// Forward declaration
namespace PajaSystem {
	class GraphicsBufferI;
};

#include "PajaTypes.h"
#include "DeviceInterfaceI.h"
#include "DataBlockI.h"
#include "ClassIdC.h"
#include "GraphicsDeviceI.h"

namespace PajaSystem {


	//! Graphics buffer super class ID.
	const PluginClass::SuperClassIdC		SUPERCLASS_GRAPHICSBUFFER = PluginClass::SuperClassIdC( 0, 0x3000004 );

	//! Graphics buffer read_pixels flags.
	enum GBufferReadE {
		GRAPHICSBUFFER_GET_RGB = 1,		//!< Get color pixels.
		GRAPHICSBUFFER_GET_RGBA,		//!< Get color and alpha pixels.
		GRAPHICSBUFFER_GET_ALPHA,		//!< Get alpha pixels.
		GRAPHICSBUFFER_GET_DEPTH,		//!< Get depth pixels.
		GRAPHICSBUFFER_GET_STENCIL,		//!< Get stencil pixels.
	};

	enum GBufferInitE {
		GRAPHICSBUFFER_INIT_OFFSCREEN = 0x01,	//!< The G-Buffer is used for offscreen rendering.
		GRAPHICSBUFFER_INIT_TEXTURE = 0x02,	//!< The G-Buffer is used for render-to-texture operation.
		GRAPHICSBUFFER_INIT_COLOR = 0x04,
		GRAPHICSBUFFER_INIT_DEPTH = 0x08,
		GRAPHICSBUFFER_INIT_EXTRA = 0x10,
	};

	//! Graphics buffer class.
	/*!	To get the dimensions of the graphics buffer, query the GraphicsViewport class from it.
	*/
	class GraphicsBufferI : public DeviceInterfaceI
	{
	public:
		//! Create new graphics device.
		virtual Edit::DataBlockI*				create() = 0;
		//! Sets the owner of the buffer.
		virtual void							set_graphicsdevice( GraphicsDeviceI* pDevice ) = 0;

		//! Returns super class ID.
		virtual PluginClass::SuperClassIdC		get_super_class_id() const;

		virtual PajaTypes::uint32				save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32				load( FileIO::LoadC* pLoad );

		//! Intialises a created graphics buffer.
		/*!	If init() is called for already created buffer, the old contents is
			deleted and new buffer is initialised.

			If flags is GRAPHICSBUFFER_INIT_OFFSCREEN, the G-Buffer is used for
			off-screen rendering.

			If flags is GRAPHICSBUFFER_INIT_TEXTURE, the G-Buffer is used for
			render-to-texture operation and can be bind as a texture.

			
		*/
		virtual bool							init( PajaTypes::uint32 ui32Flags,
													  PajaTypes::uint32 ui32Width = 0,
													  PajaTypes::uint32 ui32Height = 0 ) = 0;
		//! Returns initialisation flags.
		virtual PajaTypes::uint32		get_flags() = 0;
		//! Starts drawing block.
		virtual void							begin_draw() = 0;
		//! Ends drawing block.
		virtual void							end_draw() = 0;
		//! Flushes the rendering buffer.
		virtual void							flush() = 0;

		//! Returns the texture coordinates of the buffer.
		virtual PajaTypes::BBox2C&	get_tex_coord_bounds() = 0;

		//! Uses the graphics buffer as a texture in specified device.
		/*! This method works only if the p-buffer is initialsed with GRAPHICSBUFFER_INIT_TEXTURE flag.
		*/
		virtual void							bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Stage, PajaTypes::uint32 ui32Properties ) = 0;

		//! Get contents of the graphics buffer.
		/*!	The array passed to parameter pData depends on the flags.

			If flags is GRAPHICSBUFFER_GET_RGB, the array is assumed to be
			width * height * 3 * sizeof( uint8 ) bytes in length and the data is
			stored in RGB order, one component in each byte.

			If flags is GRAPHICSBUFFER_GET_RGBA, the array is assumed to be
			width * height * 4 * sizeof( uint8 ) bytes in length and the data is
			stored in RGBA order, one component in each byte.

			If flags is GRAPHICSBUFFER_GET_ALPHA, the array is assumed to be
			width * height * sizeof( uint8 ) bytes in length and the data is
			stored as one alpha value in each byte.

			If flags is GRAPHICSBUFFER_GET_ALPHA, the array is assumed to be
			width * height * sizeof( uint8 ) bytes in length and the data is
			stored as one alpha value in each byte.

			If flags is GRAPHICSBUFFER_GET_DEPTH, the array is assumed to be
			width * height * sizeof( float32 ) bytes in length and the data is
			stored as one depth value in each floating point value.

			If flags is GRAPHICSBUFFER_GET_STENCIL, the array is assumed to be
			width * height * sizeof( uint8 ) bytes in length and the data is
			stored as one stencil value in each byte.
		*/
		virtual void							read_pixels( PajaTypes::uint32 ui32Flags, void* pData ) = 0;

	protected:
		GraphicsBufferI();
		virtual ~GraphicsBufferI();

	};

};	// namespace

#endif // __DEMOPAJA_GRAPHICSBUFFERI_H__
