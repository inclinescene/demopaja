//
//
//

#include "DataBlockI.h"
#include "EditableI.h"
#include "ImportableI.h"
#include <string.h>

using namespace Edit;
using namespace PajaTypes;
using namespace PajaSystem;
using namespace Import;


ImportableI::ImportableI() :
	m_pDemoInterface( 0 ),
	m_pParent( 0 )
{
	// empty
}

ImportableI::ImportableI( EditableI* pOriginal ) :
	EditableI( pOriginal ),
	m_pDemoInterface( 0 ),
	m_pParent( 0 )
{
	// empty
}

ImportableI::~ImportableI()
{
	// empty
}

PluginClass::SuperClassIdC
ImportableI::get_base_class_id() const
{
	return BASECLASS_IMPORT;
}

uint32
ImportableI::get_reference_file_count()
{
	return 0;
}

FileHandleC*
ImportableI::get_reference_file( uint32 ui32Index )
{
	return 0;
}

bool
ImportableI::prompt_properties()
{
	return true;
}

bool
ImportableI::has_properties()
{
	return false;
}

bool
ImportableI::load_file( const char* szName, DemoInterfaceC* pInterface )
{
	return false;
}


bool
ImportableI::create_file( DemoInterfaceC* pInterface )
{
	return false;
}

bool
ImportableI::equals( ImportableI* pImp )
{
	if( pImp->get_class_id() == get_class_id() ) {
		if( strcmp( pImp->get_filename(), get_filename() ) == 0 )
			return true;
	}

	return false;
}

uint32
ImportableI::update_notify( EditableI* pCaller )
{
	// Pass it to the parent
	if( m_pParent )
		return m_pParent->update_notify( this );
	return 0;
}

void
ImportableI::initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface )
{
	if( pInterface )
		m_pDemoInterface = pInterface;
}

void
ImportableI::set_parent( EditableI* pParent )
{
	m_pParent = pParent;
}

EditableI*
ImportableI::get_parent() const
{
	return m_pParent;
}

void
ImportableI::update_references()
{
	uint32	ui32RefCount = get_reference_file_count();
	for( uint32 j = 0; j < ui32RefCount; j++ ) {
		FileHandleC*	pHandle = get_reference_file( j );
		if( pHandle )
			pHandle->add_reference( this );
	}
}
