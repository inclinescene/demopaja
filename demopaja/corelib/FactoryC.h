//-------------------------------------------------------------------------
//
// File:		FactoryC.h
// Desc:		Plugin Class Factory class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_FACTORY_H__
#define __DEMOPAJA_FACTORY_H__

#include "DllInterfaceC.h"
#include "ClassDescC.h"
#include <vector>
#include <string>


namespace PluginClass {

	//! Plugin class factory class.
	/*!	The class factory is used to create plugin classes. Different plugin classes
		are created based on their class IDs. Each plugin class has unique class ID.

		\see ClassIdC
	*/
	class FactoryC
	{
	public:
		//! Default constructor.
		FactoryC();

		//! Default destructor.
		virtual ~FactoryC();

		//! Loads plugins from specified directory (used internally).
		virtual void				load_plugins( const char* szDir );

		//! Registers a plugins class.
		virtual void				register_class( ClassDescC* pClass );

		//! Creates new instance of class specified by class ID and returns the class.
		virtual void*				create( const ClassIdC& rClassId );

		//! Returns number of class descriptors.
		virtual PajaTypes::uint32	get_classdesc_count();

		//! Returns pointer to a class descriptor of specified index.
		virtual ClassDescC*			get_classdesc( PajaTypes::uint32 );

		//! Returns pointer to a class descriptor of specified class ID, NULL id not found.
		virtual ClassDescC*			get_classdesc( const ClassIdC& rClassId );

		//! Returns file name of the DLL where the class descriptor of specified index has loaded.
		virtual const char*			get_classdesc_dll_name( PajaTypes::uint32 );

	private:
		struct DLLClassS {
			ClassDescC*	m_pClassDesc;
			std::string	m_sClassDescDllName;
		};
		std::vector<DllInterfaceC*>	m_rDlls;
		std::vector<DLLClassS>		m_rDLLClasses;
	};

}; // namespace


#endif // __DEMOPAJA_FACTORY_H__