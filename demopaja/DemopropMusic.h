#if !defined(AFX_DEMOPROPMUSIC_H__994ED88E_26AB_485E_A325_6BC8A4A54D42__INCLUDED_)
#define AFX_DEMOPROPMUSIC_H__994ED88E_26AB_485E_A325_6BC8A4A54D42__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DemopropMusic.h : header file
//


#include "PrefSubDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CDemopropMusic dialog

class CDemopropMusic : public CPrefSubDlg
{
// Construction
public:
	CDemopropMusic(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDemopropMusic)
	enum { IDD = IDD_DEMOPROP_MUSIC };
	CComboBox	m_rComboRate;
	CComboBox	m_rComboMixer;
	CComboBox	m_rComboDriver;
	CComboBox	m_rComboDevice;
	CString	m_sMusicFile;
	BOOL	m_bShowWave;
	int		m_i32Device;
	int		m_i32Driver;
	int		m_i32Mixer;
	int		m_i32Rate;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDemopropMusic)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	void	UpdateDeviceList();


	// Generated message map functions
	//{{AFX_MSG(CDemopropMusic)
	afx_msg void OnPickMusic();
	afx_msg void OnSelchangeCombodriver();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMOPROPMUSIC_H__994ED88E_26AB_485E_A325_6BC8A4A54D42__INCLUDED_)
