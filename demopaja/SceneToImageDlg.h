#if !defined(AFX_SCENETOIMAGEDLG_H__40740B7E_A8CD_457E_A8C2_715C63BB2810__INCLUDED_)
#define AFX_SCENETOIMAGEDLG_H__40740B7E_A8CD_457E_A8C2_715C63BB2810__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SceneToImageDlg.h : header file
//

#include "PajaTypes.h"
#include "FileListC.h"
#include "ImportableI.h"
#include "BtnST.h"
#include "SceneC.h"
#include "ColorC.h"
#include "afxwin.h"

/////////////////////////////////////////////////////////////////////////////
// CSceneToImageDlg dialog

class CSceneToImageDlg : public CDialog
{
// Construction
public:
	CSceneToImageDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSceneToImageDlg)
	enum { IDD = IDD_SCENE_TO_IMAGE_DLG };
	CComboBox	m_rComboScene;
	CButtonST	m_rOk;
	CButtonST	m_rCancel;
	CStatic	m_rStaticInfo;
	CString	m_sName;
	int		m_i32FileNum;
	//}}AFX_DATA

	Import::FileHandleC*		m_pHandle;
	Import::FileHandleC*		m_pBufferHandle;
	Import::FileListC*			m_pFileList;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSceneToImageDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	void	UpdateInfo();

	// Generated message map functions
	//{{AFX_MSG(CSceneToImageDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeComboscene();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnCbnSelchangeCombobuffer();
	CComboBox m_rComboBuffer;
	int m_i32BufferFileNum;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCENETOIMAGEDLG_H__40740B7E_A8CD_457E_A8C2_715C63BB2810__INCLUDED_)
