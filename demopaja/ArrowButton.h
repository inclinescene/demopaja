#if !defined(AFX_ARROWBUTTON_H__FF512EA1_B2F9_4730_9D42_A4846504354C__INCLUDED_)
#define AFX_ARROWBUTTON_H__FF512EA1_B2F9_4730_9D42_A4846504354C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ArrowButton.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CArrowButton window

class CArrowButton : public CButton
{
// Construction
public:
	CArrowButton();

	void	SetExpanded( int iState );
	int		GetExpanded() const;

// Attributes
public:


// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CArrowButton)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	protected:
	virtual void PreSubclassWindow();
	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CArrowButton();

	// Generated message map functions
protected:
	//{{AFX_MSG(CArrowButton)
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()

	int		m_iExpanded;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ARROWBUTTON_H__FF512EA1_B2F9_4730_9D42_A4846504354C__INCLUDED_)
