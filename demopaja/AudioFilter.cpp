
#include "PajaTypes.h"
#include "audiofilter.h"
#include <math.h>

CAudioFilter::CAudioFilter() :
	b0( 0 ),
	b1( 0 ),
	b2( 0 ),
	a0( 0 ),
	a1( 0 ),
	a2( 0 ),
	x1( 0 ),
	x2( 0 ),
	y1( 0 ),
	y2( 0 ),
	m_fCutOff( 0 )
{
}

CAudioFilter::~CAudioFilter()
{
}

void
CAudioFilter::Init( float fCutOff, float fSamplingRate )
{
	m_fCutOff = fCutOff;
	float	steep = 0.99f;
  float	r = steep * 0.99609375;
  float	f = cos( M_PI * fCutOff / fSamplingRate );
  a0 = (1 - r) * sqrt( r * (r - 4 * (f * f) + 2) + 1 );
  b1 = 2 * f * r;
  b2 = -(r * r);

	x1 = 0;
	x2 = 0;
}

float
CAudioFilter::Process( float x0 )
{
  float outp = a0 * x0 + b1 * x1 + b2 * x2;
  x2 = x1;
  x1 = outp;

	return outp;
}

float
CAudioFilter::GetCutOff() const
{
	return m_fCutOff;
}
