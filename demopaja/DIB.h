// DIB.h: interface for the CDIB class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DIB_H__AC3FE487_E6D4_11D3_A80C_0000E8D926FD__INCLUDED_)
#define AFX_DIB_H__AC3FE487_E6D4_11D3_A80C_0000E8D926FD__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CDIB  
{
public:
	UINT*		m_bits;
	BITMAPINFO	m_info;
	HBITMAP		m_bitmap;
	CSize		m_size;

	CDIB ();
	virtual ~CDIB ();

	void	WritePixel( UINT x, UINT y, UINT c );


	BOOL	Create( int Width, int Height );
	BOOL	Create( CDC *pDC, UINT uBitmapID );   // From resource bitmap
	void	Destroy();

	void	PasteToDC( CDC *pDC, int x, int y );
	void	PasteToDC( CDC *pDC, int x, int y, int w , int h );
	void	GetFromDC( CDC *pDC, int x, int y, int w, int h );
	void	GetFromBitmap( CDC *pDC, CBitmap *pBitmap );
    
	void	CopyTo( CDIB *Dib );
	void	CopyFrom( const CDIB *Dib );

	UINT&	ColorAt( int x, int y );

	BOOL	operator==( const CDIB & ) const;
	BOOL	operator!=( const CDIB & ) const;
	CDIB&	operator=( const CDIB & );
};



inline void
CDIB::WritePixel( UINT x, UINT y, UINT c )
{
	m_bits[x + y * m_size.cx] = c;
}

inline
CDIB::CDIB ()
{
	m_bits = NULL;
	m_bitmap = NULL;
	m_size = CSize ( 0, 0 );
}

inline
CDIB::~CDIB ()
{
	Destroy ();
}

inline void
CDIB::PasteToDC( CDC *pDC, int x, int y )
{
    // x,y - the DC coordinates to start from
	SetDIBitsToDevice( pDC->m_hDC, x, y, m_size.cx, m_size.cy, 0, 0, 0, m_size.cy, m_bits, &m_info, 0 ); 
}

inline void
CDIB::PasteToDC( CDC *pDC, int x, int y, int w , int h )
{
    // x,y - the DC coordinates to start from
	// w,h - the destination width and height
	StretchDIBits( pDC->m_hDC, x, y, w, h, 0, 0, m_size.cx, m_size.cy, m_bits, &m_info, DIB_RGB_COLORS, SRCCOPY );
}


inline void
CDIB::GetFromBitmap( CDC *pDC, CBitmap *pBitmap )
{
	if( m_bitmap )
		GetDIBits( pDC->m_hDC, HBITMAP(*pBitmap), 0, m_size.cy, m_bits, &(m_info), DIB_RGB_COLORS ); 
}

inline void
CDIB::CopyTo( CDIB *dib )
{
	// If DibSize Wrong Re-Create Dib
	if( (dib->m_size.cx != m_size.cx) || (dib->m_size.cy != m_size.cy) )
		dib->Create( m_size.cx, m_size.cy );
	// do Copy
	memcpy( dib->m_bits, m_bits, m_size.cx * m_size.cy * 4 );
}

inline void
CDIB::CopyFrom( const CDIB *dib )
{
	// If DibSize Wrong Re-Create Dib
	if ( (m_size.cx != dib->m_size.cx) || (m_size.cy != dib->m_size.cy) )
		Create( dib->m_size.cx, dib->m_size.cx );
	// do Paste
	memcpy( m_bits, dib->m_bits, m_size.cx * m_size.cy * 4 );
}

inline UINT &
CDIB::ColorAt(int x, int y)
{
	ASSERT ((x >= 0) && (x <= m_size.cx) &&
			(y >= 0) && (y <= m_size.cy));
	return m_bits[x + y * m_size.cx];
}    

inline BOOL 
CDIB::operator != (const CDIB &dib) const
{
	return !(*this == dib);
}




#endif // !defined(AFX_DIB_H__AC3FE487_E6D4_11D3_A80C_0000E8D926FD__INCLUDED_)
