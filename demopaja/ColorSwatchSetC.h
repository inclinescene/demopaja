#ifndef __DEMOPAJA_COLORSWATCHSETC_H__
#define __DEMOPAJA_COLORSWATCHSETC_H__


#include "PajaTypes.h"
#include "ColorC.h"
#include <vector>
#include "lcms.h"

class ColorSwatchSetC
{
public:
	ColorSwatchSetC();
	virtual ~ColorSwatchSetC();

	//! Initialised the ICM color management
	bool				init_color_management( const char* szCMYKProfile );

	//! Loads the swacthes from photoshop .ACO files
	bool				load_swatches( const char* szFile, bool bAppend = false );
	//! Saves the swathces to a photoshop .ACO file.
	bool				save_swatches( const char* szFile );

	PajaTypes::uint32	get_color_count() const;
	void				add_color( const PajaTypes::ColorC& rCol, PajaTypes::int32 i32InsBefore = -1 );
	void				del_color( PajaTypes::uint32 ui32Idx );
	const PajaTypes::ColorC&	get_color( PajaTypes::uint32 ui32Idx ) const;
	void						set_color( PajaTypes::uint32 ui32Idx, const PajaTypes::ColorC& rCol );

	void				clear_all();

	bool				is_modified() const;

protected:

	void	cmyk_to_rgb( PajaTypes::int32 iC, PajaTypes::int32 iM, PajaTypes::int32 iY, PajaTypes::int32 iK, PajaTypes::int32& iR, PajaTypes::int32& iG, PajaTypes::int32& iB );
	void	hsv_to_rgb( PajaTypes::int32 iH, PajaTypes::int32 iS, PajaTypes::int32 iV, PajaTypes::int32& iR, PajaTypes::int32& iG, PajaTypes::int32& iB );
	void	lab_to_rgb( PajaTypes::int32 iL, PajaTypes::int32 ia, PajaTypes::int32 ib, PajaTypes::int32& iR, PajaTypes::int32& iG, PajaTypes::int32& iB );
	void	swap_endian( PajaTypes::uint16& ui16Data );

	struct ColorSwatchS {
		PajaTypes::ColorC	m_rCol;
		PajaTypes::uint16	m_ui16Data[5];
	};

	cmsHPROFILE					m_hInCMYK;
	cmsHTRANSFORM				m_hTransCMYK;
	cmsHPROFILE					m_hInLab;
	cmsHTRANSFORM				m_hTransLab;
	cmsHPROFILE					m_hOutRGB;
	bool						m_bLCMSIntialised;
	bool						m_bModified;
	std::vector<ColorSwatchS>	m_rColors;
};



#endif