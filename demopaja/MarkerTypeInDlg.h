#if !defined(AFX_MARKERTYPEINDLG_H__BA056DEF_B811_437C_A95A_2E13D238C804__INCLUDED_)
#define AFX_MARKERTYPEINDLG_H__BA056DEF_B811_437C_A95A_2E13D238C804__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MarkerTypeInDlg.h : header file
//

#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "BtnST.h"
#include "ColorPickerCB.h"
#include "afxwin.h"

/////////////////////////////////////////////////////////////////////////////
// CMarkerTypeInDlg dialog

class CMarkerTypeInDlg : public CDialog
{
// Construction
public:
	CMarkerTypeInDlg(CWnd* pParent = NULL);   // standard constructor

	PajaTypes::int32			m_i32BeatsPerMin;
	PajaTypes::int32			m_i32QNotesPerBeat;
	PajaTypes::int32			m_i32EditAccuracy;

// Dialog Data
	//{{AFX_DATA(CMarkerTypeInDlg)
	enum { IDD = IDD_MARKER_TYPEIN };
	CButtonST	m_rOk;
	CButtonST	m_rCancel;
	CEdit	m_rEditTime;
	CStatic	m_rStaticTimeInfo;
	CString	m_sName;
	int		m_i32Time;
	//}}AFX_DATA

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMarkerTypeInDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CString	FormatTime( PajaTypes::int32 i32Time );

	CSpinnerButton		m_rSpinnerTime;

	// Generated message map functions
	//{{AFX_MSG(CMarkerTypeInDlg)
	afx_msg void OnChangeTime();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CColorPickerCB m_rComboColor;
	int m_i32Color;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MARKERTYPEINDLG_H__BA056DEF_B811_437C_A95A_2E13D238C804__INCLUDED_)
