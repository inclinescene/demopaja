#if !defined(AFX_DLLINFODLG_H__82B5EEC8_3697_4558_8A17_A84B4316AA99__INCLUDED_)
#define AFX_DLLINFODLG_H__82B5EEC8_3697_4558_8A17_A84B4316AA99__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DLLInfoDlg.h : header file
//

#include "demopaja.h"

/////////////////////////////////////////////////////////////////////////////
// CDLLInfoDlg dialog

class CDLLInfoDlg : public CDialog
{
// Construction
public:
	CDLLInfoDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDLLInfoDlg)
	enum { IDD = IDD_DLL_INFO };
	CEdit	m_rEditDLL;
	CStatic	m_rStaticDLL;
	CStatic	m_rStaticAuthor;
	CEdit	m_rEditAuthor;
	CStatic	m_rStaticUrl;
	CStatic	m_rStaticName;
	CStatic	m_rStaticDesc;
	CStatic	m_rStaticCopy;
	CStatic	m_rStaticClassType;
	CStatic	m_rStaticClassID;
	CEdit	m_rEditClassID;
	CComboBox	m_rComboClassList;
	CEdit	m_rEditUrl;
	CEdit	m_rEditName;
	CEdit	m_rEditDesc;
	CEdit	m_rEditCopyright;
	CEdit	m_rEditClassType;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDLLInfoDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CDemopajaApp*		m_pApp;
	CString				m_sHelpUrl;
	CFont				m_rFont;

	LONG GetRegKey( HKEY key, LPCTSTR subkey, LPTSTR retdata );

	// Generated message map functions
	//{{AFX_MSG(CDLLInfoDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonGo();
	afx_msg void OnButtonHelp();
	afx_msg void OnSelchangeComboClasslist();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLLINFODLG_H__82B5EEC8_3697_4558_8A17_A84B4316AA99__INCLUDED_)
