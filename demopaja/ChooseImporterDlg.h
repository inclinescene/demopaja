#if !defined(AFX_CHOOSEIMPORTERDLG_H__97060E9B_40A0_4E18_B981_3C6639755C2E__INCLUDED_)
#define AFX_CHOOSEIMPORTERDLG_H__97060E9B_40A0_4E18_B981_3C6639755C2E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ChooseImporterDlg.h : header file
//

#include "PajaTypes.h"
#include <vector>

/////////////////////////////////////////////////////////////////////////////
// CChooseImporterDlg dialog

class CChooseImporterDlg : public CDialog
{
// Construction
public:
	CChooseImporterDlg(CWnd* pParent = NULL);   // standard constructor


	void				AddImporter( const char* szName, PajaTypes::int32 i32Id );
	PajaTypes::uint32	GetImporterCount();
	PajaTypes::int32	GetSelectedImporter();
	PajaTypes::int32	GetImporter( PajaTypes::int32 i32Idx );
	

	// Dialog Data
	//{{AFX_DATA(CChooseImporterDlg)
	enum { IDD = IDD_CHOOSE_FILEIMPORTER };
	CStatic	m_rStaticFile;
	CListCtrl	m_rImporterList;
	//}}AFX_DATA

	CString		m_sFileName;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CChooseImporterDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	struct ImporterS {
		CString				m_sName;
		PajaTypes::int32	m_i32Id;
	};

	std::vector<ImporterS>	m_rImporters;
	PajaTypes::int32		m_i32SelectedImporter;

	// Generated message map functions
	//{{AFX_MSG(CChooseImporterDlg)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMDblclkImporterlist(NMHDR *pNMHDR, LRESULT *pResult);
	BOOL m_bUseToAll;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CHOOSEIMPORTERDLG_H__97060E9B_40A0_4E18_B981_3C6639755C2E__INCLUDED_)
