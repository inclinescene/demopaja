#if !defined(AFX_VECTOR2TYPEINDLG_H__EB00C33D_81BC_46E9_AE36_5E4C4CC2157E__INCLUDED_)
#define AFX_VECTOR2TYPEINDLG_H__EB00C33D_81BC_46E9_AE36_5E4C4CC2157E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Vector2TypeInDlg.h : header file
//

#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "Vector2C.h"
#include "ParamI.h"
#include "BtnST.h"

/////////////////////////////////////////////////////////////////////////////
// CVector2TypeInDlg dialog

class CVector2TypeInDlg : public CDialog
{
// Construction
public:
	CVector2TypeInDlg(CWnd* pParent = NULL);   // standard constructor

	PajaTypes::Vector2C			m_rMin, m_rMax;
	bool						m_bClampValues;
	PajaTypes::float32			m_f32Inc;
	Composition::ParamVector2C*	m_pParam;
	PajaTypes::int32			m_i32Time;

	// Dialog Data
	//{{AFX_DATA(CVector2TypeInDlg)
	enum { IDD = IDD_VECTOR2_TYPEIN };
	CButtonST	m_rOk;
	CButtonST	m_rCancel;
	CStatic	m_rStaticLabel2;
	CStatic	m_rStaticLabel1;
	CEdit	m_rEditY;
	CEdit	m_rEditX;
	float	m_f32ValueX;
	float	m_f32ValueY;
	CString	m_sInfoText;
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CVector2TypeInDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:

	CSpinnerButton		m_rSpinnerX;
	CSpinnerButton		m_rSpinnerY;
	PajaTypes::float32	m_f32Scale;

	void AFXAPI	DDX_MyText( CDataExchange* pDX, int nIDC, float& value );

	// Generated message map functions
	//{{AFX_MSG(CVector2TypeInDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEditx();
	afx_msg void OnChangeEdity();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_VECTOR2TYPEINDLG_H__EB00C33D_81BC_46E9_AE36_5E4C4CC2157E__INCLUDED_)
