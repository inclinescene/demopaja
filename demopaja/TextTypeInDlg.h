#if !defined(AFX_TEXTTYPEINDLG_H__B71ED6C5_166B_4E71_AD0F_3A0EB401196A__INCLUDED_)
#define AFX_TEXTTYPEINDLG_H__B71ED6C5_166B_4E71_AD0F_3A0EB401196A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TextTypeInDlg.h : header file
//

#include "PajaTypes.h"
#include "ParamI.h"
#include "BtnST.h"

/////////////////////////////////////////////////////////////////////////////
// CTextTypeInDlg dialog

class CTextTypeInDlg : public CDialog
{
// Construction
public:
	CTextTypeInDlg(CWnd* pParent = NULL);   // standard constructor

	Composition::ParamTextC*	m_pParam;
	PajaTypes::int32			m_i32Time;

	// Dialog Data
	//{{AFX_DATA(CTextTypeInDlg)
	enum { IDD = IDD_TEXT_TYPEIN };
	CButtonST	m_rOk;
	CButtonST	m_rCancel;
	CString	m_sText;
	CString	m_sInfoText;
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTextTypeInDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

protected:

	// Generated message map functions
	//{{AFX_MSG(CTextTypeInDlg)
	afx_msg void OnChangeEdit();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXTTYPEINDLG_H__B71ED6C5_166B_4E71_AD0F_3A0EB401196A__INCLUDED_)
