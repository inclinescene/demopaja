#ifndef __UNDOMANAGERC_H__
#define __UNDOMANAGERC_H__

#include <vector>
#include "PajaTypes.h"
#include "DataBlockI.h"
#include "UndoC.h"


namespace Edit {

	class UndoManagerC  
	{
	public:
		UndoManagerC();
		virtual ~UndoManagerC();

		void		push( UndoC* pCmd );

		void		undo();
		void		redo();

		// purges the possible redo operation.
		// usefull, if some changes are made to an object and something went wrong
		// and the operation was canceleld using the undo. purging redo will
		// prevent the operation being redone.
		void		purge_redo();

		bool		can_undo();
		bool		can_redo();

		void		set_undo_depth( PajaTypes::int32 i32Depth );

		const char*	get_undo_name() const;
		const char*	get_redo_name() const;

		void		purge();

	private:
		std::vector<UndoC*>		m_rUndoStack;
		PajaTypes::int32		m_i32Head;
		PajaTypes::int32		m_i32Depth;
	};

};	// namespace

#endif // __UNDOMANAGERC_H__
