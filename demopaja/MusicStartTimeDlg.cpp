// MusicStartTimeDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "MusicStartTimeDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


using namespace PajaTypes;

/////////////////////////////////////////////////////////////////////////////
// CMusicStartTimeDlg dialog


CMusicStartTimeDlg::CMusicStartTimeDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMusicStartTimeDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMusicStartTimeDlg)
	m_i32StartTime = 0;
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CMusicStartTimeDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMusicStartTimeDlg)
	DDX_Control(pDX, IDC_LISTMARKERS, m_rListMarkers);
	DDX_Control(pDX, IDOK, m_rOk);
	DDX_Control(pDX, IDCANCEL, m_rCancel);
	DDX_Control(pDX, IDC_STATICSTARTTIME, m_rStaticStartTime);
	DDX_Control(pDX, IDC_EDITSTARTTIME, m_rEditStartTime);
	//}}AFX_DATA_MAP
	DDX_MyText( pDX, IDC_EDITSTARTTIME, m_i32StartTime );
}


BEGIN_MESSAGE_MAP(CMusicStartTimeDlg, CDialog)
	//{{AFX_MSG_MAP(CMusicStartTimeDlg)
	ON_EN_CHANGE(IDC_EDITSTARTTIME, OnChangeEditstarttime)
	ON_NOTIFY(NM_CLICK, IDC_LISTMARKERS, OnClickListmarkers)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMusicStartTimeDlg message handlers


void CMusicStartTimeDlg::AddMarker( const char* szName, int32 i32Time )
{
	MarkerS	rMarker;
	rMarker.sName = szName;
	rMarker.i32Time = i32Time;
	m_rMarkers.push_back( rMarker );
}

BOOL CMusicStartTimeDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if( !m_rSpinnerStart.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYINT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
		return FALSE;
	
	m_rSpinnerStart.SetScale( 1 );
	m_rSpinnerStart.SetBuddy( &m_rEditStartTime, SPNB_ATTACH_RIGHT );

	m_rOk.SetFlat( FALSE );
	m_rCancel.SetFlat( FALSE );

	m_rListMarkers.InsertColumn( 0, "Name", LVCFMT_LEFT, 150 );
	m_rListMarkers.InsertColumn( 1, "Time", LVCFMT_RIGHT, 75 );

	for( uint32 i = 0; i < m_rMarkers.size(); i++ ) {
		int32	i32Idx = m_rListMarkers.InsertItem( i, m_rMarkers[i].sName, 0 );
		m_rListMarkers.SetItemText( i32Idx, 1, FormatTime( m_rMarkers[i].i32Time * 256 / m_i32EditAccuracy ) );
		m_rListMarkers.SetItemData( i32Idx, m_rMarkers[i].i32Time );
	}

	m_rStaticStartTime.SetWindowText( FormatTime( m_i32StartTime * 256 / m_i32EditAccuracy ) );

	return TRUE;
}

void
AFXAPI
CMusicStartTimeDlg::DDX_MyText( CDataExchange* pDX, int nIDC, int& value )
{
	TCHAR	szBuffer[32];
	HWND	hWndCtrl = pDX->PrepareEditCtrl( nIDC );

	if( pDX->m_bSaveAndValidate ) {
		// to value
		szBuffer[0] = 0;
		::GetWindowText( hWndCtrl, szBuffer, 30 );
		value = _ttoi( szBuffer );
	}
	else {
		// from value
		_tcscpy( szBuffer, _itot( value, szBuffer, 10 ) );
		::SetWindowText( hWndCtrl, szBuffer );
	}
}

void CMusicStartTimeDlg::OnChangeEditstarttime() 
{
	UpdateData( TRUE );
	m_rStaticStartTime.SetWindowText( FormatTime( m_i32StartTime * 256 / m_i32EditAccuracy ) );
}

CString CMusicStartTimeDlg::FormatTime( int32 i32Time )
{
	CString	sDuration;
	int32	i32Hours, i32Mins, i32Secs, i32Frames;
	int32	i32TimeScale = m_i32BeatsPerMin * m_i32QNotesPerBeat * 256;

	i32Frames = (i32Time * 60 * 60 / i32TimeScale); // / (256 / m_i32EditAccuracy);
	i32Secs = (i32Time * 60 / i32TimeScale) % 60;
	i32Mins = (i32Time / i32TimeScale) % 60;
	i32Hours = i32Time / 60 / i32TimeScale;
	i32Frames = (i32Time - ((i32Hours * 3600 + i32Mins * 60 + i32Secs) * i32TimeScale) / 60) / (256 / m_i32EditAccuracy);

	sDuration.Format( "%d:%02d:%02d:%02d", i32Hours, i32Mins, i32Secs, i32Frames );

	return sDuration;
}

void CMusicStartTimeDlg::OnClickListmarkers( NMHDR* pNMHDR, LRESULT* pResult ) 
{
	
	POSITION pos = m_rListMarkers.GetFirstSelectedItemPosition();
	if( pos ) {
		int nItem = m_rListMarkers.GetNextSelectedItem( pos );
		m_i32StartTime = m_rListMarkers.GetItemData( nItem );
		UpdateData( FALSE );
		m_rStaticStartTime.SetWindowText( FormatTime( m_i32StartTime * 256 / m_i32EditAccuracy ) );
	}

	*pResult = 0;
}
