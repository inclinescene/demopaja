#if !defined(AFX_FILESUBCLASSWND_H__3C5CFFB0_4CAB_48F4_92D4_6938CF6DB38D__INCLUDED_)
#define AFX_FILESUBCLASSWND_H__3C5CFFB0_4CAB_48F4_92D4_6938CF6DB38D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FileSubClassWnd.h : header file
//

// Forward decl.


/////////////////////////////////////////////////////////////////////////////
// CFileSubClassWnd window

class CFileSubClassWnd : public CWnd
{
// Construction
public:
	CFileSubClassWnd( CWnd* pDlg );

// Attributes
public:
	CWnd*	m_pFileDlg;

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFileSubClassWnd)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CFileSubClassWnd();

	// Generated message map functions
protected:
	//{{AFX_MSG(CFileSubClassWnd)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG
	afx_msg void OnAddButton();              // Add button clicked
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FILESUBCLASSWND_H__3C5CFFB0_4CAB_48F4_92D4_6938CF6DB38D__INCLUDED_)
