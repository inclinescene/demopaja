#if !defined(AFX_MERGEFILEDLG_H__60EC280F_A7E1_439F_86A1_FD1C67B80FA8__INCLUDED_)
#define AFX_MERGEFILEDLG_H__60EC280F_A7E1_439F_86A1_FD1C67B80FA8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MergeFileDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMergeFileDlg dialog

class CMergeFileDlg : public CDialog
{
// Construction
public:
	CMergeFileDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMergeFileDlg)
	enum { IDD = IDD_SCENEMERGEDLG };
	CEdit	m_rEditFolder;
	BOOL	m_bNewScene;
	CString	m_sFolder;
	int		m_iDestFolder;
	BOOL	m_bUseFiles;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMergeFileDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMergeFileDlg)
	afx_msg void OnRadiofolder();
	afx_msg void OnRadiofolder2();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MERGEFILEDLG_H__60EC280F_A7E1_439F_86A1_FD1C67B80FA8__INCLUDED_)
