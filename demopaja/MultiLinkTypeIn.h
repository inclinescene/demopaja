#pragma once
#include "afxcmn.h"
#include "afxwin.h"
#include "BtnST.h"
#include "PajaTypes.h"
#include "LayerC.h"
#include "EffectI.h"
#include "ParamI.h"

// CMultiLinkTypeIn dialog

class CMultiLinkTypeIn : public CDialog
{
	DECLARE_DYNAMIC(CMultiLinkTypeIn)

public:
	CMultiLinkTypeIn(CWnd* pParent = NULL);   // standard constructor
	virtual ~CMultiLinkTypeIn();

// Dialog Data
	enum { IDD = IDD_MULTI_LINK_TYPEIN };


	Composition::LayerC*		m_pLayer;
	Composition::EffectI*	m_pEffect;
	Composition::ParamLinkC*	m_pParam;
	int			m_iCurSel;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:

	void	UpdateLinkList();
	void	UpdateSelection();

	afx_msg void OnBnClickedButtonadd();
	afx_msg void OnBnClickedButtondel();
	afx_msg void OnBnClickedButtonup();
	afx_msg void OnBnClickedButtondown();
	virtual BOOL OnInitDialog();
protected:
	virtual void OnOK();
public:
	CListCtrl m_ListLinks;
	CButtonST m_ButtonAdd;
	CButtonST m_ButtonDel;
	CButtonST m_ButtonUp;
	CButtonST m_ButtonDown;
	CButtonST m_ButtonOk;
	CButtonST m_ButtonCancel;
	CBitmap		m_rBitmapAdd;
	CBitmap		m_rBitmapDel;
	CBitmap		m_rBitmapUp;
	CBitmap		m_rBitmapDown;
	CString m_sInfoText;
	afx_msg void OnNMClickListlinks(NMHDR *pNMHDR, LRESULT *pResult);
	CComboBox m_ComboLinks;
	afx_msg void OnCbnSelchangeCombo1();
};
