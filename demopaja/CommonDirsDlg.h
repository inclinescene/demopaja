#if !defined(AFX_COMMONDIRSDLG_H__D7573D7E_BC64_4239_B590_DAE4A04C8684__INCLUDED_)
#define AFX_COMMONDIRSDLG_H__D7573D7E_BC64_4239_B590_DAE4A04C8684__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CommonDirsDlg.h : header file
//


#include "BtnST.h"
#include "afxwin.h"


/////////////////////////////////////////////////////////////////////////////
// CCommonDirsDlg dialog

class CCommonDirsDlg : public CDialog
{
// Construction
public:
	CCommonDirsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCommonDirsDlg)
	enum { IDD = IDD_COMMONDIRS };
	CButtonST	m_rButtonMod;
	CButtonST	m_rButtonDel;
	CButtonST	m_rButtonAdd;
	CButtonST	m_rButtonChoose;
	CButtonST	m_rButtonSelect;
	CButtonST	m_rButtonAddPath;
	CListCtrl	m_rDirList;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCommonDirsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CBitmap	m_rBitmapAdd;
	CBitmap	m_rBitmapDel;
	CBitmap	m_rBitmapMod;
	CBitmap	m_rBitmapUp;
	CBitmap	m_rBitmapAddPath;

	// Generated message map functions
	//{{AFX_MSG(CCommonDirsDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnEndlabeleditDirlist(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkDirlist(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtonadd();
	afx_msg void OnButtondel();
	afx_msg void OnButtonmod();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CString m_sProjectPath;
	afx_msg void OnBnClickedButtonchoose();
	afx_msg void OnBnClickedButtonselect();
	afx_msg void OnBnClickedButtonaddpath();
	BOOL m_bUpdateImportables;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMMONDIRSDLG_H__D7573D7E_BC64_4239_B590_DAE4A04C8684__INCLUDED_)
