#if !defined(AFX_COLORTYPEINDLG_H__11067B8F_7A86_483D_B03B_E1C763BD3DB3__INCLUDED_)
#define AFX_COLORTYPEINDLG_H__11067B8F_7A86_483D_B03B_E1C763BD3DB3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorTypeInDlg.h : header file
//

#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "ParamI.h"
#include "DIB.h"
#include "BtnST.h"


/////////////////////////////////////////////////////////////////////////////
// CColorTypeInDlg dialog

class CColorTypeInDlg : public CDialog
{
// Construction
public:
	CColorTypeInDlg(CWnd* pParent = NULL);   // standard constructor


	CString						m_sCaption;

	// if the parameter is a valid pointer, the parameter is used, otherwise
	// the m_rColor member is used as color source and destination.
	// Also, if the parameter pointer is valid, the UI is updated.
	PajaTypes::ColorC			m_rColorValue;
	Composition::ParamColorC*	m_pParam;

	PajaTypes::int32			m_i32Time;
	PajaTypes::float32			m_f32Inc;


	// Dialog Data
	//{{AFX_DATA(CColorTypeInDlg)
	enum { IDD = IDD_COLOR_TYPEIN };
	CButtonST	m_rButtonSwatchmenu;
	CScrollBar	m_rScrollbarSwatch;
	CStatic	m_rStaticSwatch;
	CButtonST	m_rOk;
	CButtonST	m_rCancel;
	CStatic	m_rSliderR;
	CStatic	m_rSliderG;
	CStatic	m_rSliderB;
	CStatic	m_rSliderA;
	CStatic	m_rStaticR;
	CStatic	m_rStaticG;
	CStatic	m_rStaticB;
	CStatic	m_rColor;
	CStatic	m_rNewColor;
	CEdit	m_rEditR;
	CEdit	m_rEditB;
	CEdit	m_rEditG;
	CEdit	m_rEditA;
	float	m_f32ValueA;
	float	m_f32ValueB;
	float	m_f32ValueG;
	float	m_f32ValueR;
	float	m_f32ValueH;
	float	m_f32ValueS;
	float	m_f32ValueV;
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorTypeInDlg)
	public:
	virtual BOOL DestroyWindow();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	enum ModeE {
		MODE_RGB = 0,
		MODE_HSB,
	};

	enum TrackingE {
		TRACK_NONE = 0,
		TRACK_SLIDER_R,
		TRACK_SLIDER_G,
		TRACK_SLIDER_B,
		TRACK_SLIDER_A,
	};

	CBitmap			m_rBitmapMenu;
	CDIB			m_rDIB;
	HCURSOR			m_hCursor;
	CImageList		m_rImageList;
	int				m_iTracking;
	int				m_iMode;

	std::vector<COLORREF>	m_rSwatches;
	int						m_iSwatchSize;
	int						m_iSwatchRows;
	int						m_iScrollSwatchY;

	void AFXAPI	DDX_MyText( CDataExchange* pDX, int nIDC, float& value, float range );

//	void HsvToRgb( int iH, int iS, int iV, int& iR, int& iG, int& iB );
//	void RgbToHsv( int iR, int iG, int iB, int& iH, int& iS, int& iV );
	void HsvToRgb( PajaTypes::float32 fH, PajaTypes::float32 fS, PajaTypes::float32 fV, PajaTypes::float32& fR, PajaTypes::float32& fG, PajaTypes::float32& fB );
	void RgbToHsv( PajaTypes::float32 fR, PajaTypes::float32 fG, PajaTypes::float32 fB, PajaTypes::float32& fH, PajaTypes::float32& fS, PajaTypes::float32& fV );
	void UpdateColorSwatches();
	void SetMode( int iMode );

	enum RedrawE {
		COLOR_REDRAW_COLOR = 0x1,
		COLOR_REDRAW_SLIDERS = 0x2,
		COLOR_REDRAW_SWATCH = 0x4,
		COLOR_REDRAW_ALL = 0xff,
	};

	void	Redraw( int iReason );

	CRect	GetControlRect( CWnd& rWnd );
	void	PickColor( CPoint rPt );
	void	PickSwatchColor( CPoint rPt );

	// Generated message map functions
	//{{AFX_MSG(CColorTypeInDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnChangeEditr();
	afx_msg void OnChangeEditg();
	afx_msg void OnChangeEditb();
	afx_msg void OnChangeEdita();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	afx_msg void OnButtonSwatchmenu();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORTYPEINDLG_H__11067B8F_7A86_483D_B03B_E1C763BD3DB3__INCLUDED_)
