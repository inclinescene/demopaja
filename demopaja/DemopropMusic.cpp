// DemopropMusic.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "DemopropMusic.h"
#include "fmod.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDemopropMusic dialog


CDemopropMusic::CDemopropMusic(CWnd* pParent /*=NULL*/)
	: CPrefSubDlg(CDemopropMusic::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDemopropMusic)
	m_sMusicFile = _T("");
	m_bShowWave = FALSE;
	m_i32Device = -1;
	m_i32Driver = -1;
	m_i32Mixer = -1;
	m_i32Rate = -1;
	//}}AFX_DATA_INIT

}


void CDemopropMusic::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDemopropMusic)
	DDX_Control(pDX, IDC_COMBORATE, m_rComboRate);
	DDX_Control(pDX, IDC_COMBOMIXER, m_rComboMixer);
	DDX_Control(pDX, IDC_COMBODRIVER, m_rComboDriver);
	DDX_Control(pDX, IDC_COMBODEVICE, m_rComboDevice);
	DDX_Text(pDX, IDC_EDIT_MUSICFILE, m_sMusicFile);
	DDX_Check(pDX, IDC_SHOWWAVE, m_bShowWave);
	DDX_CBIndex(pDX, IDC_COMBODEVICE, m_i32Device);
	DDX_CBIndex(pDX, IDC_COMBODRIVER, m_i32Driver);
	DDX_CBIndex(pDX, IDC_COMBOMIXER, m_i32Mixer);
	DDX_CBIndex(pDX, IDC_COMBORATE, m_i32Rate);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDemopropMusic, CPrefSubDlg)
	//{{AFX_MSG_MAP(CDemopropMusic)
	ON_BN_CLICKED(IDC_PICK_MUSIC, OnPickMusic)
	ON_CBN_SELCHANGE(IDC_COMBODRIVER, OnSelchangeCombodriver)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDemopropMusic message handlers

void CDemopropMusic::OnPickMusic() 
{
	char	szFileTypes[] = "MP3 Music (*.mp3)|*.mp3|Ogg Vorbis Music (*.ogg)|*.ogg|WAV Music (*.wav)|*.wav|MP2 Music (*.mp2)|*.mp2|All Files (*.*)|*.*||";
	CFileDialog	rDlg( TRUE, NULL, NULL, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_EXPLORER, szFileTypes );
	rDlg.m_ofn.lpstrTitle = "Import";

	if( rDlg.DoModal() == IDOK ) {
		m_sMusicFile = rDlg.GetPathName();
		UpdateData( FALSE );	// modify text
	}
}

void CDemopropMusic::OnSelchangeCombodriver() 
{
	CDemopajaApp*		pApp = (CDemopajaApp*)AfxGetApp();

	switch( m_rComboDriver.GetCurSel() ) {
	case 0:	pApp->SetMusicDriver( FSOUND_OUTPUT_DSOUND ); break;
	case 1:	pApp->SetMusicDriver( FSOUND_OUTPUT_WINMM ); break;
	case 2:	pApp->SetMusicDriver( FSOUND_OUTPUT_A3D ); break;
	}

	UpdateDeviceList();
}

BOOL CDemopropMusic::OnInitDialog() 
{
	CPrefSubDlg::OnInitDialog();

	UpdateDeviceList();
	
	return TRUE;
}

void CDemopropMusic::UpdateDeviceList()
{
	CDemopajaApp*		pApp = (CDemopajaApp*)AfxGetApp();

	m_rComboDevice.ResetContent();

	for( int i = 0; i < pApp->GetMusicDeviceCount(); i++ )
		m_rComboDevice.AddString( pApp->GetMusicDeviceName( i ) );

	m_rComboDevice.SetCurSel( 0 );
}
