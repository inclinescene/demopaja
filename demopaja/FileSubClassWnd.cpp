// FileSubClassWnd.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "FileSubClassWnd.h"

#include "FileDialogEx.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileSubClassWnd

CFileSubClassWnd::CFileSubClassWnd( CWnd* pDlg ) :
	m_pFileDlg( pDlg )
{
}

CFileSubClassWnd::~CFileSubClassWnd()
{
}


BEGIN_MESSAGE_MAP(CFileSubClassWnd, CWnd)
	//{{AFX_MSG_MAP(CFileSubClassWnd)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
	ON_BN_CLICKED( IDC_FILEDLG_INCBUTTON, OnAddButton )
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CFileSubClassWnd message handlers

void CFileSubClassWnd::OnAddButton()
{
//	::MessageBox(NULL, "AddButton pressed", NULL, MB_OK);

	CFileDialogEx*	pDlg = (CFileDialogEx*)m_pFileDlg;

	pDlg->OnAddButton();
}
