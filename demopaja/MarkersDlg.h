#if !defined(AFX_MARKERSDLG_H__13A57FFD_899A_49AD_BD2F_A471B10F38D7__INCLUDED_)
#define AFX_MARKERSDLG_H__13A57FFD_899A_49AD_BD2F_A471B10F38D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MarkersDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMarkersDlg dialog

class CMarkersDlg : public CDialog
{
// Construction
public:
	CMarkersDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMarkersDlg)
	enum { IDD = IDD_MARKERS_DLG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMarkersDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMarkersDlg)
	afx_msg void OnRclickMarkerlist(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnClickMarkerlist(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkMarkerlist(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MARKERSDLG_H__13A57FFD_899A_49AD_BD2F_A471B10F38D7__INCLUDED_)
