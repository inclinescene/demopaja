#if !defined(AFX_ROTATEDLG_H__B4527CAC_2DD2_432A_ACD5_CA328BF21F60__INCLUDED_)
#define AFX_ROTATEDLG_H__B4527CAC_2DD2_432A_ACD5_CA328BF21F60__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RotateDlg.h : header file
//

#include "SpinnerButton.h"


/////////////////////////////////////////////////////////////////////////////
// CRotateDlg dialog

class CRotateDlg : public CDialog
{
// Construction
public:
	CRotateDlg( CWnd* pParent = NULL );   // standard constructor

// Dialog Data
	//{{AFX_DATA(CRotateDlg)
	enum { IDD = IDD_TRANS_ROTATE };
	CEdit	m_rEditAngle;
	float	m_f32Angle;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRotateDlg)
	public:
	virtual BOOL Create( CWnd* pParentWnd );
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSpinnerButton	m_rSpinner;

	// Generated message map functions
	//{{AFX_MSG(CRotateDlg)
	afx_msg void OnApply();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ROTATEDLG_H__B4527CAC_2DD2_432A_ACD5_CA328BF21F60__INCLUDED_)
