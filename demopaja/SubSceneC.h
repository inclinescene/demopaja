//
// SubSceneC.h
//
// SubScene class
//
// Copyright (c) 2001 memon/moppi productions
//

#ifndef __SUBSCENE_H__
#define __SUBSCENE_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "ColorC.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "SceneC.h"
#include "ImportableImageI.h"
#include "GraphicsBufferI.h"


//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC		CLASS_SUBSCENE( 0, 10 );
const PluginClass::ClassIdC		CLASS_SUBSCENE_IMPORTABLE( 0, 11 );
const PluginClass::ClassIdC		CLASS_SCENETOIMAGE_IMPORTABLE( 0, 12 );
const PluginClass::ClassIdC		CLASS_SHAREDBUFFER_IMPORTABLE( 0, 13 );


//////////////////////////////////////////////////////////////////////////
//
//  sub scene class descriptor.
//

class SceneImportDescC : public PluginClass::ClassDescC
{
public:
	SceneImportDescC();
	virtual ~SceneImportDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  shared buffer class descriptor.
//

class SharedBufferImportDescC : public PluginClass::ClassDescC
{
public:
	SharedBufferImportDescC();
	virtual ~SharedBufferImportDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};

//////////////////////////////////////////////////////////////////////////
//
//  scene to image class descriptor.
//

class SceneToImageImportDescC : public PluginClass::ClassDescC
{
public:
	SceneToImageImportDescC();
	virtual ~SceneToImageImportDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  Sub scene effect class descriptor.
//

class SceneEffectDescC : public PluginClass::ClassDescC
{
public:
	SceneEffectDescC();
	virtual ~SceneEffectDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
// Sub scene Importer class.
//

class SceneImportC : public Import::ImportableI
{
public:
	static SceneImportC*			create_new();
	virtual Edit::DataBlockI*		create();
	virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
	virtual void					copy( Edit::EditableI* pEditable );
	virtual void					restore( Edit::EditableI* pEditable );

	virtual const char*				get_filename();
	virtual void					set_filename( const char* szName );
	virtual bool					create_file( PajaSystem::DemoInterfaceC* pInterface );

	virtual bool					prompt_properties();
	virtual bool					has_properties();

	virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

	virtual PajaTypes::uint32							update_notify( EditableI* pCaller );

	virtual PajaTypes::uint32		get_reference_file_count();
	virtual Import::FileHandleC*	get_reference_file( PajaTypes::uint32 ui32Index );
	virtual void								update_references();

	virtual PluginClass::ClassIdC		get_class_id();
	virtual PluginClass::SuperClassIdC	get_super_class_id();
	virtual const char*					get_class_name();

	virtual const char*				get_info();
	virtual PluginClass::ClassIdC	get_default_effect();

	virtual bool					equals( Import::ImportableI* pImp );

	virtual void					eval_state( PajaTypes::int32 i32Time );

	virtual PajaTypes::int32		get_duration();
	virtual PajaTypes::float32		get_start_label();
	virtual PajaTypes::float32		get_end_label();

	virtual Composition::SceneC*	get_scene();

	virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

protected:
	SceneImportC();
	SceneImportC( Edit::EditableI* pOriginal );
	virtual ~SceneImportC();

private:
	Composition::SceneC*	m_pScene;
};


//////////////////////////////////////////////////////////////////////////
//
// Shared Buffer Importer class.
//

class SharedBufferImportC : public Import::ImportableImageI
{
public:
	static SharedBufferImportC*		create_new();
	virtual Edit::DataBlockI*		create();
	virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
	virtual void					copy( Edit::EditableI* pEditable );
	virtual void					restore( Edit::EditableI* pEditable );

	// overdriven method from datablock
	virtual void					set_alive( bool bState );

	virtual const char*				get_filename();
	virtual void					set_filename( const char* szName );
	virtual bool					create_file( PajaSystem::DemoInterfaceC* pInterface );

	virtual bool					prompt_properties();
	virtual bool					has_properties();

	virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

	virtual PajaTypes::uint32		get_reference_file_count();
	virtual Import::FileHandleC*	get_reference_file( PajaTypes::uint32 ui32Index );

	virtual PluginClass::ClassIdC		get_class_id();
	virtual PluginClass::SuperClassIdC	get_super_class_id();
	virtual const char*					get_class_name();

	// interface for importable image
	virtual PajaTypes::float32		get_width();
	virtual PajaTypes::float32		get_height();
	virtual PajaTypes::int32		get_data_width();
	virtual PajaTypes::int32		get_data_height();
	virtual PajaTypes::int32		get_data_pitch();
	virtual PajaTypes::BBox2C&	get_tex_coord_bounds();
	virtual PajaTypes::int32		get_data_bpp();
	virtual PajaTypes::uint8*		get_data();
	virtual void					bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Stage, PajaTypes::uint32 ui32Properties );

	virtual PajaSystem::GraphicsBufferI*	get_gbuffer();

	virtual const char*				get_info();
	virtual PluginClass::ClassIdC	get_default_effect();

	virtual bool					equals( Import::ImportableI* pImp );

	virtual void					eval_state( PajaTypes::int32 i32Time );

	virtual PajaTypes::int32		get_duration();
	virtual PajaTypes::float32		get_start_label();
	virtual PajaTypes::float32		get_end_label();

	virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

protected:
	SharedBufferImportC();
	SharedBufferImportC( Edit::EditableI* pOriginal );
	virtual ~SharedBufferImportC();

	void				update_gbuffer();

private:
	Import::FileHandleC*	m_pSceneHandle;
	std::string				m_sName;
	PajaSystem::GraphicsBufferI*	m_pGBuffer;
	PajaTypes::BBox2C		m_rTexBounds;
	PajaTypes::int32				m_i32Width;
	PajaTypes::int32				m_i32Height;
	bool								m_bCheckBuffer;
};


//////////////////////////////////////////////////////////////////////////
//
// Scene to Image Importer class.
//

class SceneToImageImportC : public Import::ImportableImageI
{
public:
	static SceneToImageImportC*		create_new();
	virtual Edit::DataBlockI*		create();
	virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
	virtual void					copy( Edit::EditableI* pEditable );
	virtual void					restore( Edit::EditableI* pEditable );

	// overdriven method from datablock
	virtual void					set_alive( bool bState );

	virtual const char*				get_filename();
	virtual void					set_filename( const char* szName );
	virtual bool					create_file( PajaSystem::DemoInterfaceC* pInterface );

	virtual bool					prompt_properties();
	virtual bool					has_properties();

	virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

	virtual PajaTypes::uint32					update_notify( EditableI* pCaller );

	virtual PajaTypes::uint32		get_reference_file_count();
	virtual Import::FileHandleC*	get_reference_file( PajaTypes::uint32 ui32Index );

	virtual PluginClass::ClassIdC		get_class_id();
	virtual PluginClass::SuperClassIdC	get_super_class_id();
	virtual const char*					get_class_name();

	// interface for importable image
	virtual PajaTypes::float32		get_width();
	virtual PajaTypes::float32		get_height();
	virtual PajaTypes::int32		get_data_width();
	virtual PajaTypes::int32		get_data_height();
	virtual PajaTypes::int32		get_data_pitch();
	virtual PajaTypes::BBox2C&	get_tex_coord_bounds();
	virtual PajaTypes::int32		get_data_bpp();
	virtual PajaTypes::uint8*		get_data();
	virtual void					bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Stage, PajaTypes::uint32 ui32Properties );

	virtual const char*				get_info();
	virtual PluginClass::ClassIdC	get_default_effect();

	virtual bool					equals( Import::ImportableI* pImp );

	virtual void					eval_state( PajaTypes::int32 i32Time );

	virtual PajaTypes::int32		get_duration();
	virtual PajaTypes::float32		get_start_label();
	virtual PajaTypes::float32		get_end_label();

	virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

protected:
	SceneToImageImportC();
	SceneToImageImportC( Edit::EditableI* pOriginal );
	virtual ~SceneToImageImportC();

	void				update_gbuffer();

private:
	Import::FileHandleC*	m_pSceneHandle;
	Import::FileHandleC*	m_pBufferHandle;
	std::string				m_sName;

	PajaTypes::BBox2C		m_rTexBounds;
	PajaTypes::int32		m_i32LastRenderedFrame;
	PajaSystem::GraphicsBufferI*	m_pGBuffer;
	bool								m_bCheckBuffer;
};


//////////////////////////////////////////////////////////////////////////
//
// The scene effect class.
//

class SceneEffectC : public Composition::EffectI
{
public:
	static SceneEffectC*			create_new();
	virtual Edit::DataBlockI*		create();
	virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
	virtual void					copy( Edit::EditableI* pEditable );
	virtual void					restore( Edit::EditableI* pEditable );

	virtual PajaTypes::int32		get_gizmo_count();
	virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

	virtual PluginClass::ClassIdC	get_class_id();
	virtual const char*				get_class_name();

	virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
	virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

	virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

	virtual void					eval_state( PajaTypes::int32 i32Time );
	virtual PajaTypes::BBox2C		get_bbox();

	virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

	virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

	virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


protected:
	SceneEffectC();
	SceneEffectC( Edit::EditableI* pOriginal );
	virtual ~SceneEffectC();

private:

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_FILE,
		ID_ATTRIBUTE_CLEARLAYOUT,
		ATTRIBUTE_COUNT,
	};

	enum EffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	Composition::AutoGizmoC*	m_pTraGizmo;
	Composition::AutoGizmoC*	m_pAttGizmo;

	PajaTypes::Matrix2C			m_rTM;
	PajaTypes::BBox2C			m_rBBox;
	PajaTypes::Vector2C			m_rVertices[4];
};


extern SceneImportDescC			g_rSceneImportDesc;
extern SharedBufferImportDescC	g_rSharedBufferImportDesc;
extern SceneToImageImportDescC	g_rSceneToImageImportDesc;
extern SceneEffectDescC			g_rSceneEffectDesc;


#endif	// __SUBSCENE_H__
