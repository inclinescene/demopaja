// SingleLinkTypeIn.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "SingleLinkTypeIn.h"
#include "demopajaDoc.h"

using namespace PajaTypes;
using namespace Composition;
using namespace Import;
using namespace PluginClass;
using namespace PajaSystem;

// CSingleLinkTypeIn dialog

IMPLEMENT_DYNAMIC(CSingleLinkTypeIn, CDialog)
CSingleLinkTypeIn::CSingleLinkTypeIn(CWnd* pParent /*=NULL*/)
	: CDialog(CSingleLinkTypeIn::IDD, pParent)
	, m_sInfoText(_T(""))
	, m_i32EffectNum(0)
{
}

CSingleLinkTypeIn::~CSingleLinkTypeIn()
{
}

void CSingleLinkTypeIn::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_INFO, m_sInfoText);
	DDX_Control(pDX, IDC_COMBO, m_rCombo);
	DDX_Control(pDX, IDOK, m_rOk);
	DDX_Control(pDX, IDCANCEL, m_rCancel);
	DDX_CBIndex(pDX, IDC_COMBO, m_i32EffectNum);
}


BEGIN_MESSAGE_MAP(CSingleLinkTypeIn, CDialog)
	ON_CBN_SELCHANGE(IDC_COMBO, OnCbnSelchangeCombo)
END_MESSAGE_MAP()


// CSingleLinkTypeIn message handlers

BOOL CSingleLinkTypeIn::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Add all 
	std::string	sStr;
	int32		i32Idx;
	int32		i32Sel = 0;

	i32Idx = m_rCombo.AddString( "<Unlinked>" );
	m_rCombo.SetItemData( i32Idx, 0xffffffff );

	EffectI*	pParamEffect = 0;
	
	if( m_pParam->get_link_count() )
		pParamEffect = m_pParam->get_link( 0 );

//	for( int32 i = m_pLayer->get_effect_count() - 1; i >= 0; i-- ) {
	
	bool	bCanAdd = false;

	for( int32 i = 0; i < m_pLayer->get_effect_count(); i++ ) {

		EffectI*	pEffect = m_pLayer->get_effect( i );

		// Skip null effects and self
		if( !pEffect )
		{
			continue;
		}

		// Dont allow to link to effects that are evaluated after the current effect.
		if( pEffect == m_pEffect )
		{
			bCanAdd = true;
			continue;
		}

		if( bCanAdd )
		{
			if( (m_pParam->get_class_filter() == NULL_CLASSID ||
				m_pParam->get_class_filter() == pEffect->get_class_id()) &&
				(m_pParam->get_super_class_filter() == NULL_SUPERCLASS ||
				m_pParam->get_super_class_filter() == pEffect->get_super_class_id()) ) {

				i32Idx = m_rCombo.AddString( pEffect->get_name() );
				m_rCombo.SetItemData( i32Idx, i );
			}

			if( pParamEffect == pEffect )
				i32Sel = i32Idx;
		}
	}
	m_rCombo.SetCurSel( i32Sel );
	m_i32EffectNum = i32Sel;

	m_rOk.SetFlat( FALSE );
	m_rCancel.SetFlat( FALSE );

	return TRUE;
}

void CSingleLinkTypeIn::OnCbnSelchangeCombo()
{
	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	UpdateData( TRUE );
	int32	i32Num = m_rCombo.GetItemData( m_i32EffectNum );
	EffectI*	pEffect = 0;
	if( i32Num != CB_ERR && i32Num != 0xffffffff )
		pEffect = m_pLayer->get_effect( i32Num );

	pDoc->BeginDeferHandleParamNotify();

	if( !pEffect )
	{
		// Remove link
		pDoc->HandleParamNotify( m_pParam->del_link( (uint32)0 ) );
	}
	else if( m_pParam->get_link_count() == 0 )
	{
		// Add new link
		pDoc->HandleParamNotify( m_pParam->add_link( pEffect ) );
	}
	else
	{
		// Replace old link
		pDoc->HandleParamNotify( m_pParam->set_link( 0, pEffect ) );
	}

	pDoc->EndDeferHandleParamNotify();

	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}
