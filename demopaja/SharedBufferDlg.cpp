// SharedBufferDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "SharedBufferDlg.h"


// CSharedBufferDlg dialog

IMPLEMENT_DYNAMIC(CSharedBufferDlg, CDialog)
CSharedBufferDlg::CSharedBufferDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSharedBufferDlg::IDD, pParent)
	, m_sName(_T(""))
	, m_iWidth(0)
	, m_iHeight(0)
{
}

CSharedBufferDlg::~CSharedBufferDlg()
{
}

void CSharedBufferDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDITNAME, m_sName);
	DDX_Text(pDX, IDC_EDIT_WIDTH, m_iWidth);
	DDV_MinMaxInt(pDX, m_iWidth, 1, 10000);
	DDX_Text(pDX, IDC_EDIT_HEIGHT, m_iHeight);
	DDV_MinMaxInt(pDX, m_iHeight, 1, 10000);
}


BEGIN_MESSAGE_MAP(CSharedBufferDlg, CDialog)
END_MESSAGE_MAP()


// CSharedBufferDlg message handlers

BOOL CSharedBufferDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	return TRUE;
}
