#if !defined(AFX_DYNDIALOG_H__3C1575C1_2514_11D4_A80C_0000E8D926FD__INCLUDED_)
#define AFX_DYNDIALOG_H__3C1575C1_2514_11D4_A80C_0000E8D926FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DynDialog.h : header file
//

#include <afxtempl.h>
#include "ParamI.h"
#include "PajaTypes.h"
#include "GizmoI.h"
#include <vector>



/////////////////////////////////////////////////////////////////////////////
// CDynDialog dialog

class CDynDialog : public CDialog
{
// Construction
public:
	CDynDialog(CWnd* pParent = NULL);   // standard constructor

	virtual void	UpdateFromGizmo( Composition::GizmoI* pGizmo );
	virtual bool	AddParameter( Composition::ParamI* param );
	virtual void	ParameterChanged( Composition::ParamI* param );

	virtual void	UpdateAllParamaters();

	// Returns true if the point is over some control.
	bool				HitTestControl( const CPoint& rPt );

	// Dialog Data
	//{{AFX_DATA(CDynDialog)
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


protected:

//	PajaTypes::uint32	GetNextFreeID();
//	BOOL				CreateControls();

	enum HitTestE {
		HITTEST_NONE,
		HITTEST_EDITBOX,
		HITTEST_COMBOBUTTON,
		HITTEST_SPINNERUP,
		HITTEST_SPINNERDOWN,
	};


	void				DrawEditBox( CDC* pDC, const CRect& rItemRect, const char* szText );
	void				DrawEditBoxWithSpinner( CDC* pDC, const CRect& rItemRect, const char* szText, bool bSpinnerUp, bool bSpinnerDown );
	void				DrawComboBox( CDC* pDC, const CRect& rItemRect, const char* szText, bool bState );

	PajaTypes::int32	HitTestEditBox( const CPoint& rPt, const CRect& rItemRect );
	PajaTypes::int32	HitTestEditBoxWithSpinner( const CPoint& rPt, const CRect& rItemRect );
	PajaTypes::int32	HitTestComboBox( const CPoint& rPt, const CRect& rItemRect );

	CString				Format( PajaTypes::float32 F32Value, PajaTypes::float32 f32Inc );

	void				CalcItemRects();

	void				CreateEdit( const CRect& rItemRect, const char* szText, bool bSpinner );

	void				SetSpinnerTrack( PajaTypes::float32 f32StartVal, PajaTypes::float32 f32Inc, PajaTypes::float32 f32IncSign, PajaTypes::int32 i32StartY );

	void				ParamIntCombo( const CPoint& rPt, PajaTypes::int32 i32MinWidth, Composition::ParamIntC* pParam, PajaTypes::int32 i32Time );
	void				ParamFileCombo( const CPoint& rPt, PajaTypes::int32 i32MinWidth, Composition::ParamFileC* pParam, PajaTypes::int32 i32Time );

	PajaTypes::int32	GetTimeOffset( Composition::ParamI* pParam );

	void				Redraw( PajaTypes::uint32 ui32Flags );

	enum ActiveFlagE {
		ACTIVEITEM_NONE = 0,
		ACTIVEITEM_UP_X,
		ACTIVEITEM_DOWN_X,
		ACTIVEITEM_UP_Y,
		ACTIVEITEM_DOWN_Y,
		ACTIVEITEM_UP_Z,
		ACTIVEITEM_DOWN_Z,
		ACTIVEITEM_COMBO,
	};

	class ParamItemC
	{
	public:
		ParamItemC() : m_pParam( 0 ), m_i32Y( 0 ), m_i32ActiveItem( 0 ) {};
		virtual ~ParamItemC() {};
		Composition::ParamI*	m_pParam;
		PajaTypes::int32		m_i32Y;
		CRect					m_rItemRects[3];
		PajaTypes::int32		m_i32RectCount;
		PajaTypes::int32		m_i32ActiveItem;
	};

	struct DLGTEMP {
		DLGTEMPLATE	tmp;
		WORD		menu;			// 0 = no menu
		WORD		clss;			// 0 = defautl class
		WORD		name;			// 0 = no name
	};

/*	enum ControlTypeE {
		CTLTYPE_CAPTION,
		CTLTYPE_PRE_LABEL,
		CTLTYPE_POST_LABEL,
		CTLTYPE_EDIT,
		CTLTYPE_EDIT_MAX,
		CTLTYPE_SPINNER,
		CTLTYPE_BUTTON,
	};

	struct CtlS {
		PajaTypes::uint32	m_ui32Type;
		CWnd*				m_pWnd;
	};*/


	bool						m_bValueChanged;
	CFont						m_rBoldFont;
	CFont						m_rThinFont;
	CFont						m_rSmallFont;
	PajaTypes::int32			m_i32BoldHeight;
	PajaTypes::int32			m_i32ThinHeight;

	PajaTypes::int32			m_i32CurY;

	std::vector<ParamItemC>		m_rParams;

	CEdit*						m_pEditCtrl;
	Composition::ParamI*		m_pEditingItem;
	PajaTypes::int32			m_i32EditingItem;
	PajaTypes::int32			m_i32EditingSubItem;

	Composition::ParamI*		m_pHitItem;
	PajaTypes::int32			m_i32HitItem;
	PajaTypes::int32			m_i32HitSubItem;
	PajaTypes::float32			m_f32TrackStartVal;
	PajaTypes::float32			m_f32TrackInc;
	PajaTypes::float32			m_f32TrackIncSign;
	PajaTypes::int32			m_i32TrackStartY;
	bool						m_bTrackMoved;

	PajaTypes::int32			m_i32TimerCount;
	UINT						m_uiTimerID;
	Composition::ParamI*		m_pTimerItem;
	PajaTypes::float32			m_f32TimerInc;
	
	//	std::vector<CtlS>			m_controls;

//	static PajaTypes::uint32	m_nextFreeId;

	bool						m_bUpdating;

	PajaTypes::uint32			m_ui32RedrawFlags;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDynDialog)
	public:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL Create( DWORD dwStyle, const RECT& rect, CWnd* pParentWnd );
	protected:
	virtual void PostNcDestroy();
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	//}}AFX_VIRTUAL

	virtual LRESULT DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam);

	// Generated message map functions
	//{{AFX_MSG(CDynDialog)
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	//}}AFX_MSG
	afx_msg void OnPaint();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnKillfocusEditLabel();
	afx_msg void OnChangeEditLabel();
	afx_msg void OnTimer(UINT nIDEvent);

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DYNDIALOG_H__3C1575C1_2514_11D4_A80C_0000E8D926FD__INCLUDED_)
