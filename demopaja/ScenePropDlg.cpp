// ScenePropDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "ScenePropDlg.h"
#include "ColorTypeInDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace PajaTypes;

/////////////////////////////////////////////////////////////////////////////
// CScenePropDlg dialog


CScenePropDlg::CScenePropDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CScenePropDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CScenePropDlg)
	m_sName = _T("");
	m_iHeight = 0;
	m_iWidth = 0;
	m_sDuration = _T("");
	//}}AFX_DATA_INIT
}


void CScenePropDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CScenePropDlg)
	DDX_Control(pDX, IDC_EDITWIDTH, m_rEditWidth);
	DDX_Control(pDX, IDC_EDITHEIGHT, m_rEditHeight);
	DDX_Text(pDX, IDC_EDITNAME, m_sName);
	DDX_Text(pDX, IDC_EDITHEIGHT, m_iHeight);
	DDV_MinMaxInt(pDX, m_iHeight, 0, 10000);
	DDX_Text(pDX, IDC_EDITWIDTH, m_iWidth);
	DDV_MinMaxInt(pDX, m_iWidth, 0, 10000);
	DDX_Text(pDX, IDC_EDITDURATION, m_sDuration);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CScenePropDlg, CDialog)
	//{{AFX_MSG_MAP(CScenePropDlg)
	ON_BN_CLICKED(IDC_BUTTON_LAYOUT, OnButtonLayout)
	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CScenePropDlg message handlers

BOOL CScenePropDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if( !m_rSpinnerWidth.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT | SPNB_USELIMITS, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
		return FALSE;
	m_rSpinnerWidth.SetMinMax( 0, 10000 );
	m_rSpinnerWidth.SetScale( 1 );
	m_rSpinnerWidth.SetBuddy( &m_rEditWidth, SPNB_ATTACH_RIGHT );

	
	if( !m_rSpinnerHeight.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT | SPNB_USELIMITS, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER2 ) )
		return FALSE;
	m_rSpinnerHeight.SetMinMax( 0, 10000 );
	m_rSpinnerHeight.SetScale( 1 );
	m_rSpinnerHeight.SetBuddy( &m_rEditHeight, SPNB_ATTACH_RIGHT );
	

	return TRUE;
}

void CScenePropDlg::OnButtonLayout() 
{
	CColorTypeInDlg	rDlg;

	rDlg.m_rColorValue = m_rBGColor;
	rDlg.m_sCaption = "Choose Background Color";

	if( rDlg.DoModal() == IDOK ) {
		m_rBGColor = rDlg.m_rColorValue;
		Invalidate();
	}
}

inline
COLORREF
color_to_COLOREF( const ColorC& rCol )
{
	return RGB( (int32)(rCol[0] * 255.0f), (int32)(rCol[1] * 255.0f), (int32)(rCol[2] * 255.0f) );
}

void CScenePropDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if( nIDCtl == IDC_BUTTON_LAYOUT ) {
		HBRUSH	rColorBrush = ::CreateSolidBrush( color_to_COLOREF( m_rBGColor ) );
		::FillRect( lpDrawItemStruct->hDC, &lpDrawItemStruct->rcItem, rColorBrush );
		::DeleteObject( rColorBrush );
		if( lpDrawItemStruct->itemState == ODS_FOCUS ) {
			RECT	rFocusRect = lpDrawItemStruct->rcItem;
			::InflateRect( &rFocusRect, -2, -2 );
			::DrawFocusRect( lpDrawItemStruct->hDC, &rFocusRect );
		}
	}
	else
		CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}
