#if !defined(AFX_ALIGNDLG_H__39541C20_192F_4C70_A548_5D7B0BD2DF4D__INCLUDED_)
#define AFX_ALIGNDLG_H__39541C20_192F_4C70_A548_5D7B0BD2DF4D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AlignDlg.h : header file
//

#include "PajaTypes.h"
#include "EffectI.h"
#include <vector>


/////////////////////////////////////////////////////////////////////////////
// CAlignDlg dialog

class CAlignDlg : public CDialog
{
// Construction
public:
	CAlignDlg( CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAlignDlg)
	enum { IDD = IDD_ALIGN };
	CButton	m_rVertDistTop;
	CButton	m_rVertDistHeights;
	CButton	m_rVertDistCenter;
	CButton	m_rVertDistBottom;
	CButton	m_rVertAlignTop;
	CButton	m_rVertAlignCenter;
	CButton	m_rVertAlignBottom;
	CButton	m_rHorizDistWidths;
	CButton	m_rHorizDistRight;
	CButton	m_rHorizDistLeft;
	CButton	m_rHorizDistCenter;
	CButton	m_rHorizAlignRight;
	CButton	m_rHorizAlignLeft;
	CButton	m_rHorizAlignCenter;
	CButton	m_rAlignToPage;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAlignDlg)
	public:
	virtual BOOL Create( CWnd* pParentWnd = 0 );
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CImageList		m_rImages;

	bool	GetSelectedEffects( std::vector<Composition::EffectI*>& rEffects, std::vector<PajaTypes::int32>& rParamTimes );

	// Generated message map functions
	//{{AFX_MSG(CAlignDlg)
	afx_msg void OnAlignToPage();
	afx_msg void OnHorizAlignCenter();
	afx_msg void OnHorizAlignLeft();
	afx_msg void OnHorizAlignRight();
	afx_msg void OnHorizDistCenter();
	afx_msg void OnHorizDistLeft();
	afx_msg void OnHorizDistRight();
	afx_msg void OnHorizDistWidths();
	afx_msg void OnVertAlignBottom();
	afx_msg void OnVertAlignCenter();
	afx_msg void OnVertAlignTop();
	afx_msg void OnVertDistBottom();
	afx_msg void OnVertDistCenter();
	afx_msg void OnVertDistHeights();
	afx_msg void OnVertDistTop();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	BOOL OnToolTipNotify( UINT id, NMHDR * pNMHDR, LRESULT * pResult );

	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ALIGNDLG_H__39541C20_192F_4C70_A548_5D7B0BD2DF4D__INCLUDED_)
