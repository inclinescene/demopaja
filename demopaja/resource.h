//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by demopaja.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_DEMOPATYPE                  129
#define IDB_TIMELINE                    130
#define IDB_DEL                         135
#define IDB_NEW                         136
#define IDB_UP                          137
#define IDB_DOWN                        138
#define IDC_SIZETOP                     139
#define IDC_SIZEBOT                     140
#define IDB_LOCKRIGHT                   142
#define IDB_EYERIGHT                    143
#define IDB_ARRANGE                     144
#define IDB_LAYERRIGHT                  145
#define IDC_SIZEX                       146
#define IDC_MOVE_TIME                   148
#define IDC_MOVE_KEY                    149
#define IDC_MOVE_KEY_XY                 150
#define IDB_TIMESEGRIGHT                152
#define IDB_FILETYPES                   153
#define IDD_DLL_INFO                    154
#define IDB_KEYS                        159
#define IDC_SIZEY                       160
#define IDC_MOVE_KEY_VALUE              161
#define IDB_CONTROLLERRIGHT             162
#define IDB_CURSEL                      163
#define IDB_CLOSEBUTTON                 164
#define IDD_FLOAT_TYPEIN                165
#define IDD_VECTOR2_TYPEIN              166
#define IDB_TIMESCALE                   167
#define IDD_VECTOR3_TYPEIN              168
#define IDD_COLOR_TYPEIN                169
#define IDB_COLORRAMP_SMALL             170
#define IDB_COLORRAMP                   171
#define IDC_COLORPICK                   172
#define IDB_ORTYPES                     173
#define IDD_CHOOSE_FILEIMPORTER         181
#define IDB_FILELISTRIGHT               182
#define IDC_DPHAND                      183
#define IDC_FINGER                      184
#define IDC_DPHANDDRAG                  185
#define IDD_INT_TYPEIN                  187
#define IDD_INT_COMBO_TYPEIN            188
#define IDC_ZOOM                        189
#define IDB_PLAY                        190
#define IDB_STOP                        191
#define IDB_PREV                        192
#define IDB_FORW                        193
#define IDB_BACK                        194
#define IDB_NEXT                        195
#define IDD_FILE_TYPEIN                 196
#define IDD_DEMOPROPERTIES              197
#define IDD_GRID_SETTINGS               201
#define IDD_TEXT_TYPEIN                 202
#define IDD_START_DIALOG                203
#define IDB_NEXT_PAGE                   204
#define IDR_TOOLS                       205
#define IDD_ALIGN                       210
#define IDD_TRANS_MOVE                  211
#define IDD_TRANS_ROTATE                214
#define IDD_TRANS_SCALE                 215
#define IDD_TRANSFORM                   216
#define IDB_MINIMIZED                   217
#define IDB_MINIMIZED_CHECKED           218
#define IDB_ALIGN                       221
#define IDD_TIMESEGMENT_TYPEIN          222
#define IDB_TIMESEGTYPEIN               223
#define IDD_HELPDIALOG                  224
#define IDR_HELPHOME                    228
#define IDB_HELP_REFRESH                234
#define IDD_DEMOPROP_LAYOUT             236
#define IDD_DEMOPROP_TIMING             237
#define IDD_DEMOPROP_COMMENT            238
#define IDD_DEMOPROP_DEVICES            239
#define IDD_DEMOPROP_MUSIC              240
#define IDB_TRIPOINTER                  246
#define IDB_TEST                        247
#define IDB_GIZMORIGHT                  248
#define IDB_WAVERIGHT                   249
#define IDB_TRANSFORM                   250
#define IDD_MARKER_TYPEIN               251
#define IDB_MARKERRIGHT                 252
#define IDB_TIMERULER                   253
#define IDB_NULOGO                      255
#define IDB_CREATESCENE                 256
#define IDB_CREATELAYER                 257
#define IDB_CREATEEFFECT                258
#define IDB_TRASHBIN                    259
#define IDD_SUBSCENEPROP                260
#define IDR_PLAYBAR                     261
#define IDB_IMPORTFILE                  263
#define IDB_CREATEFILE                  264
#define IDB_TIMEZOOMIN                  265
#define IDB_TIMEZOOMOUT                 266
#define IDB_DROPDOWN                    267
#define IDD_COMMONDIRS                  270
#define IDB_COMDIR_DEL                  272
#define IDB_COMDIR_MODIFY               273
#define IDB_COMDIR_UP                   274
#define IDB_COMDIR_DOWN                 275
#define IDB_COMDIR_ADDPATH              275
#define IDI_ICONWARNING                 276
#define IDD_EXPORTAVI_DLG               277
#define IDB_GRAPHBUFFER_CENTER          279
#define IDB_GRAPHBUFFER_SCALEFIT        280
#define IDB_GRAPHBUFFER_CROP            281
#define IDB_CREATEBUFFER                283
#define IDB_CREATEFOLDER                284
#define IDB_FILELIST                    285
#define IDD_FOLDERPROP                  286
#define IDB_SWATCHMENU                  287
#define IDB_FOLDERS                     288
#define IDD_MARKERS_DLG                 290
#define IDD_SCENEMERGEDLG               291
#define IDC_ADJUSTY                     292
#define IDB_SCENETAB                    293
#define IDD_MUSICSTARTTIMEDLG           296
#define IDD_RECURSIONERROR_DLG          298
#define IDB_RECURSIONERROR              299
#define IDD_SCENE_TO_IMAGE_DLG          300
#define IDD_SHARED_BUFFER_DLG           301
#define IDD_SINGLE_LINK_TYPEIN          302
#define IDD_MULTI_LINK_TYPEIN           303
#define IDB_HELP_OPEN                   307
#define IDB_HELP_HOME                   308
#define IDB_HELP_PREV                   309
#define IDB_HELP_NEXT                   310
#define IDB_HELP_STOP                   311
#define IDB_COMDIR_ADD                  312
#define ID_HELP_INDEX313                313
#define IDB_LINK_ADD                    313
#define ID_HELP_HELPINDEX               314
#define IDB_LINK_DEL                    314
#define IDB_LINK_UP                     315
#define IDB_LINK_DOWN                   316
#define IDC_BUTTON2                     1002
#define IDC_BUTTON_GO                   1002
#define IDC_BUTTONDEL                   1002
#define IDC_EDIT                        1005
#define IDC_TIME                        1006
#define IDC_EDITZ                       1007
#define IDC_EDITA                       1008
#define IDC_PIC                         1009
#define IDC_EDITX                       1016
#define IDC_EDITY                       1017
#define IDC_EDITR                       1020
#define IDC_EDITG                       1021
#define IDC_EDITB                       1022
#define IDC_NEWCOLOR                    1023
#define IDC_COLOR                       1024
#define IDC_IMPORTERLIST                1025
#define IDC_SLIDERR                     1025
#define IDC_COMBO                       1026
#define IDC_SLIDERG                     1026
#define IDC_EDIT_WIDTH                  1027
#define IDC_SLIDERB                     1027
#define IDC_EDIT_HEIGHT                 1028
#define IDC_SLIDERA                     1028
#define IDC_EDIT_BPM                    1030
#define IDC_EDIT_DURATION               1031
#define IDC_EDIT_MUSICFILE              1032
#define IDC_PICK_MUSIC                  1033
#define IDC_EDIT_COMMENT                1034
#define IDC_COMBO_ACCURACY              1041
#define IDC_BEAT_SIZE                   1044
#define IDC_MEASURE_SIZE                1045
#define IDC_GRID_ENABLED                1046
#define IDC_EDIT_GRIDSIZE               1047
#define IDC_APPLY                       1060
#define IDC_EDITANGLE                   1062
#define IDC_UNIFORM                     1062
#define IDC_TAB                         1065
#define IDC_HORIZ_ALIGN_LEFT            1069
#define IDC_ALIGN_TO_PAGE               1070
#define IDC_INFO                        1071
#define IDC_EDITORIGO                   1072
#define IDC_TIME_INFO                   1073
#define IDC_STATICORIGOTIME             1074
#define IDC_EDITTIME                    1076
#define IDC_STATICKEYTIME               1077
#define IDC_EDITKEY                     1078
#define IDC_STATICKEYCOUNT              1081
#define IDC_STATICDURATIONTIME          1082
#define IDC_STATICDURATIONFRAMES        1083
#define IDC_STATICSTARTTIME             1084
#define IDC_STATICENDTIME               1085
#define IDC_STATICSEGMENT               1086
#define IDC_FPS                         1088
#define IDC_EXPLORER                    1089
#define IDC_LABEL1                      1091
#define IDC_LABEL2                      1092
#define IDC_LABEL3                      1093
#define IDC_OPEN                        1093
#define IDC_STOP                        1099
#define IDC_RELOAD                      1100
#define IDC_HORIZ_ALIGN_CENTER          1101
#define IDC_COMBO_CLASSLIST             1101
#define IDC_HORIZ_ALIGN_RIGHT           1102
#define IDC_BUTTON_HELP                 1102
#define IDC_VERT_ALIGN_TOP              1103
#define IDC_VERT_ALIGN_CENTER           1104
#define IDC_VERT_ALIGN_BOTTOM           1105
#define IDC_EDIT_NAME                   1105
#define IDC_EDIT_DESC                   1106
#define IDC_EDIT_AUTHOR                 1107
#define IDC_HORIZ_DIST_WIDTHS           1108
#define IDC_EDIT_COPYRIGHT              1108
#define IDC_HORIZ_DIST_LEFT             1109
#define IDC_EDIT_CLASSTYPE              1109
#define IDC_HORIZ_DIST_CENTER           1110
#define IDC_EDIT_CLASSID                1110
#define IDC_HORIZ_DIST_RIGHT            1111
#define IDC_EDIT_URL                    1111
#define IDC_VERT_DIST_HEIGHTS           1112
#define IDC_STATIC_NAME                 1112
#define IDC_VERT_DIST_TOP               1113
#define IDC_STATIC_DESC                 1113
#define IDC_VERT_DIST_CENTER            1114
#define IDC_STATIC_AUTHOR               1114
#define IDC_VERT_DIST_BOTTOM            1115
#define IDC_STATIC_COPY                 1115
#define IDC_STATIC_URL                  1116
#define IDC_STATIC_CLASSTYPE            1117
#define IDC_STATIC_CLASSID              1118
#define IDC_EDIT_DLL                    1119
#define IDC_PAGETREE                    1120
#define IDC_STATIC_DLL                  1120
#define IDC_STATIC_TITLE                1121
#define IDC_STATIC_BOUDINGFRAME         1122
#define IDC_STATIC_R                    1126
#define IDC_STATIC_G                    1127
#define IDC_STATIC_B                    1128
#define IDC_CREDITS                     1137
#define IDC_DEVICE_LIST                 1141
#define IDC_COMBO_GRAPHDEVICE           1142
#define IDC_BUTTON_CONFIGGRAPH          1143
#define IDC_BUTTON_CONFIGDYN            1144
#define IDC_NAME                        1144
#define IDC_SHOWWAVE                    1145
#define IDC_TEXT                        1146
#define IDC_EDITNAME                    1148
#define IDC_EDITWIDTH                   1149
#define IDC_EDITHEIGHT                  1150
#define IDC_EDITDURATION                1151
#define IDC_COMBODRIVER                 1151
#define IDC_COMBOMIXER                  1152
#define IDC_COMBORATE                   1153
#define IDC_COMBODEVICE                 1154
#define IDC_DIRLIST                     1163
#define IDC_BUTTONADD                   1164
#define IDC_BUTTONUP                    1165
#define IDC_BUTTONSELECT                1165
#define IDC_BUTTONDOWN                  1166
#define IDC_BUTTONADDPATH               1166
#define IDC_BUTTONMOD                   1167
#define IDC_STATICFILE                  1168
#define IDC_BUTTONCHOOSE                1168
#define IDC_EDIT_FPS                    1171
#define IDC_EDIT_TIMESTART              1172
#define IDC_EDIT_TIMEEND                1173
#define IDC_STATIC_TIMESTART            1178
#define IDC_STATIC_TIMEEND              1179
#define IDC_SWATCHES                    1184
#define IDC_SCROLLBAR_SWATCH            1186
#define IDC_BUTTON_SWATCHMENU           1188
#define IDC_COMBOCOLOR                  1189
#define IDC_MARKERLIST                  1190
#define IDC_CHECKNEWSCENE               1191
#define IDC_CHECKUSEFILES               1192
#define IDC_RADIOFOLDER                 1194
#define IDC_RADIOFOLDER2                1195
#define IDC_EDITFOLDER                  1196
#define IDC_LISTMARKERS                 1200
#define IDC_EDITSTARTTIME               1202
#define IDC_EDITTIMEOFFSET              1203
#define IDC_EDITSPEED                   1204
#define IDC_STACKTREE                   1207
#define IDC_COMBOSCENE                  1208
#define IDC_STATIC_INFO                 1209
#define IDC_STATICWARNING               1210
#define IDC_BUTTON_COLOR                1213
#define IDC_BUTTON_LAYOUT               1215
#define IDC_CHECKUSEALL                 1218
#define IDC_EDIT1                       1222
#define IDC_CHECK_UPDATEIMPORTABLES     1223
#define IDC_EDIT_DATARATE               1223
#define IDC_EDIT_KEYFRAMES              1224
#define IDC_COMBOBUFFER                 1225
#define IDC_COMBO1                      1235
#define IDC_COMBO_AUDIO_COMPRESSOR      1236
#define IDC_LISTLINKS                   1237
#define IDC_COMBO_AUDIO_FORMAT          1237
#define IDC_BUTTON_CONFIGURE            1238
#define IDC_CHECK_DATARATE              1240
#define IDC_CHECK_FORCE_KEYFRAME        1241
#define IDC_COMBO_VIDEO_COMPRESSOR      1243
#define IDC_EDIT_QUALITY                1244
#define IDC_STATIC_REPORT               1245
#define ID_EDIT_DELETE                  32772
#define ID_HELP_PLUGINS                 32773
#define ID_EDIT_PURGE_UNDOSTACK         32774
#define ID_FILE_IMPORT                  32775
#define ID_FILE_EXPORT                  32776
#define ID_VIEW_TIMELINE                32777
#define ID_VIEW_FILEINSPECTOR           32778
#define ID_EDIT_DEMOPROPERTIES          32779
#define ID_VIEW_DEMOPROPERTIES          32780
#define ID_VIEW_GRID                    32781
#define ID_VIEW_ZOOM_100                32784
#define ID_VIEW_ZOOM_200                32785
#define ID_VIEW_ZOOM_300                32786
#define ID_VIEW_ZOOM_400                32787
#define ID_VIEW_ZOOM_50                 32788
#define ID_VIEW_SNAPTOGRID              32789
#define ID_VIEW_GRIDSETTINGS            32790
#define ID_PLAY_PLAY                    32791
#define ID_PLAY_STOP                    32792
#define ID_PLAY_PLAYPREVIEW             32793
#define ID_VIEW_FULLSCREEN              32794
#define ID_FILE_MERGE                   32795
#define ID_TOOL_ARROW                   32798
#define ID_TOOL_ROTATE                  32799
#define ID_TOOL_PAN                     32800
#define ID_TOOL_ZOOM                    32801
#define ID_TOOL_SCALE                   32802
#define ID_TOOL_KEYARROW                32803
#define ID_VIEW_TOOLSTOOLBAR            32804
#define ID_ALIGN_LEFT                   32806
#define ID_HORIZ_ALIGN_CENTER           32807
#define ID_HORIZ_ALIGN_RIGHT            32808
#define ID_VERT_ALIGN_TOP               32809
#define ID_VERT_ALIGN_CENTER            32810
#define ID_VERT_ALIGN_BOTTOM            32811
#define ID_VERT_DIST_HEIGHTS            32812
#define ID_VERT_DIST_TOP                32813
#define ID_VERT_DIST_CENTER             32814
#define ID_VERT_DIST_BOTTOM             32815
#define ID_HORIZ_DIST_WIDTHS            32816
#define ID_HORIS_DIST_LEFT              32817
#define ID_HORIZ_DIST_CENTER            32818
#define ID_HORIZ_DIST_RIGHT             32819
#define ID_ALIGN_TO_PAGE                32820
#define ID_HORIZ_ALIGN_LEFT             32821
#define ID_VIEW_TRANSFORMDIALOG         32822
#define ID_VIEW_ALIGNDIALOG             32823
#define ID_VIEW_HELPDIALOG              32824
#define ID_VIEW_LAYOUTRULERS            32826
#define ID_PLAY_LOOP                    32831
#define ID_PLAY_PLAYMUSIC               32832
#define ID_VIEW_PLAYTOOLBAR             32833
#define ID_FILE_DIRECTORIES             32834
#define ID_EDIT_PROJECTPATH             32836
#define ID_FILE_EXPORTAVI               32837
#define ID_VIEW_PARAMETERS              32838
#define ID_INDICATOR_GRAPHDEVICE        59142
#define ID_INDICATOR_PROJECTPATH        59143
#define ID_TIMELINE                     61204
#define ID_INSPECTOR                    61205
#define IDC_LAYERLISTSCROLL             61206
#define IDC_NEW                         61207
#define IDC_PLAY                        61207
#define IDC_DEL                         61208
#define IDC_PREV                        61208
#define IDC_UP                          61209
#define IDC_FORW                        61209
#define IDC_DOWN                        61210
#define IDC_TIMELINESCROLL              61210
#define IDC_EDITLABEL                   61211
#define IDC_FILEINSPECTOR_LIST          61212
#define IDC_SPINNER1                    61213
#define IDC_SPINNER2                    61214
#define IDC_SPINNER3                    61215
#define IDC_SPINNER4                    61216
#define IDC_FRAME                       61217
#define IDC_BACK                        61218
#define IDC_NEXT                        61219
#define IDW_GRAPHICSDEVICE              61220
#define IDC_FILEINSPECTOR_VSCROLL       61221
#define IDC_FILEINSPECTOR_HSCROLL       61222
#define IDC_CREATELAYER                 61223
#define IDC_CREATEEFFECT                61224
#define IDC_DELETEEFFECT                61225
#define IDC_IMPORTFILE                  61226
#define IDC_CREATEFILE                  61227
#define IDC_CREATESCENE                 61228
#define IDC_DELETEFILE                  61229
#define IDC_TIMEZOOMIN                  61230
#define IDC_TIMEZOOMOUT                 61231
#define IDC_SCENESEL                    61232
#define IDC_FILEDLG_INCBUTTON           61233
#define IDC_CREATEGRAPHBUFFER           61234
#define IDC_CREATEFOLDER                61235
#define ID_PARAMETERS                   61236
#define IDC_PARAMROLLUP                 61237

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        316
#define _APS_NEXT_COMMAND_VALUE         32839
#define _APS_NEXT_CONTROL_VALUE         1246
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
