// Vector3TypeInDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "demopajadoc.h"
#include "Vector3TypeInDlg.h"
#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "ControllerC.h"
#include "KeyC.h"
#include "Vector3C.h"

using namespace PajaTypes;
using namespace Composition;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CVector3TypeInDlg dialog


void
AFXAPI
CVector3TypeInDlg::DDX_MyText( CDataExchange* pDX, int nIDC, float& value )
{
	TCHAR	szBuffer[32];
	TCHAR	szFormat[32];
	HWND	hWndCtrl = pDX->PrepareEditCtrl( nIDC );

	if( pDX->m_bSaveAndValidate ) {
		// to value
		szBuffer[0] = 0;
		::GetWindowText( hWndCtrl, szBuffer, 30 );
		value = _tcstod( szBuffer, NULL );
		value /= m_f32Scale;
	}
	else {
		// from value
		_sntprintf( szFormat, 31, "%%.%df", (int32)__max( -floor( log10( m_f32Inc ) ), 0 ) );
		_sntprintf( szBuffer, 31, szFormat, value * m_f32Scale );
		::SetWindowText( hWndCtrl, szBuffer );
	}
}

CVector3TypeInDlg::CVector3TypeInDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CVector3TypeInDlg::IDD, pParent),
	m_f32Inc( 0.1f ),
	m_bClampValues( false ),
	m_f32Scale( 1.0f )
{
	//{{AFX_DATA_INIT(CVector3TypeInDlg)
	m_f32ValueX = 0.0f;
	m_f32ValueY = 0.0f;
	m_f32ValueZ = 0.0f;
	m_sInfoText = _T("");
	//}}AFX_DATA_INIT
}


void CVector3TypeInDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CVector3TypeInDlg)
	DDX_Control(pDX, IDOK, m_rOk);
	DDX_Control(pDX, IDCANCEL, m_rCancel);
	DDX_Control(pDX, IDC_LABEL2, m_rStaticLabel2);
	DDX_Control(pDX, IDC_LABEL3, m_rStaticLabel3);
	DDX_Control(pDX, IDC_LABEL1, m_rStaticLabel1);
	DDX_Control(pDX, IDC_EDITZ, m_rEditZ);
	DDX_Control(pDX, IDC_EDITY, m_rEditY);
	DDX_Control(pDX, IDC_EDITX, m_rEditX);
	DDX_MyText(pDX, IDC_EDITX, m_f32ValueX);
	DDX_MyText(pDX, IDC_EDITY, m_f32ValueY);
	DDX_MyText(pDX, IDC_EDITZ, m_f32ValueZ);
	DDX_Text(pDX, IDC_INFO, m_sInfoText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CVector3TypeInDlg, CDialog)
	//{{AFX_MSG_MAP(CVector3TypeInDlg)
	ON_EN_CHANGE(IDC_EDITX, OnChangeEditx)
	ON_EN_CHANGE(IDC_EDITY, OnChangeEdity)
	ON_EN_CHANGE(IDC_EDITZ, OnChangeEditz)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CVector3TypeInDlg message handlers

BOOL CVector3TypeInDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if( m_pParam->get_style() & PARAM_STYLE_PERCENT ) {
		m_f32Inc *= 100.0f;
		m_f32Scale = 100.0f;

		m_rMin *= 100.0f;
		m_rMax *= 100.0f;

		m_rStaticLabel1.SetWindowText( "%" );
		m_rStaticLabel2.SetWindowText( "%" );
		m_rStaticLabel3.SetWindowText( "%" );
	}
	else if( m_pParam->get_style() & PARAM_STYLE_ANGLE ) {
		m_rStaticLabel1.SetWindowText( "�" );
		m_rStaticLabel2.SetWindowText( "�" );
		m_rStaticLabel3.SetWindowText( "�" );
	}

	if( m_bClampValues ) {
		if( !m_rSpinnerX.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT | SPNB_USELIMITS, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
			return FALSE;
		m_rSpinnerX.SetMinMax( m_rMin[0], m_rMax[0] );

		if( !m_rSpinnerY.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT | SPNB_USELIMITS, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER2 ) )
			return FALSE;
		m_rSpinnerY.SetMinMax( m_rMin[1], m_rMax[1] );

		if( !m_rSpinnerZ.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT | SPNB_USELIMITS, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER3 ) )
			return FALSE;
		m_rSpinnerZ.SetMinMax( m_rMin[2], m_rMax[2] );
	}
	else {
		if( !m_rSpinnerX.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
			return FALSE;

		if( !m_rSpinnerY.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER2 ) )
			return FALSE;

		if( !m_rSpinnerZ.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER3 ) )
			return FALSE;
	}

	m_rSpinnerX.SetScale( m_f32Inc );
	m_rSpinnerX.SetBuddy( &m_rEditX, SPNB_ATTACH_RIGHT );

	m_rSpinnerY.SetScale( m_f32Inc );
	m_rSpinnerY.SetBuddy( &m_rEditY, SPNB_ATTACH_RIGHT );

	m_rSpinnerZ.SetScale( m_f32Inc );
	m_rSpinnerZ.SetBuddy( &m_rEditZ, SPNB_ATTACH_RIGHT );

	// update dialog data
	Vector3C	rVec;
	m_pParam->get_val( m_i32Time, rVec );
	m_f32ValueX = rVec[0];
	m_f32ValueY = rVec[1];
	m_f32ValueZ = rVec[2];
	UpdateData( FALSE );

	m_rOk.SetFlat( FALSE );
	m_rCancel.SetFlat( FALSE );

	return TRUE;
}

void CVector3TypeInDlg::OnChangeEditx() 
{
	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	UpdateData( TRUE );

	if( m_bClampValues ) {
		// clamp value
		if( m_f32ValueX < m_rMin[0] )
			m_f32ValueX = m_rMin[0];
		if( m_f32ValueX > m_rMax[0] )
			m_f32ValueX = m_rMax[0];
	}

	Vector3C	rVec;
	rVec[0] = m_f32ValueX;
	rVec[1] = m_f32ValueY;
	rVec[2] = m_f32ValueZ;
	pDoc->HandleParamNotify( m_pParam->set_val( m_i32Time, rVec ) );
	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}

void CVector3TypeInDlg::OnChangeEdity() 
{
	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	UpdateData( TRUE );

	if( m_bClampValues ) {
		// clamp value
		if( m_f32ValueY < m_rMin[1] )
			m_f32ValueY = m_rMin[1];
		if( m_f32ValueY > m_rMax[1] )
			m_f32ValueY = m_rMax[1];
	}

	Vector3C	rVec;
	rVec[0] = m_f32ValueX;
	rVec[1] = m_f32ValueY;
	rVec[2] = m_f32ValueZ;
	pDoc->HandleParamNotify( m_pParam->set_val( m_i32Time, rVec ) );
	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}

void CVector3TypeInDlg::OnChangeEditz() 
{
	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	UpdateData( TRUE );

	if( m_bClampValues ) {
		// clamp value
		if( m_f32ValueZ < m_rMin[2] )
			m_f32ValueZ = m_rMin[2];
		if( m_f32ValueZ > m_rMax[2] )
			m_f32ValueZ = m_rMax[2];
	}

	Vector3C	rVec;
	rVec[0] = m_f32ValueX;
	rVec[1] = m_f32ValueY;
	rVec[2] = m_f32ValueZ;
	pDoc->HandleParamNotify( m_pParam->set_val( m_i32Time, rVec ) );
	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}
