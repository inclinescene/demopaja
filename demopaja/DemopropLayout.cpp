// DemopropLayout.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "DemopropLayout.h"
#include "ColorTypeInDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


using namespace PajaTypes;

/////////////////////////////////////////////////////////////////////////////
// CDemopropLayout dialog


CDemopropLayout::CDemopropLayout(CWnd* pParent /*=NULL*/)
	: CPrefSubDlg(CDemopropLayout::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDemopropLayout)
	m_i32Height = 0;
	m_i32Width = 0;
	//}}AFX_DATA_INIT
}


void CDemopropLayout::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDemopropLayout)
	DDX_Text(pDX, IDC_EDIT_HEIGHT, m_i32Height);
	DDX_Text(pDX, IDC_EDIT_WIDTH, m_i32Width);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDemopropLayout, CPrefSubDlg)
	//{{AFX_MSG_MAP(CDemopropLayout)
	ON_BN_CLICKED(IDC_BUTTON_COLOR, OnButtonColor)
	ON_WM_DRAWITEM()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDemopropLayout message handlers

void CDemopropLayout::OnButtonColor() 
{
	CColorTypeInDlg	rDlg;

	rDlg.m_rColorValue = m_rBGColor;
	rDlg.m_sCaption = "Choose Background Color";

	if( rDlg.DoModal() == IDOK ) {
		m_rBGColor = rDlg.m_rColorValue;
		Invalidate();
	}
}

inline
COLORREF
color_to_COLOREF( const ColorC& rCol )
{
	return RGB( (int32)(rCol[0] * 255.0f), (int32)(rCol[1] * 255.0f), (int32)(rCol[2] * 255.0f) );
}

void CDemopropLayout::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if( nIDCtl == IDC_BUTTON_COLOR ) {
		HBRUSH	rColorBrush = ::CreateSolidBrush( color_to_COLOREF( m_rBGColor ) );
		::FillRect( lpDrawItemStruct->hDC, &lpDrawItemStruct->rcItem, rColorBrush );
		::DeleteObject( rColorBrush );
		if( lpDrawItemStruct->itemState == ODS_FOCUS ) {
			RECT	rFocusRect = lpDrawItemStruct->rcItem;
			::InflateRect( &rFocusRect, -2, -2 );
			::DrawFocusRect( lpDrawItemStruct->hDC, &rFocusRect );
		}
	}
	else
		CPrefSubDlg::OnDrawItem(nIDCtl, lpDrawItemStruct);
}
