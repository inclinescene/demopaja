#if !defined(AFX_INTTYPEINDLG_H__41ADC292_8879_43A1_BD78_06A8824C186D__INCLUDED_)
#define AFX_INTTYPEINDLG_H__41ADC292_8879_43A1_BD78_06A8824C186D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IntTypeInDlg.h : header file
//

#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "ParamI.h"
#include "BtnST.h"

/////////////////////////////////////////////////////////////////////////////
// CIntTypeInDlg dialog

class CIntTypeInDlg : public CDialog
{
// Construction
public:
	CIntTypeInDlg(CWnd* pParent = NULL);   // standard constructor

	PajaTypes::int32			m_i32Min, m_i32Max;
	bool						m_bClampValues;
	PajaTypes::int32			m_i32Inc;
	Composition::ParamIntC*		m_pParam;
	PajaTypes::int32			m_i32Time;

	// Dialog Data
	//{{AFX_DATA(CIntTypeInDlg)
	enum { IDD = IDD_INT_TYPEIN };
	CButtonST	m_rOk;
	CButtonST	m_rCancel;
	CEdit	m_rEdit;
	int		m_i32Value;
	CString	m_sInfoText;
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIntTypeInDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:

	CSpinnerButton	m_rSpinner;

	void AFXAPI		DDX_MyText( CDataExchange* pDX, int nIDC, int& value );

	// Generated message map functions
	//{{AFX_MSG(CIntTypeInDlg)
	afx_msg void OnChangeEdit();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INTTYPEINDLG_H__41ADC292_8879_43A1_BD78_06A8824C186D__INCLUDED_)
