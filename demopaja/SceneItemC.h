//-------------------------------------------------------------------------
//
// File:		SceneItemC.h
// Desc:		Scene item class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2003 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://moppi.inside.org/demopaja/
//-------------------------------------------------------------------------

#ifndef __SCENEITEMC_H__
#define __SCENEITEMC_H__

#include "PajaTypes.h"
#include "Composition.h"
#include "SceneC.h"
#include "LayerC.h"
#include "EffectI.h"
#include "GizmoI.h"
#include "ParamI.h"
#include <assert.h>


class ControllerSampleDataC
{
public:
	ControllerSampleDataC();

	virtual ~ControllerSampleDataC();

	PajaTypes::uint32		get_sample_count();
	PajaTypes::float32*		get_sample( PajaTypes::uint32 ui32Num );
	PajaTypes::float32		get_min() const;
	PajaTypes::float32		get_max() const;
	void					clear();
	void					sample_controller( Composition::ParamI* pParam, PajaTypes::int32 i32NumValues, PajaTypes::int32 i32MinTime, PajaTypes::int32 i32TimeStep, bool bUpdateMinMax );

private:
	PajaTypes::float32				m_f32Min, m_f32Max;
	std::vector<PajaTypes::float32>	m_rSamples;
	PajaTypes::uint32				m_ui32NumCh;
};



enum SceneItemTypeE {
	SCENEITEM_UNKNOWN = 0,
	SCENEITEM_LAYER,
	SCENEITEM_EFFECT,
	SCENEITEM_GIZMO,
	SCENEITEM_PARAMETER,
};

class SceneItemC
{
public:
	SceneItemC();
	virtual ~SceneItemC();

	void						init( Composition::LayerC* pLayer );
	void						init( Composition::EffectI* pEffect );
	void						init( Composition::GizmoI* pGizmo );
	void						init( Composition::ParamI* pParam );

	PajaTypes::uint32			get_type() const;

	void						set_x( PajaTypes::int32 i32X );
	PajaTypes::int32			get_x();
	void						set_y( PajaTypes::int32 i32Y );
	PajaTypes::int32			get_y();
	void						set_height( PajaTypes::int32 i32Height );
	PajaTypes::int32			get_height();
	void						set_flags( PajaTypes::int32 i32Flags );
	PajaTypes::int32			get_flags();

	ControllerSampleDataC*		get_controller_sampledata();
	void						delete_controller_sampledata();

	void						sample_controller( PajaTypes::int32 i32NumValues, PajaTypes::int32 i32MinTime, PajaTypes::int32 i32TimeStep, bool bUpdateMinMax );

	Composition::SceneC*		get_scene();
	Composition::LayerC*		get_layer();
	Composition::EffectI*		get_effect();
	Composition::GizmoI*		get_gizmo();
	Composition::ParamI*		get_parameter();

	PajaTypes::int32			get_layer_index();
	PajaTypes::int32			get_effect_index();
	PajaTypes::int32			get_gizmo_index();
	PajaTypes::int32			get_parameter_index();

	bool						operator==( const SceneItemC& rItem ) const;


protected:

	// Set unused indices to -1
	void*					m_pPtr;
	PajaTypes::uint32		m_ui32Type;
	PajaTypes::int32		m_i32X, m_i32Y;
	PajaTypes::int32		m_i32Height;
	PajaTypes::int32		m_i32Flags;
	ControllerSampleDataC	m_rContData;
};


#endif // __SCENEITEMC_H__