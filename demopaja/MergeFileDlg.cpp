// MergeFileDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "MergeFileDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMergeFileDlg dialog


CMergeFileDlg::CMergeFileDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMergeFileDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMergeFileDlg)
	m_bNewScene = FALSE;
	m_sFolder = _T("");
	m_iDestFolder = -1;
	m_bUseFiles = FALSE;
	//}}AFX_DATA_INIT
}


void CMergeFileDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMergeFileDlg)
	DDX_Control(pDX, IDC_EDITFOLDER, m_rEditFolder);
	DDX_Check(pDX, IDC_CHECKNEWSCENE, m_bNewScene);
	DDX_Text(pDX, IDC_EDITFOLDER, m_sFolder);
	DDX_Radio(pDX, IDC_RADIOFOLDER, m_iDestFolder);
	DDX_Check(pDX, IDC_CHECKUSEFILES, m_bUseFiles);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMergeFileDlg, CDialog)
	//{{AFX_MSG_MAP(CMergeFileDlg)
	ON_BN_CLICKED(IDC_RADIOFOLDER, OnRadiofolder)
	ON_BN_CLICKED(IDC_RADIOFOLDER2, OnRadiofolder2)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMergeFileDlg message handlers

void CMergeFileDlg::OnRadiofolder() 
{
	UpdateData( TRUE );
	if( m_iDestFolder == 1 )
		m_rEditFolder.EnableWindow( TRUE );
	else
		m_rEditFolder.EnableWindow( FALSE );
}

void CMergeFileDlg::OnRadiofolder2() 
{
	UpdateData( TRUE );
	if( m_iDestFolder == 1 )
		m_rEditFolder.EnableWindow( TRUE );
	else
		m_rEditFolder.EnableWindow( FALSE );
}

BOOL CMergeFileDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if( m_iDestFolder == 1 )
		m_rEditFolder.EnableWindow( TRUE );
	else
		m_rEditFolder.EnableWindow( FALSE );
	
	return TRUE;
}
