#if !defined(AFX_TIMESEGMENTTYPEINDLG_H__29FC74A8_C48B_48EC_87DA_B863819D9208__INCLUDED_)
#define AFX_TIMESEGMENTTYPEINDLG_H__29FC74A8_C48B_48EC_87DA_B863819D9208__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// TimesegmentTypeInDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CTimesegmentTypeInDlg dialog

#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "TimeSegmentC.h"
#include "BtnST.h"


class CTimesegmentTypeInDlg : public CDialog
{
// Construction
public:
	CTimesegmentTypeInDlg(CWnd* pParent = NULL);   // standard constructor

	Composition::TimeSegmentC*	m_pTimeSegment;
	PajaTypes::int32			m_i32Time;
	PajaTypes::int32			m_i32TimeOffset;
	PajaTypes::int32			m_i32BeatsPerMin;
	PajaTypes::int32			m_i32QNotesPerBeat;
	PajaTypes::int32			m_i32EditAccuracy;
	PajaTypes::int32			m_i32Flags;

// Dialog Data
	//{{AFX_DATA(CTimesegmentTypeInDlg)
	enum { IDD = IDD_TIMESEGMENT_TYPEIN };
	CButtonST	m_rOk;
	CButtonST	m_rCancel;
	CStatic	m_rStaticStartTime;
	CStatic	m_rStaticEndTime;
	CEdit	m_rEditKey;
	CStatic	m_rStaticSegment;
	CStatic	m_rStaticOrigoTime;
	CStatic	m_rStaticKeyTime;
	CStatic	m_rStaticKeyCount;
	CStatic	m_rStaticDurationTime;
	CStatic	m_rStaticDurationFrames;
	CEdit	m_rEditTime;
	CEdit	m_rEditOrigo;
	int		m_i32Key;
	int		m_i32Origo;
	int		m_i32KeyTime;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTimesegmentTypeInDlg)
	public:
	virtual BOOL Create(LPCTSTR lpszClassName, LPCTSTR lpszWindowName, DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID, CCreateContext* pContext = NULL);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSpinnerButton		m_rSpinnerOrigo;
	CSpinnerButton		m_rSpinnerKey;
	CSpinnerButton		m_rSpinnerKeyTime;
	CImageList			m_rImageList;
	bool				m_bUpdating;
	Composition::KeyC*	m_pCurKey;
	bool				m_bUpdatingOrigo;
	bool				m_bUpdatingKey;
	bool				m_bUpdatingKeyTime;

//	void			UpdateList();
	void			UpdateKey();
	void			UpdateDuration();
	CString			FormatTime( PajaTypes::int32 i32Time );
	void			Redraw();
	void			UpdateLabels();

	void AFXAPI		DDX_MyText( CDataExchange* pDX, int nIDC, int& value );

	// Generated message map functions
	//{{AFX_MSG(CTimesegmentTypeInDlg)
	afx_msg void OnChangeEditorigo();
	afx_msg void OnChangeEdittime();
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg void OnChangeEditkey();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TIMESEGMENTTYPEINDLG_H__29FC74A8_C48B_48EC_87DA_B863819D9208__INCLUDED_)
