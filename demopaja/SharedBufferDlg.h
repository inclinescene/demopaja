#pragma once
#include "afxwin.h"


// CSharedBufferDlg dialog

class CSharedBufferDlg : public CDialog
{
	DECLARE_DYNAMIC(CSharedBufferDlg)

public:
	CSharedBufferDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CSharedBufferDlg();

// Dialog Data
	enum { IDD = IDD_SHARED_BUFFER_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	CString m_sName;
	int m_iWidth;
	int m_iHeight;
};
