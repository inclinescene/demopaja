# Microsoft Developer Studio Project File - Name="demopaja" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=demopaja - Win32 MFC Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "demopaja.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "demopaja.mak" CFG="demopaja - Win32 MFC Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "demopaja - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "demopaja - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE "demopaja - Win32 MFC Debug" (based on "Win32 (x86) Application")
!MESSAGE "demopaja - Win32 MFC Release" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "demopaja - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".\corelib" /I "F:\Code and Docs\Libraries\lcms108a\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40b /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x40b /d "NDEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "demopaja - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "demopaja___Win32_Debug"
# PROP BASE Intermediate_Dir "demopaja___Win32_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 7
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "demopaja___Win32_Debug"
# PROP Intermediate_Dir "demopaja___Win32_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /I ".\corelib" /I "F:\Code and Docs\Libraries\lcms108a\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40b /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x40b /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ELSEIF  "$(CFG)" == "demopaja - Win32 MFC Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "demopaja___Win32_MFC_Debug"
# PROP BASE Intermediate_Dir "demopaja___Win32_MFC_Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "MFC_Debug"
# PROP Intermediate_Dir "MFC_Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /FR /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "F:\Code and Docs\Libraries\fmodapi340\api\inc" /I ".\corelib" /I "F:\Code and Docs\Libraries\lcms108a\include" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /FR /FD /GZ /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40b /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x40b /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 winmm.lib vfw32.lib fmodvc.lib glu32.lib opengl32.lib /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept /libpath:"F:\Code and Docs\Libraries\fmodapi340\api\lib"

!ELSEIF  "$(CFG)" == "demopaja - Win32 MFC Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "demopaja___Win32_MFC_Release"
# PROP BASE Intermediate_Dir "demopaja___Win32_MFC_Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "MFC_Release"
# PROP Intermediate_Dir "MFC_Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "F:\Code and Docs\Libraries\fmodapi340\api\inc" /I ".\corelib" /I "F:\Code and Docs\Libraries\lcms108a\include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40b /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x40b /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 winmm.lib vfw32.lib fmodvc.lib /nologo /subsystem:windows /machine:I386 /libpath:"F:\Code and Docs\Libraries\fmodapi340\api\lib"
# SUBTRACT LINK32 /debug

!ENDIF 

# Begin Target

# Name "demopaja - Win32 Release"
# Name "demopaja - Win32 Debug"
# Name "demopaja - Win32 MFC Debug"
# Name "demopaja - Win32 MFC Release"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AlignDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ArrowButton.cpp
# End Source File
# Begin Source File

SOURCE=.\AVIWriterC.cpp
# End Source File
# Begin Source File

SOURCE=.\BaseTabCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\BtnST.cpp
# End Source File
# Begin Source File

SOURCE=.\ChooseImporterDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorButton.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorCommonDialogC.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorPickerCB.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorSwatchSetC.cpp
# End Source File
# Begin Source File

SOURCE=.\ColorTypeInDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\CommonDirsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\demopaja.cpp
# End Source File
# Begin Source File

SOURCE=.\demopaja.rc
# End Source File
# Begin Source File

SOURCE=.\demopajaDoc.cpp
# End Source File
# Begin Source File

SOURCE=.\demopajaView.cpp
# End Source File
# Begin Source File

SOURCE=.\DemopropComment.cpp
# End Source File
# Begin Source File

SOURCE=.\DemopropDevices.cpp
# End Source File
# Begin Source File

SOURCE=.\DemoPropDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DemopropLayout.cpp
# End Source File
# Begin Source File

SOURCE=.\DemopropMusic.cpp
# End Source File
# Begin Source File

SOURCE=.\DemopropTiming.cpp
# End Source File
# Begin Source File

SOURCE=.\DIB.cpp
# End Source File
# Begin Source File

SOURCE=.\DLLInfoDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DocManagerEx.cpp
# End Source File
# Begin Source File

SOURCE=.\DynDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\EnTabCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\ExportAVIDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FileDialogEx.cpp
# End Source File
# Begin Source File

SOURCE=.\FileSubClassWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\FileTypeInDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FlatPopupMenu.cpp
# End Source File
# Begin Source File

SOURCE=.\FloatTypeInDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\FolderDialog.cpp
# End Source File
# Begin Source File

SOURCE=.\FolderPropertiesDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\GridSettingsDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\HelpDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\InspectorBar.cpp
# End Source File
# Begin Source File

SOURCE=.\IntComboTypeInDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\IntTypeInDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ItemDragTarget.cpp
# End Source File
# Begin Source File

SOURCE=.\KeySelectorC.cpp
# End Source File
# Begin Source File

SOURCE=.\logger\logger.cpp
# End Source File
# Begin Source File

SOURCE=.\MainFrm.cpp
# End Source File
# Begin Source File

SOURCE=.\MarkersDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MarkerTypeInDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MergeFileDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MoveDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\MusicStartTimeDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ParameterBar.cpp
# End Source File
# Begin Source File

SOURCE=.\PrefSubDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ProgressWnd.cpp
# End Source File
# Begin Source File

SOURCE=.\RecursionErrorDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\RollupCtrl.cpp
# End Source File
# Begin Source File

SOURCE=.\RotateDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\ScaleDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\scbarcf.cpp
# End Source File
# Begin Source File

SOURCE=.\scbarg.cpp
# End Source File
# Begin Source File

SOURCE=.\SceneItemC.cpp
# End Source File
# Begin Source File

SOURCE=.\SceneItemListC.cpp
# End Source File
# Begin Source File

SOURCE=.\ScenePropDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\SceneToImageDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\sizecbar.cpp
# End Source File
# Begin Source File

SOURCE=.\SpinnerButton.cpp
# End Source File
# Begin Source File

SOURCE=.\StartDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\SubSceneC.cpp
# End Source File
# Begin Source File

SOURCE=.\TextTypeInDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TimeLineBar.cpp
# End Source File
# Begin Source File

SOURCE=.\TimesegmentTypeInDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\TransformDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\UndoManagerC.cpp
# End Source File
# Begin Source File

SOURCE=.\Vector2TypeInDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\Vector3TypeInDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\webbrowser2.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AlignDlg.h
# End Source File
# Begin Source File

SOURCE=.\ArrowButton.h
# End Source File
# Begin Source File

SOURCE=.\AVIWriterC.h
# End Source File
# Begin Source File

SOURCE=.\BaseTabCtrl.h
# End Source File
# Begin Source File

SOURCE=.\BtnST.h
# End Source File
# Begin Source File

SOURCE=.\ChooseImporterDlg.h
# End Source File
# Begin Source File

SOURCE=.\ColorButton.h
# End Source File
# Begin Source File

SOURCE=.\ColorPickerCB.h
# End Source File
# Begin Source File

SOURCE=.\ColorSwatchSetC.h
# End Source File
# Begin Source File

SOURCE=.\ColorTypeInDlg.h
# End Source File
# Begin Source File

SOURCE=.\CommonDirsDlg.h
# End Source File
# Begin Source File

SOURCE=.\demopaja.h
# End Source File
# Begin Source File

SOURCE=.\demopajaDoc.h
# End Source File
# Begin Source File

SOURCE=.\DemopajaVersion.h
# End Source File
# Begin Source File

SOURCE=.\demopajaView.h
# End Source File
# Begin Source File

SOURCE=.\DemopropComment.h
# End Source File
# Begin Source File

SOURCE=.\DemopropDevices.h
# End Source File
# Begin Source File

SOURCE=.\DemoPropDlg.h
# End Source File
# Begin Source File

SOURCE=.\DemopropLayout.h
# End Source File
# Begin Source File

SOURCE=.\DemopropMusic.h
# End Source File
# Begin Source File

SOURCE=.\DemopropTiming.h
# End Source File
# Begin Source File

SOURCE=.\DIB.h
# End Source File
# Begin Source File

SOURCE=.\DLLInfoDlg.h
# End Source File
# Begin Source File

SOURCE=.\DocManagerEx.h
# End Source File
# Begin Source File

SOURCE=.\DynDialog.h
# End Source File
# Begin Source File

SOURCE=.\EnTabCtrl.h
# End Source File
# Begin Source File

SOURCE=.\ExportAVIDlg.h
# End Source File
# Begin Source File

SOURCE=.\FileDialogEx.h
# End Source File
# Begin Source File

SOURCE=.\FileSubClassWnd.h
# End Source File
# Begin Source File

SOURCE=.\FileTypeInDlg.h
# End Source File
# Begin Source File

SOURCE=.\FlatPopupMenu.h
# End Source File
# Begin Source File

SOURCE=.\FloatTypeInDlg.h
# End Source File
# Begin Source File

SOURCE=.\FolderDialog.h
# End Source File
# Begin Source File

SOURCE=.\FolderPropertiesDlg.h
# End Source File
# Begin Source File

SOURCE=.\GridSettingsDlg.h
# End Source File
# Begin Source File

SOURCE=.\HelpDlg.h
# End Source File
# Begin Source File

SOURCE=.\InspectorBar.h
# End Source File
# Begin Source File

SOURCE=.\IntComboTypeInDlg.h
# End Source File
# Begin Source File

SOURCE=.\IntTypeInDlg.h
# End Source File
# Begin Source File

SOURCE=.\ItemDragTarget.h
# End Source File
# Begin Source File

SOURCE=.\KeySelectorC.h
# End Source File
# Begin Source File

SOURCE=.\logger\logger.h
# End Source File
# Begin Source File

SOURCE=.\MainFrm.h
# End Source File
# Begin Source File

SOURCE=.\MarkersDlg.h
# End Source File
# Begin Source File

SOURCE=.\MarkerTypeInDlg.h
# End Source File
# Begin Source File

SOURCE=.\MergeFileDlg.h
# End Source File
# Begin Source File

SOURCE=.\MoveDlg.h
# End Source File
# Begin Source File

SOURCE=.\MusicStartTimeDlg.h
# End Source File
# Begin Source File

SOURCE=.\ParameterBar.h
# End Source File
# Begin Source File

SOURCE=.\PrefSubDlg.h
# End Source File
# Begin Source File

SOURCE=.\ProgressWnd.h
# End Source File
# Begin Source File

SOURCE=.\RecursionErrorDlg.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\RollupCtrl.h
# End Source File
# Begin Source File

SOURCE=.\RotateDlg.h
# End Source File
# Begin Source File

SOURCE=.\ScaleDlg.h
# End Source File
# Begin Source File

SOURCE=.\scbarcf.h
# End Source File
# Begin Source File

SOURCE=.\scbarg.h
# End Source File
# Begin Source File

SOURCE=.\SceneItemC.h
# End Source File
# Begin Source File

SOURCE=.\SceneItemListC.h
# End Source File
# Begin Source File

SOURCE=.\ScenePropDlg.h
# End Source File
# Begin Source File

SOURCE=.\SceneToImageDlg.h
# End Source File
# Begin Source File

SOURCE=.\sizecbar.h
# End Source File
# Begin Source File

SOURCE=.\SpinnerButton.h
# End Source File
# Begin Source File

SOURCE=.\StartDlg.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\SubSceneC.h
# End Source File
# Begin Source File

SOURCE=.\TextTypeInDlg.h
# End Source File
# Begin Source File

SOURCE=.\TimeLineBar.h
# End Source File
# Begin Source File

SOURCE=.\TimesegmentTypeInDlg.h
# End Source File
# Begin Source File

SOURCE=.\TransformDlg.h
# End Source File
# Begin Source File

SOURCE=.\UndoManagerC.h
# End Source File
# Begin Source File

SOURCE=.\Vector2TypeInDlg.h
# End Source File
# Begin Source File

SOURCE=.\Vector3TypeInDlg.h
# End Source File
# Begin Source File

SOURCE=.\webbrowser2.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\3d_pajalogo.bmp
# End Source File
# Begin Source File

SOURCE=.\res\adjusty.cur
# End Source File
# Begin Source File

SOURCE=.\res\align.bmp
# End Source File
# Begin Source File

SOURCE=.\res\arrange.bmp
# End Source File
# Begin Source File

SOURCE=.\res\back.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00001.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bmp00002.bmp
# End Source File
# Begin Source File

SOURCE=.\res\closebut.bmp
# End Source File
# Begin Source File

SOURCE=.\res\colorpic.cur
# End Source File
# Begin Source File

SOURCE=.\res\colorramp.bmp
# End Source File
# Begin Source File

SOURCE=.\res\colorramp_small.bmp
# End Source File
# Begin Source File

SOURCE=.\res\comdir_add.bmp
# End Source File
# Begin Source File

SOURCE=.\res\comdir_del.bmp
# End Source File
# Begin Source File

SOURCE=.\res\comdir_down.bmp
# End Source File
# Begin Source File

SOURCE=.\res\comdir_modify.bmp
# End Source File
# Begin Source File

SOURCE=.\res\comdir_up.bmp
# End Source File
# Begin Source File

SOURCE=.\res\controll.bmp
# End Source File
# Begin Source File

SOURCE=.\res\createbu.bmp
# End Source File
# Begin Source File

SOURCE=.\res\createefffect.bmp
# End Source File
# Begin Source File

SOURCE=.\res\createfile.bmp
# End Source File
# Begin Source File

SOURCE=.\res\createfolder.bmp
# End Source File
# Begin Source File

SOURCE=.\res\createlayer.bmp
# End Source File
# Begin Source File

SOURCE=.\res\createscene.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cur00001.cur
# End Source File
# Begin Source File

SOURCE=.\res\cur00002.cur
# End Source File
# Begin Source File

SOURCE=.\res\cursel.bmp
# End Source File
# Begin Source File

SOURCE=.\res\cursor1.cur
# End Source File
# Begin Source File

SOURCE=.\res\del.bmp
# End Source File
# Begin Source File

SOURCE=.\res\demopaja.ico
# End Source File
# Begin Source File

SOURCE=.\res\demopaja.rc2
# End Source File
# Begin Source File

SOURCE=.\res\demopajaDoc.ico
# End Source File
# Begin Source File

SOURCE=.\res\demopajalogo.gif
# End Source File
# Begin Source File

SOURCE=.\res\down.bmp
# End Source File
# Begin Source File

SOURCE=.\res\dropdown.bmp
# End Source File
# Begin Source File

SOURCE=.\res\eyeright.bmp
# End Source File
# Begin Source File

SOURCE=.\res\filelist.bmp
# End Source File
# Begin Source File

SOURCE=.\res\filelistarr.bmp
# End Source File
# Begin Source File

SOURCE=.\res\filetype.bmp
# End Source File
# Begin Source File

SOURCE=.\res\finger.cur
# End Source File
# Begin Source File

SOURCE=.\res\folders.bmp
# End Source File
# Begin Source File

SOURCE=.\res\forw.bmp
# End Source File
# Begin Source File

SOURCE=.\res\gizmoright.bmp
# End Source File
# Begin Source File

SOURCE=.\res\graphbuf_center.bmp
# End Source File
# Begin Source File

SOURCE=.\res\graphbuf_crop.bmp
# End Source File
# Begin Source File

SOURCE=.\res\graphbuf_fit.bmp
# End Source File
# Begin Source File

SOURCE=.\res\hand.cur
# End Source File
# Begin Source File

SOURCE=.\res\handdrag.cur
# End Source File
# Begin Source File

SOURCE=.\res\help.bmp
# End Source File
# Begin Source File

SOURCE=.\res\help_next.bmp
# End Source File
# Begin Source File

SOURCE=.\res\help_open.bmp
# End Source File
# Begin Source File

SOURCE=.\res\help_prev.bmp
# End Source File
# Begin Source File

SOURCE=.\res\help_reload.bmp
# End Source File
# Begin Source File

SOURCE=.\res\help_stop.bmp
# End Source File
# Begin Source File

SOURCE=.\res\helphome.bmp
# End Source File
# Begin Source File

SOURCE=.\res\helpnext.bmp
# End Source File
# Begin Source File

SOURCE=.\res\helpopen.bmp
# End Source File
# Begin Source File

SOURCE=.\res\helpprev.bmp
# End Source File
# Begin Source File

SOURCE=.\res\helprefresh.bmp
# End Source File
# Begin Source File

SOURCE=.\res\helpstop.bmp
# End Source File
# Begin Source File

SOURCE=.\res\home.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ico00001.ico
# End Source File
# Begin Source File

SOURCE=.\res\icon1.ico
# End Source File
# Begin Source File

SOURCE=.\res\iconwarning.ico
# End Source File
# Begin Source File

SOURCE=.\res\importfile.bmp
# End Source File
# Begin Source File

SOURCE=.\res\keys.bmp
# End Source File
# Begin Source File

SOURCE=.\res\layerright.bmp
# End Source File
# Begin Source File

SOURCE=.\res\leyerrig.bmp
# End Source File
# Begin Source File

SOURCE=.\res\lockrigh.bmp
# End Source File
# Begin Source File

SOURCE=.\res\marker.bmp
# End Source File
# Begin Source File

SOURCE=.\res\minimized.bmp
# End Source File
# Begin Source File

SOURCE=.\res\minimized_checked.bmp
# End Source File
# Begin Source File

SOURCE=.\res\move_key.cur
# End Source File
# Begin Source File

SOURCE=.\res\move_tim.cur
# End Source File
# Begin Source File

SOURCE=.\res\movekeyvalue.cur
# End Source File
# Begin Source File

SOURCE=.\res\movekeyxy.cur
# End Source File
# Begin Source File

SOURCE=.\res\new.bmp
# End Source File
# Begin Source File

SOURCE=.\res\new.ico
# End Source File
# Begin Source File

SOURCE=.\res\next.bmp
# End Source File
# Begin Source File

SOURCE=.\res\next_pag.bmp
# End Source File
# Begin Source File

SOURCE=.\res\nu_logo_splash.bmp
# End Source File
# Begin Source File

SOURCE=.\res\ortypes.bmp
# End Source File
# Begin Source File

SOURCE=.\res\play.bmp
# End Source File
# Begin Source File

SOURCE=.\res\playbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\prev.bmp
# End Source File
# Begin Source File

SOURCE=.\res\recursio.bmp
# End Source File
# Begin Source File

SOURCE=.\res\scenetab.bmp
# End Source File
# Begin Source File

SOURCE=.\res\sizebot.cur
# End Source File
# Begin Source File

SOURCE=.\res\sizetop.cur
# End Source File
# Begin Source File

SOURCE=.\res\sizex.cur
# End Source File
# Begin Source File

SOURCE=.\res\sizey.cur
# End Source File
# Begin Source File

SOURCE=.\res\splat_screen.bmp
# End Source File
# Begin Source File

SOURCE=.\res\stop.bmp
# End Source File
# Begin Source File

SOURCE=.\res\stop.cur
# End Source File
# Begin Source File

SOURCE=.\res\swatchmenu.bmp
# End Source File
# Begin Source File

SOURCE=.\res\test.bmp
# End Source File
# Begin Source File

SOURCE=.\res\timeline.bmp
# End Source File
# Begin Source File

SOURCE=.\res\timeruler.bmp
# End Source File
# Begin Source File

SOURCE=.\res\timescal.bmp
# End Source File
# Begin Source File

SOURCE=.\res\timeseg.bmp
# End Source File
# Begin Source File

SOURCE=.\res\timesegt.bmp
# End Source File
# Begin Source File

SOURCE=.\res\timezoomin.bmp
# End Source File
# Begin Source File

SOURCE=.\res\timezoomout.bmp
# End Source File
# Begin Source File

SOURCE=.\res\Toolbar.bmp
# End Source File
# Begin Source File

SOURCE=.\res\toolbar1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tools.bmp
# End Source File
# Begin Source File

SOURCE=.\res\transform.bmp
# End Source File
# Begin Source File

SOURCE=.\res\trashbin.bmp
# End Source File
# Begin Source File

SOURCE=.\res\tripoint.bmp
# End Source File
# Begin Source File

SOURCE=.\res\up.bmp
# End Source File
# Begin Source File

SOURCE=.\res\waveright.bmp
# End Source File
# Begin Source File

SOURCE=.\res\zoom.cur
# End Source File
# End Group
# Begin Source File

SOURCE=.\demopaja.reg
# End Source File
# Begin Source File

SOURCE=.\res\helphome.htm
# End Source File
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=.\res\sceneeffect.htm
# End Source File
# Begin Source File

SOURCE=.\res\sceneimport.htm
# End Source File
# Begin Source File

SOURCE=.\res\scenetoimageimport.html
# End Source File
# End Target
# End Project
# Section demopaja : {8856F961-340A-11D0-A96B-00C04FD705A2}
# 	2:21:DefaultSinkHeaderFile:webbrowser2.h
# 	2:16:DefaultSinkClass:CWebBrowser2
# End Section
# Section demopaja : {D30C1661-CDAF-11D0-8A3E-00C04FC9E26E}
# 	2:5:Class:CWebBrowser2
# 	2:10:HeaderFile:webbrowser2.h
# 	2:8:ImplFile:webbrowser2.cpp
# End Section
