
#define WIN32_LEAN_AND_MEAN     // Exclude rarely-used stuff from

// Windows headers
#include <windows.h>
#include <commctrl.h>
#include <gl\gl.h>
#include <gl\glu.h>


//#include "logger\logger.h"

// Demopaja headers
#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "ClassDescC.h"
#include "OpenGLDeviceC.h"
#include "OpenGLDriver.h"

using namespace PajaTypes;
using namespace PluginClass;
using namespace PajaSystem;
using namespace Edit;



//////////////////////////////////////////////////////////////////////////
//
//  OpenGL Device Driver class descriptor.
//

OpenGLDeviceDescC::OpenGLDeviceDescC()
{
	// empty
}

OpenGLDeviceDescC::~OpenGLDeviceDescC()
{
	// empty
}

void*
OpenGLDeviceDescC::create()
{
	return OpenGLDeviceC::create_new();
}

int32
OpenGLDeviceDescC::get_classtype() const
{
	return CLASS_TYPE_DEVICEDRIVER;
}

SuperClassIdC
OpenGLDeviceDescC::get_super_class_id() const
{
	return SUPERCLASS_GRAPHICSDEVICE;
}

ClassIdC
OpenGLDeviceDescC::get_class_id() const
{
	return CLASS_OPENGL_DEVICEDRIVER;
}

const char*
OpenGLDeviceDescC::get_name() const
{
	return "OpenGL Device Driver";
}

const char*
OpenGLDeviceDescC::get_desc() const
{
	return "OpenGL Device Driver";
}

const char*
OpenGLDeviceDescC::get_author_name() const
{
	return "Mikko \"memon\" Mononen";
}

const char*
OpenGLDeviceDescC::get_copyright_message() const
{
	return "Copyright (c) 2001 Moppi Productions";
}

const char*
OpenGLDeviceDescC::get_url() const
{
	return "http://moppi.inside.org/demopaja/";
}

const char*
OpenGLDeviceDescC::get_help_filename() const
{
	return "res://OpenGLDeviceHelp.html";
}

uint32
OpenGLDeviceDescC::get_ext_count() const
{
	return 0;
}

const char*
OpenGLDeviceDescC::get_ext( uint32 ui32Index ) const
{
	return 0;
}

uint32
OpenGLDeviceDescC::get_required_device_driver_count() const
{
	return 0;
}

const ClassIdC&
OpenGLDeviceDescC::get_required_device_driver( uint32 ui32Idx )
{
	return NULL_CLASSID;
}


//////////////////////////////////////////////////////////////////////////
//
// Global descriptors, which will be returned to the Demopaja.
//

OpenGLDeviceDescC		g_rOpenGLDeviceDesc;


#ifndef PAJAPLAYER

//////////////////////////////////////////////////////////////////////////
//
// The DLL
//


HINSTANCE	g_hInstance = 0;
bool		g_bControlsInit = false;

BOOL APIENTRY
DllMain( HANDLE hModule, DWORD ulReasonForCall, LPVOID lpReserved )
{
	g_hInstance = (HINSTANCE)hModule;

	if( !g_bControlsInit ) {
		// Initialise common controls
		INITCOMMONCONTROLSEX	rInitCtrls;
		ZeroMemory( &rInitCtrls, sizeof( rInitCtrls ) );
		rInitCtrls.dwSize = sizeof( rInitCtrls );
		rInitCtrls.dwICC = ICC_WIN95_CLASSES;
        InitCommonControlsEx( &rInitCtrls );
		g_bControlsInit = true;

//		logger.logFile() = "opengldriver.log";
//		logger.start( true );
	}

	return TRUE;
}

// Returns number of classes inside this plugin DLL.
__declspec( dllexport )
int32
get_classdesc_count()
{
	return 1;
}

// Returns class descriptors of the plugin classes.
__declspec( dllexport )
ClassDescC*
get_classdesc( int32 i )
{
	if( i == 0 )
		return &g_rOpenGLDeviceDesc;
	return 0;
}

// Returns the API version this DLL was made with.
__declspec( dllexport )
int32
get_api_version()
{
	return DEMOPAJA_VERSION;
}

// Returns the DLL name.
__declspec( dllexport )
char*
get_dll_name()
{
	return "opengldevice.dll - OpenGL Device Driver (c) 2001 memon/moppi productions";
}


#endif	// PAJAPLAYER
