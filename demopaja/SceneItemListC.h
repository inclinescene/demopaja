#ifndef __DEMOPAJA_SCENEITEMLISTC_H__
#define __DEMOPAJA_SCENEITEMLISTC_H__


#include "PajaTypes.h"
#include "SceneItemC.h"
#include "SceneC.h"
#include "LayerC.h"
#include "EffectI.h"
#include "GizmoI.h"
#include "ParamI.h"


class SceneItemListC
{
public:
	SceneItemListC();
	virtual ~SceneItemListC();

	void						build_sceneitem_list( Composition::SceneC* pScene );

	PajaTypes::uint32			get_sceneitem_count();
	SceneItemC*					get_sceneitem( PajaTypes::uint32 ui32Index );
	PajaTypes::int32			get_list_height();
	void						set_item_height( PajaTypes::int32 i32Height );
	PajaTypes::int32			get_item_height();
	void						expand_item( SceneItemC* pItem );
	void						collapse_item( SceneItemC* pItem );
	void						set_item_flags(SceneItemC* pItem, PajaTypes::int32 i32Flags );
	void						layer_list_modified();

	void						remove_cont_sample_data();

	void						layerlist_selection_changed();

protected:

	void	add_effects( Composition::LayerC* player, PajaTypes::int32 x, PajaTypes::int32& y);
	void	add_gizmos( Composition::EffectI* pFx, PajaTypes::int32 x, PajaTypes::int32& y );
	void	add_parameters( Composition::GizmoI* pGizmo, PajaTypes::int32 x, PajaTypes::int32& y );

/*	void	add_item( PajaTypes::int32 x, PajaTypes::int32 y, PajaTypes::int32 height, PajaTypes::int32 flags,
					PajaTypes::int32 layer, PajaTypes::int32 effect = -1,
					PajaTypes::int32 gizmo = -1, PajaTypes::int32 parameter = -1 );*/


	Composition::SceneC*	m_pCurrentScene;
	std::vector<SceneItemC>	m_rSceneItems;
	PajaTypes::int32		m_i32SceneListItemsHeight;
	PajaTypes::int32		m_i32ItemHeight;
	bool					m_bListUpdated;
};


#endif