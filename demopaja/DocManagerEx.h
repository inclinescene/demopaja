#if !defined(AFX_DOCMANAGEREX_H__C96A41E0_B134_47FD_B579_0CA0698D6AB9__INCLUDED_)
#define AFX_DOCMANAGEREX_H__C96A41E0_B134_47FD_B579_0CA0698D6AB9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DocManagerEx.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDocManagerEx window

class CDocManagerEx : public CDocManager
{
// Construction
public:
	CDocManagerEx();

// Attributes
public:

// Operations
public:

// Overrides
	// helper for standard commdlg dialogs
	virtual BOOL DoPromptFileName(CString& fileName, UINT nIDSTitle,
			DWORD lFlags, BOOL bOpenFileDialog, CDocTemplate* pTemplate);

// Implementation
public:
	virtual ~CDocManagerEx();

};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOCMANAGEREX_H__C96A41E0_B134_47FD_B579_0CA0698D6AB9__INCLUDED_)
