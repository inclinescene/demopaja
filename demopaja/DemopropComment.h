#if !defined(AFX_DEMOPROPCOMMENT_H__062A64E2_B9CB_4AE9_AABA_45C43EEB154C__INCLUDED_)
#define AFX_DEMOPROPCOMMENT_H__062A64E2_B9CB_4AE9_AABA_45C43EEB154C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DemopropComment.h : header file
//


#include "PrefSubDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CDemopropComment dialog

class CDemopropComment : public CPrefSubDlg
{
// Construction
public:
	CDemopropComment(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDemopropComment)
	enum { IDD = IDD_DEMOPROP_COMMENT };
	CEdit	m_rEditComment;
	CString	m_sComment;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDemopropComment)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CFont	m_rFont;

	// Generated message map functions
	//{{AFX_MSG(CDemopropComment)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMOPROPCOMMENT_H__062A64E2_B9CB_4AE9_AABA_45C43EEB154C__INCLUDED_)
