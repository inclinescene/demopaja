// FileTypeInDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "demopajadoc.h"
#include "FileTypeInDlg.h"
#include "FileListC.h"
#include "ImportableI.h"
#include <string>
#include "ClassIdC.h"
#include "TimeContextC.h"

using namespace PajaTypes;
using namespace Composition;
using namespace Import;
using namespace PluginClass;
using namespace PajaSystem;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFileTypeInDlg dialog


void
AFXAPI
CFileTypeInDlg::DDX_MyText( CDataExchange* pDX, int nIDC, float& value )
{
	TCHAR	szBuffer[32];
	TCHAR	szFormat[32];
	HWND	hWndCtrl = pDX->PrepareEditCtrl( nIDC );

	if( pDX->m_bSaveAndValidate ) {
		// to value
		szBuffer[0] = 0;
		::GetWindowText( hWndCtrl, szBuffer, 30 );
		value = _tcstod( szBuffer, NULL );
		value /= m_f32Scale;
	}
	else {
		// from value
		_sntprintf( szFormat, 31, "%%.%df", (int32)__max( -floor( log10( m_f32Inc ) ), 0 ) );
		_sntprintf( szBuffer, 31, szFormat, value * m_f32Scale );
		::SetWindowText( hWndCtrl, szBuffer );
	}
}


CFileTypeInDlg::CFileTypeInDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFileTypeInDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFileTypeInDlg)
	m_i32FileNum = -1;
	m_sInfoText = _T("");
	m_f32Speed = 0.0f;
	m_i32TimeOffset = 0;
	//}}AFX_DATA_INIT
}


void CFileTypeInDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFileTypeInDlg)
	DDX_Control(pDX, IDC_EDITTIMEOFFSET, m_rEditOffset);
	DDX_Control(pDX, IDC_EDITSPEED, m_rEditSpeed);
	DDX_Control(pDX, IDOK, m_rOk);
	DDX_Control(pDX, IDCANCEL, m_rCancel);
	DDX_Control(pDX, IDC_COMBO, m_rCombo);
	DDX_CBIndex(pDX, IDC_COMBO, m_i32FileNum);
	DDX_Text(pDX, IDC_INFO, m_sInfoText);
	DDX_Text(pDX, IDC_EDITTIMEOFFSET, m_i32TimeOffset);
	//}}AFX_DATA_MAP
	DDX_MyText(pDX, IDC_EDITSPEED, m_f32Speed);
}


BEGIN_MESSAGE_MAP(CFileTypeInDlg, CDialog)
	//{{AFX_MSG_MAP(CFileTypeInDlg)
	ON_CBN_SELCHANGE(IDC_COMBO, OnSelchangeCombo)
	ON_EN_CHANGE(IDC_EDITSPEED, OnChangeEditspeed)
	ON_EN_CHANGE(IDC_EDITTIMEOFFSET, OnChangeEdittimeoffset)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFileTypeInDlg message handlers

BOOL CFileTypeInDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	std::string	sStr;
	char		szFname[_MAX_FNAME];
	char		szExt[_MAX_EXT];
	int32		i32Idx;
	int32		i32Sel = 0;

	i32Idx = m_rCombo.AddString( "<Empty>" );
	m_rCombo.SetItemData( i32Idx, 0xffffffff );

	FileHandleC*	pParamHandle = m_pParam->get_file( m_i32Time );

	for( uint32 i = 0; i < m_pFileList->get_file_count(); i++ ) {

		FileHandleC*	pHandle = m_pFileList->get_file( i );
		ImportableI*	pImp = 0;

		if( !pHandle )
			continue;
		
		pImp = pHandle->get_importable();
		if( !pImp )
			continue;

		_splitpath( pImp->get_filename(), 0, 0, szFname, szExt );
		sStr = szFname;
		sStr += szExt;
	
		if( (m_pParam->get_class_filter() == NULL_CLASSID ||
			 m_pParam->get_class_filter() == pImp->get_class_id()) &&
			(m_pParam->get_super_class_filter() == NULL_SUPERCLASS ||
			m_pParam->get_super_class_filter() == pImp->get_super_class_id()) ) {

			i32Idx = m_rCombo.AddString( sStr.c_str() );
			m_rCombo.SetItemData( i32Idx, i );
		}

		if( pParamHandle == pHandle )
			i32Sel = i32Idx;
	}
	m_rCombo.SetCurSel( i32Sel );
	m_i32FileNum = i32Sel;

	m_rOk.SetFlat( FALSE );
	m_rCancel.SetFlat( FALSE );


	if( !m_rSpinnerSpeed.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT | SPNB_USELIMITS, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER1 ) )
		return FALSE;

	if( !m_rSpinnerOffset.Create( WS_CHILD | WS_VISIBLE | SPNB_SETBUDDYFLOAT, CRect( 0, 0, 0, 0 ), this, IDC_SPINNER2 ) )
		return FALSE;

	m_rSpinnerSpeed.SetScale( 1.0f );
	m_rSpinnerSpeed.SetBuddy( &m_rEditSpeed, SPNB_ATTACH_RIGHT );
	m_rSpinnerSpeed.SetMinMax( 0, 10000 );

	m_rSpinnerOffset.SetScale( 1.0f );
	m_rSpinnerOffset.SetBuddy( &m_rEditOffset, SPNB_ATTACH_RIGHT );

	// update dialog data
	m_f32Speed = m_pParam->get_time_scale( m_i32Time ) * 100.0f;

	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	m_f32StartLabel = m_pParam->get_start_label( m_i32Time );
	m_f32EndLabel = m_pParam->get_end_label( m_i32Time );
	m_f32RangeLabel = m_f32EndLabel - m_f32StartLabel;
	
	m_i32TimeOffset = (int32)((float32)m_pParam->get_time_offset( m_i32Time ) / (float32)m_pParam->get_duration( m_i32Time ) * m_f32RangeLabel);

	m_f32Inc = 1.0f;
	m_f32Scale = 1.0f;

	UpdateData( FALSE );

	return TRUE;
}

void CFileTypeInDlg::OnSelchangeCombo() 
{
	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	UpdateData( TRUE );
	int32	i32Num = m_rCombo.GetItemData( m_i32FileNum );
	FileHandleC*	pHandle = 0;
	if( i32Num != CB_ERR )
		pHandle = m_pFileList->get_file( i32Num );

	pDoc->BeginDeferHandleParamNotify();

	pDoc->HandleParamNotify( m_pParam->set_file( m_i32Time, pHandle ) );
	// if the set file causes recursion, set the file to <empty>
	if( pDoc->CheckFileRecursion() )
		pDoc->HandleParamNotify( m_pParam->set_file( m_i32Time, 0 ) );

	pDoc->EndDeferHandleParamNotify();

	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}

void CFileTypeInDlg::OnChangeEditspeed() 
{
	UpdateData( TRUE );

	// clamp speed
	if( m_f32Speed < 0 )
		m_f32Speed = 0;
	if( m_f32Speed > 10000 )
		m_f32Speed = 10000;

	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	pDoc->HandleParamNotify( m_pParam->set_time_scale( m_i32Time, m_f32Speed / 100.0f ) );

	// recalc time offset too
	int32	i32TimeOffset = (int32)((float32)m_i32TimeOffset / m_f32RangeLabel * (float32)m_pParam->get_duration( m_i32Time ));
	pDoc->HandleParamNotify( m_pParam->set_time_offset( m_i32Time, i32TimeOffset ) );
	
	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}

void CFileTypeInDlg::OnChangeEdittimeoffset() 
{
	UpdateData( TRUE );

	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );
	int32	i32TimeOffset = (int32)((float32)m_i32TimeOffset / m_f32RangeLabel * (float32)m_pParam->get_duration( m_i32Time ));
	pDoc->HandleParamNotify( m_pParam->set_time_offset( m_i32Time, i32TimeOffset ) );

	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}
