// TransformDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "TransformDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTransformDlg dialog


CTransformDlg::CTransformDlg( CWnd* pParent /*=NULL*/ ) :
	CDialog(CTransformDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CTransformDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CTransformDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CTransformDlg)
	DDX_Control(pDX, IDC_TAB, m_rTab);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CTransformDlg, CDialog)
	//{{AFX_MSG_MAP(CTransformDlg)
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB, OnSelchangeTab)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTransformDlg message handlers

void CTransformDlg::OnSelchangeTab(NMHDR* pNMHDR, LRESULT* pResult) 
{
	int	iCurTab = m_rTab.GetCurSel();

	if( iCurTab == 1 ) {
		m_pMoveDlg->ShowWindow( SW_HIDE );
		m_pRotateDlg->ShowWindow( SW_SHOW );
		m_pScaleDlg->ShowWindow( SW_HIDE );
		m_pRotateDlg->SetFocus();
	}
	else if( iCurTab == 2 ) {
		m_pMoveDlg->ShowWindow( SW_HIDE );
		m_pRotateDlg->ShowWindow( SW_HIDE );
		m_pScaleDlg->ShowWindow( SW_SHOW );
		m_pScaleDlg->SetFocus();
	}
	else {
		m_pMoveDlg->ShowWindow( SW_SHOW );
		m_pRotateDlg->ShowWindow( SW_HIDE );
		m_pScaleDlg->ShowWindow( SW_HIDE );
		m_pMoveDlg->SetFocus();
	}

	*pResult = 0;
}

BOOL CTransformDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if( !m_rImageList.Create( IDB_TRANSFORM, 12, 3, RGB( 255, 0, 255 ) ) )
		return FALSE;

	m_pMoveDlg = new CMoveDlg;
	m_pRotateDlg = new CRotateDlg;
	m_pScaleDlg = new CScaleDlg;

	if( !m_pMoveDlg || !m_pMoveDlg->Create( this ) )
		return FALSE;
	if( !m_pRotateDlg || !m_pRotateDlg->Create( this ) )
		return FALSE;
	if( !m_pScaleDlg || !m_pScaleDlg->Create( this ) )
		return FALSE;

	CRect	rTabRect;
	m_rTab.GetWindowRect( rTabRect );
	ScreenToClient( &rTabRect );

	CRect	rRect;
	CPoint	rOffset( rTabRect.left, rTabRect.bottom + 2 );

	m_pMoveDlg->GetWindowRect( &rRect );
	ScreenToClient( &rRect );
	rRect.OffsetRect( rOffset );
	m_pMoveDlg->MoveWindow( &rRect );

	m_pRotateDlg->GetWindowRect( &rRect );
	ScreenToClient( &rRect );
	rRect.OffsetRect( rOffset );
	m_pRotateDlg->MoveWindow( &rRect );

	m_pScaleDlg->GetWindowRect( &rRect );
	ScreenToClient( &rRect );
	rRect.OffsetRect( rOffset );
	m_pScaleDlg->MoveWindow( &rRect );

	m_pMoveDlg->ShowWindow( SW_SHOW );

	m_rTab.SetImageList( &m_rImageList );

	m_rTab.InsertItem( 0, "Move", 2 );
	m_rTab.InsertItem( 1, "Rotate", 0 );
	m_rTab.InsertItem( 2, "Scale", 1 );
	
	return TRUE;
}

BOOL CTransformDlg::Create( CWnd* pParentWnd ) 
{
	return CDialog::Create(IDD, pParentWnd);
}

void CTransformDlg::OnDestroy() 
{
	CDialog::OnDestroy();
	
	m_pMoveDlg->DestroyWindow();
	m_pScaleDlg->DestroyWindow();
	m_pRotateDlg->DestroyWindow();

	delete m_pMoveDlg;	
	delete m_pScaleDlg;
	delete m_pRotateDlg;

	m_pRotateDlg = 0;
	m_pScaleDlg = 0;
	m_pMoveDlg = 0;
}
