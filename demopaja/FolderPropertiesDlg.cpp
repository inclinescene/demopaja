// FolderPropertiesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "FolderPropertiesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFolderPropertiesDlg dialog


CFolderPropertiesDlg::CFolderPropertiesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CFolderPropertiesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CFolderPropertiesDlg)
	m_sName = _T("");
	m_iColor = -1;
	//}}AFX_DATA_INIT
}


void CFolderPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CFolderPropertiesDlg)
	DDX_Control(pDX, IDC_COMBOCOLOR, m_rComboColor);
	DDX_Text(pDX, IDC_EDITNAME, m_sName);
	DDX_CBIndex(pDX, IDC_COMBOCOLOR, m_iColor);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CFolderPropertiesDlg, CDialog)
	//{{AFX_MSG_MAP(CFolderPropertiesDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFolderPropertiesDlg message handlers


static
COLORREF
Mix( COLORREF rCol1, COLORREF rCol2, int iAlpha )
{
	int	iR, iG, iB;

	iR = ((GetRValue( rCol1 ) * iAlpha) + (GetRValue( rCol2) * (255 - iAlpha))) / 255;
	iG = ((GetGValue( rCol1 ) * iAlpha) + (GetGValue( rCol2) * (255 - iAlpha))) / 255;
	iB = ((GetBValue( rCol1 ) * iAlpha) + (GetBValue( rCol2) * (255 - iAlpha))) / 255;

	return	RGB( iR, iG, iB );
}

BOOL CFolderPropertiesDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	
	COLORREF	rFolderColors[8] = {
		RGB( 255, 255, 255 ),
		RGB( 255, 0,   0 ),
		RGB( 255, 255, 0 ),
		RGB( 0,   255, 0 ),
		RGB( 0,   255, 255 ),
		RGB( 0,   0,   255 ),
		RGB( 255, 0,   255 ),
		RGB( 0,   0,   0 ),
	};
	COLORREF	rBaseGrey = ::GetSysColor( COLOR_BTNFACE );

	char	szNames[8][32] = {
		"White",
		"Red",
		"Yellow",
		"Green",
		"Cyan",
		"Blue",
		"Magenta",
		"Black",
	};
	
	for( int i = 0; i < 8; i++ )
		m_rComboColor.AddColor( szNames[i], Mix( rFolderColors[i], rBaseGrey, 64 ) );

	m_rComboColor.SetCurSel( m_iColor );

	return TRUE;
}
