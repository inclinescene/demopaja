#if !defined(AFX_PARAMETERBAR_H__B6772C20_0879_478C_B60C_CDA2F903B463__INCLUDED_)
#define AFX_PARAMETERBAR_H__B6772C20_0879_478C_B60C_CDA2F903B463__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ParameterBar.h : header file
//

#include "afxole.h"
#include "SizeCBar.h"
#include "EffectI.h"
#include "RollupCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CParameterBar window

#ifndef baseCMyBar
#define baseCMyBar CSizingControlBarG
#endif

class CParameterBar : public baseCMyBar
{
// Construction
public:
	CParameterBar();
	virtual ~CParameterBar();

	virtual DROPEFFECT	OnDragOver( COleDataObject* pDataObject, DWORD dwKeyState, CPoint point );
	virtual BOOL		OnDrop( COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point );

	void	UpdateFromEffect( Composition::EffectI* pEffect, bool bChangeUI = false );

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CParameterBar)
	//}}AFX_VIRTUAL


	// Generated message map functions
protected:
	//{{AFX_MSG(CParameterBar)
	afx_msg void OnPaint();
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

	CRollupCtrl				m_wndRollup;
	CFont					m_rBoldFont;
	CFont					m_rNormalFont;

	Composition::EffectI*	m_pEffect;
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PARAMETERBAR_H__B6772C20_0879_478C_B60C_CDA2F903B463__INCLUDED_)
