#if !defined(AFX_RECURSIONERRORDLG_H__5B5E1D8B_C975_4CD5_9316_E782279F95C0__INCLUDED_)
#define AFX_RECURSIONERRORDLG_H__5B5E1D8B_C975_4CD5_9316_E782279F95C0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// RecursionErrorDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CRecursionErrorDlg dialog


#include <vector>


enum RecursionErrorFlagE {
	RECERR_SCENE,
	RECERR_EFFECT,
	RECERR_PARAM,
	RECERR_FILEHANDLE,
	RECERR_RENDER_TO_TEXTURE,
	RECERR_ERROR,
};

class CRecursionErrorDlg : public CDialog
{
// Construction
public:
	CRecursionErrorDlg(CWnd* pParent = NULL);   // standard constructor

	void	AddErrorItem( const char* szName, int iFlag );

// Dialog Data
	//{{AFX_DATA(CRecursionErrorDlg)
	enum { IDD = IDD_RECURSIONERROR_DLG };
	CTreeCtrl	m_rStackTree;
	CString	m_sWarningText;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CRecursionErrorDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CImageList			m_rImageList;


	struct RecErrorItemS {
		CString	sName;
		int		iFlags;
	};

	std::vector<RecErrorItemS>	m_rItems;



	// Generated message map functions
	//{{AFX_MSG(CRecursionErrorDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RECURSIONERRORDLG_H__5B5E1D8B_C975_4CD5_9316_E782279F95C0__INCLUDED_)
