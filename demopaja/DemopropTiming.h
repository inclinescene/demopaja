#if !defined(AFX_DEMOPROPTIMING_H__6E2CB71D_642E_4725_ABD8_16578B1570AB__INCLUDED_)
#define AFX_DEMOPROPTIMING_H__6E2CB71D_642E_4725_ABD8_16578B1570AB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DemopropTiming.h : header file
//


#include "PrefSubDlg.h"

/////////////////////////////////////////////////////////////////////////////
// CDemopropTiming dialog

class CDemopropTiming : public CPrefSubDlg
{
// Construction
public:
	CDemopropTiming(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDemopropTiming)
	enum { IDD = IDD_DEMOPROP_TIMING };
	CStatic	m_rStaticFPS;
	int		m_i32BeatSize;
	int		m_i32MeasureSize;
	CString	m_sDuration;
	int		m_i32BPM;
	int		m_i32Accuracy;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDemopropTiming)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	void	UpdateFPS();

	// Generated message map functions
	//{{AFX_MSG(CDemopropTiming)
	afx_msg void OnChangeEditBpm();
	afx_msg void OnSelchangeBeatSize();
	afx_msg void OnSelchangeComboAccuracy();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMOPROPTIMING_H__6E2CB71D_642E_4725_ABD8_16578B1570AB__INCLUDED_)
