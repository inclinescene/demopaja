//-------------------------------------------------------------------------
//
// File:		ColorCommonDialogC.h
// Desc:		Color common dialog.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://moppi.inside.org/demopaja/
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_COLORCOMMONDIALOGC_H__
#define __DEMOPAJA_COLORCOMMONDIALOGC_H__

#include <windows.h>
#include "PajaTypes.h"
#include "ColorC.h"
#include "ClassIdC.h"
#include "DataBlockI.h"
#include "CommonDialogI.h"
#include <string>

namespace PajaSystem {

	
	const PluginClass::ClassIdC		CLASS_COLORCOMMONDIALOG( 0, 1000 );


	class ColorCommonDialogC : public CommonDialogI
	{
	public:

		static ColorCommonDialogC*			create_new();

		virtual Edit::DataBlockI*			create();

		virtual PluginClass::ClassIdC		get_class_id();
		virtual bool						do_modal();

		virtual void						set_parent_wnd( HWND hParent );
		virtual void						set_caption( const char* szName );

		virtual void						set_color( const PajaTypes::ColorC& rCol );
		virtual const PajaTypes::ColorC&	get_color() const;

	protected:
		ColorCommonDialogC();
		virtual ~ColorCommonDialogC();

		PajaTypes::ColorC	m_rColor;
		std::string			m_sCaption;
		HWND				m_hWndParent;
	};

};

#endif // __DEMOPAJA_COLORCOMMONDIALOGC_H__