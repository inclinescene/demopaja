#if !defined(AFX_DEMOPROPDEVICES_H__505A340F_419B_4553_9B29_DA06758B4C30__INCLUDED_)
#define AFX_DEMOPROPDEVICES_H__505A340F_419B_4553_9B29_DA06758B4C30__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DemopropDevices.h : header file
//


#include "PrefSubDlg.h"
#include "ClassIdC.h"
#include <vector>

/////////////////////////////////////////////////////////////////////////////
// CDemopropDevices dialog

class CDemopropDevices : public CPrefSubDlg
{
// Construction
public:
	CDemopropDevices(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDemopropDevices)
	enum { IDD = IDD_DEMOPROP_DEVICES };
	CComboBox	m_rComboGraphDevice;
	CListCtrl	m_rListDeviceList;
	//}}AFX_DATA

	PluginClass::ClassIdC				m_rGraphicsDeviceClassId;
	std::vector<PluginClass::ClassIdC>	m_rDynamicDevices;
	bool								m_bCanChangeExclusive;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDemopropDevices)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDemopropDevices)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeComboGraphdevice();
	afx_msg void OnButtonConfiggraph();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DEMOPROPDEVICES_H__505A340F_419B_4553_9B29_DA06758B4C30__INCLUDED_)
