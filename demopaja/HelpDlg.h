//{{AFX_INCLUDES()
#include "webbrowser2.h"
//}}AFX_INCLUDES
#if !defined(AFX_HELPDLG_H__FBBCFCAD_13A9_4C4F_A6DA_0DC08D09C804__INCLUDED_)
#define AFX_HELPDLG_H__FBBCFCAD_13A9_4C4F_A6DA_0DC08D09C804__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// HelpDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CHelpDlg dialog

#include "BtnST.h"

class CHelpDlg : public CDialog
{
// Construction
public:
	CHelpDlg(CWnd* pParent = NULL);   // standard constructor

	void	Browse( const char* szFile );

// Dialog Data
	//{{AFX_DATA(CHelpDlg)
	enum { IDD = IDD_HELPDIALOG };
	CButtonST	m_rButtonStop;
	CButtonST	m_rButtonReload;
	CButtonST	m_rButtonPrev;
	CButtonST	m_rButtonNext;
	CButtonST	m_rButtonOpen;
	CWebBrowser2	m_rWebBrowser;
//	CImageList		m_rImagelist;
	CString			m_sURL;
	//}}AFX_DATA

	CBitmap			m_rBitmapOpen;
	CBitmap			m_rBitmapPrev;
	CBitmap			m_rBitmapNext;
	CBitmap			m_rBitmapRefresh;
	CBitmap			m_rBitmapStop;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CHelpDlg)
	public:
	virtual BOOL Create( CWnd* pParentWnd = NULL );
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CHelpDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnNext();
	afx_msg void OnPrev();
	afx_msg void OnReload();
	afx_msg void OnStop();
	afx_msg void OnOpen();
	afx_msg void OnCommandStateChangeExplorer(long Command, BOOL Enable);
	DECLARE_EVENTSINK_MAP()
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_HELPDLG_H__FBBCFCAD_13A9_4C4F_A6DA_0DC08D09C804__INCLUDED_)
