#if !defined(AFX_GRIDSETTINGSDLG_H__E3F6E255_6BB4_4930_9F8C_854C08F49C5B__INCLUDED_)
#define AFX_GRIDSETTINGSDLG_H__E3F6E255_6BB4_4930_9F8C_854C08F49C5B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GridSettingsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGridSettingsDlg dialog

class CGridSettingsDlg : public CDialog
{
// Construction
public:
	CGridSettingsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGridSettingsDlg)
	enum { IDD = IDD_GRID_SETTINGS };
	int		m_i32GridSize;
	BOOL	m_bGridEnabled;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGridSettingsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGridSettingsDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRIDSETTINGSDLG_H__E3F6E255_6BB4_4930_9F8C_854C08F49C5B__INCLUDED_)
