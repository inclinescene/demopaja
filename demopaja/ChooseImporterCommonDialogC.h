//-------------------------------------------------------------------------
//
// File:		ColorCommonDialogC.h
// Desc:		Color common dialog.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2003 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://moppi.inside.org/demopaja/
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_CHOOSEIMPORTERCOMMONDIALOGC_H__
#define __DEMOPAJA_CHOOSEIMPORTERCOMMONDIALOGC_H__

#include <windows.h>
#include "PajaTypes.h"
#include "ColorC.h"
#include "ClassIdC.h"
#include "DataBlockI.h"
#include "CommonDialogI.h"
#include <string>

namespace PajaSystem {

	//! Choose importer common dialog class ID.	
	const PluginClass::ClassIdC		CLASS_CHOOSEIMPORTERCOMMONDIALOG( 0, 1001 );

	//! Choose Importer Dialog Class

	class ChooseImporterCommonDialogC : public CommonDialogI
	{
	public:

		//! Creates new dialog.
		static ChooseImporterCommonDialogC*	create_new();

		//! Creates new dialog.
		virtual Edit::DataBlockI*			create();

		//! Returns the class ID if the dialog.
		virtual PluginClass::ClassIdC		get_class_id();
		//! Prompts the dialog.
		virtual bool						do_modal();

		virtual void						set_file_name( const char* szFilename );
		virtual void						add_importer( const char* szDesc, PajaTypes::uint32 ui32ID );
		virtual PajaTypes::int32			get_selected_importer() const;
		virtual bool						get_use_extension_to_all() const;

	protected:
		ChooseImporterCommonDialogC();
		virtual ~ChooseImporterCommonDialogC();

		struct ImporterS {
			std::string			m_sDesc;
			PajaTypes::uint32	m_ui32ID;
		};
		std::vector<ImporterS>	m_rImporters;
		std::string				m_sFileName;
		PajaTypes::int32		m_i32SelID;
		bool					m_bUseToAll;
	};

};

#endif // __DEMOPAJA_COLORCOMMONDIALOGC_H__