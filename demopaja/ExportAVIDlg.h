#if !defined(AFX_EXPORTAVIDLG_H__B9DDB1D0_717E_46F7_B770_3B57BB7A34B3__INCLUDED_)
#define AFX_EXPORTAVIDLG_H__B9DDB1D0_717E_46F7_B770_3B57BB7A34B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ExportAVIDlg.h : header file
//

#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "afxwin.h"
#include <mmsystem.h>
#include <vfw.h>

/////////////////////////////////////////////////////////////////////////////
// CExportAVIDlg dialog

class CExportAVIDlg : public CDialog
{
// Construction
public:
	CExportAVIDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CExportAVIDlg)
	enum { IDD = IDD_EXPORTAVI_DLG };
	CStatic	m_rStaticTimeStart;
	CStatic	m_rStaticTimeEnd;
	CEdit	m_rEditTimeStart;
	CEdit	m_rEditTimeEnd;
	int		m_iWidth;
	int		m_iHeight;
	int		m_iFPS;
	//}}AFX_DATA

	PajaTypes::int32			m_i32StartTime;
	PajaTypes::int32			m_i32EndTime;

	PajaTypes::int32			m_i32BeatsPerMin;
	PajaTypes::int32			m_i32QNotesPerBeat;
	PajaTypes::int32			m_i32EditAccuracy;

	WAVEFORMATEX*			m_pWaveFormat;
	WAVEFORMATEX*			m_pWaveFormatOut;
	HACMDRIVERID			m_hAudioDriverId;

	COMPVARS*					m_pCompVars;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CExportAVIDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	ICINFO*						m_pCompInfo;
	int								m_nComp;
	COMPVARS*					m_pCV;
	char							m_tbuf[128];

	HIC								m_hic;
	FOURCC						m_fccSelect;
	ICINFO*						m_piiCurrent;

	void*							m_pState;
	int								m_cbState;
	char							m_szCurrentCompression[256];



	void			ReenableOptions( HIC hic, ICINFO *pii );
	void			SelectCompressor( PajaTypes::int32 i32Comp );
	void			UpdateLabels();
	CString			FormatTime( PajaTypes::int32 i32Time );

	bool							m_bUpdating;
	CSpinnerButton		m_rSpinnerTimeStart;
	CSpinnerButton		m_rSpinnerTimeEnd;
	CSpinnerButton		m_rSpinnerQuality;

	// Generated message map functions
	//{{AFX_MSG(CExportAVIDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEditTimeend();
	afx_msg void OnChangeEditTimestart();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_ComboAudioCompressor;
	afx_msg void OnDestroy();
	afx_msg void OnCbnSelchangeComboAudioCompressor();
	CComboBox m_ComboAudioFormat;
protected:
	virtual void OnOK();
public:
	afx_msg void OnBnClickedButtonConfigure();
	afx_msg void OnCbnSelchangeComboVideoCompressor();
	CComboBox m_ComboVideoCompressor;
	CStatic m_StaticReport;
	CEdit m_EditQuality;
	CEdit m_EditRate;
	CEdit m_EditKeyFrame;
	CButton m_CheckRate;
	CButton m_CheckKeyframe;
	CButton m_ButtonCofigure;
	CEdit m_EditWidth;
	CEdit m_EditHeight;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EXPORTAVIDLG_H__B9DDB1D0_717E_46F7_B770_3B57BB7A34B3__INCLUDED_)
