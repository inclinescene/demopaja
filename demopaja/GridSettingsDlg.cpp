// GridSettingsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "GridSettingsDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGridSettingsDlg dialog


CGridSettingsDlg::CGridSettingsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGridSettingsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGridSettingsDlg)
	m_i32GridSize = 0;
	m_bGridEnabled = FALSE;
	//}}AFX_DATA_INIT
}


void CGridSettingsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGridSettingsDlg)
	DDX_Text(pDX, IDC_EDIT_GRIDSIZE, m_i32GridSize);
	DDX_Check(pDX, IDC_GRID_ENABLED, m_bGridEnabled);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGridSettingsDlg, CDialog)
	//{{AFX_MSG_MAP(CGridSettingsDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGridSettingsDlg message handlers
