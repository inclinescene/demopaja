#if !defined(AFX_COLORBUTTON_H__AB482EBC_B438_4D89_AD28_16D00F4FFF65__INCLUDED_)
#define AFX_COLORBUTTON_H__AB482EBC_B438_4D89_AD28_16D00F4FFF65__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ColorButton.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CColorButton window

class CColorButton : public CButton
{
// Construction
public:
	CColorButton();

	void	SetColor( int iR, int iG, int iB, int iA );

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CColorButton)
	public:
	virtual void DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct);
	protected:
	virtual void PreSubclassWindow();
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CColorButton();

	// Generated message map functions
protected:
	//{{AFX_MSG(CColorButton)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG


	int			m_iR, m_iG, m_iB, m_iA;
	COLORREF	m_rColor, m_rColorAlpha, m_rColorLight, m_rColorDark;

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COLORBUTTON_H__AB482EBC_B438_4D89_AD28_16D00F4FFF65__INCLUDED_)
