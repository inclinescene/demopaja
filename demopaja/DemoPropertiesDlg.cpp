// DemoProperties.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "DemoPropertiesDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDemoPropertiesDlg dialog


CDemoPropertiesDlg::CDemoPropertiesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CDemoPropertiesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDemoPropertiesDlg)
	m_sComment = _T("");
	m_sDuration = _T("");
	m_i32FPS = 0;
	m_i32Height = 0;
	m_rMusicFile = _T("");
	m_i32Width = 0;
	//}}AFX_DATA_INIT
}


void CDemoPropertiesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDemoPropertiesDlg)
	DDX_Control(pDX, IDC_EDIT_COMMENT, m_rEditComment);
	DDX_Text(pDX, IDC_EDIT_COMMENT, m_sComment);
	DDX_Text(pDX, IDC_EDIT_DURATION, m_sDuration);
	DDX_Text(pDX, IDC_EDIT_FPS, m_i32FPS);
	DDV_MinMaxInt(pDX, m_i32FPS, 0, 200);
	DDX_Text(pDX, IDC_EDIT_HEIGHT, m_i32Height);
	DDV_MinMaxInt(pDX, m_i32Height, 0, 10000);
	DDX_Text(pDX, IDC_EDIT_MUSICFILE, m_rMusicFile);
	DDX_Text(pDX, IDC_EDIT_WIDTH, m_i32Width);
	DDV_MinMaxInt(pDX, m_i32Width, 0, 10000);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDemoPropertiesDlg, CDialog)
	//{{AFX_MSG_MAP(CDemoPropertiesDlg)
	ON_BN_CLICKED(IDC_PICK_MUSIC, OnPickMusic)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDemoPropertiesDlg message handlers

BOOL CDemoPropertiesDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	if( !m_rFont.CreateFont( 14, 0, 0, 0, FW_NORMAL, 0, 0, 0,
		ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"courier new" ) ) {
		return FALSE;
	}

	m_rEditComment.SetFont( &m_rFont );
	
	return TRUE;
}

void CDemoPropertiesDlg::OnPickMusic() 
{
	char	szFileTypes[] = "MP3 Music (*.mp3)|*.mp3|All Files (*.*)|*.*||";
	CFileDialog	rDlg( TRUE, NULL, NULL, OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_EXPLORER, szFileTypes );
	rDlg.m_ofn.lpstrTitle = "Import";

	if( rDlg.DoModal() == IDOK ) {
		m_rMusicFile = rDlg.GetPathName();
		UpdateData( FALSE );	// modify text
	}
}
