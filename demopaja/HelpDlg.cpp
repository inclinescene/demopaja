// HelpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "afxdtctl.h"
#include "demopaja.h"
#include "HelpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CHelpDlg dialog


CHelpDlg::CHelpDlg(CWnd* pParent /*=NULL*/) :
	CDialog(CHelpDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CHelpDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CHelpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CHelpDlg)
	DDX_Control(pDX, IDC_STOP, m_rButtonStop);
	DDX_Control(pDX, IDC_RELOAD, m_rButtonReload);
	DDX_Control(pDX, IDC_PREV, m_rButtonPrev);
	DDX_Control(pDX, IDC_NEXT, m_rButtonNext);
	DDX_Control(pDX, IDC_OPEN, m_rButtonOpen);
	DDX_Control(pDX, IDC_EXPLORER, m_rWebBrowser);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CHelpDlg, CDialog)
	//{{AFX_MSG_MAP(CHelpDlg)
	ON_WM_SIZE()
	ON_BN_CLICKED(IDC_NEXT, OnNext)
	ON_BN_CLICKED(IDC_PREV, OnPrev)
	ON_BN_CLICKED(IDC_RELOAD, OnReload)
	ON_BN_CLICKED(IDC_STOP, OnStop)
	ON_BN_CLICKED(IDC_OPEN, OnOpen)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CHelpDlg message handlers

BOOL CHelpDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	// Load bitmaps to buttons
//	m_rImagelist.Create( IDB_HELP, 12, 6, RGB( 192, 192, 192 ) );

	// map colors
	COLORMAP	rColors[3];
	// magenta BG
	rColors[0].from = RGB( 255, 0, 255 );
	rColors[0].to = ::GetSysColor( COLOR_BTNFACE );
	// light grey
	rColors[1].from = RGB( 192, 192, 192 );
	rColors[1].to = ::GetSysColor( COLOR_BTNFACE );
	// dark grey
	rColors[2].from = RGB( 128, 128, 128 );
	rColors[2].to = ::GetSysColor( COLOR_BTNSHADOW );

	if( !m_rBitmapOpen.LoadMappedBitmap( IDB_HELP_OPEN, 0, rColors, 3 ) )
		return FALSE;
	if( !m_rBitmapPrev.LoadMappedBitmap( IDB_HELP_PREV, 0, rColors, 3 ) )
		return FALSE;
	if( !m_rBitmapNext.LoadMappedBitmap( IDB_HELP_NEXT, 0, rColors, 3 ) )
		return FALSE;
	if( !m_rBitmapRefresh.LoadMappedBitmap( IDB_HELP_REFRESH, 0, rColors, 3 ) )
		return FALSE;
	if( !m_rBitmapStop.LoadMappedBitmap( IDB_HELP_STOP, 0, rColors, 3 ) )
		return FALSE;

	m_rButtonOpen.SetBitmaps( m_rBitmapOpen ); //SetIcon( m_rImagelist.ExtractIcon( 0 ) );
	m_rButtonPrev.SetBitmaps( m_rBitmapPrev ); //SetIcon( m_rImagelist.ExtractIcon( 2 ) );
	m_rButtonNext.SetBitmaps( m_rBitmapNext ); //SetIcon( m_rImagelist.ExtractIcon( 3 ) );
	m_rButtonReload.SetBitmaps( m_rBitmapRefresh ); //SetIcon( m_rImagelist.ExtractIcon( 4 ) );
	m_rButtonStop.SetBitmaps( m_rBitmapStop ); //SetIcon( m_rImagelist.ExtractIcon( 5 ) );

	if( m_sURL.IsEmpty() ) {
		// Goto to the defautl page
		TCHAR	sModuleName[_MAX_PATH];
		::GetModuleFileName( AfxGetInstanceHandle(), sModuleName, _MAX_PATH );

		CString	sPage;
		sPage.Format( "/%d", IDR_HELPHOME );
		m_sURL = "res://";
		m_sURL += sModuleName;
		m_sURL += sPage;
	}

	Browse( m_sURL );

	return TRUE;
}

BOOL CHelpDlg::Create( CWnd* pParentWnd ) 
{
	return CDialog::Create(IDD, pParentWnd);
}

void CHelpDlg::Browse( const char* szFile )
{
	COleSafeArray	rPostArray;

	m_rWebBrowser.Navigate2( COleVariant( szFile, VT_BSTR ),
							 COleVariant( (long)0, VT_I4 ),
							 COleVariant( (LPCTSTR)NULL, VT_BSTR ),
							 COleSafeArray(),
							 COleVariant( (LPCTSTR)NULL, VT_BSTR ) );
}

void CHelpDlg::OnSize(UINT nType, int cx, int cy) 
{
	CDialog::OnSize(nType, cx, cy);

	if( IsWindowVisible() ) {

		CRect	rClientRect;
		GetClientRect( rClientRect );

		m_rWebBrowser.SetWidth( rClientRect.Width() );
		m_rWebBrowser.SetHeight( rClientRect.Height() - m_rWebBrowser.GetTop() );
	}
}

void CHelpDlg::OnNext() 
{
	m_rWebBrowser.GoForward();
}

void CHelpDlg::OnPrev() 
{
	m_rWebBrowser.GoBack();
}

void CHelpDlg::OnReload() 
{
	m_rWebBrowser.Refresh();
}

void CHelpDlg::OnStop() 
{
	m_rWebBrowser.Stop();
}

void CHelpDlg::OnOpen() 
{
	char	szFileType[] = "HTML Files (*.html)|*.htm;*.html|All Files (*.*)|*.*||";
	CFileDialog	rDlg( TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_EXPLORER, szFileType );

	if( rDlg.DoModal() == IDOK )
		Browse( rDlg.GetPathName() );
}

BEGIN_EVENTSINK_MAP(CHelpDlg, CDialog)
    //{{AFX_EVENTSINK_MAP(CHelpDlg)
	ON_EVENT(CHelpDlg, IDC_EXPLORER, 105 /* CommandStateChange */, OnCommandStateChangeExplorer, VTS_I4 VTS_BOOL)
	//}}AFX_EVENTSINK_MAP
END_EVENTSINK_MAP()

void CHelpDlg::OnCommandStateChangeExplorer(long iCommand, BOOL bEnable) 
{
	switch( iCommand ) {
	case CSC_NAVIGATEFORWARD:
		m_rButtonNext.EnableWindow( bEnable );
		break;

	case CSC_NAVIGATEBACK:
		m_rButtonPrev.EnableWindow( bEnable );
		break;
	}
}
