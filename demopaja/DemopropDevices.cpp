// DemopropGraphics.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "demopajaDoc.h"
#include "demopajaview.h"
#include "DemopropDevices.h"
#include "DeviceContextC.h"
#include "FactoryC.h"
#include "ClassDescC.h"
#include "GraphicsDeviceI.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


using namespace PajaTypes;
using namespace PajaSystem;
using namespace PluginClass;


/////////////////////////////////////////////////////////////////////////////
// CDemopropDevices dialog


CDemopropDevices::CDemopropDevices(CWnd* pParent /*=NULL*/) :
	CPrefSubDlg(CDemopropDevices::IDD, pParent),
	m_bCanChangeExclusive( true )
{
	//{{AFX_DATA_INIT(CDemopropDevices)
	//}}AFX_DATA_INIT
}


void CDemopropDevices::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDemopropDevices)
	DDX_Control(pDX, IDC_COMBO_GRAPHDEVICE, m_rComboGraphDevice);
	DDX_Control(pDX, IDC_DEVICE_LIST, m_rListDeviceList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDemopropDevices, CPrefSubDlg)
	//{{AFX_MSG_MAP(CDemopropDevices)
	ON_CBN_SELCHANGE(IDC_COMBO_GRAPHDEVICE, OnSelchangeComboGraphdevice)
	ON_BN_CLICKED(IDC_BUTTON_CONFIGGRAPH, OnButtonConfiggraph)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDemopropDevices message handlers

BOOL CDemopropDevices::OnInitDialog() 
{
	CPrefSubDlg::OnInitDialog();
	
	CDemopajaApp*	pApp = (CDemopajaApp*)AfxGetApp();
	CDemopajaDoc*	pDoc = GetDoc();
	DeviceContextC*	pContext = pDoc->GetDeviceContext();
	FactoryC*		pFactory = pApp->GetFactory();

	uint32	i;

	//
	// Init graphics device
	//

	m_rComboGraphDevice.ResetContent();

	int32	i32CurSel = -1;

	for( i = 0; i < pFactory->get_classdesc_count(); i++ ) {
		ClassDescC*	pDesc = pFactory->get_classdesc( i );
		if( pDesc->get_classtype() == CLASS_TYPE_DEVICEDRIVER &&
			pDesc->get_super_class_id() == SUPERCLASS_GRAPHICSDEVICE ) {

			int32	i32Idx = m_rComboGraphDevice.AddString( pDesc->get_name() );
			m_rComboGraphDevice.SetItemData( i32Idx, i );

			if( pDesc->get_class_id() == m_rGraphicsDeviceClassId )
				i32CurSel = i32Idx;
		}
	}
	m_rComboGraphDevice.SetCurSel( i32CurSel );

	if( !m_bCanChangeExclusive )
		m_rComboGraphDevice.EnableWindow( FALSE );

	//
	// Init dynamic devices
	//

	// add headers
	m_rListDeviceList.InsertColumn( 0, "Device", LVCFMT_LEFT, 135 );
	m_rListDeviceList.InsertColumn( 1, "Type", LVCFMT_LEFT, 70 );
	

	for( i = 0; i < m_rDynamicDevices.size(); i++ ) {

		ClassDescC*	pDesc = pFactory->get_classdesc( m_rDynamicDevices[i] );

		if( pDesc && pDesc->get_classtype() == CLASS_TYPE_DEVICEDRIVER ) {

			// Skip graphics drivers
			if( pDesc->get_super_class_id() == SUPERCLASS_GRAPHICSDEVICE )
				continue;

			int32	i32Idx = m_rListDeviceList.InsertItem( 0, pDesc->get_name() );
			m_rListDeviceList.SetItemText( i32Idx, 1, "Generic" );
		}
	}


	return TRUE;
}

void CDemopropDevices::OnSelchangeComboGraphdevice() 
{
	CDemopajaApp*	pApp = (CDemopajaApp*)AfxGetApp();
	FactoryC*		pFactory = pApp->GetFactory();

	int32	i32CurSel = m_rComboGraphDevice.GetCurSel();
	if( i32CurSel != CB_ERR ) {
		ClassDescC*	pDesc = pFactory->get_classdesc( m_rComboGraphDevice.GetItemData( i32CurSel ) );
		if( pDesc )
			m_rGraphicsDeviceClassId = pDesc->get_class_id();
	}
}

void CDemopropDevices::OnButtonConfiggraph() 
{
	CDemopajaDoc*	pDoc = GetDoc();
	DeviceContextC*	pContext = pDoc->GetDeviceContext();
	CDemopajaView*	pView = 0;
	// get the view
	POSITION rPos = pDoc->GetFirstViewPosition();
	if( rPos ) {
		pView = (CDemopajaView*)pDoc->GetNextView( rPos );
		if( pView ) {
			GraphicsDeviceI*	pDevice = (GraphicsDeviceI*)pContext->query_interface( SUPERCLASS_GRAPHICSDEVICE );
			if( pDevice && pDevice->get_class_id() == m_rGraphicsDeviceClassId )
				pDevice->configure();
			else {
				::MessageBox( NULL, "Selected device is not created yet.\nYou must first commit your current changes.", "Error", MB_OK | MB_ICONERROR );
			}
		}	
	}
}
