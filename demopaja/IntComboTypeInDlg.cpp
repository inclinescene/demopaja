// IntComboTypeInDlg.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "demopajadoc.h"
#include "IntComboTypeInDlg.h"
#include "PajaTypes.h"
#include "ParamI.h"

using namespace PajaTypes;
using namespace Composition;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CIntComboTypeInDlg dialog


CIntComboTypeInDlg::CIntComboTypeInDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CIntComboTypeInDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CIntComboTypeInDlg)
	m_i32Value = -1;
	m_sInfoText = _T("");
	//}}AFX_DATA_INIT
	m_i32DefaultValue = 0;
}


void CIntComboTypeInDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CIntComboTypeInDlg)
	DDX_Control(pDX, IDOK, m_rOk);
	DDX_Control(pDX, IDCANCEL, m_rCancel);
	DDX_Control(pDX, IDC_COMBO, m_rCombo);
	DDX_CBIndex(pDX, IDC_COMBO, m_i32Value);
	DDX_Text(pDX, IDC_INFO, m_sInfoText);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CIntComboTypeInDlg, CDialog)
	//{{AFX_MSG_MAP(CIntComboTypeInDlg)
	ON_CBN_SELCHANGE(IDC_COMBO, OnSelchangeCombo)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CIntComboTypeInDlg message handlers

BOOL CIntComboTypeInDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// update dialog data
	m_pParam->get_val( m_i32Time, m_i32Value );
	UpdateData( FALSE );

	// add items to combobox
	int32	i32Sel = 0;
	for( uint32 i = 0; i < m_pParam->get_label_count(); i++ ) {
		if( i == 0 ) m_i32DefaultValue = m_pParam->get_label_value( i );
		int32	i32Idx = m_rCombo.AddString( m_pParam->get_label_name( i ) );
		m_rCombo.SetItemData( i32Idx, m_pParam->get_label_value( i ) );
		if( m_pParam->get_label_value( i ) == m_i32Value )
			i32Sel = i32Idx;
	}
	m_rCombo.SetCurSel( i32Sel );

	m_rOk.SetFlat( FALSE );
	m_rCancel.SetFlat( FALSE );

	return TRUE;
}

void CIntComboTypeInDlg::OnSelchangeCombo() 
{
	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	UpdateData( TRUE );

	int32	i32Num = m_rCombo.GetItemData( m_i32Value );

	if( i32Num != CB_ERR )
		pDoc->HandleParamNotify( m_pParam->set_val( m_i32Time, i32Num ) );
	else
		pDoc->HandleParamNotify( m_pParam->set_val( m_i32Time, m_i32DefaultValue ) );

	pDoc->NotifyViews( NOTIFY_REDRAW_GRAPHICS );
}
