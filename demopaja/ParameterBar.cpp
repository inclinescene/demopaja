// ParameterBar.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "demopajadoc.h"
#include "ParameterBar.h"
#include "DynDialog.h"
#include "Composition.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


using namespace PajaTypes;
using namespace Edit;
using namespace Import;
using namespace Composition;
using namespace PluginClass;


/////////////////////////////////////////////////////////////////////////////
// CParameterBar

CParameterBar::CParameterBar() :
	m_pEffect( 0 )
{
}

CParameterBar::~CParameterBar()
{
}


BEGIN_MESSAGE_MAP(CParameterBar, baseCMyBar)
	//{{AFX_MSG_MAP(CParameterBar)
	ON_WM_PAINT()
	ON_WM_CREATE()
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CParameterBar message handlers

DROPEFFECT CParameterBar::OnDragOver( COleDataObject* pDataObject, DWORD dwKeyState, CPoint point )
{
	return DROPEFFECT_NONE;
}

BOOL CParameterBar::OnDrop( COleDataObject* pDataObject, DROPEFFECT dropEffect, CPoint point )
{
	return FALSE;
}

void CParameterBar::UpdateFromEffect( EffectI* pEffect, bool bChangeUI )
{
	CDemopajaDoc*	pDoc = GetDoc();
	ASSERT_VALID( pDoc );

	pDoc->EndDeferHandleParamNotify();

	if( pEffect && pEffect == m_pEffect ) {
		// Update parameters

		bool	bGizmoLayoutSame = true;

		if( pEffect->get_gizmo_count() == m_wndRollup.GetPagesCount() ) {
			int32	i32SameCount = 0;
			for( uint32 i = 0; i < m_wndRollup.GetPagesCount(); i++ ) {
				RC_PAGEINFO*	pInfo = m_wndRollup.GetPageInfo( i );
				if( pInfo->pGizmo != pEffect->get_gizmo( i ) ) {
					bGizmoLayoutSame = false;
					break;
				}
			}
		}
		else {
			bGizmoLayoutSame = false;
		}

		if( bGizmoLayoutSame ) {
			for( uint32 i = 0; i < m_wndRollup.GetPagesCount(); i++ ) {
				RC_PAGEINFO*	pInfo = m_wndRollup.GetPageInfo( i );

				// check the expanded state
				if( pInfo->pGizmo->get_flags() & ITEM_EXPANDED && !pInfo->bExpanded )
					m_wndRollup.ExpandPage( i, TRUE );
				else if( !(pInfo->pGizmo->get_flags() & ITEM_EXPANDED) && pInfo->bExpanded )
					m_wndRollup.ExpandPage( i, FALSE );

				// update parameters
				CDynDialog*	pDlg = (CDynDialog*)pInfo->pwndTemplate;
//				pDlg->UpdateAllParamaters();
				pDlg->UpdateFromGizmo( pInfo->pGizmo );
			}
			m_wndRollup.UpdateLayout();
			Invalidate();
			return;
		}
	}


	m_pEffect = pEffect;

	// clear rollup
	m_wndRollup.RemoveAllPages();

	if( m_pEffect == 0 )
		return;

	CRect	rRect;
	GetClientRect( &rRect );
	rRect.right = rRect.Width();
	rRect.left = 0;
	rRect.top = 0;
	rRect.bottom = 0;


	bool	bFailed = false;

	// Create dialogs
	for( uint32 i = 0; i < pEffect->get_gizmo_count(); i++ ) {
		GizmoI*	pGizmo = pEffect->get_gizmo( i );

		CDynDialog*	pDlg = new CDynDialog;

//		for( uint32 j = 0; j < pGizmo->get_parameter_count(); j++ ) {
//			pDlg->AddParameter( pGizmo->get_parameter( j ) );
//		}

		if( !pDlg->Create( WS_VISIBLE | WS_CHILD | DS_3DLOOK, rRect, &m_wndRollup ) ) {
			delete pDlg;
			bFailed = true;
			break;
		}

		int32	i32Idx = m_wndRollup.InsertPage( pGizmo->get_name(), pDlg, pGizmo );

		pDlg->UpdateFromGizmo( pGizmo );

//		if( pGizmo->get_flags() & ITEM_EXPANDED )
//			m_wndRollup.ExpandPage( i32Idx, TRUE );
	}

	if( bFailed )
		m_wndRollup.RemoveAllPages();

	m_wndRollup.UpdateLayout();

/*	for( i = 0; i < m_wndRollup.GetPagesCount(); i++ ) {
		RC_PAGEINFO*	pInfo = m_wndRollup.GetPageInfo( i );
		if( pInfo->pGizmo && (pInfo->pGizmo->get_flags() & ITEM_EXPANDED) )
			m_wndRollup.ExpandPage( i, TRUE );
	}*/

	Invalidate();
}

void CParameterBar::OnPaint() 
{
	CPaintDC dc(this); // device context for painting


	CRect	rClient;
	GetClientRect( &rClient );

	COLORREF rBgColor = ::GetSysColor( COLOR_BTNFACE );
	COLORREF rTextColor = ::GetSysColor( COLOR_BTNTEXT );

	dc.FillSolidRect( 0, 0, rClient.Width(), 20, rBgColor );
	
	CFont*	pOldFont = dc.SelectObject( &m_rNormalFont );

	dc.SetTextColor( rTextColor );
	dc.SetBkMode( TRANSPARENT );

	CString	sName;

	if( m_pEffect ) {
		SceneC*	pScene = 0;
		LayerC*	pLayer = m_pEffect->get_parent();
		if( pLayer )
			pScene = pLayer->get_parent();

		if( pScene ) {
			sName += pScene->get_name();
			sName += " / ";
		}
		if( pLayer ) {
			sName += pLayer->get_name();
			sName += " / ";
		}
		sName += m_pEffect->get_name();
	}
	else
		sName = "---";

	dc.TextOut( 3, 2, sName );

	dc.SelectObject( pOldFont );

//	if( m_pEffect )
//		dc.TextOut( 10, 10, m_pEffect->get_name() );
}

int CParameterBar::OnCreate( LPCREATESTRUCT lpCreateStruct )
{
	if( baseCMyBar::OnCreate( lpCreateStruct ) == -1 )
		return -1;

	if( !m_rBoldFont.CreateFont( 14, 0, 0, 0, FW_BOLD, 0, 0, 0,
		ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"arial" ) ) {
		TRACE( "Cannot create default font.\n" );
		return -1;
	}
	if( !m_rNormalFont.CreateFont( 14, 0, 0, 0, FW_NORMAL, 0, 0, 0,
		ANSI_CHARSET, OUT_DEFAULT_PRECIS,
		CLIP_DEFAULT_PRECIS,
		DEFAULT_QUALITY,
		DEFAULT_PITCH | FF_DONTCARE,
		"arial" ) ) {
		TRACE( "Cannot create default font.\n" );
		return -1;
	}

	if( !m_wndRollup.Create( WS_VISIBLE|WS_CHILD, CRect( 0, 0, 0, 0 ), this, IDC_PARAMROLLUP ) )
		return -1;
	m_wndRollup.SetButtonFont( &m_rBoldFont );
	m_wndRollup.ModifyStyleEx( 0, WS_EX_STATICEDGE, SWP_FRAMECHANGED );	// sunken border
	
	return 0;
}

void CParameterBar::OnSize(UINT nType, int cx, int cy) 
{
	baseCMyBar::OnSize( nType, cx, cy );
	m_wndRollup.MoveWindow( 0, 20, cx, cy - 20, TRUE );
}

BOOL CParameterBar::OnEraseBkgnd( CDC* pDC )
{
	return FALSE;
}

HBRUSH CParameterBar::OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor )
{
	return (HBRUSH)::GetStockObject( NULL_BRUSH );
}
