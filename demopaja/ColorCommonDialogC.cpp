

#include "stdafx.h"
#include "demopaja.h"

#include "PajaTypes.h"
#include "ColorC.h"
#include "ClassIdC.h"
#include "DataBlockI.h"
#include "CommonDialogI.h"
#include "ColorCommonDialogC.h"

#include "ColorTypeInDlg.h"

using namespace PajaSystem;
using namespace PajaTypes;
using namespace PluginClass;

	
ColorCommonDialogC::ColorCommonDialogC() :
	m_hWndParent( 0 )
{
	// empty
}

ColorCommonDialogC::~ColorCommonDialogC()
{
	// empty
}


Edit::DataBlockI*
ColorCommonDialogC::create()
{
	return new ColorCommonDialogC;
}


ColorCommonDialogC*
ColorCommonDialogC::create_new()
{
	return new ColorCommonDialogC;
}

ClassIdC
ColorCommonDialogC::get_class_id()
{
	return CLASS_COLORCOMMONDIALOG;
}

bool
ColorCommonDialogC::do_modal()
{
	CColorTypeInDlg	rDlg( CWnd::FromHandle( m_hWndParent ) );

	rDlg.m_rColorValue = m_rColor;
	rDlg.m_sCaption = m_sCaption.c_str();

	if( rDlg.DoModal() == IDOK ) {
		m_rColor = rDlg.m_rColorValue;
		return true;
	}

	return false;
}

void
ColorCommonDialogC::set_caption( const char* szName )
{
	m_sCaption = szName;
}

void
ColorCommonDialogC::set_parent_wnd( HWND hParent )
{
	m_hWndParent = hParent;
}

void
ColorCommonDialogC::set_color( const ColorC& rCol )
{
	m_rColor = rCol;
}

const ColorC&
ColorCommonDialogC::get_color() const
{
	return m_rColor;
}

