#include "stdafx.h"
#include "demopaja.h"

#include "PajaTypes.h"
#include "ColorC.h"
#include "ClassIdC.h"
#include "DataBlockI.h"
#include "CommonDialogI.h"
#include "ChooseImporterCommonDialogC.h"

#include "ChooseImporterDlg.h"

using namespace PajaSystem;
using namespace PajaTypes;
using namespace PluginClass;

	
ChooseImporterCommonDialogC::ChooseImporterCommonDialogC() :
	m_i32SelID( -1 ),
	m_bUseToAll( false )
{
	// empty
}

ChooseImporterCommonDialogC::~ChooseImporterCommonDialogC()
{
	// empty
}


Edit::DataBlockI*
ChooseImporterCommonDialogC::create()
{
	return new ChooseImporterCommonDialogC;
}


ChooseImporterCommonDialogC*
ChooseImporterCommonDialogC::create_new()
{
	return new ChooseImporterCommonDialogC;
}

ClassIdC
ChooseImporterCommonDialogC::get_class_id()
{
	return CLASS_CHOOSEIMPORTERCOMMONDIALOG;
}

bool
ChooseImporterCommonDialogC::do_modal()
{
	m_i32SelID = -1;
	m_bUseToAll = false;

	CChooseImporterDlg	rChooseImpDlg;

	rChooseImpDlg.m_sFileName = m_sFileName.c_str();

	for( uint32 i = 0; i < m_rImporters.size(); i++ )
		rChooseImpDlg.AddImporter( m_rImporters[i].m_sDesc.c_str(), m_rImporters[i].m_ui32ID );

	// prepare the dialog for new use
	m_sFileName.clear();
	m_rImporters.clear();

	if( rChooseImpDlg.DoModal() == IDOK ) {
		if( rChooseImpDlg.GetSelectedImporter() != -1 ) {
			m_i32SelID = rChooseImpDlg.GetSelectedImporter();
			m_bUseToAll = rChooseImpDlg.m_bUseToAll == TRUE ? true : false;
			return true;
		}
	}
	return false;
}

void
ChooseImporterCommonDialogC::set_file_name( const char* szFilename )
{
	m_sFileName = szFilename;
}

void
ChooseImporterCommonDialogC::add_importer( const char* szDesc, uint32 ui32ID )
{
	ImporterS	rImp;
	rImp.m_sDesc = szDesc;
	rImp.m_ui32ID = ui32ID;
	m_rImporters.push_back( rImp );
}

int32
ChooseImporterCommonDialogC::get_selected_importer() const
{
	return m_i32SelID;
}

bool
ChooseImporterCommonDialogC::get_use_extension_to_all() const
{
	return m_bUseToAll;
}
