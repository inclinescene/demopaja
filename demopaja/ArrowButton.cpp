// ArrowButton.cpp : implementation file
//

#include "stdafx.h"
#include "ArrowButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CArrowButton

CArrowButton::CArrowButton() :
	m_iExpanded( 0 )
{
}

CArrowButton::~CArrowButton()
{
}


BEGIN_MESSAGE_MAP(CArrowButton, CButton)
	//{{AFX_MSG_MAP(CArrowButton)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CArrowButton message handlers

void CArrowButton::DrawItem( LPDRAWITEMSTRUCT lpDIS ) 
{
	CDC*	pDC = CDC::FromHandle( lpDIS->hDC );

	BOOL bIsChecked  = (lpDIS->itemState & ODS_CHECKED);
	BOOL bIsPressed  = (lpDIS->itemState & ODS_SELECTED);
	BOOL bIsFocused  = (lpDIS->itemState & ODS_FOCUS);
	BOOL bIsDisabled = (lpDIS->itemState & ODS_DISABLED);

	CRect itemRect = lpDIS->rcItem;

	CBrush	br( GetSysColor( COLOR_BTNFACE ) );  
	pDC->FillRect( &itemRect, &br );

	// draw button
	pDC->SetBkMode( TRANSPARENT );

	// draw separator line
	pDC->FillSolidRect( itemRect.left, itemRect.top, itemRect.Width(), 1, ::GetSysColor(COLOR_BTNSHADOW) );

	if( m_iExpanded == 1 ) {
		int	iCenter = (itemRect.top + itemRect.bottom) / 2;

		CPen	rGrey( PS_SOLID, 0, GetSysColor( COLOR_BTNSHADOW ) );
		CPen	rDark( PS_SOLID, 0, GetSysColor( COLOR_3DDKSHADOW ) );
		CPen*	pOldPen = pDC->SelectObject( &rGrey );
		CBrush*	pOldBrush = pDC->SelectObject( &br );

		POINT	rTriangle[3];
		rTriangle[0].x = itemRect.left;
		rTriangle[0].y = iCenter - 2;
		rTriangle[1].x = itemRect.left + 10;
		rTriangle[1].y = iCenter - 2;
		rTriangle[2].x = itemRect.left + 5;
		rTriangle[2].y = iCenter + 3;

		pDC->Polygon( rTriangle, 3 );

		pDC->SelectObject( &rDark );
		pDC->MoveTo( rTriangle[1].x, rTriangle[1].y );
		pDC->LineTo( rTriangle[2].x, rTriangle[2].y );

		pDC->SelectObject( pOldPen );
		pDC->SelectObject( pOldBrush );
	}
	else {
		int	iCenter = (itemRect.top + itemRect.bottom) / 2;

		CPen	rGrey( PS_SOLID, 0, GetSysColor( COLOR_BTNSHADOW ) );
		CPen	rDark( PS_SOLID, 0, GetSysColor( COLOR_3DDKSHADOW ) );
		CPen*	pOldPen = pDC->SelectObject( &rGrey );
		CBrush*	pOldBrush = pDC->SelectObject( &br );

		POINT	rTriangle[3];
		rTriangle[0].x = itemRect.left + 5 + 2;
		rTriangle[0].y = iCenter;
		rTriangle[1].x = itemRect.left + 2;
		rTriangle[1].y = iCenter + 5;
		rTriangle[2].x = itemRect.left + 2;
		rTriangle[2].y = iCenter - 5;

		pDC->Polygon( rTriangle, 3 );

		pDC->SelectObject( &rDark );
		pDC->MoveTo( rTriangle[0].x, rTriangle[0].y );
		pDC->LineTo( rTriangle[1].x, rTriangle[1].y );

		pDC->SelectObject( pOldPen );
		pDC->SelectObject( pOldBrush );
	}

	// Read the button's title
	CString sTitle;
	GetWindowText( sTitle );

	CRect captionRect = lpDIS->rcItem;
	// Write the button title (if any)
	if( sTitle.IsEmpty() == FALSE ) {
		// Draw the button's title
		// If button is pressed then "press" title also
		if( bIsPressed )
			captionRect.OffsetRect( 1, 1 );

		// make room for the arrow
		captionRect.left += 20;

		// Center text
		CRect centerRect = captionRect;
		pDC->DrawText(sTitle, -1, captionRect, DT_WORDBREAK | DT_CENTER | DT_CALCRECT);
		captionRect.OffsetRect(0/*(centerRect.Width() - captionRect.Width())/2*/, (centerRect.Height() - captionRect.Height())/2);

		if( bIsDisabled ) {
			captionRect.OffsetRect(1, 1);
			pDC->SetTextColor( ::GetSysColor( COLOR_3DHILIGHT ) );
			pDC->DrawText(sTitle, -1, captionRect, DT_WORDBREAK | DT_CENTER);
			captionRect.OffsetRect(-1, -1);
			pDC->SetTextColor( ::GetSysColor( COLOR_3DSHADOW ) );
			pDC->DrawText(sTitle, -1, captionRect, DT_WORDBREAK | DT_CENTER);
		}
		else {
			if( bIsChecked )
				pDC->SetTextColor( RGB( 255, 255, 0 ) );
			else
				pDC->SetTextColor( ::GetSysColor( COLOR_BTNTEXT ) );
			pDC->DrawText( sTitle, -1, captionRect, DT_WORDBREAK | DT_CENTER );
		}
	}

}

void CArrowButton::PreSubclassWindow() 
{
	UINT nBS;
	nBS = GetButtonStyle();

	// Add BS_OWNERDRAW style
	SetButtonStyle( nBS | BS_OWNERDRAW );
	CButton::PreSubclassWindow();
}

LRESULT CArrowButton::DefWindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
	if( message == WM_LBUTTONDBLCLK ) {
		message = WM_LBUTTONDOWN;
	}	

	return CButton::DefWindowProc(message, wParam, lParam);
}

void CArrowButton::SetExpanded( int iState )
{
	m_iExpanded = iState;
	Invalidate();
}

int CArrowButton::GetExpanded() const
{
	return m_iExpanded;
}
