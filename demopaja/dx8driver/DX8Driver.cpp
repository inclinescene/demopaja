
#define WIN32_LEAN_AND_MEAN     // Exclude rarely-used stuff from

// Windows headers
#include <windows.h>
#include <commctrl.h>

// Demopaja headers
#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "ClassDescC.h"
#include "DX8DeviceC.h"
#include "DX8Driver.h"

using namespace PajaTypes;
using namespace PluginClass;
using namespace PajaSystem;
using namespace Edit;



//////////////////////////////////////////////////////////////////////////
//
//  DirectX 8 Device Driver class descriptor.
//

DX8DeviceDescC::DX8DeviceDescC()
{
	// empty
}

DX8DeviceDescC::~DX8DeviceDescC()
{
	// empty
}

void*
DX8DeviceDescC::create()
{
	return DX8DeviceC::create_new();
}

int32
DX8DeviceDescC::get_classtype() const
{
	return CLASS_TYPE_DEVICEDRIVER;
}

SuperClassIdC
DX8DeviceDescC::get_super_class_id() const
{
	return SUPERCLASS_GRAPHICSDEVICE;
}

ClassIdC
DX8DeviceDescC::get_class_id() const
{
	return CLASS_DX8_DEVICEDRIVER;
}

const char*
DX8DeviceDescC::get_name() const
{
	return "DirectX 8 Device Driver";
}

const char*
DX8DeviceDescC::get_desc() const
{
	return "DirectX 8 Device Driver";
}

const char*
DX8DeviceDescC::get_author_name() const
{
	return "Mikko \"memon\" Mononen";
}

const char*
DX8DeviceDescC::get_copyright_message() const
{
	return "Copyright (c) 2001 Moppi Productions";
}

const char*
DX8DeviceDescC::get_url() const
{
	return "http://moppi.inside.org/demopaja/";
}

const char*
DX8DeviceDescC::get_help_filename() const
{
	return "res://DX8DeviceHelp.html";
}

uint32
DX8DeviceDescC::get_ext_count() const
{
	return 0;
}

const char*
DX8DeviceDescC::get_ext( uint32 ui32Index ) const
{
	return 0;
}

uint32
DX8DeviceDescC::get_required_device_driver_count() const
{
	return 0;
}

const ClassIdC&
DX8DeviceDescC::get_required_device_driver( uint32 ui32Idx )
{
	return NULL_CLASSID;
}


//////////////////////////////////////////////////////////////////////////
//
// Global descriptors, which will be returned to the Demopaja.
//

DX8DeviceDescC		g_rDX8DeviceDesc;


#ifndef PAJAPLAYER

//////////////////////////////////////////////////////////////////////////
//
// The DLL
//


HINSTANCE	g_hInstance = 0;
bool		g_bControlsInit = false;

BOOL APIENTRY
DllMain( HANDLE hModule, DWORD ulReasonForCall, LPVOID lpReserved )
{
	g_hInstance = (HINSTANCE)hModule;

	if( !g_bControlsInit ) {
		// Initialise common controls
		INITCOMMONCONTROLSEX	rInitCtrls;
		ZeroMemory( &rInitCtrls, sizeof( rInitCtrls ) );
		rInitCtrls.dwSize = sizeof( rInitCtrls );
		rInitCtrls.dwICC = ICC_WIN95_CLASSES;
        InitCommonControlsEx( &rInitCtrls );
		g_bControlsInit = true;
	}

	return TRUE;
}

// Returns number of classes inside this plugin DLL.
__declspec( dllexport )
int32
get_classdesc_count()
{
	return 1;
}

// Returns class descriptors of the plugin classes.
__declspec( dllexport )
ClassDescC*
get_classdesc( int32 i )
{
	if( i == 0 )
		return &g_rDX8DeviceDesc;
	return 0;
}

// Returns the API version this DLL was made with.
__declspec( dllexport )
int32
get_api_version()
{
	return DEMOPAJA_VERSION;
}

// Returns the DLL name.
__declspec( dllexport )
char*
get_dll_name()
{
	return "dxdevice.dll - DirectX 8 Device Driver (c) 2001 memon/moppi productions";
}


#endif	// PAJAPLAYER
