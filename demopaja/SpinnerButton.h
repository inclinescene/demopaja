//
// SpinnerButton.h : header file
//
/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000 Mikko Mononen <memon@inside.org>
// Feel free to use this file 
//
// This file implements a spinner button control similar
// to the one in 3D Studio MAX. Spinner can automatically
// update it's value to any control (buddy). Output value
// can be integer or double. Internally value is handled in
// floating-point.User can use two arrow buttons to increment
// and decrement the value or use mouse wheel or adjust the
// value by pressing any of the arrow buttons and moving
// the mouse. CTRL-key combined with any operation scales
// the increment by factor of 10. Value clampping can be
// enabed or disabled and minumum and maximum values can be set.
//

#if !defined(AFX_SPINNERBUTTON_H__3C1575C7_2514_11D4_A80C_0000E8D926FD__INCLUDED_)
#define AFX_SPINNERBUTTON_H__3C1575C7_2514_11D4_A80C_0000E8D926FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CSpinnerButton control

// Creation flags
#define		SPNB_SETBUDDYINT	0x00000001
#define		SPNB_SETBUDDYFLOAT	0x00000002
#define		SPNB_USELIMITS		0x00000004
#define		SPNB_ALL			0x0000000f

// Flags for set buddy
#define		SPNB_ATTACH_LEFT	0x0001
#define		SPNB_ATTACH_RIGHT	0x0002


class CSpinnerButton : public CWnd
{
public:
	// Construction
	CSpinnerButton();
	virtual ~CSpinnerButton();

	// Operations
	virtual BOOL	Create( DWORD dwStyle, const RECT& rect, CWnd* pParentWnd, UINT nID );

	// Attributes
	virtual CWnd*	SetBuddy( CWnd* buddy, DWORD nFlags = 0 );
	virtual CWnd*	GetBuddy() const;
	virtual void	SetScale( double scale );
	virtual	void	SetMinMax( double min, double max );
	virtual double	GetMin() const;
	virtual double	GetMax() const;
	virtual void	SetVal( double val );
	virtual double	GetVal() const;
	virtual void	SetMultiplier( double val );
	virtual double	GetMultiplier() const;

	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSpinnerButton)
	//}}AFX_VIRTUAL

protected:

	CWnd*	m_buddy;
	CRect	m_upRect;
	CRect	m_downRect;
	UINT	m_arrowSize;
	BOOL	m_up;
	BOOL	m_down;
	UINT	m_timer;
	BOOL	m_adjusting;
	HCURSOR	m_oldCursor;
	double	m_starty;
	UINT	m_flags;
	double	m_startValue;
	double	m_value;
	double	m_scale;
	double	m_multiplier;
	double	m_min, m_max;
	BOOL	m_negScale;
	UINT	m_timerCount;


	virtual void	UpdateBuddy( BOOL save );

	// Generated message map functions
	//{{AFX_MSG(CSpinnerButton)
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SPINNERBUTTON_H__3C1575C7_2514_11D4_A80C_0000E8D926FD__INCLUDED_)
