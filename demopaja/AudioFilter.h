
#ifndef __SAFARI_AUDIOFILTER_H__
#define __SAFARI_AUDIOFILTER_H__

#define FILTER_GAIN 1.0
#define FILTER_Q 0.5

enum EFilterType {
    FILTER_HIGH_PASS,
    FILTER_LOW_PASS
};

class CAudioFilter
{
public:
	CAudioFilter();
	virtual ~CAudioFilter();

	void		Init( float fCutOff, float fSamplingRate );
	float		Process( float fSample );
	float		GetCutOff() const;

protected:
	float	b0, b1, b2, a0, a1, a2;
	float	x1, x2, y1, y2;
	float	m_fCutOff;
};

#endif