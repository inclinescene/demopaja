// ColorButton.cpp : implementation file
//

#include "stdafx.h"
#include "demopaja.h"
#include "ColorButton.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CColorButton

CColorButton::CColorButton()
{
}

CColorButton::~CColorButton()
{
}


BEGIN_MESSAGE_MAP(CColorButton, CButton)
	//{{AFX_MSG_MAP(CColorButton)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CColorButton message handlers

void CColorButton::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	CDC*	pDC;
	CRect	rFocusRect, rButtonRect;
	UINT	uiState;
	
	pDC  = CDC::FromHandle( lpDrawItemStruct->hDC );
	uiState = lpDrawItemStruct->itemState;
	
	rFocusRect.CopyRect( &lpDrawItemStruct->rcItem );
	rButtonRect.CopyRect( &lpDrawItemStruct->rcItem );
	
	rFocusRect.InflateRect( -3, -3 );

	// Frame
	if( uiState & ODS_SELECTED ) { 
		pDC->Draw3dRect( &rButtonRect, m_rColorDark, m_rColorLight );
	}
	else {
		pDC->Draw3dRect( &rButtonRect, m_rColorLight, m_rColorDark );
	}


	rButtonRect.InflateRect( -1, -1 );
	
	// fill
	pDC->FillSolidRect( &rButtonRect, m_rColor );
	

	// Focus
	if( uiState & ODS_FOCUS )
		pDC->DrawFocusRect( &rFocusRect );
	
}


void CColorButton::SetColor( int iR, int iG, int iB, int iA )
{

	m_iR = iR;
	m_iG = iG;
	m_iB = iB;
	m_iA = iA;

/*
	int			m_iR, m_iG, m_iB, m_iA;
	COLORREF	m_rColor, m_rColorAlpha, m_rColorLight, m_rColorDark;
*/

	m_rColor = RGB( m_iR, m_iG, m_iB );
	m_rColorLight = RGB( 255 - ((255 - m_iR) / 2), 255 - ((255 - m_iG) / 2), 255 - ((255 - m_iB) / 2) );
	m_rColorDark = RGB( m_iR / 2, m_iG / 2, m_iB / 2 );

	Invalidate();
}

void CColorButton::PreSubclassWindow() 
{
	UINT nBS;
	nBS = GetButtonStyle();

	// Add BS_OWNERDRAW style
	SetButtonStyle( nBS | BS_OWNERDRAW );
	CButton::PreSubclassWindow();
}
