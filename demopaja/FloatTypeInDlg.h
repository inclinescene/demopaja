#if !defined(AFX_FLOATTYPEINDLG_H__6551EAFE_8F62_4AFB_B432_38BD915D1027__INCLUDED_)
#define AFX_FLOATTYPEINDLG_H__6551EAFE_8F62_4AFB_B432_38BD915D1027__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// FloatTypeInDlg.h : header file
//

#include "SpinnerButton.h"
#include "PajaTypes.h"
#include "ParamI.h"
#include "BtnST.h"

/////////////////////////////////////////////////////////////////////////////
// CFloatTypeInDlg dialog

class CFloatTypeInDlg : public CDialog
{
// Construction
public:
	CFloatTypeInDlg(CWnd* pParent = NULL);   // standard constructor

	PajaTypes::float32			m_f32Min, m_f32Max;
	bool						m_bClampValues;
	PajaTypes::float32			m_f32Inc;
	Composition::ParamFloatC*	m_pParam;
	PajaTypes::int32			m_i32Time;

	// Dialog Data
	//{{AFX_DATA(CFloatTypeInDlg)
	enum { IDD = IDD_FLOAT_TYPEIN };
	CButtonST	m_rOk;
	CButtonST	m_rCancel;
	CStatic	m_rStaticLabel1;
	CEdit	m_rEdit;
	float	m_rValue;
	CString	m_sInfoText;
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CFloatTypeInDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	CSpinnerButton		m_rSpinner;
	PajaTypes::float32	m_f32Scale;

	void AFXAPI		DDX_MyText( CDataExchange* pDX, int nIDC, float& value );

	// Generated message map functions
	//{{AFX_MSG(CFloatTypeInDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnChangeEdit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_FLOATTYPEINDLG_H__6551EAFE_8F62_4AFB_B432_38BD915D1027__INCLUDED_)
