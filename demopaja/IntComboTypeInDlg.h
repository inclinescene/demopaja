#if !defined(AFX_INTCOMBOTYPEINDLG_H__EED1D702_C159_4FA3_8A94_351D62463F5C__INCLUDED_)
#define AFX_INTCOMBOTYPEINDLG_H__EED1D702_C159_4FA3_8A94_351D62463F5C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IntComboTypeInDlg.h : header file
//

#include "PajaTypes.h"
#include "ParamI.h"
#include "BtnST.h"

/////////////////////////////////////////////////////////////////////////////
// CIntComboTypeInDlg dialog

class CIntComboTypeInDlg : public CDialog
{
// Construction
public:
	CIntComboTypeInDlg(CWnd* pParent = NULL);   // standard constructor

	Composition::ParamIntC*		m_pParam;
	PajaTypes::int32			m_i32Time;

	// Dialog Data
	//{{AFX_DATA(CIntComboTypeInDlg)
	enum { IDD = IDD_INT_COMBO_TYPEIN };
	CButtonST	m_rOk;
	CButtonST	m_rCancel;
	CComboBox	m_rCombo;
	int		m_i32Value;
	CString	m_sInfoText;
	//}}AFX_DATA


	// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIntComboTypeInDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	// Implementation
protected:
	PajaTypes::int32			m_i32DefaultValue;

	// Generated message map functions
	//{{AFX_MSG(CIntComboTypeInDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSelchangeCombo();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_INTCOMBOTYPEINDLG_H__EED1D702_C159_4FA3_8A94_351D62463F5C__INCLUDED_)
