# Demopaja by Moppi

This is **not by Incline!**  This is a read-only copy of the Demopaja source,
for easier browsing.  Demopaja is by [memon](https://demozoo.org/sceners/380/)
of [Moppi Productions](https://demozoo.org/groups/381/).

## Original announcement

This is copied from [memon's blog post announcing the source release](http://digestingduck.blogspot.com/2012/04/demopaja-sources.html).
The download is also available [on scene.org](https://files.scene.org/view/resources/demomaker/demopaja/source/demopaja_050508.zip)).

> ### Digesting Duck
> 
> Blog about game AI and prototyping
> 
> Sunday, April 15, 2012
> 
> ### Demopaja Sources
>
> ![screenshot](demopaja.jpg)
>
> Inspired by Farbrasuch's release of their demo tools, I decided to dig up my old Demopaja backup, clean up some directories from random crap, and put it up here for downloads:
> 
> [`Demopaja_050508.zip`](https://sites.google.com/site/recastnavigation/Demopaja_050508.zip)
> 
> 
> I don't have Windows dev machine these days anymore so I have no idea if it still compiles. I think it compiled back in 5th May in 2008 when I looked at it the last time.
> 
> I'm still quite proud of the tool, the documentation and all that. Even if I do despise the coding style – I was young then, you know.
> 
> [EDIT] You can treat the code public domain, some libs and snippets have their own licenses, treat them appropriately.
> 
> Posted by Mikko Mononen at 6:07 AM 

