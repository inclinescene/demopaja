//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by FlareImagePlugin.rc
//
#define IDD_DIALOG1                     101
#define IDD_EDITDIALOG                  101
#define IDC_HAND                        106
#define IDC_HANDDRAG                    107
#define IDC_BUTTONGLOW                  1004
#define IDC_VIEW                        1005
#define IDC_COMBOSIZE                   1006
#define IDC_BUTTONSPOT                  1007
#define IDC_BUTTONSPIKES                1008
#define IDC_EDITNAME                    1009
#define IDC_BUTTONBG                    1010
#define IDC_BUTTONZOOMIN                1011
#define IDC_BUTTONZOOMOUT               1012
#define IDC_EDITZOOM                    1014
#define IDC_STATICGLOWALPHA             1015
#define IDC_STATICSPOTALPHA             1016
#define IDC_STATICSPIKESALPHA           1017
#define IDC_STATICBGALPHA               1018

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        110
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
