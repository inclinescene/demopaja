#ifndef __FLARERENDERERC_H__
#define __FLARERENDERERC_H__

#include "PajaTypes.h"
#include "ColorC.h"

class FlareRendererC
{
public:
	FlareRendererC();
	~FlareRendererC();

	static void		render_flare( PajaTypes::uint8* pBuffer, PajaTypes::uint32 ui32Width, PajaTypes::uint32 ui32Height,
								  const PajaTypes::ColorC& rBGColor, const PajaTypes::ColorC& rGlowColor, const PajaTypes::ColorC& rSpotColor, const PajaTypes::ColorC& rSpikesColor, bool bUseTransparency );
};


#endif