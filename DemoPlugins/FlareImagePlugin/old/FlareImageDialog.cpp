#include <windows.h>
#include "PajaTypes.h"
#include "ColorC.h"
#include "res\resource.h"
#include <math.h>
#include "SpinnerBtnC.h"
#include <string>
#include <stdio.h>
#include "ColorCommonDialogC.h"
#include "ImportInterfaceC.h"

using namespace PajaTypes;
using namespace Import;
using namespace PajaSystem;


inline
ColorC
COLOREF_to_color( COLORREF rColRef )
{
	ColorC	rCol;
	rCol.convert_from_uint8( GetRValue( rColRef ), GetGValue( rColRef ), GetBValue( rColRef ) );
	return rCol;
}

inline
COLORREF
color_to_COLOREF( const ColorC& rCol )
{
	return RGB( (int32)(rCol[0] * 255.0f), (int32)(rCol[1] * 255.0f), (int32)(rCol[2] * 255.0f) );
}


//
// DIB class
//

class CDIB  
{
public:
	CDIB ();
	virtual ~CDIB ();
	BOOL	Create( int Width, int Height );
	void	Destroy();
	void	PasteToDC( HDC hDC, int x, int y );
	void	PasteToDC( HDC hDC, int x, int y, int w , int h );

	UINT*		m_bits;
	BITMAPINFO	m_info;
	HBITMAP		m_bitmap;
	SIZE		m_size;
};



inline
CDIB::CDIB ()
{
	m_bits = NULL;
	m_bitmap = NULL;
	m_size.cx = 0;
	m_size.cy = 0;
}

inline
CDIB::~CDIB ()
{
	Destroy ();
}

BOOL
CDIB::Create( int width, int height )
{
	Destroy();

	ZeroMemory( &m_info, sizeof( BITMAPINFO ) );

	m_info.bmiHeader.biSize = sizeof( BITMAPINFOHEADER );
	m_info.bmiHeader.biWidth = width;
	m_info.bmiHeader.biHeight = -height;
	m_info.bmiHeader.biPlanes = 1;
	m_info.bmiHeader.biBitCount = 32; 
	m_info.bmiHeader.biCompression = BI_RGB;
	m_info.bmiHeader.biSizeImage = width * height * 4;
    
	m_bitmap = CreateDIBSection( NULL, &m_info, DIB_RGB_COLORS, (void **)&m_bits, NULL, NULL ); 

	if( m_bitmap ) {
		m_size.cx = width;
		m_size.cy = height;
		return TRUE;
	} else {
		m_size.cx = 0;
		m_size.cy = 0;
		return FALSE;
	}
}

void
CDIB::Destroy ()
{
	if( m_bitmap )
		DeleteObject( m_bitmap );
	m_bitmap = NULL;
	m_size.cx = 0;
	m_size.cy = 0;
}

inline void
CDIB::PasteToDC( HDC hDC, int x, int y )
{
    // x,y - the DC coordinates to start from
	SetDIBitsToDevice( hDC, x, y, m_size.cx, m_size.cy, 0, 0, 0, m_size.cy, m_bits, &m_info, 0 ); 
}

inline void
CDIB::PasteToDC( HDC hDC, int x, int y, int w , int h )
{
    // x,y - the DC coordinates to start from
	// w,h - the destination width and height
	StretchDIBits( hDC, x, y, w, h, 0, 0, m_size.cx, m_size.cy, m_bits, &m_info, DIB_RGB_COLORS, SRCCOPY );
}



//
// dialog prog
//

extern HINSTANCE	g_hInstance;

ImportInterfaceC*	g_pImpInterface;

std::string	g_sName;
uint32		g_ui32Width;
uint32		g_ui32Height;

/*
COLORREF	g_rGlowColor;
COLORREF	g_rSpotColor;
COLORREF	g_rSpikesColor;
COLORREF	g_rBGColor;*/

ColorC	g_rGlowColor;
ColorC	g_rSpotColor;
ColorC	g_rSpikesColor;
ColorC	g_rBGColor;

CDIB*		g_pDIB = 0;

RECT		g_rViewRect;
static COLORREF	acrCustClr[16]; 



int32			g_i32Zoom = 100;
int32			g_i32OffsetX = 0;
int32			g_i32OffsetY = 0;
int32			g_i32OffsetXMax = 0;
int32			g_i32OffsetYMax = 0;
int32			g_i32StartOffsetX = 0;
int32			g_i32StartOffsetY = 0;

int32			g_i32StartX = 0;
int32			g_i32StartY = 0;
bool			g_bPanorate = false;

static	uint32	g_ui32Seed = 1;

inline
uint32
irand()
{
	return ((uint32)(g_ui32Seed = 0x015a4e35 * g_ui32Seed + 1) >> 16);
}


void
render_flare( uint8* pBuffer, uint32 ui32Width, uint32 ui32Height,
			  const ColorC& rBGColor, const ColorC& rGlowColor, const ColorC& rSpotColor, const ColorC& rSpikesColor )
{
	int		r, g, b, a;
	float	dx, dy, mag, ymag;

	float	maxDist = (float)ui32Width / 2.0f;
	maxDist *= maxDist;

	uint8*	pDest = 0;
	uint32	i, j;

	// Draw glow
	pDest = (uint8*)pBuffer;
	for( i = 0; i < ui32Height; i++ ) {
		dy = (float)i - (float)ui32Height / 2.0f;
		ymag = (float)exp( -dy * dy / maxDist );
		for( j = 0; j < ui32Width; j++ ) {

			dx = (float)j - (float)ui32Width / 2.0f;
			mag = (float)exp( -dx * dx / maxDist ) * ymag;
			mag *= mag;
			mag *= 0.5f;
			r = (int)((rGlowColor[0] * mag + rBGColor[0] * (1.0f - mag)) * 255.0f);
			g = (int)((rGlowColor[1] * mag + rBGColor[1] * (1.0f - mag)) * 255.0f);
			b = (int)((rGlowColor[2] * mag + rBGColor[2] * (1.0f - mag)) * 255.0f);
			a = (int)((rGlowColor[3] * mag + rBGColor[3] * (1.0f - mag)) * 255.0f);

			*pDest++ = (uint8)b;
			*pDest++ = (uint8)g;
			*pDest++ = (uint8)r;
			*pDest++ = (uint8)a;
		}
	}

	// Draw spot
	pDest = (uint8*)pBuffer;
	for( i = 0; i < ui32Height; i++ ) {
		dy = (float)i - (float)ui32Height / 2.0f;
		ymag = (float)exp( -dy * dy / maxDist );
		for( j = 0; j < ui32Width; j++ ) {

			dx = (float)j - (float)ui32Width / 2.0f;
			mag = (float)exp( -dx * dx / maxDist ) * ymag;
			mag = pow( mag, 15 ) * 1.3f;
			r = (int)(rSpotColor[0] * mag * 255.0f);
			g = (int)(rSpotColor[1] * mag * 255.0f);
			b = (int)(rSpotColor[2] * mag * 255.0f);
			a = (int)(rSpotColor[3] * mag * 255.0f);

			*pDest = (uint8)__min( b + *pDest, 255 );
			pDest++;
			*pDest = (uint8)__min( g + *pDest, 255 );
			pDest++;
			*pDest = (uint8)__min( r + *pDest, 255 );
			pDest++;
			*pDest = (uint8)__min( a + *pDest, 255 );
			pDest++;
		}
	}

#define		SPIKE_COUNT		64
#define		SPIKE_MASK		(SPIKE_COUNT - 1)

	g_ui32Seed = 12345678;
	float32	f32Spikes[SPIKE_COUNT];
	ZeroMemory( f32Spikes, SPIKE_COUNT * sizeof( float32 ) );
	for( i = 0; i < 16; i++ )
		f32Spikes[irand() & SPIKE_MASK] = (float32)(32 + (irand() & 255)) / (32.0f + 255.0f);

	// Draw spikes
	pDest = (uint8*)pBuffer;
	for( i = 0; i < ui32Height; i++ ) {
		dy = (float)i - (float)ui32Height / 2.0f;
		ymag = (float)exp( -dy * dy / maxDist );
		for( j = 0; j < ui32Width; j++ ) {


			dx = (float)j - (float)ui32Width / 2.0f;

			float32	f32Angle = atan2( dx, dy );
			f32Angle = ((f32Angle / M_PI) * 0.5f + 0.5f) * (float32)SPIKE_COUNT;

			int32	i32Idx1 = (int32)f32Angle;
			int32	i32Idx2 = i32Idx1 + 1;
			float32	f32A = f32Angle - (float32)i32Idx1;

			i32Idx1 &= SPIKE_MASK;
			i32Idx2 &= SPIKE_MASK;

			mag = (float)exp( -dx * dx / maxDist ) * ymag;
			mag = pow( mag, 15 ) + mag * 0.55 * (f32Spikes[i32Idx1] * (1.0f - f32A) + f32Spikes[i32Idx2] * f32A);
			mag = __min( mag, 1.0f );
			mag *= mag; //pow( mag, 5 );

			r = (int)(rSpikesColor[0] * mag * 255.0f);
			g = (int)(rSpikesColor[1] * mag * 255.0f);
			b = (int)(rSpikesColor[2] * mag * 255.0f);
			a = (int)(rSpikesColor[3] * mag * 255.0f);

			*pDest = (uint8)__min( b + *pDest, 255 );
			pDest++;
			*pDest = (uint8)__min( g + *pDest, 255 );
			pDest++;
			*pDest = (uint8)__min( r + *pDest, 255 );
			pDest++;
			*pDest = (uint8)__min( a + *pDest, 255 );
			pDest++;
		}
	}
}


void
create_flare()
{
	if( g_pDIB ) {
		delete g_pDIB;
		g_pDIB = 0;
	}

	g_pDIB = new CDIB();
	g_pDIB->Create( g_ui32Width, g_ui32Height );

/*	ColorC	rGlow, rSpot, rSpikes, rBG;
	rGlow.convert_from_uint8( GetRValue( g_rGlowColor ), GetGValue( g_rGlowColor ), GetBValue( g_rGlowColor ) );
	rSpot.convert_from_uint8( GetRValue( g_rSpotColor ), GetGValue( g_rSpotColor ), GetBValue( g_rSpotColor ) );
	rSpikes.convert_from_uint8( GetRValue( g_rSpikesColor ), GetGValue( g_rSpikesColor ), GetBValue( g_rSpikesColor ) );
	rBG.convert_from_uint8( GetRValue( g_rBGColor ), GetGValue( g_rBGColor ), GetBValue( g_rBGColor ) );
*/

//	render_flare( (uint8*)g_pDIB->m_bits, g_ui32Width, g_ui32Height, rBG, rGlow, rSpot, rSpikes );
	render_flare( (uint8*)g_pDIB->m_bits, g_ui32Width, g_ui32Height, g_rBGColor, g_rGlowColor, g_rSpotColor, g_rSpikesColor );
}

void
ScreenToClient( HWND hWnd, LPRECT lpRect )
{
	POINT	rPt;
	rPt.x = lpRect->left;
	rPt.y = lpRect->top;
	ScreenToClient( hWnd, &rPt );
	lpRect->left = rPt.x;
	lpRect->top = rPt.y;

	rPt.x = lpRect->right;
	rPt.y = lpRect->bottom;
	ScreenToClient( hWnd, &rPt );
	lpRect->right = rPt.x;
	lpRect->bottom = rPt.y;
}

void
ClientToScreen( HWND hWnd, LPRECT lpRect )
{
	POINT	rPt;
	rPt.x = lpRect->left;
	rPt.y = lpRect->top;
	ClientToScreen( hWnd, &rPt );
	lpRect->left = rPt.x;
	lpRect->top = rPt.y;

	rPt.x = lpRect->right;
	rPt.y = lpRect->bottom;
	ClientToScreen( hWnd, &rPt );
	lpRect->right = rPt.x;
	lpRect->bottom = rPt.y;
}



void
UpdateScrollRange()
{
	int32	i32ViewWidth = g_rViewRect.right - g_rViewRect.left;
	int32	i32ViewHeight = g_rViewRect.bottom - g_rViewRect.top;

	int32	i32Width = g_pDIB->m_size.cx;
	int32	i32Height = g_pDIB->m_size.cy;

	i32Width *= g_i32Zoom;
	i32Height *= g_i32Zoom;
	i32Width /= 100;
	i32Height /= 100;

	g_i32OffsetXMax = 0;
	g_i32OffsetYMax = 0;

	if( i32Width > i32ViewWidth ) {
		g_i32OffsetXMax = i32Width - i32ViewWidth;
	}

	if( i32Height > i32ViewHeight ) {
		g_i32OffsetYMax = i32Height - i32ViewHeight;
	}

	if( g_i32OffsetX < 0 ) g_i32OffsetX = 0;
	if( g_i32OffsetX > g_i32OffsetXMax ) g_i32OffsetX = g_i32OffsetXMax;
	if( g_i32OffsetY < 0 ) g_i32OffsetY = 0;
	if( g_i32OffsetY > g_i32OffsetYMax ) g_i32OffsetY = g_i32OffsetYMax;
}


// Mesage handler for about box.
LRESULT CALLBACK
FlareDlgProc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam )
{
	switch( message ) {
	case WM_INITDIALOG:
		{
			// Get view area
			HWND	hView;
			hView = GetDlgItem( hDlg, IDC_VIEW );
			GetClientRect( hView, &g_rViewRect );
			ClientToScreen( hView, &g_rViewRect );
			ScreenToClient( hDlg, &g_rViewRect );


			HWND	hComboSize;
			hComboSize = GetDlgItem( hDlg, IDC_COMBOSIZE );

			// add selections
			SendMessage( hComboSize, CB_ADDSTRING, 0, (LPARAM)"32x32" );
			SendMessage( hComboSize, CB_ADDSTRING, 0, (LPARAM)"64x64" );
			SendMessage( hComboSize, CB_ADDSTRING, 0, (LPARAM)"128x128" );
			SendMessage( hComboSize, CB_ADDSTRING, 0, (LPARAM)"256x256" );

			int32	i32Selection = 0;

			switch( g_ui32Width ) {
			case 32:
				g_ui32Width = g_ui32Height = 32;
				i32Selection = 0;
				break;
			case 64:
				g_ui32Width = g_ui32Height = 64;
				i32Selection = 1;
				break;
			case 128:
				g_ui32Width = g_ui32Height = 128;
				i32Selection = 2;
				break;
			case 256:
				g_ui32Width = g_ui32Height = 256;
				i32Selection = 3;
				break;
			default:
				g_ui32Width = g_ui32Height = 32;
				i32Selection = 0;
				break;
			}
			SendMessage( hComboSize, CB_SETCURSEL, (WPARAM)i32Selection, 0 );

			create_flare();
			InvalidateRect( hDlg, &g_rViewRect, FALSE );


			HWND	hEditZoom;
			char	szVal[32];
			_snprintf( szVal, 31, "%d%%", g_i32Zoom );
			hEditZoom = GetDlgItem( hDlg, IDC_EDITZOOM );
			SetWindowText( hEditZoom, szVal );

			// name
			HWND	hEditName;
			hEditName = GetDlgItem( hDlg, IDC_EDITNAME );
			SetWindowText( hEditName, g_sName.c_str() );


			UpdateScrollRange();

			return TRUE;
		}


	case WM_DRAWITEM: 
		{
			LPDRAWITEMSTRUCT lpdis = (LPDRAWITEMSTRUCT) lParam; 

			if( lpdis->CtlID == IDC_BUTTONGLOW ) {
				HBRUSH	rColorBrush = CreateSolidBrush( color_to_COLOREF( g_rGlowColor ) );
				FillRect( lpdis->hDC, &lpdis->rcItem, rColorBrush );
				DeleteObject( rColorBrush );
				return TRUE;
			}
			else if( lpdis->CtlID == IDC_BUTTONSPOT ) {
				HBRUSH	rColorBrush = CreateSolidBrush( color_to_COLOREF( g_rSpotColor ) );
				FillRect( lpdis->hDC, &lpdis->rcItem, rColorBrush );
				DeleteObject( rColorBrush );
				return TRUE;
			}
			else if( lpdis->CtlID == IDC_BUTTONSPIKES ) {
				HBRUSH	rColorBrush = CreateSolidBrush( color_to_COLOREF( g_rSpikesColor ) );
				FillRect( lpdis->hDC, &lpdis->rcItem, rColorBrush );
				DeleteObject( rColorBrush );
				return TRUE;
			}
			else if( lpdis->CtlID == IDC_BUTTONBG ) {
				HBRUSH	rColorBrush = CreateSolidBrush( color_to_COLOREF( g_rBGColor ) );
				FillRect( lpdis->hDC, &lpdis->rcItem, rColorBrush );
				DeleteObject( rColorBrush );
				return TRUE;
			}
			return FALSE;
		}

	case WM_PAINT:
		{
			PAINTSTRUCT	ps;
			HDC			hDC = BeginPaint( hDlg, &ps );

			if( !g_pDIB )
				create_flare();

			int32	i32Width = g_pDIB->m_size.cx;
			int32	i32Height = g_pDIB->m_size.cy;

			i32Width *= g_i32Zoom;
			i32Height *= g_i32Zoom;
			i32Width /= 100;
			i32Height /= 100;
			
			int32	i32ViewWidth = g_rViewRect.right - g_rViewRect.left;
			int32	i32ViewHeight = g_rViewRect.bottom - g_rViewRect.top;

			int32	i32SrcWidth = g_pDIB->m_size.cx;
			int32	i32SrcHeight = g_pDIB->m_size.cy;


			if( i32Width < i32ViewWidth ) {
				RECT	rect;
				rect.left = g_rViewRect.left + i32Width;
				rect.top = g_rViewRect.top;
				rect.right = g_rViewRect.right;
				rect.bottom = g_rViewRect.top + i32Height;
				FillRect( hDC, &rect, (HBRUSH)(COLOR_APPWORKSPACE + 1) );
			}
			if( i32Height < i32ViewHeight ) {
				RECT	rect;
				rect.left = g_rViewRect.left;
				rect.top = g_rViewRect.top + i32Height;
				rect.right = g_rViewRect.left + i32Width;
				rect.bottom = g_rViewRect.bottom;
				FillRect( hDC, &rect, (HBRUSH)(COLOR_APPWORKSPACE + 1) );
			}
			if( i32Width < i32ViewWidth && i32Height < i32ViewHeight ) {
				RECT	rect;
				rect.left = g_rViewRect.left + i32Width;
				rect.top = g_rViewRect.top + i32Height;
				rect.right = g_rViewRect.right;
				rect.bottom = g_rViewRect.bottom;
				FillRect( hDC, &rect, (HBRUSH)(COLOR_APPWORKSPACE + 1) );
			}


			// save old clip region
			RECT	rOldRect;
			HRGN	hOldClipRgn;
			GetClipBox( hDC, &rOldRect );
			hOldClipRgn = CreateRectRgnIndirect( &rOldRect );

			// create new clip region
			HRGN	hRegion;
			hRegion = CreateRectRgnIndirect( &g_rViewRect );
			SelectClipRgn( hDC, hRegion );


			g_pDIB->PasteToDC( hDC, g_rViewRect.left - g_i32OffsetX, g_rViewRect.top - g_i32OffsetY, i32Width, i32Height );


			// restore old clip region
			SelectClipRgn( hDC, hOldClipRgn );
			DeleteObject( hOldClipRgn );
			DeleteObject( hRegion );


			EndPaint( hDlg, &ps );
			return TRUE;
		}

	case WM_COMMAND:
		if( LOWORD( wParam ) == IDC_COMBOSIZE ) {
			HWND	hComboSize = (HWND)lParam;
			if( HIWORD( wParam ) == CBN_SELCHANGE ) {
				int32	i32Sel = SendMessage( hComboSize, CB_GETCURSEL, 0, 0 );
				switch( i32Sel ) {
				case 0: g_ui32Width = g_ui32Height = 32; break;
				case 1: g_ui32Width = g_ui32Height = 64; break;
				case 2: g_ui32Width = g_ui32Height = 128; break;
				case 3: g_ui32Width = g_ui32Height = 256; break;
				}
				create_flare();
				InvalidateRect( hDlg, NULL, FALSE );
				UpdateScrollRange();
			}
		}
		else if( LOWORD( wParam ) == IDC_BUTTONGLOW ) {
			ColorCommonDialogC*	pDlg = (ColorCommonDialogC*)g_pImpInterface->get_common_dialog( CLASS_COLORCOMMONDIALOG );
			if( pDlg ) {
				pDlg->set_color( g_rGlowColor );
				pDlg->set_parent_wnd( hDlg );
				if( pDlg->do_modal() ) {
					g_rGlowColor = pDlg->get_color();
					create_flare();
					InvalidateRect( hDlg, NULL, FALSE );
				}
			}
		}
		else if( LOWORD( wParam ) == IDC_BUTTONSPOT ) {
			ColorCommonDialogC*	pDlg = (ColorCommonDialogC*)g_pImpInterface->get_common_dialog( CLASS_COLORCOMMONDIALOG );
			if( pDlg ) {
				pDlg->set_color( g_rSpotColor );
				pDlg->set_parent_wnd( hDlg );
				if( pDlg->do_modal() ) {
					g_rSpotColor = pDlg->get_color();
					create_flare();
					InvalidateRect( hDlg, NULL, FALSE );
				}
			}
		}
		else if( LOWORD( wParam ) == IDC_BUTTONSPIKES ) {
			ColorCommonDialogC*	pDlg = (ColorCommonDialogC*)g_pImpInterface->get_common_dialog( CLASS_COLORCOMMONDIALOG );
			if( pDlg ) {
				pDlg->set_color( g_rSpikesColor );
				pDlg->set_parent_wnd( hDlg );
				if( pDlg->do_modal() ) {
					g_rSpikesColor = pDlg->get_color();
					create_flare();
					InvalidateRect( hDlg, NULL, FALSE );
				}
			}
		}
		else if( LOWORD( wParam ) == IDC_BUTTONBG ) {
/*			CHOOSECOLOR	rChooseColor;
			ZeroMemory( &rChooseColor, sizeof( CHOOSECOLOR ) );
			rChooseColor.lStructSize = sizeof( CHOOSECOLOR );
			rChooseColor.hwndOwner = hDlg;
			rChooseColor.lpCustColors = (LPDWORD)acrCustClr;
			rChooseColor.Flags = CC_RGBINIT | CC_FULLOPEN;
			rChooseColor.rgbResult = g_rBGColor;
			ChooseColor( &rChooseColor );
			g_rBGColor = rChooseColor.rgbResult;
			create_flare();
			InvalidateRect( hDlg, NULL, FALSE );*/
			ColorCommonDialogC*	pDlg = (ColorCommonDialogC*)g_pImpInterface->get_common_dialog( CLASS_COLORCOMMONDIALOG );
			if( pDlg ) {
				pDlg->set_color( g_rBGColor );
				pDlg->set_parent_wnd( hDlg );
				if( pDlg->do_modal() ) {
					g_rBGColor = pDlg->get_color();
					create_flare();
					InvalidateRect( hDlg, NULL, FALSE );
				}
			}
		}
		else if( LOWORD( wParam ) == IDC_BUTTONZOOMIN ) {
			if( g_i32Zoom > 100 )
				g_i32Zoom += 50;
			else
				g_i32Zoom += 25;

			if( g_i32Zoom > 600 )
				g_i32Zoom = 600;

			HWND	hEditZoom;
			char	szVal[32];
			_snprintf( szVal, 31, "%d%%", g_i32Zoom );
			hEditZoom = GetDlgItem( hDlg, IDC_EDITZOOM );
			SetWindowText( hEditZoom, szVal );

			UpdateScrollRange();

			InvalidateRect( hDlg, NULL, FALSE );
		}
		else if( LOWORD( wParam ) == IDC_BUTTONZOOMOUT ) {
			if( g_i32Zoom > 100 )
				g_i32Zoom -= 50;
			else
				g_i32Zoom -= 25;

			if( g_i32Zoom < 25 )
				g_i32Zoom = 25;

			HWND	hEditZoom;
			char	szVal[32];
			_snprintf( szVal, 31, "%d%%", g_i32Zoom );
			hEditZoom = GetDlgItem( hDlg, IDC_EDITZOOM );
			SetWindowText( hEditZoom, szVal );

			UpdateScrollRange();

			InvalidateRect( hDlg, NULL, FALSE );
		}
		else if( LOWORD( wParam ) == IDOK || LOWORD( wParam ) == IDCANCEL ) {

			char	szText[256];

			HWND	hEditName;
			hEditName = GetDlgItem( hDlg, IDC_EDITNAME );
			GetWindowText( hEditName, szText, 255 );
			g_sName = szText;

			EndDialog( hDlg, LOWORD( wParam ) );
			delete g_pDIB;
			g_pDIB = 0;
			return TRUE;
		}
		break;

    case WM_LBUTTONDOWN:
		{
			if( g_i32OffsetXMax != 0 || g_i32OffsetYMax != 0 ) {
				POINT	pt;
				pt.x = (short)LOWORD( lParam );
				pt.y = (short)HIWORD( lParam );

				if( PtInRect( &g_rViewRect, pt ) ) {
					g_i32StartX = pt.x;
					g_i32StartY = pt.y;
					g_i32StartOffsetX = g_i32OffsetX;
					g_i32StartOffsetY = g_i32OffsetY;
					g_bPanorate = true;
					SetCapture( hDlg );
					SetCursor( LoadCursor( g_hInstance, MAKEINTRESOURCE( IDC_HANDDRAG ) ) );
				}
			}
		}
		break;
		
    case WM_LBUTTONUP:
		{
			if( GetCapture() == hDlg ) {
				ReleaseCapture();
				InvalidateRect( hDlg, &g_rViewRect, FALSE );
				g_bPanorate = false;
			}
		}
		break;
		
    case WM_MOUSEMOVE:
		{
			if( GetCapture() == hDlg && g_bPanorate ) {
				int32	i32X = (short)LOWORD( lParam );
				int32	i32Y = (short)HIWORD( lParam );

				g_i32OffsetX = (g_i32StartX - i32X) + g_i32StartOffsetX;
				g_i32OffsetY = (g_i32StartY - i32Y) + g_i32StartOffsetY;

				if( g_i32OffsetX < 0 ) g_i32OffsetX = 0;
				if( g_i32OffsetX > g_i32OffsetXMax ) g_i32OffsetX = g_i32OffsetXMax;
				if( g_i32OffsetY < 0 ) g_i32OffsetY = 0;
				if( g_i32OffsetY > g_i32OffsetYMax ) g_i32OffsetY = g_i32OffsetYMax;

				InvalidateRect( hDlg, &g_rViewRect, FALSE );
			}
		}
		break;

	case WM_SETCURSOR:
		{
			if( g_i32OffsetXMax != 0 || g_i32OffsetYMax != 0 ) {
				POINT	pt;
				GetCursorPos( &pt );
				ScreenToClient( hDlg, &pt );

				if( PtInRect( &g_rViewRect, pt ) ) {
					SetCursor( LoadCursor( g_hInstance, MAKEINTRESOURCE( IDC_HAND ) ) );
					return TRUE;
				}
			}
		}
		break;
		

	case WM_DESTROY:
		break;
	
	}

    return FALSE;
}

bool
prompt_dialog( ImportInterfaceC* pInterface, uint32& ui32Width, uint32& ui32Height, ColorC& rBGColor, ColorC& rGlowColor, ColorC& rSpotColor, ColorC& rSpikesColor, std::string& sName )
{
	g_pImpInterface = pInterface;

//	g_rGlowColor = color_to_COLOREF( rGlowColor );
//	g_rSpotColor = color_to_COLOREF( rSpotColor );
//	g_rSpikesColor = color_to_COLOREF( rSpikesColor );
//	g_rBGColor = color_to_COLOREF( rBGColor );

	g_rGlowColor = rGlowColor;
	g_rSpotColor = rSpotColor;
	g_rSpikesColor = rSpikesColor;
	g_rBGColor = rBGColor;

	g_ui32Width = ui32Width;
	g_ui32Height = ui32Height;

	g_sName = sName;

	bool	bRes = DialogBox( g_hInstance, (LPCTSTR)IDD_EDITDIALOG, NULL, (DLGPROC)FlareDlgProc ) == IDOK ? true : false;

//	rGlowColor = COLOREF_to_color( g_rGlowColor );
//	rSpotColor = COLOREF_to_color( g_rSpotColor );
//	rSpikesColor = COLOREF_to_color( g_rSpikesColor );
//	rBGColor = COLOREF_to_color( g_rBGColor );

	rGlowColor = g_rGlowColor;
	rSpotColor = g_rSpotColor;
	rSpikesColor = g_rSpikesColor;
	rBGColor = g_rBGColor;

	ui32Width = g_ui32Width;
	ui32Height = g_ui32Height;

	sName = g_sName;

	return bRes;
}
