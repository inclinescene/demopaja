#ifndef __FLAREIMAGEDIALOG_H__
#define __FLAREIMAGEDIALOG_H__

#include "PajaTypes.h"
#include "ColorC.h"
#include "ImportInterfaceC.h"
#include <string>

bool	prompt_dialog( Import::ImportInterfaceC* pInterface, PajaTypes::uint32& ui32Width, PajaTypes::uint32& ui32Height, PajaTypes::ColorC& rBGColor, PajaTypes::ColorC& rGlowColor, PajaTypes::ColorC& rSpotColor, PajaTypes::ColorC& rSpikesColor, std::string& sName );

void	render_flare( PajaTypes::uint8* pBuffer, PajaTypes::uint32 ui32Width, PajaTypes::uint32 ui32Height,
					  const PajaTypes::ColorC& rBGColor, const PajaTypes::ColorC& rGlowColor, const PajaTypes::ColorC& rSpotColor, const PajaTypes::ColorC& rSpikesColor );

#endif	// __FLAREIMAGEDIALOG_H__