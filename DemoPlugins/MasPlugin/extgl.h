#ifndef __EXTGL_H__
#define __EXTGL_H__

// Windows headers
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include "glext.h"

// Separate specular
#ifndef GL_LIGHT_MODEL_COLOR_CONTROL
#define GL_LIGHT_MODEL_COLOR_CONTROL		((GLenum)0x81F8)
#define GL_SINGLE_COLOR						((GLenum)0x81F9)
#define GL_SEPARATE_SPECULAR_COLOR			((GLenum)0x81FA)
#endif

// Multitexture
extern PFNGLMULTITEXCOORD1FARBPROC		glMultiTexCoord1fARB;
extern PFNGLMULTITEXCOORD2FARBPROC		glMultiTexCoord2fARB;
extern PFNGLMULTITEXCOORD2FVARBPROC		glMultiTexCoord2fvARB;
extern PFNGLMULTITEXCOORD3FARBPROC		glMultiTexCoord3fARB;
extern PFNGLMULTITEXCOORD4FARBPROC		glMultiTexCoord4fARB;
extern PFNGLACTIVETEXTUREARBPROC		glActiveTextureARB;
extern PFNGLCLIENTACTIVETEXTUREARBPROC	glClientActiveTextureARB;

// Vertex array
extern PFNGLLOCKARRAYSEXTPROC			glLockArraysEXT;
extern PFNGLUNLOCKARRAYSEXTPROC			glUnlockArraysEXT;

extern bool	g_bCompiledVertexArray;
extern bool	g_bMultiTexture;
extern bool	g_bSeparateSpec;

extern void	init_gl_extensions();

#endif