#ifndef CAMERAC_H
#define CAMERAC_H

#include "PajaTypes.h"
#include "Vector3C.h"
#include "ControllerC.h"
#include "FileIO.h"
#include "ContFloatC.h"
#include "ContVector3C.h"
#include "ScenegraphItemI.h"


const PajaTypes::int32	CGITEM_CAMERA = 0x2000;


class CameraC : public ScenegraphItemI
{
public:
	CameraC();
	virtual ~CameraC();

	virtual PajaTypes::int32			get_type();

	virtual void						set_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_position();

	virtual void						set_target_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_target_position();

	virtual void						set_roll( const PajaTypes::float32 f32Val );
	virtual const PajaTypes::float32	get_roll();

	virtual void						set_fov( const PajaTypes::float32 f32Val );
	virtual const PajaTypes::float32	get_fov();

	virtual void						set_near_plane( const PajaTypes::float32 f32Val );
	virtual const PajaTypes::float32	get_near_plane();

	virtual void						set_far_plane( const PajaTypes::float32 f32Val );
	virtual const PajaTypes::float32	get_far_plane();

	virtual void						eval_state( PajaTypes::int32 i32Time );
	virtual void						eval_geom( PajaTypes::int32 i32Time );

	virtual void						set_position_controller( ContVector3C* pCont );
	virtual void						set_target_position_controller( ContVector3C* pCont );
	virtual void						set_roll_controller( ContFloatC* pCont );
	virtual void						set_fov_controller( ContFloatC* pCont );

	virtual void						update_dependencies( std::vector<ScenegraphItemI*> rScenegraph );

	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

private:
	PajaTypes::Vector3C	m_rPos;
	PajaTypes::Vector3C	m_rTgt;
	PajaTypes::float32	m_f32Roll;
	PajaTypes::float32	m_f32FOV;
	PajaTypes::float32	m_f32NearPlane;
	PajaTypes::float32	m_f32FarPlane;

	ContVector3C*		m_pPosCont;
	ContVector3C*		m_pTgtCont;
	ContFloatC*			m_pRollCont;
	ContFloatC*			m_pFovCont;
};


#endif // CAMERAC_H