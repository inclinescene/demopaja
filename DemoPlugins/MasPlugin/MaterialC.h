#ifndef __MATERIALC_H__
#define __MATERIALC_H__

enum MappingTypeE {
	MAPPING_NONE = 0,
	MAPPING_EXPLICIT,
	MAPPING_SPHERICAL,
};

enum MaterialTransE {
	TRANSPARENCY_FILTER,
	TRANSPARENCY_ADD,
	TRANSPARENCY_SUB,
};

#endif	// __MATERIALC_H__