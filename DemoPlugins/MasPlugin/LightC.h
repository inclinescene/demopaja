#ifndef LIGHTC_H
#define LIGHTC_H

#include "PajaTypes.h"
#include "Vector3C.h"
#include "ControllerC.h"
#include "ContFloatC.h"
#include "ContVector3C.h"
#include "FileIO.h"
#include "ScenegraphItemI.h"


const PajaTypes::int32	CGITEM_LIGHT = 0x3000;

enum LightTypeE {
	LIGHT_OMNI,
	LIGHT_SPOT,
};

enum DecayTypeE {
	DECAY_NONE,
	DECAY_INVERSE,
	DECAY_INVERSE_SQR,
};

class LightC : public ScenegraphItemI
{
public:
	LightC();
	virtual ~LightC();

	virtual PajaTypes::int32			get_type();

	virtual void						set_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_position();

	virtual void						set_target( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_target();

	virtual void						set_color( const PajaTypes::ColorC& rVal );
	virtual const PajaTypes::ColorC&	get_color();

	virtual void						set_multiplier( PajaTypes::float32 f32Val );
	virtual PajaTypes::float32			get_multiplier();

	virtual void						set_hotspot( PajaTypes::float32 f32Val );
	virtual PajaTypes::float32			get_hotspot();

	virtual void						set_falloff( PajaTypes::float32 f32Val );
	virtual PajaTypes::float32			get_falloff();

	virtual void						set_decay( PajaTypes::float32 f32Val );
	virtual PajaTypes::float32			get_decay();

	virtual void						set_decay_type( PajaTypes::uint32 ui32Type );
	virtual PajaTypes::uint32			get_decay_type();

	virtual void						set_light_type( PajaTypes::uint32 ui32Type );
	virtual PajaTypes::uint32			get_light_type();

	virtual void						set_cast_shadows( bool bState );
	virtual bool						get_cast_shadows() const;

	virtual void						set_projection_texture( Import::FileHandleC* pHandle );
	virtual Import::FileHandleC*		get_projection_texture() const;

	virtual void						eval_state( PajaTypes::int32 i32Time );
	virtual void						eval_geom( PajaTypes::int32 i32Time );

	virtual void						set_position_controller( ContVector3C* pCont );
	virtual void						set_target_controller( ContVector3C* pCont );
	virtual void						set_color_controller( ContVector3C* pCont );
	virtual void						set_multiplier_controller( ContFloatC* pCont );
	virtual void						set_hotspot_controller( ContFloatC* pCont );
	virtual void						set_falloff_controller( ContFloatC* pCont );
	virtual void						set_decay_controller( ContFloatC* pCont );

	virtual void						update_dependencies( std::vector<ScenegraphItemI*> rScenegraph );

	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

private:
	PajaTypes::uint32		m_ui32LightType;
	PajaTypes::uint32		m_ui32DecayType;
	PajaTypes::Vector3C		m_rPos;
	PajaTypes::Vector3C		m_rTgt;
	PajaTypes::ColorC		m_rColor;
	PajaTypes::float32		m_f32Multiplier;
	PajaTypes::float32		m_f32FallOff;
	PajaTypes::float32		m_f32Hotspot;
	PajaTypes::float32		m_f32Decay;
	bool					m_bCastShadows;
	Import::FileHandleC*	m_pProjectionTex;

	ContVector3C*		m_pPosCont;
	ContVector3C*		m_pTgtCont;
	ContVector3C*		m_pColorCont;
	ContFloatC*			m_pMultiplierCont;
	ContFloatC*			m_pFalloffCont;
	ContFloatC*			m_pHotspotCont;
	ContFloatC*			m_pDecayCont;
};


#endif // LIGHT_H