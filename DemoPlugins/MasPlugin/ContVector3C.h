#ifndef __CONTVECTOR3C_H__
#define __CONTVECTOR3C_H__


#include "PajaTypes.h"
#include "KeyC.h"
#include "Vector3C.h"
#include "FileIO.h"
#include "ControllerC.h"

class KeyVector3C
{
public:
	KeyVector3C();
	virtual ~KeyVector3C();

	virtual const PajaTypes::Vector3C&		get_value() const;
	virtual void				set_value( const PajaTypes::Vector3C& rVec );

	virtual const PajaTypes::Vector3C&		get_in_tan() const;
	virtual void				set_in_tan( const PajaTypes::Vector3C& rVec );

	virtual const PajaTypes::Vector3C&		get_out_tan() const;
	virtual void				set_out_tan( const PajaTypes::Vector3C& rVec );

	virtual PajaTypes::int32	get_time();
	virtual void				set_time( PajaTypes::int32 i32Time );

	// TCB and ease
	virtual PajaTypes::float32	get_ease_in();
	virtual PajaTypes::float32	get_ease_out();
	virtual PajaTypes::float32	get_tens();
	virtual PajaTypes::float32	get_cont();
	virtual PajaTypes::float32	get_bias();
	virtual void				set_ease_in( PajaTypes::float32 f32Val );
	virtual void				set_ease_out( PajaTypes::float32 f32Val );
	virtual void				set_tens( PajaTypes::float32 f32Val );
	virtual void				set_cont( PajaTypes::float32 f32Val );
	virtual void				set_bias( PajaTypes::float32 f32Val );

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

private:
	PajaTypes::Vector3C	m_rVal;
	PajaTypes::Vector3C	m_rInTan;
	PajaTypes::Vector3C	m_rOutTan;
	PajaTypes::float32	m_f32EaseIn, m_f32EaseOut;
	PajaTypes::float32	m_f32Tens, m_f32Cont, m_f32Bias;
	PajaTypes::int32	m_i32Time;
};


class ContVector3C
{
public:
	ContVector3C( PajaTypes::uint32 ui32Type = Composition::KEY_SMOOTH );
	virtual ~ContVector3C();

	virtual PajaTypes::int32	get_key_count();
	virtual KeyVector3C*		get_key( PajaTypes::int32 i32Index );
	virtual void				del_key( PajaTypes::int32 i32Index );
	virtual KeyVector3C*		add_key();

	virtual void				set_start_ort( PajaTypes::uint32 ui32Ort );
	virtual void				set_end_ort( PajaTypes::uint32 ui32Ort );
	virtual PajaTypes::uint32	get_start_ort() const;
	virtual PajaTypes::uint32	get_end_ort() const;

	virtual PajaTypes::int32	get_min_time() const;
	virtual PajaTypes::int32	get_max_time() const;

	virtual void				sort_keys();
	virtual void				prepare();

	virtual PajaTypes::Vector3C	get_value( PajaTypes::int32 i32Time ) const;

	virtual void				set_type( PajaTypes::uint32 ui32Type );
	virtual PajaTypes::uint32	get_type() const;

	virtual ContVector3C*		duplicate();

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

protected:
	// tool methods
	void						compute_hermite_basis( PajaTypes::float32 f32U, PajaTypes::float32* pV ) const;
	PajaTypes::float32			ease( PajaTypes::float32 f32U, PajaTypes::float32 f32A, PajaTypes::float32 f32B ) const;
	void						comp_first_deriv( PajaTypes::uint32 ui32CurIndex, PajaTypes::uint32 ui32NextIndex );
	void						comp_last_deriv( PajaTypes::uint32 ui32CurIndex, PajaTypes::uint32 ui32NextIndex );
	void						comp_2key_deriv( PajaTypes::uint32 ui32CurIndex, PajaTypes::uint32 ui32NextIndex );
	void						comp_middle_deriv( PajaTypes::uint32 ui32PrevIndex, PajaTypes::uint32 ui32Index, PajaTypes::uint32 ui32NextIndex );

private:

	std::vector<KeyVector3C*>	m_rKeys;
	PajaTypes::uint32			m_ui32StartOrt;
	PajaTypes::uint32			m_ui32EndOrt;
	PajaTypes::uint32			m_ui32Type;			// interpolation type
};

#endif