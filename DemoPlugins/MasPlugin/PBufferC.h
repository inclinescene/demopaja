#ifndef __PBUFFERC_H__
#define __PBUFFERC_H__

#include <gl\gl.h>
#include "wglext.h"
#include "PajaTypes.h"

namespace MASPlugin
{

#define	PBUFFER_TEXTURE_RGBA	0x01
#define	PBUFFER_TEXTURE_DEPTH	0x02

class PBufferC
{
public:
	PBufferC();
	virtual ~PBufferC();
	bool				init( PajaTypes::uint32 ui32Width, PajaTypes::uint32 ui32Height, PajaTypes::uint32 ui32Mode );
	void				destroy();
	void				bind( PajaTypes::uint32 ui32Mode );
	void				copy_texture();

	PajaTypes::uint32	get_width() const;
	PajaTypes::uint32	get_height() const;

	bool				begin_draw();
	void				end_draw();

	bool				is_initialised() const;

protected:
	PajaTypes::uint32	m_ui32Width, m_ui32Height;
	PajaTypes::uint32	m_ui32Mode;
	HPBUFFERARB			m_hPBuffer;
	HDC					m_hPBufferDC;
	HGLRC				m_hPBufferRC;
	PajaTypes::uint32	m_ui32PBufferTexRGBAID;
	PajaTypes::uint32	m_ui32PBufferTexDepthID;

	PajaTypes::int32	m_i32OldViewport[4];
	HDC					m_hPrevDC;
	HGLRC				m_hPrevRC;
	bool				m_bBindToRenderTextureRGBA;
	bool				m_bBindToRenderTextureDepth;
};

};

#endif