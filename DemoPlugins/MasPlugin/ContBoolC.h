#ifndef __CONTBOOLC_H__
#define __CONTBOOLC_H__


#include "PajaTypes.h"
#include "UndoC.h"
#include "KeyC.h"
#include "FileIO.h"
#include "ControllerC.h"


class KeyBoolC
{
public:
	KeyBoolC();
	virtual ~KeyBoolC();

	virtual PajaTypes::int32	get_time();
	virtual void				set_time( PajaTypes::int32 i32Time );

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

private:
	PajaTypes::int32	m_i32Time;
};


class ContBoolC
{
public:
	ContBoolC();
	virtual ~ContBoolC();

	virtual PajaTypes::int32	get_key_count();
	virtual KeyBoolC*			get_key( PajaTypes::int32 i32Index );
	virtual void				del_key( PajaTypes::int32 i32Index );
	virtual KeyBoolC*			add_key();

	virtual PajaTypes::int32	get_min_time() const;
	virtual PajaTypes::int32	get_max_time() const;

	virtual void				sort_keys();

	virtual void				set_start_val( bool bVal );
	virtual bool				get_start_val() const;

	virtual bool				get_value( PajaTypes::int32 i32Time ) const;

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

private:
	std::vector<KeyBoolC*>		m_rKeys;
	bool						m_bStartVal;
};

#endif