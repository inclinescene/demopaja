#ifndef __BONEC_H__
#define __BONEC_H__

#include "PajaTypes.h"
#include "ColorC.h"
#include "ImportableI.h"
#include "FileIO.h"
#include "ScenegraphItemI.h"
#include "QuatC.h"
#include "Vector2C.h"
#include "Vector3C.h"
#include "ContVector3C.h"
#include "ContQuatC.h"
#include "ControllerC.h"


const PajaTypes::int32	CGITEM_BONE = 0x1100;


class BoneC : public ScenegraphItemI
{
public:
	BoneC();
	virtual ~BoneC();

	virtual PajaTypes::int32			get_type();

	// transformation

	virtual void						eval_state( PajaTypes::int32 i32Time );
	virtual void						eval_geom( PajaTypes::int32 i32Time );

	virtual void						set_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_position();

	virtual void						set_rotation( const PajaTypes::QuatC& rRot );
	virtual const PajaTypes::QuatC&		get_rotation();

	virtual void						set_position_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_position_controller();

	virtual void						set_rotation_controller( ContQuatC* pCont );
	virtual ContQuatC*					get_rotation_controller();

	virtual void						set_bone_id( PajaTypes::uint32 ui32Id );
	virtual PajaTypes::uint32			get_bone_id() const;

	virtual void						update_dependencies( std::vector<ScenegraphItemI*> rScenegraph );

	// serialize
	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

private:

	PajaTypes::Vector3C			m_rPosition;
	PajaTypes::QuatC			m_rRotation;

	ContVector3C*				m_pPosCont;
	ContQuatC*					m_pRotCont;

	PajaTypes::uint32			m_ui32BoneId;
};


#endif // MESHREFC_H