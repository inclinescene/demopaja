#ifndef __SHAPE_C__
#define __SHAPE_C__

#include "PajaTypes.h"
#include "ColorC.h"
#include "ImportableI.h"
#include "FileIO.h"
#include "ScenegraphItemI.h"
#include "QuatC.h"
#include "Vector2C.h"
#include "Vector3C.h"
#include "ContVector3C.h"
#include "ContQuatC.h"
#include "ContBoolC.h"
#include "ControllerC.h"
#include "BoneC.h"
#include "MaterialC.h"

const PajaTypes::int32	CGITEM_SHAPE = 0x1200;


class KnotC {
public:
	KnotC();
	virtual ~KnotC();

	virtual void	set_knot( const PajaTypes::Vector3C& rKnot );
	virtual void	set_in_vector( const PajaTypes::Vector3C& rVec );
	virtual void	set_out_vector( const PajaTypes::Vector3C& rVec );

	const PajaTypes::Vector3C&	get_knot() const;
	const PajaTypes::Vector3C&	get_in_vector() const;
	const PajaTypes::Vector3C&	get_out_vector() const;

private:
	PajaTypes::Vector3C		m_rKnot;
	PajaTypes::Vector3C		m_rInVector;
	PajaTypes::Vector3C		m_rOutVector;
};



class PathC {
public:
	PathC();
	virtual ~PathC();

	virtual void				add_knot( const KnotC& rKnot );
	virtual PajaTypes::uint32	get_knot_count() const;
	virtual const KnotC&		get_knot( PajaTypes::uint32 ui32Index ) const;

	virtual void				calc_outline();

	virtual PajaTypes::uint32	get_vertex_count() const;
	virtual PajaTypes::float32*	get_vertex_pointer();

	virtual void				calc_bbox();
	virtual PajaTypes::Vector3C	get_bbox_min() const;
	virtual PajaTypes::Vector3C	get_bbox_max() const;

	virtual void				set_closed( bool bState );
	virtual bool				get_closed() const;

	// serialize
	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

private:

	std::vector<KnotC>				m_rKnots;
	std::vector<PajaTypes::float32>	m_rVertices;
	PajaTypes::Vector3C				m_rMin;
	PajaTypes::Vector3C				m_rMax;
	bool							m_bClosed;
};


class ShapeC : public ScenegraphItemI
{
public:
	ShapeC();
	virtual ~ShapeC();

	virtual PajaTypes::int32			get_type();

	// transformation

	virtual void						eval_state( PajaTypes::int32 i32Time );
	virtual void						eval_geom( PajaTypes::int32 i32Time );

	virtual void						set_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_position();

	virtual void						set_scale( const PajaTypes::Vector3C& rScale );
	virtual const PajaTypes::Vector3C&	get_scale();

	virtual void						set_scale_rotation( const PajaTypes::QuatC& rRot );
	virtual const PajaTypes::QuatC&		get_scale_rotation();

	virtual void						set_rotation( const PajaTypes::QuatC& rRot );
	virtual const PajaTypes::QuatC&		get_rotation();

	virtual void						set_position_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_position_controller();

	virtual void						set_rotation_controller( ContQuatC* pCont );
	virtual ContQuatC*					get_rotation_controller();

	virtual void						set_scale_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_scale_controller();

	virtual void						set_scale_rotation_controller( ContQuatC* pCont );
	virtual ContQuatC*					get_scale_rotation_controller();

	virtual void						set_vis_controller( ContBoolC* pCont );
	virtual ContBoolC*					get_vis_controller();

	virtual bool						get_visible() const;

	// geometry

	virtual void						add_path( PathC* pList );
	virtual PajaTypes::uint32			get_path_count() const;
	virtual PathC*						get_path( PajaTypes::uint32 ui32Index );

	virtual void						calc_outline();

	virtual void						calc_bbox();
	virtual PajaTypes::Vector3C			get_bbox_min() const;
	virtual PajaTypes::Vector3C			get_bbox_max() const;


	// material
	virtual void						set_ambient( const PajaTypes::ColorC &rAmbient );
	virtual void						set_diffuse( const PajaTypes::ColorC &rDiffuse );
	virtual void						set_specular( const PajaTypes::ColorC &rSpecular );
	virtual void						set_shininess( const PajaTypes::float32 f32Shininess );
	virtual void						set_transparency( const PajaTypes::float32 f32Trans );
	virtual void						set_selfillum( const PajaTypes::float32 f32SelfIllum );

	virtual const PajaTypes::ColorC&	get_ambient() const;
	virtual const PajaTypes::ColorC&	get_diffuse() const;
	virtual const PajaTypes::ColorC&	get_specular() const;
	virtual PajaTypes::float32			get_shininess() const;
	virtual PajaTypes::float32			get_transparency() const;
	virtual PajaTypes::float32			get_selfillum() const;

	virtual void						set_additive( bool bState );
	virtual bool						get_additive() const;

	virtual bool						is_transparent() const;

	virtual void						update_dependencies( std::vector<ScenegraphItemI*> rScenegraph );

	// serialize
	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

private:

	bool								m_bVis;

	PajaTypes::Vector3C					m_rPosition;
	PajaTypes::Vector3C					m_rScale;
	PajaTypes::QuatC					m_rScaleRot;
	PajaTypes::QuatC					m_rRotation;

	ContVector3C*						m_pPosCont;
	ContQuatC*							m_pRotCont;
	ContVector3C*						m_pScaleCont;
	ContQuatC*							m_pScaleRotCont;
	ContBoolC*							m_pVisCont;

	PajaTypes::Vector3C					m_rMin;
	PajaTypes::Vector3C					m_rMax;

	//Geometry
	std::vector<PathC*>					m_rPaths;

	// Material
	PajaTypes::ColorC					m_rAmbient;
	PajaTypes::ColorC					m_rDiffuse;
	PajaTypes::ColorC					m_rSpecular;
	PajaTypes::float32					m_f32Shininess;
	PajaTypes::float32					m_f32Transparency;
	PajaTypes::float32					m_f32SelfIllum;
	bool								m_bTransparent;
	bool								m_bAdditive;

};


#endif // __SHAPE_C__	