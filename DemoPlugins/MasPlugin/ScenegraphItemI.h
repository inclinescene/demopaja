#ifndef SCENEGRAPHITEMI_H
#define SCENEGRAPHITEMI_H


#include "PajaTypes.h"
#include "Vector3C.h"
#include "Matrix3C.h"
#include "FileIO.h"
#include <vector>
#include <string>


class ScenegraphItemI
{
public:
	ScenegraphItemI();
	virtual ~ScenegraphItemI();

	virtual PajaTypes::int32			get_type() = 0;
	virtual void						eval_state( PajaTypes::int32 i32Time ) = 0;
	virtual void						eval_geom( PajaTypes::int32 i32Time ) = 0;

	virtual void						set_name( const char* szName );
	virtual const char*					get_name();

	virtual void						set_parent_name( const char* szName );
	virtual const char*					get_parent_name();

	virtual ScenegraphItemI*			get_parent() const;
	virtual void						set_parent( ScenegraphItemI* pParent );

	virtual void						set_tm( const PajaTypes::Matrix3C& rMat );
	virtual const PajaTypes::Matrix3C&	get_tm();

	virtual void						set_base_tm( const PajaTypes::Matrix3C& rTM );
	virtual const PajaTypes::Matrix3C&	get_base_tm() const;

	virtual void						set_hidden( bool bState );
	virtual bool						get_hidden() const;

	virtual void						update_dependencies( std::vector<ScenegraphItemI*> rScenegraph ) = 0;

	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

protected:
	ScenegraphItemI*		m_pParent;
	std::string				m_sName;
	std::string				m_sParentName;
	PajaTypes::uint32		m_ui32ID;
	PajaTypes::Matrix3C		m_rTM;
	PajaTypes::Matrix3C		m_rBaseTM;
	bool					m_bHidden;
};


#endif // SCENEGRAPHITEMI_H