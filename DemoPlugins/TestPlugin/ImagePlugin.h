//
// ImagePlugin.h
//
// Image Plugin
//
// Copyright (c) 2000-2003 memon/moppi productions
//


#ifndef __IMAGEPLUGIN_H__
#define __IMAGEPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLViewportC.h"
#include "OpenGLDeviceC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "ImportableImageI.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const	PluginClass::ClassIdC	CLASS_IMAGE_EFFECT( 0, 100 );
const	PluginClass::ClassIdC	CLASS_TGA_IMPORT( 0, 200 );


//////////////////////////////////////////////////////////////////////////
//
//  TGA importer class descriptor.
//

class TGAImportDescC : public PluginClass::ClassDescC
{
public:
	TGAImportDescC();
	virtual ~TGAImportDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  Simple image effect class descriptor.
//

class ImageDescC : public PluginClass::ClassDescC
{
public:
	ImageDescC();
	virtual ~ImageDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


namespace ImagePlugin {

//////////////////////////////////////////////////////////////////////////
//
// TGA Importer class.
//

	// .TGA file header
	#pragma pack(1)     // Gotta pack these structures!
	struct TGAHeaderS {
		unsigned char	idlen;
		unsigned char	cmtype;
		unsigned char	imgtype;

		unsigned short	cmorg;
		unsigned short	cmlen;
		unsigned char	cmes;

		short			xorg;
		short			yorg;
		short			width;
		short			height;
		unsigned char	pixsize;
		unsigned char	desc;
	};
	#pragma pack()


	class TGAImportC : public Import::ImportableImageI
	{
	public:
		static TGAImportC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual const char*				get_filename();
		virtual void					set_filename( const char* szName );
		virtual bool					load_file( const char* szName, PajaSystem::DemoInterfaceC* pInterface );
		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		// The importable image interface.
		virtual PajaTypes::float32		get_width();
		virtual PajaTypes::float32		get_height();
		virtual PajaTypes::int32		get_data_width();
		virtual PajaTypes::int32		get_data_height();
		virtual PajaTypes::int32		get_data_pitch();
		virtual PajaTypes::BBox2C&	get_tex_coord_bounds();
		virtual PajaTypes::int32		get_data_bpp();
		virtual PajaTypes::uint8*		get_data();

		virtual void					bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Stage, PajaTypes::uint32 ui32Properties );
		virtual const char*				get_info();
		virtual PluginClass::ClassIdC	get_default_effect();

		virtual PajaTypes::int32		get_duration();
		virtual PajaTypes::float32		get_start_label();
		virtual PajaTypes::float32		get_end_label();

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

	protected:
		TGAImportC();
		TGAImportC( Edit::EditableI* pOriginal );
		virtual ~TGAImportC();
		bool						load_8bit_pal_TGA( FILE* fp, TGAHeaderS& pHdr );
		bool						load_8bit_gray_TGA( FILE* fp, TGAHeaderS& pHdr );
		bool						load_16bit_TGA( FILE* fp, TGAHeaderS& pHdr );
		bool						load_24bit_TGA( FILE* fp, TGAHeaderS& pHdr );
		bool						load_32bit_TGA( FILE* fp, TGAHeaderS& pHdr );

		void						upload_texture();

	private:
		PajaTypes::uint32		m_ui32TextureId;
		PajaTypes::uint8*		m_pData;
		PajaTypes::int32		m_i32Width, m_i32Height;
		PajaTypes::int32		m_i32Bpp;
		PajaTypes::BBox2C		m_rTexBounds;
		std::string					m_sFileName;
	};


//////////////////////////////////////////////////////////////////////////
//
// The Image effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_ROT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_IMAGE = 0,
		ID_ATTRIBUTE_SCALE,
		ID_ATTRIBUTE_OFFSET,
		ID_ATTRIBUTE_WRAP,
		ID_ATTRIBUTE_RESAMPLE,
		ID_ATTRIBUTE_COLOR,
		ID_ATTRIBUTE_OPACITY,
		ID_ATTRIBUTE_BLEND,
		ID_ATTRIBUTE_MASK,
		ATTRIBUTE_COUNT,
	};

	enum ImageEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	enum ResampleModeE {
		RESAMPLEMODE_BILINEAR = 0,
		RESAMPLEMODE_NEAREST = 1,
	};

	enum WrapModeE {
		WRAPMODE_CLAMP = 0,
		WRAPMODE_REPEAT = 1,
	};

	enum BlendE {
		BLENDMODE_NORMAL = 0,
		BLENDMODE_REPLACE,
		BLENDMODE_ADD,
		BLENDMODE_MULT,
		BLENDMODE_SUB,
		BLENDMODE_LIGHTEN,
		BLENDMODE_DARKEN,
	};

	class ImageEffectC : public Composition::EffectI
	{
	public:
		static ImageEffectC*		create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual PajaTypes::uint32		update_notify( EditableI* pCaller );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		ImageEffectC();
		ImageEffectC( Edit::EditableI* pOriginal );
		virtual ~ImageEffectC();

	private:

		void			init_textures();
		void			release_textures();

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];

		static PajaTypes::uint32	m_ui32WhiteTextureID;
		static PajaTypes::uint32	m_ui32BlackTextureID;
		static PajaTypes::int32		m_i32TextureRefCount;
		bool											m_bTextureRef;
	};

};	// namespace


extern TGAImportDescC	g_rTGAImportDesc;
extern ImageDescC	g_rImageDesc;


#endif	// __IMAGEPLUGIN_H__



/*
#ifndef __TESTPLUGIN_H__
#define __TESTPLUGIN_H__

#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "ClassDescC.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "LayerC.h"
#include "ParamI.h"
#include "ImportableI.h"
#include "ImportableImageI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "TimeContextC.h"

#include "AutoGizmoC.h"

const	PluginClass::ClassIdC	CLASS_IMAGE_EFFECT( 0, 100 );
const	PluginClass::ClassIdC	CLASS_TGA_IMPORT( 0, 200 );

namespace TestPlugin {

	// .TGA file header

	#pragma pack(1)     // Gotta pack these structures!

	struct TGAHeaderS {
		unsigned char	idlen;
		unsigned char	cmtype;
		unsigned char	imgtype;

		unsigned short	cmorg;
		unsigned short	cmlen;
		unsigned char	cmes;

		short			xorg;
		short			yorg;
		short			width;
		short			height;
		unsigned char	pixsize;
		unsigned char	desc;
	};

	#pragma pack()

	enum TGAPropertiesE {
		TGA_RGB = 1,
		TGA_ALPHA = 2,
		TGA_GREY = 4,
		TGA_WRAP = 8,
		TGA_CLAMP = 16,
		TGA_LINEAR = 32,
		TGA_NEAREST = 64,
	};


	enum TGAImportFlagsE {
		TGA_EMBED_DATA = 1,
	};

	class TGAImportC : public Import::ImportableImageI
	{
	public:
		static TGAImportC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		// the interface
		// returns the name of the file this importable refers to
		virtual const char*				get_filename();
		virtual void					set_filename( const char* szName );
		// loads the file
		virtual bool					load_file( const char* szName, PajaSystem::DemoInterfaceC* pInterface );
		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		// interface for this class
		virtual PajaTypes::float32		get_width();
		virtual PajaTypes::float32		get_height();
		virtual PajaTypes::int32		get_data_width();
		virtual PajaTypes::int32		get_data_height();
		virtual PajaTypes::int32		get_data_pitch();
		virtual PajaTypes::int32		get_data_bpp();
		virtual PajaTypes::uint8*		get_data();
		virtual void					bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Properties );
		virtual const char*				get_info();
		virtual PluginClass::ClassIdC	get_default_effect();

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::int32		get_duration();
		virtual PajaTypes::float32		get_start_label();
		virtual PajaTypes::float32		get_end_label();

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		TGAImportC();
		TGAImportC( Edit::EditableI* pOriginal );
		virtual ~TGAImportC();
		bool						load_8bit_pal_TGA( FILE* fp, TGAHeaderS& pHdr );
		bool						load_8bit_gray_TGA( FILE* fp, TGAHeaderS& pHdr );
		bool						load_16bit_TGA( FILE* fp, TGAHeaderS& pHdr );
		bool						load_24bit_TGA( FILE* fp, TGAHeaderS& pHdr );
		bool						load_32bit_TGA( FILE* fp, TGAHeaderS& pHdr );

		void						upload_texture();

	private:
		// RGB, RGBA, GREY, ALPHA
		PajaTypes::uint32		m_ui32TextureIds[4];	// one for each possible data type
		PajaTypes::uint8*		m_pData;
		PajaTypes::int32		m_i32Width, m_i32Height;
		PajaTypes::int32		m_i32Bpp;

		std::string				m_sFileName;
	};


	enum TestGizmoParamsE {
		TEST_PARAM_POS = 0,
		TEST_PARAM_PIVOT,
		TEST_PARAM_ROT,
		TEST_PARAM_SCALE,
		TEST_PARAM_COLOR,
		TEST_PARAM_RENDERMODE,
		TEST_PARAM_FILTERMODE,
		TEST_PARAM_SHAPE,
		TEST_PARAM_FILE,
		TEST_PARAM_COUNT,
	};


	class TestGizmoC : public Composition::GizmoI
	{
	public:

		static TestGizmoC*			create_new( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
		virtual Edit::DataBlockI*	create();
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		virtual void				copy( Edit::EditableI* pEditable );
		virtual void				restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_parameter_count();
		virtual Composition::ParamI*	get_parameter( PajaTypes::int32 i32Index );

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

		void						init();
		Import::ImportableImageI*	get_file( PajaTypes::int32 i32Time, PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext );
		PajaTypes::Vector2C			get_pos( PajaTypes::int32 i32Time );
		PajaTypes::Vector2C			get_pivot( PajaTypes::int32 i32Time );
		PajaTypes::float32			get_rot( PajaTypes::int32 i32Time );
		PajaTypes::Vector2C			get_scale( PajaTypes::int32 i32Time );
		PajaTypes::ColorC			get_color( PajaTypes::int32 i32Time );
		PajaTypes::int32			get_render_mode( PajaTypes::int32 i32Time );
		PajaTypes::int32			get_filter_mode( PajaTypes::int32 i32Time );
		PajaTypes::int32			get_shape( PajaTypes::int32 i32Time );

	protected:
		TestGizmoC();
		TestGizmoC( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
		TestGizmoC( Edit::EditableI* pOriginal );
		virtual ~TestGizmoC();

	private:
		Composition::ParamVector2C*		m_pParamPos;
		Composition::ParamFloatC*		m_pParamRot;
		Composition::ParamVector2C*		m_pParamScale;
		Composition::ParamVector2C*		m_pParamPivot;
		Composition::ParamColorC*		m_pParamColor;
		Composition::ParamIntC*			m_pParamRenderMode;
		Composition::ParamIntC*			m_pParamFilterMode;
		Composition::ParamIntC*			m_pParamShape;
		Composition::ParamFileC*		m_pParamFile;
	};


	//
	// transform
	//

	enum TransformGizmoParamsE {
		TRANSFORM_PARAM_POS = 0,
		TRANSFORM_PARAM_PIVOT,
		TRANSFORM_PARAM_ROT,
		TRANSFORM_PARAM_SCALE,
		TRANSFORM_PARAM_COUNT,
	};

	class TransformGizmoC : public Composition::GizmoI
	{
	public:

		static TransformGizmoC*		create_new( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
		virtual Edit::DataBlockI*	create();
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		virtual void				copy( Edit::EditableI* pEditable );
		virtual void				restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_parameter_count();
		virtual Composition::ParamI*	get_parameter( PajaTypes::int32 i32Index );

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

		void						init();
		PajaTypes::Vector2C			get_pos( PajaTypes::int32 i32Time );
		PajaTypes::Vector2C			get_pivot( PajaTypes::int32 i32Time );
		PajaTypes::float32			get_rot( PajaTypes::int32 i32Time );
		PajaTypes::Vector2C			get_scale( PajaTypes::int32 i32Time );

	protected:
		TransformGizmoC();
		TransformGizmoC( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
		TransformGizmoC( Edit::EditableI* pOriginal );
		virtual ~TransformGizmoC();

	private:
		Composition::ParamVector2C*		m_pParamPos;
		Composition::ParamFloatC*		m_pParamRot;
		Composition::ParamVector2C*		m_pParamScale;
		Composition::ParamVector2C*		m_pParamPivot;

	};


	//
	// attributes
	//

	enum AttributeGizmoParamsE {
		ATTRIBUTE_PARAM_SHAPE = 0,
		ATTRIBUTE_PARAM_RENDERMODE,
		ATTRIBUTE_PARAM_FILTERMODE,
		ATTRIBUTE_PARAM_OUTLINE_WIDTH,
		ATTRIBUTE_PARAM_OUTLINE_COLOR,
		ATTRIBUTE_PARAM_FILL_COLOR,
		ATTRIBUTE_PARAM_FILL_FILE,
		ATTRIBUTE_PARAM_COUNT,
	};

	class AttributeGizmoC : public Composition::GizmoI
	{
	public:

		static AttributeGizmoC*		create_new( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
		virtual Edit::DataBlockI*	create();
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		virtual void				copy( Edit::EditableI* pEditable );
		virtual void				restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_parameter_count();
		virtual Composition::ParamI*	get_parameter( PajaTypes::int32 i32Index );

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

		void						init();
		PajaTypes::int32			get_render_mode( PajaTypes::int32 i32Time );
		PajaTypes::int32			get_filter_mode( PajaTypes::int32 i32Time );
		PajaTypes::int32			get_shape( PajaTypes::int32 i32Time );
		Import::ImportableImageI*	get_fill_file( PajaTypes::int32 i32Time );
		PajaTypes::ColorC			get_fill_color( PajaTypes::int32 i32Time );
		PajaTypes::ColorC			get_outline_color( PajaTypes::int32 i32Time );
		PajaTypes::float32			get_outline_width( PajaTypes::int32 i32Time );

	protected:
		AttributeGizmoC();
		AttributeGizmoC( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
		AttributeGizmoC( Edit::EditableI* pOriginal );
		virtual ~AttributeGizmoC();

	private:
		Composition::ParamIntC*			m_pParamRenderMode;
		Composition::ParamIntC*			m_pParamFilterMode;
		Composition::ParamIntC*			m_pParamShape;
		Composition::ParamFileC*		m_pParamFillFile;
		Composition::ParamColorC*		m_pParamFillColor;
		Composition::ParamFloatC*		m_pParamOutlineWidth;
		Composition::ParamColorC*		m_pParamOutlineColor;
	};


	//
	// the effect
	//

	class TestEffectC : public Composition::EffectI
	{
	public:
		static TestEffectC*			create_new();
		virtual Edit::DataBlockI*	create();
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		virtual void				copy( Edit::EditableI* pEditable );
		virtual void				restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void				initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void				eval_state( PajaTypes::int32 i32Time );
		virtual PajaTypes::BBox2C	get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual PajaTypes::Matrix2C	get_texture_matrix() const;

		virtual bool				hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );


	protected:
		TestEffectC();
		TestEffectC( Edit::EditableI* pOriginal );
		virtual ~TestEffectC();

	private:

		TestGizmoC*			m_pTestGizmo;
		TransformGizmoC*	m_pTransGizmo;
		AttributeGizmoC*	m_pAttribGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
		PajaTypes::ColorC	m_rFillColor;
		PajaTypes::ColorC	m_rOutlineColor;
		PajaTypes::float32	m_f32OutlineWidth;
		PajaTypes::int32	m_i32RenderMode;
		PajaTypes::int32	m_i32FilterMode;
		PajaTypes::int32	m_i32Shape;
		PajaTypes::float32	m_f32Angle;
		PajaTypes::int32	m_i32Time;
	};


};	// namespace


// descprion test effect
class TestDescC : public PluginClass::ClassDescC
{
public:
	TestDescC() {};
	virtual ~TestDescC() {};

	void*						create() { return (void*)TestPlugin::TestEffectC::create_new(); };

	PajaTypes::int32			get_classtype() const { return PluginClass::CLASS_TYPE_EFFECT; };
	PluginClass::SuperClassIdC	get_super_class_id() const { return PluginClass::SUPERCLASS_EFFECT; };
	PluginClass::ClassIdC		get_class_id() const { return CLASS_IMAGE_EFFECT; };

	const char*					get_name() const { return "Image"; };
	const char*					get_desc() const { return "Image effect"; };

	const char*					get_author_name() const { return "Mikko \"memon\" Mononen"; };
	const char*					get_copyright_message() const { return "Copyright (c) 2000 Moppi Productions"; };
	const char*					get_url() const { return "http://www.moppi.inside.org/demopaja/"; };
	const char*					get_help_filename() const { return "res://testeffect.html"; };

	virtual PajaTypes::uint32			get_required_device_driver_count() const
	{
		return 1;
	}

	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx )
	{
		return PajaSystem::CLASS_OPENGL_DEVICEDRIVER;
	}


	// file extension info. (only used in import plugins)
	PajaTypes::uint32			get_ext_count() const { return 0; };
	const char*					get_ext( PajaTypes::uint32 ui32Index ) const { return 0; };
};


// descprion for TGA import
class TGAImportDescC : public PluginClass::ClassDescC
{
public:
	TGAImportDescC() {};
	virtual ~TGAImportDescC() {};

	void*						create() { return (void*)TestPlugin::TGAImportC::create_new(); };

	PajaTypes::int32			get_classtype() const { return PluginClass::CLASS_TYPE_FILEIMPORT; };
	PluginClass::SuperClassIdC	get_super_class_id() const { return PluginClass::SUPERCLASS_IMAGE; };
	PluginClass::ClassIdC		get_class_id() const { return CLASS_TGA_IMPORT; };

	const char*					get_name() const { return "Targa Image"; };
	const char*					get_desc() const { return "Importer for Targa (.TGA) images"; };

	const char*					get_author_name() const { return "Mikko \"memon\" Mononen"; };
	const char*					get_copyright_message() const { return "Copyright (c) 2000 Moppi Productions"; };
	const char*					get_url() const { return "http://www.moppi.inside.org/demopaja/"; };
	const char*					get_help_filename() const { return "res://tgaimport.html"; };

	virtual PajaTypes::uint32			get_required_device_driver_count() const
	{
		return 1;
	}

	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx )
	{
		return PajaSystem::CLASS_OPENGL_DEVICEDRIVER;
	}


	// file extension info. (only used in import plugins)
	PajaTypes::uint32			get_ext_count() const { return 4; };
	const char*					get_ext( PajaTypes::uint32 ui32Index ) const
	{
		if( ui32Index == 0 )
			return "tga";
		else if( ui32Index == 1 )
			return "vda";
		else if( ui32Index == 2 )
			return "icb";
		else if( ui32Index == 3 )
			return "vst";
		return 0;
	}
};

extern TestDescC		g_rTestDesc;
extern TGAImportDescC	g_rTGAImportDesc;


#endif // __TESTPLUGIN_H__
*/