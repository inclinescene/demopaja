        ��  ��                  t      ��
 ��h     0         // Number of outer filter taps
#define NUM_DOF_TAPS 12

float4 viewportScale;
float4 viewportBias;
float radius;

// transformations
matrix matWorldViewProj;

float2 filterTaps[NUM_DOF_TAPS];

texture tSceneColor;
texture tSceneColorHalf;
texture tBlurDepth;

///////////////////////////////////////////////////////////////////////////////

struct VS_INPUT
{
	float3 vPos: POSITION;
	float4 vDiffuse : COLOR0;
	float2 vTexCoord: TEXCOORD;
};

struct VS_OUTPUT
{
	float4 vPos: POSITION;
	float4 vDiffuse : COLOR0;
	float2 vTexCoord: TEXCOORD;
};

///////////////////////////////////////////////////////////////////////////////

struct PS_INPUT
{
	float2 vTexCoord: TEXCOORD;
	float4 vDiffuse : COLOR0;
};

sampler SceneColorSampler = sampler_state
{
	texture = (tSceneColor);

	MinFilter = Linear;
	MagFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

sampler SceneColorHalfSampler = sampler_state
{
	texture = (tSceneColorHalf);

	MinFilter = Linear;
	MagFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

sampler DepthBlurSampler = sampler_state
{
	texture = (tBlurDepth);

	MinFilter = Linear;
	MagFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;
};

///////////////////////////////////////////////////////////////////////////////

// Screen space vertex shader
VS_OUTPUT DOF_vs(VS_INPUT v)
{
	VS_OUTPUT o;

	o.vPos = mul( float4( v.vPos, 1 ), matWorldViewProj);

	// Output tex. coordinates
	o.vTexCoord = v.vTexCoord;

	o.vDiffuse = v.vDiffuse;

	return o;
}

// DoF filter shader
float4 DOFFirstPass_ps(PS_INPUT v) : COLOR
{
	// Get center sample
	float4 colorSum = tex2D(SceneColorSampler, v.vTexCoord);
	float2 centerDepthBlur = tex2D(DepthBlurSampler, v.vTexCoord);
	
	// Compute CoC size based on blurriness
	float sizeCoC = centerDepthBlur.y * radius;

	float totalContribution = 1.0f;

	// Run through all taps
	for (int i = 0; i < NUM_DOF_TAPS; i++)
	{
		// Compute tap coordinates
		float2 tapCoord = v.vTexCoord + filterTaps[i] * sizeCoC;

		// Fetch tap sample
		float4 tapColor = tex2D( SceneColorSampler, tapCoord );
		float2 tapDepthBlur = tex2D( DepthBlurSampler, tapCoord );

		// Compute tap contribution
		float tapContribution = (tapDepthBlur.x > centerDepthBlur.x) ? 1.0f : tapDepthBlur.y;

		// Accumulate color and contribution
		colorSum += tapColor * tapContribution;
		totalContribution += tapContribution;
	}

	// Normalize to get proper luminance
	float4 finalColor = colorSum / totalContribution;

	return finalColor;
}


// DoF filter shader
float4 DOFSecondPass_ps(PS_INPUT v) : COLOR
{
/*
	// Get center sample
	float4 colorSharp = tex2D(SceneColorSampler, v.vTexCoord);
	float4 colorSum = tex2D(SceneColorHalfSampler, v.vTexCoord);
	float2 centerDepthBlur = tex2D(DepthBlurSampler, v.vTexCoord);
	
	// Compute CoC size based on blurriness
	float sizeCoC = centerDepthBlur.y * radius;

	float totalContribution = 1.0f;

	// Run through all taps
	for (int i = 0; i < NUM_DOF_TAPS; i++)
	{
		// Compute tap coordinates
		float2 tapCoord = v.vTexCoord + filterTaps[i] * sizeCoC;

		// Fetch tap sample
		float4 tapColor = tex2D( SceneColorHalfSampler, tapCoord );
		float2 tapDepthBlur = tex2D( DepthBlurSampler, tapCoord );

		// Compute tap contribution
		float tapContribution = (tapDepthBlur.x > centerDepthBlur.x) ? 1.0f : tapDepthBlur.y;

		// Accumulate color and contribution
		colorSum += tapColor * tapContribution;
		totalContribution += tapContribution;
	}

	// Normalize to get proper luminance
	float4 finalColor = colorSum / totalContribution;

	// mix with the blur image.
	float	mix = saturate( centerDepthBlur.y * 8.0 );
	
	finalColor = lerp( colorSharp, finalColor, mix );
*/

	float4 finalColor = float4( 1, 1, 1, 1 );

	return finalColor;
}

technique FirstPassF
{
	pass P0
	{
		VertexShader = compile vs_2_0 DOF_vs();
		PixelShader = compile ps_2_0 DOFFirstPass_ps();
		CullMode = NONE;
	}
}

technique SecondPassF
{
	pass P0
	{
		VertexShader = compile vs_2_0 DOF_vs();
		PixelShader = compile ps_2_0 DOFSecondPass_ps();
		CullMode = NONE;
	}
}
