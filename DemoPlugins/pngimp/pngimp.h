//
// PNGimp.h
//
// PNG Import Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

#ifndef __PNGIMP_H__
#define __PNGIMP_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EditableI.h"
#include "UndoC.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLViewportC.h"
#include "OpenGLDeviceC.h"
#include "TimeContextC.h"
#include "ImportableImageI.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_PNG_IMPORT( 0xD73ABC0C, 0x9DF34452 );


//////////////////////////////////////////////////////////////////////////
//
//  PNG importer class descriptor.
//

class PNGImportDescC : public PluginClass::ClassDescC
{
public:
	PNGImportDescC();
	virtual ~PNGImportDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};



namespace PNGPlugin {

//////////////////////////////////////////////////////////////////////////
//
// PNG Importer class.
//

	class PNGImportC : public Import::ImportableImageI
	{
	public:
		static PNGImportC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual const char*				get_filename();
		virtual void					set_filename( const char* szName );
		virtual bool					load_file( const char* szName, PajaSystem::DemoInterfaceC* pInterface );
		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		// The importable image interface.
		virtual PajaTypes::float32		get_width();
		virtual PajaTypes::float32		get_height();
		virtual PajaTypes::int32		get_data_width();
		virtual PajaTypes::int32		get_data_height();
		virtual PajaTypes::BBox2C&	get_tex_coord_bounds();
		virtual PajaTypes::int32		get_data_pitch();
		virtual PajaTypes::int32		get_data_bpp();
		virtual PajaTypes::uint8*		get_data();

		virtual void					bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Stage, PajaTypes::uint32 ui32Properties );
		virtual const char*				get_info();
		virtual PluginClass::ClassIdC	get_default_effect();

		virtual PajaTypes::int32		get_duration();
		virtual PajaTypes::float32		get_start_label();
		virtual PajaTypes::float32		get_end_label();

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

	protected:
		PNGImportC();
		PNGImportC( Edit::EditableI* pOriginal );
		virtual ~PNGImportC();

		void						upload_texture();

	private:

		PajaTypes::uint32		m_ui32TextureId;
		PajaTypes::uint8*		m_pData;
		PajaTypes::int32		m_i32Width, m_i32Height;
		PajaTypes::int32		m_i32Bpp;
		PajaTypes::BBox2C		m_rTexBounds;

		std::string				m_sFileName;
	};



};	// namespace


extern PNGImportDescC	g_rPNGImportDesc;


#endif	// __PNGIMP_H__
