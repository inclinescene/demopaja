// Windows headers
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include "glext.h"
#include "extgl.h"

PFNGLCOLORTABLEPROC				glColorTable = NULL;
PFNGLCOLORTABLEPARAMETERFVPROC	glColorTableParameterfv = NULL;
PFNGLCOLORTABLEPARAMETERIVPROC	glColorTableParameteriv = NULL;
bool	g_bColorTableInitialised = false;


void
init_gl_extension()
{
	if( !g_bColorTableInitialised ) {
/*		char*	szExtensions;	
		szExtensions = (char*)glGetString( GL_EXTENSIONS );

		if( szExtensions &&
			strstr( szExtensions, "GL_EXT_blend_color" ) != 0 &&
			strstr( szExtensions, "GL_EXT_blend_minmax" ) != 0 &&
			strstr( szExtensions, "GL_EXT_blend_subtract" ) != 0 ) {	*/

			glColorTable			= (PFNGLCOLORTABLEPROC)wglGetProcAddress( "glColorTable" );
			glColorTableParameterfv	= (PFNGLCOLORTABLEPARAMETERFVPROC)wglGetProcAddress( "glColorTableParameterfv" );
			glColorTableParameteriv	= (PFNGLCOLORTABLEPARAMETERIVPROC)wglGetProcAddress( "glColorTableParameteriv" );

			if( glColorTable && glColorTableParameterfv && glColorTableParameteriv ) {
				g_bColorTableInitialised = true;
//				TRACE( "found color tables!!\n" );
			}

/*			g_bBlendEqu = true;
			TRACE( "GLARE: blendequ ok\n" );
		}*/
	}

}
