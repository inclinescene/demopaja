#ifndef __COLORPLUGIN_EXTGL_H__
#define __COLORPLUGIN_EXTGL_H__

// Windows headers
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include "glext.h"

extern PFNGLCOLORTABLEPROC				glColorTable;
extern PFNGLCOLORTABLEPARAMETERFVPROC	glColorTableParameterfv;
extern PFNGLCOLORTABLEPARAMETERIVPROC	glColorTableParameteriv;
extern bool	g_bColorTableInitialised;

extern void	init_gl_extension();

#endif