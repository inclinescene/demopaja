#ifndef __COLORMATRIX_H__
#define __COLORMATRIX_H__

void matrixmult( float* a, float* b, float* c );
void identmat( float *matrix );
void xformpnt( float* matrix, float x, float y, float z, float *tx, float *ty, float *tz );
void cscalemat( float* mat, float rscale, float gscale, float bscale );
void lummat( float* mat );
void saturatemat( float* mat, float sat );
void offsetmat( float* mat, float roffset, float goffset, float boffset );
void xrotatemat( float* mat, float rs, float rc );
void yrotatemat( float* mat, float rs, float rc );
void zrotatemat( float* mat, float rs, float rc );
void zshearmat( float* mat, float dx, float dy );
void simplehuerotatemat( float* mat, float rot );
void huerotatemat( float* mat, float rot );


#endif