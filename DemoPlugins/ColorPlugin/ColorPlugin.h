//
// BrightnessContrastPlugin.h
//
// Brightness/Contrast Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

#ifndef __BRIGHTNESSCONTRASTPLUGIN_H__
#define __BRIGHTNESSCONTRASTPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "ImportInterfaceC.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_BRIGHTNESSCONTRAST_EFFECT( 0xA4417381, 0xBE694108 );
const PluginClass::ClassIdC	CLASS_HUESATURATION_EFFECT( 0x369659B1, 0xA3DA4015 );
const PluginClass::ClassIdC	CLASS_LEVELS_EFFECT( 0xF90A7A28, 0x034645EB );
const PluginClass::ClassIdC	CLASS_TRESHOLD_EFFECT( 0x1BB8A7CA, 0xAFA6481B );


//////////////////////////////////////////////////////////////////////////
//
//  BrightnessContrast effect class descriptor.
//

class BrightnessContrastDescC : public PluginClass::ClassDescC
{
public:
	BrightnessContrastDescC();
	virtual ~BrightnessContrastDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  HueSaturation effect class descriptor.
//

class HueSaturationDescC : public PluginClass::ClassDescC
{
public:
	HueSaturationDescC();
	virtual ~HueSaturationDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  Levels effect class descriptor.
//

class LevelsDescC : public PluginClass::ClassDescC
{
public:
	LevelsDescC();
	virtual ~LevelsDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


namespace BrightnessContrastPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The BrightnessContrast effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_BRIGHTNESS = 0,
		ID_ATTRIBUTE_CONTRAST,
		ATTRIBUTE_COUNT,
	};

	enum EffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	class BrightnessContrastEffectC : public Composition::EffectI
	{
	public:
		static BrightnessContrastEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext );

		virtual void					eval_state( PajaTypes::int32 i32Time, PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		BrightnessContrastEffectC();
		BrightnessContrastEffectC( Edit::EditableI* pOriginal );
		virtual ~BrightnessContrastEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
	};

};	// namespace


namespace HueSaturationPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The HueSaturation effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_HUE = 0,
		ID_ATTRIBUTE_SATURATION,
		ID_ATTRIBUTE_LIGHTNESS,
		ATTRIBUTE_COUNT,
	};

	enum EffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	class HueSaturationEffectC : public Composition::EffectI
	{
	public:
		static HueSaturationEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext );

		virtual void					eval_state( PajaTypes::int32 i32Time, PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		HueSaturationEffectC();
		HueSaturationEffectC( Edit::EditableI* pOriginal );
		virtual ~HueSaturationEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
	};

};	// namespace


namespace LevelsPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The Levels effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum InputGizmoParamsE {
		ID_INPUT_LOW = 0,
		ID_INPUT_HIGH,
		ID_INPUT_GAMMA,
		INPUT_COUNT,
	};

	enum OutputGizmoParamsE {
		ID_OUTPUT_LOW = 0,
		ID_OUTPUT_HIGH,
		OUTPUT_COUNT,
	};

	enum EffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_INPUT,
		ID_GIZMO_OUTPUT,
		GIZMO_COUNT,
	};

	class LevelsEffectC : public Composition::EffectI
	{
	public:
		static LevelsEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext );

		virtual void					eval_state( PajaTypes::int32 i32Time, PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		LevelsEffectC();
		LevelsEffectC( Edit::EditableI* pOriginal );
		virtual ~LevelsEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pInGizmo;
		Composition::AutoGizmoC*	m_pOutGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
	};

};	// namespace


extern BrightnessContrastDescC	g_rBrightnessContrastDesc;
extern HueSaturationDescC		g_rHueSaturationDesc;
extern LevelsDescC				g_rLevelsDesc;


#endif	// __AVIPLUGIN_H__
