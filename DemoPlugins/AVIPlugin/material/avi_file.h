extern int avi_read_aframe(VIDEO_FILE *p, unsigned char *buf, int frame);
extern int avi_read_vframe(VIDEO_FILE *p, unsigned char *buf, int frame, int *len);
extern int avi_open_file(char *filename, VIDEO_FILE *p, int verbose);
extern int avi_check_file(char *filename);
extern int avi_close_file(VIDEO_FILE *p);
