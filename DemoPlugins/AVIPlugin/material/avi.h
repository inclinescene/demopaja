#define WAVE_FORMAT_PCM                 (0x0001)
#define WAVE_FORMAT_ALAW                (0x0006)
#define WAVE_FORMAT_MULAW               (0x0007)

#define AVIF_HASINDEX                   0x10

#define AVI_SWAP4(a) (a)

struct RIFF_avih {
    uint32 cs;
    uint32 us_frame;          /* microsec per frame */
    uint32 bps;               /* byte/s overall */
    uint32 unknown1;          /* pad_gran (???) */
    uint32 flags;
    uint32 frames;            /* # of frames (all) */
    uint32 init_frames;       /* initial frames (???) */
    uint32 streams;
    uint32 bufsize;           /* suggested buffer size */
    uint32 width;
    uint32 height;
    uint32 scale;
    uint32 rate;
    uint32 start;
    uint32 length;
};

struct RIFF_strh {
    uint32 cs;
    char   type[4];           /* stream type */
    char   handler[4];
    uint32 flags;
    uint32 priority;
    uint32 init_frames;       /* initial frames (???) */
    uint32 scale;
    uint32 rate;
    uint32 start;
    uint32 length;
    uint32 bufsize;           /* suggested buffer size */
    uint32 quality;
    uint32 samplesize;
    /* XXX 16 bytes ? */
};

struct RIFF_strf_vids {       /* == BitMapInfoHeader */
    uint32 cs;
    uint32 size;
    uint32 width;
    uint32 height;
    uint16 planes;
    uint16 bit_cnt;
    char   compression[4];
    uint32 image_size;
    uint32 xpels_meter;
    uint32 ypels_meter;
    uint32 num_colors;        /* used colors */
    uint32 imp_colors;        /* important colors */
    /* may be more for some codecs */
};

struct RIFF_strf_auds {       /* == WaveHeader (?) */
    uint32 cs;
    uint16 format;
    uint16 channels;
    uint32 rate;
    uint32 av_bps;
    uint16 blockalign;
    uint16 size;
};

#define size_strl_vids (sizeof(struct RIFF_strh) + \
			sizeof(struct RIFF_strf_vids) + \
			4*5)
#define size_strl_auds (sizeof(struct RIFF_strh) + \
			sizeof(struct RIFF_strf_auds) + \
			4*5)

typedef struct {
    uint32 flags;
    uint32 pos;
    uint32 len;
} vid_idx;

typedef struct {
    uint32 flags;
    uint32 pos;
    uint32 len;
    uint32 offset;
} aud_idx;

typedef struct {
    int fd;
    FILE *f;
    struct RIFF_avih *avih;
    struct RIFF_strh *a_strh, *v_strh;
    struct RIFF_strf_auds *a_strf;
    struct RIFF_strf_vids *v_strf;
    vid_idx *vindex;
    aud_idx *aindex;
    int afr, last_aframe, aleft;
    unsigned int filesize;
} AVI_FILE;

