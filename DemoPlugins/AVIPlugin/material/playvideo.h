#include "vidplay.h"

#define TRUE  1
#define FALSE 0
#define BUFSIZE 4096

typedef unsigned int   uint32;
typedef unsigned short uint16;

#define MAKEFOURCC(a,b,c,d) ( ((uint32)a)      | (((uint32)b)<< 8) | \
                             (((uint32)c)<<16) | (((uint32)d)<<24)  )

#define MAKE4CC(a) ( ((uint32)a[0])      | (((uint32)a[1])<< 8) | \
                             (((uint32)a[2])<<16) | (((uint32)a[3])<<24)  )

#define MAKECC4(a) ( ((uint32)a[3])      | (((uint32)a[2])<< 8) | \
                             (((uint32)a[1])<<16) | (((uint32)a[0])<<24)  )

/* The only FOURCCs interpreted by this program: */

#define RIFFtag MAKEFOURCC('R','I','F','F')
#define LISTtag MAKEFOURCC('L','I','S','T')
#define AVItag  MAKEFOURCC('A','V','I',' ')
#define avihtag MAKEFOURCC('a','v','i','h')
#define strltag MAKEFOURCC('s','t','r','l')
#define strhtag MAKEFOURCC('s','t','r','h')
#define strftag MAKEFOURCC('s','t','r','f')
#define vidstag MAKEFOURCC('v','i','d','s')
#define audstag MAKEFOURCC('a','u','d','s')
#define dmlhtag MAKEFOURCC('d','m','l','h')
#define movitag MAKEFOURCC('m','o','v','i')
#define hdrltag MAKEFOURCC('h','d','r','l')
#define idx1tag MAKEFOURCC('i','d','x','1')

#define MJPGtag MAKEFOURCC('M','J','P','G')
#define jpegtag MAKEFOURCC('j','p','e','g')
#define mpegtag MAKEFOURCC('m','p','e','g')
#define rletag  MAKEFOURCC('r','l','e',' ')
#define rpzatag MAKEFOURCC('r','p','z','a')
#define CRAMtag MAKEFOURCC('C','R','A','M')
#define cvidtag MAKEFOURCC('c','v','i','d')
#define IV32tag MAKEFOURCC('I','V','3','2')
#define IV31tag MAKEFOURCC('I','V','3','1')
#define iv31tag MAKEFOURCC('i','v','3','1')
#define IV41tag MAKEFOURCC('I','V','4','1')

#define _00dbtag MAKEFOURCC('0','0','d','b')
#define _00ivtag MAKEFOURCC('0','0','i','v')
#define _00dctag MAKEFOURCC('0','0','d','c')
#define _01wbtag MAKEFOURCC('0','1','w','b')

/* Build a string from a FOURCC number
   (s must have room for at least 5 chars) */

#define FOURCC2Str(fcc,s) \
{   s[0]=(fcc      ) & 0xFF; \
    s[1]=(fcc >>  8) & 0xFF; \
    s[2]=(fcc >> 16) & 0xFF; \
    s[3]=(fcc >> 24) & 0xFF; \
    s[4]=0; }


