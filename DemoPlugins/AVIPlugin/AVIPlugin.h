//
// AVIPlugin.h
//
// AVI Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

#ifndef __AVIPLUGIN_H__
#define __AVIPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLViewportC.h"
#include "OpenGLDeviceC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "AVIPlayerC.h"
#include "ImportableImageI.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_AVIPLAYER_EFFECT( 0x311832A9, 0x166D4653 );
const PluginClass::ClassIdC	CLASS_AVI_IMPORT( 0x221CA738, 0x431A45BA );


//////////////////////////////////////////////////////////////////////////
//
//  AVI importer class descriptor.
//

class AVIImportDescC : public PluginClass::ClassDescC
{
public:
	AVIImportDescC();
	virtual ~AVIImportDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  Simple image effect class descriptor.
//

class AVIPlayerDescC : public PluginClass::ClassDescC
{
public:
	AVIPlayerDescC();
	virtual ~AVIPlayerDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


namespace AVIPlugin {

//////////////////////////////////////////////////////////////////////////
//
// AVI Importer class.
//

//	class AVIImportC : public Import::ImportableVideoI
	class AVIImportC : public Import::ImportableImageI
	{
	public:
		static AVIImportC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual const char*				get_filename();
		virtual void					set_filename( const char* szName );
		virtual bool					load_file( const char* szName, PajaSystem::DemoInterfaceC* pInterface );
		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		// The importable image interface.
		virtual PajaTypes::float32		get_width();
		virtual PajaTypes::float32		get_height();
		virtual PajaTypes::int32		get_data_width();
		virtual PajaTypes::int32		get_data_height();
		virtual PajaTypes::int32		get_data_pitch();
		virtual PajaTypes::BBox2C&	get_tex_coord_bounds();
		virtual PajaTypes::int32		get_data_bpp();
		virtual PajaTypes::uint8*		get_data();

		virtual PajaTypes::int32		get_first_frame();
		virtual PajaTypes::int32		get_last_frame();
		virtual PajaTypes::float32		get_fps();

		virtual void					bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Stage, PajaTypes::uint32 ui32Properties );
		virtual const char*				get_info();
		virtual PluginClass::ClassIdC	get_default_effect();

		virtual PajaTypes::int32		get_duration();
		virtual PajaTypes::float32		get_start_label();
		virtual PajaTypes::float32		get_end_label();

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

	protected:
		AVIImportC();
		AVIImportC( Edit::EditableI* pOriginal );
		virtual ~AVIImportC();

	private:

		PajaTypes::uint32		m_ui32TextureId;
		std::string				m_sFileName;

		PajaTypes::uint8*		m_pResampleData;
		PajaTypes::uint32		m_ui32ResampleWidth;
		PajaTypes::uint32		m_ui32ResampleHeight;
		PajaTypes::BBox2C		m_rTexBounds;

		PajaTypes::uint8*		m_pRawData;
		PajaTypes::uint32		m_ui32RawSize;
		AVIPlayerC*				m_pPlayer;
		PajaTypes::int32		m_i32LastFrame;
		PajaTypes::int32		m_i32CurrentFrame;
		bool					m_bNeedUpdate;
	};


//////////////////////////////////////////////////////////////////////////
//
// The AVI Player effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_ROT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_COLOR = 0,
		ID_ATTRIBUTE_RENDERMODE,
		ID_ATTRIBUTE_FILTERMODE,
		ID_ATTRIBUTE_FILE,
		ATTRIBUTE_COUNT,
	};

	enum TimeGizmoParamsE {
		ID_TIME_FPS = 0,	// deprecated, needed for ID compatibility
		ID_TIME_FRAME,
	};

	enum AVIPlayerEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		ID_GIZMO_TIME,
		GIZMO_COUNT,
	};

	enum FilterModeE {
		FILTERMODE_LINEAR = 0,
		FILTERMODE_NEAREST = 1,
	};

	enum RenderModeE {
		RENDERMODE_NORMAL = 0,
		RENDERMODE_ADD = 1,
		RENDERMODE_MULT = 2,
	};

	class AVIPlayerEffectC : public Composition::EffectI
	{
	public:
		static AVIPlayerEffectC*		create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual PajaTypes::uint32		update_notify( EditableI* pCaller );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		AVIPlayerEffectC();
		AVIPlayerEffectC( Edit::EditableI* pOriginal );
		virtual ~AVIPlayerEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;
		Composition::AutoGizmoC*	m_pTimeGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
		PajaTypes::ColorC	m_rFillColor;
		PajaTypes::int32	m_i32Frame;
//		PajaTypes::int32	m_i32Time;
//		PajaTypes::int32	m_i32FileTime;
		PajaTypes::int32	m_i32RenderMode;
		PajaTypes::int32	m_i32FilterMode;
	};

};	// namespace


extern AVIImportDescC	g_rAVImportDesc;
extern AVIPlayerDescC	g_rAVIPlayerDesc;


#endif	// __AVIPLUGIN_H__
