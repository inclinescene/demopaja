#ifndef MESHREFC_H
#define MESHREFC_H

#include "PajaTypes.h"
#include "ColorC.h"
#include "ImportableI.h"
#include "FileIO.h"
#include "ScenegraphItemI.h"
#include "QuatC.h"
#include "Vector2C.h"
#include "Vector3C.h"
#include "ContVector3C.h"
#include "ContQuatC.h"
#include "ControllerC.h"


const PajaTypes::int32	CGITEM_HELPER = 0x1000;


class HelperC : public ScenegraphItemI
{
public:
	HelperC();
	virtual ~HelperC();

	virtual PajaTypes::int32			get_type();

	// transformation

	virtual void						eval_state( PajaTypes::int32 i32Time );

	virtual void						set_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_position();

	virtual void						set_scale( const PajaTypes::Vector3C& rScale );
	virtual const PajaTypes::Vector3C&	get_scale();

	virtual void						set_rotation( const PajaTypes::QuatC& rRot );
	virtual const PajaTypes::QuatC&		get_rotation();

	virtual void						set_position_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_position_controller();

	virtual void						set_rotation_controller( ContQuatC* pCont );
	virtual ContQuatC*					get_rotation_controller();

	virtual void						set_scale_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_scale_controller();

	// geometry
	virtual void			set_center( PajaTypes::Vector3C rCenter );
	virtual void			set_radius( PajaTypes::float32 f32Radius );
	virtual const PajaTypes::Vector3C&	get_center() const;
	virtual PajaTypes::float32					get_radius();

	// serialize
	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

private:

	PajaTypes::Vector3C			m_rPosition;
	PajaTypes::Vector3C			m_rScale;
	PajaTypes::QuatC			m_rRotation;

	ContVector3C*				m_pPosCont;
	ContQuatC*					m_pRotCont;
	ContVector3C*				m_pScaleCont;

	PajaTypes::Vector3C			m_rCenter;
	PajaTypes::float32			m_f32Radius;
};


#endif // MESHREFC_H