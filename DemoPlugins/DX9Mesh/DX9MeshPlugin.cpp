//
// DX9MeshPlugin.cpp
//
// DX9Mesh Plugin
//
// Copyright (c) 2000-2004 memon/moppi productions
//

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <d3d9.h>
#include <d3dx9math.h>

// Demopaja headers
#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "ClassDescC.h"
#include "ImportableI.h"
#include "DX9DeviceC.h"
#include "Dx9ViewportC.h"
#include "FileIO.h"
#include "AutoGizmoC.h"
#include "Vector2C.h"
#include "BBox2C.h"
#include "EffectMaskI.h"

#include "DX9MeshPlugin.h"

using namespace PajaTypes;
using namespace PluginClass;
using namespace PajaSystem;
using namespace Edit;
using namespace Composition;
using namespace Import;
using namespace FileIO;

using namespace DX9MeshPlugin;


//////////////////////////////////////////////////////////////////////////
//
//  DX9Mesh effect class descriptor.
//

DX9MeshDescC::DX9MeshDescC()
{
	// empty
}

DX9MeshDescC::~DX9MeshDescC()
{
	// empty
}

void*
DX9MeshDescC::create()
{
	return (void*)DX9MeshEffectC::create_new();
}

int32
DX9MeshDescC::get_classtype() const
{
	return CLASS_TYPE_EFFECT;
}

SuperClassIdC
DX9MeshDescC::get_super_class_id() const
{
	return SUPERCLASS_EFFECT;
}

ClassIdC
DX9MeshDescC::get_class_id() const
{
	return CLASS_DX9MESH_EFFECT;
};

const char*
DX9MeshDescC::get_name() const
{
	return CLASS_DX9MESH_EFFECT_NAME;
}

const char*
DX9MeshDescC::get_desc() const
{
	return CLASS_DX9MESH_EFFECT_DESC;
}

const char*
DX9MeshDescC::get_author_name() const
{
	return "Mikko \"memon\" Mononen";
}

const char*
DX9MeshDescC::get_copyright_message() const
{
	return "Copyright (c) 2000-2004 Moppi Productions";
}

const char*
DX9MeshDescC::get_url() const
{
	return "http://www.demopaja.org/";
}

const char*
DX9MeshDescC::get_help_filename() const
{
	return "res://ImageHelp.html";
}

uint32
DX9MeshDescC::get_required_device_driver_count() const
{
	return 1;
}

const ClassIdC&
DX9MeshDescC::get_required_device_driver( uint32 ui32Idx )
{
	return PajaSystem::CLASS_DX9_DEVICEDRIVER;
}


uint32
DX9MeshDescC::get_ext_count() const
{
	return 0;
}

const char*
DX9MeshDescC::get_ext( uint32 ui32Index ) const
{
	return 0;
}


//////////////////////////////////////////////////////////////////////////
//
//  X File importer class descriptor.
//

DX9XImportDescC::DX9XImportDescC()
{
	// empty
}

DX9XImportDescC::~DX9XImportDescC()
{
	// empty
}

void*
DX9XImportDescC::create()
{
	return DX9XImportC::create_new();
}

int32
DX9XImportDescC::get_classtype() const
{
	return CLASS_TYPE_FILEIMPORT;
}

SuperClassIdC
DX9XImportDescC::get_super_class_id() const
{
	return SUPERCLASS_IMPORT;
}

ClassIdC
DX9XImportDescC::get_class_id() const
{
	return CLASS_DX9X_IMPORT;
}

const char*
DX9XImportDescC::get_name() const
{
	return CLASS_DX9X_IMPORT_NAME;
}

const char*
DX9XImportDescC::get_desc() const
{
	return CLASS_DX9X_IMPORT_DESC;
}

const char*
DX9XImportDescC::get_author_name() const
{
	return "Mikko \"memon\" Mononen";
}

const char*
DX9XImportDescC::get_copyright_message() const
{
	return "Copyright (c) 2004 Moppi Productions";
}

const char*
DX9XImportDescC::get_url() const
{
	return "http://www.demopaja.org";
}

const char*
DX9XImportDescC::get_help_filename() const
{
	return "res://MAImportHelp.html";
}

uint32
DX9XImportDescC::get_required_device_driver_count() const
{
	return 1;
}

const ClassIdC&
DX9XImportDescC::get_required_device_driver( uint32 ui32Idx )
{
	return PajaSystem::CLASS_DX9_DEVICEDRIVER;
}

uint32
DX9XImportDescC::get_ext_count() const
{
	return 1;
}

const char*
DX9XImportDescC::get_ext( uint32 ui32Index ) const
{
	if( ui32Index == 0 )
		return "x";
	return 0;
}



//////////////////////////////////////////////////////////////////////////
//
//  ASE importer class descriptor.
//

DX9ASEImportDescC::DX9ASEImportDescC()
{
	// empty
}

DX9ASEImportDescC::~DX9ASEImportDescC()
{
	// empty
}

void*
DX9ASEImportDescC::create()
{
	return DX9ASEImportC::create_new();
}

int32
DX9ASEImportDescC::get_classtype() const
{
	return CLASS_TYPE_FILEIMPORT;
}

SuperClassIdC
DX9ASEImportDescC::get_super_class_id() const
{
	return SUPERCLASS_IMPORT;
}

ClassIdC
DX9ASEImportDescC::get_class_id() const
{
	return CLASS_DX9ASE_IMPORT;
}

const char*
DX9ASEImportDescC::get_name() const
{
	return CLASS_DX9ASE_IMPORT_NAME;
}

const char*
DX9ASEImportDescC::get_desc() const
{
	return CLASS_DX9ASE_IMPORT_DESC;
}

const char*
DX9ASEImportDescC::get_author_name() const
{
	return "Mikko \"memon\" Mononen";
}

const char*
DX9ASEImportDescC::get_copyright_message() const
{
	return "Copyright (c) 2004 Moppi Productions";
}

const char*
DX9ASEImportDescC::get_url() const
{
	return "http://www.demopaja.org";
}

const char*
DX9ASEImportDescC::get_help_filename() const
{
	return "res://MAImportHelp.html";
}

uint32
DX9ASEImportDescC::get_required_device_driver_count() const
{
	return 1;
}

const ClassIdC&
DX9ASEImportDescC::get_required_device_driver( uint32 ui32Idx )
{
	return PajaSystem::CLASS_DX9_DEVICEDRIVER;
}

uint32
DX9ASEImportDescC::get_ext_count() const
{
	return 1;
}

const char*
DX9ASEImportDescC::get_ext( uint32 ui32Index ) const
{
	if( ui32Index == 0 )
		return "ase";
	return 0;
}


//////////////////////////////////////////////////////////////////////////
//
// Global descriptors, which will be returned to the Demopaja.
//

DX9MeshDescC			g_rDX9MeshDesc;
DX9XImportDescC		g_rDX9XImport;
DX9ASEImportDescC		g_rDX9ASEImport;

#ifndef DEMOPAJAPLAYER

//////////////////////////////////////////////////////////////////////////
//
// The DLL
//

HINSTANCE	g_hInstance = 0;

BOOL APIENTRY
DllMain( HANDLE hModule, DWORD ulReasonForCall, LPVOID lpReserved )
{
	switch( ulReasonForCall )
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
		g_hInstance = (HINSTANCE)hModule;
		break;
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}

	return TRUE;
}


//
// Returns number of classes inside this plugin DLL.
//

__declspec( dllexport )
int32
get_classdesc_count()
{
	return 3;
}


//
// Returns class descriptors of the plugin classes.
//

__declspec( dllexport )
ClassDescC*
get_classdesc( int32 i )
{
	if( i == 0 )
		return &g_rDX9MeshDesc;
	else if( i == 1 )
		return &g_rDX9XImport;
	else if( i == 2 )
		return &g_rDX9ASEImport;
	return 0;
}


//
// Returns the API version this DLL was made with.
//

__declspec( dllexport )
int32
get_api_version()
{
	return DEMOPAJA_VERSION;
}

//
// Returns the DLL name.
//

__declspec( dllexport )
char*
get_dll_name()
{
	return "DX9MeshPlugin.dll - Image Plugin (c)2000-2004 memon/moppi productions";
}

#endif
