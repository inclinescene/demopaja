#ifndef SCENEGRAPHITEMI_H
#define SCENEGRAPHITEMI_H


#include "PajaTypes.h"
#include "Vector3C.h"
#include "Matrix3C.h"
#include "FileIO.h"
#include <vector>
#include <string>


class ScenegraphItemI
{
public:
	ScenegraphItemI();
	virtual ~ScenegraphItemI();

	virtual PajaTypes::int32			get_type() = 0;
	virtual void						eval_state( PajaTypes::int32 i32Time ) = 0;

	virtual void						add_child( ScenegraphItemI* pChild );
	virtual PajaTypes::uint32			get_child_count();
	virtual ScenegraphItemI*			get_child( PajaTypes::uint32 ui32Index );
	virtual void						remove_child( PajaTypes::uint32 ui32Index );

	virtual void						set_name( const char* szName );
	virtual const char*					get_name();

	virtual void						set_parent_name( const char* szName );
	virtual const char*					get_parent_name();

	virtual void						set_tm( const PajaTypes::Matrix3C& rMat );
	virtual const PajaTypes::Matrix3C&	get_tm();

	// just for save
	virtual void						set_id( PajaTypes::uint32 ui32ID );
	virtual PajaTypes::uint32			get_id() const;

	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

protected:
	std::vector<ScenegraphItemI*>	m_rChilds;
	std::string						m_sName;
	std::string						m_sParentName;
	PajaTypes::uint32				m_ui32ID;
	PajaTypes::Matrix3C				m_rTM;
};


#endif // SCENEGRAPHITEMI_H