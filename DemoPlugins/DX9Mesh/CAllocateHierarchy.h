
#ifndef __ALLOCATE_HIERARCHY_H__
#define __ALLOCATE_HIERARCHY_H__

#include <d3dx9.h>
#include <tchar.h>
#include "CD3DModel.h"


struct D3DXFRAME_DERIVED: public D3DXFRAME
{
	D3DXMATRIXA16        CombinedTransformationMatrix;
};

//Derived from the default mesh container
struct D3DXMESHCONTAINER_DERIVED: public D3DXMESHCONTAINER
{
//	LPDIRECT3DTEXTURE9*		ppTextures;       // array of textures, entries are NULL if no texture specified    
	DWORD*								pTextures;
	// SkinMesh info             
	LPD3DXMESH						pOrigMesh;
	LPD3DXATTRIBUTERANGE	pAttributeTable;
	DWORD									NumAttributeGroups; 
	DWORD									NumInfl;
	LPD3DXBUFFER					pBoneCombinationBuf;
	D3DXMATRIX**					ppBoneMatrixPtrs;
	D3DXMATRIX*						pBoneOffsetMatrices;
	DWORD									NumPaletteEntries;
	bool									bHasTangents;
};


class CAllocateHierarchy: public ID3DXAllocateHierarchy
{
public:
	STDMETHOD(CreateFrame)(THIS_ LPCSTR Name, LPD3DXFRAME *ppNewFrame);
	STDMETHOD(CreateMeshContainer)(THIS_ 
		LPCSTR Name, 
		CONST D3DXMESHDATA *pMeshData,
		CONST D3DXMATERIAL *pMaterials, 
		CONST D3DXEFFECTINSTANCE *pEffectInstances, 
		DWORD NumMaterials, 
		CONST DWORD *pAdjacency, 
		LPD3DXSKININFO pSkinInfo, 
		LPD3DXMESHCONTAINER *ppNewMeshContainer);
	STDMETHOD(DestroyFrame)(THIS_ LPD3DXFRAME pFrameToFree);
	STDMETHOD(DestroyMeshContainer)(THIS_ LPD3DXMESHCONTAINER pMeshContainerBase);

	HRESULT GenerateSkinnedMesh( LPDIRECT3DDEVICE9 pd3dDevice, D3DXMESHCONTAINER_DERIVED *pMeshContainer );
};

#endif