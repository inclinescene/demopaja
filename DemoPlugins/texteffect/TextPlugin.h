
#ifndef __TEXTPLUGIN_H__
#define __TEXTPLUGIN_H__

#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "ClassDescC.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "LayerC.h"
#include "ParamI.h"
#include "ImportableI.h"
#include "ImportableImageI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLViewportC.h"
#include "OpenGLDeviceC.h"
#include "TimeContextC.h"
#include <string>

const	PluginClass::ClassIdC	CLASS_TEXT_EFFECT( 0, 105 );
const	PluginClass::ClassIdC	CLASS_MOFO_IMPORT( 0, 205 );


namespace TextPlugin {

class MOFOImportC : public Import::ImportableI
{
public:
	static MOFOImportC*			create_new();
	virtual Edit::DataBlockI*	create();
	virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
	virtual void				copy( Edit::EditableI* pEditable );
	virtual void				restore( Edit::EditableI* pEditable );

	// the interface
	// returns the name of the file this importable refers to
	virtual const char*			get_filename();
	virtual void				set_filename( const char* szName );
	// loads the file
	virtual bool				load_file( const char* szName, PajaSystem::DemoInterfaceC* pInterface );
	virtual void				initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

	virtual PluginClass::ClassIdC		get_class_id();
	virtual PluginClass::SuperClassIdC	get_super_class_id();
	virtual const char*					get_class_name();

	/*
	ImportableFontI ...

	// tavallinen ImportableImageI tekstuuria varten
	virtual PajaTypes::int32	get_width() = 0;
	virtual PajaTypes::int32	get_height() = 0;
	virtual PajaTypes::int32	get_pitch() = 0;
	virtual PajaTypes::int32	get_bpp() = 0;
	virtual PajaTypes::uint8*	get_data() = 0;
	virtual void				bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Properties ) = 0;

	// hmmm....
	virtual PajaTypes::int32	get_glyph_height() const;
	virtual PajaTypes::int32	get_glyph_width( PajaTypes::uint32 ui32Index ) const;
	virtual PajaTypes::int32	get_glyph_advance( PajaTypes::uint32 ui32Index ) const;
	virtual PajaTypes::uint32	get_glyph_vertex_count() const;
	virtual float32*			get_glyph_vertices( PajaTypes::uint32 ui32Index ) const;
	virtual PajaTypes::uint32	get_glyph_texcoord_count() const;
	virtual float32*			get_glyph_texcoords( PajaTypes::uint32 ui32Index ) const;

	*/

	// interface for this class
	virtual PajaTypes::int32	get_width();
	virtual PajaTypes::int32	get_height();

	virtual PajaTypes::uint16	get_font_height();
	virtual PajaTypes::uint16	get_glyph_width( PajaTypes::uint32 ui32Index );
	virtual PajaTypes::uint16	get_glyph_advance( PajaTypes::uint32 ui32Index );
	virtual void				get_glyph_coords( PajaTypes::uint32 ui32Index, PajaTypes::float32& f32U0, PajaTypes::float32& f32V0, PajaTypes::float32& f32U1, PajaTypes::float32& f32V1 );
	virtual void				bind_texture( PajaSystem::DeviceInterfaceI* pInterface );

	virtual void					eval_state( PajaTypes::int32 i32Time );

	virtual const char*				get_info();
	virtual PluginClass::ClassIdC	get_default_effect();

	virtual PajaTypes::int32	get_duration();
	virtual PajaTypes::float32	get_start_label();
	virtual PajaTypes::float32	get_end_label();

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

protected:
	MOFOImportC();
	MOFOImportC( Edit::EditableI* pOriginal );
	virtual ~MOFOImportC();

private:
	// RGB, RGBA, GREY, ALPHA
	PajaTypes::uint8*		m_pData;
	PajaTypes::uint16*		m_pWidths;		// [96]
	PajaTypes::uint16*		m_pCoords;		// [96 * 4]
	PajaTypes::uint32		m_ui32FirstGlyph;
	PajaTypes::uint32		m_ui32LastGlyph;
	PajaTypes::uint32		m_ui32FontHeight;
	PajaTypes::uint32		m_ui32GlyphCount;

	PajaTypes::uint32		m_ui32TexWidth;
	PajaTypes::uint32		m_ui32TexHeight;

	PajaTypes::uint32		m_ui32TextureId;
	std::string				m_sFileName;
};


//
// text gizmo	(OBOSOLETE)
//

enum TextGizmoParamsE {
	TEXT_PARAM_POS = 0,
	TEXT_PARAM_PIVOT,
	TEXT_PARAM_SCALE,
	TEXT_PARAM_ROT,
	TEXT_PARAM_COLOR,
	TEXT_PARAM_FILE,
	TEXT_PARAM_COUNT,
};


class TextGizmoC : public Composition::GizmoI
{
public:

	static TextGizmoC*			create_new( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	virtual Edit::DataBlockI*	create();
	virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
	virtual void				copy( Edit::EditableI* pEditable );
	virtual void				restore( Edit::EditableI* pEditable );

	virtual PajaTypes::int32		get_parameter_count();
	virtual Composition::ParamI*	get_parameter( PajaTypes::int32 i32Index );

	virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

	void						init();
	MOFOImportC*				get_file( PajaTypes::int32 i32Time, PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext );
	PajaTypes::Vector2C			get_pos( PajaTypes::int32 i32Time );
	PajaTypes::Vector2C			get_pivot( PajaTypes::int32 i32Time );
	PajaTypes::Vector2C			get_scale( PajaTypes::int32 i32Time );
	PajaTypes::ColorC			get_color( PajaTypes::int32 i32Time );
	PajaTypes::float32			get_rot( PajaTypes::int32 i32Time );

protected:
	TextGizmoC();
	TextGizmoC( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	TextGizmoC( Edit::EditableI* pOriginal );
	virtual ~TextGizmoC();

private:
	Composition::ParamVector2C*		m_pParamPos;
	Composition::ParamVector2C*		m_pParamScale;
	Composition::ParamVector2C*		m_pParamPivot;
	Composition::ParamFloatC*		m_pParamRot;
	Composition::ParamColorC*		m_pParamColor;
	Composition::ParamFileC*		m_pParamFile;
};


//
// text anim	(OBOSOLETE)
//

enum TextAnimGizmoParamsE {
	TEXTANIM_PARAM_OFFSET = 0,
	TEXTANIM_PARAM_SCALE,
	TEXTANIM_PARAM_COLOR,
	TEXTANIM_PARAM_DELAY,
	TEXTANIM_PARAM_TEXT,
	TEXTANIM_PARAM_COUNT,
};

class TextAnimGizmoC : public Composition::GizmoI
{
public:

	static TextAnimGizmoC*		create_new( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	virtual Edit::DataBlockI*	create();
	virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
	virtual void				copy( Edit::EditableI* pEditable );
	virtual void				restore( Edit::EditableI* pEditable );

	virtual PajaTypes::int32		get_parameter_count();
	virtual Composition::ParamI*	get_parameter( PajaTypes::int32 i32Index );

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	void						init();
	PajaTypes::Vector2C			get_offset( PajaTypes::int32 i32Time );
	PajaTypes::Vector2C			get_scale( PajaTypes::int32 i32Time );
	PajaTypes::ColorC			get_color( PajaTypes::int32 i32Time );
	const char*					get_text( PajaTypes::int32 i32Time );
	PajaTypes::int32			get_delay( PajaTypes::int32 i32Time );

	PajaTypes::int32			get_offset_range();
	PajaTypes::int32			get_scale_range();
	PajaTypes::int32			get_color_range();


protected:
	TextAnimGizmoC();
	TextAnimGizmoC( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	TextAnimGizmoC( Edit::EditableI* pOriginal );
	virtual ~TextAnimGizmoC();

private:
	Composition::ParamVector2C*		m_pParamOffset;
	Composition::ParamVector2C*		m_pParamScale;
	Composition::ParamColorC*		m_pParamColor;
	Composition::ParamTextC*		m_pParamText;
	Composition::ParamIntC*			m_pParamDelay;
	std::string						m_rCurText;
};


//
// transform gizmo
//

enum TransformParamsE {
	TRANSFORM_PARAM_POS = 0,
	TRANSFORM_PARAM_PIVOT,
	TRANSFORM_PARAM_ROT,
	TRANSFORM_PARAM_SCALE,
	TRANSFORM_PARAM_COUNT,
};


class TransformGizmoC : public Composition::GizmoI
{
public:

	static TransformGizmoC*		create_new( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	virtual Edit::DataBlockI*	create();
	virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
	virtual void				copy( Edit::EditableI* pEditable );
	virtual void				restore( Edit::EditableI* pEditable );

	virtual PajaTypes::int32		get_parameter_count();
	virtual Composition::ParamI*	get_parameter( PajaTypes::int32 i32Index );

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	void						init();
	PajaTypes::Vector2C			get_pos( PajaTypes::int32 i32Time );
	PajaTypes::Vector2C			get_pivot( PajaTypes::int32 i32Time );
	PajaTypes::Vector2C			get_scale( PajaTypes::int32 i32Time );
	PajaTypes::float32			get_rot( PajaTypes::int32 i32Time );

protected:
	TransformGizmoC();
	TransformGizmoC( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	TransformGizmoC( Edit::EditableI* pOriginal );
	virtual ~TransformGizmoC();

private:
	Composition::ParamVector2C*		m_pParamPos;
	Composition::ParamVector2C*		m_pParamScale;
	Composition::ParamVector2C*		m_pParamPivot;
	Composition::ParamFloatC*		m_pParamRot;
};


//
// attribute
//

enum AttributeGizmoParamsE {
	ATTRIBUTE_PARAM_TEXT = 0,
	ATTRIBUTE_PARAM_COLOR,
	ATTRIBUTE_PARAM_FILE,
	ATTRIBUTE_PARAM_COUNT,
};

class AttributeGizmoC : public Composition::GizmoI
{
public:

	static AttributeGizmoC*		create_new( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	virtual Edit::DataBlockI*	create();
	virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
	virtual void				copy( Edit::EditableI* pEditable );
	virtual void				restore( Edit::EditableI* pEditable );

	virtual PajaTypes::int32		get_parameter_count();
	virtual Composition::ParamI*	get_parameter( PajaTypes::int32 i32Index );

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	void						init();
	const char*					get_text( PajaTypes::int32 i32Time );
	MOFOImportC*				get_file( PajaTypes::int32 i32Time, PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext );
	PajaTypes::ColorC			get_color( PajaTypes::int32 i32Time );

protected:
	AttributeGizmoC();
	AttributeGizmoC( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	AttributeGizmoC( Edit::EditableI* pOriginal );
	virtual ~AttributeGizmoC();

private:
	Composition::ParamColorC*		m_pParamColor;
	Composition::ParamFileC*		m_pParamFile;
	Composition::ParamTextC*		m_pParamText;
	std::string						m_rCurText;
};

//
// text anim
//

enum AnimationGizmoParamsE {
	ANIMATION_PARAM_OFFSET = 0,
	ANIMATION_PARAM_SCALE,
	ANIMATION_PARAM_COLOR,
	ANIMATION_PARAM_DELAY,
	ANIMATION_PARAM_COUNT,
};

class AnimationGizmoC : public Composition::GizmoI
{
public:

	static AnimationGizmoC*		create_new( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	virtual Edit::DataBlockI*	create();
	virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
	virtual void				copy( Edit::EditableI* pEditable );
	virtual void				restore( Edit::EditableI* pEditable );

	virtual PajaTypes::int32		get_parameter_count();
	virtual Composition::ParamI*	get_parameter( PajaTypes::int32 i32Index );

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	void						init();
	PajaTypes::Vector2C			get_offset( PajaTypes::int32 i32Time );
	PajaTypes::Vector2C			get_scale( PajaTypes::int32 i32Time );
	PajaTypes::ColorC			get_color( PajaTypes::int32 i32Time );
	PajaTypes::int32			get_delay( PajaTypes::int32 i32Time );

	PajaTypes::int32			get_offset_range();
	PajaTypes::int32			get_scale_range();
	PajaTypes::int32			get_color_range();


protected:
	AnimationGizmoC();
	AnimationGizmoC( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	AnimationGizmoC( Edit::EditableI* pOriginal );
	virtual ~AnimationGizmoC();

private:
	Composition::ParamVector2C*		m_pParamOffset;
	Composition::ParamVector2C*		m_pParamScale;
	Composition::ParamColorC*		m_pParamColor;
	Composition::ParamIntC*			m_pParamDelay;
};


//
// the effect
//

class TextEffectC : public Composition::EffectI
{
public:
	static TextEffectC*			create_new();
	virtual Edit::DataBlockI*	create();
	virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
	virtual void				copy( Edit::EditableI* pEditable );
	virtual void				restore( Edit::EditableI* pEditable );

	virtual PajaTypes::int32		get_gizmo_count();
	virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

	virtual PluginClass::ClassIdC	get_class_id();
	virtual const char*				get_class_name();

	virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
	virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

	virtual void				initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

	virtual void				eval_state( PajaTypes::int32 i32Time );
	virtual PajaTypes::BBox2C	get_bbox();

	virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

	virtual bool				hit_test( const PajaTypes::Vector2C& rPoint );

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );


protected:
	TextEffectC();
	TextEffectC( Edit::EditableI* pOriginal );
	virtual ~TextEffectC();

private:
	TextGizmoC*			m_pTextGizmo;
	TextAnimGizmoC*		m_pTextAnimGizmo;

	TransformGizmoC*	m_pTransGizmo;
	AttributeGizmoC*	m_pAttribGizmo;
	AnimationGizmoC*	m_pAnimGizmo;

	PajaTypes::Matrix2C	m_rTM;
	PajaTypes::BBox2C	m_rBBox;
	PajaTypes::Vector2C	m_rVertices[4];
	PajaTypes::ColorC	m_rColor;
	PajaTypes::int32	m_i32Time;
	PajaTypes::float32	m_f32Width, m_f32Height;
	std::vector<PajaTypes::Vector2C>	m_rVertCache;
};

}; // namespace


// descprion Text effect
class TextDescC : public PluginClass::ClassDescC
{
public:
	TextDescC() {};
	virtual ~TextDescC() {};

	void*						create() { return (void*)TextPlugin::TextEffectC::create_new(); };

	PajaTypes::int32			get_classtype() const { return PluginClass::CLASS_TYPE_EFFECT; };
	PluginClass::SuperClassIdC	get_super_class_id() const { return PluginClass::SUPERCLASS_EFFECT; };
	PluginClass::ClassIdC		get_class_id() const { return CLASS_TEXT_EFFECT; };

	const char*					get_name() const { return "Text"; };
	const char*					get_desc() const { return "Text Effects"; };

	const char*					get_author_name() const { return "Mikko \"memon\" Mononen"; };
	const char*					get_copyright_message() const { return "Copyright (c) 2000-2002 Moppi Productions"; };
	const char*					get_url() const { return "http://moppi.inside.org/demopaja/"; };
	const char*					get_help_filename() const { return "res://texteffect.html"; };

	virtual PajaTypes::uint32			get_required_device_driver_count() const
	{
		return 1;
	}

	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx )
	{
		return PajaSystem::CLASS_OPENGL_DEVICEDRIVER;
	}

	// file extension info. (only used in import plugins)
	PajaTypes::uint32			get_ext_count() const { return 0; };
	const char*					get_ext( PajaTypes::uint32 ui32Index ) const { return 0; };
};


// descprion for MOFO import
class MOFOImportDescC : public PluginClass::ClassDescC
{
public:
	MOFOImportDescC() {};
	virtual ~MOFOImportDescC() {};

	void*						create() { return (void*)TextPlugin::MOFOImportC::create_new(); };

	PajaTypes::int32			get_classtype() const { return PluginClass::CLASS_TYPE_FILEIMPORT; };
	PluginClass::SuperClassIdC	get_super_class_id() const { return PluginClass::SUPERCLASS_IMPORT; };
	PluginClass::ClassIdC		get_class_id() const { return CLASS_MOFO_IMPORT; };

	const char*					get_name() const { return "Moppi Font "; };
	const char*					get_desc() const { return "Importer for Moppi Font (.mofo) files"; };

	const char*					get_author_name() const { return "Mikko \"memon\" Mononen"; };
	const char*					get_copyright_message() const { return "Copyright (c) 2000-2002 Moppi Productions"; };
	const char*					get_url() const { return "http://moppi.inside.org/demopaja/"; };
	const char*					get_help_filename() const { return "res://mofoimport.html"; };

	virtual PajaTypes::uint32			get_required_device_driver_count() const
	{
		return 1;
	}

	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx )
	{
		return PajaSystem::CLASS_OPENGL_DEVICEDRIVER;
	}

	// file extension info. (only used in import plugins)
	PajaTypes::uint32			get_ext_count() const { return 1; };
	const char*					get_ext( PajaTypes::uint32 ui32Index ) const
	{
		if( ui32Index == 0 )
			return "mofo";
		return 0;
	}
};


extern TextDescC		g_rTextDesc;
extern MOFOImportDescC	g_rMOFOImportDesc;

#endif // __TEXTPLUGIN_H__
