//
// DX9Boids.h
//
// DX9Boids Plugin
//
// Copyright (c) 2004 memon/moppi productions
//

#ifndef __DX9BoidsPLUGIN_H__
#define __DX9BoidsPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "DX9ViewportC.h"
#include "DX9DeviceC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "ImportableImageI.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

#define	CLASS_DX9BOIDS_EFFECT_NAME		"Boids (DX9)";
#define	CLASS_DX9BOIDS_EFFECT_DESC		"Boids (DX9)";

const PluginClass::ClassIdC	CLASS_DX9BOIDS_EFFECT( 0x08D81474, 0xFEF44C12 );


//////////////////////////////////////////////////////////////////////////
//
//  DX9Boids effect class descriptor.
//

class DX9BoidsDescC : public PluginClass::ClassDescC
{
public:
	DX9BoidsDescC();
	virtual ~DX9BoidsDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};

namespace DX9BoidsPlugin {

//////////////////////////////////////////////////////////////////////////
//
// DX9Boids effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_ROT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_IMAGE = 0,
		ID_ATTRIBUTE_SCALE,
		ID_ATTRIBUTE_OFFSET,
		ID_ATTRIBUTE_WRAP,
		ID_ATTRIBUTE_RESAMPLE,
		ID_ATTRIBUTE_COLOR,
		ID_ATTRIBUTE_OPACITY,
		ID_ATTRIBUTE_BLEND,
		ID_ATTRIBUTE_MASK,
		ATTRIBUTE_COUNT,
	};

	enum AVIPlayerEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	enum ResampleModeE {
		RESAMPLEMODE_BILINEAR = 0,
		RESAMPLEMODE_NEAREST = 1,
	};

	enum WrapModeE {
		WRAPMODE_CLAMP = 0,
		WRAPMODE_REPEAT = 1,
	};

	enum BlendE {
		BLENDMODE_NORMAL = 0,
		BLENDMODE_REPLACE,
		BLENDMODE_ADD,
		BLENDMODE_MULT,
		BLENDMODE_SUB,
		BLENDMODE_LIGHTEN,
		BLENDMODE_DARKEN,
	};

	class DX9BoidsEffectC : public Composition::EffectI
	{
	public:
		static DX9BoidsEffectC*				create_new();
		virtual Edit::DataBlockI*			create();
		virtual Edit::DataBlockI*			create( Edit::EditableI* pOriginal );
		virtual void									copy( Edit::EditableI* pEditable );
		virtual void									restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32			get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*						get_class_name();

		virtual void									set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual PajaTypes::uint32			update_notify( EditableI* pCaller );

		virtual void									initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void									eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::BBox2C			get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool									hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );


	protected:
		DX9BoidsEffectC();
		DX9BoidsEffectC( Edit::EditableI* pOriginal );
		virtual ~DX9BoidsEffectC();

	private:
		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
	};

};	// namespace


extern DX9BoidsDescC			g_rDX9BoidsDesc;


#endif	// __DX9Boids_H__
