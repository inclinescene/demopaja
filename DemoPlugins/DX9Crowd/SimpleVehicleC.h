#ifndef __SIMPLEVEHICLEC_H__
#define __SIMPLEVEHICLEC_H__

#include "PajaTypes.h"
#include "Vector2C.h"
#include <vector>
#include "DX9MeshPlugin.h"

struct ObstacleS
{
	PajaTypes::Vector2C	m_Pos;
	PajaTypes::float32	m_f32Radius;
};


enum VehicleStateE
{
	VEC_TASK_WANDER,
	VEC_TASK_CHAT,
	VEC_TASK_STARE,
	VEC_TASK_LOOKAT,
	VEC_TASK_GROW,
};

struct SimpleVehicleStateS
{
	PajaTypes::Vector2C	m_Pos;
	PajaTypes::Vector2C	m_OldPos;
	PajaTypes::Vector2C	m_Vel;
	PajaTypes::Vector2C	m_Acc;
	PajaTypes::Vector2C	m_Dir;
	PajaTypes::Vector2C	m_Left;
	PajaTypes::Vector2C	m_TopPos;
	PajaTypes::Vector2C	m_TopOldPos;
	PajaTypes::Vector2C	m_TopVel;
	PajaTypes::Vector2C	m_SteeringForce;
	PajaTypes::Vector2C	m_AvoidForce;
	PajaTypes::Vector2C	m_LookAtDir;
	PajaTypes::float32	m_f32MaxForce;
	PajaTypes::float32	m_f32MaxSpeed;
	PajaTypes::float32	m_f32Mass;
	PajaTypes::float32	m_f32Heading;
	PajaTypes::float32	m_f32OldHeading;
	PajaTypes::float32	m_f32HeadingVel;
	PajaTypes::float32	m_f32WanderSteering;
	PajaTypes::int32		m_i32GridX;
	PajaTypes::int32		m_i32GridY;
	PajaTypes::int32		m_i32ChatCounter;
	PajaTypes::int32		m_i32Task;
	PajaTypes::int32		m_i32Age;
	PajaTypes::int32		m_i32BoustCounter;
	PajaTypes::int32		m_i32StareId;
	PajaTypes::Vector2C	m_LookAtPos;
	PajaTypes::int32		m_i32TaskCounter;
	PajaTypes::int32		m_i32AgeAnimCounter;
	PajaTypes::float32	m_f32AgeAnim;
	PajaTypes::float32	m_f32OldAgeAnim;
	PajaTypes::float32	m_f32Height;
	PajaTypes::float32	m_f32OldHeight;
	PajaTypes::float32	m_f32HeightVel;
	PajaTypes::float32	m_f32HeightForce;
};

class SimpleVehicleC
{
public:
	SimpleVehicleC();
	virtual ~SimpleVehicleC();

	void		init( const PajaTypes::Vector2C& Pos, PajaTypes::int32 i32ID, PajaTypes::int32 i32ModelID, PajaTypes::float32 f32Radius );

	const PajaTypes::Vector2C&	get_pos() const;
	const PajaTypes::Vector2C&	get_old_pos() const;
	const PajaTypes::Vector2C&	get_top_pos() const;
	const PajaTypes::Vector2C&	get_top_old_pos() const;
	PajaTypes::int32						get_grid_x() const;
	PajaTypes::int32						get_grid_y() const;
	PajaTypes::int32						get_task() const;
	PajaTypes::int32						get_age() const;
	void												set_age( PajaTypes::int32 i32Age );
	PajaTypes::float32					get_heading() const;
	PajaTypes::float32					get_old_heading() const;
	const SimpleVehicleStateS&	get_state() const;
	void												set_state( const SimpleVehicleStateS& State );
	bool												can_chat() const;
	void												set_chat();
	void												set_look_at( const PajaTypes::Vector2C& LookAt );
	void												set_stare( PajaTypes::int32 i32ID, PajaTypes::int32 i32CallerAge );
	bool												is_bousting() const;
	PajaTypes::int32						get_id() const;
	PajaTypes::int32						get_model_id() const;
	void												set_model( CD3DModelInstance* pModelInst );
	CD3DModelInstance*					get_model();
	PajaTypes::float32					get_age_anim() const;
	PajaTypes::float32					get_old_age_anim() const;
	void												set_height_force( PajaTypes::float32 f32Force );
	PajaTypes::float32					get_height() const;
	PajaTypes::float32					get_old_height() const;

	void		handle_collision( SimpleVehicleC* );
	void		handle_collision( std::vector<ObstacleS>& Obstacles );
	void		update( PajaTypes::float32 f32DT );

protected:

	void		update_grid_pos();

	PajaTypes::Vector2C		wander();

	SimpleVehicleStateS	m_State;
	PajaTypes::int32		m_i32ID;
	PajaTypes::int32		m_i32ModelID;
	CD3DModelInstance*	m_pInstance;
	PajaTypes::float32	m_f32Radius;
};


inline
const PajaTypes::Vector2C&
SimpleVehicleC::get_pos() const
{
	return m_State.m_Pos;
}

inline
const PajaTypes::Vector2C&
SimpleVehicleC::get_old_pos() const
{
	return m_State.m_OldPos;
}

inline
const PajaTypes::Vector2C&
SimpleVehicleC::get_top_pos() const
{
	return m_State.m_TopPos;
}

inline
const PajaTypes::Vector2C&
SimpleVehicleC::get_top_old_pos() const
{
	return m_State.m_TopOldPos;
}

inline
PajaTypes::float32
SimpleVehicleC::get_heading() const
{
	return m_State.m_f32Heading;
}

inline
PajaTypes::float32
SimpleVehicleC::get_old_heading() const
{
	return m_State.m_f32OldHeading;
}

inline
const SimpleVehicleStateS&
SimpleVehicleC::get_state() const
{
	return m_State;
}

inline
PajaTypes::int32
SimpleVehicleC::get_grid_x() const
{
	return m_State.m_i32GridX;
}

inline
PajaTypes::int32
SimpleVehicleC::get_grid_y() const
{
	return m_State.m_i32GridX;
}

inline
void
SimpleVehicleC::set_state( const SimpleVehicleStateS& State )
{
	m_State = State;
}

inline
PajaTypes::float32
SimpleVehicleC::get_age_anim() const
{
	return m_State.m_f32AgeAnim;
}

inline
PajaTypes::float32
SimpleVehicleC::get_old_age_anim() const
{
	return m_State.m_f32OldAgeAnim;
}


#endif