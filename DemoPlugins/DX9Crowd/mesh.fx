//
// Skinned Mesh Effect file 
// Copyright (c) 2000-2002 Microsoft Corporation. All rights reserved.
//

float4 light1Dir = {0.0f, 0.0f, -1.0f, 1.0f};    //light Direction 
float4 light1Diffuse = {0.6f, 0.6f, 0.6f, 1.0f}; // Light Diffuse
float4 lightSpecular = {0.3f, 0.3f, 0.3f, 1.0f}; // Light Diffuse

float4 light2Dir = {0.0f, 0.0f, -1.0f, 1.0f};    //light Direction 
float4 light2Diffuse = {0.6f, 0.6f, 0.6f, 1.0f}; // Light Diffuse

float4 light3Dir = {0.0f, 0.0f, -1.0f, 1.0f};    //light Direction 
float4 light3Diffuse = {0.6f, 0.6f, 0.6f, 1.0f}; // Light Diffuse

float4 MaterialAmbient : MATERIALAMBIENT = {0.1f, 0.1f, 0.1f, 1.0f};
float4 MaterialDiffuse : MATERIALDIFFUSE = {0.8f, 0.8f, 0.8f, 1.0f};

float4 eyePosObj = { 0, 0, 0, 0 };

// Matrix Pallette
static const int MAX_MATRICES = 20;
float4x3    mWorldMatrixArray[MAX_MATRICES] : WORLDMATRIXARRAY;
float4x4    mViewProj : VIEWPROJECTION;

float		focalDist;
float		focalRange;


texture tBaseTex;
texture tBumpTex;


///////////////////////////////////////////////////////
struct VS_INPUT
{
    float4  Pos             : POSITION;
    float4  BlendWeights    : BLENDWEIGHT;
    float4  BlendIndices    : BLENDINDICES;
    float3  Normal          : NORMAL;
    float3  Tangent         : TANGENT;
    float3  Tex0            : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4  Pos			: POSITION;
    float4  Diffuse		: COLOR0;
    float2  Tex0		: TEXCOORD0;
    float3  LightDir	: TEXCOORD1;
    float3  EyeDir		: TEXCOORD2;
    float3  HalfAngle	: TEXCOORD3;
    float	Depth		: TEXCOORD4;
};

struct PS_INPUT
{
    float2	Tex0		: TEXCOORD0;
    float3	LightDir	: TEXCOORD1;
    float3	EyeDir		: TEXCOORD2;
    float3  HalfAngle	: TEXCOORD3;
    float	Depth		: TEXCOORD4;
	float4	Color		: COLOR0;
};

struct PS_OUTPUT
{
	float4 vColor	: COLOR0;
	float4 vDoF		: COLOR1;
};

sampler BaseMapSampler = sampler_state
{
	texture = (tBaseTex);

	MinFilter = Linear;
	MagFilter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
};

sampler BumpMapSampler = sampler_state
{
	texture = (tBumpTex);

	MinFilter = Linear;
	MagFilter = Linear;
	AddressU = Wrap;
	AddressV = Wrap;
};


float3 Diffuse(float3 Normal, float3 Dir)
{
    float CosTheta;
    
    // N.L Clamped
    CosTheta = max(0.0f, dot(Normal, Dir));
       
    // propogate scalar result to vector
    return (CosTheta);
}


VS_OUTPUT VShadeBone(VS_INPUT i, uniform int NumBones)
{
    VS_OUTPUT   o;
    float3      Pos = 0.0f;
    float3      Normal = 0.0f;    
    float       LastWeight = 0.0f;
     
    // Compensate for lack of UBYTE4 on Geforce3
    int4 IndexVector = D3DCOLORtoUBYTE4(i.BlendIndices);

    // cast the vectors to arrays for use in the for loop below
    float BlendWeightsArray[4] = (float[4])i.BlendWeights;
    int   IndexArray[4]        = (int[4])IndexVector;
    
    // calculate the pos/normal using the "normal" weights 
    //        and accumulate the weights to calculate the last weight
    for (int iBone = 0; iBone < NumBones-1; iBone++)
    {
        LastWeight = LastWeight + BlendWeightsArray[iBone];
        
        Pos += mul(i.Pos, mWorldMatrixArray[IndexArray[iBone]]) * BlendWeightsArray[iBone];
        Normal += mul(i.Normal, mWorldMatrixArray[IndexArray[iBone]]) * BlendWeightsArray[iBone];
    }
    LastWeight = 1.0f - LastWeight; 

    // Now that we have the calculated weight, add in the final influence
    Pos += (mul(i.Pos, mWorldMatrixArray[IndexArray[NumBones-1]]) * LastWeight);
    Normal += (mul(i.Normal, mWorldMatrixArray[IndexArray[NumBones-1]]) * LastWeight); 
    
    // transform position from world space into view and then projection space
    float4	PosWVP;
    PosWVP = mul(float4(Pos.xyz, 1.0f), mViewProj);
    o.Pos = PosWVP;
    o.Depth = PosWVP.z;

    // normalize normals
    Normal = normalize(Normal);

    // Shade (Ambient + etc.)
    float3	Light = Diffuse(Normal, light1Dir) * light1Diffuse.xyz;
    Light += Diffuse(Normal, light2Dir) * light2Diffuse.xyz;
    Light += Diffuse(Normal, light3Dir) * light3Diffuse.xyz;
    o.Diffuse.xyz = MaterialAmbient.xyz + Light.xyz; // * MaterialDiffuse.xyz;
    o.Diffuse.w = 1.0f;

    // copy the input texture coordinate through
    o.Tex0 = i.Tex0.xy;

    o.LightDir = float3( 0, 0, 0 );
    o.EyeDir = float3( 0, 0, 0 );
    o.HalfAngle = float3( 0, 0, 0 );

    return o;
}

VS_OUTPUT VShade(VS_INPUT i)
{
    VS_OUTPUT   o;
    float3      Pos = mul( i.Pos, mWorldMatrixArray[0] );
    float3      Normal = mul( i.Normal, mWorldMatrixArray[0] );

    // transform position from world space into view and then projection space
    float4	PosWVP;
    PosWVP = mul(float4(Pos.xyz, 1.0f), mViewProj);
    o.Pos = PosWVP;
    o.Depth = PosWVP.z;
    
	// compute the 3x3 tranform matrix
	// to transform from world space to tangent space
	float3x3 worldToTangentSpace;
	worldToTangentSpace[0] = mul(i.Tangent, mWorldMatrixArray[0]);
	worldToTangentSpace[1] = mul(cross(i.Tangent, i.Normal), mWorldMatrixArray[0]);
	worldToTangentSpace[2] = mul(i.Normal, mWorldMatrixArray[0]);

	float3	LightDir = normalize( mul( worldToTangentSpace, light1Dir.xyz ) ); // L
	o.LightDir.xyz = LightDir;

	float3 EyeDir = normalize( -Pos.xyz );
	EyeDir = mul( worldToTangentSpace, EyeDir );
	o.EyeDir = EyeDir;
	o.HalfAngle = normalize( EyeDir + LightDir );
	
    float3	Light = Diffuse(Normal, light2Dir) * light1Diffuse.xyz;
    Light += Diffuse(Normal, light3Dir) * light3Diffuse.xyz;
    o.Diffuse.xyz = MaterialAmbient.xyz + Light.xyz; // * MaterialDiffuse.xyz;
    o.Diffuse.w = 1.0f;

    o.Tex0  = i.Tex0.xy;

    return o;
}


VS_OUTPUT VShadeBasic(VS_INPUT i)
{
    VS_OUTPUT   o;
    float3      Pos = mul( i.Pos, mWorldMatrixArray[0] );
    float3      Normal = mul( i.Normal, mWorldMatrixArray[0] );

    // transform position from world space into view and then projection space
    float4	PosWVP;
    PosWVP = mul(float4(Pos.xyz, 1.0f), mViewProj);
    o.Pos = PosWVP;
    o.Depth = PosWVP.z;

    // Shade (Ambient + etc.)
    float3	Light = Diffuse(Normal, light1Dir) * light1Diffuse.xyz;
    Light += Diffuse(Normal, light2Dir) * light2Diffuse.xyz;
    Light += Diffuse(Normal, light3Dir) * light3Diffuse.xyz;
    o.Diffuse.xyz = MaterialAmbient.xyz + Light.xyz; // * MaterialDiffuse.xyz;
    o.Diffuse.w = 1.0f;

    o.Tex0  = i.Tex0.xy;
    o.LightDir = float3( 0, 0, 0 );
    o.EyeDir = float3( 0, 0, 0 );
    o.HalfAngle = float3( 0, 0, 0 );

    return o;
}

float4 PBasic( PS_INPUT v ) : COLOR
{
	// get the height using the tex coords
	float3 col = v.Color * tex2D( BaseMapSampler, v.Tex0 ).xyz;
	return float4( col, 1 );
}

PS_OUTPUT PBasicDOF( PS_INPUT v ) : COLOR
{
	PS_OUTPUT o;

	// Blur factor based on the distance from focal plane
	float blur = saturate( abs( v.Depth - focalDist ) * focalRange);

	float3 col = v.Color * tex2D( BaseMapSampler, v.Tex0 ).xyz;

	o.vColor = float4( col, 1 );
	o.vDoF = float4( v.Depth, blur, 0, 0 );
	return o;
}


float4 PBump( PS_INPUT v ) : COLOR
{
	// get the height using the tex coords
	float height = tex2D( BumpMapSampler, v.Tex0 ).a;

	// scale and bias factors	
	float scale = 0.04;
	float bias = -0.02;

	// calculate displacement	
	float displacement = (height * scale) + bias;
	
	float3 uv2 = float3(v.Tex0, 1);
	
	// calculate the new tex coord to use for normal and diffuse
	float2 newTexCoord = ((v.EyeDir * displacement) + uv2).xy;
	
	float3 LightDir = normalize( v.LightDir );
	float3 HalfAngle = normalize( v.HalfAngle );
	float3 bumpNormal = 2 * (tex2D( BumpMapSampler, newTexCoord ) - 0.5);
	float3 light = saturate( dot( bumpNormal, LightDir ) ) * light1Diffuse + v.Color;
	float4 diffuse = tex2D( BaseMapSampler, newTexCoord );
	float3 specular = pow( saturate( dot( bumpNormal, HalfAngle ) ), 32 ) * lightSpecular;
	float3 col = diffuse.xyz * light + specular * diffuse.w;
//	float3 col = diffuse * saturate( dot( bumpNormal, LightDir ) ) * light1Diffuse;
		
	return float4( col, 1 );
}

PS_OUTPUT PBumpDOF( PS_INPUT v ) : COLOR
{
	PS_OUTPUT o;
	
	// get the height using the tex coords
	float height = tex2D( BumpMapSampler, v.Tex0 ).a;

	// scale and bias factors	
	float scale = 0.04;
	float bias = -0.02;

	// calculate displacement	
	float displacement = (height * scale) + bias;
	
	float3 uv2 = float3(v.Tex0, 1);
	
	// calculate the new tex coord to use for normal and diffuse
	float2 newTexCoord = ((v.EyeDir * displacement) + uv2).xy;
	
	float3 LightDir = normalize( v.LightDir );
	float3 HalfAngle = normalize( v.HalfAngle );
	float3 bumpNormal = 2 * (tex2D( BumpMapSampler, newTexCoord ) - 0.5);
	float3 light = saturate( dot( bumpNormal, LightDir ) ) * light1Diffuse + v.Color;
	float4 diffuse = tex2D( BaseMapSampler, newTexCoord );
	float3 specular = pow( saturate( dot( bumpNormal, HalfAngle ) ), 32 ) * lightSpecular;
	float3 col = diffuse.xyz * light + specular * diffuse.w;
//	float3 col = diffuse * saturate( dot( bumpNormal, LightDir ) ) * light1Diffuse;

	// Blur factor based on the distance from focal plane
	float blur = saturate( abs( v.Depth - focalDist ) * focalRange);

	o.vColor = float4( col, 1 );
	o.vDoF = float4( v.Depth, blur, 0, 0 );
	return o;
}



int CurNumBones = 2;
VertexShader vsArray[4] = { compile vs_1_1 VShadeBone(1), 
                            compile vs_1_1 VShadeBone(2),
                            compile vs_1_1 VShadeBone(3),
                            compile vs_1_1 VShadeBone(4)
                          };

int NumRenderTarget = 0;
PixelShader psArrayBump[2] = {	compile ps_2_0 PBump(),
							compile ps_2_0 PBumpDOF()
						};

PixelShader psArrayBasic[2] = {	compile ps_2_0 PBasic(),
							compile ps_2_0 PBasicDOF()
						};

//////////////////////////////////////
// Techniques specs follow
//////////////////////////////////////
technique tBone
{
    pass p0
    {
        VertexShader = (vsArray[CurNumBones]);
		PixelShader = (psArrayBasic[NumRenderTarget]);
    }
}

technique tBump
{
    pass p0
    {
        VertexShader = compile vs_1_1 VShade();
		PixelShader = (psArrayBump[NumRenderTarget]);
    }
}

technique tBasic
{
    pass p0
    {
        VertexShader = compile vs_1_1 VShadeBasic();
		PixelShader = (psArrayBasic[NumRenderTarget]);
    }
}

