//-------------------------------------------------------------------------
//
// File:		RandomMT.cpp
// Desc:		Marsenne Twister Random generator.
// Author:	memon <memon@inside.org>
//					based on Marsenne Twister implmentation by Takuji Nishimura
//					http://www.math.keio.ac.jp/~matumoto/emt.html
//
//-------------------------------------------------------------------------
//	This source code is part of the game Fibble.
//  Made by Kylpyamme Design for Crytek.
//-------------------------------------------------------------------------

#ifndef __FIBBLE_RANDOMMT_H__
#define __FIBBLE_RANDOMMT_H__

#include "PajaTypes.h"

//! Seeds random number generator.
void		seed_gen_rand( PajaTypes::uint32 ui32Seed );
//! Returns 32-bit random number.
PajaTypes::uint32		gen_rand();
//! Returns floating point random number.
PajaTypes::float64	float_gen_rand();


#endif