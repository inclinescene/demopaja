#ifndef __JPEGPLUGIN_H__
#define __JPEGPLUGIN_H__

#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "ClassDescC.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "LayerC.h"
#include "ParamI.h"
#include "ImportableI.h"
#include "ImportableImageI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "OpenGLDeviceC.h"

const	PluginClass::ClassIdC	CLASS_JPEG_IMPORT( 0, 201 );


namespace JpegImport {

	const	PluginClass::ClassIdC	CLASS_IMAGE_EFFECT( 0, 100 );

	class JPEGImportC : public Import::ImportableImageI
	{
	public:
		static JPEGImportC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		// the interface
		// returns the name of the file this importable refers to
		virtual const char*				get_filename();
		virtual void					set_filename( const char* szName );
		// loads the file
		virtual bool					load_file( const char* szName, PajaSystem::DemoInterfaceC* pInterface );
		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		// interface for this class
		virtual PajaTypes::float32		get_width();
		virtual PajaTypes::float32		get_height();
		virtual PajaTypes::int32		get_data_width();
		virtual PajaTypes::int32		get_data_height();
		virtual PajaTypes::int32		get_data_pitch();
		virtual PajaTypes::BBox2C&	get_tex_coord_bounds();
		virtual PajaTypes::int32		get_data_bpp();
		virtual PajaTypes::uint8*		get_data();
		virtual void					bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Stage, PajaTypes::uint32 ui32Properties );
		virtual const char*				get_info();
		virtual PluginClass::ClassIdC	get_default_effect();

		virtual PajaTypes::int32		get_duration();
		virtual PajaTypes::float32		get_start_label();
		virtual PajaTypes::float32		get_end_label();

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

	protected:
		JPEGImportC();
		JPEGImportC( Edit::EditableI* pOriginal );
		virtual ~JPEGImportC();

		bool						uncompress();
		void						upload_texture();

	private:
		PajaTypes::uint32		m_ui32TextureId;
		PajaTypes::uint8*		m_pData;
		PajaTypes::uint8*		m_pCompressedData;
		PajaTypes::int32		m_i32CompressedSize;
		PajaTypes::int32		m_i32Width, m_i32Height;
		PajaTypes::int32		m_i32Bpp;
		PajaTypes::BBox2C		m_rTexBounds;

		std::string				m_sFileName;
	};

}; // namspace


// descprion for TGA import
class JPEGImportDescC : public PluginClass::ClassDescC
{
public:
	JPEGImportDescC() {};
	virtual ~JPEGImportDescC() {};

	void*						create() { return (void*)JpegImport::JPEGImportC::create_new(); };

	PajaTypes::int32			get_classtype() const { return PluginClass::CLASS_TYPE_FILEIMPORT; };
	PluginClass::SuperClassIdC	get_super_class_id() const { return PluginClass::SUPERCLASS_IMAGE; };
	PluginClass::ClassIdC		get_class_id() const { return CLASS_JPEG_IMPORT; };

	const char*					get_name() const { return "JPEG Image"; };
	const char*					get_desc() const { return "Importer for JPEG (.JPG) images"; };

	const char*					get_author_name() const { return "Mikko \"memon\" Mononen"; };
	const char*					get_copyright_message() const { return "Copyright (c) 2000 Moppi Productions"; };
	const char*					get_url() const { return "http://www.moppi.inside.org/demopaja/"; };
	const char*					get_help_filename() const { return "res://jpegimp.html"; };

	virtual PajaTypes::uint32			get_required_device_driver_count() const
	{
		return 1;
	}

	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx )
	{
		return PajaSystem::CLASS_OPENGL_DEVICEDRIVER;
	}

	// file extension info. (only used in import plugins)
	PajaTypes::uint32			get_ext_count() const { return 3; };
	const char*					get_ext( PajaTypes::uint32 ui32Index ) const
	{
		if( ui32Index == 0 )
			return "jpg";
		else if( ui32Index == 1 )
			return "jpeg";
		else if( ui32Index == 2 )
			return "jpe";
		return 0;
	}
};

extern JPEGImportDescC	g_rJPEGImportDesc;

#endif // __TESTPLUGIN_H__
