#ifndef __FLARE3DPLUGIN_H__
#define __FLARE3DPLUGIN_H__

#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "ClassDescC.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "LayerC.h"
#include "ParamI.h"
#include "ImportableI.h"
#include "ImportableImageI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "TimeContextC.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "ImportInterfaceC.h"
#include "OpenGLInterfaceC.h"
#include "..\3dengine\engine.h"


const	PluginClass::ClassIdC	CLASS_FLARE3D_EFFECT( 0, 106 );



enum TransGizmoParamsE {
	TRANS_PARAM_POS = 0,
	TRANS_PARAM_PIVOT,
	TRANS_PARAM_SCALE,
	TRANS_PARAM_COUNT,
};


class TransGizmoC : public Composition::GizmoI
{
public:

	static TransGizmoC*				create_new( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	virtual Edit::DataBlockI*		create();
	virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
	virtual void					copy( Edit::EditableI* pEditable );
	virtual void					restore( Edit::EditableI* pEditable );

	virtual PajaTypes::int32		get_parameter_count();
	virtual Composition::ParamI*	get_parameter( PajaTypes::int32 i32Index );

	virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

	void							init();
	MASImportC*						get_file();
	PajaTypes::Vector2C				get_pos( PajaTypes::int32 i32Time );
	PajaTypes::Vector2C				get_pivot( PajaTypes::int32 i32Time );
	PajaTypes::Vector2C				get_scale( PajaTypes::int32 i32Time );

protected:
	TransGizmoC();
	TransGizmoC( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	TransGizmoC( Edit::EditableI* pOriginal );
	virtual ~TransGizmoC();

private:
	Composition::ParamVector2C*	m_pParamPos;
	Composition::ParamVector2C*	m_pParamScale;
	Composition::ParamVector2C*	m_pParamPivot;
};



enum AttribGizmoParamsE {
	ATTRIB_PARAM_WIDTH = 0,
	ATTRIB_PARAM_HEIGHT,
	ATTRIB_PARAM_RENDERMODE,
	ATTRIB_PARAM_CAMERA,
	ATTRIB_PARAM_FPS,
	ATTRIB_PARAM_CLEARZ,
	ATTRIB_PARAM_FLARESIZE,
	ATTRIB_PARAM_COLOR,
	ATTRIB_PARAM_FILE,
	ATTRIB_PARAM_IMAGE,
	ATTRIB_PARAM_COUNT,
};


class AttribGizmoC : public Composition::GizmoI
{
public:

	static AttribGizmoC*			create_new( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	virtual Edit::DataBlockI*		create();
	virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
	virtual void					copy( Edit::EditableI* pEditable );
	virtual void					restore( Edit::EditableI* pEditable );

	virtual PajaTypes::int32		get_parameter_count();
	virtual Composition::ParamI*	get_parameter( PajaTypes::int32 i32Index );

	virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

	virtual void					update_notify( PajaTypes::uint32 ui32ID, PajaTypes::int32 i32Time );

	void							init();
	MASImportC*						get_file();
	Import::ImportableImageI*		get_image();
	PajaTypes::int32				get_render_mode( PajaTypes::int32 i32Time );
	PajaTypes::int32				get_width( PajaTypes::int32 i32Time );
	PajaTypes::int32				get_height( PajaTypes::int32 i32Time );
	CameraC*						get_camera( PajaTypes::int32 i32Time );
	PajaTypes::int32				get_fps( PajaTypes::int32 i32Time );
	PajaTypes::int32				get_test_z( PajaTypes::int32 i32Time );
	PajaTypes::ColorC				get_color( PajaTypes::int32 i32Time );
	PajaTypes::float32				get_flare_size( PajaTypes::int32 i32Time );

protected:
	AttribGizmoC();
	AttribGizmoC( Composition::EffectI* pParent, PajaTypes::uint32 ui32Id );
	AttribGizmoC( Edit::EditableI* pOriginal );
	virtual ~AttribGizmoC();

private:
	Composition::ParamIntC*		m_pParamRenderMode;
	Composition::ParamIntC*		m_pParamWidth;
	Composition::ParamIntC*		m_pParamHeight;
	Composition::ParamIntC*		m_pParamCamera;
	Composition::ParamIntC*		m_pParamFps;
	Composition::ParamIntC*		m_pParamTestZ;
	Composition::ParamFileC*	m_pParamFile;
	Composition::ParamFileC*	m_pParamImage;
	Composition::ParamColorC*	m_pParamColor;
	Composition::ParamFloatC*	m_pParamFlareSize;
};



class FlareEffectC : public Composition::EffectI
{
public:
	static FlareEffectC*			create_new();
	virtual Edit::DataBlockI*		create();
	virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
	virtual void					copy( Edit::EditableI* pEditable );
	virtual void					restore( Edit::EditableI* pEditable );

	virtual PajaTypes::int32		get_gizmo_count();
	virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

	virtual PluginClass::ClassIdC	get_class_id();
	virtual const char*				get_class_name();

	virtual void					set_default_file( Import::FileHandleC* pHandle );
	virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

	virtual void					initialize( PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext );

	virtual void					do_frame( PajaSystem::DeviceContextC* pContext );
	virtual void					eval_state( PajaTypes::int32 i32Time, PajaSystem::TimeContextC* pTimeContext  );
	virtual PajaTypes::BBox2C		get_bbox();

	virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

	virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

	virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

protected:
	FlareEffectC();
	FlareEffectC( Edit::EditableI* pOriginal );
	virtual ~FlareEffectC();

private:

	virtual void	render_item( PajaSystem::OpenGLInterfaceC* pInterface, ScenegraphItemI* pItem, PajaTypes::int32 i32Frame );
	virtual void	render_normal( PajaSystem::OpenGLInterfaceC* pInterface, GeomListC* pGList );

	TransGizmoC*		m_pTransGizmo;
	AttribGizmoC*		m_pAttribGizmo;
	PajaTypes::Matrix2C	m_rTM;
	PajaTypes::BBox2C	m_rBBox;
	PajaTypes::Vector2C	m_rVertices[4];
	PajaTypes::int32	m_i32RenderMode;
	PajaTypes::int32	m_i32Time;
	PajaTypes::int32	m_i32Frame;
	PajaTypes::ColorC	m_rColor;
	PajaTypes::float32	m_f32FlareSize;
	bool				m_bHasTexture;
};


// descprion test effect
class FlareDescC : public PluginClass::ClassDescC
{
public:
	FlareDescC() {};
	virtual ~FlareDescC() {};

	void*						create() { return (void*)FlareEffectC::create_new(); };

	PajaTypes::int32			get_classtype() const { return PluginClass::CLASS_TYPE_EFFECT; };
	PluginClass::SuperClassIdC	get_super_classid() const { return PluginClass::SUPERCLASS_EFFECT; };
	PluginClass::ClassIdC		get_classid() const { return CLASS_FLARE3D_EFFECT; };

	const char*					get_name() const { return "Flare 3D"; };
	const char*					get_desc() const { return "Flare 3D - Renders 3D scenes as flares"; };

	const char*					get_author_name() const { return "Mikko \"memon\" Mononen"; };
	const char*					get_copyright_message() const { return "Copyright (c) 2000 Moppi Productions"; };
	const char*					get_url() const { return "http://www.moppi.inside.org/demopaja/"; };
	const char*					get_help_filename() const { return "flare3d.html"; };

	bool						check_device_support( PajaSystem::DeviceContextC* pContext )
	{
		if( pContext->query_interface( PajaSystem::INTERFACE_OPENGL ) )
			return true;
		return false;
	}

	// file extension info. (only used in import plugins)
	PajaTypes::uint32			get_ext_count() const { return 0; };
	const char*					get_ext( PajaTypes::uint32 ui32Index ) const { return 0; };
};



#endif // __FLARE3DPLUGIN_H__
