/* (c) 2002 Nathaniel Hoffman, Kenneth J. Mitchell and Arcot J. Preetham */

#ifndef _AtmosphereC_H
#define _AtmosphereC_H

#include <stdio.h>
#include <d3dx9math.h>
 
// Various AtmosphereC parameters.
typedef enum {
	ATM_BETA_RAY_MULTIPLIER,
	ATM_BETA_MIE_MULTIPLIER,
	ATM_INSCATTERING_MULTIPLIER,
	ATM_EXTINCTION_MULTIPLIER,
	ATM_HGG,
} AtmosphereParamsE;


class AtmosphereC {
	
public:
	AtmosphereC(); 
	virtual ~AtmosphereC() {};

	virtual void SetParam(AtmosphereParamsE param, float fValue);
	virtual float GetParam(AtmosphereParamsE param);
	
	virtual D3DXVECTOR3 GetBetaRayleigh() {return m_vBetaRay;};
	virtual D3DXVECTOR3 GetBetaDashRayleigh() {return m_vBetaDashRay;};

	virtual D3DXVECTOR3 GetBetaMie() {return m_vBetaMie;};
	virtual D3DXVECTOR3 GetBetaDashMie() {return m_vBetaDashMie;};
	
	virtual void Interpolate(AtmosphereC *one, AtmosphereC *two, float f);

	virtual void Dump2(FILE *f);
	virtual void Read2(FILE *f);
	virtual void Finalize() {};

	virtual AtmosphereC operator + ( const AtmosphereC&) const;
    AtmosphereC operator * ( float ) const;
    friend AtmosphereC operator * ( float , const AtmosphereC& );




private:
	float m_fHGg;		// g value in Henyey Greenstein approximation function.

	// Ideally, the following multipliers should all be 1.0, but provided here for study.
	// The final color of an object in AtmosphereC is sum of inscattering term and extinction term. 
	float m_fInscatteringMultiplier;	// Multiply inscattering term with this factor.
	float m_fExtinctionMultiplier;		// Multiply extinction term with this factor.
	float m_fBetaRayMultiplier;			// Multiply  Rayleigh scattering coefficient with this factor
	float m_fBetaMieMultiplier;			// Multiply  Mie scattering coefficient with this factor

	D3DXVECTOR3 m_vBetaRay;				// Rayleigh scattering coeff
	D3DXVECTOR3 m_vBetaDashRay;			// Rayleigh Angular scattering coeff without phase term.
	D3DXVECTOR3 m_vBetaMie;				// Mie scattering coeff
	D3DXVECTOR3 m_vBetaDashMie;			// Mie Angular scattering coeff without phase term.

	void CalculateScatteringConstants();


};

AtmosphereC operator * ( float , const AtmosphereC& );

#endif