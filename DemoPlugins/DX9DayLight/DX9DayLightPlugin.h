//
// DX9DayLight.h
//
// DX9DayLight Plugin
//
// Copyright (c) 2004 memon/moppi productions
//

#ifndef __DX9DayLightPLUGIN_H__
#define __DX9DayLightPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "DX9ViewportC.h"
#include "DX9DeviceC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "ImportableImageI.h"
#include "DX9MeshPlugin.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

#define	CLASS_DX9DAYLIGHT_EFFECT_NAME	"DayLight (DX9)"
#define	CLASS_DX9DAYLIGHT_EFFECT_DESC	"DayLight (DX9)"

const PluginClass::ClassIdC	CLASS_DX9DAYLIGHT_EFFECT( 0x72563C2F, 0x22CB4FD4 );


//////////////////////////////////////////////////////////////////////////
//
//  DX9DayLight effect class descriptor.
//

class DX9DayLightDescC : public PluginClass::ClassDescC
{
public:
	DX9DayLightDescC();
	virtual ~DX9DayLightDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};

namespace DX9DayLightPlugin {

//////////////////////////////////////////////////////////////////////////
//
// DX9DayLight effect class.
//

/*
	m_EditSunIntensityScale = 100;
	float inten1 = m_pSun->GetIntensity()/m_EditSunIntensityScale;
	m_SliderSunIntensity = 100 * inten1;

	m_SliderSunTheta = (int) (m_pSun->GetSunTheta() * 100.0f/D3DXToRadian(90));
	m_SliderSunPhi = (int) (m_pSun->GetSunPhi() * 100.0f/D3DXToRadian(360));
	if (m_SliderSunPhi > 100) m_SliderSunPhi -= 100;

	m_SliderTime = (int)(100.0f * ThetaPhi2Time(m_pSun->GetSunTheta(), m_pSun->GetSunPhi()));

	m_SliderBetaRay = (int)(100.0 * m_pAtmosphere->GetParam(eAtmBetaRayMultiplier));
	m_SliderBetaMie = (int)(100.0 * m_pAtmosphere->GetParam(eAtmBetaMieMultiplier));
	m_EditBetaRayScale = 1.0f;
	m_EditBetaMieScale = 1.0f;
	m_SliderHGg = (int)(100.0 * m_pAtmosphere->GetParam(eAtmHGg));
	
	m_CheckMultExtinction = (bool)(m_pAtmosphere->GetParam(eAtmExtinctionMultiplier));
	m_SliderMultInscattering = (int)(100.0 * m_pAtmosphere->GetParam(eAtmInscatteringMultiplier));

*/
	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_PRIMARY_LIGHT = 0,
		ID_ATTRIBUTE_FILL_LIGHT_1,
		ID_ATTRIBUTE_FILL_LIGHT_2,
		ID_ATTRIBUTE_FILE,
		ATTRIBUTE_COUNT,
	};

	enum TimeGizmoParamsE {
		ID_TIME_FRAME = 0,
		TIME_COUNT,
	};

	enum AVIPlayerEffectGizmosE {
		ID_GIZMO_ATTRIB,
		ID_GIZMO_TIME,
		GIZMO_COUNT,
	};

	class DX9DayLightEffectC : public Composition::EffectI
	{
	public:
		static DX9DayLightEffectC*				create_new();
		virtual Edit::DataBlockI*			create();
		virtual Edit::DataBlockI*			create( Edit::EditableI* pOriginal );
		virtual void									copy( Edit::EditableI* pEditable );
		virtual void									restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32			get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*						get_class_name();

		virtual void									set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual PajaTypes::uint32			update_notify( EditableI* pCaller );

		virtual void									initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void									eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::BBox2C			get_bbox();

		virtual LightC*								get_primary_light();
		virtual LightC*								get_fill_light_1();
		virtual LightC*								get_fill_light_2();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool									hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );


	protected:
		DX9DayLightEffectC();
		DX9DayLightEffectC( Edit::EditableI* pOriginal );
		virtual ~DX9DayLightEffectC();

	private:
		Composition::AutoGizmoC*	m_pAttGizmo;
		Composition::AutoGizmoC*	m_pTimeGizmo;

		PajaTypes::Matrix2C				m_rTM;
		PajaTypes::BBox2C					m_rBBox;
		LightC*										m_pPrimaryLight;
		LightC*										m_pFill1Light;
		LightC*										m_pFill2Light;
	};

};	// namespace


extern DX9DayLightDescC			g_rDX9DayLightDesc;


#endif	// __DX9DayLight_H__
