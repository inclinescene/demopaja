/* (c) 2002 Nathaniel Hoffman, Kenneth J. Mitchell and Arcot J. Preetham */

#ifndef _SUN_H
#define _SUN_H

#include <stdio.h>
#include <d3dx9math.h>

class SunC {
	
public:
	SunC(float fTheta = 0, float fPhi = 0,
		float fIntensity = 1.0); 
								// Theta is angle from zenith direction (Y axis)
								// Theta = 0 (Pos y), Theta = 180 (Neg Y)
								// Phi is angle in CW from x axis in XZ coordinate system.
								// eg. E(pos X) = 0, N(pos Z) = 90, W = 180, S = 270.
								

	virtual ~SunC() {};

				
	virtual D3DXVECTOR4	GetDirection();
	virtual float				GetIntensity();
	virtual D3DXVECTOR4	GetColor();
	virtual D3DXVECTOR4	GetColorAndIntensity();
	virtual D3DXVECTOR4	GetColorWithIntensity();

	virtual void				SetSunIntensity(float fIntensity) {m_fIntensity = fIntensity;};
	virtual void				SetSunThetaPhi(float fTheta, float fPhi) {m_fTheta = fTheta; m_fPhi = fPhi;} ;
	virtual float				GetSunTheta() {return m_fTheta;};
	virtual float				GetSunPhi() {return m_fPhi;};

	virtual void Interpolate(SunC *one, SunC *two, float f);

	virtual void Dump2(FILE *f);
	virtual void Read2(FILE *f);
	virtual void Finalize() {};

	virtual SunC operator + ( const SunC&) const;
    SunC operator * ( float ) const;
    friend SunC operator * ( float , const SunC& );


private:
	float m_fTheta, m_fPhi;
	float m_fIntensity;

	D3DXVECTOR4 ComputeAttenuation(float fTheta, int nTurbidity = 2);

};


#endif
