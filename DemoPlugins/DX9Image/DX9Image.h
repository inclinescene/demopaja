//
// DX9Image.h
//
// DX9Image Plugin
//
// Copyright (c) 2004 memon/moppi productions
//

#ifndef __DX9IMAGEPLUGIN_H__
#define __DX9IMAGEPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "DX9ViewportC.h"
#include "DX9DeviceC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "ImportableImageI.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_DX9IMAGE_EFFECT( 0x9F40FC1F, 0x23D34790 );
const	PluginClass::ClassIdC	CLASS_DX9DDS_IMPORT( 0xC2A0D3B3, 0x310F4C4E );


//////////////////////////////////////////////////////////////////////////
//
//  DX9 .DDS importer class descriptor.
//

class DX9DDSImportDescC : public PluginClass::ClassDescC
{
public:
	DX9DDSImportDescC();
	virtual ~DX9DDSImportDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  Image effect class descriptor.
//

class DX9ImageDescC : public PluginClass::ClassDescC
{
public:
	DX9ImageDescC();
	virtual ~DX9ImageDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};

namespace DX9ImagePlugin {

//////////////////////////////////////////////////////////////////////////
//
// DDS Importer class.
//

	class DX9DDSImportC : public Import::ImportableImageI
	{
	public:
		static DX9DDSImportC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual const char*				get_filename();
		virtual void					set_filename( const char* szName );
		virtual bool					load_file( const char* szName, PajaSystem::DemoInterfaceC* pInterface );
		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		// The importable image interface.
		virtual PajaTypes::float32		get_width();
		virtual PajaTypes::float32		get_height();
		virtual PajaTypes::int32		get_data_width();
		virtual PajaTypes::int32		get_data_height();
		virtual PajaTypes::int32		get_data_pitch();
		virtual PajaTypes::BBox2C&	get_tex_coord_bounds();
		virtual PajaTypes::int32		get_data_bpp();
		virtual PajaTypes::uint8*		get_data();

		virtual void					bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Stage, PajaTypes::uint32 ui32Properties );
		virtual const char*				get_info();
		virtual PluginClass::ClassIdC	get_default_effect();

		virtual PajaTypes::int32		get_duration();
		virtual PajaTypes::float32		get_start_label();
		virtual PajaTypes::float32		get_end_label();

		virtual IDirect3DTexture9*		get_texture();

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

	protected:
		DX9DDSImportC();
		DX9DDSImportC( Edit::EditableI* pOriginal );
		virtual ~DX9DDSImportC();

		bool						init_texture();

	private:
		IDirect3DTexture9*	m_pD3DTexture;
		PajaTypes::uint8*		m_pData;
		PajaTypes::uint32		m_ui32DataSize;
		PajaTypes::int32		m_i32Width, m_i32Height;
		PajaTypes::int32		m_i32Bpp;
		PajaTypes::BBox2C		m_rTexBounds;
		std::string					m_sFileName;
	};


//////////////////////////////////////////////////////////////////////////
//
// Image effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_ROT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_IMAGE = 0,
		ID_ATTRIBUTE_SCALE,
		ID_ATTRIBUTE_OFFSET,
		ID_ATTRIBUTE_WRAP,
		ID_ATTRIBUTE_RESAMPLE,
		ID_ATTRIBUTE_COLOR,
		ID_ATTRIBUTE_OPACITY,
		ID_ATTRIBUTE_BLEND,
		ID_ATTRIBUTE_BAND,
		ID_ATTRIBUTE_BAND_AMOUNT,
		ATTRIBUTE_COUNT,
	};

	enum AVIPlayerEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	enum ResampleModeE {
		RESAMPLEMODE_BILINEAR = 0,
		RESAMPLEMODE_NEAREST = 1,
	};

	enum WrapModeE {
		WRAPMODE_CLAMP = 0,
		WRAPMODE_REPEAT = 1,
	};

	enum BlendE {
		BLENDMODE_NORMAL = 0,
		BLENDMODE_REPLACE,
		BLENDMODE_ADD,
		BLENDMODE_MULT,
		BLENDMODE_SUB,
		BLENDMODE_LIGHTEN,
		BLENDMODE_DARKEN,
	};

	class DX9ImageEffectC : public Composition::EffectI
	{
	public:
		static DX9ImageEffectC*		create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual PajaTypes::uint32		update_notify( EditableI* pCaller );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		DX9ImageEffectC();
		DX9ImageEffectC( Edit::EditableI* pOriginal );
		virtual ~DX9ImageEffectC();

	private:

		void			init_effect();

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];

		LPD3DXEFFECT									m_pEffect;
		LPDIRECT3DVERTEXDECLARATION9  m_pVertDecl;
	};

};	// namespace


extern DX9ImageDescC			g_rDX9ImageDesc;
extern DX9DDSImportDescC	g_rDX9DDSImport;


#endif	// __DX9IMAAGE_H__
