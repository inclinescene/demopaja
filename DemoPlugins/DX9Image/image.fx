// Number of outer filter taps

float fadeBlack = 0.0;
float fadeWhite = 0.0;

// transformations
matrix matWorldViewProj;

texture tTexture;

///////////////////////////////////////////////////////////////////////////////

struct VS_INPUT
{
	float3 vPos: POSITION;
	float4 vDiffuse : COLOR0;
	float2 vTexCoord: TEXCOORD;
};

struct VS_OUTPUT
{
	float4 vPos: POSITION;
	float4 vDiffuse : COLOR0;
	float2 vTexCoord: TEXCOORD;
};

///////////////////////////////////////////////////////////////////////////////

struct PS_INPUT
{
	float2 vTexCoord: TEXCOORD;
	float4 vDiffuse : COLOR0;
};

sampler ColorTextureSampler = sampler_state
{
	texture = (tTexture);

/*	MinFilter = Linear;
	MagFilter = Linear;
	AddressU = Clamp;
	AddressV = Clamp;*/
};

///////////////////////////////////////////////////////////////////////////////

// Screen space vertex shader
VS_OUTPUT Image_vs(VS_INPUT v)
{
	VS_OUTPUT o;

	o.vPos = mul( float4( v.vPos, 1 ), matWorldViewProj);

	// Output tex. coordinates
	o.vTexCoord = v.vTexCoord;
	o.vDiffuse = v.vDiffuse;

	return o;
}

float4 ColorTexture_ps(PS_INPUT v) : COLOR
{
	// Get center sample
	float4 colorSum = tex2D( ColorTextureSampler, v.vTexCoord ) * v.vDiffuse;
	colorSum.xyz = lerp( colorSum.xyz, float3( 1, 1, 1 ), fadeWhite );
	colorSum.xyz = lerp( colorSum.xyz, float3( 0, 0, 0 ), fadeBlack );
	return colorSum;
}


// DoF filter shader
float4 ColorOnly_ps(PS_INPUT v) : COLOR
{
	// Get center sample
	float4 colorSum = v.vDiffuse;
	colorSum.xyz = lerp( colorSum.xyz, float3( 1, 1, 1 ), fadeWhite );
	colorSum.xyz = lerp( colorSum.xyz, float3( 0, 0, 0 ), fadeBlack );
	return colorSum;
}

technique ColorTextureF
{
	pass P0
	{
		VertexShader = compile vs_2_0 Image_vs();
		PixelShader = compile ps_2_0 ColorTexture_ps();
		CullMode = NONE;
	}
}

technique ColorOnlyF
{
	pass P0
	{
		VertexShader = compile vs_2_0 Image_vs();
		PixelShader = compile ps_2_0 ColorOnly_ps();
		CullMode = NONE;
	}
}
