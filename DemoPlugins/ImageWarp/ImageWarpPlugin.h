//
// ImageWarpPlugin.h
//
// Image Warp Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

#ifndef __IMAGEWARPPLUGIN_H__
#define __IMAGEWARPPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_WARP_EFFECT( 0x7B772BAB, 0xF2284E0B );


//////////////////////////////////////////////////////////////////////////
//
//  Image Warp effect class descriptor.
//

class WarpDescC : public PluginClass::ClassDescC
{
public:
	WarpDescC();
	virtual ~WarpDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};

namespace WarpPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The Warp effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_COLOR = 0,
		ID_ATTRIBUTE_RENDERMODE,
		ID_ATTRIBUTE_FILTERMODE,
		ID_ATTRIBUTE_SCALE,
		ID_ATTRIBUTE_OFFSET,
		ID_ATTRIBUTE_ROTATION,
		ID_ATTRIBUTE_REPEAT,
		ID_ATTRIBUTE_FILE,
		ID_ATTRIBUTE_FILE_RENDERMODE,
		ATTRIBUTE_COUNT,
	};

	enum WarpEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	enum FilterModeE {
		FILTERMODE_LINEAR = 0,
		FILTERMODE_NEAREST = 1,
	};

	enum RenderModeE {
		RENDERMODE_NORMAL = 0,
		RENDERMODE_ADD = 1,
		RENDERMODE_MULT = 2,
	};

	class WarpEffectC : public Composition::EffectI
	{
	public:
		static WarpEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		WarpEffectC();
		WarpEffectC( Edit::EditableI* pOriginal );
		virtual ~WarpEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;
		Composition::AutoGizmoC*	m_pFukiGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];

		PajaTypes::uint32	m_ui32TexId;
		PajaTypes::uint32	m_ui32TexWidth;
		PajaTypes::uint32	m_ui32TexHeight;
		PajaTypes::int32	m_i32TexCapturedWidth;
		PajaTypes::int32	m_i32TexCapturedHeight;

		PajaTypes::ColorC	m_rFillColor;
		PajaTypes::int32	m_i32RenderMode;
		PajaTypes::int32	m_i32FilterMode;
		PajaTypes::int32	m_i32RepeatCount;
		PajaTypes::Vector2C	m_rWarpScale;
		PajaTypes::Vector2C	m_rWarpOffset;
		PajaTypes::float32	m_f32WarpRotation;

		PajaTypes::int32	m_i32TexSize;
		PajaTypes::int32	m_i32ForceCopy;

//		PajaTypes::int32	m_i32Time;
	};

};	// namespace

extern WarpDescC	g_rWarpDesc;


#endif	// __IMAGEWARPPLUGIN_H__
