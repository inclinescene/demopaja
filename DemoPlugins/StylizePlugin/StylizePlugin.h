//
// StylizePlugin.h
//
// Stylize effects Plugin
//
// Copyright (c) 2000-2003 memon/moppi productions
//


#ifndef __STYLIZEPLUGIN_H__
#define __STYLIZEPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLViewportC.h"
#include "OpenGLDeviceC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "ImportableImageI.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const	PluginClass::ClassIdC	CLASS_GRADIENT_EFFECT( 0xC33F7DEE, 0x6C1B47B7 );


//////////////////////////////////////////////////////////////////////////
//
//  Gradient effect class descriptor.
//

class GradientDescC : public PluginClass::ClassDescC
{
public:
	GradientDescC();
	virtual ~GradientDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


namespace GradientPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The Gradient effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_ROT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_IMAGE = 0,
		ID_ATTRIBUTE_SCALE,
		ID_ATTRIBUTE_OFFSET,
		ID_ATTRIBUTE_WRAP,
		ID_ATTRIBUTE_RESAMPLE,
		ID_ATTRIBUTE_WEIGHT,
		ID_ATTRIBUTE_MASK,
		ATTRIBUTE_COUNT,
	};

	enum GradientGizmoParamsE {
		ID_GRADIENT_COLOR_A = 0,
		ID_GRADIENT_OPACITY_A,
		ID_GRADIENT_COLOR_B,
		ID_GRADIENT_OPACITY_B,
		ID_GRADIENT_MIDPOINT,
		ID_GRADIENT_STEEPNESS,
		GRADIENT_COUNT,
	};

	enum ImageEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		ID_GIZMO_GRADIENT,
		GIZMO_COUNT,
	};

	enum ResampleModeE {
		RESAMPLEMODE_BILINEAR = 0,
		RESAMPLEMODE_NEAREST = 1,
	};

	enum WrapModeE {
		WRAPMODE_CLAMP = 0,
		WRAPMODE_REPEAT = 1,
	};

	enum BlendE {
		BLENDMODE_NORMAL = 0,
		BLENDMODE_REPLACE,
		BLENDMODE_ADD,
		BLENDMODE_MULT,
		BLENDMODE_SUB,
		BLENDMODE_LIGHTEN,
		BLENDMODE_DARKEN,
	};

	class GradientEffectC : public Composition::EffectI
	{
	public:
		static GradientEffectC*		create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual PajaTypes::uint32		update_notify( EditableI* pCaller );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		GradientEffectC();
		GradientEffectC( Edit::EditableI* pOriginal );
		virtual ~GradientEffectC();

	private:

		void			init_textures();
		void			release_textures();

		void			update_gradient( const PajaTypes::ColorC& rColA, const PajaTypes::ColorC& rColB, PajaTypes::float32 f32MidPoint, PajaTypes::float32 f32Steepness );

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;
		Composition::AutoGizmoC*	m_pGradGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];

		static PajaTypes::uint32	m_ui32WhiteTextureID;
		static PajaTypes::uint32	m_ui32BlackTextureID;
		static PajaTypes::uint32	m_ui32GradientTextureID;
		static PajaTypes::int32		m_i32TextureRefCount;
		bool											m_bTextureRef;
	};

};	// namespace


extern GradientDescC	g_rGradientDesc;


#endif	// __IMAGEPLUGIN_H__
