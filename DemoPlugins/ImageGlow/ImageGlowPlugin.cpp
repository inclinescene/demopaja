//
// ImageGlowPlugin.cpp
//
// Image Glow Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

//#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <stdio.h>

// Demopaja headers
#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "ClassDescC.h"
#include "ImportableI.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "ImageGlowPlugin.h"
#include "FileIO.h"
#include "AutoGizmoC.h"
#include "ImportableImageI.h"

using namespace PajaTypes;
using namespace PluginClass;
using namespace PajaSystem;
using namespace Edit;
using namespace Composition;
using namespace Import;
using namespace FileIO;

using namespace ImageGlowPlugin;


//////////////////////////////////////////////////////////////////////////
//
//  Image Glow effect class descriptor.
//

ImageGlowDescC::ImageGlowDescC()
{
	// empty
}

ImageGlowDescC::~ImageGlowDescC()
{
	// empty
}

void*
ImageGlowDescC::create()
{
	return (void*)ImageGlowEffectC::create_new();
}

int32
ImageGlowDescC::get_classtype() const
{
	return CLASS_TYPE_EFFECT;
}

SuperClassIdC
ImageGlowDescC::get_super_class_id() const
{
	return SUPERCLASS_EFFECT;
}

ClassIdC
ImageGlowDescC::get_class_id() const
{
	return CLASS_IMAGEGLOW_EFFECT;
};

const char*
ImageGlowDescC::get_name() const
{
	return "Image Glow";
}

const char*
ImageGlowDescC::get_desc() const
{
	return "Image Glow Effect";
}

const char*
ImageGlowDescC::get_author_name() const
{
	return "Mikko \"memon\" Mononen";
}

const char*
ImageGlowDescC::get_copyright_message() const
{
	return "Copyright (c) 2001 Moppi Productions";
}

const char*
ImageGlowDescC::get_url() const
{
	return "http://moppi.inside.org/demopaja/";
}

const char*
ImageGlowDescC::get_help_filename() const
{
	return "res://imageglow.html";
}

uint32
ImageGlowDescC::get_required_device_driver_count() const
{
	return 1;
}

const ClassIdC&
ImageGlowDescC::get_required_device_driver( uint32 ui32Idx )
{
	return PajaSystem::CLASS_OPENGL_DEVICEDRIVER;
}


uint32
ImageGlowDescC::get_ext_count() const
{
	return 0;
}

const char*
ImageGlowDescC::get_ext( uint32 ui32Index ) const
{
	return 0;
}


//////////////////////////////////////////////////////////////////////////
//
// Global descriptors, which will be returned to the Demopaja.
//

ImageGlowDescC	g_rImageGlowDesc;

#ifndef DEMOPAJAPLAYER

//////////////////////////////////////////////////////////////////////////
//
// The DLL
//

BOOL APIENTRY
DllMain( HANDLE hModule, DWORD ulReasonForCall, LPVOID lpReserved )
{
    switch( ulReasonForCall )
	{
		case DLL_PROCESS_ATTACH:
		case DLL_THREAD_ATTACH:
		case DLL_THREAD_DETACH:
		case DLL_PROCESS_DETACH:
			break;
    }
    return TRUE;
}


//
// Returns number of classes inside this plugin DLL.
//

__declspec( dllexport )
int32
get_classdesc_count()
{
	return 1;
}


//
// Returns class descriptors of the plugin classes.
//

__declspec( dllexport )
ClassDescC*
get_classdesc( int32 i )
{
	if( i == 0 )
		return &g_rImageGlowDesc;
	return 0;
}


//
// Returns the API version this DLL was made with.
//

__declspec( dllexport )
int32
get_api_version()
{
	return DEMOPAJA_VERSION;
}

//
// Returns the DLL name.
//

__declspec( dllexport )
char*
get_dll_name()
{
	return "ImageGlow.dll - Image Glow Effect plugin (c)2000 memon/moppi productions";
}

#endif



//////////////////////////////////////////////////////////////////////////
//
// The effect
//

ImageGlowEffectC::ImageGlowEffectC()
{
	//
	// Create Transform gizmo.
	//
	m_pTraGizmo = AutoGizmoC::create_new( this, "Transform", ID_GIZMO_TRANS );

	m_pTraGizmo->add_parameter(	ParamVector2C::create_new( m_pTraGizmo, "Position", Vector2C(), ID_TRANSFORM_POS,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_ABS_POSITION, PARAM_ANIMATABLE ) );
	
	m_pTraGizmo->add_parameter(	ParamVector2C::create_new( m_pTraGizmo, "Pivot", Vector2C(), ID_TRANSFORM_PIVOT,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_REL_POSITION | PARAM_STYLE_WORLD_SPACE, PARAM_ANIMATABLE ) );
	
	m_pTraGizmo->add_parameter(	ParamVector2C::create_new( m_pTraGizmo, "Scale", Vector2C( 1, 1 ), ID_TRANSFORM_SCALE,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_PERCENT, PARAM_ANIMATABLE, Vector2C(), Vector2C(), 0.01f ) );


	//
	// Create Attributes gizmo.
	//
	m_pAttGizmo = AutoGizmoC::create_new( this, "Attributes", ID_GIZMO_ATTRIB );

	// Sample count
	m_pAttGizmo->add_parameter(	ParamFloatC::create_new( m_pAttGizmo, "Sample Count", 0.5f, ID_ATTRIBUTE_SAMPLECOUNT,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_PERCENT, PARAM_ANIMATABLE, 0, 1, 0.01f ) );

	// Compare mode
	ParamIntC*	pCompMode = ParamIntC::create_new( m_pAttGizmo, "Compare Mode", 0, ID_ATTRIBUTE_COMPAREMODE, PARAM_STYLE_COMBOBOX, PARAM_ANIMATABLE, 0, 1 );
	pCompMode->add_label( 0, "Greater" );
	pCompMode->add_label( 1, "Less" );
	m_pAttGizmo->add_parameter( pCompMode );

	// Treshold
	m_pAttGizmo->add_parameter(	ParamFloatC::create_new( m_pAttGizmo, "Treshold", 0.5f, ID_ATTRIBUTE_TRESHOLD,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_PERCENT, PARAM_ANIMATABLE, 0, 1, 0.01f ) );

	// randomness
	m_pAttGizmo->add_parameter(	ParamFloatC::create_new( m_pAttGizmo, "Randomness", 0, ID_ATTRIBUTE_RANDOMNESS,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_PERCENT, PARAM_ANIMATABLE, 0, 1, 0.01f ) );

	// size
	m_pAttGizmo->add_parameter(	ParamFloatC::create_new( m_pAttGizmo, "Glow Size", 1, ID_ATTRIBUTE_GLOWSIZE,
						PARAM_STYLE_EDITBOX, PARAM_ANIMATABLE, 0, 0, 1 ) );

	// feedback
	m_pAttGizmo->add_parameter(	ParamFloatC::create_new( m_pAttGizmo, "Feedback", 0, ID_ATTRIBUTE_FEEDBACK,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_PERCENT, PARAM_ANIMATABLE, 0, 1, 0.01f ) );

	// Render mode
	ParamIntC*	pRenderMode = ParamIntC::create_new( m_pAttGizmo, "Render Mode", 0, ID_ATTRIBUTE_RENDERMODE, PARAM_STYLE_COMBOBOX, PARAM_ANIMATABLE, 0, 2 );
	pRenderMode->add_label( 0, "Normal" );
	pRenderMode->add_label( 1, "Add" );
	pRenderMode->add_label( 2, "Mult" );
	m_pAttGizmo->add_parameter( pRenderMode );

	// Color
	m_pAttGizmo->add_parameter(	ParamColorC::create_new( m_pAttGizmo, "Color", ColorC( 1, 1, 1, 1 ), ID_ATTRIBUTE_COLOR,
		PARAM_STYLE_COLORPICKER_RGBA, PARAM_ANIMATABLE ) );

	// Image
	m_pAttGizmo->add_parameter(	ParamFileC::create_new( m_pAttGizmo, "Image", SUPERCLASS_IMAGE, NULL_CLASSID, ID_ATTRIBUTE_IMAGE ) );

}

ImageGlowEffectC::ImageGlowEffectC( EditableI* pOriginal ) :
	EffectI( pOriginal ),
	m_pTraGizmo( 0 ),
	m_pAttGizmo( 0 )
{
	// Empty. The parameters are not created in the clone constructor.
}

ImageGlowEffectC::~ImageGlowEffectC()
{
	// Return if this is a clone.
	if( get_original() )
		return;

	// Release gizmos.
	m_pTraGizmo->release();
	m_pAttGizmo->release();
}

ImageGlowEffectC*
ImageGlowEffectC::create_new()
{
	return new ImageGlowEffectC;
}

DataBlockI*
ImageGlowEffectC::create()
{
	return new ImageGlowEffectC;
}

DataBlockI*
ImageGlowEffectC::create( EditableI* pOriginal )
{
	return new ImageGlowEffectC( pOriginal );
}

void
ImageGlowEffectC::copy( EditableI* pEditable )
{
	EffectI::copy( pEditable );

	// Make deep copy.
	ImageGlowEffectC*	pEffect = (ImageGlowEffectC*)pEditable;
	m_pTraGizmo->copy( pEffect->m_pTraGizmo );
	m_pAttGizmo->copy( pEffect->m_pAttGizmo );
}

void
ImageGlowEffectC::restore( EditableI* pEditable )
{
	EffectI::restore( pEditable );

	// Make shallow copy.
	ImageGlowEffectC*	pEffect = (ImageGlowEffectC*)pEditable;
	m_pTraGizmo = pEffect->m_pTraGizmo;
	m_pAttGizmo = pEffect->m_pAttGizmo;
}

int32
ImageGlowEffectC::get_gizmo_count()
{
	// Return number of gizmos inside this effect.
	return GIZMO_COUNT;
}

GizmoI*
ImageGlowEffectC::get_gizmo( PajaTypes::int32 i32Index )
{
	// Returns specified gizmo.
	// Since the ID's are zero based, we can use them as indices.
	switch( i32Index ) {
	case ID_GIZMO_TRANS:
		return m_pTraGizmo;
	case ID_GIZMO_ATTRIB:
		return m_pAttGizmo;
	}

	return 0;
}

ClassIdC
ImageGlowEffectC::get_class_id()
{
	// Return the class ID. Should be same as in the class descriptor.
	return CLASS_IMAGEGLOW_EFFECT;
}

const char*
ImageGlowEffectC::get_class_name()
{
	// Return the class name. Should be same as in the class descriptor.
	return "Image Glow";
}

void
ImageGlowEffectC::set_default_file( int32 i32Time, FileHandleC* pHandle )
{
	// empty
}

ParamI*
ImageGlowEffectC::get_default_param( int32 i32Param )
{
	// Return specified default parameter.
	if( i32Param == DEFAULT_PARAM_POSITION )
		return m_pTraGizmo->get_parameter( ID_TRANSFORM_POS );
	else if( i32Param == DEFAULT_PARAM_SCALE )
		return m_pTraGizmo->get_parameter( ID_TRANSFORM_SCALE );
	else if( i32Param == DEFAULT_PARAM_PIVOT )
		return m_pTraGizmo->get_parameter( ID_TRANSFORM_PIVOT );
	return 0;
}

void
ImageGlowEffectC::initialize( uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface )
{
	EffectI::initialize( ui32Reason, pInterface );

	DeviceContextC* pContext = m_pDemoInterface->get_device_context();
	TimeContextC* pTimeContext = m_pDemoInterface->get_time_context();

	if( ui32Reason == INIT_DEVICE_CHANGED ) {

		OpenGLDeviceC*	pDevice = (OpenGLDeviceC*)pContext->query_interface( CLASS_OPENGL_DEVICEDRIVER );
		if( !pDevice )
			return;

		if( pDevice->get_state() == DEVICE_STATE_SHUTTINGDOWN ) {
			// Delete textures
		}

	}
}


static	uint32	g_ui32Seed = 1;

inline
float32
frand()
{
	return (float32)((uint32)(g_ui32Seed = 0x015a4e35 * g_ui32Seed + 1) >> 16) * 2.0f * (1.0f / 65536.0f) - 1.0f;
}


void
ImageGlowEffectC::eval_state( int32 i32Time )
{
	if( !m_pDemoInterface )
		return;

	DeviceContextC* pContext = m_pDemoInterface->get_device_context();
	TimeContextC* pTimeContext = m_pDemoInterface->get_time_context();

	int32		i, j;
	Matrix2C	rPosMat, rRotMat, rScaleMat, rPivotMat;
	Vector2C	rScale;
	Vector2C	rPos;
	Vector2C	rPivot;

	// Get parameters which affect the transformation.
	((ParamVector2C*)m_pTraGizmo->get_parameter( ID_TRANSFORM_POS ))->get_val( i32Time, rPos );
	((ParamVector2C*)m_pTraGizmo->get_parameter( ID_TRANSFORM_PIVOT ))->get_val( i32Time, rPivot );
	((ParamVector2C*)m_pTraGizmo->get_parameter( ID_TRANSFORM_SCALE ))->get_val( i32Time, rScale );

	// Calculate transformation matrix.
	rPivotMat.set_trans( rPivot );
	rPosMat.set_trans( rPos );
	rScaleMat.set_scale( rScale ) ;
	m_rTM = rPivotMat * rScaleMat * rPosMat;

	float32		f32Width = 25;
	float32		f32Height = 25;
	Vector2C	rMin, rMax;
	Vector2C	rVec;

	// Calcualte vertices of the rectangle.
	m_rVertices[0][0] = -f32Width;		// top-left
	m_rVertices[0][1] = -f32Height;

	m_rVertices[1][0] =  f32Width;		// top-right
	m_rVertices[1][1] = -f32Height;

	m_rVertices[2][0] =  f32Width;		// bottom-right
	m_rVertices[2][1] =  f32Height;

	m_rVertices[3][0] = -f32Width;		// bottom-left
	m_rVertices[3][1] =  f32Height;

	// Calculate bounding box
	for( i = 0; i < 4; i++ ) {
		rVec = m_rTM * m_rVertices[i];
		m_rVertices[i] = rVec;

		if( !i )
			rMin = rMax = rVec;
		else {
			if( rVec[0] < rMin[0] ) rMin[0] = rVec[0];
			if( rVec[1] < rMin[1] ) rMin[1] = rVec[1];
			if( rVec[0] > rMax[0] ) rMax[0] = rVec[0];
			if( rVec[1] > rMax[1] ) rMax[1] = rVec[1];
		}
	}

	// Store bounding box.
	m_rBBox[0] = rMin;
	m_rBBox[1] = rMax;


	PajaTypes::float32				f32SampleCount;
	PajaTypes::float32				f32Treshold;
	PajaTypes::float32				f32GlowSize;
	PajaTypes::float32				f32Randomness;
	PajaTypes::float32				f32Feedback;
	PajaTypes::int32				i32RenderMode;
	PajaTypes::int32				i32CompMode;
	PajaTypes::ColorC				rColor;


	// Get SampleCount
	((ParamFloatC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_SAMPLECOUNT ))->get_val( i32Time, f32SampleCount );

	// Get Treshold
	((ParamFloatC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_TRESHOLD ))->get_val( i32Time, f32Treshold );

	// Get Randomness
	((ParamFloatC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_RANDOMNESS ))->get_val( i32Time, f32Randomness );

	// Get glow size
	((ParamFloatC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_GLOWSIZE ))->get_val( i32Time, f32GlowSize );

	// Get Color
	((ParamColorC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_COLOR ))->get_val( i32Time, rColor );

	// Get Rendermode
	((ParamIntC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_RENDERMODE ))->get_val( i32Time, i32RenderMode );

	// Get compare mode
	((ParamIntC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_COMPAREMODE ))->get_val( i32Time, i32CompMode );

	// Get feedback
	((ParamFloatC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_FEEDBACK ))->get_val( i32Time, f32Feedback );

	g_ui32Seed = (uint32)i32Time;


	//
	// Draw effect
	//

	// Get the OpenGL device.
	OpenGLDeviceC*	pDevice = (OpenGLDeviceC*)pContext->query_interface( CLASS_OPENGL_DEVICEDRIVER );
	if( !pDevice )
		return;

	// Get the OpenGL viewport.
	OpenGLViewportC*	pViewport = (OpenGLViewportC*)pDevice->query_interface( GRAPHICSDEVICE_VIEWPORT_INTERFACE );
	if( !pViewport )
		return;

	// Set orthographic projection.
	pViewport->set_ortho( m_rBBox, m_rBBox[0][0], m_rBBox[1][0], m_rBBox[1][1], m_rBBox[0][1] );

	float32	f32Ratio = 1.0f;
	if( m_rBBox.width() != 0 )
		f32Ratio = m_rBBox.height() / m_rBBox.width();

	int32	i32SampleWidth = (int32)(80.0f * (0.2f + f32SampleCount * 0.8f));
	int32	i32SampleHeight = (int32)(80.0f * (0.2f + f32SampleCount * 0.8f) * f32Ratio);

	if( i32SampleWidth < 2 )
		i32SampleWidth = 2;
	if( i32SampleHeight < 2 )
		i32SampleHeight = 2;

	if( m_rSamples.size() < (i32SampleWidth * i32SampleHeight) ) {
		m_rSamples.resize( i32SampleWidth * i32SampleHeight );
	}

	//
	// Capture
	//
	uint32	ui32Pixel;
	uint32	ui32Idx = 0;

	uint32	ui32SaveSeed = g_ui32Seed;

	int32	i32X, i32Y;
	rMin = pViewport->layout_to_client( m_rBBox[0] );
	rMax = pViewport->layout_to_client( m_rBBox[1] );
	Vector2C	rSize = rMax - rMin;
	float32		f32MaxRandX = (rSize[0] / i32SampleWidth) * 0.6f;
	float32		f32MaxRandY = (rSize[1] / i32SampleHeight) * 0.6f;

	for( i = 0; i < i32SampleHeight; i++ ) {
		for( j = 0; j < i32SampleWidth; j++ ) {
			rPos[0] = rMin[0] + rSize[0] * ((float32)j / (float32)(i32SampleWidth - 1));
			rPos[1] = rMin[1] + rSize[1] * ((float32)i / (float32)(i32SampleHeight - 1));

			if( f32Randomness > 0 ) {
				rPos[0] += f32MaxRandX * f32Randomness * frand();
				rPos[1] += f32MaxRandY * f32Randomness * frand();
			}

			i32X = (int32)rPos[0];
			i32Y = (int32)rPos[1];
			glReadPixels( i32X, i32Y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &ui32Pixel );

			ColorC	rCol;
			rCol.convert_from_uint8( ui32Pixel & 0xff, (ui32Pixel >> 8) & 0xff, (ui32Pixel >> 16) & 0xff );

			m_rSamples[ui32Idx] *= f32Feedback;
			m_rSamples[ui32Idx] += rCol;

			ui32Idx++;
		}
	}


	//
	// Render
	//

	rMin = m_rBBox[0]; //Vector2C( -25, -25 );
	rMax = m_rBBox[1]; //Vector2C( 25, 25 );
	rSize = rMax - rMin;

	glEnable( GL_BLEND );
	glDisable( GL_LIGHTING );
	glDisable( GL_DEPTH_TEST );
	glDisable( GL_TEXTURE_2D );



	// If an image is provided, use it instead of capturing the screen.
	ImportableImageI*	pImpImage = 0;
	FileHandleC*		pImageHandle;
	int32				i32FileTime;
	((ParamFileC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_IMAGE ))->get_file( i32Time, pImageHandle, i32FileTime );
	if( pImageHandle )
		pImpImage = (ImportableImageI*)pImageHandle->get_importable();
	if( pImpImage ) {
		pImpImage->eval_state( i32FileTime );
		glEnable( GL_TEXTURE_2D );
		pImpImage->bind_texture( pDevice, 0, IMAGE_CLAMP | IMAGE_LINEAR );
	}


	if( i32RenderMode == RENDERMODE_NORMAL )
		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	else if( i32RenderMode == RENDERMODE_ADD )
		glBlendFunc( GL_SRC_ALPHA, GL_ONE );
	else
		glBlendFunc( GL_DST_COLOR, GL_ONE_MINUS_SRC_ALPHA );

	glDepthMask( GL_FALSE );

	glBegin( GL_QUADS );

	ui32Idx = 0;
	ColorC	rCol;

	f32MaxRandX = (rSize[0] / i32SampleWidth) * 0.6f;
	f32MaxRandY = (rSize[1] / i32SampleHeight) * 0.6f;

	g_ui32Seed = ui32SaveSeed;

	for( i = 0; i < i32SampleHeight; i++ ) {
		for( j = 0; j < i32SampleWidth; j++ ) {

			rPos[0] = rMin[0] + rSize[0] * ((float32)j / (float32)(i32SampleWidth - 1));
			rPos[1] = rMin[1] + rSize[1] * ((float32)i / (float32)(i32SampleHeight - 1));

			if( f32Randomness > 0 ) {
				rPos[0] += f32MaxRandX * f32Randomness * frand();
				rPos[1] += f32MaxRandY * f32Randomness * frand();
			}

			rCol = m_rSamples[ui32Idx];

			bool	bPass = false;

			if( i32CompMode == 0 ) {
				if( (rCol[0] >= f32Treshold) || (rCol[1] >= f32Treshold) || (rCol[2] >= f32Treshold) )
					bPass = true;
			}
			else {
				if( (rCol[0] <= f32Treshold) || (rCol[1] <= f32Treshold) || (rCol[2] <= f32Treshold) )
					bPass = true;
			}

			if( bPass ) {

				rCol = rCol * rColor;

				if( i32RenderMode == RENDERMODE_MULT ) {
					rCol[0] *= rCol[3];
					rCol[1] *= rCol[3];
					rCol[2] *= rCol[3];
				}

				glColor4fv( rCol );
				
				glTexCoord2f( 0, 0 );
				glVertex2f( rPos[0] - f32GlowSize, rPos[1] - f32GlowSize );

				glTexCoord2f( 1, 0 );
				glVertex2f( rPos[0] + f32GlowSize, rPos[1] - f32GlowSize );
				
				glTexCoord2f( 1, 1 );
				glVertex2f( rPos[0] + f32GlowSize, rPos[1] + f32GlowSize );
				
				glTexCoord2f( 0, 1 );
				glVertex2f( rPos[0] - f32GlowSize, rPos[1] + f32GlowSize );
			}

			ui32Idx++;
		}
	}

	glEnd();

	glDepthMask( GL_TRUE );
}

BBox2C
ImageGlowEffectC::get_bbox()
{
	// Return the bounding box.
	return m_rBBox;
}

const Matrix2C&
ImageGlowEffectC::get_transform_matrix() const
{
	// Return the trnasformation matrix.
	return m_rTM;
}

bool
ImageGlowEffectC::hit_test( const Vector2C& rPoint )
{
	// Point in polygon test.
	// from c.g.a FAQ
	int		i, j;
	bool	bInside = false;

	for( i = 0, j = 4 - 1; i < 4; j = i++ ) {
		if( ( ((m_rVertices[i][1] <= rPoint[1]) && (rPoint[1] < m_rVertices[j][1])) ||
			((m_rVertices[j][1] <= rPoint[1]) && (rPoint[1] < m_rVertices[i][1])) ) &&
			(rPoint[0] < (m_rVertices[j][0] - m_rVertices[i][0]) * (rPoint[1] - m_rVertices[i][1]) / (m_rVertices[j][1] - m_rVertices[i][1]) + m_rVertices[i][0]) )

			bInside = !bInside;
	}

	return bInside;
}


enum ImageGlowEffectChunksE {
	CHUNK_IMAGEGLOW_BASE =			0x1000,
	CHUNK_IMAGEGLOW_TRANSGIZMO =	0x2000,
	CHUNK_IMAGEGLOW_ATTRIBGIZMO =	0x3000,
};

const uint32	IMAGEGLOW_VERSION = 1;

uint32
ImageGlowEffectC::save( SaveC* pSave )
{
	uint32	ui32Error = IO_OK;

	// EffectI base class
	pSave->begin_chunk( CHUNK_IMAGEGLOW_BASE, IMAGEGLOW_VERSION );
		ui32Error = EffectI::save( pSave );
	pSave->end_chunk();

	// Transform
	pSave->begin_chunk( CHUNK_IMAGEGLOW_TRANSGIZMO, IMAGEGLOW_VERSION );
		ui32Error = m_pTraGizmo->save( pSave );
	pSave->end_chunk();

	// Attribute
	pSave->begin_chunk( CHUNK_IMAGEGLOW_ATTRIBGIZMO, IMAGEGLOW_VERSION );
		ui32Error = m_pAttGizmo->save( pSave );
	pSave->end_chunk();

	return ui32Error;
}

uint32
ImageGlowEffectC::load( LoadC* pLoad )
{
	uint32	ui32Error = IO_OK;

	while( (ui32Error = pLoad->open_chunk()) == IO_OK ) {

		switch( pLoad->get_chunk_id() ) {
		case CHUNK_IMAGEGLOW_BASE:
			// EffectI base class
			if( pLoad->get_chunk_version() == IMAGEGLOW_VERSION )
				ui32Error = EffectI::load( pLoad );
			break;

		case CHUNK_IMAGEGLOW_TRANSGIZMO:
			// Transform
			if( pLoad->get_chunk_version() == IMAGEGLOW_VERSION )
				ui32Error = m_pTraGizmo->load( pLoad );
			break;

		case CHUNK_IMAGEGLOW_ATTRIBGIZMO:
			// Attribute
			if( pLoad->get_chunk_version() == IMAGEGLOW_VERSION )
				ui32Error = m_pAttGizmo->load( pLoad );
			break;

		default:
			assert( 0 );
		}

		pLoad->close_chunk();

		if( ui32Error != IO_OK && ui32Error != IO_END )
			return ui32Error;
	}

	return ui32Error;
}
