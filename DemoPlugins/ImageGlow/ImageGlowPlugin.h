//
// ImageGlowPlugin.h
//
// Image Glow Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

#ifndef __IMAGEGLOWPLUGIN_H__
#define __IMAGEGLOWPLUGIN_H__

#include <vector>
#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "ImportableImageI.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_IMAGEGLOW_EFFECT( 0x3B41017E, 0xB9804CD4 );


//////////////////////////////////////////////////////////////////////////
//
//  Image Glow effect class descriptor.
//

class ImageGlowDescC : public PluginClass::ClassDescC
{
public:
	ImageGlowDescC();
	virtual ~ImageGlowDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


namespace ImageGlowPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The Image Glow effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_SAMPLECOUNT = 0,
		ID_ATTRIBUTE_TRESHOLD,
		ID_ATTRIBUTE_RANDOMNESS,
		ID_ATTRIBUTE_GLOWSIZE,
		ID_ATTRIBUTE_RENDERMODE,
		ID_ATTRIBUTE_COLOR,
		ID_ATTRIBUTE_IMAGE,
		ID_ATTRIBUTE_COMPAREMODE,
		ID_ATTRIBUTE_FEEDBACK,
		ATTRIBUTE_COUNT,
	};

	enum AVIPlayerEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	enum RenderModeE {
		RENDERMODE_NORMAL = 0,
		RENDERMODE_ADD = 1,
		RENDERMODE_MULT = 2,
	};

	class ImageGlowEffectC : public Composition::EffectI
	{
	public:
		static ImageGlowEffectC*		create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		ImageGlowEffectC();
		ImageGlowEffectC( Edit::EditableI* pOriginal );
		virtual ~ImageGlowEffectC();

	private:

		Composition::AutoGizmoC*		m_pTraGizmo;
		Composition::AutoGizmoC*		m_pAttGizmo;

		PajaTypes::Matrix2C				m_rTM;
		PajaTypes::BBox2C				m_rBBox;
		PajaTypes::Vector2C				m_rVertices[4];

		std::vector<PajaTypes::ColorC>	m_rSamples;
	};

};	// namespace


extern ImageGlowDescC	g_rImageGlowDesc;


#endif	// __IMAGEGLOWPLUGIN_H__
