#ifndef __CONTFLOATC_H__
#define __CONTFLOATC_H__


#include "PajaTypes.h"
#include "UndoC.h"
#include "KeyC.h"
#include "FileIO.h"
#include "ControllerC.h"


class KeyFloatC
{
public:
	KeyFloatC();
	virtual ~KeyFloatC();

	virtual PajaTypes::float32	get_value() const;
	virtual void				set_value( PajaTypes::float32 f32Val );

	virtual PajaTypes::float32	get_in_tan() const;
	virtual void				set_in_tan( PajaTypes::float32 f32Val );

	virtual PajaTypes::float32	get_out_tan() const;
	virtual void				set_out_tan( PajaTypes::float32 f32Val );

	virtual PajaTypes::int32	get_time();
	virtual void				set_time( PajaTypes::int32 i32Time );

	// TCB and ease
	virtual PajaTypes::float32	get_ease_in();
	virtual PajaTypes::float32	get_ease_out();
	virtual PajaTypes::float32	get_tens();
	virtual PajaTypes::float32	get_cont();
	virtual PajaTypes::float32	get_bias();
	virtual void				set_ease_in( PajaTypes::float32 f32Val );
	virtual void				set_ease_out( PajaTypes::float32 f32Val );
	virtual void				set_tens( PajaTypes::float32 f32Val );
	virtual void				set_cont( PajaTypes::float32 f32Val );
	virtual void				set_bias( PajaTypes::float32 f32Val );

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

private:
	PajaTypes::float32	m_f32Val;
	PajaTypes::float32	m_f32InTan;
	PajaTypes::float32	m_f32OutTan;
	PajaTypes::float32	m_f32EaseIn, m_f32EaseOut;
	PajaTypes::float32	m_f32Tens, m_f32Cont, m_f32Bias;
	PajaTypes::int32	m_i32Time;
};


class ContFloatC
{
public:
	ContFloatC( PajaTypes::uint32 ui32Type = Composition::KEY_SMOOTH );
	virtual ~ContFloatC();

	virtual PajaTypes::int32	get_key_count();
	virtual KeyFloatC*			get_key( PajaTypes::int32 i32Index );
	virtual void				del_key( PajaTypes::int32 i32Index );
	virtual KeyFloatC*			add_key();

	virtual void				set_start_ort( PajaTypes::uint32 ui32Ort );
	virtual void				set_end_ort( PajaTypes::uint32 ui32Ort );
	virtual PajaTypes::uint32	get_start_ort() const;
	virtual PajaTypes::uint32	get_end_ort() const;

	virtual PajaTypes::int32	get_min_time() const;
	virtual PajaTypes::int32	get_max_time() const;

	virtual void				sort_keys();
	virtual void				prepare();

	virtual PajaTypes::float32	get_value( PajaTypes::int32 i32Time ) const;

	virtual PajaTypes::uint32	get_type() const;

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

protected:
	// tool methods
	void						compute_hermite_basis( PajaTypes::float32 f32U, PajaTypes::float32* pV ) const;
	PajaTypes::float32			ease( PajaTypes::float32 f32U, PajaTypes::float32 f32A, PajaTypes::float32 f32B ) const;
	void						comp_first_deriv( PajaTypes::uint32 ui32CurIndex, PajaTypes::uint32 ui32NextIndex );
	void						comp_last_deriv( PajaTypes::uint32 ui32CurIndex, PajaTypes::uint32 ui32NextIndex );
	void						comp_2key_deriv( PajaTypes::uint32 ui32CurIndex, PajaTypes::uint32 ui32NextIndex );
	void						comp_middle_deriv( PajaTypes::uint32 ui32PrevIndex, PajaTypes::uint32 ui32Index, PajaTypes::uint32 ui32NextIndex );

private:

	std::vector<KeyFloatC*>		m_rKeys;
	PajaTypes::uint32			m_ui32StartOrt;
	PajaTypes::uint32			m_ui32EndOrt;
	PajaTypes::uint32			m_ui32Type;			// interpolation type
};

#endif