#ifndef OOBU_MESH3DC_H
#define OOBU_MESH3DC_H

#include "PajaTypes.h"
#include "GeomListC.h"
#include "Vector3C.h"
#include "FileIO.h"
#include <vector>
#include <string>



class Mesh3DC
{
public:
	Mesh3DC();
	Mesh3DC( Mesh3DC& rMesh );
	virtual ~Mesh3DC();
		
	//Geometry handling...
	virtual PajaTypes::uint32	get_geomlist_count();													// Size of geomlists vector
	virtual GeomListC*			add_geomlist( GeomListC* pGeomList = NULL );
	virtual void				insert_geomlist( PajaTypes::uint32 ui32Index, GeomListC* pGeomList = 0 );	// Insert a new geom list to vector
	virtual void				replace_geomlist( PajaTypes::uint32 ui32Index, GeomListC* pGeomList = 0 );	// Replaces a geom list from vector
	virtual void				erase_geomlist( PajaTypes::uint32 ui32Index );								// Remove a geom list from vector
	virtual GeomListC*			get_geomlist( PajaTypes::uint32 ui32Index );								// Returns a geomlist from vector

	virtual void				update_bounds();
	virtual bool				get_bounds( PajaTypes::Vector3C& rMin, PajaTypes::Vector3C& rMax );
	
	virtual const char*			get_name();
	virtual void				set_name( const char* szName );

	virtual PajaTypes::uint32	get_displist_name();
	virtual void				set_displist_name( PajaTypes::uint32 ui32Name );

	// just for save
	virtual void				set_id( PajaTypes::uint32 ui32ID );
	virtual PajaTypes::uint32	get_id() const;

	virtual PajaTypes::uint32	get_triangle_count();
	virtual PajaTypes::uint32	get_vertex_count();

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

private:
	bool					m_bBBoxValid;
	bool					m_bBBoxEmpty;
	PajaTypes::Vector3C		m_rMin, m_rMax;	//Bounding box.. AABB
	std::vector<GeomListC*>	m_rLists;
	std::string				m_sName;
	PajaTypes::uint32		m_ui32DisplaylistName;
	PajaTypes::uint32		m_ui32ID;
};
	
#endif
