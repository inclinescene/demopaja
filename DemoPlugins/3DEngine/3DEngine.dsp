# Microsoft Developer Studio Project File - Name="3DEngine" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=3DEngine - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "3DEngine.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "3DEngine.mak" CFG="3DEngine - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "3DEngine - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "3DEngine - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "3DEngine - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\..\demopaja\plugins"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "MY3DENGINE_EXPORTS" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "..\..\demopaja\opengldriver" /I "..\..\demopaja\corelib" /I "F:\Code and Docs\Libraries\minilzo.106" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "MY3DENGINE_EXPORTS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40b /d "NDEBUG"
# ADD RSC /l 0x40b /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 opengl32.lib glu32.lib kernel32.lib user32.lib gdi32.lib /nologo /dll /machine:I386

!ELSEIF  "$(CFG)" == "3DEngine - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\..\demopaja\plugins"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "MY3DENGINE_EXPORTS" /YX /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I "..\..\demopaja\opengldriver" /I "..\..\demopaja\corelib" /I "F:\Code and Docs\Libraries\minilzo.106" /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "MY3DENGINE_EXPORTS" /YX /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x40b /d "_DEBUG"
# ADD RSC /l 0x40b /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 opengl32.lib glu32.lib kernel32.lib user32.lib gdi32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "3DEngine - Win32 Release"
# Name "3DEngine - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\ASELoaderC.cpp
# End Source File
# Begin Source File

SOURCE=.\CameraC.cpp
# End Source File
# Begin Source File

SOURCE=.\ContFloatC.cpp
# End Source File
# Begin Source File

SOURCE=.\ContQuatC.cpp
# End Source File
# Begin Source File

SOURCE=.\ContVector3C.cpp
# End Source File
# Begin Source File

SOURCE=.\engine.cpp
# End Source File
# Begin Source File

SOURCE=.\geomlistc.cpp
# End Source File
# Begin Source File

SOURCE=.\LightC.cpp
# End Source File
# Begin Source File

SOURCE=.\mesh3dc.cpp
# End Source File
# Begin Source File

SOURCE=.\MeshMorphC.cpp
# End Source File
# Begin Source File

SOURCE=.\MeshRefC.cpp
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\minilzo.106\minilzo.c"
# End Source File
# Begin Source File

SOURCE=.\mtllayerc.cpp
# End Source File
# Begin Source File

SOURCE=.\ParticleSystemC.cpp
# End Source File
# Begin Source File

SOURCE=.\ScenegraphItemI.cpp
# End Source File
# Begin Source File

SOURCE=.\tcsetc.cpp
# End Source File
# Begin Source File

SOURCE=.\TextureC.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\ASELoaderC.h
# End Source File
# Begin Source File

SOURCE=.\CameraC.h
# End Source File
# Begin Source File

SOURCE=.\ContFloatC.h
# End Source File
# Begin Source File

SOURCE=.\ContQuatC.h
# End Source File
# Begin Source File

SOURCE=.\ContVector3C.h
# End Source File
# Begin Source File

SOURCE=.\engine.h
# End Source File
# Begin Source File

SOURCE=.\geomlistc.h
# End Source File
# Begin Source File

SOURCE=.\LightC.h
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\minilzo.106\lzoconf.h"
# End Source File
# Begin Source File

SOURCE=.\mesh3dc.h
# End Source File
# Begin Source File

SOURCE=.\MeshMorphC.h
# End Source File
# Begin Source File

SOURCE=.\MeshRefC.h
# End Source File
# Begin Source File

SOURCE="..\..\..\Code and Docs\Libraries\minilzo.106\minilzo.h"
# End Source File
# Begin Source File

SOURCE=.\mtllayerc.h
# End Source File
# Begin Source File

SOURCE=.\ParticleSystemC.h
# End Source File
# Begin Source File

SOURCE=.\ScenegraphItemI.h
# End Source File
# Begin Source File

SOURCE=.\tcsetc.h
# End Source File
# Begin Source File

SOURCE=.\TextureC.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# End Target
# End Project
