#ifndef MESHMORPHC_H
#define MESHMORPHC_H

#include "PajaTypes.h"
#include "ScenegraphItemI.h"

const PajaTypes::int32	CGITEM_MESHMORPH = 0x1000;


class MeshMorphC : public ScenegraphItemI
{
public:
	MeshMorphC();
	virtual ~MeshMorphC();

	virtual PajaTypes::int32		get_type();

private:
	Mesh3DC*	m_pMesh;
};


#endif // MESHREFC_H