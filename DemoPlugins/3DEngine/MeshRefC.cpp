

#include "ScenegraphItemI.h"
#include "PajaTypes.h"
#include "MeshRefC.h"


using namespace PajaTypes;


MeshRefC::MeshRefC()
{
	// empty
}

MeshRefC::~MeshRefC()
{
	// empty
}

int32
MeshRefC::get_type()
{
	return CGITEM_MESHREF;
}
