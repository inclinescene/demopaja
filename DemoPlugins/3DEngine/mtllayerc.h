#ifndef MTLLAYERC_H
#define MTLLAYERC_H

#include "PajaTypes.h"
#include "ColorC.h"
#include "ImportableI.h"
#include "FileIO.h"


class MtlLayerC
{
public:
	enum MappingTypeE
	{
		MAPPING_NONE = 0,
		MAPPING_COORDS = 1,			//Mapping coords
		MAPPING_OBJ_XYZ,			//Mapping from ObjectSpace vertices normalized to Object BBox
		MAPPING_WORLD_XYZ,			//Mapping from WorldSpace vertices
		MAPPING_SPHERICAL_ENV,		//Guess what...
		MAPPING_CYLINDRICAL_ENV,	//Guess what...
		MAPPING_SHRINKWRAP_ENV,		//Okay this i am not sure how to do... just do it like 3DSMAX does it.. not sure how though..
		MAPPING_SCREEN,				//Aligned to screen...  not sure how though..
	};
	enum MappingAxisE
	{
		MAPPING_AXIS_XY = 0,
		MAPPING_AXIS_UV = 0,
		MAPPING_AXIS_YZ = 1,
		MAPPING_AXIS_VW = 1,
		MAPPING_AXIS_ZX = 2,
		MAPPING_AXIS_WU = 2,
		MAPPING_AXIS_XYZ = 3,
		MAPPING_AXIS_UVW = 3
	};

	//MapTypeE are flags... combine with OR ( | ) 		
	enum LayerTypeE
	{
		//None=0,		
		LAYER_NONE,
		LAYER_LIGHTING_SPECULAR	= 1,	// Light this layer...use lights here...DO ONLY SPECULAR LIGHTING
		LAYER_LIGHTING_DIFFUSE	= 2,	// Light this layer...use lights here...DO ONLY DIFFUSE LIGHTING
		LAYER_LIGHTING_AMBIENT	= 4,	// Light this layer...use lights here...DO ONLY AMBIENT LIGHTING
		LAYER_CONSTANT_COLOR	= 8,	// duh...
		LAYER_VERTEX_COLOR		= 16,	// No texture BUT vertexcolors... =)
		LAYER_TEXTURE			= 32,	// No texture BUT vertexcolors... =)
		LAYER_BLEND_ADD			= 64,	// Add this layer on to the previous one...
		LAYER_BLEND_MULTIPLY	= 128,	// Multiply this layer on to the previous one...			
		LAYER_BLEND_REPLACE		= 256,	// Replace previous layer with this one...(Should be used only on first layer...if you want an opaque object..)											
										// Using it in other layers is PLAIN STUPID!!
		LAYER_TWO_SIDED			= 512,
	};

	MtlLayerC();
	virtual ~MtlLayerC();

	virtual void						set_texture( Import::FileHandleC* pHandle );
	virtual Import::FileHandleC*		get_texture() const;

	virtual void						set_mapping_type( MappingTypeE eType );
	virtual MappingTypeE				get_mapping_type() const;

	virtual void						set_mapping_axis( MappingAxisE eAxis );
	virtual MappingAxisE				get_mapping_axis() const;

	virtual void						set_flags( PajaTypes::uint16 ui16Flags );
	virtual void						add_flags( PajaTypes::uint16 ui16Flags );
	virtual void						del_flags( PajaTypes::uint16 ui16Flags );
	virtual PajaTypes::uint16			get_flags();
	
	virtual void						set_mapping_channel( PajaTypes::uint16 ui16Ch );
	virtual PajaTypes::uint16			get_mapping_channel();

	virtual void						set_ambient( const PajaTypes::ColorC &rAmbient );
	virtual void						set_diffuse( const PajaTypes::ColorC &rDiffuse );
	virtual void						set_specular( const PajaTypes::ColorC &rSpecular );
	virtual void						set_shininess( const PajaTypes::float32 f32Shininess );
	virtual void						set_opacity( const PajaTypes::float32 f32Opacity );
	virtual void						set_self_illumination( const PajaTypes::float32 f32SelfIllum );
	
	virtual const PajaTypes::ColorC&	get_ambient() const;
	virtual const PajaTypes::ColorC&	get_diffuse() const;
	virtual const PajaTypes::ColorC&	get_specular() const;
	virtual PajaTypes::float32			get_shininess();
	virtual PajaTypes::float32			get_opacity();
	virtual PajaTypes::float32			get_self_illumination();

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

protected:
private:
	PajaTypes::ColorC	m_rAmbient;
	PajaTypes::ColorC	m_rDiffuse;
	PajaTypes::ColorC	m_rSpecular;
	PajaTypes::float32	m_f32Shininess;
	PajaTypes::float32	m_f32Opacity;
	PajaTypes::float32	m_f32SelfIllumination;
	
	//Texture
	PajaTypes::uint16		m_ui16Flags;
	PajaTypes::uint16		m_ui16MappingChannel;
	Import::FileHandleC*	m_pTextureHandle;
	MappingTypeE			m_eMappingType;
	MappingAxisE			m_eMappingAxis;
};


#endif