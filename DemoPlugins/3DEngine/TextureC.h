#ifndef TEXTUREC_H
#define TEXTUREC_H


#include "PajaTypes.h"
#include "ImportableI.h"


class TextureC
{
public:
	TextureC();
	virtual ~TextureC();


private:
	FileHandleC	m_rFile;
};


#endif // TEXTUREC_H