#ifndef CAMERAC_H
#define CAMERAC_H

#include "PajaTypes.h"
#include "Vector3C.h"
#include "ControllerC.h"
#include "FileIO.h"
#include "ContFloatC.h"
#include "ContVector3C.h"
#include <string>


class CameraC
{
public:
	CameraC();
	virtual ~CameraC();

	virtual void						set_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_position();

	virtual void						set_target_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_target_position();

	virtual void						set_roll( const PajaTypes::float32 f32Val );
	virtual const PajaTypes::float32	get_roll();

	virtual void						set_fov( const PajaTypes::float32 f32Val );
	virtual const PajaTypes::float32	get_fov();

	virtual void						set_near_plane( const PajaTypes::float32 f32Val );
	virtual const PajaTypes::float32	get_near_plane();

	virtual void						set_far_plane( const PajaTypes::float32 f32Val );
	virtual const PajaTypes::float32	get_far_plane();

	virtual void						eval_state( PajaTypes::int32 i32Time );

	virtual void						set_position_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_position_controller();

	virtual void						set_target_position_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_target_position_controller();

	virtual void						set_roll_controller( ContFloatC* pCont );
	virtual ContFloatC*					get_roll_controller();

	virtual void						set_fov_controller( ContFloatC* pCont );
	virtual ContFloatC*					get_fov_controller();

	virtual void						set_near_plane_controller( ContFloatC* pCont );
	virtual ContFloatC*					get_near_plane_controller();

	virtual void						set_far_plane_controller( ContFloatC* pCont );
	virtual ContFloatC*					get_far_plane_controller();

	virtual void						set_name( const char* szName );
	virtual const char*					get_name();

	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

private:
	PajaTypes::Vector3C	m_rPos;
	PajaTypes::Vector3C	m_rTgt;
	PajaTypes::float32	m_f32Roll;
	PajaTypes::float32	m_f32FOV;
	PajaTypes::float32	m_f32NearPlane;
	PajaTypes::float32	m_f32FarPlane;

	ContVector3C*		m_pPosCont;
	ContVector3C*		m_pTgtCont;
	ContFloatC*			m_pRollCont;
	ContFloatC*			m_pFOVCont;
	ContFloatC*			m_pNearPlaneCont;
	ContFloatC*			m_pFarPlaneCont;

	std::string					m_sName;
};


#endif // CAMERAC_H