#ifndef PARTICLESYSTEMC_H
#define PARTICLESYSTEMC_H

#include "PajaTypes.h"
#include "Vector3C.h"
#include "ControllerC.h"
#include "FileIO.h"
#include <string>


class ParticleSystemC
{
public:
	ParticleSystemC();
	virtual ~ParticleSystemC();

	virtual void						set_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_position();

	virtual void						set_target_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_target_position();

	virtual void						set_color( const PajaTypes::ColorC& rVal );
	virtual const PajaTypes::ColorC&	get_color();

	virtual void						set_multiplier( PajaTypes::float32 f32Val );
	virtual PajaTypes::float32			get_multiplier();

	virtual void						eval_state( PajaTypes::int32 i32Time );

	virtual void						set_position_controller( Composition::ControllerC* pCont );
	virtual Composition::ControllerC*	get_position_controller();

	virtual void						set_target_position_controller( Composition::ControllerC* pCont );
	virtual Composition::ControllerC*	get_target_position_controller();

	virtual void						set_color_controller( Composition::ControllerC* pCont );
	virtual Composition::ControllerC*	get_color_controller();

	virtual void						set_multiplier_controller( Composition::ControllerC* pCont );
	virtual Composition::ControllerC*	get_multiplier_controller();

	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

private:
	PajaTypes::Vector3C	m_rPos;
	PajaTypes::Vector3C	m_rTgt;
	PajaTypes::ColorC	m_rColor;
	PajaTypes::float32	m_f32Multiplier;

	Composition::ControllerC*	m_pPosCont;
	Composition::ControllerC*	m_pTgtCont;
	Composition::ControllerC*	m_pColorCont;
	Composition::ControllerC*	m_pMultCont;
};


#endif // PARTICLESYSTEMC_H