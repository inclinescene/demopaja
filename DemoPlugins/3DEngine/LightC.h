#ifndef LIGHTC_H
#define LIGHTC_H

#include "PajaTypes.h"
#include "Vector3C.h"
#include "ControllerC.h"
#include "ContFloatC.h"
#include "ContVector3C.h"
#include "FileIO.h"
#include <string>


class LightC
{
public:
	LightC();
	virtual ~LightC();

	virtual void						set_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_position();

	virtual void						set_color( const PajaTypes::ColorC& rVal );
	virtual const PajaTypes::ColorC&	get_color();

	virtual void						set_multiplier( PajaTypes::float32 f32Val );
	virtual PajaTypes::float32			get_multiplier();

	virtual void						set_decay_start( PajaTypes::float32 f32Val );
	virtual PajaTypes::float32			get_decay_start();

	virtual void						set_do_decay( bool bState );
	virtual bool						get_do_decay();

	virtual void						eval_state( PajaTypes::int32 i32Time );

	virtual void						set_position_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_position_controller();

	virtual void						set_color_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_color_controller();

	virtual void						set_multiplier_controller( ContFloatC* pCont );
	virtual ContFloatC*					get_multiplier_controller();

	virtual void						set_decay_start_controller( ContFloatC* pCont );
	virtual ContFloatC*					get_decay_start_controller();

	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

private:
	PajaTypes::Vector3C	m_rPos;
	PajaTypes::ColorC	m_rColor;
	PajaTypes::float32	m_f32Multiplier;
	PajaTypes::float32	m_f32DecayStart;
	bool				m_bDoDecay;

	ContVector3C*		m_pPosCont;
	ContVector3C*		m_pColorCont;
	ContFloatC*			m_pMultCont;
	ContFloatC*			m_pDecayStartCont;
};


#endif // LIGHT_H