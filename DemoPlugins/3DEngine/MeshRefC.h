#ifndef MESHREFC_H
#define MESHREFC_H

#include "PajaTypes.h"
#include "ScenegraphItemI.h"

const PajaTypes::int32	CGITEM_MESHREF = 0x1000;


class MeshRefC : public ScenegraphItemI
{
public:
	MeshRefC();
	virtual ~MeshRefC();

	virtual PajaTypes::int32		get_type();
};


#endif // MESHREFC_H