#ifndef SCENEGRAPHITEMI_H
#define SCENEGRAPHITEMI_H


#include "PajaTypes.h"
#include "Vector3C.h"
#include "Mesh3DC.h"
#include "ControllerC.h"
#include "ContVector3C.h"
#include "ContFloatC.h"
#include "ContQuatC.h"
#include "FileIO.h"
#include <vector>




class ScenegraphItemI
{
public:
	ScenegraphItemI();
	virtual ~ScenegraphItemI();

	virtual PajaTypes::int32			get_type() = 0;
	
	virtual void						set_mesh( Mesh3DC* pMesh );
	virtual Mesh3DC*					get_mesh();

	virtual void						set_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_position();

	virtual void						set_scale( const PajaTypes::Vector3C& rScale );
	virtual const PajaTypes::Vector3C&	get_scale();

	virtual void						set_scale_rotation( const PajaTypes::QuatC& rRot );
	virtual const PajaTypes::QuatC&		get_scale_rotation();

	virtual void						set_rotation( const PajaTypes::QuatC& rRot );
	virtual const PajaTypes::QuatC&		get_rotation();

	virtual void						get_bounds( PajaTypes::Vector3C& rMin, PajaTypes::Vector3C& rMax );

	virtual void						eval_state( PajaTypes::int32 i32Time );

	virtual void						set_position_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_position_controller();

	virtual void						set_rotation_controller( ContQuatC* pCont );
	virtual ContQuatC*					get_rotation_controller();

	virtual void						set_scale_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_scale_controller();

	virtual void						set_scale_rotation_controller( ContQuatC* pCont );
	virtual ContQuatC*					get_scale_rotation_controller();

	virtual void						set_visibility_controller( ContFloatC* pCont );
	virtual ContFloatC*					get_visibility_controller();

	virtual bool						get_visibility();

	virtual void						add_child( ScenegraphItemI* pChild );
	virtual PajaTypes::uint32			get_child_count();
	virtual ScenegraphItemI*			get_child( PajaTypes::uint32 ui32Index );
	virtual void						remove_child( PajaTypes::uint32 ui32Index );

	// just for save
	virtual void						set_id( PajaTypes::uint32 ui32ID );
	virtual PajaTypes::uint32			get_id() const;
	virtual PajaTypes::uint32			get_mesh_id() const;
//	virtual PajaTypes::uint32			get_child_id_count();
//	virtual PajaTypes::uint32			get_child_id( PajaTypes::uint32 ui32Index );

	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

private:

	Mesh3DC*					m_pMesh;
	PajaTypes::Vector3C			m_rPosition;
	PajaTypes::Vector3C			m_rScale;
	PajaTypes::QuatC			m_rScaleRot;
	PajaTypes::QuatC			m_rRotation;
	bool						m_bVisible;

	ContVector3C*				m_pPosCont;
	ContQuatC*					m_pRotCont;
	ContVector3C*				m_pScaleCont;
	ContQuatC*					m_pScaleRotCont;
	ContFloatC*					m_pVisCont;

	bool						m_bBBoxEmpty;
	PajaTypes::Vector3C			m_rMin;
	PajaTypes::Vector3C			m_rMax;

	std::vector<ScenegraphItemI*>	m_rChilds;
	PajaTypes::uint32				m_ui32ID;
	PajaTypes::uint32				m_ui32MeshID;
};


#endif // SCENEGRAPHITEMI_H