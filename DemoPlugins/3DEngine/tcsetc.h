#ifndef TCSETC_H
#define TCSETC_H

#include "PajaTypes.h"
#include "FileIO.h"
#include <assert.h>

//
// texture coodrinate class
//

class TextureCoordC
{
public:
	TextureCoordC();
	TextureCoordC( PajaTypes::float32 f32U, PajaTypes::float32 f32V, PajaTypes::float32 f32W );
	TextureCoordC( const TextureCoordC& rCol );
	virtual ~TextureCoordC();

	PajaTypes::float32&			operator[]( PajaTypes::int32 i32Index );
	const PajaTypes::float32&	operator[]( PajaTypes::int32 i32Index ) const;
								operator PajaTypes::float32*();
private:
	PajaTypes::float32	m_f32Coords[3];
};

inline
PajaTypes::float32&
TextureCoordC::operator[]( PajaTypes::int32 i32Index )
{
	assert( i32Index >= 0 && i32Index < 3 );
	return m_f32Coords[i32Index];
}

inline
const PajaTypes::float32&
TextureCoordC::operator[]( PajaTypes::int32 i32Index ) const
{
	assert( i32Index >= 0 && i32Index < 3 );
	return m_f32Coords[i32Index];
}

inline
TextureCoordC::operator PajaTypes::float32*()
{
	return m_f32Coords;
}


//
// texture coordinate set class
//

class GeomListC;

class TCSetC
{
public:

	enum MappingAxisE {
		AXIS_UV,
		AXIS_VW,
		AXIS_WU,
	};

	TCSetC();
	virtual ~TCSetC();	

	virtual TCSetC&				operator=( const TCSetC& rSet );

	virtual void				set_mapping_axis( MappingAxisE eAxis );
	virtual MappingAxisE		get_mapping_axis();

	virtual void				reserve_indices( PajaTypes::uint16 ui16Size );
	virtual void				reserve_coords( PajaTypes::uint16 ui16Size );
	
	virtual void				set_index( PajaTypes::uint16 ui16Idx, PajaTypes::uint16 ui16Index );
	virtual void				set_coord( PajaTypes::uint16 ui16Idx, const TextureCoordC& rCoord );

	virtual PajaTypes::uint16	get_index( PajaTypes::uint16 ui16Idx );
	virtual TextureCoordC		get_coord( PajaTypes::uint16 ui16Idx );

	virtual PajaTypes::uint16	get_index_count();
	virtual PajaTypes::uint16	get_coord_count();

	virtual PajaTypes::uint16*	get_index_pointer();
	virtual PajaTypes::float32*	get_coord_pointer();

	virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

protected:
private:

	virtual PajaTypes::uint32	write_compressed( FileIO::SaveC* pSave, PajaTypes::uint8* pBuffer, PajaTypes::uint32 ui32BufferSize, PajaTypes::uint8* pCompressed, long* pWorkMem );
	virtual PajaTypes::uint32	read_compressed( FileIO::LoadC* pLoad, PajaTypes::uint8* pBuffer, PajaTypes::uint32 ui32BufferSize );


	PajaTypes::uint16	m_ui16IndexCount;
	PajaTypes::uint16	m_ui16CoordCount;
	PajaTypes::uint16*	m_pIndices;
	PajaTypes::float32*	m_pCoords;
	MappingAxisE		m_eAxis;
	PajaTypes::uint32	m_ui32Index1;
	PajaTypes::uint32	m_ui32Index2;
};


#endif