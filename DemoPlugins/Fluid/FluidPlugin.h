//
// FluidPlugin.h
//
// Fluid Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

#ifndef __FLUIDPLUGIN_H__
#define __FLUIDPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"

#include "FluidSolverC.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_FLUID_EFFECT( 0x472010BD, 0x36924209 );


//////////////////////////////////////////////////////////////////////////
//
//  Fluid effect class descriptor.
//

class FluidDescC : public PluginClass::ClassDescC
{
public:
	FluidDescC();
	virtual ~FluidDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};

namespace FluidPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The Warp effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_COLOR = 0,
		ID_ATTRIBUTE_RENDERMODE,
		ID_ATTRIBUTE_MIXMODE,
		ID_ATTRIBUTE_CHAOS,
		ID_ATTRIBUTE_DEFORM,
		ID_ATTRIBUTE_FILE,
		ATTRIBUTE_COUNT,
	};

	enum EmitterParamsE {
		ID_EMITTER_MODE,
		ID_EMITTER_POSITION,
		ID_EMITTER_ROTATION,
		ID_EMITTER_AMOUNT,
	};

	enum WarpEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		ID_GIZMO_EMITTER1,
		ID_GIZMO_EMITTER2,
		ID_GIZMO_EMITTER3,
		ID_GIZMO_EMITTER4,
		ID_GIZMO_EMITTER5,
		GIZMO_COUNT,
	};

	enum RenderModeE {
		RENDERMODE_NORMAL = 0,
		RENDERMODE_ADD = 1,
		RENDERMODE_MULT = 2,
	};

	enum MixModeE {
		MIXMODE_REVEAL = 0,
		MIXMODE_HIDE = 1,
	};

	enum EmitterModeE {
		EMITTER_NONE = 0,
		EMITTER_MATERIAL = 1,
		EMITTER_OBSTACLE = 2,
	};

	class FluidEffectC : public Composition::EffectI
	{
	public:
		static FluidEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		FluidEffectC();
		FluidEffectC( Edit::EditableI* pOriginal );
		virtual ~FluidEffectC();

		void	fill_emitter_gizmo( Composition::AutoGizmoC* pGizmo, bool bOn );

	private:

		struct EmitterS {
			PajaTypes::int32	m_i32Mode;
			PajaTypes::Vector2C	m_rPos;
			PajaTypes::float32	m_f32Rot;
			PajaTypes::float32	m_f32Amount;
		};

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;
		Composition::AutoGizmoC*	m_pEm1Gizmo;
		Composition::AutoGizmoC*	m_pEm2Gizmo;
		Composition::AutoGizmoC*	m_pEm3Gizmo;
		Composition::AutoGizmoC*	m_pEm4Gizmo;
		Composition::AutoGizmoC*	m_pEm5Gizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];

		FluidSolverC		m_rFluid;
		PajaTypes::int32	m_i32ValidTime;
		bool				m_bInvalidate;
	};

};	// namespace

extern FluidDescC	g_rFluidDesc;


#endif	// __FLUIDPLUGIN_H__
