#ifndef __SWFFONT_H__
#define __SWFFONT_H__

#include "PajaTypes.h"
#include "Vector2C.h"
#include "ColorC.h"
#include <vector>
#include "SWFChar.h"
#include "SWFShape.h"


//
// Glyph
//

class SWFGlyphC
{
public:
	SWFGlyphC();
	virtual ~SWFGlyphC();

	SWFShapeRecordC*	get_shape();
	PajaTypes::uint16	get_code() const;
	void				set_code( PajaTypes::uint16 ui16Code );

private:
	PajaTypes::uint16	m_ui16Code;
	SWFShapeRecordC		m_rShape;
};


//
// Font character
//

class SWFFontC : public SWFCharC
{
public:
	SWFFontC();
	virtual ~SWFFontC();

	virtual PajaTypes::uint32	get_type() const;
	void						add_glyph( SWFGlyphC* pGlyph );
	PajaTypes::uint32			get_glyph_count() const;
	SWFGlyphC*					get_glyph( PajaTypes::uint32 ui32Idx );
	SWFGlyphC*					get_glyph_by_code( PajaTypes::uint16 ui16Code );

private:
	std::vector<SWFGlyphC*>	m_rGlyphs;
};

#endif