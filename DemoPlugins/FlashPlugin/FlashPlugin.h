//
// FlashPlugin.h
//
// Flash player and importer Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

#ifndef __DEMOPAJA_SWFPLUGIN_H__
#define __DEMOPAJA_SWFPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLViewportC.h"
#include "OpenGLDeviceC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "FlashScriptC.h"


//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_SWFPLAYER_EFFECT( 0x957B746D, 0x062B4A02 );
const PluginClass::ClassIdC	CLASS_SWF_IMPORT( 0x0F93FF55, 0xB1BC4310 );


//////////////////////////////////////////////////////////////////////////
//
//  AVI importer class descriptor.
//

class SWFImportDescC : public PluginClass::ClassDescC
{
public:
	SWFImportDescC();
	virtual ~SWFImportDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  Simple image effect class descriptor.
//

class SWFPlayerDescC : public PluginClass::ClassDescC
{
public:
	SWFPlayerDescC();
	virtual ~SWFPlayerDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


namespace FlashPlugin {

//////////////////////////////////////////////////////////////////////////
//
// SWF Importer class.
//

	class SWFImportC : public Import::ImportableI
	{
	public:
		static SWFImportC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual const char*				get_filename();
		virtual void					set_filename( const char* szName );
		virtual bool					load_file( const char* szName, PajaSystem::DemoInterfaceC* pInterface );
		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual PluginClass::SuperClassIdC	get_super_class_id();
		virtual const char*				get_class_name();

		// The importable image interface.
		virtual PajaTypes::int32		get_width();
		virtual PajaTypes::int32		get_height();

		virtual PajaTypes::int32		get_first_frame();
		virtual PajaTypes::int32		get_last_frame();
		virtual PajaTypes::float32		get_fps();

		virtual FlashScriptC*			get_script();
		virtual PajaTypes::int32		get_frame() const;

		virtual const char*				get_info();
		virtual PluginClass::ClassIdC	get_default_effect();

		virtual PajaTypes::int32		get_duration();
		virtual PajaTypes::float32		get_start_label();
		virtual PajaTypes::float32		get_end_label();

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

	protected:
		SWFImportC();
		SWFImportC( Edit::EditableI* pOriginal );
		virtual ~SWFImportC();

	private:

		std::string				m_sFileName;

		PajaTypes::uint8*		m_pRawData;
		PajaTypes::uint32		m_ui32RawSize;
		FlashScriptC*			m_pScript;
		PajaTypes::int32		m_i32CurrentFrame;
	};


//////////////////////////////////////////////////////////////////////////
//
// The Flash Player effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_COLOR = 0,
		ID_ATTRIBUTE_RENDERMODE,
		ID_ATTRIBUTE_CLEARBG,
		ID_ATTRIBUTE_FILE,
		ATTRIBUTE_COUNT,
	};

	enum TimeGizmoParamsE {
		ID_TIME_FPS = 0,		// deprecated, needed for correct ID
		ID_TIME_FRAME,
	};

	enum FlashPlayerEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		ID_GIZMO_TIME,
		GIZMO_COUNT,
	};

	enum RenderModeE {
		RENDERMODE_NORMAL = 0,
		RENDERMODE_ADD = 1,
	};

	class FlashPlayerEffectC : public Composition::EffectI
	{
	public:
		static FlashPlayerEffectC*		create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		FlashPlayerEffectC();
		FlashPlayerEffectC( Edit::EditableI* pOriginal );
		virtual ~FlashPlayerEffectC();

	private:

		void	draw_sprite( FlashScriptC* pScript, SWFDisplayListC* pDispList, PajaTypes::Matrix2C rTM, SWFCXFormC rCXForm );
		void	draw_shape( SWFShapeRecordC* pShape, PajaTypes::Matrix2C rDrawTM, SWFCXFormC rCXForm, PajaTypes::ColorC rGlyphColor = PajaTypes::ColorC(), bool bIsGlyph = false );

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;
		Composition::AutoGizmoC*	m_pTimeGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
		PajaTypes::ColorC	m_rFillColor;
		PajaTypes::int32	m_i32RenderMode;
		PajaTypes::int32	m_i32ClearBG;
		PajaTypes::float32	m_f32ViewportScale;
	};

};	// namespace

extern SWFImportDescC	g_rSWFImportDesc;
extern SWFPlayerDescC	g_rSWFPlayerDesc;


#endif	// __DEMOPAJA_SWFPLUGIN_H__
