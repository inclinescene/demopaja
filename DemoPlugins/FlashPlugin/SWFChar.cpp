
#include "PajaTypes.h"
#include "SWFChar.h"


using namespace PajaTypes;


SWFCharC::SWFCharC() :
	m_ui32TagId( 0 )
{
	// empty
}

SWFCharC::~SWFCharC()
{
	// Empty
}

uint32
SWFCharC::get_id() const
{
	return m_ui32TagId;
}

void
SWFCharC::set_id( uint32 ui32Id )
{
	m_ui32TagId = ui32Id;
}
