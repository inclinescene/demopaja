#ifndef __SWFDISPLIST_H__
#define __SWFDISPLIST_H__

#include "PajaTypes.h"
#include "Vector2C.h"
#include "Matrix2C.h"
#include "ColorC.h"
#include <vector>
#include "SWFChar.h"
#include "SWFFont.h"



//
// Color transform
//

class SWFCXFormC 
{
public:
	SWFCXFormC( const PajaTypes::ColorC& rMult = PajaTypes::ColorC( 1, 1, 1, 1 ),
				const PajaTypes::ColorC& rAdd = PajaTypes::ColorC( 0, 0, 0, 0 ) );
	virtual ~SWFCXFormC();

	const PajaTypes::ColorC&	get_color_mult() const;
	const PajaTypes::ColorC&	get_color_add() const;
	void						set_color_mult( const PajaTypes::ColorC& rMult );
	void						set_color_add( const PajaTypes::ColorC& rAdd );

private:
	PajaTypes::ColorC	m_rMult;
	PajaTypes::ColorC	m_rAdd;
};


//
// Object reference
//

class SWFObjectRefC
{
public:
	SWFObjectRefC( PajaTypes::uint32 ui32CharId, PajaTypes::uint32 ui32Depth,
					const PajaTypes::Matrix2C& rTM, const SWFCXFormC& rCXForm );
	SWFObjectRefC( const SWFObjectRefC& rRef );
	virtual ~SWFObjectRefC();

	PajaTypes::uint32			get_depth() const;
	PajaTypes::uint32			get_char_id() const;
	const PajaTypes::Matrix2C&	get_tm() const;
	const SWFCXFormC&			get_cxform() const;

	void						set_tm( const PajaTypes::Matrix2C& rTM );
	void						set_cxform( const SWFCXFormC& rCXForm );

	void						set_frame( PajaTypes::uint32 ui32Frame );
	PajaTypes::uint32			get_frame() const;

private:
	PajaTypes::uint32	m_ui32Depth;
	PajaTypes::uint32	m_ui32CharId;
	PajaTypes::uint32	m_ui32Frame;
	PajaTypes::Matrix2C	m_rTM;
	SWFCXFormC			m_rCXForm;
};


//
// Display list
//

class SWFDisplayListC
{
public:
	SWFDisplayListC();
	SWFDisplayListC( SWFDisplayListC& rList );
	virtual ~SWFDisplayListC();

	void				move_object( PajaTypes::uint32 ui32Depth, const PajaTypes::Matrix2C& rTM, const SWFCXFormC& rCXForm, bool bHasTM, bool bHasCxForm );
	void				place_object( PajaTypes::uint32 ui32CharId, PajaTypes::uint32 ui32Depth, const PajaTypes::Matrix2C& rTM, const SWFCXFormC& rCXForm );
	void				replace_object( PajaTypes::uint32 ui32CharId, PajaTypes::uint32 ui32Depth, const PajaTypes::Matrix2C& rTM, const SWFCXFormC& rCXForm, bool bHasTM, bool bHasCxForm );
	void				remove_object( PajaTypes::uint32 ui32Depth, PajaTypes::int32 i32TagId = -1 );

	PajaTypes::uint32	get_object_count() const;
	SWFObjectRefC*		get_object( PajaTypes::uint32 ui32Idx );

private:
	std::vector<SWFObjectRefC*>	m_rObjects;
};

#endif