#include "PajaTypes.h"
#include "Vector2C.h"
#include "Matrix2C.h"
#include "ColorC.h"
#include "BBox2C.h"
#include <vector>
#include "SWFChar.h"
#include "SWFFont.h"
#include "SWFText.h"

using namespace PajaTypes;


//
// Text record
//

SWFTextRecordC::SWFTextRecordC() :
	m_ui32FontId( 0 ),
	m_f32FontHeight( 0 ),
	m_bIsFormat( false )
{
	// empty
}

SWFTextRecordC::~SWFTextRecordC()
{
	// empty
}

void
SWFTextRecordC::set_color( const ColorC& rCol )
{
	m_rColor = rCol;
	m_bIsFormat = true;
}

const ColorC&
SWFTextRecordC::get_color() const
{
	return m_rColor;
}

void
SWFTextRecordC::set_offset( const Vector2C& rOffset )
{
	m_rOffset = rOffset;
	m_bIsFormat = true;
}

const Vector2C&
SWFTextRecordC::get_offset() const
{
	return m_rOffset;
}

void
SWFTextRecordC::set_font_height( float32 f32Height )
{
	m_f32FontHeight = f32Height;
	m_bIsFormat = true;
}

float32
SWFTextRecordC::get_font_height() const
{
	return m_f32FontHeight;
}

void
SWFTextRecordC::set_font_id( uint32 ui32FontId )
{
	m_ui32FontId = ui32FontId;
	m_bIsFormat = true;
}

uint32
SWFTextRecordC::get_font_id() const
{
	return m_ui32FontId;
}

void
SWFTextRecordC::add_glyph( uint16 ui16Char, float32 f32Adv )
{
	CharS	rChar;
	rChar.m_ui16Char = ui16Char;
	rChar.m_f32Advance = f32Adv;

	m_rText.push_back( rChar );
}

uint32
SWFTextRecordC::get_glyph_count() const
{
	return m_rText.size();
}

uint16
SWFTextRecordC::get_glyph_index( uint32 ui32Idx ) const
{
	if( ui32Idx < m_rText.size() )
		return m_rText[ui32Idx].m_ui16Char;
	return 0;
}

float32
SWFTextRecordC::get_glyph_advance( uint32 ui32Idx ) const
{
	if( ui32Idx < m_rText.size() )
		return m_rText[ui32Idx].m_f32Advance;
	return 0;
}

bool
SWFTextRecordC::is_format() const
{
	return m_bIsFormat;
}


//
// Text character
//

SWFTextC::SWFTextC()
{
	// empty
}

SWFTextC::~SWFTextC()
{
	for( uint32 i = 0; i < m_rTexts.size(); i++ )
		delete m_rTexts[i];
	m_rTexts.clear();
}

uint32
SWFTextC::get_type() const
{
	return SWF_CHAR_TEXT;
}

void
SWFTextC::add_text( SWFTextRecordC* pText )
{
	m_rTexts.push_back( pText );
}

uint32
SWFTextC::get_text_count() const
{
	return m_rTexts.size();
}

SWFTextRecordC*
SWFTextC::get_text( uint32 ui32Idx ) const
{
	if( ui32Idx < m_rTexts.size() )
		return m_rTexts[ui32Idx];
	return 0;
}

void
SWFTextC::set_tm( const Matrix2C& rTM )
{
	m_rTM = rTM;
}

const Matrix2C&
SWFTextC::get_tm() const
{
	return m_rTM;
}

void
SWFTextC::set_bbox( const BBox2C& rBox )
{
	m_rBBox = rBox;
}

const BBox2C&
SWFTextC::get_bbox() const
{
	return m_rBBox;
}
