#ifndef __SWFCHAR_H__
#define __SWFCHAR_H__


#include "PajaTypes.h"


enum CharTypeE {
	SWF_CHAR_SHAPE = 1,
	SWF_CHAR_TEXT,
	SWF_CHAR_FONT,
	SWF_CHAR_BITMAP,
	SWF_CHAR_SPRITE,
	SWF_CHAR_BUTTON,
};


//
// SWF chararcter.
//

class SWFCharC
{
public:
	SWFCharC();
	virtual ~SWFCharC();

	virtual PajaTypes::uint32	get_type() const = 0;

	virtual PajaTypes::uint32	get_id() const;
	virtual void				set_id( PajaTypes::uint32 ui32Id );

private:
	PajaTypes::uint32	m_ui32TagId;
};


#endif