#include "PajaTypes.h"
#include "SWFChar.h"
#include "SWFDictionaryC.h"

using namespace PajaTypes;


SWFDictionaryC::SWFDictionaryC()
{
	// empty
}

SWFDictionaryC::~SWFDictionaryC()
{
	for( uint32 i = 0; i < m_rDict.size(); i++ ) {
		delete m_rDict[i];
	}
}

void
SWFDictionaryC::add_character( SWFCharC* pChar )
{
	m_rDict.push_back( pChar );
}

uint32
SWFDictionaryC::get_char_count()
{
	return m_rDict.size();
}

SWFCharC*
SWFDictionaryC::get_char( uint32 ui32Idx )
{
	if( ui32Idx < m_rDict.size() )
		return m_rDict[ui32Idx];
	return 0;
}

SWFCharC*
SWFDictionaryC::get_char_by_id( uint32 ui32ID )
{
	for( uint32 i = 0; i < m_rDict.size(); i++ )
		if( m_rDict[i]->get_id() == ui32ID )
			return m_rDict[i];
	return 0;
}

