#ifndef __FLASHDICTIONARYC_H__
#define __FLASHDICTIONARYC_H__


#include "PajaTypes.h"
#include "SWFChar.h"
#include <vector>

//
// Character dictionary
//

class SWFDictionaryC
{
public:
	SWFDictionaryC();
	virtual ~SWFDictionaryC();

	void				add_character( SWFCharC* pChar );
	PajaTypes::uint32	get_char_count();
	SWFCharC*			get_char( PajaTypes::uint32 ui32Idx );
	SWFCharC*			get_char_by_id( PajaTypes::uint32 ui32ID );

protected:
	std::vector<SWFCharC*>	m_rDict;
};

#endif