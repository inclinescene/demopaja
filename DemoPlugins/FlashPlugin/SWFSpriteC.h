#ifndef __FLASHSPRITEC_H__
#define __FLASHSPRITEC_H__


#include "PajaTypes.h"
#include "Vector2C.h"
#include "Matrix2C.h"
#include <vector>

#include "SWFChar.h"
#include "SWFDispList.h"
#include "SWFFont.h"
#include "SWFShape.h"
#include "SWFText.h"
#include "SWFDictionaryC.h"


//
// Sprite character
//

class SWFSpriteC : public SWFCharC
{
public:
	SWFSpriteC();
	virtual ~SWFSpriteC();

	virtual PajaTypes::uint32	get_type() const;

	void						add_displaylist( SWFDisplayListC* pList );
	PajaTypes::uint32			get_displaylist_count() const;
	SWFDisplayListC*			get_displaylist( PajaTypes::uint32 ui32Idx );

	void						update_frames();
	void						update_shape_scalerange( SWFDictionaryC* pDict );

private:

	struct SpriteFrameS {
		PajaTypes::uint32	m_ui32Depth;
		PajaTypes::uint32	m_ui32CharId;
		PajaTypes::uint32	m_ui32Frame;
		bool				m_bUpdated;
	};

	std::vector<SWFDisplayListC*>	m_rFrames;
};

#endif