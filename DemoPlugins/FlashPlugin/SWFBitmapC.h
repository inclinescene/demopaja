#ifndef __SWFBITMAPC_H__
#define __SWFBITMAPC_H__


#include "PajaTypes.h"
#include "SWFChar.h"

//
// Bitmap
//


class SWFBitmapC : public SWFCharC
{
public:
	SWFBitmapC();
	virtual ~SWFBitmapC();

	virtual PajaTypes::uint32	get_type() const;

	void				set_width( PajaTypes::uint32 ui32Width );
	PajaTypes::uint32	get_width() const;

	void				set_height( PajaTypes::uint32 ui32Height );
	PajaTypes::uint32	get_height() const;

	void				set_bpp( PajaTypes::uint32 ui32BPP );
	PajaTypes::uint32	get_bpp() const;

	void				set_data( PajaTypes::uint8* pData );
	PajaTypes::uint8*	get_data() const;

	PajaTypes::uint32	get_texture_id() const;
	void				prepare();
	void				unprepare();

private:
	PajaTypes::uint32	m_ui32Width, m_ui32Height;
	PajaTypes::uint32	m_ui32BPP;
	PajaTypes::uint32	m_ui32TexId;
	PajaTypes::uint8*	m_pData;
};


#endif