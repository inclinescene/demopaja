
#include "PajaTypes.h"
#include "Vector2C.h"
#include "ColorC.h"
#include <vector>
#include "SWFFont.h"
#include "SWFShape.h"
#include "SWFChar.h"


using namespace PajaTypes;


SWFGlyphC::SWFGlyphC()
{
	// empty
}

SWFGlyphC::~SWFGlyphC()
{
	// empty
}

SWFShapeRecordC*
SWFGlyphC::get_shape()
{
	return &m_rShape;
}

uint16
SWFGlyphC::get_code() const
{
	return m_ui16Code;
}

void
SWFGlyphC::set_code( uint16 ui16Code )
{
	m_ui16Code = ui16Code;
}


//
// Font character
//

SWFFontC::SWFFontC()
{
	// empty
}

SWFFontC::~SWFFontC()
{
	for( PajaTypes::uint32 i = 0; i < m_rGlyphs.size(); i++ )
		delete m_rGlyphs[i];

	m_rGlyphs.clear();
}

uint32
SWFFontC::get_type() const
{
	return SWF_CHAR_FONT;
}

void
SWFFontC::add_glyph( SWFGlyphC* pGlyph )
{
	m_rGlyphs.push_back( pGlyph );
}

uint32
SWFFontC::get_glyph_count() const
{
	return m_rGlyphs.size();
}

SWFGlyphC*
SWFFontC::get_glyph( uint32 ui32Idx )
{
	if( ui32Idx < m_rGlyphs.size() )
		return m_rGlyphs[ui32Idx];
	return 0;
}

SWFGlyphC*
SWFFontC::get_glyph_by_code( uint16 ui16Code )
{
	for( uint32 i = 0; i < m_rGlyphs.size(); i++ )
		if( m_rGlyphs[i]->get_code() == ui16Code )
			return m_rGlyphs[i];
	return 0;
}
