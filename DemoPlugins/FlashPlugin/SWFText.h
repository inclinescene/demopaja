#ifndef __SWFTEXT_H__
#define __SWFTEXT_H__

#include "PajaTypes.h"
#include "Vector2C.h"
#include "Matrix2C.h"
#include "ColorC.h"
#include "BBox2C.h"
#include <vector>
#include "SWFChar.h"
#include "SWFFont.h"


//
// Text record
//

class SWFTextRecordC
{
public:
	SWFTextRecordC();
	virtual ~SWFTextRecordC();

	void						set_color( const PajaTypes::ColorC& rCol );
	const PajaTypes::ColorC&	get_color() const;

	void						set_offset( const PajaTypes::Vector2C& rOffset );
	const PajaTypes::Vector2C&	get_offset() const;

	void						set_font_height( PajaTypes::float32 f32Height );
	PajaTypes::float32			get_font_height() const;

	void						set_font_id( PajaTypes::uint32 ui32FontId );
	PajaTypes::uint32			get_font_id() const;

	void						add_glyph( PajaTypes::uint16 ui16Char, PajaTypes::float32 f32Adv );
	PajaTypes::uint32			get_glyph_count() const;
	PajaTypes::uint16			get_glyph_index( PajaTypes::uint32 ui32Idx ) const;
	PajaTypes::float32			get_glyph_advance( PajaTypes::uint32 ui32Idx ) const;

	bool						is_format() const;

private:

	struct CharS {
		PajaTypes::uint16	m_ui16Char;
		PajaTypes::float32	m_f32Advance;
	};

	std::vector<CharS>	m_rText;

	PajaTypes::uint32	m_ui32FontId;
	PajaTypes::float32	m_f32FontHeight;
	PajaTypes::Vector2C	m_rOffset;
	PajaTypes::ColorC	m_rColor;
	bool				m_bIsFormat;
};


//
// Text character
//

class SWFTextC : public SWFCharC
{
public:
	SWFTextC();
	virtual ~SWFTextC();

	virtual PajaTypes::uint32	get_type() const;

	void						add_text( SWFTextRecordC* pText );
	PajaTypes::uint32			get_text_count() const;
	SWFTextRecordC*				get_text( PajaTypes::uint32 ui32Idx ) const;

	void						set_tm( const PajaTypes::Matrix2C& rTM );
	const PajaTypes::Matrix2C&	get_tm() const;

	void						set_bbox( const PajaTypes::BBox2C& rBox );
	const PajaTypes::BBox2C&	get_bbox() const;

private:
	PajaTypes::BBox2C				m_rBBox;
	PajaTypes::Matrix2C				m_rTM;
	std::vector<SWFTextRecordC*>	m_rTexts;
};

#endif