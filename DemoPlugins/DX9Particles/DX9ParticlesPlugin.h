//
// DX9Particles.h
//
// DX9Particles Plugin
//
// Copyright (c) 2004 memon/moppi productions
//

#ifndef __DX9ParticlesPLUGIN_H__
#define __DX9ParticlesPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "DX9ViewportC.h"
#include "DX9DeviceC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "ImportableImageI.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

#define	CLASS_DX9PARTICLES_EFFECT_NAME	"Particles (DX9)"
#define	CLASS_DX9PARTICLES_EFFECT_DESC	"Particles (DX9)"

const PluginClass::ClassIdC	CLASS_DX9PARTICLES_EFFECT( 0xAD4A1038, 0x007B425B );


//////////////////////////////////////////////////////////////////////////
//
//  DX9Particles effect class descriptor.
//

class DX9ParticlesDescC : public PluginClass::ClassDescC
{
public:
	DX9ParticlesDescC();
	virtual ~DX9ParticlesDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};

namespace DX9ParticlesPlugin {

//////////////////////////////////////////////////////////////////////////
//
// DX9Particles effect class.
//

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_IMAGE = 0,
		ID_ATTRIBUTE_COLOR,
		ID_ATTRIBUTE_OPACITY,
		ID_ATTRIBUTE_BLEND,
		ATTRIBUTE_COUNT,
	};

	class DX9ParticlesEffectC : public Composition::EffectI
	{
	public:
		static DX9ParticlesEffectC*				create_new();
		virtual Edit::DataBlockI*			create();
		virtual Edit::DataBlockI*			create( Edit::EditableI* pOriginal );
		virtual void									copy( Edit::EditableI* pEditable );
		virtual void									restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32			get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*						get_class_name();

		virtual void									set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual PajaTypes::uint32			update_notify( EditableI* pCaller );

		virtual void									initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void									eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::BBox2C			get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool									hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );


	protected:
		DX9ParticlesEffectC();
		DX9ParticlesEffectC( Edit::EditableI* pOriginal );
		virtual ~DX9ParticlesEffectC();

	private:
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
	};

};	// namespace


extern DX9ParticlesDescC			g_rDX9ParticlesDesc;


#endif	// __DX9Particles_H__
