//
// BlurPlugin.h
//
// Blur Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

#ifndef __BLURPLUGIN_H__
#define __BLURPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectPostProcessI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_BLUR_EFFECT( 0xC7826FD6, 0x4AB048D8 );


//////////////////////////////////////////////////////////////////////////
//
//  Blur effect class descriptor.
//

class BlurDescC : public PluginClass::ClassDescC
{
public:
	BlurDescC();
	virtual ~BlurDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


namespace BlurPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The Blur effect class.
//

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_SIZE = 0,
		ID_ATTRIBUTE_BLEND_MODE,
		ID_ATTRIBUTE_BLEND_AMOUNT,
		ATTRIBUTE_COUNT,
	};

	enum AVIPlayerEffectGizmosE {
		ID_GIZMO_ATTRIB = 0,
		GIZMO_COUNT,
	};

	enum RenderModeE {
		RENDERMODE_NORMAL = 0,
		RENDERMODE_ADD = 1,
		RENDERMODE_MULT = 0,
	};

	class BlurEffectC : public Composition::EffectPostProcessI
	{
	public:
		static BlurEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaSystem::GraphicsBufferI*	get_render_target();

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		BlurEffectC();
		BlurEffectC( Edit::EditableI* pOriginal );
		virtual ~BlurEffectC();

	private:

		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;

		PajaSystem::GraphicsBufferI*	m_pGBuffer;
	};

};	// namespace


extern BlurDescC	g_rBlurDesc;


#endif	// __AVIPLUGIN_H__
