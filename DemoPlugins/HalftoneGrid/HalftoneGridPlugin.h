//
// HalftoneGridPlugin.h
//
// Halftone Grid Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

#ifndef __HALFTONEGRIDPLUGIN_H__
#define __HALFTONEGRIDPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_HALFTONEGRID_EFFECT( 0xBD6591CA, 0x8CC54FFB );


//////////////////////////////////////////////////////////////////////////
//
//  Halftone Grid effect class descriptor.
//

class HalftoneGridDescC : public PluginClass::ClassDescC
{
public:
	HalftoneGridDescC();
	virtual ~HalftoneGridDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


namespace HalftoneGridPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The Blur effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_COLOR = 0,
		ID_ATTRIBUTE_RENDERMODE,
		ID_ATTRIBUTE_COLOR_SOURCE,
		ID_ATTRIBUTE_SIZE_SOURCE,
		ID_ATTRIBUTE_MASK_SOURCE,
		ID_ATTRIBUTE_SCROLL_SPEED,
		ID_ATTRIBUTE_DOT_IMAGE,
		ID_ATTRIBUTE_MASK_IMAGE,
		ID_ATTRIBUTE_SCROLL_IMAGE,
		ATTRIBUTE_COUNT,
	};

	enum PointsGizmoParamsE {
		ID_POINTS_WIDTH = 0,
		ID_POINTS_HEIGHT,
		ID_POINTS_ANCHOR_START,
		ID_POINTS_CONTROL_START,
		ID_POINTS_CONTROL_END,
		ID_POINTS_ANCHOR_END,
		ID_POINTS_SIZE_ENVELOPE,
		ID_POINTS_COLUMN_OFFSET,
		ID_POINTS_SHOW_GRID,
		POINTS_COUNT,
	};

	enum HalftoneGridEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		ID_GIZMO_POINTS,
		GIZMO_COUNT,
	};

	enum RenderModeE {
		RENDERMODE_NORMAL = 0,
		RENDERMODE_ADD = 1,
		RENDERMODE_MULT = 2,
	};

	enum SizeSourceE {
		SIZE_SOURCE_NONE = 0,
		SIZE_SOURCE_R,
		SIZE_SOURCE_G,
		SIZE_SOURCE_B,
		SIZE_SOURCE_A,
	};

	enum MaskSourceE {
		MASK_SOURCE_R = 0,
		MASK_SOURCE_G,
		MASK_SOURCE_B,
		MASK_SOURCE_A,
	};

	enum ColorSourceE {
		COLOR_SOURCE_CONSTANT = 0,
		COLOR_SOURCE_SCROLL
	};

	class HalftoneGridEffectC : public Composition::EffectI
	{
	public:
		static HalftoneGridEffectC*		create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		HalftoneGridEffectC();
		HalftoneGridEffectC( Edit::EditableI* pOriginal );
		virtual ~HalftoneGridEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;
		Composition::AutoGizmoC*	m_pPointsGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
		PajaTypes::Vector2C	m_rMin, m_rMax;

		PajaTypes::Vector2C	m_rAnchorStart;
		PajaTypes::Vector2C	m_rControlStart;
		PajaTypes::Vector2C	m_rControlEnd;
		PajaTypes::Vector2C	m_rAnchorEnd;

		std::vector<PajaTypes::Vector2C>	m_rGrid;
		std::vector<PajaTypes::float32>		m_rSize;
		std::vector<PajaTypes::float32>		m_rColOffset;
		
		PajaTypes::float32	m_f32ScrollOffset;

		PajaTypes::int32	m_i32ShowGrid;
		PajaTypes::int32	m_i32RenderMode;
		PajaTypes::int32	m_i32ColorSource;
		PajaTypes::int32	m_i32SizeSource;
		PajaTypes::int32	m_i32MaskSource;

		PajaTypes::int32	m_i32GridWidth;
		PajaTypes::int32	m_i32GridHeight;

		PajaTypes::ColorC	m_rFillColor;
	};

};	// namespace


extern HalftoneGridDescC	g_rHalftoneGridDesc;


#endif	// __HALFTONEGRIDPLUGIN_H__
