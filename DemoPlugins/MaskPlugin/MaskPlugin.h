//
// MaskPlugin.h
//
// Mask Plugin
//
// Copyright (c) 2003 memon/moppi productions
//

#ifndef __MASKPLUGIN_H__
#define __MASKPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "EffectMaskI.h"


//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_IMAGEMASK_EFFECT( 0x9A8E3CDD, 0x859247E5 );
const PluginClass::ClassIdC	CLASS_VALUEFIELDMASK_EFFECT( 0x2C96959C, 0x08004AEB );


//////////////////////////////////////////////////////////////////////////
//
//  Image Mask effect class descriptor.
//

class ImageMaskDescC : public PluginClass::ClassDescC
{
public:
	ImageMaskDescC();
	virtual ~ImageMaskDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  Value Field Mask Desc effect class descriptor.
//

/*
class ValueFieldMaskDescC : public PluginClass::ClassDescC
{
public:
	ValueFieldMaskDescC();
	virtual ~ValueFieldMaskDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};
*/

namespace ImageMaskPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The Image Mask effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_ROT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_IMAGE = 0,
		ID_ATTRIBUTE_SCALE,
		ID_ATTRIBUTE_OFFSET,
		ID_ATTRIBUTE_WRAP,
		ID_ATTRIBUTE_RESAMPLE,
		ATTRIBUTE_COUNT,
	};

	enum ImageMaskEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	enum ResampleModeE {
		RESAMPLEMODE_BILINEAR = 0,
		RESAMPLEMODE_NEAREST = 1,
	};

	enum WrapModeE {
		WRAPMODE_CLAMP = 0,
		WRAPMODE_REPEAT = 1,
	};

	class ImageMaskEffectC : public Composition::EffectMaskI
	{
	public:
		static ImageMaskEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual void				draw_ui( PajaTypes::uint32 ui32Flags );

		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual Import::ImportableImageI*		get_mask_image();
		virtual PajaTypes::int32						get_mask_image_flags();
		virtual const PajaTypes::Matrix2C&	get_mask_matrix() const;

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		ImageMaskEffectC();
		ImageMaskEffectC( Edit::EditableI* pOriginal );
		virtual ~ImageMaskEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::Matrix2C	m_rTextureTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
		Import::ImportableImageI*			m_pMaskImp;
		PajaTypes::int32			m_i32MaskImageFlags;
		PajaTypes::int32			m_i32FileTime;
	};

};	// namespace

/*
namespace ValueFieldMaskPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The Value Field Mask effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_ROT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_IMAGE = 0,
		ID_ATTRIBUTE_SCALE,
		ID_ATTRIBUTE_OFFSET,
		ID_ATTRIBUTE_WRAP,
		ID_ATTRIBUTE_RESAMPLE,
		ATTRIBUTE_COUNT,
	};

	enum ImageMaskEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	enum ResampleModeE {
		RESAMPLEMODE_BILINEAR = 0,
		RESAMPLEMODE_NEAREST = 1,
	};

	enum WrapModeE {
		WRAPMODE_CLAMP = 0,
		WRAPMODE_REPEAT = 1,
	};

	class ValueFieldMaskEffectC : public Composition::EffectMaskI
	{
	public:
		static ValueFieldMaskEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual Import::ImportableImageI*		get_mask_image();
		virtual PajaTypes::int32						get_mask_image_flags();
		virtual PajaTypes::Matrix2C					get_mask_matrix() const;

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		ValueFieldMaskEffectC();
		ValueFieldMaskEffectC( Edit::EditableI* pOriginal );
		virtual ~ValueFieldMaskEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::Matrix2C	m_rTextureTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
	};

};	// namespace
*/

extern ImageMaskDescC				g_rImageMaskDesc;
//extern ValueFieldMaskDescC	g_rValueFieldMaskDesc;


#endif	// __AVIPLUGIN_H__
