#ifndef __GLARE_EXTGL_H__
#define __GLARE_EXTGL_H__

#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include "glext.h"


// Multitexture
extern bool								g_bMultiTexture;
extern PFNGLMULTITEXCOORD1FARBPROC		glMultiTexCoord1fARB;
extern PFNGLMULTITEXCOORD2FARBPROC		glMultiTexCoord2fARB;
extern PFNGLMULTITEXCOORD2FVARBPROC		glMultiTexCoord2fvARB;
extern PFNGLMULTITEXCOORD3FARBPROC		glMultiTexCoord3fARB;
extern PFNGLMULTITEXCOORD4FARBPROC		glMultiTexCoord4fARB;
extern PFNGLACTIVETEXTUREARBPROC		glActiveTextureARB;
extern PFNGLCLIENTACTIVETEXTUREARBPROC	glClientActiveTextureARB;

extern bool								g_bBlendEqu;
extern PFNGLBLENDEQUATIONPROC			glBlendEquation;
extern PFNGLBLENDFUNCSEPARATEEXTPROC	glBlendFuncSeparate;
extern PFNGLBLENDCOLORPROC				glBlendColor;

extern void	init_gl_extension();


#endif;