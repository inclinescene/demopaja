//
// GlarePlugin.cpp
//
// Glare Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

//#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <stdio.h>
#include "glext.h"

#include "extgl.h"

// Demopaja headers
#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "ClassDescC.h"
#include "ImportableI.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "GlarePlugin.h"
#include "FileIO.h"
#include "AutoGizmoC.h"
#include "ImportableImageI.h"
#include "PBufferC.h"
#include "EffectMaskI.h"

using namespace PajaTypes;
using namespace PluginClass;
using namespace PajaSystem;
using namespace Edit;
using namespace Composition;
using namespace Import;
using namespace FileIO;

using namespace GBlurPlugin;


// stuff from GlarePlugin.cpp
/*extern PBufferC*		g_pPBuffer;
extern void				init_pbuffer();
extern void				release_pbuffer();*/


#define			CAPTURE_OFFSET	1


static
void
TRACE( const char* szFormat, ...  )
{
	char	szMsg[256];
	va_list	rList;
	va_start( rList, szFormat );
	_vsnprintf( szMsg, 255, szFormat, rList );
	va_end( rList );
	OutputDebugString( szMsg );
}

static
bool
CHECK_GL_ERROR( const char* szName )
{
	GLenum	eError = glGetError();
	if( eError ) {
		OutputDebugString( szName );
		OutputDebugString( " " );
		OutputDebugString( (const char*)gluErrorString( eError ) );
		OutputDebugString( "\n" );
		return true;
	}
	return false;
}


inline
uint32
lowest_bit_mask( uint32 v )
{
	return (v & -v);
}

static
uint32
ceil_power2( uint32 ui32Num )
{
	uint32	i = lowest_bit_mask( ui32Num );
	while( i < ui32Num )
		i <<= 1;
	return i;
}



//////////////////////////////////////////////////////////////////////////
//
//  blur effect class descriptor.
//

GBlurDescC::GBlurDescC()
{
	// empty
}

GBlurDescC::~GBlurDescC()
{
	// empty
}

void*
GBlurDescC::create()
{
	return (void*)GBlurEffectC::create_new();
}

int32
GBlurDescC::get_classtype() const
{
	return CLASS_TYPE_EFFECT;
}

SuperClassIdC
GBlurDescC::get_super_class_id() const
{
	return SUPERCLASS_EFFECT;
}

ClassIdC
GBlurDescC::get_class_id() const
{
	return CLASS_GBLUR_EFFECT;
}

const char*
GBlurDescC::get_name() const
{
	return "GBlur";
}

const char*
GBlurDescC::get_desc() const
{
	return "GBlur Effect";
}

const char*
GBlurDescC::get_author_name() const
{
	return "Mikko \"memon\" Mononen";
}

const char*
GBlurDescC::get_copyright_message() const
{
	return "Copyright (c) 2003 Moppi Productions";
}

const char*
GBlurDescC::get_url() const
{
	return "http://moppi.inside.org/demopaja/";
}

const char*
GBlurDescC::get_help_filename() const
{
	return "res://GlareHelp.html";
}

uint32
GBlurDescC::get_required_device_driver_count() const
{
	return 1;
}

const ClassIdC&
GBlurDescC::get_required_device_driver( uint32 ui32Idx )
{
	return PajaSystem::CLASS_OPENGL_DEVICEDRIVER;
}


uint32
GBlurDescC::get_ext_count() const
{
	return 0;
}

const char*
GBlurDescC::get_ext( uint32 ui32Index ) const
{
	return 0;
}


//////////////////////////////////////////////////////////////////////////
//
// Global descriptors, which will be returned to the Demopaja.
//

GBlurDescC	g_rGBlurDesc;

//////////////////////////////////////////////////////////////////////////
//
// The GBlur effect
//

//uint32	GBlurEffectC::m_ui32WhiteTextureID = 0;
//uint32	GBlurEffectC::m_ui32BlackTextureID = 0;
//int32		GBlurEffectC::m_i32TextureRefCount = 0;

GBlurEffectC::GBlurEffectC() :
//	m_ui32TexId( 0 ),
//	m_ui32TexWidth( 0 ),
//	m_ui32TexHeight( 0 ),
	m_i32TexCapturedWidth( 0 ),
	m_i32TexCapturedHeight( 0 )
//	m_bTextureRef( false )
{
//	init_pbuffer();

//	for( int32 i = 0; i < BLUR_TEX_COUNT; i++ )
//		m_ui32BlurTexId[i] = 0;

	//
	// Create Transform gizmo.
	//
	m_pTraGizmo = AutoGizmoC::create_new( this, "Transform", ID_GIZMO_TRANS );

	m_pTraGizmo->add_parameter(	ParamVector2C::create_new( m_pTraGizmo, "Position", Vector2C(), ID_TRANSFORM_POS,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_ABS_POSITION, PARAM_ANIMATABLE ) );
	
	m_pTraGizmo->add_parameter(	ParamVector2C::create_new( m_pTraGizmo, "Pivot", Vector2C(), ID_TRANSFORM_PIVOT,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_REL_POSITION | PARAM_STYLE_WORLD_SPACE, PARAM_ANIMATABLE ) );
	
	m_pTraGizmo->add_parameter(	ParamVector2C::create_new( m_pTraGizmo, "Scale", Vector2C( 1, 1 ), ID_TRANSFORM_SCALE,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_PERCENT, PARAM_ANIMATABLE, Vector2C(), Vector2C(), 0.01f ) );


	//
	// Create Attributes gizmo.
	//
	m_pAttGizmo = AutoGizmoC::create_new( this, "Attributes", ID_GIZMO_ATTRIB );

	// GBlur Size
	m_pAttGizmo->add_parameter(	ParamFloatC::create_new( m_pAttGizmo, "Size", 0.1f, ID_ATTRIBUTE_SIZE,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_PERCENT, PARAM_ANIMATABLE, 0, 1.0f, 0.01f ) );

	// GBlur Amount
	m_pAttGizmo->add_parameter(	ParamFloatC::create_new( m_pAttGizmo, "Opacity", 1.0f, ID_ATTRIBUTE_OPACITY,
						PARAM_STYLE_EDITBOX | PARAM_STYLE_PERCENT, PARAM_ANIMATABLE, 0, 1.0f, 0.01f ) );

	// Blend
	ParamIntC*	pBlendMode = ParamIntC::create_new( m_pAttGizmo, "Blend", 0, ID_ATTRIBUTE_BLEND, PARAM_STYLE_COMBOBOX, PARAM_ANIMATABLE, 0, 5 );
	pBlendMode->add_label( 0, "Normal" );
	pBlendMode->add_label( 1, "Add" );
	pBlendMode->add_label( 2, "Mult" );
	pBlendMode->add_label( 3, "Sub" );
	pBlendMode->add_label( 4, "Lighten" );
	pBlendMode->add_label( 5, "Darken" );
	m_pAttGizmo->add_parameter( pBlendMode );

	// Mask
	m_pAttGizmo->add_parameter(	ParamLinkC::create_new( m_pAttGizmo, "Mask", SUPERCLASS_EFFECT_MASK, NULL_CLASSID,
						ID_ATTRIBUTE_MASK, PARAM_STYLE_SINGLE_LINK ) );
}

GBlurEffectC::GBlurEffectC( EditableI* pOriginal ) :
//	EffectI( pOriginal ),
	EffectBlurI( pOriginal ),
	m_pTraGizmo( 0 ),
	m_pAttGizmo( 0 ),
//	m_ui32TexId( 0 ),
//	m_ui32TexWidth( 0 ),
//	m_ui32TexHeight( 0 ),
	m_i32TexCapturedWidth( 0 ),
	m_i32TexCapturedHeight( 0 )
//	m_bTextureRef( false )
{
	// The parameters are not created in the clone constructor.
//	init_pbuffer();

//	for( int32 i = 0; i < BLUR_TEX_COUNT; i++ )
//		m_ui32BlurTexId[i] = 0;
}

GBlurEffectC::~GBlurEffectC()
{

	TRACE( "~GBlurEffectC()\n" );

//	release_pbuffer();

	// Return if this is a clone.
	if( get_original() )
		return;

/*	if( m_bTextureRef )
	{
		release_textures();
		m_bTextureRef = false;
	}


	if( m_ui32TexId ) {
		glDeleteTextures( 1, &m_ui32TexId );
		m_ui32TexId = 0;
	}

	for( int32 i = 0; i < BLUR_TEX_COUNT; i++ ) {
		if( m_ui32BlurTexId[i] ) {
			glDeleteTextures( 1, &m_ui32BlurTexId[i] );
			m_ui32BlurTexId[i] = 0;
		}
	}
*/

	// Release gizmos.
	m_pTraGizmo->release();
	m_pAttGizmo->release();
}

GBlurEffectC*
GBlurEffectC::create_new()
{
	return new GBlurEffectC;
}

DataBlockI*
GBlurEffectC::create()
{
	return new GBlurEffectC;
}

DataBlockI*
GBlurEffectC::create( EditableI* pOriginal )
{
	return new GBlurEffectC( pOriginal );
}

void
GBlurEffectC::copy( EditableI* pEditable )
{
	EffectI::copy( pEditable );

	// Make deep copy.
	GBlurEffectC*	pEffect = (GBlurEffectC*)pEditable;
	m_pTraGizmo->copy( pEffect->m_pTraGizmo );
	m_pAttGizmo->copy( pEffect->m_pAttGizmo );
}

void
GBlurEffectC::restore( EditableI* pEditable )
{
	EffectI::restore( pEditable );

	// Make shallow copy.
	GBlurEffectC*	pEffect = (GBlurEffectC*)pEditable;
	m_pTraGizmo = pEffect->m_pTraGizmo;
	m_pAttGizmo = pEffect->m_pAttGizmo;
}

int32
GBlurEffectC::get_gizmo_count()
{
	// Return number of gizmos inside this effect.
	return GIZMO_COUNT;
}

GizmoI*
GBlurEffectC::get_gizmo( PajaTypes::int32 i32Index )
{
	// Returns specified gizmo.
	// Since the ID's are zero based, we can use them as indices.
	switch( i32Index ) {
	case ID_GIZMO_TRANS:
		return m_pTraGizmo;
	case ID_GIZMO_ATTRIB:
		return m_pAttGizmo;
	}

	return 0;
}

ClassIdC
GBlurEffectC::get_class_id()
{
	// Return the class ID. Should be same as in the class descriptor.
	return CLASS_GBLUR_EFFECT;
}

const char*
GBlurEffectC::get_class_name()
{
	// Return the class name. Should be same as in the class descriptor.
	return "GBlur";
}

void
GBlurEffectC::set_default_file( int32 i32Time, FileHandleC* pHandle )
{
	// empty
}

ParamI*
GBlurEffectC::get_default_param( int32 i32Param )
{
	// Return specified default parameter.
	if( i32Param == DEFAULT_PARAM_POSITION )
		return m_pTraGizmo->get_parameter( ID_TRANSFORM_POS );
	else if( i32Param == DEFAULT_PARAM_SCALE )
		return m_pTraGizmo->get_parameter( ID_TRANSFORM_SCALE );
	else if( i32Param == DEFAULT_PARAM_PIVOT )
		return m_pTraGizmo->get_parameter( ID_TRANSFORM_PIVOT );
	return 0;
}

void
GBlurEffectC::initialize( uint32 ui32Reason, DemoInterfaceC* pInterface )
{
	EffectI::initialize( ui32Reason, pInterface );

	DeviceContextC* pContext = m_pDemoInterface->get_device_context();
	TimeContextC* pTimeContext = m_pDemoInterface->get_time_context();

	OpenGLDeviceC*	pDevice = (OpenGLDeviceC*)pContext->query_interface( CLASS_OPENGL_DEVICEDRIVER );
	if( !pDevice )
		return;

/*	if( ui32Reason == INIT_DEVICE_CHANGED ) {

		if( pDevice->get_state() == DEVICE_STATE_SHUTTINGDOWN ) {
			// Delete textures
			if( m_ui32TexId ) {
				glDeleteTextures( 1, &m_ui32TexId );
				m_ui32TexId = 0;
			}
		}

	}
	else*/
	if( ui32Reason == INIT_INITIAL_UPDATE ) {

		init_gl_extension();

//		init_textures();

		// Get the OpenGL viewport.
		OpenGLViewportC*	pViewport = (OpenGLViewportC*)pDevice->query_interface( GRAPHICSDEVICE_VIEWPORT_INTERFACE );
		if( !pViewport )
			return;

		init_resources( pViewport->get_width(), pViewport->get_height() );
//		check_buffer_size( pViewport->get_width(), pViewport->get_height() );
	}
}


void
GBlurEffectC::make_blur_textures()
{
	int32	i;

	if( !m_pPBuffer || !m_pPBuffer->is_initialised() )
		return;

	// if multitexture is not supported, die out
	if( !g_bMultiTexture )
		return;

	if( !m_pPBuffer->begin_draw() ) {
		TRACE( "begin_draw failed\n" );
		return;
	}

	//Setup ortho mode.
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	glOrtho( 0, m_pPBuffer->get_width(), 0, m_pPBuffer->get_height(), -100, 100 );
	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();

	glEnable( GL_BLEND );
	glBlendFunc( GL_CONSTANT_ALPHA_EXT, GL_ONE_MINUS_CONSTANT_ALPHA_EXT );
	glDisable( GL_DEPTH_TEST );
	glDepthMask( GL_FALSE );

	float32	f32A = 0.5f;

	// the texture mix alpha is in color, and the secand mix alpha is in the alpha
	glColor4f( 1, 1, 1, 0.5f );
	glBlendColor( 1, 1, 1, 0.5f );

	for( i = 0; i < BLUR_TEX_COUNT; i++ ) {		

		int32	i32W = (int32)m_ui32TexWidth >> (i + 1);
		int32	i32H = (int32)m_ui32TexHeight >> (i + 1);

//		TRACE( "w: %d  h: %d\n", i32W, i32H );

		float32	f32OX = 1.0f / (float32)i32W;
		float32	f32OY = 1.0f / (float32)i32H;

		uint32	ui32SrcTex;
		if( i > 0 )
			ui32SrcTex = m_ui32BlurTexId[i - 1];
		else
			ui32SrcTex = m_ui32TexId;

		glActiveTextureARB( GL_TEXTURE0_ARB );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32SrcTex );
		CHECK_GL_ERROR( "make blur: tex0 color env (bind)\n" );

		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );


		glActiveTextureARB( GL_TEXTURE1_ARB );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32SrcTex );
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_PRIMARY_COLOR_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_ALPHA_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_ALPHA_EXT, GL_SRC_ALPHA );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_ALPHA_EXT, GL_PRIMARY_COLOR_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_ALPHA_EXT, GL_SRC_ALPHA );



		if( i == 0 )
		{
			// Draw some padding to the outer edge

			float32	f32InvWidth = 1.0f / (float32)m_ui32TexWidth;
			float32	f32InvHeight = 1.0f / (float32)m_ui32TexHeight;
			float32	f32TexW = ((float32)m_i32TexCapturedWidth - 1) * f32InvWidth;
			float32	f32TexH = ((float32)m_i32TexCapturedHeight - 1) * f32InvHeight;

			int32	i32CapW = m_i32TexCapturedWidth >> 1;
			int32	i32CapH = m_i32TexCapturedHeight >> 1;

			const int32	i32Extend = 256;


			glDisable( GL_BLEND );
	
			glBegin( GL_QUADS );
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 0 + f32OX, 0 );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 0 - f32OX, 0 );
				glVertex2i( 0, 0 );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 1 + f32OX, 0 );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 1 - f32OX, 0 );
				glVertex2i( i32W, 0 );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 1 + f32OX, 1 );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 1 - f32OX, 1 );
				glVertex2i( i32W, (float32)i32H );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 0 + f32OX, 1 );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 0 - f32OX, 1 );
				glVertex2i( 0, i32H );
			glEnd();

			glEnable( GL_BLEND );
			
			glBegin( GL_QUADS );
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 0, 0 + f32OY );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 0, 0 - f32OY );
				glVertex2i( 0, 0 );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 1, 0 + f32OY );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 1, 0 - f32OY );
				glVertex2i( i32W, 0 );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 1, 1 + f32OY );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 1, 1 - f32OY );
				glVertex2i( i32W, i32H );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 0, 1 + f32OY );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 0, 1 - f32OY );
				glVertex2i( 0, i32H );
			glEnd();


			glDisable( GL_BLEND );

			glBegin( GL_QUADS );

				int32	i32PaddingOffset = -1;
				float32	f32RepW = f32TexW;
				float32	f32RepH = f32TexH;

				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32RepW, 0 );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32RepW, 0 );
				glVertex2i( i32CapW + i32PaddingOffset, 0 );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32RepW, 0 );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32RepW, 0 );
				glVertex2i( i32CapW + i32Extend, 0 );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32RepW, f32TexH + (float32)i32Extend * (2.0f * f32InvHeight) );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32RepW, f32TexH + (float32)i32Extend * (2.0f * f32InvHeight) );
				glVertex2i( i32CapW + i32Extend, i32CapH + i32Extend );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32RepW, f32TexH + (float32)i32Extend * (2.0f * f32InvHeight) );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32RepW, f32TexH + (float32)i32Extend * (2.0f * f32InvHeight) );
				glVertex2i( i32CapW + i32PaddingOffset, i32CapH + i32Extend );

				// top padding
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 0, f32RepH );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 0, f32RepH );
				glVertex2i( 0, i32CapH + i32PaddingOffset );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32TexW + (float32)i32Extend * (2.0f * f32InvWidth), f32RepH );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32TexW + (float32)i32Extend * (2.0f * f32InvWidth), f32RepH );
				glVertex2i( i32CapW + i32Extend, i32CapH + i32PaddingOffset );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32TexW + (float32)i32Extend * (2.0f * f32InvWidth), f32RepH );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32TexW + (float32)i32Extend * (2.0f * f32InvWidth), f32RepH );
				glVertex2i( i32CapW + i32Extend, i32CapH + i32Extend );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 0, f32RepH );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 0, f32RepH );
				glVertex2i( 0, i32CapH + i32Extend );


				// corner
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32RepW, f32RepH );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32RepW, f32RepH );
				glVertex2i( i32CapW + i32PaddingOffset, i32CapH + i32PaddingOffset );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32RepW, f32RepH );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32RepW, f32RepH );
				glVertex2i( i32CapW + i32Extend, i32CapH + i32PaddingOffset );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32RepW, f32RepH );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32RepW, f32RepH );
				glVertex2i( i32CapW + i32Extend, i32CapH + i32Extend );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32RepW, f32RepH );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32RepW, f32RepH );
				glVertex2i( i32CapW + i32PaddingOffset, i32CapH + i32Extend );


			glEnd();
		}
		else
		{
			glDisable( GL_BLEND );
	
			glBegin( GL_QUADS );
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 0 + f32OX, 0 );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 0 - f32OX, 0 );
				glVertex2i( 0, 0 );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 1 + f32OX, 0 );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 1 - f32OX, 0 );
				glVertex2i( i32W, 0 );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 1 + f32OX, 1 );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 1 - f32OX, 1 );
				glVertex2i( i32W, (float32)i32H );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 0 + f32OX, 1 );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 0 - f32OX, 1 );
				glVertex2i( 0, i32H );
			glEnd();

			glEnable( GL_BLEND );
			
			glBegin( GL_QUADS );
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 0, 0 + f32OY );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 0, 0 - f32OY );
				glVertex2i( 0, 0 );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 1, 0 + f32OY );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 1, 0 - f32OY );
				glVertex2i( i32W, 0 );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 1, 1 + f32OY );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 1, 1 - f32OY );
				glVertex2i( i32W, i32H );
				
				glMultiTexCoord2fARB( GL_TEXTURE1_ARB, 0, 1 + f32OY );
				glMultiTexCoord2fARB( GL_TEXTURE0_ARB, 0, 1 - f32OY );
				glVertex2i( 0, i32H );
			glEnd();
		}



		glBindTexture( GL_TEXTURE_2D, m_ui32BlurTexId[i] );

		glCopyTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, 0, 0, i32W, i32H );
	}


	glActiveTextureARB( GL_TEXTURE1_ARB );
	glDisable( GL_TEXTURE_2D );
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

	glActiveTextureARB( GL_TEXTURE0_ARB );
	glDisable( GL_TEXTURE_2D );
	glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

	glDisable( GL_BLEND );
	glEnable( GL_DEPTH_TEST );
	glDepthMask( GL_TRUE );


	m_pPBuffer->end_draw();
}

/*
void
GBlurEffectC::check_buffer_size( int32 i32Width, int32 i32Height )
{
	int32	i;

	// get image from frame buffer
	int32	i32WidthPow2 = ceil_power2( i32Width );
	int32	i32HeightPow2 = ceil_power2( i32Height );

	// Resize pbfueer on demand
	if( g_pPBuffer ) {
		int32	i32PBufWidth = i32WidthPow2 / 2;
		int32	i32PBufHeight = i32HeightPow2 / 2;
		if( !g_pPBuffer->is_initialised() || i32PBufWidth > g_pPBuffer->get_width() || i32PBufHeight > g_pPBuffer->get_height() ) {
			TRACE( "GBlur: pbuffer->init( %d, %d )\n", i32PBufWidth, i32PBufHeight );
			g_pPBuffer->init( i32PBufWidth, i32PBufHeight, PBUFFER_TEXTURE_NONE );
		}
	}

	// Resize texture

	// If the maximum image size has changed, invalidate texture object if necessary
	if( i32WidthPow2 > (int32)m_ui32TexWidth || i32HeightPow2 > (int32)m_ui32TexHeight ) {
		if( m_ui32TexId )
			glDeleteTextures( 1, &m_ui32TexId );
		m_ui32TexId = 0;

		for( i = 0; i < BLUR_TEX_COUNT; i++ ) {
			if( m_ui32BlurTexId[i] )
				glDeleteTextures( 1, &m_ui32BlurTexId[i] );
			m_ui32BlurTexId[i] = 0;
		}
	}

	if( !m_ui32TexId ) {
		glGenTextures( 1, &m_ui32TexId );
		glBindTexture( GL_TEXTURE_2D, m_ui32TexId );
		m_ui32TexWidth = i32WidthPow2;
		m_ui32TexHeight = i32HeightPow2;

		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, m_ui32TexWidth, m_ui32TexHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0 );

		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );

		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
	}


	for( int32 i = 0; i < BLUR_TEX_COUNT; i++ ) {
		if( !m_ui32BlurTexId[i] ) {
			glGenTextures( 1, &m_ui32BlurTexId[i] );
			glBindTexture( GL_TEXTURE_2D, m_ui32BlurTexId[i] );
			int32	i32BlurTexWidth = (int32)m_ui32TexWidth >> (i + 1);
			int32	i32BlurTexHeight = (int32)m_ui32TexHeight >> (i + 1);
			glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, i32BlurTexWidth, i32BlurTexHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0 );
			glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
			glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
		}
	}

}
*/

void
GBlurEffectC::eval_state( int32 i32Time )
{
	if( !m_pDemoInterface )
		return;

	// Requires some extensions.
	if( !g_bBlendEqu || !g_bMultiTexture )
		return;

	DeviceContextC* pContext = m_pDemoInterface->get_device_context();
	TimeContextC* pTimeContext = m_pDemoInterface->get_time_context();

	int32		i;
	Matrix2C	rPosMat, rRotMat, rScaleMat, rPivotMat;

	Vector2C	rScale;
	Vector2C	rPos;
	Vector2C	rPivot;

	// Get parameters which affect the transformation.
	((ParamVector2C*)m_pTraGizmo->get_parameter( ID_TRANSFORM_POS ))->get_val( i32Time, rPos );
	((ParamVector2C*)m_pTraGizmo->get_parameter( ID_TRANSFORM_PIVOT ))->get_val( i32Time, rPivot );
	((ParamVector2C*)m_pTraGizmo->get_parameter( ID_TRANSFORM_SCALE ))->get_val( i32Time, rScale );

	// Calculate transformation matrix.
	rPivotMat.set_trans( rPivot );
	rPosMat.set_trans( rPos );
	rScaleMat.set_scale( rScale ) ;
	m_rTM = rPivotMat * rScaleMat * rPosMat;

	float32		f32Width = 25;
	float32		f32Height = 25;
	Vector2C	rMin, rMax;
	Vector2C	rVec;

	// Calcualte vertices of the rectangle.
	m_rVertices[0][0] = -f32Width;		// top-left
	m_rVertices[0][1] = -f32Height;

	m_rVertices[1][0] =  f32Width;		// top-right
	m_rVertices[1][1] = -f32Height;

	m_rVertices[2][0] =  f32Width;		// bottom-right
	m_rVertices[2][1] =  f32Height;

	m_rVertices[3][0] = -f32Width;		// bottom-left
	m_rVertices[3][1] =  f32Height;

	// Calculate bounding box
	for( i = 0; i < 4; i++ ) {
		rVec = m_rTM * m_rVertices[i];
		m_rVertices[i] = rVec;

		if( !i )
			rMin = rMax = rVec;
		else {
			if( rVec[0] < rMin[0] ) rMin[0] = rVec[0];
			if( rVec[1] < rMin[1] ) rMin[1] = rVec[1];
			if( rVec[0] > rMax[0] ) rMax[0] = rVec[0];
			if( rVec[1] > rMax[1] ) rMax[1] = rVec[1];
		}
	}

	// Store bounding box.
	m_rBBox[0] = rMin;
	m_rBBox[1] = rMax;

	float32	f32Size = 1.0f;
	float32	f32Opacity = 1.0f;
	int32			i32Blend = BLENDMODE_NORMAL;

	// Get Size
	((ParamFloatC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_SIZE ))->get_val( i32Time, f32Size );
	// Get Opacity.
	((ParamFloatC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_OPACITY ))->get_val( i32Time, f32Opacity );
	// Get Blend mode.
	((ParamIntC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_BLEND ))->get_val( i32Time, i32Blend );

	// Mask
	ParamLinkC*				pMaskParam = (ParamLinkC*)m_pAttGizmo->get_parameter_by_id( ID_ATTRIBUTE_MASK );
	EffectMaskI*			pMaskEffect = (EffectMaskI*)pMaskParam->get_link( 0 );
	ImportableImageI*	pMaskImp = 0;
	int32							i32MaskImageFlags = 0;
	Matrix2C					rMaskMat;
	if( pMaskEffect )
	{
		pMaskImp = pMaskEffect->get_mask_image();
		i32MaskImageFlags = pMaskEffect->get_mask_image_flags();
		rMaskMat = pMaskEffect->get_mask_matrix();
	}

	//
	// draw effect
	//

	// Get the OpenGL device.
	OpenGLDeviceC*	pDevice = (OpenGLDeviceC*)pContext->query_interface( CLASS_OPENGL_DEVICEDRIVER );
	if( !pDevice )
		return;

	// Get the OpenGL viewport.
	OpenGLViewportC*	pViewport = (OpenGLViewportC*)pDevice->query_interface( GRAPHICSDEVICE_VIEWPORT_INTERFACE );
	if( !pViewport )
		return;

	// Set orthographic projection.
	pViewport->set_ortho_pixel( m_rBBox );

	// get image from frame buffer
	int32	i32Width = pViewport->get_width();
	int32	i32Height = pViewport->get_height();

//	check_buffer_size( i32Width, i32Height );
	check_blur_buffer_size( i32Width, i32Height );

	// the scale used with size parameter
	Vector2C	rOne( 1, 1 );
	rOne = pViewport->delta_layout_to_client( rOne );

	// calc where we get our image
	Vector2C	rOffset( 4, 4 );
	BBox2C	rCapture;
	rCapture[0] = pViewport->layout_to_client( m_rBBox[0] );
	rCapture[1] = pViewport->layout_to_client( m_rBBox[1] );

	int32	i32X, i32Y;

	i32X = (int32)floor( rCapture[0][0] ) - CAPTURE_OFFSET;
	i32Y = (int32)floor( rCapture[0][1] ) - CAPTURE_OFFSET;

	m_i32TexCapturedWidth = (int32)ceil( rCapture.width() ) + 2 * CAPTURE_OFFSET;
	m_i32TexCapturedHeight = (int32)ceil( rCapture.height() ) + 2 * CAPTURE_OFFSET;

	if( i32X >= i32Width )
		return;
	if( i32X < 0 ) {
		m_i32TexCapturedWidth += i32X;
		i32X = 0;
	}
	if( (i32X + m_i32TexCapturedWidth) >= i32Width )
		m_i32TexCapturedWidth -= (i32X + m_i32TexCapturedWidth) - i32Width;
	if( m_i32TexCapturedWidth <= 0 )
		return;

	if( i32Y >= i32Height )
		return;
	if( i32Y < 0 ) {
		m_i32TexCapturedHeight += i32Y;
		i32Y = 0;
	}
	if( (i32Y + m_i32TexCapturedHeight) >= i32Height )
		m_i32TexCapturedHeight -= (i32Y + m_i32TexCapturedHeight) - i32Height;
	if( m_i32TexCapturedHeight <= 0 )
		return;


	glActiveTextureARB( GL_TEXTURE0_ARB );

	glEnable( GL_TEXTURE_2D );
	glDisable( GL_DEPTH_TEST );
	glDepthMask( GL_FALSE );
	glEnable( GL_BLEND );

	glBindTexture( GL_TEXTURE_2D, m_ui32TexId );

	glCopyTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, i32X, i32Y, m_i32TexCapturedWidth, m_i32TexCapturedHeight );


	// make blur textures
	make_blur_textures();

	float32	f32TexX0 = 0;
	float32	f32TexY0 = 0;
	float32	f32TexX1 = (float32)m_i32TexCapturedWidth;
	float32	f32TexY1 = (float32)m_i32TexCapturedHeight;

	float32	f32InvTexWidth = 1.0f/ (float32)(m_ui32TexWidth);
	float32	f32InvTexHeight = 1.0f/ (float32)(m_ui32TexHeight);

	f32TexX0 *= f32InvTexWidth;
	f32TexY0 *= f32InvTexHeight;
	f32TexX1 *= f32InvTexWidth;
	f32TexY1 *= f32InvTexHeight;

	// draw GBlur

	uint32	ui32TexId0 = 0;
	uint32	ui32TexId1 = 0;

	float32	f32LogSize = (float32)(log( f32Size * 20.0f * rOne[0] ) / log( 2.0f ));
	int32	i32Idx0 = (int32)floor( f32LogSize );
	float32	f32A = 1.0f - (f32LogSize - (float32)i32Idx0);
	int32	i32Idx1 = i32Idx0 + 1;

	i32Idx0 = __max( __min( i32Idx0, BLUR_TEX_COUNT ), 0 );
	i32Idx1 = __max( __min( i32Idx1, BLUR_TEX_COUNT ), 0 );

	if( i32Idx0 == 0 )
		ui32TexId0 = m_ui32TexId;
	else if( i32Idx0 <= BLUR_TEX_COUNT )
		ui32TexId0 = m_ui32BlurTexId[i32Idx0 - 1];

	if( i32Idx1 == 0 )
		ui32TexId1 = m_ui32TexId;
	else if( i32Idx1 <= BLUR_TEX_COUNT )
		ui32TexId1 = m_ui32BlurTexId[i32Idx1 - 1];


	int32	i32TextureUnit = 0;
	int32	i32MaskUnit = -1;
	int32	i32TempUnit = -1;


	if( i32Blend == BLENDMODE_NORMAL )
	{
		// Alpha is the destination blend
		glColor4f( 1, 1, 1, f32Opacity );

		// Put blend factor in texture env constant color.
		float32	f32EnvColor[4] = { 1, 1, 1, 1 };
		f32EnvColor[3] = f32A;

		// First blur texture
		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32TexId0 );
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
		i32TextureUnit++;

		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32TexId1 );

		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );
		glTexEnvfv( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, f32EnvColor );
		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_CONSTANT_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PRIMARY_COLOR_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );

		/*		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_ALPHA_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_ALPHA_EXT, GL_SRC_ALPHA );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_ALPHA_EXT, GL_CONSTANT_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_ALPHA_EXT, GL_SRC_ALPHA );*/


		i32TextureUnit++;


		// Mask
		if( pMaskImp ) {
			glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
			pMaskImp->bind_texture( pDevice, i32TextureUnit, i32MaskImageFlags );
			glEnable( GL_TEXTURE_2D );

			// Pass color from previous texture stage
			glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

			glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_REPLACE );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );

			// Modulate alpha
			glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_MODULATE );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_TEXTURE );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_ALPHA_EXT, GL_PREVIOUS_EXT );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_ALPHA_EXT, GL_SRC_ALPHA );

			i32MaskUnit = i32TextureUnit;
			i32TextureUnit++;
		}

/*		// Transparency unit
		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glBindTexture( GL_TEXTURE_2D, m_ui32WhiteTextureID );
		glEnable( GL_TEXTURE_2D );

		// Pass color from previous texture stage
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_REPLACE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );

		// Modulate alpha
		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_MODULATE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_ALPHA_EXT, GL_PRIMARY_COLOR_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_ALPHA_EXT, GL_SRC_ALPHA );

		i32TextureUnit++;
*/

		glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	}
	else if( i32Blend == BLENDMODE_ADD )
	{
		// Alpha is the destination blend
		glColor4f( 1, 1, 1, f32Opacity );

		// Put blend factor in texture env constant color.
		float32	f32EnvColor[4] = { 1, 1, 1, 1 };
		f32EnvColor[3] = f32A;

		// First blur texture
		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32TexId0 );
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
		i32TextureUnit++;

		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32TexId1 );

		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );
		glTexEnvfv( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, f32EnvColor );
		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_CONSTANT_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PRIMARY_COLOR_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );

		i32TextureUnit++;


		// Mask
		if( pMaskImp ) {
			glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
			pMaskImp->bind_texture( pDevice, i32TextureUnit, i32MaskImageFlags );
			glEnable( GL_TEXTURE_2D );

			// Pass color from previous texture stage
			glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

			glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_REPLACE );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );

			// Modulate alpha
			glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_MODULATE );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_TEXTURE );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_ALPHA_EXT, GL_PREVIOUS_EXT );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_ALPHA_EXT, GL_SRC_ALPHA );

			i32MaskUnit = i32TextureUnit;
			i32TextureUnit++;
		}

/*		// Transparency unit
		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glBindTexture( GL_TEXTURE_2D, m_ui32WhiteTextureID );
		glEnable( GL_TEXTURE_2D );

		// Pass color from previous texture stage
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_REPLACE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );

		// Modulate alpha
		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_MODULATE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_ALPHA_EXT, GL_PRIMARY_COLOR_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_ALPHA_EXT, GL_SRC_ALPHA );

		i32TextureUnit++;
*/
		glBlendFunc( GL_SRC_ALPHA, GL_ONE );
	}
	else if( i32Blend == BLENDMODE_MULT )
	{
		// Alpha is the destination blend
		glColor4f( 1, 1, 1, f32Opacity );

		// Put blend factor in texture env constant color.
		float32	f32EnvColor[4] = { 1, 1, 1, 1 };
		f32EnvColor[3] = f32A;

		// First blur texture
		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32TexId0 );
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
		i32TextureUnit++;

		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32TexId1 );

		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );
		glTexEnvfv( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, f32EnvColor );
		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_CONSTANT_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PRIMARY_COLOR_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );

		i32TextureUnit++;


		// Mask
		if( pMaskImp ) {
			glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
			pMaskImp->bind_texture( pDevice, i32TextureUnit, i32MaskImageFlags );
			glEnable( GL_TEXTURE_2D );

			// Pass color from previous texture stage
			glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

			glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_REPLACE );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );

			// Modulate alpha
			glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_MODULATE );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_TEXTURE );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_ALPHA_EXT, GL_PREVIOUS_EXT );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_ALPHA_EXT, GL_SRC_ALPHA );

			i32MaskUnit = i32TextureUnit;
			i32TextureUnit++;
		}

		// Transparency unit
		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glBindTexture( GL_TEXTURE_2D, m_ui32WhiteTextureID );
		glEnable( GL_TEXTURE_2D );

		// Pass color from previous texture stage
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA );

		// Modulate alpha
		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );

		i32TextureUnit++;

		glBlendFunc( GL_DST_COLOR, GL_ZERO );
	}
	else if( i32Blend == BLENDMODE_SUB )
	{
		// Alpha is the destination blend
		glColor4f( 1, 1, 1, f32Opacity );

		// Put blend factor in texture env constant color.
		float32	f32EnvColor[4] = { 1, 1, 1, 1 };
		f32EnvColor[3] = f32A;

		// First blur texture
		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32TexId0 );
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
		i32TextureUnit++;

		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32TexId1 );

		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );
		glTexEnvfv( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, f32EnvColor );
		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_CONSTANT_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PRIMARY_COLOR_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );

		i32TextureUnit++;


		// Mask
		if( pMaskImp ) {
			glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
			pMaskImp->bind_texture( pDevice, i32TextureUnit, i32MaskImageFlags );
			glEnable( GL_TEXTURE_2D );

			// Pass color from previous texture stage
			glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

			glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_REPLACE );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );

			// Modulate alpha
			glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_MODULATE );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_TEXTURE );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_ALPHA_EXT, GL_PREVIOUS_EXT );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_ALPHA_EXT, GL_SRC_ALPHA );

			i32MaskUnit = i32TextureUnit;
			i32TextureUnit++;
		}

/*		// Transparency unit
		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glBindTexture( GL_TEXTURE_2D, m_ui32WhiteTextureID );
		glEnable( GL_TEXTURE_2D );

		// Pass color from previous texture stage
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA );

		// Modulate alpha
		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );*/

		i32TextureUnit++;

		glBlendEquation( GL_FUNC_REVERSE_SUBTRACT_EXT );
		glBlendFunc( GL_SRC_ALPHA, GL_ONE );
	}
	else if( i32Blend == BLENDMODE_LIGHTEN )
	{
		// Alpha is the destination blend
		glColor4f( 1, 1, 1, f32Opacity );

		// Put blend factor in texture env constant color.
		float32	f32EnvColor[4] = { 1, 1, 1, 1 };
		f32EnvColor[3] = f32A;

		// First blur texture
		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32TexId0 );
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
		i32TextureUnit++;

		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32TexId1 );

		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );
		glTexEnvfv( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, f32EnvColor );
		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_CONSTANT_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PRIMARY_COLOR_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );

		i32TextureUnit++;


		// Mask
		if( pMaskImp ) {
			glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
			pMaskImp->bind_texture( pDevice, i32TextureUnit, i32MaskImageFlags );
			glEnable( GL_TEXTURE_2D );

			// Pass color from previous texture stage
			glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

			glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_REPLACE );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );

			// Modulate alpha
			glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_MODULATE );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_TEXTURE );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_ALPHA_EXT, GL_PREVIOUS_EXT );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_ALPHA_EXT, GL_SRC_ALPHA );

			i32MaskUnit = i32TextureUnit;
			i32TextureUnit++;
		}

		// Transparency unit
		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glBindTexture( GL_TEXTURE_2D, m_ui32BlackTextureID );
		glEnable( GL_TEXTURE_2D );

		// Pass color from previous texture stage
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA );

		// Modulate alpha
		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );

		i32TextureUnit++;

		glBlendEquation( GL_MAX );
		glBlendFunc( GL_ONE, GL_ONE );
	}
	else if( i32Blend == BLENDMODE_DARKEN )
	{
		// Alpha is the destination blend
		glColor4f( 1, 1, 1, f32Opacity );

		// Put blend factor in texture env constant color.
		float32	f32EnvColor[4] = { 1, 1, 1, 1 };
		f32EnvColor[3] = f32A;

		// First blur texture
		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32TexId0 );
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE );
		i32TextureUnit++;

		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glEnable( GL_TEXTURE_2D );
		glBindTexture( GL_TEXTURE_2D, ui32TexId1 );

		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );
		glTexEnvfv( GL_TEXTURE_ENV, GL_TEXTURE_ENV_COLOR, f32EnvColor );
		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_CONSTANT_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PRIMARY_COLOR_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );

		i32TextureUnit++;


		// Mask
		if( pMaskImp ) {
			glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
			pMaskImp->bind_texture( pDevice, i32TextureUnit, i32MaskImageFlags );
			glEnable( GL_TEXTURE_2D );

			// Pass color from previous texture stage
			glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

			glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_REPLACE );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );

			// Modulate alpha
			glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_MODULATE );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_TEXTURE );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );
			glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_ALPHA_EXT, GL_PREVIOUS_EXT );
			glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_ALPHA_EXT, GL_SRC_ALPHA );

			i32MaskUnit = i32TextureUnit;
			i32TextureUnit++;
		}

		// Transparency unit
		glActiveTextureARB( GL_TEXTURE0_ARB + i32TextureUnit );
		glBindTexture( GL_TEXTURE_2D, m_ui32WhiteTextureID );
		glEnable( GL_TEXTURE_2D );

		// Pass color from previous texture stage
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE_EXT );

		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB_EXT, GL_INTERPOLATE_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB_EXT, GL_TEXTURE );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB_EXT, GL_SRC_COLOR );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB_EXT, GL_SRC_ALPHA );

		// Modulate alpha
		glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_ALPHA_EXT, GL_REPLACE );
		glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_ALPHA_EXT, GL_PREVIOUS_EXT );
		glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_ALPHA_EXT, GL_SRC_ALPHA );

		i32TextureUnit++;

		glBlendEquation( GL_MIN );
		glBlendFunc( GL_ONE, GL_ONE );
	}



	// Calc mask coordinates
	Vector2C	rMaskTexCoords[4];

	// Mask Texcoords
	rMaskTexCoords[0] = pViewport->client_to_layout( Vector2C( (float32)i32X, (float32)i32Y ) ) * rMaskMat;
	rMaskTexCoords[1] = pViewport->client_to_layout( Vector2C( (float32)(i32X + m_i32TexCapturedWidth), (float32)i32Y ) ) * rMaskMat;
	rMaskTexCoords[2] = pViewport->client_to_layout( Vector2C( (float32)(i32X + m_i32TexCapturedWidth), (float32)(i32Y + m_i32TexCapturedHeight) ) ) * rMaskMat;
	rMaskTexCoords[3] = pViewport->client_to_layout( Vector2C( (float32)i32X, (float32)(i32Y + m_i32TexCapturedHeight) ) ) * rMaskMat;

	glBegin( GL_QUADS );

	if( i32MaskUnit != -1 )
		glMultiTexCoord2fARB( i32MaskUnit, rMaskTexCoords[0][0], rMaskTexCoords[0][1] );
	glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32TexX0, f32TexY0 );
	glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32TexX0, f32TexY0 );
	glVertex2f( (float32)i32X, (float32)i32Y );

	if( i32MaskUnit != -1 )
		glMultiTexCoord2fARB( i32MaskUnit, rMaskTexCoords[1][0], rMaskTexCoords[1][1] );
	glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32TexX1, f32TexY0 );
	glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32TexX1, f32TexY0 );
	glVertex2f( (float32)(i32X + m_i32TexCapturedWidth), (float32)i32Y );
	
	if( i32MaskUnit != -1 )
		glMultiTexCoord2fARB( i32MaskUnit, rMaskTexCoords[2][0], rMaskTexCoords[2][1] );
	glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32TexX1, f32TexY1 );
	glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32TexX1, f32TexY1 );
	glVertex2f( (float32)(i32X + m_i32TexCapturedWidth), (float32)(i32Y + m_i32TexCapturedHeight) );
	
	if( i32MaskUnit != -1 )
		glMultiTexCoord2fARB( i32MaskUnit, rMaskTexCoords[3][0], rMaskTexCoords[3][1] );
	glMultiTexCoord2fARB( GL_TEXTURE1_ARB, f32TexX0, f32TexY1 );
	glMultiTexCoord2fARB( GL_TEXTURE0_ARB, f32TexX0, f32TexY1 );
	glVertex2f( (float32)i32X, (float32)(i32Y + m_i32TexCapturedHeight) );

	glEnd();


	glEnable( GL_DEPTH_TEST );
	glDisable( GL_BLEND );
	glDepthMask( GL_TRUE );

	// Reset blend equ
	glBlendEquation( GL_FUNC_ADD );

	// Reset to default texture unit
	for( i = 0; i < i32TextureUnit; i++ )
	{
		glActiveTextureARB( GL_TEXTURE0_ARB + i );
		glDisable( GL_TEXTURE_2D );
		glTexEnvf( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
	}

}

/*
void
GBlurEffectC::init_textures()
{
	if( m_i32TextureRefCount == 0 )
	{
		TRACE( "init textures\n" );

		uint8	ui8WhiteTextureData[(4 * 4) * 3] = {
			255, 255, 255,	255, 255, 255,	255, 255, 255,	255, 255, 255,
			255, 255, 255,	255, 255, 255,	255, 255, 255,	255, 255, 255,
			255, 255, 255,	255, 255, 255,	255, 255, 255,	255, 255, 255,
			255, 255, 255,	255, 255, 255,	255, 255, 255,	255, 255, 255,
		};

		uint8	ui8BlackTextureData[(4 * 4) * 3] = {
			0, 0, 0,	0, 0, 0,	0, 0, 0,	0, 0, 0,
			0, 0, 0,	0, 0, 0,	0, 0, 0,	0, 0, 0,
			0, 0, 0,	0, 0, 0,	0, 0, 0,	0, 0, 0,
			0, 0, 0,	0, 0, 0,	0, 0, 0,	0, 0, 0,
		};

		// Create textures
		glGenTextures( 1, &m_ui32WhiteTextureID );
		glBindTexture( GL_TEXTURE_2D, m_ui32WhiteTextureID );
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB8, 4, 4, 0, GL_RGB, GL_UNSIGNED_BYTE, ui8WhiteTextureData );

		glGenTextures( 1, &m_ui32BlackTextureID );
		glBindTexture( GL_TEXTURE_2D, m_ui32BlackTextureID );
		glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
		glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
		glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB8, 4, 4, 0, GL_RGB, GL_UNSIGNED_BYTE, ui8BlackTextureData );
	}

	m_i32TextureRefCount++;

	m_bTextureRef = true;
}

void
GBlurEffectC::release_textures()
{
	m_i32TextureRefCount--;
	if( m_i32TextureRefCount == 0 )
	{
		// Delete textures
		if( m_ui32WhiteTextureID )
			glDeleteTextures( 1, &m_ui32WhiteTextureID );
		if( m_ui32BlackTextureID )
			glDeleteTextures( 1, &m_ui32BlackTextureID );
	}
}
*/

BBox2C
GBlurEffectC::get_bbox()
{
	// Return the bounding box.
	return m_rBBox;
}

const Matrix2C&
GBlurEffectC::get_transform_matrix() const
{
	// Return the trnasformation matrix.
	return m_rTM;
}

bool
GBlurEffectC::hit_test( const Vector2C& rPoint )
{
	// Point in polygon test.
	// from c.g.a FAQ
	int		i, j;
	bool	bInside = false;

	for( i = 0, j = 4 - 1; i < 4; j = i++ ) {
		if( ( ((m_rVertices[i][1] <= rPoint[1]) && (rPoint[1] < m_rVertices[j][1])) ||
			((m_rVertices[j][1] <= rPoint[1]) && (rPoint[1] < m_rVertices[i][1])) ) &&
			(rPoint[0] < (m_rVertices[j][0] - m_rVertices[i][0]) * (rPoint[1] - m_rVertices[i][1]) / (m_rVertices[j][1] - m_rVertices[i][1]) + m_rVertices[i][0]) )

			bInside = !bInside;
	}

	return bInside;
}


enum GBlurEffectChunksE {
	CHUNK_GBLUR_BASE =			0x1000,
	CHUNK_GBLUR_TRANSGIZMO =		0x2000,
	CHUNK_GBLUR_ATTRIBGIZMO =	0x3000,
};

const uint32	GBLUR_VERSION = 1;

uint32
GBlurEffectC::save( SaveC* pSave )
{
	uint32	ui32Error = IO_OK;

	// EffectI base class
	pSave->begin_chunk( CHUNK_GBLUR_BASE, GBLUR_VERSION );
		ui32Error = EffectI::save( pSave );
	pSave->end_chunk();

	// Transform
	pSave->begin_chunk( CHUNK_GBLUR_TRANSGIZMO, GBLUR_VERSION );
		ui32Error = m_pTraGizmo->save( pSave );
	pSave->end_chunk();

	// Attribute
	pSave->begin_chunk( CHUNK_GBLUR_ATTRIBGIZMO, GBLUR_VERSION );
		ui32Error = m_pAttGizmo->save( pSave );
	pSave->end_chunk();

	return ui32Error;
}

uint32
GBlurEffectC::load( LoadC* pLoad )
{
	uint32	ui32Error = IO_OK;

	while( (ui32Error = pLoad->open_chunk()) == IO_OK ) {

		switch( pLoad->get_chunk_id() ) {
		case CHUNK_GBLUR_BASE:
			// EffectI base class
			if( pLoad->get_chunk_version() == GBLUR_VERSION )
				ui32Error = EffectI::load( pLoad );
			break;

		case CHUNK_GBLUR_TRANSGIZMO:
			// Transform
			if( pLoad->get_chunk_version() == GBLUR_VERSION )
				ui32Error = m_pTraGizmo->load( pLoad );
			break;

		case CHUNK_GBLUR_ATTRIBGIZMO:
			// Attribute
			if( pLoad->get_chunk_version() == GBLUR_VERSION )
				ui32Error = m_pAttGizmo->load( pLoad );
			break;

		default:
			assert( 0 );
		}

		pLoad->close_chunk();

		if( ui32Error != IO_OK && ui32Error != IO_END )
			return ui32Error;
	}

	return ui32Error;
}
