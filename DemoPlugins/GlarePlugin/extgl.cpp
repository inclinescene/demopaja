#include <windows.h>
#include <gl\gl.h>
#include <gl\glu.h>
#include <stdio.h>
#include "glext.h"
#include "extgl.h"


// Multitexture
bool							g_bMultiTexture = false;
PFNGLMULTITEXCOORD1FARBPROC		glMultiTexCoord1fARB		= NULL;
PFNGLMULTITEXCOORD2FARBPROC		glMultiTexCoord2fARB		= NULL;
PFNGLMULTITEXCOORD2FVARBPROC	glMultiTexCoord2fvARB		= NULL;
PFNGLMULTITEXCOORD3FARBPROC		glMultiTexCoord3fARB		= NULL;
PFNGLMULTITEXCOORD4FARBPROC		glMultiTexCoord4fARB		= NULL;
PFNGLACTIVETEXTUREARBPROC		glActiveTextureARB			= NULL;
PFNGLCLIENTACTIVETEXTUREARBPROC	glClientActiveTextureARB	= NULL;

bool							g_bBlendEqu = false;
PFNGLBLENDEQUATIONPROC			glBlendEquation = NULL;
PFNGLBLENDCOLORPROC				glBlendColor = NULL;
PFNGLBLENDFUNCSEPARATEEXTPROC	glBlendFuncSeparate = NULL;

void
init_gl_extension()
{
	if( !g_bMultiTexture ) {

		char*	szExtensions;	
		szExtensions = (char*)glGetString( GL_EXTENSIONS );

		if( szExtensions && strstr( szExtensions, "GL_ARB_multitexture" ) != 0 &&
			strstr( szExtensions, "GL_EXT_texture_env_combine" ) != 0 ) {	

			glMultiTexCoord1fARB	 = (PFNGLMULTITEXCOORD1FARBPROC)wglGetProcAddress( "glMultiTexCoord1fARB" );
			glMultiTexCoord2fARB	 = (PFNGLMULTITEXCOORD2FARBPROC)wglGetProcAddress( "glMultiTexCoord2fARB" );
			glMultiTexCoord2fvARB	 = (PFNGLMULTITEXCOORD2FVARBPROC)wglGetProcAddress( "glMultiTexCoord2fvARB" );
			glMultiTexCoord3fARB	 = (PFNGLMULTITEXCOORD3FARBPROC)wglGetProcAddress( "glMultiTexCoord3fARB" );
			glMultiTexCoord4fARB	 = (PFNGLMULTITEXCOORD4FARBPROC)wglGetProcAddress( "glMultiTexCoord4fARB" );
			glActiveTextureARB		 = (PFNGLACTIVETEXTUREARBPROC)wglGetProcAddress( "glActiveTextureARB" );
			glClientActiveTextureARB = (PFNGLCLIENTACTIVETEXTUREARBPROC)wglGetProcAddress( "glClientActiveTextureARB" );
			g_bMultiTexture = true;
		}

	}

	if( !g_bBlendEqu ) {
		char*	szExtensions;	
		szExtensions = (char*)glGetString( GL_EXTENSIONS );

		if( szExtensions &&
			strstr( szExtensions, "GL_EXT_blend_color" ) != 0 &&
			strstr( szExtensions, "GL_EXT_blend_minmax" ) != 0 &&
			strstr( szExtensions, "GL_EXT_blend_subtract" ) != 0 ) {	

			glBlendEquation			 = (PFNGLBLENDEQUATIONPROC)wglGetProcAddress( "glBlendEquationEXT" );
			glBlendColor			 = (PFNGLBLENDCOLORPROC)wglGetProcAddress( "glBlendColorEXT" );

			g_bBlendEqu = true;
		}
	}
}
