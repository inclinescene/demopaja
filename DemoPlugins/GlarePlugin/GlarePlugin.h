//
// GlarePlugin.h
//
// Glare Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

#ifndef __GLAREPLUGIN_H__
#define __GLAREPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "PBufferC.h"


#define	BLUR_TEX_COUNT	5


//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_DEPTHBLUR_EFFECT( 0x1B5779A0, 0xB3024B26 );
const PluginClass::ClassIdC	CLASS_GBLUR_EFFECT( 0x31A6DDBC, 0xD2004214 );
const PluginClass::ClassIdC	CLASS_ENCHANGE_EFFECT( 0x1E93B09D, 0x7D324BAA );


//////////////////////////////////////////////////////////////////////////
//
//  Glare effect class descriptor.
//

class DepthBlurDescC : public PluginClass::ClassDescC
{
public:
	DepthBlurDescC();
	virtual ~DepthBlurDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  GBlur effect class descriptor.
//

class GBlurDescC : public PluginClass::ClassDescC
{
public:
	GBlurDescC();
	virtual ~GBlurDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  Enchange effect class descriptor.
//

class EnchangeDescC : public PluginClass::ClassDescC
{
public:
	EnchangeDescC();
	virtual ~EnchangeDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//
// Class to hold common blur objects.
//

class EffectBlurI : public Composition::EffectI
{
public:

	void		init_resources( PajaTypes::int32 i32Width, PajaTypes::int32 i32Height );
	void		check_blur_buffer_size( PajaTypes::int32 i32Width, PajaTypes::int32 i32Height );
	void		release_resources();

protected:

	//! Default constructor.
	EffectBlurI();
	//! Default constructor with reference to the original.
	EffectBlurI( Edit::EditableI* pOriginal );
	//! Default destructor.
	virtual ~EffectBlurI();

	bool											m_bTextureRef;

	static PBufferC*					m_pPBuffer;
	static PajaTypes::int32		m_i32PBufferRefCount;
	static PajaTypes::uint32	m_ui32BlurTexId[BLUR_TEX_COUNT];
	static PajaTypes::uint32	m_ui32TexId;
	static PajaTypes::uint32	m_ui32TexWidth;
	static PajaTypes::uint32	m_ui32TexHeight;
	static PajaTypes::uint32	m_ui32WhiteTextureID;
	static PajaTypes::uint32	m_ui32BlackTextureID;

};

namespace DepthBlurPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The DepthBlur effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_NEAR = 0,
		ID_ATTRIBUTE_FAR,
		ID_ATTRIBUTE_OPACITY,
		ID_ATTRIBUTE_BLEND,
		ID_ATTRIBUTE_MASK,
		ATTRIBUTE_COUNT,
	};

	enum AVIPlayerEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	enum BlendE {
		BLENDMODE_NORMAL = 0,
		BLENDMODE_ADD,
		BLENDMODE_MULT,
		BLENDMODE_SUB,
		BLENDMODE_LIGHTEN,
		BLENDMODE_DARKEN,
	};

	class DepthBlurEffectC : public EffectBlurI
	{
	public:
		static DepthBlurEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		DepthBlurEffectC();
		DepthBlurEffectC( Edit::EditableI* pOriginal );
		virtual ~DepthBlurEffectC();

	private:

		void	make_blur_textures();
		void	update_range_textures( PajaTypes::float32 f32NearRange, PajaTypes::float32 f32FarRange );

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];

		PajaTypes::int32	m_i32TexCapturedWidth;
		PajaTypes::int32	m_i32TexCapturedHeight;

		PajaTypes::uint32	m_ui32GradientTextures[5];
	};

};	// namespace


namespace GBlurPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The GBlur effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_SIZE = 0,
		ID_ATTRIBUTE_OPACITY,
		ID_ATTRIBUTE_FEEDBACK = 3,		// deprecated
		ID_ATTRIBUTE_BLEND,
		ID_ATTRIBUTE_MASK,
		ATTRIBUTE_COUNT,
	};

	enum AVIPlayerEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	enum BlendE {
		BLENDMODE_NORMAL = 0,
		BLENDMODE_ADD,
		BLENDMODE_MULT,
		BLENDMODE_SUB,
		BLENDMODE_LIGHTEN,
		BLENDMODE_DARKEN,
	};

	class GBlurEffectC : public EffectBlurI //public Composition::EffectI
	{
	public:
		static GBlurEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		GBlurEffectC();
		GBlurEffectC( Edit::EditableI* pOriginal );
		virtual ~GBlurEffectC();

	private:

		void	make_blur_textures();


		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];

		PajaTypes::int32	m_i32TexCapturedWidth;
		PajaTypes::int32	m_i32TexCapturedHeight;
	};

};	// namespace

/*
namespace EnchangePlugin {

//////////////////////////////////////////////////////////////////////////
//
// The GBlur effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_SIZE = 0,
		ID_ATTRIBUTE_AMOUNT,
		ATTRIBUTE_COUNT,
	};

	enum AVIPlayerEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	class EnchangeEffectC : public Composition::EffectI
	{
	public:
		static EnchangeEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32  );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		EnchangeEffectC();
		EnchangeEffectC( Edit::EditableI* pOriginal );
		virtual ~EnchangeEffectC();

	private:

		void	check_buffer_size( PajaTypes::int32 i32Width, PajaTypes::int32 i32Height );
		void	make_blur_textures();

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];

		PajaTypes::uint32	m_ui32BlurTexId[BLUR_TEX_COUNT];
		PajaTypes::uint32	m_ui32TexId;
		PajaTypes::uint32	m_ui32TexWidth;
		PajaTypes::uint32	m_ui32TexHeight;
		PajaTypes::int32	m_i32TexCapturedWidth;
		PajaTypes::int32	m_i32TexCapturedHeight;

		PajaTypes::int32	m_i32ValidTime;
		bool				m_bValidFeedback;

		PajaTypes::float32	m_f32LastTexX0, m_f32LastTexY0, m_f32LastTexX1, m_f32LastTexY1;
		PajaTypes::int32	m_i32LastX, m_i32LastY;
		PajaTypes::int32	m_i32LastWidth, m_i32LastHeight;
		PajaTypes::float32	m_f32LastSize, m_f32LastAmount;

	};

};	// namespace
*/

extern GBlurDescC		g_rGBlurDesc;
extern DepthBlurDescC		g_rDepthBlurDesc;
//extern EnchangeDescC	g_rEnchangeDesc;


#endif	// __AVIPLUGIN_H__
