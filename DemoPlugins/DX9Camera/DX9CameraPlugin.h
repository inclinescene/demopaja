//
// DX9Camera.h
//
// DX9Camera Plugin
//
// Copyright (c) 2004 memon/moppi productions
//

#ifndef __DX9CameraPLUGIN_H__
#define __DX9CameraPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "DX9ViewportC.h"
#include "DX9DeviceC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "ImportableImageI.h"
#include "DX9MeshPlugin.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

#define	CLASS_DX9CAMERA_EFFECT_NAME	"Camera (DX9)"
#define	CLASS_DX9CAMERA_EFFECT_DESC	"Camera (DX9)"

const PluginClass::ClassIdC	CLASS_DX9CAMERA_EFFECT( 0x44F229CE, 0x62E24B7F );


//////////////////////////////////////////////////////////////////////////
//
//  DX9Camera effect class descriptor.
//

class DX9CameraDescC : public PluginClass::ClassDescC
{
public:
	DX9CameraDescC();
	virtual ~DX9CameraDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};

namespace DX9CameraPlugin {

//////////////////////////////////////////////////////////////////////////
//
// DX9Camera effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_SIZE = 0,
		ID_ATTRIBUTE_CAMERA,
		ID_ATTRIBUTE_FOCAL_DISTANCE,
		ID_ATTRIBUTE_FOCAL_RANGE,
		ID_ATTRIBUTE_FOG_TYPE,
		ID_ATTRIBUTE_FOG_START,
		ID_ATTRIBUTE_FOG_END,
		ID_ATTRIBUTE_FOG_COLOR,
		ID_ATTRIBUTE_NOISE_AMOUNT,
		ID_ATTRIBUTE_NOISE_SCALE,
		ID_ATTRIBUTE_FILE,
		ATTRIBUTE_COUNT,
	};

	enum TimeGizmoParamsE {
		ID_TIME_FRAME = 0,
		TIME_COUNT,
	};

	enum EffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		ID_GIZMO_TIME,
		GIZMO_COUNT,
	};

	class DX9CameraEffectC : public Composition::EffectI
	{
	public:
		static DX9CameraEffectC*				create_new();
		virtual Edit::DataBlockI*			create();
		virtual Edit::DataBlockI*			create( Edit::EditableI* pOriginal );
		virtual void									copy( Edit::EditableI* pEditable );
		virtual void									restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32			get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*						get_class_name();

		virtual void									set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual PajaTypes::uint32			update_notify( EditableI* pCaller );

		virtual void									initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void									eval_state( PajaTypes::int32 i32Time );

		virtual LPD3DXMATRIX					get_camera_matrix();
		virtual LPD3DXMATRIX					get_proj_matrix();
		virtual PajaTypes::float32		get_focal_distance();
		virtual PajaTypes::float32		get_focal_range();

		virtual PajaTypes::int32			get_fog_type();
		virtual PajaTypes::float32		get_fog_start();
		virtual PajaTypes::float32		get_fog_end();
		virtual PajaTypes::ColorC			get_fog_color();

		virtual PajaTypes::BBox2C			get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool									hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );


	protected:
		DX9CameraEffectC();
		DX9CameraEffectC( Edit::EditableI* pOriginal );
		virtual ~DX9CameraEffectC();

	private:
		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;
		Composition::AutoGizmoC*	m_pTimeGizmo;

		D3DXMATRIX								m_matCamera;
		D3DXMATRIX								m_matProj;
		PajaTypes::float32				m_f32FocalDistance;
		PajaTypes::float32				m_f32FocalRange;

		PajaTypes::int32					m_i32FogType;
		PajaTypes::float32				m_f32FogStart;
		PajaTypes::float32				m_f32FogEnd;
		PajaTypes::ColorC					m_FogColor;

		PajaTypes::Matrix2C				m_rTM;
		PajaTypes::BBox2C					m_rBBox;
	};

};	// namespace


extern DX9CameraDescC			g_rDX9CameraDesc;


#endif	// __DX9Camera_H__
