//
// DX9Tree.h
//
// DX9Tree Plugin
//
// Copyright (c) 2004 memon/moppi productions
//

#ifndef __DX9TreePLUGIN_H__
#define __DX9TreePLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "DX9ViewportC.h"
#include "DX9DeviceC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "ImportableImageI.h"
#include "DX9MeshPlugin.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

#define	CLASS_DX9TREE_EFFECT_NAME	"Tree (DX9)"
#define	CLASS_DX9TREE_EFFECT_DESC	"Tree (DX9)"

const PluginClass::ClassIdC	CLASS_DX9TREE_EFFECT( 0x5AA58896, 0x8596434B );


//////////////////////////////////////////////////////////////////////////
//
//  DX9Tree effect class descriptor.
//

class DX9TreeDescC : public PluginClass::ClassDescC
{
public:
	DX9TreeDescC();
	virtual ~DX9TreeDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};

namespace DX9TreePlugin {

//////////////////////////////////////////////////////////////////////////
//
// DX9Tree effect class.
//

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_TREE_MESH = 0,
		ID_ATTRIBUTE_VINE_IMAGE,
		ID_ATTRIBUTE_VINE_OFFSET,
		ID_ATTRIBUTE_VINE_SCALE,
		ID_ATTRIBUTE_VINE_OPACITY,
		ID_ATTRIBUTE_VINE_BAND,
		ID_ATTRIBUTE_VINE_AMOUNT,
		ATTRIBUTE_COUNT,
	};

	enum AVIPlayerEffectGizmosE {
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	enum ResampleModeE {
		RESAMPLEMODE_BILINEAR = 0,
		RESAMPLEMODE_NEAREST = 1,
	};

	enum WrapModeE {
		WRAPMODE_CLAMP = 0,
		WRAPMODE_REPEAT = 1,
	};

	enum BlendE {
		BLENDMODE_NORMAL = 0,
		BLENDMODE_REPLACE,
		BLENDMODE_ADD,
		BLENDMODE_MULT,
		BLENDMODE_SUB,
		BLENDMODE_LIGHTEN,
		BLENDMODE_DARKEN,
	};

	class DX9TreeEffectC : public Composition::EffectI
	{
	public:
		static DX9TreeEffectC*				create_new();
		virtual Edit::DataBlockI*			create();
		virtual Edit::DataBlockI*			create( Edit::EditableI* pOriginal );
		virtual void									copy( Edit::EditableI* pEditable );
		virtual void									restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32			get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*						get_class_name();

		virtual void									set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual PajaTypes::uint32			update_notify( EditableI* pCaller );

		virtual void									initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void									eval_state( PajaTypes::int32 i32Time );

		virtual PajaTypes::BBox2C			get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool									hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );


	protected:
		DX9TreeEffectC();
		DX9TreeEffectC( Edit::EditableI* pOriginal );
		virtual ~DX9TreeEffectC();

		void		init_effect();

	private:
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;

		CD3DModelInstance*				m_pInstance;
		LPD3DXEFFECT							m_pEffect;

	};

};	// namespace


extern DX9TreeDescC			g_rDX9TreeDesc;


#endif	// __DX9Tree_H__
