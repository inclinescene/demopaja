//
// ValueFieldPlugin.h
//
// ValueField Plugin
//
// Copyright (c) 2003 memon/moppi productions
//

#ifndef __VALUEFIELDPLUGIN_H__
#define __VALUEFIELDPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLDeviceC.h"
#include "OpenGLViewportC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"
#include "EffectValueFieldI.h"


//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_TRANSFORMFIELD_EFFECT( 0xA9465D5B, 0x8AFD449F );
const PluginClass::ClassIdC	CLASS_FLUIDFIELD_EFFECT( 0x6A70CC97, 0xEFC74A07 );
const PluginClass::ClassIdC	CLASS_WAVEFIELD_EFFECT( 0xA142D492, 0x274A4098 );
const PluginClass::ClassIdC	CLASS_IMAGEFIELD_EFFECT( 0xCBB02D3C, 0x25AA44EE );


//////////////////////////////////////////////////////////////////////////
//
//  Point Field effect class descriptor.
//

class TransformFieldDescC : public PluginClass::ClassDescC
{
public:
	TransformFieldDescC();
	virtual ~TransformFieldDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  Fluid Field effect class descriptor.
//

class FluidFieldDescC : public PluginClass::ClassDescC
{
public:
	FluidFieldDescC();
	virtual ~FluidFieldDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  Wave Field effect class descriptor.
//

class WaveFieldDescC : public PluginClass::ClassDescC
{
public:
	WaveFieldDescC();
	virtual ~WaveFieldDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


//////////////////////////////////////////////////////////////////////////
//
//  Image Field effect class descriptor.
//

class ImageFieldDescC : public PluginClass::ClassDescC
{
public:
	ImageFieldDescC();
	virtual ~ImageFieldDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};



//////////////////////////////////////////////////////////////////////////
//
// The Transform Field effect class.
//

namespace TransformFieldPlugin {

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_ROT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_SIZE,						// x, y
		ID_ATTRIBUTE_TRESHOLD,
		ID_ATTRIBUTE_EASE_IN,
		ID_ATTRIBUTE_EASE_OUT,
		ATTRIBUTE_COUNT,
	};

	enum DeformGizmoParamsE {
		ID_DEFORM_OFFSET = 0,
		ID_DEFORM_ROTATION,
		ID_DEFORM_SCALE,
	};

	enum TransformFieldEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		ID_GIZMO_DEFORM,
		GIZMO_COUNT,
	};


	class TransformFieldEffectC : public Composition::EffectValueFieldI
	{
	public:
		static TransformFieldEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual void				draw_ui( PajaTypes::uint32 ui32Flags );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual float						sample_value_float( const PajaTypes::Vector2C& rSamplePos, const PajaTypes::Matrix2C& rTM );
		virtual PajaTypes::Vector2C	sample_value_vector2( const PajaTypes::Vector2C& rSamplePos, const PajaTypes::Matrix2C& rTM );
		virtual void						sample_value_array_float( const PajaTypes::Vector2C* pSamplePosArray, float* pOutArray, PajaTypes::uint32 ui32NumSamples, const PajaTypes::Matrix2C& rTM );
		virtual void						sample_value_array_vector2( const PajaTypes::Vector2C* pSamplePosArray, PajaTypes::Vector2C* pOutArray, PajaTypes::uint32 ui32NumSamples, const PajaTypes::Matrix2C& rTM );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		TransformFieldEffectC();
		TransformFieldEffectC( Edit::EditableI* pOriginal );
		virtual ~TransformFieldEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;
		Composition::AutoGizmoC*	m_pDeformGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];

		PajaTypes::Matrix2C	m_rConvTM;
		PajaTypes::Matrix2C	m_rInvConvTM;

		PajaTypes::Matrix2C	m_rDeformTM;

		PajaTypes::Vector2C	m_rDeformOffset;
		PajaTypes::Vector2C	m_rDeformScale;
		PajaTypes::float32	m_f32DeformRotate;

		PajaTypes::Vector2C	m_rSize;
		PajaTypes::float32	m_f32Treshold;
		PajaTypes::float32	m_f32EaseIn;
		PajaTypes::float32	m_f32EaseOut;
	};

};	// namespace


//////////////////////////////////////////////////////////////////////////
//
// The Fluid Field effect class.
//

namespace FluidFieldPlugin {

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_ROT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_IMAGE = 0,
		ID_ATTRIBUTE_SCALE,
		ID_ATTRIBUTE_OFFSET,
		ID_ATTRIBUTE_WRAP,
		ID_ATTRIBUTE_RESAMPLE,
		ATTRIBUTE_COUNT,
	};

	enum FluidFieldEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	class FluidFieldEffectC : public Composition::EffectValueFieldI
	{
	public:
		static FluidFieldEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual void				draw_ui( PajaTypes::uint32 ui32Flags );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual float						sample_value_float( const PajaTypes::Vector2C& rSamplePos, const PajaTypes::Matrix2C& rTM );
		virtual PajaTypes::Vector2C	sample_value_vector2( const PajaTypes::Vector2C& rSamplePos, const PajaTypes::Matrix2C& rTM );
		virtual void						sample_value_array_float( const PajaTypes::Vector2C* pSamplePosArray, float* pOutArray, PajaTypes::uint32 ui32NumSamples, const PajaTypes::Matrix2C& rTM );
		virtual void						sample_value_array_vector2( const PajaTypes::Vector2C* pSamplePosArray, PajaTypes::Vector2C* pOutArray, PajaTypes::uint32 ui32NumSamples, const PajaTypes::Matrix2C& rTM );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		FluidFieldEffectC();
		FluidFieldEffectC( Edit::EditableI* pOriginal );
		virtual ~FluidFieldEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::Matrix2C	m_rTextureTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
	};

};	// namespace


//////////////////////////////////////////////////////////////////////////
//
// The Wave Field effect class.
//

namespace WaveFieldPlugin {

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_ROT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_AMOUNT = 0,			// x, y
		ID_ATTRIBUTE_SIZE,						// x, y
		ID_ATTRIBUTE_TYPE,					// radial, axial
		ID_ATTRIBUTE_FREQUENCY,
		ID_ATTRIBUTE_PHASE,
		ID_ATTRIBUTE_FALLOFF,				// Off, Curve
		ID_ATTRIBUTE_NEAR_CURVE,
		ID_ATTRIBUTE_FAR_CURVE,
		ATTRIBUTE_COUNT,
	};

	enum WaveFieldEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	class WaveFieldEffectC : public Composition::EffectValueFieldI
	{
	public:
		static WaveFieldEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual void				draw_ui( PajaTypes::uint32 ui32Flags );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual float						sample_value_float( const PajaTypes::Vector2C& rSamplePos, const PajaTypes::Matrix2C& rTM );
		virtual PajaTypes::Vector2C	sample_value_vector2( const PajaTypes::Vector2C& rSamplePos, const PajaTypes::Matrix2C& rTM );
		virtual void						sample_value_array_float( const PajaTypes::Vector2C* pSamplePosArray, float* pOutArray, PajaTypes::uint32 ui32NumSamples, const PajaTypes::Matrix2C& rTM );
		virtual void						sample_value_array_vector2( const PajaTypes::Vector2C* pSamplePosArray, PajaTypes::Vector2C* pOutArray, PajaTypes::uint32 ui32NumSamples, const PajaTypes::Matrix2C& rTM );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		WaveFieldEffectC();
		WaveFieldEffectC( Edit::EditableI* pOriginal );
		virtual ~WaveFieldEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::Matrix2C	m_rTextureTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
	};

};	// namespace


//////////////////////////////////////////////////////////////////////////
//
// The Image Field effect class.
//

namespace ImageFieldPlugin {

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_ROT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_IMAGE = 0,
		ID_ATTRIBUTE_SCALE,
		ID_ATTRIBUTE_OFFSET,
		ID_ATTRIBUTE_WRAP,
		ID_ATTRIBUTE_AMOUNT = 0,			// -100..100
		ATTRIBUTE_COUNT,
	};

	enum ImageFieldEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	enum ResampleModeE {
		RESAMPLEMODE_BILINEAR = 0,
		RESAMPLEMODE_NEAREST = 1,
	};

	enum WrapModeE {
		WRAPMODE_CLAMP = 0,
		WRAPMODE_REPEAT = 1,
	};

	class ImageFieldEffectC : public Composition::EffectValueFieldI
	{
	public:
		static ImageFieldEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( PajaTypes::int32 i32Time, Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual void					eval_state( PajaTypes::int32 i32Time );
		virtual void				draw_ui( PajaTypes::uint32 ui32Flags );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual float						sample_value_float( const PajaTypes::Vector2C& rSamplePos, const PajaTypes::Matrix2C& rTM );
		virtual PajaTypes::Vector2C	sample_value_vector2( const PajaTypes::Vector2C& rSamplePos, const PajaTypes::Matrix2C& rTM );
		virtual void						sample_value_array_float( const PajaTypes::Vector2C* pSamplePosArray, float* pOutArray, PajaTypes::uint32 ui32NumSamples, const PajaTypes::Matrix2C& rTM );
		virtual void						sample_value_array_vector2( const PajaTypes::Vector2C* pSamplePosArray, PajaTypes::Vector2C* pOutArray, PajaTypes::uint32 ui32NumSamples, const PajaTypes::Matrix2C& rTM );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		ImageFieldEffectC();
		ImageFieldEffectC( Edit::EditableI* pOriginal );
		virtual ~ImageFieldEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::Matrix2C	m_rTextureTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
	};

};	// namespace


extern TransformFieldDescC	g_rTransformFieldDesc;
extern FluidFieldDescC			g_rFluidFieldDesc;
extern WaveFieldDescC				g_rWaveFieldDesc;
extern ImageFieldDescC			g_rImageFieldDesc;


#endif	// __VALUEFIELDPLUGIN_H__
