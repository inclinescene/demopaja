//
// BlurPlugin.h
//
// Blur Plugin
//
// Copyright (c) 2000 memon/moppi productions
//

#ifndef __BLURPLUGIN_H__
#define __BLURPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "ImportInterfaceC.h"
#include "DX8DeviceC.h"
#include "DX8ViewportC.h"
#include "TimeContextC.h"
#include "AutoGizmoC.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_TEST_EFFECT( 0xE49D6E2E, 0x997B434D );


//////////////////////////////////////////////////////////////////////////
//
//  Blur effect class descriptor.
//

class TestDescC : public PluginClass::ClassDescC
{
public:
	TestDescC();
	virtual ~TestDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};


namespace TestPlugin {

//////////////////////////////////////////////////////////////////////////
//
// The Blur effect class.
//

	enum TransformGizmoParamsE {
		ID_TRANSFORM_POS = 0,
		ID_TRANSFORM_PIVOT,
		ID_TRANSFORM_SCALE,
		TRANSFORM_COUNT,
	};

	enum AttributeGizmoParamsE {
		ID_ATTRIBUTE_COLOR = 0,
		ID_ATTRIBUTE_RENDERMODE,
		ATTRIBUTE_COUNT,
	};

	enum TestEffectGizmosE {
		ID_GIZMO_TRANS = 0,
		ID_GIZMO_ATTRIB,
		GIZMO_COUNT,
	};

	enum RenderModeE {
		RENDERMODE_ORTHO = 0,
		RENDERMODE_PERSPECTIVE,
		RENDERMODE_NORMALIZED,
	};

	class TestEffectC : public Composition::EffectI
	{
	public:
		static TestEffectC*				create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual PajaTypes::int32		get_gizmo_count();
		virtual Composition::GizmoI*	get_gizmo( PajaTypes::int32 i32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		virtual void					set_default_file( Import::FileHandleC* pHandle );
		virtual Composition::ParamI*	get_default_param( PajaTypes::int32 i32Param );

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext );

		virtual void					do_frame( PajaSystem::DeviceContextC* pContext );
		virtual void					eval_state( PajaTypes::int32 i32Time, PajaSystem::TimeContextC* pTimeContext );
		virtual PajaTypes::BBox2C		get_bbox();

		virtual const PajaTypes::Matrix2C&	get_transform_matrix() const;

		virtual bool					hit_test( const PajaTypes::Vector2C& rPoint );

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );


	protected:
		TestEffectC();
		TestEffectC( Edit::EditableI* pOriginal );
		virtual ~TestEffectC();

	private:

		Composition::AutoGizmoC*	m_pTraGizmo;
		Composition::AutoGizmoC*	m_pAttGizmo;

		PajaTypes::Matrix2C	m_rTM;
		PajaTypes::BBox2C	m_rBBox;
		PajaTypes::Vector2C	m_rVertices[4];
		PajaTypes::ColorC	m_rColor;
		PajaTypes::int32	m_i32RenderMode;
		PajaTypes::float32	m_f32Frame;
		PajaTypes::float32	m_f32Aspect;
	};

};	// namespace


extern TestDescC	g_rTestDesc;


#endif
