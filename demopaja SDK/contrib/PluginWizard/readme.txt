Demopaja Plugin Wizard
   by: Aaron Hilton (aka OTri)
  web: http://OTri.net/
email: OTri@otri.net
  ICQ: 822109

Build effect plug-ins for demopaja v0.6b, quick and easily.
Demopaja is a demo creation tool, which doubles as a great testbed
for visual effects that you want tweaked in realtime.

Installation:
Copy to your Visual C++ template directory, usually found in:
 "C:\Program Files\Microsoft Visual Studio\Common\MSDev98\Template"

Usage:
1) Run DevStudio
2) create a new project
3) choose the Demopaja Plugin Wizard
4) Give the project a name, which is also your plugin class name.

That's it...
 Enjoy.
