//-------------------------------------------------------------------------
//
// File:		DemoInterfaceC.h
// Desc:		Demo interface class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_DEMOINTERFACEC_H__
#define __DEMOPAJA_DEMOINTERFACEC_H__

namespace PajaSystem {
	class DemoInterfaceC;
};

#include "FactoryC.h"
#include "ImportableI.h"
#include "ClassIdC.h"
#include "FileListC.h"
#include "FileHandleC.h"
#include "TimeContextC.h"
#include "DeviceContextC.h"
#include "CommonDialogI.h"
#include <string>
#include <vector>
#include <map>

namespace PajaSystem {

	//! Import interface class.
	/*!	Import interface class is a tool class for importers.
		It's purpose is to enable the importers to import other
		files during the load. For example, a 3D file importer can
		request the interface to import it's textures.

		This class is implemented by the system.
	*/
	class DemoInterfaceC
	{
	public:
		//! Default constructor (used internally).
		DemoInterfaceC( PajaSystem::DeviceContextC* pContext, PajaSystem::TimeContextC* pTimeContext, Import::FileListC* pFileList, PluginClass::FactoryC* pFactory );
		//! Default destructor (used internally).
		virtual ~DemoInterfaceC();

		//! Imports requested file.
		/*! Request a import for a specified file. File importer filters can be used
			range the number of possible importers which could match the file extension.

			For example if it is known that the file must be image, SUPERCLASS_IMAGE could be
			passed as the rSuperFilter argument.
			
			If a filter (class Id of super class ID) isn't used NULL_CLASSID or NULL_SUPERCLASS
			should be passed as argument.

			Example:
			\code
			// Request import for a image.
			FileHandleC*	pHandle = m_pImpInterface->request_import( szFileName, SUPERCLASS_IMAGE, NULL_CLASSID );
			\endcode
		*/
		virtual Import::FileHandleC*	request_import( const char* szName, const PluginClass::SuperClassIdC& rSuperFilter,
												const PluginClass::ClassIdC& rClassFilter );

		//! Used internally.
		virtual void			initialize_importable( Import::ImportableI* pImp, PajaTypes::uint32 ui32Reason );

		//! Notify all importable which refer to this file handle.
		virtual void			file_update_notify( Import::FileHandleC* pHandle );

		//! Returns pointer to the file list.
		virtual Import::FileListC*		get_filelist();
		//! Returns pointer to the file list.
		virtual PajaSystem::DeviceContextC*	get_device_context();
		//! Returns pointer to the file list.
		virtual PajaSystem::TimeContextC*		get_time_context();

		//! Adds search directory.
		/*!	The paths in Demopaja are relative to the given project path. This
			method sets the project path.
		*/
		virtual void			set_project_path( const char* szDir );

		//! Returns the project path.
		virtual const char*		get_project_path() const;

		//! Returns true if path is relative path, else false.
		/*! The method returns true if the path looks like relative path (that is, it lacks
			the part of the path which defined the physical or network drive.
		*/
		virtual bool			is_relative_path( const char* szFileName );

		//! Converts absolute path to relative path.
		/*! The returned string is temporary, and is valid only until this method is called again.
			Before using the string, it should be copied. If the file name is not relative to the
			set project path, it will be left absolute.
		*/
		virtual const char*		get_relative_path( const char* szFileName );

		//! Converts relative path to absolute path.
		/*! The returned string is temporary, and is valid only until this method is called again.
			Before using the string, it should be copied.
		*/
		virtual const char*		get_absolute_path( const char* szFileName );

		//! Adds new common dialog to list.
		virtual void						add_common_dialog( PajaSystem::CommonDialogI* pDlg );
		//! Gets a common dialog based on the class ID.
		virtual PajaSystem::CommonDialogI*	get_common_dialog( const PluginClass::ClassIdC& rClassID );

		//! Used internally, sets the parent folder of hte session.
		virtual void						set_parent_folder( Import::FileHandleC* pFolder );
		//! Starts a new importsession, used internally.
		virtual PajaTypes::uint32			begin_session();
		//! Ends an importsession, used internally.
		virtual void						end_session( PajaTypes::uint32 ui32SessionID );

	private:

		typedef std::map<std::string, PluginClass::ClassDescC*>	DefaultImpMapT;

		struct ImportSessionS {
			DefaultImpMapT				m_rDefaultImp;
			PajaTypes::uint32			m_ui32SessionID;
			Import::FileHandleC*		m_pFolder;
		};

		PluginClass::FactoryC*		m_pFactory;
		Import::FileListC*					m_pFileList;
		PajaSystem::DeviceContextC*	m_pDevContext;
		PajaSystem::TimeContextC*	m_pTimeContext;
		std::string					m_sProjectPath;
		std::vector<PajaSystem::CommonDialogI*>	m_rCommonDialogs;
		std::vector<ImportSessionS>	m_rSessions;
		PajaTypes::uint32			m_ui32SessionID;
	};

};

#endif // __DEMOPAJA_DemoInterfaceC_H__