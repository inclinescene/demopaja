//-------------------------------------------------------------------------
//
// File:		Edit.h
// Desc:		Edit namespace header.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_EDIT_H__
#define __DEMOPAJA_EDIT_H__

//! The Edit namespace
/*!	The classes collected into the Edit namespace are very closely
	related to the internal undo-system of Demopaja. The classes range
	from the actual undo object to the basic data objects. which are handled
	by the undo system.
*/
namespace Edit {
	// empty
};

#endif // __DEMOPAJA_EDIT_H__