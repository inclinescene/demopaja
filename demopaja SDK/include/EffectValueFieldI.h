//-------------------------------------------------------------------------
//
// File:		EffectValueFieldI.h
// Desc:		Value Field Effect interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------


#ifndef __DEMOPAJA_EFFECTVALUEFIELDI_H__
#define __DEMOPAJA_EFFECTVALUEFIELDI_H__

// forward declaration
namespace Composition {
	class EffectMaskI;
};

#include "PajaTypes.h"
#include "DataBlockI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "GizmoI.h"
#include "TimeSegmentC.h"
#include "ClassIdC.h"
#include "FileIO.h"
#include "ImportableI.h"
#include "DeviceContextC.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "Vector2C.h"
#include "TimeContextC.h"
#include "FileHandleC.h"
#include "LayerC.h"
#include "EffectI.h"
#include "ImportableImageI.h"
#include <string>
#include <vector>


namespace PluginClass {
	//! The Value Field Effect super class ID.
	const PluginClass::SuperClassIdC		SUPERCLASS_EFFECT_VALUEFIELD = PluginClass::SuperClassIdC( 0x1E27BE62, 0x97CD4AF6 );
};

namespace Composition {

	class EffectValueFieldI : public EffectI
	{
	public:
		//! Returns super class ID of the effect.
		virtual PluginClass::SuperClassIdC	get_super_class_id();

		//! Samples the value field from specified position and returns single scalar value.
		virtual float						sample_value_float( const PajaTypes::Vector2C& rSamplePos, const PajaTypes::Matrix2C& rTM ) = 0;
		//! Samples the value field from specified position and returns single 2D vector value.
		virtual PajaTypes::Vector2C	sample_value_vector2( const PajaTypes::Vector2C& rSamplePos, const PajaTypes::Matrix2C& rTM ) = 0;

		//! Samples the value field from array of points specified position and outputs same amount of scalar values.
		virtual void						sample_value_array_float( const PajaTypes::Vector2C* pSamplePosArray, float* pOutArray, PajaTypes::uint32 ui32NumSamples, const PajaTypes::Matrix2C& rTM ) = 0;
		//! Samples the value field from array of points specified position and outputs same amount of 2D vector values.
		virtual void						sample_value_array_vector2( const PajaTypes::Vector2C* pSamplePosArray, PajaTypes::Vector2C* pOutArray, PajaTypes::uint32 ui32NumSamples, const PajaTypes::Matrix2C& rTM ) = 0;

	protected:
		//! Default constructor.
		EffectValueFieldI();
		//! Default constructor with reference to the original.
		EffectValueFieldI( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~EffectValueFieldI();
	};

};

#endif // __DEMOPAJA_EFFECTVALUEFIELDI_H__