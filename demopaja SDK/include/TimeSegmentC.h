//-------------------------------------------------------------------------
//
// File:		TimeSegmentC.h
// Desc:		Time segment class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_TIMESEGMENTC_H__
#define __DEMOPAJA_TIMESEGMENTC_H__


#include "DataBlockI.h"
#include "PajaTypes.h"
#include "EditableI.h"
#include "KeyC.h"
#include "UndoC.h"
#include "FileIO.h"
#include <vector>


namespace Composition {

	//! Base classID.
	const PluginClass::SuperClassIdC		BASECLASS_TIMESEGMENT = PluginClass::SuperClassIdC( 1, 11 );

	//! Time segment class.
	/*!	Time segment class implements the visibility control for Demopaja layers and effects.
		The time segment has a origo in time and set of keys. The origo is treated as
		the beginning of the effect (or for example begin of animation the effect plays).
		Every two keys defines a segment where the is_visible() method returns true (the layer/effect
		is visible). If there is odd number of keys the last key is ignored.

		The time values of the keys in a time segment are stored relative to the
		time origo. The methods, which operates the keys based in time uses
		the same time space as the time segment itself.

		The changes made to the time segment is saved to a undo object if present.

		This class is implemented by the system.

		\see KeyC
	*/
	class TimeSegmentC : public Edit::EditableI
	{
	public:
		//! Create new time segment.
		/*!	Sets the origo of the time segment to zero. Noe keys are created.
		*/
		static TimeSegmentC*		create_new();
		//! Create new time segment.
		/*!	Sets the origo of the timesegment to i32SegmentStart and creates two keys,
			first at the point in time of i32SegmentStart and second at the point in time
			of i32SegmentEnd.
		*/
		static TimeSegmentC*		create_new( PajaTypes::int32 i32SegmentStart, PajaTypes::int32 i32SegmentEnd );

		//! Create new time segment.
		/*!	Sets the origo of the time segment to zero. Noe keys are created.
		*/
		virtual Edit::DataBlockI*	create();
		//! Creates new time segment (used internally), see Edit::EditableI::create().
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		//! Deep copy from a data block, see Edit::DataBlockI::copy().
		virtual void				copy( Edit::EditableI* pEditable );
		//! Shallow copy from a editable, see Edit::EditableI::restore().
		virtual void				restore( Edit::EditableI* pEditable );
		//! Returns the base class ID.
		virtual PluginClass::SuperClassIdC		get_base_class_id() const;

		//! Returns the origo of the time segment.
		virtual PajaTypes::int32	get_segment_start();
		//! Sets the origo of the time segment.
		virtual void				set_segment_start( PajaTypes::int32 i32Start );

		//! Returns the number of keys.
		virtual PajaTypes::int32	get_key_count();
		//! Returns a key at specified index.
		virtual KeyC*				get_key( PajaTypes::int32 i32Index );
		//! Deletes a key at specified index.
		virtual void				del_key( PajaTypes::int32 i32Index );

		//! Returns the time of the key.
		/*!	The returned time is in the same time space as the origo,
			even keys are relative to the origo of the time segment.
		*/
		virtual PajaTypes::int32	get_key_time( PajaTypes::int32 i32Index );

		//! Sets the time of a key
		/*!	The time is in the same time space as the origo,
			even keys are relative to the origo of the time segment.
		*/
		virtual void				set_key_time( PajaTypes::int32 i32Index, PajaTypes::int32 i32Time );

		//! Returns a key at specified time.
		/*!	The time is in the same time space as the origo,
			even keys are relative to the origo of the time segment.
		*/
		virtual KeyC* 				get_key_at_time( PajaTypes::int32 i32Time );
		//! Adds a key at specified time.
		/*!	The time is in the same time space as the origo,
			even keys are relative to the origo of the time segment.
		*/
		virtual KeyC*				add_key_at_time( PajaTypes::int32 i32Time );

		//! Sort the keys based on the time of a key.
		virtual void				sort_keys();

		//! Returns true if there is visible segment at given time.
		virtual bool				is_visible( PajaTypes::int32 i32Time );

		//! Serialize the time segment to a Demopaja output stream.
		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		//! Serialize the time segment from a Demopaja input stream.
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		TimeSegmentC();
		//! Creates new time segment.
		TimeSegmentC( PajaTypes::int32 i32SegmentStart, PajaTypes::int32 i32SegmentLength );
		//! Default constructor with reference to the original.
		TimeSegmentC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~TimeSegmentC();

	private:
		PajaTypes::int32	m_i32SegmentStart;
		std::vector<KeyC*>	m_rTrimKeys;
	};

}; // namespace

#endif // __DEMOPAJA_TIMESEGMENTC_H__
