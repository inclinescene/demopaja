//-------------------------------------------------------------------------
//
// File:		GUIDrawInterfaceI.h
// Desc:		GUI Draw interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_GUIDRAWINTERFACEC_H__
#define __DEMOPAJA_GUIDRAWINTERFACEC_H__

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include "PajaTypes.h"
#include "BBox2C.h"
#include "ColorC.h"
#include "Vector2C.h"
#include "DeviceInterfaceI.h"
#include "FileIO.h"

namespace PajaSystem {

	const PluginClass::SuperClassIdC		GRAPHICSDEVICE_GUIDRAW_INTERFACE = PluginClass::SuperClassIdC( 0, 0x3000003 );


	//! GUI Draw Interface
	/*! Graphics User Interface Draw interface is used to abstract the rendering of the GUI items
		in the Layout View. This class provides a set of basic drawing methods, such as line drawing,
		to draw the GUI.
	*/
	class GUIDrawInterfaceI : public DeviceInterfaceI
	{
	public:

		virtual PluginClass::SuperClassIdC		get_super_class_id() const;
		virtual PajaTypes::uint32				save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32				load( FileIO::LoadC* pLoad );

		//! Draws the layout rectangle.
		virtual void	draw_layout( const PajaTypes::ColorC& rLayout ) = 0;

		//! Begins a layout drawing block.
		virtual void	begin_layout() = 0;
		//! Ends a layout drawing block.
		virtual void	end_layout() = 0;

		//! Uses specified font.
		virtual void	use_font( HFONT hFont ) = 0;
		//! Draws text using current font.
		virtual void	draw_text( const PajaTypes::Vector2C& rPos, const char* szText ) = 0;

		//! Sets current draw color.
		virtual void	set_color( const PajaTypes::ColorC& rColor ) = 0;
		//! Sets current point size.
		virtual void	set_point_size( PajaTypes::float32 f32Size ) = 0;
		//! Draws a line.
		virtual void	draw_line( const PajaTypes::Vector2C& rFrom, const PajaTypes::Vector2C& rTo ) = 0;
		//! Draws a point.
		virtual void	draw_point( const PajaTypes::Vector2C& rPos ) = 0;
		//! Draws grid to based on latest draw layout call and grid size.
		virtual void	draw_grid( PajaTypes::float32 f32Width, PajaTypes::float32 f32Height, PajaTypes::float32 f32GridSize ) = 0;
		//! Draws selection box.
		virtual void	draw_selection_box( const PajaTypes::Vector2C& rMin, const PajaTypes::Vector2C& rMax ) = 0;
		//! Draw a box.
		virtual void	draw_box( const PajaTypes::Vector2C& rMin, const PajaTypes::Vector2C& rMax ) = 0;
		//! Draws a marker.
		virtual void	draw_marker( const PajaTypes::Vector2C& rPos, PajaTypes::float32 f32Size ) = 0;

	protected:
		GUIDrawInterfaceI();
		virtual ~GUIDrawInterfaceI();

	};

};	// namespace

#endif