//-------------------------------------------------------------------------
//
// File:		LayerC.h
// Desc:		Composition layer class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_LAYERC_H__
#define __DEMOPAJA_LAYERC_H__

namespace Composition {
	class LayerC;
};

#include "PajaTypes.h"
#include "DataBlockI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "TimeSegmentC.h"
#include "EffectI.h"
#include "GizmoI.h"
#include "FileIO.h"
#include "SceneC.h"
#include "ClassIdC.h"
#include <string>
#include <vector>


namespace Composition {

	//! Base classID.
	const PluginClass::SuperClassIdC		BASECLASS_LAYER = PluginClass::SuperClassIdC( 1, 8 );

	//! Composition Layer class.
	/*!	Layer class is used to group effects and to control their visibility.
		The layer class used by the Demopaja system and is
		only exposed to the export plugins.
		The changes to the layer are stored to a undo object if present.

		This class is implemented by the system.
	*/
	class LayerC : public Edit::EditableI
	{
	public:
		//! Creates new layer.
		static LayerC*				create_new( SceneC* pParent );
		//! Creates new layer (used internally), see Edit::EditableI::create().
		virtual Edit::DataBlockI*	create();
		//! Creates new layer (used internally), see Edit::EditableI::create().
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		//! Deep copy from a data block, see Edit::DataBlockI::copy().
		virtual void				copy( Edit::EditableI* pEditable );
		//! Shallow copy from a editable, see Edit::EditableI::restore().
		virtual void				restore( Edit::EditableI* pEditable );
		//! Returns the base class ID.
		virtual PluginClass::SuperClassIdC		get_base_class_id() const;
		//! Update notify from a connected editable.
		virtual PajaTypes::uint32							update_notify( EditableI* pCaller );

		//! Sets the name of the layer\. The name is NULL terminated string.
		void						set_name( const char* szName );
		//! Return the name of the layer.
		const char*					get_name() const;

		//! Return number of effects inside the layer.
		PajaTypes::int32			get_effect_count();
		//! Returns effect at specified index.
		EffectI*					get_effect( PajaTypes::int32 i32Index );

		//! Adds new effect to the layer.
		PajaTypes::int32			add_effect( EffectI* pEffect );
		//! Removes and deletes a effect at specified index from the layer.
		void						del_effect( PajaTypes::int32 i32Index );

		//! Removes a effect at specified index from list.
		/*!	This method removes the effect from the list inside the layer, and
			retuns the effect. The effect is not deleted.
			\see del_effect
		*/
		EffectI*					remove_effect( PajaTypes::int32 i32Index );
		//! Adds a effect to list, the change is not stored to the present undo object.
		void						insert_effect( PajaTypes::int32 i32IndexBefore, EffectI* pEffect  );

		//! Sets the effect flags.
		/*! Be careful to use this method. There are some flags, which
			have to be in place to make the layer work correctly.
			Use add, del or toggle flags methods instead.
		*/
		void						set_flags( PajaTypes::int32 i32Flags );
		//! Sets only specified flags.
		void						add_flags( PajaTypes::int32 i32Flags );
		//! Removes only specified flags.
		void						del_flags( PajaTypes::int32 i32Flags );
		//! Toggles only specified flags.
		void						toggle_flags( PajaTypes::int32 i32Flags );
		//! Returns key flags.
		PajaTypes::int32			get_flags();

		//! Sets the parent Scene.
		void						set_parent( SceneC* pScene );
		//! Gets the parent Scene.
		SceneC*						get_parent() const;

		//! Returns the timesegment of the layer.
		TimeSegmentC*				get_timesegment();

		//! Serialize the layer to a Demopaja output stream.
		PajaTypes::uint32			save( FileIO::SaveC* pSave );
		//! Serialize the layer from a Demopaja input stream.
		PajaTypes::uint32			load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		LayerC( SceneC* pParent );
		//! Constructor with reference to the original.
		LayerC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~LayerC();

	private:
		std::string				m_sName;
		std::vector<EffectI*>	m_rEffects;
		PajaTypes::int32		m_i32Flags;
		TimeSegmentC*			m_pTimeSegment;
		SceneC*					m_pParent;
	};

};

#endif // __LAYERC_H__
