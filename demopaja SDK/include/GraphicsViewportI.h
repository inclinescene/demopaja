//-------------------------------------------------------------------------
//
// File:		GraphicsViewportI.h
// Desc:		Graphics viewport interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_GRAPHICSVIEWPORTI_H__
#define __DEMOPAJA_GRAPHICSVIEWPORTI_H__

#include "PajaTypes.h"
#include "DeviceInterfaceI.h"
#include "Vector2C.h"
#include "BBox2C.h"
#include "FileIO.h"


namespace PajaSystem {

	const PluginClass::SuperClassIdC		GRAPHICSDEVICE_VIEWPORT_INTERFACE = PluginClass::SuperClassIdC( 0, 0x3000002 );


	//! Device interface for graphics viewport
	/*!	The purpose of the device interface is to setup
		the area where an effect can draw and to convert coordinates
		from screen (pixel) coordinates to the layout.
	*/
	class GraphicsViewportI : public DeviceInterfaceI
	{
	public:

		virtual PluginClass::SuperClassIdC		get_super_class_id() const;
		virtual PajaTypes::uint32				save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32				load( FileIO::LoadC* pLoad );

		//! Sets perspective viewport and projection matrix.
		/*!	\param rBBox the bounding box of the effect.
			\param f32FOV FOV on x-axis.
			\param f32Aspect Aspect ration of the viewport.
			\param f32ZNear Near clipping plane.
			\param f32ZFar Far clipping plane.

			This method is a replacement for gluPerspective. Note that the FOV is different.
			Scrissor testing is enabled and the bounding box is used to define the scissor box.
		*/
		virtual void				set_perspective( const PajaTypes::BBox2C& rBBox, PajaTypes::float32 f32FOV, PajaTypes::float32 f32Aspect,
										PajaTypes::float32 f32ZNear, PajaTypes::float32 f32ZFar ) = 0;

		//! Sets orthographic viewport and projection matrix.
		/*!	\param rBBox the bounding box of the effect.
			\param f32Width Width of the viewport.
			\param f32Height Height of the viewport.
			\param f32ZNear Near clipping plane.
			\param f32ZFar Far clipping plane.

			This method is a replacement for glOrtho.
			Scrissor testing is enabled and the bounding box is used to define the scissor box.
		*/
		virtual void				set_ortho( const PajaTypes::BBox2C& rBBox,
										PajaTypes::float32 f32Left, PajaTypes::float32 f32Right,
										PajaTypes::float32 f32Top, PajaTypes::float32 f32Bottom,
										PajaTypes::float32 f32ZNear = -1, PajaTypes::float32 f32ZFar = 1 ) = 0;

		//! Sets orthographic viewport and projection matrix.
		/*!	\param rBBox the bounding box of the effect.
			\param f32ZNear Near clipping plane.
			\param f32ZFar Far clipping plane.

			This method sets the orthographic projection to match the pixel dimensions of the
			current rendering output. The origin is set to the lower left corner of the
			whole rendering area.

			This method is equivalent to call glOrtho with:
				\b glOrtho( 0, <viewport width>, 0, <viewport height>, f32ZNear, f32ZFar );

			Scrissor testing is enabled and the bounding box is used to define the scissor box.
		*/
		virtual void				set_ortho_pixel( const PajaTypes::BBox2C& rBBox, PajaTypes::float32 f32ZNear = -1, PajaTypes::float32 f32ZFar = 1 ) = 0;

		//! Converts positions in screen pixels to the layout coordinate system.
		virtual PajaTypes::Vector2C	client_to_layout( const PajaTypes::Vector2C& rVec ) = 0;
		//! Converts positions in layout coordinate system uints to the screen pixels.
		virtual PajaTypes::Vector2C	layout_to_client( const PajaTypes::Vector2C& rVec ) = 0;
		//! Converts delta values of screen pixels to the layout coordinate system.
		virtual PajaTypes::Vector2C	delta_client_to_layout( const PajaTypes::Vector2C& rVec ) = 0;
		//! Converts delta values layout coordinate system uints to the screen pixels.
		virtual PajaTypes::Vector2C	delta_layout_to_client( const PajaTypes::Vector2C& rVec ) = 0;

		//! Returns the viewport (visible are of the demo).
		virtual const PajaTypes::BBox2C&	get_viewport() = 0;
		//! Returns the layout (rendering are of the demo).
		virtual const PajaTypes::BBox2C&	get_layout() = 0;
		//! Returns the width of the screen in pixels.
		virtual PajaTypes::int32			get_width() = 0;
		//! Returns the height of the screen in pixels.
		virtual PajaTypes::int32			get_height() = 0;
		//! Returns the pixel aspect ratio.
		virtual PajaTypes::float32			get_pixel_aspect_ratio() = 0;

		//! Sets the pixel aspect ratio.
		virtual void				set_pixel_aspect_ratio( PajaTypes::float32 f32PixelAspect ) = 0;
		//! Sets the dimension of the OpenGL rendering are in pixels (used internally).
		virtual void				set_dimension( PajaTypes::int32 i32PosX, PajaTypes::int32 i32PosY, PajaTypes::int32 i32Width, PajaTypes::int32 i32Height ) = 0;
		//! Sets the viewport (used internally).
		virtual void				set_viewport( const PajaTypes::BBox2C& rViewport ) = 0;
		//! Sets the layout (used internally).
		virtual void				set_layout( const PajaTypes::BBox2C& rLayout ) = 0;

		//! Resets the last set viewport to the device.
		virtual void				activate() = 0;

	protected:
		//! Default constructor
		GraphicsViewportI();
		//! Default destructor.
		virtual ~GraphicsViewportI();
	};

};

#endif // __DEMOPAJA_GRAPHICSVIEWPORTI_H__

