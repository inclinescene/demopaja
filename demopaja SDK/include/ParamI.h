//-------------------------------------------------------------------------
//
// File:		ParamI.h
// Desc:		Parameter classes.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_PARAMI_H__
#define __DEMOPAJA_PARAMI_H__

namespace Composition {
	class ParamI;
};

#include "PajaTypes.h"
#include "DataBlockI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ColorC.h"
#include "Vector2C.h"
#include "Vector3C.h"
#include "ControllerC.h"
#include "ClassIdC.h"
#include "FileHandleC.h"
#include "GizmoI.h"
#include "EffectI.h"

// STL
#include <vector>
#include <string>


//
// ParamI
//
// Interface for parameters
//


namespace Composition {

	//! Base classID.
	const PluginClass::SuperClassIdC		BASECLASS_PARAMETER = PluginClass::SuperClassIdC( 1, 9 );

	const bool		PARAM_ANIMATABLE = true;		//!< Constant for parameter creation.
	const bool		PARAM_NOT_ANIMATABLE = false;	//!< Constant for parameter creation.

	//! Parameter types
	enum ParameterTypesE {
		PARAM_TYPE_INT = 1,		//!< Integer parameter.
		PARAM_TYPE_FLOAT,		//!< Floating-point value parameter.
		PARAM_TYPE_VECTOR2,		//!< 2D vector parameter.
		PARAM_TYPE_VECTOR3,		//!< 3D vector parameter.
		PARAM_TYPE_COLOR,		//!< Color parameter.
		PARAM_TYPE_TEXT,		//!< Text parameter.
		PARAM_TYPE_FILE,		//!< File parameter.
		PARAM_TYPE_LINK,		//!< Link parameter.
	};

	//! Parameter styles.
	enum ParamStyleE {
		PARAM_STYLE_EDITBOX			= 0x0001,		//!< Parameter has editbox type-in.
		PARAM_STYLE_COMBOBOX		= 0x0002,		//!< Parameter has combobox (drop down selection box) type-in.
		PARAM_STYLE_TEXTPARAGRAPH	= 0x0004,		//!< Parameter has text paragraph type-in (not implemented).
		PARAM_STYLE_FILE			= 0x0008,		//!< Parameter has file type-in.
		PARAM_STYLE_COLORPICKER_RGBA	= 0x0010,		//!< Parameter has RGBA color picked type-in.
		PARAM_STYLE_PERCENT			= 0x0020,		//!< Parameter is percentage.
		PARAM_STYLE_ANGLE			= 0x0040,		//!< Parameter is angle.
		PARAM_STYLE_ABS_POSITION	= 0x0080,		//!< Positional parameter is absolute position (in screen space).
		PARAM_STYLE_REL_POSITION	= 0x0100,		//!< Positional parameter is relative position to the effect center.
		PARAM_STYLE_WORLD_SPACE		= 0x0200,		//!< Positional parameter is in world space (screen space).
		PARAM_STYLE_OBJECT_SPACE	= 0x0400,		//!< Positional parameter is object space (effect space).
		PARAM_STYLE_SINGLE_LINK = 0x0800,			//!< Single link.
		PARAM_STYLE_MULTI_LINK = 0x1000,		//!< Multi link.
		PARAM_STYLE_COLORPICKER_RGB	= 0x2000,		//!< Parameter has RGB color picked type-in.
	};

	//! Parameter value change notifications.
	enum ParamSetValNotifyE {
		PARAM_NOTIFY_NONE = 0,		//!< No notify
		PARAM_NOTIFY_UI_CHANGE,		//!< The gizmo/parameter layout has change and UI needs to be refreshed.
	};


	//! Parameter class.
	/*!	Parameter class defines a parameter which can be edited and animated
		in Demopaja GUI. ParamI implements a base class for all parameters.

		An ID is attached to each parameter. When a parameter is changed on a gizmo,
		the parameter send a notify to the gizmo via the update_notify() method,
		the update notify is relayed to the effect via effects update_notify method.
		When handling a notify message each parameter and gizmo can be identified from
		the ID. The ID of a parameter is specified in the create method or it can be
		changed with the set_id() method.

		If the value range of a animated parameter is changed no range checking is done to
		the keyframes in the controller. The plugin has to either clanp the keys, or
		handle the parameters out of the range.

		The content of the parameter (including name, range, etc) can be changed at any time,
		and the change is shown next time the GUI is updated. A common place to change a
		parameter is on update_notify() method of a gizmo or an effect. Parameters are ment to
		be created at the constructor of a gizmo. Hence, the some of the properties of the
		parameter namely, the name of the parameter, the style and the ID are not serialized
		to the stream.

		Demopaja does not currently support overridden parameters.

		All the parameters classes are implemented by the system.

		\see Composition::GizmoI
	*/
	class ParamI : public Edit::EditableI
	{
		friend class ParamIntC;
		friend class ParamFloatC;
		friend class ParamVector2C;
		friend class ParamVector3C;
		friend class ParamColorC;
		friend class ParamTextC;
		friend class ParamFileC;
		friend class ParamLinkC;

	public:

		//! Deep copy from a data block, see Edit::DataBlockI::copy().
		virtual void				copy( Edit::EditableI* pEditable );
		//! Shallow copy from a editable, see Edit::EditableI::restore().
		virtual void				restore( Edit::EditableI* pEditable );
		//! Returns the base class ID.
		virtual PluginClass::SuperClassIdC		get_base_class_id() const;

		//! Sets the parameter flags.
		virtual void				set_flags( PajaTypes::int32 i32Flags );
		//! Sets only specified flags.
		virtual void				add_flags( PajaTypes::int32 i32Flags );
		//! Removes only specified flags.
		virtual void				del_flags( PajaTypes::int32 i32Flags );
		//! Toggles only specified flags.
		virtual void				toggle_flags( PajaTypes::int32 i32Flags );
		//! Returns gizmo flags.
		virtual PajaTypes::int32	get_flags();

		//! Sets the expanded height of the parameter (used by the GUI).
		virtual void				set_expanded_height( PajaTypes::int32 i32Height );
		//! Returns the expanded height of the parameter (used by the GUI).
		virtual PajaTypes::int32	get_expanded_height();

		//! Returns the type of the parameter.
		virtual PajaTypes::uint32	get_type() const = 0;
		
		//! Sets the name of the parameter.
		virtual void				set_name( const char* name );
		//! Returns the name of the parameter.
		virtual const char*			get_name();

		//! Sets the controller of the parameter.
		virtual void				set_controller( ControllerC* cnt ) = 0;
		//! Returns the conroller attached to the parameter.
		/*!	NULL is returned if the parameter cannot be animated. */
		virtual ControllerC*		get_controller() = 0;

		//! Sets the spinner increment of the parameter.
		/*!	The spinner increment is the smallest value the parameter is meant to
			control. For 1/10th accuracy use spinner increment of 0.1.
			The spinner increment is used in the spinners of the type-ins and also
			to draw the values in the GUI.
		*/
		virtual void				set_increment( PajaTypes::float32 f32Inc ) = 0;
		//! Returns the spinner increment.
		virtual PajaTypes::float32	get_increment() const = 0;
		//! Returns the range of the parameter.
		/*!	\param pMin pointer to a array of maximum of KEY_MAXCHANNEL floats.
			\param pMax pointer to a array of maximum of KEY_MAXCHANNEL floats.

			If the type of the parameter id not know use arrays of KEY_MAXCHANNEL floats.

			\return true if the range is valid, false if the range is not used.
		*/
		virtual bool				get_min_max( PajaTypes::float32* pMin, PajaTypes::float32* pMax ) = 0;

		//! This method is called when a controller is changed.
		virtual PajaTypes::uint32							update_notify( EditableI* pCaller );

		//! Sets the ID of the parmeter.
		virtual void				set_id( PajaTypes::uint32 id ) = 0;
		//! Returns the ID of the parameter.
		virtual PajaTypes::uint32	get_id() const = 0;

		//! Sets the style of the parameter.
		virtual void				set_style( PajaTypes::uint32 style ) = 0;
		//! Returns the style of the parameter.
		virtual PajaTypes::uint32	get_style() const = 0;

		//! Serialize the parameter to a Demopaja output stream.
		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		//! Serialize the parameter from a Demopaja input stream.
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

		//! Returns the parent gizmo of the parameter.
		virtual void				set_parent( GizmoI* pParent );
		//! Sets the parent gizmo of the parameter.
		virtual GizmoI*				get_parent();

	protected:
		//! Default constructor.
		ParamI();
		//! Constructor with reference to the original.
		ParamI( Edit::EditableI* pOriginal );
		//! Constructor.
		ParamI( GizmoI* pParent, const char* name, PajaTypes::uint32 id,
							PajaTypes::uint32 style = 0, bool animatable = true );
		//! Default destructor.
		virtual ~ParamI();

	private:
		PajaTypes::uint32	m_ui32Id;
		PajaTypes::uint32	m_ui32Style;
		PajaTypes::int32	m_i32Flags;
		PajaTypes::int32	m_i32Height;
		ControllerC*		m_pCont;
		std::string			m_sName;
		GizmoI*				m_pParent;
	};


	//! Integer parameter class.
	/*!	The integer parameter can be used to coltrol parameters which can be
		represent as integers. This parameter also gives a possibility to
		have set of predefined values, which can be named, and selected
		using a combobox control.

		Even this is integer parameter the internal format is 32-bit float.
		So it is not possible to access the whole 32-bit integer range.

		A new integer parameter is created using the create_new() static method.
	*/
	class ParamIntC : public ParamI
	{
	public:

		//! Creates a new integer parameter.
		/*!	\param pParent Pointer to the parent gizmo.
			\param szName The name of the parameter as NULL terminated string.
			\param i32Value Start value of the parameter.
			\param ui32ID ID of the parameter.
			\param ui32Style The style of the parameter. Can be PARAM_STYLE_EDITBOX or PARAM_STYLE_COMBOBOX.
				Default PARAM_STYLE_EDITBOX.
			\param bAnimatable If set true the parameter is animatable,
				else it's not (you may use PARAM_ANIMATABLE or PARAM_NOT_ANIMATABLE too). Default true.
			\param i32Min The minimum of the parameter value range. Default 0.
			\param i32Max The maximum of the parameter value range. Default 0.
			\param i32Inc The increment of the value spinner in type-in.

			If minimum and maximum of the range are same no range checking is used.

			If the stype is PARAM_STYLE_COMBOBOX only labeled values can be selected from the type-in.

			\b Example:
			This example creates new parameter. The name of the parameter is "Filter mode",
			it's default value is 0 (zero) and it's ID is ID_FILTERMODE (constant defined
			by the plugin writer), the type-in style is combobox, the parameter can be animated,
			and the value range of the parameter is from 0 to 1. Also labels are defined for
			values 0, and 1. Default increment value 1 is used.

			\code
			m_pParamFilterMode = ParamIntC::create_new( this, "Filter mode", 0, ID_FILTERMODE,
														PARAM_STYLE_COMBOBOX, PARAM_ANIMATABLE, 0, 1 );
			m_pParamFilterMode->add_label( 0, "Bilinear" );
			m_pParamFilterMode->add_label( 1, "Nearest" );
			\endcode
		*/
		static ParamIntC*			create_new( GizmoI* pParent,
												const char* szName,
												PajaTypes::int32 i32Value,
												PajaTypes::uint32 ui32ID,
												PajaTypes::uint32 ui32Style = PARAM_STYLE_EDITBOX,
												bool bAnimatable = true,
												PajaTypes::int32 i32Min = 0,
												PajaTypes::int32 i32Max = 0,
												PajaTypes::int32 i32Inc = 1 );
		//! Creates new parameter.
		/*!	Following default values are used:
			\code
			pParent = 0,
			szName = 0,
			i32Value = 0,
			ui32ID = 0,
			ui32Style = 0,
			bAnimatable = false,
			i32Min = 0,
			i32Max = 0,
			i32Inc = 1
			\endcode

			\see create_new
		*/
		virtual Edit::DataBlockI*	create();
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		virtual void				copy( Edit::EditableI* pEditable );
		virtual void				restore( Edit::EditableI* pEditable );

		virtual PajaTypes::uint32	get_type() const;
		
		virtual void				set_controller( ControllerC* cnt );
		virtual ControllerC*		get_controller();

		//! Sets the range of the parameter.
		virtual void				set_min_max( PajaTypes::int32 i32Min, PajaTypes::int32 i32Max );
		//! Returns the minimum of the value range.
		virtual PajaTypes::int32	get_min() const;
		//! Returns the maximum of the value range.
		virtual PajaTypes::int32	get_max() const;
		virtual void				set_increment( PajaTypes::float32 inc );
		virtual PajaTypes::float32	get_increment() const;
		virtual bool				get_min_max( PajaTypes::float32* pMin, PajaTypes::float32* pMax );

		virtual void				set_style( PajaTypes::uint32 style );
		virtual PajaTypes::uint32	get_style() const;

		virtual void				set_id( PajaTypes::uint32 id );
		virtual PajaTypes::uint32	get_id() const;

		//! Clears all labels.
		virtual void				clear_labels();
		//! Adds a value label.
		/*!	\param i32Val Value to bind the label.
			\param szName Name of the value as NULL terminated string.
		*/
		virtual void				add_label( PajaTypes::int32 i32Val, const char* szName );
		//! Adds a value label.
		/*!	\param i32Val Value to bind the label.
			\param sName Name of the value as STL string.
		*/
		virtual void				add_label( PajaTypes::int32 i32Val, const std::string& sName );
		//! Returns number of labels.
		virtual PajaTypes::uint32	get_label_count() const;
		//! Returns the name of the label at specified index.
		virtual const char*			get_label_name( PajaTypes::uint32 i );
		//! Returns the value of the label at specified index.
		virtual PajaTypes::int32	get_label_value( PajaTypes::uint32 i );
		//! Removes the label at specified index.
		virtual void				remove_label( PajaTypes::int32 val );

		//! Sets the value of the parameter at specified time.
		virtual PajaTypes::uint32	set_val( PajaTypes::int32 t, const PajaTypes::int32& val );
		//! Gets the value of the parameter at specified time.
		virtual void				get_val( PajaTypes::int32 t, PajaTypes::int32& val );

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		ParamIntC();
		//! Constructor.
		ParamIntC( GizmoI* pParent, const char* name, PajaTypes::int32 value, PajaTypes::uint32 id,
				   PajaTypes::uint32 style = PARAM_STYLE_EDITBOX, bool animatable = true,
				   PajaTypes::int32 min = 0, PajaTypes::int32 max = 0, PajaTypes::int32 inc = 1 );
		//! Constructor with reference to the original.
		ParamIntC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~ParamIntC();

	private:

		struct LabelS {
			PajaTypes::int32	m_i32Val;
			std::string			m_sName;
		};

		PajaTypes::int32		m_i32Const;		// constant value
		PajaTypes::int32		m_i32Inc;
		PajaTypes::int32		m_i32Min, m_i32Max;
		std::vector<LabelS>		m_rLabels;
	};


	//! Float parameter class.
	/*! Float parameter can used to control pretty much everything where
		one parameter is enough. This parameter can have extra styles which
		enables it to be used as a percentage or an angle parameter.

		A new float parameter is created using the create_new() static method.
	*/
	class ParamFloatC : public ParamI
	{
	public:

		//! Creates a new float parameter.
		/*!	\param pParent Pointer to the parent gizmo.
			\param szName The name of the parameter as NULL terminated string.
			\param f32Value Start value of the parameter.
			\param ui32ID ID of the parameter.
			\param ui32Style The style of the parameter.
				Must be PARAM_STYLE_EDITBOX. In addition PARAM_STYLE_PERCENT or PARAM_STYLE_ANGLE can be set.
				Default PARAM_STYLE_EDITBOX.
			\param bAnimatable If set true the parameter is animatable,
				else it's not (you may use PARAM_ANIMATABLE or PARAM_NOT_ANIMATABLE too). Default true.
			\param f32Min The minimum of the parameter value range. Default 0.
			\param f32Max The maximum of the parameter value range. Default 0.
			\param f32Inc The increment of the value spinner in type-in. Default 0.01.

			If minimum and maximum of the range are same no range checking is used.

			The only type-in style is editbox.
			
			If \b PARAM_STYLE_PERCENT is set, percent marks are used
			in the GUI, and also the values are multiplied by 100 before displayed. That is,
			the value for 100% is 1.0.
			
			If \b PARAM_STYLE_ANGLE is set, the angle mark is shown
			in the GUI. The value is not scaled.

			\b Example:
			This example creates new parameter. The name of the parameter is "Rotation",
			it's default value is 0 (zero) and it's ID is ID_ROTATION (constant defined
			by the plugin writer), the type-in style is editbox, the parameter can be animated,
			and no range is defined. Parameter has been marked to be used as rotation.
			Spinner increment is 1.0.

			\code
			m_pParamRot = ParamFloatC::create_new( this, "Rotation", 0,
													ID_ROTATION, PARAM_STYLE_EDITBOX | PARAM_STYLE_ANGLE,
													PARAM_ANIMATABLE, 0, 0, 1.0f );
			\endcode
		*/
		static ParamFloatC*			create_new( GizmoI* pParent,
												const char* name,
												PajaTypes::float32 value,
												PajaTypes::uint32 id,
												PajaTypes::uint32 style = PARAM_STYLE_EDITBOX,
												bool animatable = true,
												PajaTypes::float32 min = 0,
												PajaTypes::float32 max = 0,
												PajaTypes::float32 inc = 0.01f );
		//! Creates new parameter.
		/*!	Following default values are used:
			\code
			pParent = 0,
			szName = 0,
			f32Value = 0,
			ui32ID = 0,
			ui32Style = 0,
			bAnimatable = false,
			f32Min = 0,
			f32Max = 0,
			f32Inc = 0.01f
			\endcode

			\see create_new
		*/
		virtual Edit::DataBlockI*	create();
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		virtual void				copy( Edit::EditableI* pEditable );
		virtual void				restore( Edit::EditableI* pEditable );

		virtual PajaTypes::uint32	get_type() const;
		
		virtual void				set_controller( ControllerC* cnt );
		virtual	ControllerC*		get_controller();

		//! Sets the range of the parameter.
		virtual void				set_min_max( PajaTypes::float32 min, PajaTypes::float32 max );
		//! Returns the minimum of the value range.
		virtual PajaTypes::float32	get_min() const;
		//! Returns the maximum of the value range.
		virtual PajaTypes::float32	get_max() const;
		virtual void				set_increment( PajaTypes::float32 inc );
		virtual PajaTypes::float32	get_increment() const;
		virtual bool				get_min_max( PajaTypes::float32* pMin, PajaTypes::float32* pMax );

		virtual void				set_style( PajaTypes::uint32 style );
		virtual PajaTypes::uint32	get_style() const;

		virtual void				set_id( PajaTypes::uint32 id );
		virtual PajaTypes::uint32	get_id() const;

		//! Sets the value of the parameter at specified time.
		virtual PajaTypes::uint32	set_val( PajaTypes::int32 t, const PajaTypes::float32& val );
		//! Gets the value of the parameter at specified time.
		virtual void				get_val( PajaTypes::int32 t, PajaTypes::float32& val );

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		ParamFloatC();
		//! Constructor.
		ParamFloatC( GizmoI* pParent, const char* name, PajaTypes::float32 value, PajaTypes::uint32 id,
					 PajaTypes::uint32 style = PARAM_STYLE_EDITBOX, bool animatable = true,
					 PajaTypes::float32 min = 0, PajaTypes::float32 max = 0, PajaTypes::float32 inc = 0.01f );
		//! Constructor with reference to the original.
		ParamFloatC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~ParamFloatC();

	private:
		PajaTypes::float32	m_f32Const;
		PajaTypes::float32	m_f32Min, m_f32Max;
		PajaTypes::float32	m_f32Inc;
	};


	//! 2D vector parameter class.
	/*! The 2D vector parameter can used to control everything where
		two parameter is enough. This parameter can have extra styles which
		enables it to be used as a percentage or an angle parameter. Also a couple
		of flags enables it to be used efficiently as positional parameter. This
		parameter type can also be edited in the Layout view if prper styles are set.

		A new 2D vector parameter is created using the create_new() static method.
	*/
	class ParamVector2C : public ParamI
	{
	public:

		//! Creates a new 2D vector parameter.
		/*!	\param pParent Pointer to the parent gizmo.
			\param szName The name of the parameter as NULL terminated string.
			\param rValue Start value of the parameter.
			\param ui32ID ID of the parameter.
			\param ui32Style The style of the parameter. Must be PARAM_STYLE_EDITBOX.
				In addition PARAM_STYLE_PERCENT, PARAM_STYLE_ANGLE, PARAM_STYLE_ABS_POSITION,
				PARAM_STYLE_REL_POSITION, PARAM_STYLE_WORLD_SPACE, or PARAM_STYLE_OBJECT_SPACE can be set.
				Default PARAM_STYLE_EDITBOX.
			\param bAnimatable If set true the parameter is animatable,
				else it's not (you may use PARAM_ANIMATABLE or PARAM_NOT_ANIMATABLE too). Default true.
			\param rMin The minimum of the parameter value range. Default (0, 0).
			\param rMax The maximum of the parameter value range. Default (0, 0).
			\param f32Inc The increment of the value spinner in type-in. Default 0.1.

			If minimum and maximum of the range are same no range checking is used.

			The only type-in style is editbox.
			
			If \b PARAM_STYLE_PERCENT is set, percent marks are used in the GUI,
			and also the values are multiplied by 100 before displayed. That is,
			the value for 100% is 1.0.
			
			If \b PARAM_STYLE_ANGLE is set, the angle mark is shown
			in the GUI. The value is not scaled.

			If \b PARAM_STYLE_ABS_POSITION is set the parameter is treated as absolute position
			from the layout origo (lower left corner). Absolute positions are always in the world
			space. If the parameter is the position of the effect, this style has to be set.

			If \b PARAM_STYLE_REL_POSITION is set the parameter is treated as relative to the
			position of the effect (not including the pivot). If the parameter is the pivot of the
			effect, this style has to be set along with the \b PARAM_STYLE_WORLD_SPACE style.

			If \b PARAM_STYLE_WORLD_SPACE is set the parameter is in the world space coordinate system.
			The world space coordinate system is the same as the layouts coord-sys.

			If \b PARAM_STYLE_OBJECT_SPACE is set the parameter is in the object space coordinate system.
			The object space coordinate system is the same as the effect's coord-sys. If this style is
			set, the point this parameter defines will move along with the effect as it rotates, scales,
			or when it's pivot is changed.

			If the parameter has \b PARAM_STYLE_ABS_POSITION or \b PARAM_STYLE_REL_POSITION style the
			parameter can be edited in the Layout view.

			\b Example:
			This example creates new parameter. The name of the parameter is "Center",
			it's default value is (0, 0) and it's ID is ID_CENTER (constant defined
			by the plugin writer), the type-in style is editbox, the parameter can be animated,
			and no range is defined. The represents a position which is relative to the effect position
			and is in effects coordinate system. The is used as effect's center point and therefore
			it has to transform along with the effect. Spinner increment is default 0.1.

			\code
			m_pParamCenter = ParamVector2C::create_new( this, "Center", Vector2C(), ID_CENTER,
														PARAM_STYLE_EDITBOX | PARAM_STYLE_REL_POSITION | PARAM_STYLE_OBJECT_SPACE,
														PARAM_ANIMATABLE );
			\endcode
		*/
		static ParamVector2C*		create_new( GizmoI* pParent,
												const char* name,
												const PajaTypes::Vector2C& value,
												PajaTypes::uint32 id,
												PajaTypes::uint32 style = PARAM_STYLE_EDITBOX,
												bool animatable = true,
												const PajaTypes::Vector2C& min = PajaTypes::Vector2C(),
												const PajaTypes::Vector2C& max = PajaTypes::Vector2C(),
												PajaTypes::float32 inc = 0.1f );
		//! Creates new parameter.
		/*!	Following default values are used:
			\code
			pParent = 0,
			szName = 0,
			rValue = (0, 0),
			ui32ID = 0,
			ui32Style = 0,
			bAnimatable = false,
			rMin = (0, 0),
			rMax = (0, 0),
			f32Inc = 0.1f
			\endcode

			\see create_new
		*/
		virtual Edit::DataBlockI*	create();
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		virtual void				copy( Edit::EditableI* pEditable );
		virtual void				restore( Edit::EditableI* pEditable );

		virtual PajaTypes::uint32	get_type() const;
		
		virtual void				set_controller( ControllerC* cnt );
		virtual ControllerC*		get_controller();

		//! Sets the range of the parameter.
		virtual void				set_min_max( const PajaTypes::Vector2C& min, const PajaTypes::Vector2C& max );
		//! Returns the minimum of the value range.
		virtual const PajaTypes::Vector2C&	get_min() const;
		//! Returns the maximum of the value range.
		virtual const PajaTypes::Vector2C&	get_max() const;
		virtual bool				get_min_max( PajaTypes::float32* pMin, PajaTypes::float32* pMax );

		virtual void				set_increment( PajaTypes::float32 inc );
		virtual PajaTypes::float32	get_increment() const;

		virtual void				set_style( PajaTypes::uint32 style );
		virtual PajaTypes::uint32	get_style() const;

		virtual void				set_id( PajaTypes::uint32 id );
		virtual PajaTypes::uint32	get_id() const;

		//! Sets the value of the parameter at specified time.
		virtual PajaTypes::uint32	set_val( PajaTypes::int32 t, const PajaTypes::Vector2C& val );
		//! Gets the value of the parameter at specified time.
		virtual void				get_val( PajaTypes::int32 t, PajaTypes::Vector2C& val );

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		ParamVector2C();
		//! Constructor.
		ParamVector2C( GizmoI* pParent, const char* name, const PajaTypes::Vector2C& value, PajaTypes::uint32 id,
					   PajaTypes::uint32 style = PARAM_STYLE_EDITBOX, bool animatable = true,
					   const PajaTypes::Vector2C& min = PajaTypes::Vector2C(),
					   const PajaTypes::Vector2C& max = PajaTypes::Vector2C(), PajaTypes::float32 inc = 0.1f );
		//! Constructor with reference to the original.
		ParamVector2C( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~ParamVector2C();

	private:
		PajaTypes::Vector2C	m_rConst;
		PajaTypes::Vector2C	m_rMin, m_rMax;
		PajaTypes::float32	m_f32Inc;
	};


	//! 3D vector parameter class.
	/*! The 3D vector parameter can used to control everything where
		three parameters are enough. This parameter can have extra styles which
		enables it to be used as a percentage or an angle parameter.

		A new 3D vector parameter is created using the create_new() static method.
	*/
	class ParamVector3C : public ParamI
	{
	public:

		//! Creates a new 3D vector parameter.
		/*!	\param pParent Pointer to the parent gizmo.
			\param szName The name of the parameter as NULL terminated string.
			\param rValue Start value of the parameter.
			\param ui32ID ID of the parameter.
			\param ui32Style The style of the parameter. Must be PARAM_STYLE_EDITBOX.
				In addition PARAM_STYLE_PERCENT or PARAM_STYLE_ANGLE can be set.
				Default PARAM_STYLE_EDITBOX.
			\param bAnimatable If set true the parameter is animatable,
				else it's not (you may use PARAM_ANIMATABLE or PARAM_NOT_ANIMATABLE too). Default true.
			\param rMin The minimum of the parameter value range. Default (0, 0, 0).
			\param rMax The maximum of the parameter value range. Default (0, 0, 0).
			\param f32Inc The increment of the value spinner in type-in. Default 0.1.

			If minimum and maximum of the range are same no range checking is used.

			The only type-in style is editbox.
			
			If \b PARAM_STYLE_PERCENT is set, percent marks are used in the GUI,
			and also the values are multiplied by 100 before displayed. That is,
			the value for 100% is 1.0.
			
			If \b PARAM_STYLE_ANGLE is set, the angle mark is shown
			in the GUI. The value is not scaled.

			\b Example:
			This example creates new parameter. The name of the parameter is "Position",
			it's default value is (0, 0, 0) and it's ID is ID_POSITION (constant defined
			by the plugin writer), the type-in style is editbox, the parameter can be animated,
			and no range is defined. Spinner increment is default 0.1.

			\code
			m_pParamPos = ParamVector3C::create_new( this, "Position", Vector3C(), ID_POSITION,
														PARAM_STYLE_EDITBOX, PARAM_ANIMATABLE );
			\endcode
		*/
		static ParamVector3C*		create_new( GizmoI* pParent,
												const char* szName,
												const PajaTypes::Vector3C& rValue,
												PajaTypes::uint32 ui32ID,
												PajaTypes::uint32 ui32Style = PARAM_STYLE_EDITBOX,
												bool bAnimatable = true,
												const PajaTypes::Vector3C& rMin = PajaTypes::Vector3C(),
												const PajaTypes::Vector3C& rMax = PajaTypes::Vector3C(),
												PajaTypes::float32 f32Inc = 0.1f );
		//! Creates new parameter.
		/*!	Following default values are used:
			\code
			pParent = 0,
			szName = 0,
			rValue = (0, 0, 0),
			ui32ID = 0,
			ui32Style = 0,
			bAnimatable = false,
			rMin = (0, 0, 0),
			rMax = (0, 0, 0),
			f32Inc = 0.1f
			\endcode

			\see create_new
		*/
		virtual Edit::DataBlockI*	create();
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		virtual void				copy( Edit::EditableI* pEditable );
		virtual void				restore( Edit::EditableI* pEditable );

		virtual PajaTypes::uint32	get_type() const;
		
		virtual void				set_controller( ControllerC* cnt );
		virtual ControllerC*		get_controller();

		//! Sets the range of the parameter.
		virtual void				set_min_max( const PajaTypes::Vector3C& min, const PajaTypes::Vector3C& max );
		//! Returns the minimum of the value range.
		virtual const PajaTypes::Vector3C&	get_min() const;
		//! Returns the maximum of the value range.
		virtual const PajaTypes::Vector3C&	get_max() const;

		virtual void				set_increment( PajaTypes::float32 inc );
		virtual PajaTypes::float32	get_increment() const;
		virtual bool				get_min_max( PajaTypes::float32* pMin, PajaTypes::float32* pMax );

		virtual void				set_style( PajaTypes::uint32 style );
		virtual PajaTypes::uint32	get_style() const;

		virtual void				set_id( PajaTypes::uint32 id );
		virtual PajaTypes::uint32	get_id() const;

		//! Sets the value of the parameter at specified time.
		virtual PajaTypes::uint32	set_val( PajaTypes::int32 t, const PajaTypes::Vector3C& val );
		//! Gets the value of the parameter at specified time.
		virtual void				get_val( PajaTypes::int32 t, PajaTypes::Vector3C& val );

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		ParamVector3C();
		//! Constructor.
		ParamVector3C( GizmoI* pParent, const char* name, const PajaTypes::Vector3C& value, PajaTypes::uint32 id,
					   PajaTypes::uint32 style = PARAM_STYLE_EDITBOX, bool animatable = true,
					   const PajaTypes::Vector3C& min = PajaTypes::Vector3C(),
					   const PajaTypes::Vector3C& max = PajaTypes::Vector3C(), PajaTypes::float32 inc = 0.1f );
		//! Constructor with reference to the original.
		ParamVector3C( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~ParamVector3C();

	private:
		PajaTypes::Vector3C	m_rConst;
		PajaTypes::Vector3C	m_rMin, m_rMax;
		PajaTypes::float32	m_f32Inc;
	};

	//! Color parameter class.
	/*! The color parameter class can be used to control four component color variables.
		The components are R, G, B, A. It's up to the user how the components are threated.
		Colors are chosen using a color picker type-in.

		The color parameter can only be used to control color values from 0 to 1. The values
		are shown in the GUI in range 0 to 255.

		A new color parameter is created using the create_new() static method.
	*/
	class ParamColorC : public ParamI
	{
	public:

		//! Creates a new color parameter.
		/*!	\param pParent Pointer to the parent gizmo.
			\param szName The name of the parameter as NULL terminated string.
			\param rValue Start value of the parameter.
			\param ui32ID ID of the parameter.
			\param ui32Style The style of the parameter. Must be PARAM_STYLE_COLORPICKER_RGB or PARAM_STYLE_COLORPICKER_RGBA.
				Default PARAM_STYLE_COLORPICKER_RGBA.
			\param bAnimatable If set true the parameter is animatable,
				else it's not (you may use PARAM_ANIMATABLE or PARAM_NOT_ANIMATABLE too). Default true.

			The only type-in style is color picker.

			The range and the spinner increment are set internally.
  
			\b Example:
			This example creates new parameter. The name of the parameter is "Color",
			it's default value is opaque white and it's ID is ID_COLOR (constant defined
			by the plugin writer), the type-in style is color picker, the parameter can be animated.

			\code
			m_pParamColor = ParamColorC::create_new( this, "Color", ColorC( 1, 1, 1, 1 ), ID_COLOR,
														PARAM_STYLE_COLORPICKER_RGBA, PARAM_ANIMATABLE );
			\endcode
		*/
		static ParamColorC*			create_new( GizmoI* pParent,
												const char* szName,
												const PajaTypes::ColorC& rValue,
												PajaTypes::uint32 ui32ID,
												PajaTypes::uint32 ui32Style = PARAM_STYLE_COLORPICKER_RGBA,
												bool bAnimatable = true );
		//! Creates new parameter.
		/*!	Following default values are used:
			\code
			pParent = 0,
			szName = 0,
			rValue = (0, 0, 0, 1), //RGBA
			ui32ID = 0,
			ui32Style = 0,
			bAnimatable = false,
			\endcode

			\see create_new
		*/
		virtual Edit::DataBlockI*	create();
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		virtual void				copy( Edit::EditableI* pEditable );
		virtual void				restore( Edit::EditableI* pEditable );

		virtual PajaTypes::uint32	get_type() const;
		
		virtual void				set_controller( ControllerC* cnt );
		virtual ControllerC*		get_controller();

		//! Set spinner increment (ignored for color parameter).
		virtual void				set_increment( PajaTypes::float32 inc );
		virtual PajaTypes::float32	get_increment() const;
		virtual bool				get_min_max( PajaTypes::float32* pMin, PajaTypes::float32* pMax );

		virtual void				set_style( PajaTypes::uint32 style );
		virtual PajaTypes::uint32	get_style() const;

		virtual void				set_id( PajaTypes::uint32 id );
		virtual PajaTypes::uint32	get_id() const;

		//! Sets the value of the parameter at specified time.
		virtual PajaTypes::uint32	set_val( PajaTypes::int32 t, const PajaTypes::ColorC& val );
		//! Gets the value of the parameter at specified time.
		virtual void				get_val( PajaTypes::int32 t, PajaTypes::ColorC& val );

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		ParamColorC();
		//! Constructor.
		ParamColorC( GizmoI* pParent, const char* name, const PajaTypes::ColorC& value, PajaTypes::uint32 id,
					 PajaTypes::uint32 style = PARAM_STYLE_COLORPICKER_RGBA, bool animatable = true );
		//! Constructor with reference to the original.
		ParamColorC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~ParamColorC();

	private:
		PajaTypes::ColorC	m_rConst;
	};


	//! Text parameter class.
	/*!	Text parameter class provides a simple text input to the effect.
		The text parameter cannot be animated.

		Range and spinner increment are not used.
		
		A new text parameter is created using the create_new() static method.
	*/
	class ParamTextC : public ParamI
	{
	public:

		//! Creates a new color parameter.
		/*!	\param pParent Pointer to the parent gizmo.
			\param szName The name of the parameter as NULL terminated string.
			\param rValue Start value of the parameter.
			\param ui32ID ID of the parameter.
			\param ui32Style The style of the parameter. Must be PARAM_STYLE_EDITBOX.
				Default PARAM_STYLE_EDITBOX.

			The only type-in style is edit box.

			\b Example:
			This example creates new parameter. The name of the parameter is "Text",
			it's default value is string "Text" and it's ID is ID_TEXT (constant defined
			by the plugin writer), the type-in style is the default editbox.

			\code
			m_pParamText = ParamTextC::create_new( this, "Text", "Text", ID_TEXT );
			\endcode
		*/
		static ParamTextC*			create_new( GizmoI* pParent,
												const char* szName,
												const char* szText,
												PajaTypes::uint32 id,
												PajaTypes::uint32 style = PARAM_STYLE_EDITBOX );

		//! Creates new parameter.
		/*!	Following default values are used:
			\code
			pParent = 0,
			szName = 0,
			szValue = 0,
			ui32ID = 0,
			ui32Style = 0,
			\endcode

			\see create_new
		*/
		virtual Edit::DataBlockI*	create();
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		virtual void				copy( Edit::EditableI* pEditable );
		virtual void				restore( Edit::EditableI* pEditable );

		virtual PajaTypes::uint32	get_type() const;
		
		virtual void				set_controller( ControllerC* cnt );
		virtual ControllerC*		get_controller();

		virtual void				set_style( PajaTypes::uint32 style );
		virtual PajaTypes::uint32	get_style() const;

		virtual void				set_id( PajaTypes::uint32 id );
		virtual PajaTypes::uint32	get_id() const;

		virtual void				set_increment( PajaTypes::float32 inc );
		virtual PajaTypes::float32	get_increment() const;
		virtual bool				get_min_max( PajaTypes::float32* pMin, PajaTypes::float32* pMax );

		//! Sets the value of the parameter as NULL terminated string at specified time.
		virtual PajaTypes::uint32	set_val( PajaTypes::int32 i32Time, const char* szVal );
		//! Returns the value of the parameter.
		/*!	\param i32Time Point in time to get the value.
			\param szVal pointer to the string to copy the value.
			\param i32Size size of the string to copy the value.
			\return Pointer to the parameter string.
		*/
		virtual const char*			get_val( PajaTypes::int32 i32Time, char* szVal = 0, PajaTypes::int32 i32Size = 0 );

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		ParamTextC();
		//! Constructor.
		ParamTextC( GizmoI* pParent, const char* name, const char* text, PajaTypes::uint32 id,
					PajaTypes::uint32 style = PARAM_STYLE_EDITBOX );
		//! Constructor with reference to the original.
		ParamTextC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~ParamTextC();

	private:
		std::string		m_sText;
	};


	//! File parameter class.
	/*! The file parameter provides access to the imported files in the Demopaja system
		for the effect plugins.

		Range and spinner increment are not used.

		A new file parameter is created using the create_new() static method.
	*/
	class ParamFileC : public ParamI
	{
	public:

		//! Creates a new file parameter.
		/*!	\param pParent Pointer to the parent gizmo.
			\param rSuperClassFilter Specifies super class filter.
			\param rClassFilter Specifies class filter.
			\param ui32ID ID of the parameter.
			\param ui32Style The style of the parameter. Must be PARAM_STYLE_FILE.
				Default PARAM_STYLE_FILE.

			The only type-in style is file selector.

			The super class filter and class filter enables only a defined set of files
			to be set via this parameter. For example if the superclass filter is 
			SUPERCLASS_IMAGE all the files that are selected via this parameter implement
			the Import::ImportableImageI interface. Passing null (super) class ID disables
			the filtering.

			\b Example 1:
			This example creates new parameter. The name of the parameter is "Image",
			it's super class filter is set to enable only files which support the
			ImportableImageI interface, any sub-type can be used. Parameter's ID is
			ID_IMAGE (constant defined by the plugin writer), the type-in style is
			the default file selector.

			\code
			m_pParamFile = ParamFileC::create_new( this, "Image", SUPERCLASS_IMAGE, NULL_CLASSID, ID_IMAGE );
			\endcode

			\b Example 2:
			This example creates new parameter. The name of the parameter is "Scene",
			the class filters are set to enable only specified file type to be set.
			Parameter's ID is ID_SCENE (constant defined by the plugin writer), the
			type-in style is the default file selector.

			\code
			m_pParamFile = ParamFileC::create_new( this, "Scene", NULL_SUPERCLASSID, MY_3DFILE_CLASSID, ID_3DSCENE );
			\endcode
		*/
		static ParamFileC*			create_new( GizmoI* pParent, const char* szName,
												PluginClass::SuperClassIdC rSuperClassFilter,
												PluginClass::ClassIdC rClassFilter,
												PajaTypes::uint32 ui32ID,
												PajaTypes::uint32 ui32Style = PARAM_STYLE_FILE,
												bool bAnimatable = false );
		//! Creates new parameter.
		/*!	Following default values are used:
			\code
			pParent = 0,
			rSuperClassFilter = NULL_SUPERCLASSID,
			rClassFilter = NULL_CLASSID,
			ui32ID = 0,
			ui32Style = 0,
			bAnimatable = false
			\endcode

			\see create_new
		*/
		virtual Edit::DataBlockI*	create();
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		virtual void				copy( Edit::EditableI* pEditable );
		virtual void				restore( Edit::EditableI* pEditable );

		virtual PajaTypes::uint32	get_type() const;
		
		virtual void				set_controller( ControllerC* cnt );
		virtual ControllerC*		get_controller();

		virtual void				set_style( PajaTypes::uint32 style );
		virtual PajaTypes::uint32	get_style() const;

		virtual void				set_id( PajaTypes::uint32 id );
		virtual PajaTypes::uint32	get_id() const;

		virtual void				set_increment( PajaTypes::float32 inc );
		virtual PajaTypes::float32	get_increment() const;
		virtual bool				get_min_max( PajaTypes::float32* pMin, PajaTypes::float32* pMax );

		//! Sets the file handle.
		virtual PajaTypes::uint32			set_file( PajaTypes::int32 t, Import::FileHandleC* pHandle );
		//! Returns the file handle.
		virtual Import::FileHandleC*		get_file( PajaTypes::int32 t );
		//! Returns the file handle and file evaluation time.
		virtual void						get_file( PajaTypes::int32 t, Import::FileHandleC*& pHandle, PajaTypes::int32& i32FileTime );

		//! Returns the super class filter.
		virtual PluginClass::SuperClassIdC	get_super_class_filter();
		//! Returns the super filter.
		virtual PluginClass::ClassIdC		get_class_filter();

		//! Sets the super class filter.
		virtual void				set_super_class_filter( PluginClass::SuperClassIdC rSuperClassId );
		//! Sets the class filter.
		virtual void				set_class_filter( PluginClass::ClassIdC rClassId );

		//! Sets the time offset which is used to display the file duration.
		/*!	
			\see set_time_scale()
		*/
		virtual PajaTypes::uint32			set_time_offset( PajaTypes::int32 t, PajaTypes::int32 i32TimeOffset );
		//! Sets the scaling factor which is used to scale the duration got from the file.
		/*!	
			\see set_time_offset()
		*/
		virtual PajaTypes::uint32			set_time_scale( PajaTypes::int32 t, PajaTypes::float32 f32TimeScale );

		//! Returns the time offset\. See: set_time_offset().
		virtual PajaTypes::int32	get_time_offset( PajaTypes::int32 t );
		//! Returns the time scale factor\. See: set_time_scale().
		virtual PajaTypes::float32	get_time_scale( PajaTypes::int32 t );

		// return negative value the duration can't be specified (the file is still).

		//! Returns the duration of the file.
		/*!	If the duration cannot be determined (for example the data is a still image)
			negative value is returned.

			This information is used by the system to draw the duration of the file.

			\see set_time_scale()
			\see set_time_offset()
		*/
		virtual PajaTypes::int32	get_duration( PajaTypes::int32 t );
		//! Returns start label/frame.
		virtual	PajaTypes::float32	get_start_label( PajaTypes::int32 t );
		//! Returns end label/frame.
		virtual	PajaTypes::float32	get_end_label( PajaTypes::int32 t );

		//! Used internally (updates all file handles this parameter has, including controller).
		virtual PajaTypes::uint32	update_file_handles( Import::FileHandleC* pNewHandle );
		//! Used internally (updates all file handles' reference information this parameter has, including controller).
		virtual void				update_file_references();

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		ParamFileC();
		//! Constructor.
		ParamFileC( GizmoI* pParent, const char* name, PluginClass::SuperClassIdC rSuperClassFilter,
					PluginClass::ClassIdC rClassFilter, PajaTypes::uint32 id,
					PajaTypes::uint32 style = PARAM_STYLE_EDITBOX, bool bAnimatable = true );
		//! Constructor with reference to the original.
		ParamFileC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~ParamFileC();

	private:
		Import::FileHandleC*		m_pHandle;
		PluginClass::SuperClassIdC	m_rSuperClassFilter;
		PluginClass::ClassIdC		m_rClassFilter;
		PajaTypes::int32			m_i32TimeOffset;
		PajaTypes::float32			m_f32TimeScale;
	};


	//! Link parameter class.
	/*! The link parameter provides access to other effects in the Demopaja system
		for the effect plugins.

		Range and spinner increment are not used.

		A new link parameter is created using the create_new() static method.
	*/
	class ParamLinkC : public ParamI
	{
	public:

		//! Creates a new link parameter.
		/*!	\param pParent Pointer to the parent gizmo.
			\param rSuperClassFilter Specifies super class filter.
			\param rClassFilter Specifies class filter.
			\param ui32ID ID of the parameter.
			\param ui32Style The style of the parameter. Must be either PARAM_STYLE_SINGLE_LINK or PARAM_STYLE_MULTI_LINK.
				Default PARAM_STYLE_SINGLE_LINK.

			The only type-in style is link selector.

			The super class filter and class filter enables only a defined set of effects
			to be linked via this parameter. For example if the superclass filter is 
			SUPERCLASS_MASK all the effects that are linked via this parameter implement
			the mask image interface. Passing null (super) class ID disables the filtering.

			\b Example:

			\code
			m_pParamMask = ParamLinkC::create_new( pGizmoAttrib, "Mask", SUPERCLASS_MASK, NULL_CLASS_ID, ID_ATTIB_MASK, PARAM_STYLE_SINGLE_LINK );
			\endcode
		*/
		static ParamLinkC*			create_new( GizmoI* pParent, const char* szName,
												PluginClass::SuperClassIdC rSuperClassFilter,
												PluginClass::ClassIdC rClassFilter,
												PajaTypes::uint32 ui32ID,
												PajaTypes::uint32 ui32Style = PARAM_STYLE_SINGLE_LINK );
		//! Creates new parameter.
		/*!	Following default values are used:
			\code
			pParent = 0,
			rSuperClassFilter = NULL_SUPERCLASSID,
			rClassFilter = NULL_CLASSID,
			ui32ID = 0,
			ui32Style = 0,
			\endcode

			\see create_new
		*/
		virtual Edit::DataBlockI*	create();
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		virtual void				copy( Edit::EditableI* pEditable );
		virtual void				restore( Edit::EditableI* pEditable );

		virtual PajaTypes::uint32	get_type() const;
		
		virtual void				set_controller( ControllerC* cnt );
		virtual ControllerC*		get_controller();

		virtual void				set_style( PajaTypes::uint32 style );
		virtual PajaTypes::uint32	get_style() const;

		virtual void				set_id( PajaTypes::uint32 id );
		virtual PajaTypes::uint32	get_id() const;

		virtual void				set_increment( PajaTypes::float32 inc );
		virtual PajaTypes::float32	get_increment() const;
		virtual bool				get_min_max( PajaTypes::float32* pMin, PajaTypes::float32* pMax );

		//! Adds new link.
		virtual PajaTypes::uint32			add_link( EffectI* pEffect );
		//! Adds new link.
		virtual PajaTypes::uint32			insert_link( PajaTypes::int32 i32InsAt, EffectI* pEffect );
		//! Sets the link handle.
		virtual PajaTypes::uint32			del_link( PajaTypes::uint32 ui32Index );
		//! Sets the link handle.
		virtual PajaTypes::uint32			del_link( EffectI* pEffect );
		//! Sets the link handle.
		virtual PajaTypes::uint32			del_all_links();
		//! Sets the link handle.
		virtual PajaTypes::uint32			set_link( PajaTypes::uint32 ui32Index, EffectI* pEffect );
		//! Returns the link handle.
		virtual PajaTypes::uint32			get_link_count() const;
		//! Returns the link handle.
		virtual EffectI*							get_link( PajaTypes::uint32 ui32Index );

		//! Returns the super class filter.
		virtual PluginClass::SuperClassIdC	get_super_class_filter();
		//! Returns the super filter.
		virtual PluginClass::ClassIdC		get_class_filter();

		//! Sets the super class filter.
		virtual void				set_super_class_filter( PluginClass::SuperClassIdC rSuperClassId );
		//! Sets the class filter.
		virtual void				set_class_filter( PluginClass::ClassIdC rClassId );

		//! Used internally (updates all link handles' reference information this parameter has).
		virtual void				update_link_references();

		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		ParamLinkC();
		//! Constructor.
		ParamLinkC( GizmoI* pParent, const char* name, PluginClass::SuperClassIdC rSuperClassFilter,
					PluginClass::ClassIdC rClassFilter, PajaTypes::uint32 id,
					PajaTypes::uint32 style = PARAM_STYLE_SINGLE_LINK );
		//! Constructor with reference to the original.
		ParamLinkC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~ParamLinkC();

	private:
		struct LinkS
		{
			EffectI*	m_pEffect;
			PajaTypes::uint32	m_ui32ID;
		};
		std::vector<LinkS*>					m_vecLinks;
		PluginClass::SuperClassIdC	m_rSuperClassFilter;
		PluginClass::ClassIdC				m_rClassFilter;
	};



};	// namespace

#endif
