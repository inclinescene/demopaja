//-------------------------------------------------------------------------
//
// File:		DeviceInterfaceI.h
// Desc:		Device Interface interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_DEVICEINTERFACEI_H__
#define __DEMOPAJA_DEVICEINTERFACEI_H__

// forward declaration
//namespace PajaSystem {
//	class DeviceInterfaceI;
//};

#include "PajaTypes.h"
#include "ClassIdC.h"
#include "DataBlockI.h"
#include "FileIO.h"

namespace PajaSystem {

	//! Device state, \see DeviceInterfaceI::get_state().
	enum DeviceStateE {
		DEVICE_STATE_OK,				//!< Device OK.
		DEVICE_STATE_LOST,				//!< Device lost, some functionality may not be working.
		DEVICE_STATE_SHUTTINGDOWN,		//!< Device is about to be released.
	};

	//! Device interface.
	/*!	Device interface is the base class interface for all devices the effects in
		Demopaja system can use.
	*/
	class DeviceInterfaceI : public Edit::DataBlockI
	{
	public:

		//! Returns super class ID.
		virtual PluginClass::SuperClassIdC		get_super_class_id() const = 0;
		//! Returns class ID.
		virtual PluginClass::ClassIdC			get_class_id() const = 0;
		//! Returns device's class name as NULL terminated string.
		virtual const char*						get_class_name() = 0;

		//! Returns true if device is exclusive.
		/*!	Exclusive device means that there can be only one device of
			this superclass in the device context.

			Default implementation returns false.
		*/
		virtual bool							get_exclusive() const;

		//! Returns device state, \see DeviceStateE.
		virtual PajaTypes::uint32				get_state() const;
		//! Sets device state, \see DeviceStateE.
		virtual void							set_state( PajaTypes::uint32 ui32State );

		//! Extends device functionality, default implementation returns NULL.
		virtual PajaSystem::DeviceInterfaceI*	query_interface( const PluginClass::SuperClassIdC& rSuperClassId );

		//! Serialize device settings to a Demopaja output stream.
		virtual PajaTypes::uint32				save( FileIO::SaveC* pSave ) = 0;
		//! Serialize device settings from a Demopaja input stream.
		virtual PajaTypes::uint32				load( FileIO::LoadC* pLoad ) = 0;

	protected:
		//! Default constructor.
		DeviceInterfaceI();
		//! Default destructor.
		virtual ~DeviceInterfaceI();

		PajaTypes::uint32	m_ui32State;
	};

}; // namespace

#endif // __DEMOPAJA_DEVICEINTERFACEI_H__