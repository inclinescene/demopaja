//-------------------------------------------------------------------------
//
// File:		Composition.h
// Desc:		Composition namespace header.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_COMPOSITION_H__
#define __DEMOPAJA_COMPOSITION_H__

//! The composition namespace
/*!	The classes collected into the Composition namespace are
	used as parts of demo composition. The classes range from a controller
	and parameter to a layer or complete scene.
*/
namespace Composition {

	//! Common flags Effect, Gizmo, and Parameter.
	enum ItemFlagsE {
		ITEM_EXPANDED		= 0x0001,
		ITEM_SELECTED		= 0x0002,
		ITEM_VISIBLE		= 0x0004,
		ITEM_LOCKED			= 0x0008,
		ITEM_LAYER			= 0x0010,
		ITEM_EFFECT			= 0x0020,
		ITEM_GIZMO			= 0x0040,
		ITEM_PARAMETER		= 0x0080,
		ITEM_ANIMATED		= 0x0100,
		ITEM_ANIMATABLE		= 0x0200,
		ITEM_CLAMPVALUES	= 0x0400,
		ITEM_EXPANDABLE		= 0x0800,
		ITEM_GUIHIDDEN		= 0x1000,
	};

};

#endif // __DEMOPAJA_COMPOSITION_H__