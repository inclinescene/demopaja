//-------------------------------------------------------------------------
//
// File:		TimeContextC.h
// Desc:		Time context class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_TIMECONTEXTC_H__
#define __DEMOPAJA_TIMECONTEXTC_H__

#include "PajaTypes.h"

namespace PajaSystem {

	//! Time context class.
	/*!	Time context is helper class which provides the timing information to the effect plugin class.
		There are also couple of methods which helps converting timing information provided in FPS
		form to the Demopaja time and back.

		This class is implemented by the system.
	*/
	class TimeContextC
	{
	public:
		//! Default constructor (used internally).
		TimeContextC( PajaTypes::int32 i32BeatsPerMin, PajaTypes::int32 i32QNotesPerBeat, PajaTypes::int32 i32EditAccuracy );

		//! Default destructor (used internally).
		virtual ~TimeContextC();

		//! Converts time in FPS timecode to Demopaja timecode.
		/*!	\param f64Time Time in FPS (frames per second) timecode
			\param f64FPS Frames per second.

			Example:
			The animation in a file is defined as 30 FPS and the first frame
			is 10 and last frame is 520, and you should convert the time
			in Demopaja system.
			\code
			int32	i32FPS = 30;
			int32	i32FirstFrame = 10;
			int32	i32LastFrame = 520;
			int32	i32Length = i32LastFrame - i32FirstFrame;
			// Convert the time. (FPS -> Demopaja)
			float64	f64Time = pTimeContext->convert_fps_to_time( i32Length, i32FPS );
			\endcode
		*/
		virtual PajaTypes::float64	convert_fps_to_time( PajaTypes::float64 f64Time, PajaTypes::float64 f64FPS );

		//! Converts time in Demopaja timecode to FPS timecode.
		/*!	\param f64Time Time in Demopaja timecode
			\param f64FPS Frames per second.

			Example:
			Your animation is defined as 30 FPS, 160 ticks per frame, and you want to get the
			frame number at current time (i32Time in the example):

			\code
			int32	i32FPS = 30;
			int32	i32TicksPerFrame = 160;
			// Convert the time. (Demopaja -> FPS)
			m_i32Frame = (int32)(pTimeContext->convert_time_to_fps( i32Time, i32FPS ) * (float64)i32TicksPerFrame);
			// Adjust the animation to start at the first frame.
			m_i32Frame += i32FirstFrame * i32TicksPerFrame;
			\endcode
		*/
		virtual PajaTypes::float64	convert_time_to_fps( PajaTypes::float64 f64FpsTime, PajaTypes::float64 f64FPS );

		//! Returns beats per minute (BPM).
		virtual	PajaTypes::int32	get_beats_per_min() const;

		//! Returns quarter notes per beat.
		virtual	PajaTypes::int32	get_qnotes_per_beat() const;

		//! Returns edit accuracy.
		virtual	PajaTypes::int32	get_edit_accuracy() const;

		//! Returns the size of one frame in ticks.
		virtual	PajaTypes::int32	get_frame_size_in_ticks() const;

		//! Returns unique ID for a frame.
		/*!	Each time a frame is rendered the ID is incremented.
			The frame ID can be used to determine if a frame is rerendered,
			even the evaluation time has not changed.
		*/
		virtual PajaTypes::uint32	get_frame_id() const;

		//! Sets the frame ID, used internally.
		virtual void				set_frame_id( PajaTypes::uint32 ui32ID );

		//! Sets the time settings, used internally.
		virtual void				set_time_settings( PajaTypes::int32 i32BeatsPerMin, PajaTypes::int32 i32QNotesPerBeat, PajaTypes::int32 i32EditAccuracy );

	private:
		PajaTypes::int32	m_i32BeatsPerMin;
		PajaTypes::int32	m_i32QNotesPerBeat;
		PajaTypes::int32	m_i32EditAccuracy;
		PajaTypes::float64	m_f64TimeScale;
		PajaTypes::float64	m_f64InvTimeScale;
		PajaTypes::uint32	m_ui32FrameID;
	};

}; // namespace

#endif // __DEMOPAJA_TIMECONTEXTC_H__