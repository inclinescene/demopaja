//-------------------------------------------------------------------------
//
// File:		FileHandleC.h
// Desc:		File handle class.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_FILEHANDLEC_H__
#define __DEMOPAJA_FILEHANDLEC_H__


namespace Import {
	class FileHandleC;
};


#include "PajaTypes.h"
#include "DataBlockI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ImportableI.h"
#include <string>
#include <list>

namespace Import {

	//! Base classID.
	const PluginClass::SuperClassIdC		BASECLASS_FILEHANDLE = PluginClass::SuperClassIdC( 1, 3 );

	//! File handle Flags.
	enum FileHandleFlagsE {
		FILEHANDLE_FOLDER =					0x001,		//!< Flag indicating that the file handle is a folder.
		FILEHANDLE_FOLDER_EXPANDED =		0x002,		//!< Flag indicating that the folder is expanded.
		FILEHANDLE_FOLDERCOLOR_WHITE =		0x010,		//!< Color flag.
		FILEHANDLE_FOLDERCOLOR_RED =		0x020,		//!< Color flag.
		FILEHANDLE_FOLDERCOLOR_YELLOW =		0x040,		//!< Color flag.
		FILEHANDLE_FOLDERCOLOR_GREEN =		0x080,		//!< Color flag.
		FILEHANDLE_FOLDERCOLOR_CYAN =		0x100,		//!< Color flag.
		FILEHANDLE_FOLDERCOLOR_BLUE =		0x200,		//!< Color flag.
		FILEHANDLE_FOLDERCOLOR_MAGENTA =	0x400,		//!< Color flag.
		FILEHANDLE_FOLDERCOLOR_BLACK =		0x800,		//!< Color flag.
		FILEHANDLE_FOLDERCOLOR_MASK =		0xff0,		//!< Color mask flag.
		FILEHANDLE_INVALID_ID = -1,						//!< Used to represend invalid file ID with get_id() method.
	};

	//! File handle class.
	/*!	The data loaded into Demopaja can be accessed via ImportableI interface.
		This file handle class is used to hold that interface. Even if files fail to
		load the file handle will always be there, so the file handle works as kind of
		smart reference to the file.

		This class is implemented by the system.
	*/
	class FileHandleC : public Edit::EditableI
	{
	public:
		//! Creates new file handle.
		static FileHandleC*			create_new();
		//! Creates new file handle.
		virtual Edit::DataBlockI*	create();
		//! Creates new file handle (used internally), see Edit::EditableI::create().
		virtual Edit::DataBlockI*	create( Edit::EditableI* pOriginal );
		//! Deep copy from a data block, see Edit::DataBlockI::copy().
		virtual void				copy( Edit::EditableI* pEditable );
		//! Shallow copy from a editable, see Edit::EditableI::restore().
		virtual void				restore( Edit::EditableI* pEditable );
		//! Returns the base class ID.
		virtual PluginClass::SuperClassIdC		get_base_class_id() const;
		//! Update notify from a connected editable.
		virtual PajaTypes::uint32							update_notify( EditableI* pCaller );

		//! Returns the importable the file handle refers to.
		/*!	The return value can be NULL if the file is not
			present (failure in load, etc.). Always check the
			return value.
		*/
		virtual ImportableI*		get_importable();

		//! Sets the importable the file handle refers to. Used internally.
		virtual void				set_importable( ImportableI* pImportable );

		//! Get serialization ID.
		/*!
		*/
		virtual PajaTypes::uint32	get_id();

		//! Set serialization ID.
		/*!	When the data is serialized out each file handle is given an ID which
			is used on to patch the file handles when the data
			is serialized in. The system sets this ID and it should not be changed.

			\see FileIO::LoadC::add_file_handle_patch
		*/
		virtual void				set_id( PajaTypes::uint32 i32ID );

		//! Increase reference count (used internally).
		virtual void				add_reference( EditableI* pEditable );
		//! Reset reference count (used internally).
		virtual void				reset_references();
		//! Returns reference count (used internally).
		virtual PajaTypes::int32	get_reference_count();

		//! Returns file handle which is parent to this handle (used only in editor).
		virtual FileHandleC*		get_parent_handle();
		//! Sets the file handle which is parent to this handle (used only in editor).
		virtual void				set_parent_handle( FileHandleC* pHandle );
		//! Returns file handle ID which is parent to this handle (used only in editor).
		virtual PajaTypes::int32	get_parent_id();
		//! Sets the file handle ID which is parent to this handle (used only in editor).
		virtual void				set_parent_id( PajaTypes::int32 i32ID );

		//! Returns the name of the folder if the file handle is used as a folder (used only in editor).
		virtual const char*			get_folder_name() const;
		//! Sets the the name of the folder if the file handle is used as a folder (used only in editor).
		virtual void				set_folder_name( const char* szName );

		virtual void				set_flags( PajaTypes::int32 i32Flags );
		virtual void				add_flags( PajaTypes::int32 i32Flags );
		virtual void				del_flags( PajaTypes::int32 i32Flags );
		virtual void				toggle_flags( PajaTypes::int32 i32Flags );
		virtual PajaTypes::int32	get_flags();

		// Serialize editable to a Demopaja output stream.
		virtual PajaTypes::uint32	save( FileIO::SaveC* pSave );
		// Serialize editable from a Demopaja input stream.
		virtual PajaTypes::uint32	load( FileIO::LoadC* pLoad );

	protected:
		//! Default constructor.
		FileHandleC();
		//! Default constructor with reference to the original.
		FileHandleC( Edit::EditableI* pOriginal );
		//! Default destructor.
		virtual ~FileHandleC();

	private:
		PajaTypes::int32	m_i32RefCount;
		ImportableI*		m_pImportable;
		PajaTypes::uint32	m_i32ID;
		FileHandleC*		m_pParent;
		PajaTypes::uint32	m_i32ParentID;
		PajaTypes::int32	m_i32Flags;
		std::string			m_sFolderName;
		std::list<EditableI*>	m_lstReferences;
	};

};	// namespace

#endif