//-------------------------------------------------------------------------
//
// File:		OpenGLBufferI.h
// Desc:		OpenGL off-screen graphics rendering interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://moppi.inside.org/demopaja/
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_OPENGLBUFFERC_H__
#define __DEMOPAJA_OPENGLBUFFERC_H__

// Forward declaration
namespace PajaSystem {
	class OpenGLBufferC;
};

#include <gl\gl.h>
#include "wglext.h"
#include "PajaTypes.h"
#include "DeviceInterfaceI.h"
#include "DataBlockI.h"
#include "ClassIdC.h"
#include "GraphicsDeviceI.h"
#include "OpenGLViewportC.h"
#include "OpenGLDeviceC.h"

namespace PajaSystem {


	//! The ID of the OpenGL buffer interface.
	const PluginClass::ClassIdC	CLASS_OPENGL_BUFFER = PluginClass::ClassIdC( 0, 10003 );


	//! OpenGL graphics buffer class.
	/*!	To get the dimensions of the graphics buffer, query the GraphicsViewport class from it.
	*/
	class OpenGLBufferC : public GraphicsBufferI
	{
	public:
		//! Create new graphics device.
		virtual Edit::DataBlockI*				create();
		static OpenGLBufferC*					create_new( GraphicsDeviceI* pDevice );
		virtual PluginClass::ClassIdC			get_class_id() const;
		virtual const char*						get_class_name();
		virtual DeviceInterfaceI*				query_interface( const PluginClass::SuperClassIdC& rSuperClassId );

		//! Sets the owner of the buffer.
		virtual void							set_graphicsdevice( GraphicsDeviceI* pDevice );

		//! Intialises a created graphics buffer.
		/*!	If init() is called for already created buffer, the old contents is
			deleted and new buffer is initialised.
		*/
		virtual bool							init( PajaTypes::uint32 ui32Flags,
													  PajaTypes::uint32 ui32Width = 0,
													  PajaTypes::uint32 ui32Height = 0 );
		//! Returns initialisation flags.
		virtual PajaTypes::uint32		get_flags();
		//! Starts drawing block.
		virtual void							begin_draw();
		//! Ends drawing block.
		virtual void							end_draw();
		//! Flushes the rendering buffer.
		virtual void							flush();

		//! Returns the texture coordinates of the buffer.
		virtual PajaTypes::BBox2C&	get_tex_coord_bounds();

		//! Uses the graphics buffer as a texture in specified device.
		virtual void							bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Stage, PajaTypes::uint32 ui32Properties );

		//! Get contents of the graphics buffer.
		virtual void							read_pixels( PajaTypes::uint32 ui32Flags, void* pData );

		virtual void							activate();

	protected:
		OpenGLBufferC();
		OpenGLBufferC( GraphicsDeviceI* pDevice );
		virtual ~OpenGLBufferC();

		void		destroy();

		OpenGLDeviceC*		m_pParentDev;
		OpenGLViewportC*	m_pViewport;

		HPBUFFERARB			m_hPBuffer;
		HDC					m_hPBufferDC;
		HGLRC				m_hPBufferRC;
		PajaTypes::uint32	m_ui32InitFlags;

		bool							m_bRectangle;

		PajaTypes::BBox2C	m_rTexBounds;
		PajaTypes::uint32	m_ui32PBufferTexID;
		bool							m_bBindToRenderTexture;
	};

};	// namespace

#endif // __DEMOPAJA_GRAPHICSBUFFERI_H__
