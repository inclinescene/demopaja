#ifndef __OPENGLGUIDRAWINTERFACEC_H__
#define __OPENGLGUIDRAWINTERFACEC_H__

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <gl\gl.h>

// forw declaration
namespace PajaSystem {
	class OpenGLGUIDrawInterfaceC;
};

#include "PajaTypes.h"
#include "BBox2C.h"
#include "ColorC.h"
#include "Vector2C.h"
#include "DeviceInterfaceI.h"
#include "GUIDrawInterfaceI.h"
#include "OpenGLViewportC.h"
#include "OpenGLDeviceC.h"


namespace PajaSystem {

	//! The ID of the OpenGL interface, use this ID to request the interface.
	const PluginClass::ClassIdC	CLASS_OPENGL_GUIINTERFACE = PluginClass::ClassIdC( 0, 10002 );


	class OpenGLGUIDrawInterfaceC : public GUIDrawInterfaceI
	{
	public:

		virtual Edit::DataBlockI*				create();
		static OpenGLGUIDrawInterfaceC*			create_new( OpenGLDeviceC* pDevice );
		virtual PluginClass::ClassIdC			get_class_id() const;
		virtual const char*						get_class_name();

		// draws the layout
		virtual void	draw_layout( const PajaTypes::ColorC& rLayout );

		virtual void	begin_layout();
		virtual void	end_layout();

		// Uses specified font.
		virtual void	use_font( HFONT hFont );
		// Draws text using current font.
		virtual void	draw_text( const PajaTypes::Vector2C& rPos, const char* szText );

		// Sets current draw color.
		virtual void	set_color( const PajaTypes::ColorC& rColor );
		// Sets current point size.
		virtual void	set_point_size( PajaTypes::float32 f32Size );
		// Draws a line.
		virtual void	draw_line( const PajaTypes::Vector2C& rFrom, const PajaTypes::Vector2C& rTo );
		// Draws a line.
		virtual void	draw_point( const PajaTypes::Vector2C& rPos );
		// Draws grid to based on latest draw layout call and grid size.
		virtual void	draw_grid( PajaTypes::float32 f32Width, PajaTypes::float32 f32Height, PajaTypes::float32 f32GridSize );
		// Draws selection box.
		virtual void	draw_selection_box( const PajaTypes::Vector2C& rMin, const PajaTypes::Vector2C& rMax );
		// Draw a box.
		virtual void	draw_box( const PajaTypes::Vector2C& rMin, const PajaTypes::Vector2C& rMax );
		// Draws a marker.
		virtual void	draw_marker( const PajaTypes::Vector2C& rPos, PajaTypes::float32 f32Size );

	protected:
		OpenGLGUIDrawInterfaceC();
		OpenGLGUIDrawInterfaceC( OpenGLDeviceC* pDevice );
		virtual ~OpenGLGUIDrawInterfaceC();

	private:
		OpenGLDeviceC*		m_pDevice;
		DWORD				m_dwFontListStart;

	};

};	// namespace

#endif