//-------------------------------------------------------------------------
//
// File:		DemopajaVersion.h
// Desc:		SDK version information.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_DEMOPAJAVERSION_H__
#define __DEMOPAJA_DEMOPAJAVERSION_H__

#include "PajaTypes.h"

// Version Information
const PajaTypes::uint32		DEMOPAJA_VER_MAJOR = 0;
const PajaTypes::uint32		DEMOPAJA_VER_MINOR = 7;
const PajaTypes::uint32		DEMOPAJA_VERSION = (DEMOPAJA_VER_MAJOR << 16 | DEMOPAJA_VER_MINOR);

#endif	// __DEMOPAJA_DEMOPAJAVERSION_H__