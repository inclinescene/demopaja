//-------------------------------------------------------------------------
//
// File:		DataBlockI.h
// Desc:		Plugin data interface.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_DATABLOCKI_H__
#define __DEMOPAJA_DATABLOCKI_H__


namespace Edit {

	//! Plugin data interface.
	/*! Data block class is base class for every object created in a plugin class in
		a DLL and returned to the Demopaja system. Data block class takes care that
		the memory management of the main application and the plugin won't corrupt.
		This class also hides the constructor and destructor to prevent the misuse
		of the classes derived from this class. 
	*/
	class DataBlockI
	{
	public:
		//! Creates new datablock.
		/*!	The create method of data block class creates new instance of the same class the data block is. 

			Example implementation:
			\code
			DataBlockI*
			TGAImportC::create()
			{
				return new TGAImportC;
			}
			\endcode

			\return Pointer to a new instance of this class. 
		*/
		virtual DataBlockI*			create() = 0;

		//! Deletes data block.
		/*!	This method should delete all the contents of this class.
			The default implementation is shown below. So in practice,
			you can implement the desctructor method to release the
			data as usual. 

			The default implementation of this method is: 

			\code
			void
			DataBlockI::release()
			{
				delete this;
			}
			\endcode
		*/
		virtual void				release();

		//! Sets flag indicating wheter the datablock is alive or dead.
		/*!	This flag indicates, if the editable is alive or dead.
			If the editable is "dead", it still exists in the memory, but
			it's been deleted and hangs somewhere in the undostack.
			If your operation has to be only done to the "living" datablocks,
			check this flag.
		*/
		virtual void				set_alive( bool bState );
		//! Gets the alive flag, \see set_alive().
		virtual bool				get_alive() const;

	protected:
		//! Protected default constructor.
		DataBlockI();
		//! Protected default desctructor.
		virtual ~DataBlockI();

		bool		m_bAlive;
	};

};	// namespace

#endif // __DEMOPAJA_DATABLOCKI_H__
