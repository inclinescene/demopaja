//-------------------------------------------------------------------------
//
// File:		CommonDialogI.h
// Desc:		Interface for common dialogs.
// Author:		memon <memon@inside.org>
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2004 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK. For conditions of 
//  distribution and use, see the accompanying license.txt file.
//  http://www.demopaja.org
//-------------------------------------------------------------------------

#ifndef __DEMOPAJA_COMMONDIALOGI_H__
#define __DEMOPAJA_COMMONDIALOGI_H__

#include "PajaTypes.h"
#include "ClassIdC.h"
#include "DataBlockI.h"

namespace PajaSystem {

	//! Common Dialog Base Class
	/*! The DemoInterfaceC provides support for some common dialogs
		inside the Demopaja editor. One such dialog is the color choose
		dialog which is widely used in the editor. See derived classes
		for more information on each dialog.
	*/

	class CommonDialogI : public Edit::DataBlockI
	{
	public:

		//! Returns the class ID of the common dialog.
		virtual PluginClass::ClassIdC		get_class_id() = 0;
		//! Runs the dialog in modal mode (returns after completed).
		virtual bool						do_modal() = 0;

	protected:
		CommonDialogI();
		virtual ~CommonDialogI();

	};

};

#endif // __DEMOPAJA_DemoInterfaceC_H__
