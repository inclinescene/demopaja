//
// SpinnerButton.h : header file
//
/////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2000 Mikko Mononen <memon@inside.org>
// Feel free to use this file 
//
// This file implements a spinner button control similar
// to the one in 3D Studio MAX. Spinner can automatically
// update it's value to any control (buddy). Output value
// can be integer or double. Internally value is handled in
// floating-point.User can use two arrow buttons to increment
// and decrement the value or use mouse wheel or adjust the
// value by pressing any of the arrow buttons and moving
// the mouse. CTRL-key combined with any operation scales
// the increment by factor of 10. Value clampping can be
// enabed or disabled and minumum and maximum values can be set.
//

#ifndef __DEMOPAJA_SPINNERBTNC_H__
#define __DEMOPAJA_SPINNERBTNC_H__

#include <windows.h>


// Creation flags
#define		SPNB_SETBUDDYINT	0x00000001
#define		SPNB_SETBUDDYFLOAT	0x00000002
#define		SPNB_USELIMITS		0x00000004
#define		SPNB_ALL			0x0000000f

// Flags for set buddy
#define		SPNB_ATTACH_LEFT	0x0001
#define		SPNB_ATTACH_RIGHT	0x0002


class SpinnerBtnC
{
public:
	// Construction
	SpinnerBtnC();
	virtual ~SpinnerBtnC();

	// Operations
	BOOL	Create( HINSTANCE hInstance, DWORD dwStyle, const RECT& rect, HWND hParentWnd, UINT nID );

	// Attributes
	HWND	SetBuddy( HWND hBuddy, DWORD nFlags = 0 );
	HWND	GetBuddy() const;
	void	SetScale( double scale );
	void	SetMinMax( double min, double max );
	double	GetMin() const;
	double	GetMax() const;
	void	SetVal( double val );
	double	GetVal() const;
	void	SetMultiplier( double val );
	double	GetMultiplier() const;


	void	OnPaint( HWND hWnd, HDC hDC );
	void	OnLButtonDown( UINT nFlags, POINT point );
	void	OnLButtonUp( UINT nFlags, POINT point );
	void	OnMouseMove( UINT nFlags, POINT point );
	void	OnSize( UINT nType, int cx, int cy );
	void	OnTimer( UINT nIDEvent );

protected:
	void	CreateObjects();
	void	DeleteObjects();
	bool	RegisterClass( HINSTANCE hInstance );
	void	UpdateBuddy( BOOL save );
	void	MakeFormatString();

	static bool        m_bClassRegistered;

	HWND	m_hWnd;
	HWND	m_hWndBuddy;
	RECT	m_rUpRect;
	RECT	m_rDownRect;
	UINT	m_nArrowSize;
	BOOL	m_bUp;
	BOOL	m_bDown;
	UINT	m_nTimer;
	BOOL	m_bAdjusting;
	HCURSOR	m_hOldCursor;
	double	m_dStarty;
	UINT	m_nFlags;
	double	m_dStartValue;
	double	m_dValue;
	double	m_dScale;
	double	m_dMultiplier;
	double	m_dMin, m_dMax;
	BOOL	m_bNegScale;
	UINT	m_nTimerCount;
	char	m_szFormatStr[32];

	HPEN	m_hBlackPen;
	HBRUSH	m_hBlackBrush;

};


#endif // __DEMOPAJA_SPINNERBTNC_H__
