//
// FlareImagePlugin.h
//
// Flare Image Plugin
// This example plugin demostrates the procedural data editing and
// file referencing.
//
// Copyright (c) 2000 - 2002 memon/moppi productions
//

#ifndef __FLAREIMAGEPLUGIN_H__
#define __FLAREIMAGEPLUGIN_H__


#include "DemopajaVersion.h"
#include "PajaTypes.h"
#include "EffectI.h"
#include "EditableI.h"
#include "UndoC.h"
#include "ParamI.h"
#include "BBox2C.h"
#include "Matrix2C.h"
#include "DeviceContextC.h"
#include "DeviceInterfaceI.h"
#include "DemoInterfaceC.h"
#include "OpenGLViewportC.h"
#include "OpenGLDeviceC.h"
#include "TimeContextC.h"
#include "ImportableImageI.h"

//////////////////////////////////////////////////////////////////////////
//
//  Class IDs
//

const PluginClass::ClassIdC	CLASS_FLAREIMAGE_IMPORT( 0x816635E2, 0xDD3C434C );


//////////////////////////////////////////////////////////////////////////
//
//  AVI importer class descriptor.
//

class FlareImageImportDescC : public PluginClass::ClassDescC
{
public:
	FlareImageImportDescC();
	virtual ~FlareImageImportDescC();
	virtual void*						create();
	virtual PajaTypes::int32			get_classtype() const;
	virtual PluginClass::SuperClassIdC	get_super_class_id() const;
	virtual PluginClass::ClassIdC		get_class_id() const;
	virtual const char*					get_name() const;
	virtual const char*					get_desc() const;
	virtual const char*					get_author_name() const;
	virtual const char*					get_copyright_message() const;
	virtual const char*					get_url() const;
	virtual const char*					get_help_filename() const;
	virtual PajaTypes::uint32			get_required_device_driver_count() const;
	virtual const PluginClass::ClassIdC&	get_required_device_driver( PajaTypes::uint32 ui32Idx );
	virtual PajaTypes::uint32			get_ext_count() const;
	virtual const char*					get_ext( PajaTypes::uint32 ui32Index ) const;
};




namespace FlareImagePlugin {

//////////////////////////////////////////////////////////////////////////
//
// Default effect
//

	const	PluginClass::ClassIdC	CLASS_IMAGE_EFFECT( 0, 100 );

//////////////////////////////////////////////////////////////////////////
//
// Flare Image Proc Importer class.
//

	class FlareImageImportC : public Import::ImportableImageI
	{
	public:
		static FlareImageImportC*		create_new();
		virtual Edit::DataBlockI*		create();
		virtual Edit::DataBlockI*		create( Edit::EditableI* pOriginal );
		virtual void					copy( Edit::EditableI* pEditable );
		virtual void					restore( Edit::EditableI* pEditable );

		virtual const char*				get_filename();
		virtual void						set_filename( const char* szName );
		virtual bool					create_file( PajaSystem::DemoInterfaceC* pInterface );
		virtual bool					prompt_properties();
		virtual bool					has_properties();

		virtual void					initialize( PajaTypes::uint32 ui32Reason, PajaSystem::DemoInterfaceC* pInterface );

		virtual PajaTypes::uint32	update_notify( EditableI* pCaller );

		virtual PajaTypes::uint32		get_reference_file_count();
		virtual Import::FileHandleC*	get_reference_file( PajaTypes::uint32 ui32Index );

		virtual PluginClass::ClassIdC	get_class_id();
		virtual const char*				get_class_name();

		// The importable image interface.
		virtual PajaTypes::float32		get_width();
		virtual PajaTypes::float32		get_height();
		virtual PajaTypes::int32		get_data_width();
		virtual PajaTypes::int32		get_data_height();
		virtual PajaTypes::int32		get_data_pitch();
		virtual PajaTypes::int32		get_data_bpp();
		virtual PajaTypes::uint8*		get_data();

		virtual void					bind_texture( PajaSystem::DeviceInterfaceI* pInterface, PajaTypes::uint32 ui32Properties );
		virtual const char*				get_info();
		virtual PluginClass::ClassIdC	get_default_effect();

		virtual void					eval_state( PajaTypes::int32 i32Time );

		virtual bool					equals( Import::ImportableI* pImp );

		virtual PajaTypes::int32		get_duration();
		virtual PajaTypes::float32		get_start_label();
		virtual PajaTypes::float32		get_end_label();

		virtual PajaTypes::uint32		save( FileIO::SaveC* pSave );
		virtual PajaTypes::uint32		load( FileIO::LoadC* pLoad );

	protected:
		FlareImageImportC();
		FlareImageImportC( Edit::EditableI* pOriginal );
		virtual ~FlareImageImportC();

	private:

		void					invalidate_data();

		PajaTypes::uint32		m_ui32TextureId;
		std::string				m_sFileName;

		PajaTypes::uint32		m_ui32Width;
		PajaTypes::uint32		m_ui32Height;
		PajaTypes::ColorC		m_rGlowColor;
		PajaTypes::float32		m_f32GlowSize;
		PajaTypes::ColorC		m_rBGColor;
		PajaTypes::uint8*		m_pData;
		Import::FileHandleC*	m_pImgHandle;
		bool					m_bUpdateTexture;

	};

};	// namespace


extern FlareImageImportDescC	g_rFlareImportDesc;


#endif	// __FLAREIMAGEPLUGIN_H__
