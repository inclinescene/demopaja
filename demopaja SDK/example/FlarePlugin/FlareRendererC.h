#ifndef __FLARERENDERERC_H__
#define __FLARERENDERERC_H__

#include "PajaTypes.h"
#include "ColorC.h"
#include "ImportableImageI.h"

class FlareRendererC
{
public:
	FlareRendererC();
	~FlareRendererC();

	static void		render_flare( PajaTypes::uint8* pBuffer, PajaTypes::uint32 ui32Width, PajaTypes::uint32 ui32Height,
								  const PajaTypes::ColorC& rBGColor, const PajaTypes::ColorC& rGlowColor, PajaTypes::float32 f32Size, Import::ImportableImageI* pImp, bool bUseTransparency );
};


#endif