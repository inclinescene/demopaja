#ifndef __FLAREIMAGEDIALOG_H__
#define __FLAREIMAGEDIALOG_H__

#include "PajaTypes.h"
#include "ColorC.h"
#include "DemoInterfaceC.h"
#include "ImportableImageI.h"
#include "FileHandleC.h"
#include "SpinnerBtnC.h"
#include <string>

//
// Helper class to display the image
//
class DIBitmapC
{
public:
	DIBitmapC ();
	virtual ~DIBitmapC ();
	bool				create( PajaTypes::int32 i32Width, PajaTypes::int32 i32Height );
	void				destroy();
	void				paste_to_DC( HDC hDC, PajaTypes::int32 i32X, PajaTypes::int32 i32Y );
	void				paste_to_DC( HDC hDC, PajaTypes::int32 i32X, PajaTypes::int32 i32Y, PajaTypes::int32 i32W , PajaTypes::int32 i32H );
	PajaTypes::int32	get_width() const;
	PajaTypes::int32	get_height() const;
	UINT*				get_bits();

protected:
	UINT*		m_pBits;
	BITMAPINFO	m_rInfo;
	HBITMAP		m_hBitmap;
	SIZE		m_rSize;
};



class FlareDialogC
{
public:

	FlareDialogC( PajaSystem::DemoInterfaceC* pInterface );
	virtual ~FlareDialogC();

	void						set_bg_color( const PajaTypes::ColorC& rColor );
	const PajaTypes::ColorC&	get_bg_color() const;

	void						set_glow_color( const PajaTypes::ColorC& rColor );
	const PajaTypes::ColorC&	get_glow_color() const;

	void						set_name( const char* szName );
	const char*					get_name() const;

	void						set_glow_size( PajaTypes::float32 f32Size );
	PajaTypes::float32			get_glow_size() const;

	void						set_width( PajaTypes::uint32 ui32Width );
	PajaTypes::uint32			get_width() const;

	void						set_height( PajaTypes::uint32 ui32Height );
	PajaTypes::uint32			get_height() const;

	void						set_handle( Import::FileHandleC* pHandle );
	Import::FileHandleC*		get_handle() const;

	void						set_owner_importable( Import::ImportableImageI* pImp );

	bool						do_modal();

protected:

	static LRESULT CALLBACK		dlg_proc( HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam );
	void						update_flare();
	void						update_scroll_range();

	void						on_lbutton_down( POINT pt );
	void						on_lbutton_up( POINT pt );
	void						on_mouse_move( POINT pt );
	BOOL						on_command( HWND hCtl, PajaTypes::uint32 ui32Cmd, PajaTypes::uint32 ui32Notify );
	BOOL						on_draw_item( LPDRAWITEMSTRUCT lpdis );
	void						on_paint( HDC hDC );
	BOOL						on_init_dialog();
	BOOL						on_set_cursor( POINT pt );

	void						update_alpha( PajaTypes::int32 i32ID, PajaTypes::float32 f32Alpha );

	void						set_warning( const char* szMsg );
	void						check_file( Import::FileHandleC* );

	void						invalidate_view();
	void						invalidate();

	PajaSystem::DemoInterfaceC*	m_pInterface;

	PajaTypes::ColorC			m_rBGColor;
	PajaTypes::ColorC			m_rGlowColor;
	PajaTypes::uint32			m_ui32Width;
	PajaTypes::uint32			m_ui32Height;
	PajaTypes::float32			m_f32Size;
	Import::FileHandleC*		m_pHandle;
	Import::ImportableImageI*	m_pOwnerImp;
	std::string					m_sName;
	DIBitmapC					m_rDIB;
	SpinnerBtnC					m_rSpinnerSize;

	HWND						m_hDlg;
	RECT						m_rViewRect;
	bool						m_bPanorate;
	PajaTypes::int32			m_i32Zoom;
	PajaTypes::int32			m_i32OffsetX;
	PajaTypes::int32			m_i32OffsetY;
	PajaTypes::int32			m_i32OffsetXMax;
	PajaTypes::int32			m_i32OffsetYMax;
	PajaTypes::int32			m_i32StartOffsetX;
	PajaTypes::int32			m_i32StartOffsetY;

	PajaTypes::int32			m_i32StartX;
	PajaTypes::int32			m_i32StartY;
};


#endif	// __FLAREIMAGEDIALOG_H__