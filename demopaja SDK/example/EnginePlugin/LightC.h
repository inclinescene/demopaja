#ifndef LIGHTC_H
#define LIGHTC_H

#include "PajaTypes.h"
#include "Vector3C.h"
#include "ControllerC.h"
#include "ContFloatC.h"
#include "ContVector3C.h"
#include "FileIO.h"
#include "ScenegraphItemI.h"


const PajaTypes::int32	CGITEM_LIGHT = 0x3000;


class LightC : public ScenegraphItemI
{
public:
	LightC();
	virtual ~LightC();

	virtual PajaTypes::int32			get_type();

	virtual void						set_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_position();

	virtual void						set_color( const PajaTypes::ColorC& rVal );
	virtual const PajaTypes::ColorC&	get_color();

	virtual void						set_multiplier( PajaTypes::float32 f32Val );
	virtual PajaTypes::float32			get_multiplier();

	virtual void						eval_state( PajaTypes::int32 i32Time );

	virtual void						set_position_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_position_controller();

	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

private:
	PajaTypes::Vector3C	m_rPos;
	PajaTypes::ColorC	m_rColor;
	PajaTypes::float32	m_f32Multiplier;

	ContVector3C*		m_pPosCont;
};


#endif // LIGHT_H