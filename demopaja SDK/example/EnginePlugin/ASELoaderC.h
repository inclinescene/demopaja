//
// ASCII scene .ASE loader
//


#ifndef __ASELOADERC_H__
#define __ASELOADERC_H__

#include "PajaTypes.h"
#include "ColorC.h"
#include "DemoInterfaceC.h"
#include "ImportableImageI.h"
#include "ScenegraphItemI.h"
#include "ContVector3C.h"
#include "ContQuatC.h"
#include "ContFloatC.h"
#include "CameraC.h"
#include "LightC.h"
#include "MeshC.h"

// stl
#include <vector>
#include <string>



class ASELoaderC
{
public:
	ASELoaderC();
	virtual ~ASELoaderC();

	bool				load( const char* szName, PajaSystem::DemoInterfaceC* pInterface );

	void				get_scenegraph( std::vector<MeshC*>& rScenegraph );
	void				get_lights( std::vector<LightC*>& rLights );
	void				get_cameras( std::vector<CameraC*>& rCameras );

	void				get_time_parameters( PajaTypes::int32& i32FPS, PajaTypes::int32& i32TicksPerFrame, PajaTypes::int32& i32FirstFrame, PajaTypes::int32& i32LastFrame );


private:

	struct ASEMaterialS
	{
		std::string				m_sTextureName;
		PajaTypes::ColorC		m_rAmbient;
		PajaTypes::ColorC		m_rDiffuse;
		PajaTypes::ColorC		m_rSpecular;
		PajaTypes::float32		m_f32Shininess;
	};

	void					read_row();
	char*					get_row();
	void					remove_nl( char* buf );
	char*					extract_string( char* szBuf );
	bool					is_block();
	bool					is_token( const char* token );
	bool					eof();

	Import::FileHandleC*	get_texture( std::string& name );
	MeshC*					find_object( const std::string& sName, MeshC* pParent = 0 );

	void					parse_scene();

	void					parse_map( std::string& sName );
	void					parse_materials();
	void					parse_material( ASEMaterialS* pMtl );

	void					parse_pos_track( ContVector3C* pCont );
	void					parse_sampled_pos_track( ContVector3C* pCont );
	void					parse_vector3_track( ContVector3C* pCont );
	void					parse_rot_track( ContQuatC* pCont );
	void					parse_sampled_rot_track( ContQuatC* pCont );
	void					parse_scale_track( ContVector3C* pContScale, ContQuatC* pContRot );
	void					parse_sampled_scale_track( ContVector3C* pContScale, ContQuatC* pContRot );
	void					parse_float_track( ContFloatC* pCont );

	void					parse_mesh_node_tm( MeshC* pMesh );

	void					fix_tm( MeshC* pMesh, MeshC* pParent );
	void					add_scenegraphitem( MeshC* pMesh, MeshC* pCurGroup );

	MeshC*					parse_geomobject();
	MeshC*					parse_helperobject( std::string& sGroupName, bool& bIsGroup );
	MeshC*					parse_group( std::string& sGroupName, PajaTypes::int32 i32Indent );
	PajaTypes::uint32		parse_dummy();
	void					parse_mesh( MeshC* pMesh );

	bool					parse_look_at_node_tm( PajaTypes::Vector3C& rPos );

	CameraC*				parse_camera();
	void					parse_camera_tm_anim( CameraC* pCam );
	void					parse_camera_settings( CameraC* pCam );

	void					parse_object_tm_anim( MeshC* pObj );
	void					parse_object_vis_track( MeshC* pObj );

	void					parse_light_settings( LightC* pLight );
	void					parse_light_tm_anim( LightC* pLight );
	LightC*					parse_light();


	FILE*							m_pStream;
	char							m_szRow[1024];
	char							m_szWord[256];

	PajaTypes::uint32				m_ui32Version;
	std::vector<ASEMaterialS>		m_rMaterials;

	std::vector<CameraC*>			m_rCameras;
	std::vector<LightC*>			m_rLights;
	std::vector<MeshC*>				m_rScenegraph;

	PajaSystem::DemoInterfaceC*	m_pInterface;

	PajaTypes::int32				m_i32FPS;
	PajaTypes::int32				m_i32TicksPerFrame;
	PajaTypes::int32				m_i32FirstFrame;
	PajaTypes::int32				m_i32LastFrame;
};


#endif
