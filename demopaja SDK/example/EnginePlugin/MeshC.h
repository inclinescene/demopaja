#ifndef MESHREFC_H
#define MESHREFC_H

#include "PajaTypes.h"
#include "ColorC.h"
#include "ImportableI.h"
#include "FileIO.h"
#include "ScenegraphItemI.h"
#include "QuatC.h"
#include "Vector2C.h"
#include "Vector3C.h"
#include "ContVector3C.h"
#include "ContQuatC.h"
#include "ControllerC.h"


const PajaTypes::int32	CGITEM_MESH = 0x1000;


class MeshC : public ScenegraphItemI
{
public:
	MeshC();
	virtual ~MeshC();

	virtual PajaTypes::int32			get_type();

	// transformation

	virtual void						eval_state( PajaTypes::int32 i32Time );

	virtual void						set_position( const PajaTypes::Vector3C& rPos );
	virtual const PajaTypes::Vector3C&	get_position();

	virtual void						set_scale( const PajaTypes::Vector3C& rScale );
	virtual const PajaTypes::Vector3C&	get_scale();

	virtual void						set_scale_rotation( const PajaTypes::QuatC& rRot );
	virtual const PajaTypes::QuatC&		get_scale_rotation();

	virtual void						set_rotation( const PajaTypes::QuatC& rRot );
	virtual const PajaTypes::QuatC&		get_rotation();

	virtual void						set_position_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_position_controller();

	virtual void						set_rotation_controller( ContQuatC* pCont );
	virtual ContQuatC*					get_rotation_controller();

	virtual void						set_scale_controller( ContVector3C* pCont );
	virtual ContVector3C*				get_scale_controller();

	virtual void						set_scale_rotation_controller( ContQuatC* pCont );
	virtual ContQuatC*					get_scale_rotation_controller();

	// geometry

	virtual void						add_index( PajaTypes::uint32 ui32Index );		
	virtual void						add_vert( const PajaTypes::Vector3C& rVec );
	virtual void						add_norm( const PajaTypes::Vector3C& rNorm );
	virtual void						add_texcoord_index( PajaTypes::uint32 ui32Index );		
	virtual void						add_texcoord( const PajaTypes::Vector2C& rTex );

	virtual PajaTypes::uint32			get_index_count() const;
	virtual PajaTypes::uint32			get_texcoord_index_count() const;
	virtual PajaTypes::uint32			get_vert_count() const;
	virtual PajaTypes::uint32			get_norm_count() const;
	virtual PajaTypes::uint32			get_texcoord_count() const;

	virtual PajaTypes::uint32			get_index( PajaTypes::uint32 ui32Index );
	virtual PajaTypes::uint32			get_texcoord_index( PajaTypes::uint32 ui32Index );
	virtual const PajaTypes::Vector3C&	get_vert( PajaTypes::uint32 ui32Index );
	virtual const PajaTypes::Vector3C&	get_norm( PajaTypes::uint32 ui32Index );
	virtual const PajaTypes::Vector2C&	get_texcoord( PajaTypes::uint32 ui32Index );

	// material
	virtual void						set_texture( Import::FileHandleC* pHandle );
	virtual Import::FileHandleC*		get_texture() const;

	virtual void						set_ambient( const PajaTypes::ColorC &rAmbient );
	virtual void						set_diffuse( const PajaTypes::ColorC &rDiffuse );
	virtual void						set_specular( const PajaTypes::ColorC &rSpecular );
	virtual void						set_shininess( const PajaTypes::float32 f32Shininess );

	virtual const PajaTypes::ColorC&	get_ambient() const;
	virtual const PajaTypes::ColorC&	get_diffuse() const;
	virtual const PajaTypes::ColorC&	get_specular() const;
	virtual PajaTypes::float32			get_shininess();

	// serialize
	virtual PajaTypes::uint32			save( FileIO::SaveC* pSave );
	virtual PajaTypes::uint32			load( FileIO::LoadC* pLoad );

private:

	PajaTypes::Vector3C			m_rPosition;
	PajaTypes::Vector3C			m_rScale;
	PajaTypes::QuatC			m_rScaleRot;
	PajaTypes::QuatC			m_rRotation;

	ContVector3C*				m_pPosCont;
	ContQuatC*					m_pRotCont;
	ContVector3C*				m_pScaleCont;
	ContQuatC*					m_pScaleRotCont;

	// Material
	PajaTypes::ColorC			m_rAmbient;
	PajaTypes::ColorC			m_rDiffuse;
	PajaTypes::ColorC			m_rSpecular;
	PajaTypes::float32			m_f32Shininess;
	Import::FileHandleC*		m_pTextureHandle;

	//Geometry
	std::vector<PajaTypes::uint32>		m_rIndices;
	std::vector<PajaTypes::uint32>		m_rTexIndices;
	std::vector<PajaTypes::Vector3C>	m_rVertices;
	std::vector<PajaTypes::Vector3C>	m_rNormals;
	std::vector<PajaTypes::Vector2C>	m_rTexCoords;
};


#endif // MESHREFC_H