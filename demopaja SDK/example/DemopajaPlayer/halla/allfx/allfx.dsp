# Microsoft Developer Studio Project File - Name="allfx" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=allfx - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "allfx.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "allfx.mak" CFG="allfx - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "allfx - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "allfx - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "allfx - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I "..\..\..\include\opengldriver" /I "..\..\..\include" /I "F:\Code and Docs\Libraries\fftw-2.1.3\fftw" /I "F:\Code and Docs\Libraries\fftw-2.1.3\rfftw" /I "F:\Code and Docs\Libraries\zlib113" /I "F:\Code and Docs\Libraries\ijl11\include" /I "F:\Code and Docs\Libraries\minilzo.106" /I "F:\Code Projects\DemoPlugins\pngimp\pnglib" /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /D "PAJAPLAYER" /D "DEMOPAJAPLAYER" /YX /FD /c
# ADD BASE RSC /l 0x40b /d "NDEBUG"
# ADD RSC /l 0x40b /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "allfx - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ  /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ  /c
# ADD BASE RSC /l 0x40b /d "_DEBUG"
# ADD RSC /l 0x40b /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "allfx - Win32 Release"
# Name "allfx - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\BlurPlugin\BlurPlugin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\BoneC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\CameraC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ContBoolC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ContFloatC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ContQuatC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ContVector3C.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\extgl.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\flareplugin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\Fluid\FluidPlugin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\Fluid\FluidSolverC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\glmatrix.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\ImageWarp\ImageWarpPlugin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\jpegimp\jpegimp.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\LightC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\MASLoaderC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\MasPlugin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\MaterialC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\MeshC.cpp
# End Source File
# Begin Source File

SOURCE="..\..\..\..\..\Code and Docs\Libraries\minilzo.106\minilzo.c"
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ParticleSystemC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\PBufferC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\perlin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\png.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngerror.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pnggccrd.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngget.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pngimp.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngmem.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngpread.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngread.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngrio.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngrtran.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngrutil.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngset.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngtest.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngtrans.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngvcrd.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngwio.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngwrite.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngwtran.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngwutil.c
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ScenegraphItemI.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ShapeC.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\TestPlugin\TestPlugin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\WSMObjectC.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\BlurPlugin\BlurPlugin.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\BoneC.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\CameraC.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ContBoolC.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ContFloatC.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ContQuatC.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ContVector3C.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\extgl.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\Fluid\FluidPlugin.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\Fluid\FluidSolverC.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\BlurPlugin\Glext.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\Glext.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\TestPlugin\glext.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\glmatrix.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\ImageWarp\ImageWarpPlugin.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\jpegimp\jpegimp.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\LightC.h
# End Source File
# Begin Source File

SOURCE="..\..\..\..\..\Code and Docs\Libraries\minilzo.106\lzoconf.h"
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\MASLoaderC.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\MasPlugin.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\MaterialC.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\MeshC.h
# End Source File
# Begin Source File

SOURCE="..\..\..\..\..\Code and Docs\Libraries\minilzo.106\minilzo.h"
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\noise.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ParticleSystemC.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\PBufferC.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\png.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngasmrd.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pnglib\pngconf.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\pngimp\pngimp.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\resource.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ScenegraphItemI.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\ShapeC.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\TestPlugin\TestPlugin.h
# End Source File
# Begin Source File

SOURCE=..\..\..\..\DemoPlugins\MasPlugin\WSMObjectC.h
# End Source File
# End Group
# Begin Source File

SOURCE="..\..\..\..\..\Code and Docs\Libraries\ijl11\lib\msvc\ijl11.lib"
# End Source File
# End Target
# End Project
