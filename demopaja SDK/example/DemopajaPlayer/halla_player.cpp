//-------------------------------------------------------------------------
//
// File:		PajaPlayer.cpp
// Purpose:		Moppi Demopaja Player example
// Author:		memon <memon@inside.org>
// Version:		0.6
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK.
//  For conditions of distribution and use, see the accompanying license.txt file.
//  http://moppi.inside.org/demopaja/
//-------------------------------------------------------------------------

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include "res/resource.h"
#include "DemopajaPlayerC.h"
#include "FileIO.h"

#include "unzip.h"

#include "OpenGLDriver\OpenGlDriver.h"


#include "BlurPlugin\BlurPlugin.h"
#include "ImageWarp\ImageWarpPlugin.h"
#include "jpegimp\jpegimp.h"
#include "pngimp\pngimp.h"
#include "MasPlugin\MasPlugin.h"
#include "TestPlugin\TestPlugin.h"
#include "Fluid\FluidPlugin.h"




using namespace PajaTypes;
using namespace FileIO;


// Globals.
bool		g_bFullscreen = false;
int32		g_i32Resolution = 2;


// Init dialog proc.
BOOL CALLBACK
InitDlgProc( HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	switch( uMsg ) {

	case WM_INITDIALOG:
		{
			// set fullscreen as checked
			::CheckDlgButton( hwndDlg, IDC_CHECK, BST_CHECKED );

			// resolution display
			char	szItems[8][64] = {
					"400 x 300",
					"512 x 384",
					"640 x 480",
					"800 x 600",
					"1024 x 768",
					"1152 x 864",
					"1280 x 960",
					"1600 x 1200" };
			for( int32 i = 0; i < 8; i++ )
				SendDlgItemMessage( hwndDlg, IDC_COMBORESOLUTION, CB_INSERTSTRING, i, (LPARAM)(LPCTSTR)szItems[i] );

			// set default resolution
			SendDlgItemMessage( hwndDlg, IDC_COMBORESOLUTION, CB_SETCURSEL, g_i32Resolution, 0 );


			if( g_bFullscreen )
				EnableWindow( GetDlgItem( hwndDlg, IDC_COMBORESOLUTION ), FALSE );

			return TRUE;
		}
		break;

	case WM_COMMAND:
		{
			WORD	wNotifyCode = HIWORD(wParam);	// notification code 
			WORD	wID = LOWORD(wParam);			// item, control, or accelerator identifier 
			HWND	hwndCtl = (HWND)lParam;			// handle of control 

			if( wNotifyCode == BN_CLICKED ) {
				if( wID == IDC_CHECK ) {
					if( IsDlgButtonChecked( hwndDlg, IDC_CHECK ) == BST_CHECKED )
						EnableWindow( GetDlgItem( hwndDlg, IDC_COMBORESOLUTION ), TRUE );
					else
						EnableWindow( GetDlgItem( hwndDlg, IDC_COMBORESOLUTION ), FALSE );

				}
				else if( wID == IDOK ) {
					if( IsDlgButtonChecked( hwndDlg, IDC_CHECK ) == BST_CHECKED )
						g_bFullscreen = true;
					else
						g_bFullscreen = false;

					g_i32Resolution = SendDlgItemMessage( hwndDlg, IDC_COMBORESOLUTION, CB_GETCURSEL, 0, 0 );

					EndDialog( hwndDlg, IDOK );
				}
				else if( wID == IDCANCEL )
					EndDialog( hwndDlg, IDCANCEL );
			}
		}

	default:
		return FALSE;
		break;
	}
}
 


static
void
TRACE( const char* szFormat, ...  )
{
	char	szMsg[256];
	va_list	rList;
	va_start( rList, szFormat );
	_vsnprintf( szMsg, 255, szFormat, rList );
	va_end( rList );
	OutputDebugString( szMsg );
}

/*
void	load_callback( float32 f32Percent )
{
	TRACE( "load: %d %%\n", (int32)(f32Percent * 100.0f) );
}
*/




PajaTypes::uint32				g_ui32DataSize;

PajaTypes::uint8*				g_pData;


bool
open_zip( const char* szFilename, const char* szInZipFile )
{
	char			filename_inzip[256];
	int				err = UNZ_OK;
	FILE*			fout = NULL;
//	unsigned char*	buf;
	unzFile			uf = NULL;
    
	uf = unzOpen( szFilename );
	
	if( unzLocateFile( uf, szInZipFile, 0 ) != UNZ_OK ) {
//		printf("file %s not found in the zipfile\n",filename);
		return false;
	}
	
	unz_file_info	file_info;
	uLong			ratio = 0;
	err = unzGetCurrentFileInfo( uf, &file_info, filename_inzip, sizeof( filename_inzip ), NULL, 0, NULL, 0 );
	
	if( err != UNZ_OK ) {
		TRACE("error %d with zipfile in unzGetCurrentFileInfo\n",err);
		return false;
	}
	
	
	err = unzOpenCurrentFile( uf );
	if( err != UNZ_OK ) {
		TRACE("error %d with zipfile in unzOpenCurrentFile\n",err);
		return false;
	}

	g_ui32DataSize = file_info.uncompressed_size;
	g_pData = new uint8[g_ui32DataSize];

//	*size = file_info.uncompressed_size;
//	buf = new unsigned char[file_info.uncompressed_size];
	
	unsigned int	count = 0;
	err = 1;
	while( err > 0 ) {
		err = unzReadCurrentFile( uf, &g_pData[count], 65535 );
		if( err < 0 ) {
			TRACE("error %d with zipfile in unzReadCurrentFile\n",err);
			break;
		}
		else
			count += err;
	}
//	assert( count == file_info.uncompressed_size );
	if( err == UNZ_OK ) {
		err = unzCloseCurrentFile( uf );
		if( err != UNZ_OK )
		{
			TRACE("error %d with zipfile in unzCloseCurrentFile\n",err);
		}
	}
	else {
		g_ui32DataSize = 0;
		delete [] g_pData;
		g_pData = 0;
		unzCloseCurrentFile( uf ); /* don't lose the error */       
	}

	TRACE( "data size: %d\n", g_ui32DataSize );
	
	return true;
}



int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{

	// Show init dialog.
	if( DialogBox( hInstance, MAKEINTRESOURCE( IDD_INIT ), NULL, InitDlgProc ) != IDOK ) {
		return 0;
	}


	DemopajaPlayerC	rPlayer;


	// Init build-in plugins
	rPlayer.register_plugin( &g_rOpenGLDeviceDesc );

	rPlayer.register_plugin( &g_rBlurDesc );
	rPlayer.register_plugin( &g_rFluidDesc );
	rPlayer.register_plugin( &g_rWarpDesc );
	rPlayer.register_plugin( &g_rJPEGImportDesc );
	rPlayer.register_plugin( &g_rPNGImportDesc );

	rPlayer.register_plugin( &g_rEngineDesc );
	rPlayer.register_plugin( &g_rFlareDesc );
	rPlayer.register_plugin( &g_rMASImportDesc );

	rPlayer.register_plugin( &g_rTestDesc );
	rPlayer.register_plugin( &g_rTGAImportDesc );


	// Load plugins.
//	rPlayer.load_plugins( "plugins" );


	// Init demo system
	if( !rPlayer.init( hInstance, g_bFullscreen ) ) {
		OutputDebugString( "init failed\n" );
		return -1;
	}


	// Load the music from resource
/*	HRSRC	hMusicRes;
	HGLOBAL	hMusicMem;
	hMusicRes = FindResource( NULL, "music", "MP3" );
	hMusicMem = LoadResource( NULL, hMusicRes );
	uint8*	pMusicData = (uint8*)LockResource( hMusicMem );
	uint32	ui32MusicDataSize = SizeofResource( NULL, hMusicRes );

	rPlayer.set_music_data( pMusicData, ui32MusicDataSize );

	// Load demo from resource.
	HRSRC	hDemoRes;
	HGLOBAL	hDemoMem;
	hDemoRes = FindResource( NULL, "demo", "DMO" );
	hDemoMem = LoadResource( NULL, hDemoRes );
	uint8*	pDemoData = (uint8*)LockResource( hDemoMem );
	uint32	ui32DemoDataSize = SizeofResource( NULL, hDemoRes );*/

//	rPlayer.set_load_callback( load_callback );

//	if( rPlayer.load_demo( "demo.dat" ) != IO_OK ) {


	if( !open_zip( "demo.dat", "demo.dmo" ) ) {
		return -1;
	}

	switch( g_i32Resolution ) {
	case 0: rPlayer.set_fullscreen_resolution( 400, 300 ); break;
	case 1: rPlayer.set_fullscreen_resolution( 512, 384 ); break;
	case 2: rPlayer.set_fullscreen_resolution( 640, 480 ); break;
	case 3: rPlayer.set_fullscreen_resolution( 800, 600 ); break;
	case 4: rPlayer.set_fullscreen_resolution( 1024, 768 ); break;
	case 5: rPlayer.set_fullscreen_resolution( 1152, 864 ); break;
	case 6: rPlayer.set_fullscreen_resolution( 1280, 960 ); break;
	case 7: rPlayer.set_fullscreen_resolution( 1600, 1200 ); break;
	default: rPlayer.set_fullscreen_resolution( 640, 480 ); break;
	}

	if( rPlayer.load_demo( g_pData, g_ui32DataSize ) != IO_OK ) {
		OutputDebugString( "load failed\n" );
		return -1;
	}

	// hide cursor
	while( ShowCursor( FALSE ) > 0 ) {};

	//Run demo.
	int iRes = rPlayer.run();

	// show cursor
	while( ShowCursor( TRUE ) < 0 ) {};

	return iRes;
}



