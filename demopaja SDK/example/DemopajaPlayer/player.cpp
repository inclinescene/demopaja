//-------------------------------------------------------------------------
//
// File:		PajaPlayer.cpp
// Purpose:		Moppi Demopaja Player example
// Author:		memon <memon@inside.org>
// Version:		0.7
//
//-------------------------------------------------------------------------
//	Copyright (c) 2000-2002 Moppi Productions. All Rights Reserved.
//  This file is part of Moppi Demopaja SDK.
//  For conditions of distribution and use, see the accompanying license.txt file.
//  http://moppi.inside.org/demopaja/
//-------------------------------------------------------------------------

#define WIN32_LEAN_AND_MEAN		// Exclude rarely-used stuff from Windows headers
#include <windows.h>
#include "res/resource.h"
#include "DemopajaPlayerC.h"
#include "FileIO.h"

#include "OpenGLDriver\OpenGlDriver.h"


using namespace PajaTypes;
using namespace FileIO;


// Globals.
bool		g_bFullscreen = false;
int32		g_i32Resolution = 2;


// Init dialog proc.
BOOL CALLBACK
InitDlgProc( HWND hwndDlg, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	switch( uMsg ) {

	case WM_INITDIALOG:
		{
			// set fullscreen as checked
			::CheckDlgButton( hwndDlg, IDC_CHECK, BST_CHECKED );

			// resolution display
			char	szItems[8][64] = {
					"400 x 300",
					"512 x 384",
					"640 x 480",
					"800 x 600",
					"1024 x 768",
					"1152 x 864",
					"1280 x 960",
					"1600 x 1200" };
			for( int32 i = 0; i < 8; i++ )
				SendDlgItemMessage( hwndDlg, IDC_COMBORESOLUTION, CB_INSERTSTRING, i, (LPARAM)(LPCTSTR)szItems[i] );

			// set default resolution
			SendDlgItemMessage( hwndDlg, IDC_COMBORESOLUTION, CB_SETCURSEL, g_i32Resolution, 0 );


			if( g_bFullscreen )
				EnableWindow( GetDlgItem( hwndDlg, IDC_COMBORESOLUTION ), FALSE );

			return TRUE;
		}
		break;

	case WM_COMMAND:
		{
			WORD	wNotifyCode = HIWORD(wParam);	// notification code 
			WORD	wID = LOWORD(wParam);			// item, control, or accelerator identifier 
			HWND	hwndCtl = (HWND)lParam;			// handle of control 

			if( wNotifyCode == BN_CLICKED ) {
				if( wID == IDC_CHECK ) {
					if( IsDlgButtonChecked( hwndDlg, IDC_CHECK ) == BST_CHECKED )
						EnableWindow( GetDlgItem( hwndDlg, IDC_COMBORESOLUTION ), TRUE );
					else
						EnableWindow( GetDlgItem( hwndDlg, IDC_COMBORESOLUTION ), FALSE );

				}
				else if( wID == IDOK ) {
					if( IsDlgButtonChecked( hwndDlg, IDC_CHECK ) == BST_CHECKED )
						g_bFullscreen = true;
					else
						g_bFullscreen = false;

					g_i32Resolution = SendDlgItemMessage( hwndDlg, IDC_COMBORESOLUTION, CB_GETCURSEL, 0, 0 );

					EndDialog( hwndDlg, IDOK );
				}
				else if( wID == IDCANCEL )
					EndDialog( hwndDlg, IDCANCEL );
			}
		}

	default:
		return FALSE;
		break;
	}
}
 

//
// Load Callback test
//
void	load_callback( float32 f32Percent )
{
	char	szMsg[64];
	_snprintf( szMsg, 64, "Loading %d%%\n", (int32)(f32Percent * 100.0f) );
	OutputDebugString( szMsg );
}



int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
{

	// Show init dialog.
	if( DialogBox( hInstance, MAKEINTRESOURCE( IDD_INIT ), NULL, InitDlgProc ) != IDOK ) {
		return 0;
	}


	DemopajaPlayerC	rPlayer;

	// Init build-in plugins
	rPlayer.register_plugin( &g_rOpenGLDeviceDesc );

	// Load plugins from path.
	rPlayer.load_plugins( "plugins" );


	// Init demo system
	if( !rPlayer.init( hInstance, g_bFullscreen ) ) {
		OutputDebugString( "init failed\n" );
		return -1;
	}

	// set fullscreen mode, if no mode is set, default mode
	// mased on the demo resolution will be choosed.
	switch( g_i32Resolution ) {
	case 0: rPlayer.set_fullscreen_resolution( 400, 300 ); break;
	case 1: rPlayer.set_fullscreen_resolution( 512, 384 ); break;
	case 2: rPlayer.set_fullscreen_resolution( 640, 480 ); break;
	case 3: rPlayer.set_fullscreen_resolution( 800, 600 ); break;
	case 4: rPlayer.set_fullscreen_resolution( 1024, 768 ); break;
	case 5: rPlayer.set_fullscreen_resolution( 1152, 864 ); break;
	case 6: rPlayer.set_fullscreen_resolution( 1280, 960 ); break;
	case 7: rPlayer.set_fullscreen_resolution( 1600, 1200 ); break;
	default: rPlayer.set_fullscreen_resolution( 640, 480 ); break;
	}

/*

	Example to how to load demo data from resource.

	// Load the music from resource
	HRSRC	hMusicRes;
	HGLOBAL	hMusicMem;
	hMusicRes = FindResource( NULL, "music", "MP3" );
	hMusicMem = LoadResource( NULL, hMusicRes );
	uint8*	pMusicData = (uint8*)LockResource( hMusicMem );
	uint32	ui32MusicDataSize = SizeofResource( NULL, hMusicRes );

	rPlayer.set_music_data( pMusicData, ui32MusicDataSize );

	// Load demo from resource.
	HRSRC	hDemoRes;
	HGLOBAL	hDemoMem;
	hDemoRes = FindResource( NULL, "demo", "DMO" );
	hDemoMem = LoadResource( NULL, hDemoRes );
	uint8*	pDemoData = (uint8*)LockResource( hDemoMem );
	uint32	ui32DemoDataSize = SizeofResource( NULL, hDemoRes );*/

	// Set load callback
	rPlayer.set_load_callback( load_callback );

	// Load demo
	if( rPlayer.load_demo( "demo.dmo" ) != IO_OK ) {
		MessageBox( NULL, "Failed to load file \"demo.dmo\"", "Load Error", MB_OK );
		return -1;
	}

	// hide cursor
	while( ShowCursor( FALSE ) > 0 ) {};

	// Run the demo.
	int iRes = rPlayer.run();

	// show cursor
	while( ShowCursor( TRUE ) < 0 ) {};

	return iRes;
}



