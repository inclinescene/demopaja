
                MOPPI DEMOPAJA SDK
                   version 0.71b


This SDK contains libraries to write importer and
effect plugins to the Demopaja and also source and
libraries to play Demopaja files.

The documentation is places in the "doc" directory
and contains full class reference and some explanations
how things work. I hope to make the documentation better
in the future so any feedback is welcome.

There are three example plugins included. The first plugin
contains a importer for PCX images and a simple effect
which displays an image. The second demonstrates how to
create procedural data in Demopaja, the plugins creates
a simple flare image. The third plugin includes ASE loader
and ASE player. The examples are in "example" directory.
The "example" directory also contains the Demopaja player.

The contrib directory contains useful code and tools
contributed by the Demopaja users. Currently there is
Demopaja plugin wizard by Aaron Hilton (aka OTri).
See the readme file inside that directory for mode info.

The classidgen.exe is a random plugin class ID generator.
See the documentation why and where it is used.

To compile the examples you will need FMOD 3.40
extracted in the fmodapi340 directory. FMOD can be
downloaded from http://www.fmod.org

Some plugins included in the example may require
Intel JPEG Library. The IJL comes with Demopaja
package.

The DirectX 8.0 driver has not been updated. If there's
need for the driver, I'll update it. Mail me if you need it.


--memon
<memon@inside.org>